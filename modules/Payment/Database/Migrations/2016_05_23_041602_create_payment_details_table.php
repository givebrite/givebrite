<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDetailsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('donation_id')->unsigned()->index();
            $table->foreign('donation_id')->references('id')->on('campaign_donations')->onDelete('cascade');
            $table->string('payment_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_details');
    }

}
