<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsGiftaddToCampaignDonationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_donations', function(Blueprint $table)
        {
			$table->boolean('is_giftaid')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_donations', function(Blueprint $table)
        {
			$table->dropColumn('is_giftaid')->default(0);
        });
    }

}
