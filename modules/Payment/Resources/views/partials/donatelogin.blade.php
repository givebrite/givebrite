<div class="signUp-form">
    <div class="headingForm">Or sign up with email address</div>
    <div class="form-sec">
        {!! Form::hidden('sign_up_option', 'indiviual') !!}
        <div class="form-row multiinputs clearfix">
            <div class="inputBlock">
                {!! Form::text('first_name', null, ['class' => 'form-control','placeholder' => 'First Name']) !!}
                {!! $errors->first('first_name', '<div class="text-danger">:message</div>') !!}
            </div>
            <div class="inputBlock">
                {!! Form::text('sur_name', null, ['class' => 'form-control','placeholder' => 'Surname']) !!}
                {!! $errors->first('sur_name', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        <div class="form-row">
            <div class="inputBlock">
                {!! Form::text('email', null, ['class' => 'form-control','placeholder' => 'E-mail Address']) !!}
                {!! $errors->first('email', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        <div class="form-row">
            <div class="inputBlock">
                {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) !!}
                {!! $errors->first('password', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        <div class="address">
            <div class="form-row clearfix">
                <div class="inputBlock">
                    {!! Form::text('postcode', null, ['class' => 'form-control','placeholder' => 'Enter Your Postcode','id'=>'postcode']) !!}
                    {!! $errors->first('postcode', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            <div class="form-row">
                <div class="inputBlock">
                    {!! Form::text('address', null, ['class' => 'form-control','placeholder' => 'Address']) !!}
                    {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            <div class="show-Location" id="location-div">
                <div class="form-row">
                    <div class="inputBlock">
                        {!! Form::text('locality', null, ['class' => 'form-control','placeholder' => 'Town']) !!}
                        {!! $errors->first('locality', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="inputBlock">
                        {!! Form::text('country', null, ['class' => 'form-control','placeholder' => 'Country']) !!}
                        {!! $errors->first('country', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>
            </div>
            <input name="lat" type="hidden">
            <input name="lng" type="hidden">
        </div>
        <div class="text-center form-row privacyPolicy">By signing up you agree to the <a href="#">Terms</a> and <a href="#">Privacy Policy</a></div>
        <div class="form-row">
            <div class="inputBlock checkboxSec"> <span><input type="checkbox" value="1" name="receive_update" id="checkboxG4" class="css-checkbox" /><label for="checkboxG4" class="css-label">Receive updates from us?</label></span></div>
        </div>
    </div>
</div>