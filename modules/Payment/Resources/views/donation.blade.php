@extends('site.layouts.master')

@section('title', 'Donation' )

@section('style')
    <link rel="stylesheet" href="{{ asset('modules/payment/css/payment-custom.css') }}">
    <style type="text/css">
        
        .paymentDetails p{

            margin: 10px 0 38px !important;
            
        }

    </style>
    
@stop

@section('content')
    <?php $base_path_campaign = Config::get('config.thumb_campaign_upload_path');?>

    <div class="container donatePage-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="leftColHeading donate-heading">Donation Summary</div>
                <input type="hidden" id="new-amount" value="{{ $amountToStripe }}">
                <div class="paymentDetails payment-content">
                    <div class="payment-row clearfix payment-heading">
                    <span class="label">Your Donation Amount
                        @if(isset($donation_type) && !empty($donation_type))
                            <br/><span class="user-label">- Donation Type: <span
                                        class="orange-text">{{$donation_type->title}}</span></span>

                        @endif
                    </span>
                        <span id="amount-display-one" class="value">&pound;{{ $amount }}
                    </span>
                    </div>
                    <div class="payment-row clearfix payment-condition">
                        <span class="donation-check"><input type="checkbox" name="checkboxG4" id="checkboxG4"
                                                            class="css-checkbox" value="{{ $amountToStripe }}"
                                                            onchange="document.getElementById('sendNewSms').disabled = !this.checked;"/><label
                                    for="checkboxG4" class="css-label"></label></span>
                        <span class="charity-amount-check">
                            <?php $tx_cost = $amountToStripe - $amount; ?>
                            This transaction will cost the charity <strong>&pound;{{number_format($tx_cost, 2, '.', '')}}  </strong> I would like to pay towards the cost.
                    </span>
                        <span class="pull-right">
                        <span id="amount-display-two"
                              class="paySec">&pound;{{number_format($amountToStripe-$amount, 2, '.', '')}}</span>
                    </span>
                    </div>
                    <div class="payment-row clearfix payment-total">
                        <span class="label">Total</span>
                        <span id="amount-display-three"
                              class="value total-value">&pound;{{number_format($amount, 2, '.', '')}}</span>
                    </div>
                    <form id="billing-form" action="{{URL::to('donation/'.$slug)}}" method="POST">
                        <input type="hidden" value="0" id="is_fees_paid_by_donor" name="is_fees_paid_by_donor">
                        <input type="hidden" value="0" id="transaction_fee" name="transaction_fee">
                        @if(isset($donation_type) && !empty($donation_type))
                            <input type="hidden" value="{{$donation_type->id}}" id="donation_type_id"
                                   name="donation_type_id">
                        @else
                            <input type="hidden" value="0" id="donation_type_id"
                                   name="donation_type_id">
                        @endif

                        @if(Helper::checkCampaignType($campaign->campaignId) == "Charity")
                            <?php

                            $charityId = !empty($campaign->charity_id) ? $campaign->charity_id : $campaign->user_id;
                            $charity = \Modules\Auth\Entities\UserProfile::where('user_id', $charityId)->first();
                            $charityName = $charity->charity_name;

                            ?>
                            <div class="mobileDiv">
                                <div class="mobile-bg">
                                    <div class="payment-row clearfix giftaidDiv">
                                        <div class="col-sm-3 giftAidTextFirst">Increase your donation to</br><span
                                                    class="giftAddAmount">&pound;{{Helper::showMoney(($amount*125)/100,"sterling",2)}}</span></br>
                                            at no extra cost to you
                                        </div>
                                        <div class="col-sm-1 col-xs-2 col-xs-offset-2 giftaidCheckBox">
                                            <input name="is_giftaid" id="is_giftaid" class="css-checkbox"
                                                   type="checkbox"><label for="is_giftaid" class="css-label"></label>
                                        </div>
                                        <div class="col-sm-3 giftAidTextSecond col-xs-8">
                                            Tick here if you would like <span
                                                    class="charity-name">{{!empty($charityName) ? $charityName : "charity"}}</span>
                                            to reclaim the tax you have paid on all your donations made in the last four
                                            years, and any future donations you may make.*
                                        </div>
                                    </div>
                                </div>
                                <div class="donation-outside-text">
                                    <p>* To qualify for Gift Aid, you must pay as much UK income and/or capital gains
                                        tax as {{!empty($charityName) ? $charityName : 'charity'}} (and any other
                                        organisation you may support) will reclaim in each tax year (6 April to 5
                                        April), currently 25p for every £1 you donate. If you pay less it is your
                                        responsibility to pay back any difference. Gift Aid will be used to
                                        fund {{!empty($charityName) ? $charityName : 'charity'}}’s general work.</p>
                                </div>
                            </div>

                            <div class="form-sec payaddress-margin">

                                <div class="row" id="address_info">
                                    
                                    <div class="col-md-4 col-sm-6">
                                        
                                         <div class="row postcodeDiv" style="display: none;">
                                        
                                            <div class="col-md-12">
                                                <h3> Address Information</h3>
                                            </div>

                                            <div class="form-row multiinputs clearfix">
                                                
                                                <div class="col-xs-7 col-sm-6 col-md-9 col-lg-9">
                                                 <input id="user_postcode" type="text" name="postcode"
                                                           class="form-control" placeholder="Postcode"
                                                           value="{{$user_profile_data->postcode}}" 
                                                           postCodeStatus="404">
                                                
                                                        <div class="col-lg-12" style="z-index:9; width: 130%;">
                                                            <p>
                                                                
                                                                <span class="text-primary" id="postCodeMessage"> </span>

                                                                 <span class="minor pull-right">
                                                                    <a class="location-link">Enter Address manually</a>
                                                                 </span> 

                                                            </p>
                                                        </div>

                                                </div>
                                               
                                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 searchPostCode">
                                               
                                                    <a href="javascript:void(0)" id="searchPostCode">Search</a>

                                                </div>
                                                   
                                               
                                            </div>
                                            
                                        </div>


                                         <!--select dropdown-->
                                             
                                         <div class="form-row" id="AddressSelectMain" style="position: static; display:none">
                                           
                                           <div class="register-select"> 
                                            
                                            <select name="addressSelect" id="addressSelect" class="valid" >
                                                
                                                <option value="">Please Select your address</option>

                                            </select>

                                           </div> 
                                            
                                            <div class="select__arrow"></div>
                                            
                                        </div>

                                        <!--select dropdown-->
                                        <div class="addressFields" style="display: none;">
    
                                        <div class="form-row">
                                            <div class="inputBlock">
                                                <input id="user_add" type="text" name="address" class="form-control"
                                                       placeholder="Address" value="{{$user_profile_data->address}}">
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="inputBlock">
                                                <input id="user_locality" type="text" name="locality" class="form-control"
                                                       placeholder="locality" value="{{$user_profile_data->locality}}">
                                            </div>
                                        </div>


                                        <div class="form-row">
                                            <div class="inputBlock">
                                                <input id="user_country" type="text" name="country" class="form-control"
                                                       placeholder="Country" value="{{$user_profile_data->country}}">
                                            </div>
                                        </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                        @endif

                        <?php $donate_amount = $amountToStripe * 100?>
                        <br><br>
                        <div class="green-btn-stripe">
                            <input type="hidden" id="actual-amount" name="actual-amount" value="{{ $amount }}">
                            <input type="hidden" id="amount-by-user" name="amount-by-user" value="{{ $amount }}">
                            @if(isset($donation_type) && !empty($donation_type))
                            <input type="hidden" name="donation_type_title" value="{{$donation_type->title}}">
                            @else
                            <input type="hidden" name="donation_type_title" value="">
                            @endif    
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key={{Config::get('app.stripe_publishable_key')}}
                                    data-name="GiveBrite"
                                    data-description="GiveBrite - {{$campaign->campaign_name}}"
                                    data-image="{{ asset('site/images/default.png') }}"
                                    data-locale="auto"
                                    data-currency="GBP"
                                    data-email="{{ (Auth::check() && Session::has('user_session_data')) ? Session::get('user_session_data')['email'] : '' }}">
                            </script>
                        </div>
                    </form>
                    <div class='payment-errors'></div>
                </div>

            </div>
        </div>

    </div>
    </div>
    <?php $user_id = Session::all()['user_session_data']['id']?>
@stop
@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&signed_in=true&libraries=drawing&callback=initMap"
            async defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
    <script src="{{ asset('modules/payment/js/payment.js') }}"></script>
    <script>
        user_id = '<?php echo $user_id ?>';
        $(function () {
            $('.percent').percentageLoader({
                valElement: 'p',
                strokeWidth: 5,
                bgColor: '#d9d9d9',
                ringColor: '#37c2df',
                textColor: '#9f9f9f',
                fontSize: '18px',
                fontWeight: 'bold'
            });
        });

        $("#checkbox_unlink").click(function () {
            if ($(this).is(':checked')) {
                user_amt = $("#amount-display-one").html();
                user_amt = Number(user_amt.substring(1));
                stripe_charge = $("#amount-display-two").html();
                stripe_charge = Number(stripe_charge.substring(1));
                total_amt = user_amt + stripe_charge;
                $("#amount-display-three").html('&pound;' + total_amt)
                $("#new-amount").val(total_amt)
            } else {
                $("#amount-display-three").html('&pound;' + user_amt)
                $("#new-amount").val(user_amt);
            }
        });

        function valid_postcode(postcode) {
        
            postcode = postcode.replace(/\s/g, "");
            var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
            return regex.test(postcode);
       
        }

        $(document).ready(function () {

            $("#is_giftaid").click(function () {
                            
                var user_add = $("#user_add").val();
                var user_country = $("#user_country").val();
                var user_postcode = $("#user_postcode").val();

                if ($(".stripe-button-el").attr('disabled')) {
                    $(".stripe-button-el").attr('disabled', false);
                } else {
                    $(".stripe-button-el").attr('disabled', 'disabled');
                }
                if (user_add != '' && user_country != '' && user_postcode != '') {
                    $(".stripe-button-el").attr('disabled', false);
                }
                if ($(this).is(":checked")) {

                    $(".postcodeDiv").show();//$("#address_info").show();

                    if((user_add != '') || (user_country != '') || (user_postcode != '')){

                        $(".addressFields").show();
                    
                    } else {

                        $(".addressFields").hide();
                    }

                    //postcode validator 
                    var user_postcode = $("#user_postcode").val();
                    var response = valid_postcode(user_postcode);
                    console.log(response);
                    if(response == false){

                        $(".stripe-button-el").attr('disabled', 'disabled');

                    } else {

                        $(".stripe-button-el").attr('disabled', false);
                    }
                    //postcode validator 
                    
                    //$(".addressFields").show();

                } else {

                    $(".postcodeDiv").hide(); 
                    $(".addressFields").hide();//$("#address_info").hide();
                    //$(".addressFields").hide();
                }
            });
            tx_cost = '<?php echo $tx_cost; ?>';
            $("#checkboxG4").click(function () {
                if ($(this).is(':checked')) {
                    $("#is_fees_paid_by_donor").val(1);
                    $("#transaction_fee").val(tx_cost.toFixed(2));
                } else {
                    $("#is_fees_paid_by_donor").val(0);
                    $("#transaction_fee").val(0);
                }
            });


            $("#address_info input").on('keyup', function () {
                
                var address = $('#user_add').val();
                var country = $('#user_country').val();
                var postcode = $('#user_postcode').val();
                if (address != '' && country != '' && postcode != '') {

                    //postcode verification
                    var response = valid_postcode(postcode);
                    console.log(response);
                    
                    if(response == false){

                        $(".stripe-button-el").attr('disabled', 'disabled');

                    } else {

                        $(".stripe-button-el").attr('disabled', false);
                    }
                    //postcode verification
                    

                } else {

                    $(".stripe-button-el").attr('disabled', 'disabled');
                }
         
            });

            $(".stripe-button-el").click(function (e) {
                
                if ($("#is_giftaid").is(':checked')) {
                    var address = $('#user_add').val();
                    var country = $('#user_country').val();
                    var postcode = $('#user_postcode').val();

                    var token =  $('meta[name="csrf-token"]').attr('content');
                    data = {user_id: user_id, address: address, country: country, postcode: postcode}
                    $.ajax({
                        method: "POST",
                        type: 'json',
                        url: site_url + '/add_donation_address',
                        data: {
                            "_token": token,
                            "data": data
                        },
                        success: function (res) {
                            
                            console.log(res);
                        },
                        error:function (xhr, ajaxOptions, thrownError){

                            if(xhr.status==404) {
                               
                               alert('please refresh the page');
                            }
                        }
                    });
                }
            });

            $('#searchPostCode').click(function(e) {
              
              var string = $('#user_postcode').val();
              $(".stripe-button-el").attr('disabled', 'disabled');

              $.ajax({
                    url: "https://api.ideal-postcodes.co.uk/v1/postcodes/"+string+"?api_key=iddqd",
                    dataType: "json",
                    type: "get",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $('#user_postcode').val(data.result[0]['postcode']);
                        $('#addressSelect').html('<option>Please Select your address </option>');
                        $('#postCodeMessage').html("");    

                        $.each(data.result, function(index, element) {

                        $('#addressSelect').append('<option value="'+ element.line_1+' , '+element.district+' ,'+element.country+' ">'+ element.line_1+' , '+element.district+' ,'+element.country+' </option>');
                        
                        });
                        $('#AddressSelectMain').show();
                        
                     },
                    error:function (xhr, ajaxOptions, thrownError){

                        if(xhr.status==404) {
                                
                              $('#postCodeMessage').html('Please enter a valid postcode');
                              $(".stripe-button-el").attr('disabled', 'disabled');
                         }
                    }
                });

                //console.log(this.value);
          });

            $('#addressSelect').on('change', function() {

                  var address = this.value;
                  var arr = address.split(',');
                  $('#user_add').val(arr[0]);
                  $('#user_locality').val(arr[1]);
                  $('#user_country').val(arr[2]);
                  $('#postCodeMessage').html('');
                  $(".addressFields").show();
                  $('#AddressSelectMain').hide();
                  $(".stripe-button-el").attr('disabled', false);
                  console.log(arr);
            });

            $('.location-link').click(function(){
              
               $(".addressFields").toggle();
            
       
            });

        });
    </script>
@stop
