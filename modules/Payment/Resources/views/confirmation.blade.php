@extends('site.layouts.master')

@section('title', 'Comment')

@section('style')
<link rel="stylesheet" href="{{ asset('modules/payment/css/payment-custom.css') }}">
@stop

@section('content')

<div class="container donatePage-content">
    <div class="row">
        <div class="col-sm-12 col-md-7">
            <div class="confirmHeading">Thank you for your <span>donation!</span></div> 
            <div class="comment-section form-sec">
                {!! Form::open(array('route' => ['comment', $slug])) !!}
                <div class="labelSec">Leave a comment</div>
                <div class="textareablock">
                    {!! Form::textarea('comment') !!}
                    {!! $errors->first('comment', '<div class="text-danger">:message</div>') !!}
                </div>
                
                <div class="formbtns">
                    {!! Form::submit('Submit Comment', array('class'=>'blue-btn-lg', 'name'=>'named')) !!}
                    {!! Form::submit('Submit Anonymously', array('class'=>'comment-anonymous grey-btn-lg', 'name'=>'anonymous')) !!}
                </div>
                {!! Form::close() !!}
            </div>      
        </div>
        <div class="col-sm-0 col-md-4 donateRightCol col-md-offset-1">
        </div>
    </div>
</div>
@stop
