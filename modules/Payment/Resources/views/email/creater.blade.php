<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
        <title>:: Welcome to Site ::</title>
        <style type="text/css">
            body {
                background: #f6f5f2;
                font-family: Arial, Helvetica, sans-serif;
                color: #b8b8b8;
                font-size: 14px;
            }
            .setW100P {
                width: 650px;
            }
            * {
                margin: 0px;
                padding: 0px;
                box-sizing: border-box;
            }
            .ExternalClass * {
                line-height: 121%;
            }
            table {
                border-collapse: collapse;
            }
            table td {
                border-collapse: collapse;
            }
            img {
                border: none;
                outline: none;
            }
            @media(max-width:750px) {
                .setW100P {
                    width: 493px !important;
                }
            }
            @media(max-width:500px) {
                .setW100P {
                    width: 80% !important;
                }
            }
        </style>
    </head>
    <body>
        <div align="center" style="background: #f6f5f2; font-family: Arial, Helvetica, sans-serif; color: #b8b8b8; font-size: 14px;">
            <table style="border:none; font-size: 14px; width:650px;" align="center">
                <tr>
                    <td align="center" valign="top" style="padding-top:30px; padding-bottom:60px;"><img src="images/email-logo.png" /></td>
                </tr>
                <tr>
                    <td align="center" valign="top" bgcolor="#ffffff" style="border-radius:3px; ">
                        <table align="left" class="table-left-padd" border="0" cellspacing="0" cellpadding="0" width="100%">

                            <tr>
                                <td style="border-bottom:1px solid #f2f2f2; padding:29px 0px 20px 40px;"><!--1px solid #f2f2f2--> 
                                    <img src="images/woohoo-img.png" /></td>
                                <td style="border-bottom:1px solid #f2f2f2; padding:29px 0px 10px 40px; font-size:14px; color:#b8b8b8;"><!--1px solid #f2f2f2--> 
                                    Summary of your Donation</td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table align="left" class="table-left-padd" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td style="padding:15px 20px 0px 40px; background:#e5f0f2;">
                                    <p style="font-size:19px; color:#494949; padding-bottom:5px;">You received a generous donation</p>
                                </td>
                            </tr>

                            <tr>
                                <td style="background:#e5f0f2; padding:10px 20px 10px 40px;">
                                    <table align="left" class="table-left-padd" border="0" cellspacing="0" cellpadding="0" width="100%" style="font-size:12px; color:#494949;">
                                        <tr>
                                            <td width="30%">Received</td>
                                            <td width="30%">Campaign</td>
                                            <td width="40%">Donation Type</td>
                                        </tr>
                                    </table>


                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table align="left" class="table-left-padd" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td style="padding:15px 20px 20px 40px; background:#f7fafa;">

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; color:#494949;">
                                        <tr>
                                            <td width="30%" style="color:#00bd43; font-weight:bold;">&pound;{{ $amount }}</td>
                                            <td width="30%">{{ $campaign_name }}</td>
                                            <td width="40%">Lillah</td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="background:#ffffff; padding:20px 20px 20px 40px;">
                        Keep up the good work.
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:35px;" align="center"> Unsubscribe </td>
                </tr>
                <tr>
                    <td style="padding-top:25px;" align="center"> GiveBrite HQ<br />
                        Abbey House, 10 Abbey Hills Road, <br />
                        Greater Manchester, OL8 2BS </td>
                </tr>
            </table>
        </div>
    </body>
</html>
