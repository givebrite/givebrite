@extends('site.layouts.master')

@section('title', 'Donate')

@section('content')
    <?php $base_path_campaign = Config::get('config.thumb_campaign_upload_path'); ?>
    <div class="container donatePage-content">
        <div class="row">
            <div class="donation_form_bk">
                {!! Form::open(array('route' => ['donate', $campaign->slug], 'novalidate' => 'novalidate')) !!}
                <div class="modal-header">
                    <h4 class="modal-title">Donate</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-donation-amout">
                        <label for="donation_amount">Donation amount <span class="astrikBlock">*</span></label>
                        <div class="inputBlock">
                            <p class="gbpBtn">
                                <input type='hidden' name='goal_amount_type' value='monetary' placeholder="&pound;">
                                <span class="sterling">&pound;</span>
                                {!! Form::text('amount',Session::has('donationAmount') ? Session::get('donationAmount') : '' , ['id'=>'goal_amount_monetary']) !!}
                            </p>
                            {!! $errors->first('amount', '<div class="text-danger">:message</div>') !!}
                        </div>
                        {{--<input type="text" id="donation_amount" name="amount"--}}
                               {{--value="{{ Session::has('donationAmount') ? Session::get('donationAmount') : ''}}"--}}
                               {{--placeholder="&pound;"/>--}}
                    </div>
                    @if(!empty($donation))
                        <div class="choose-donation">
                            <span>Choose donation type</span>
                        </div>
                    @endif
                    <div class="select-donation">
                        @foreach($donation as $d)
                            <div class="select-donation-type">
                                <label class="control control--radio">
                                    <input
                                            @if($d->is_default)
                                            checked
                                            @endif
                                            type="radio" name="default" value="{{$d->id}}">
                                    {{$d->title}}
                                    <div class="control__indicator"></div>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Donate', array('class'=>'btn btn-success')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @stop
        @section('script')
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&signed_in=true&libraries=drawing&callback=initMap"
                    async defer></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
            <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
            <script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
            <script src="{{ asset('js/amount-pre-fillable.js') }}"></script>
            <script>
                $("#donation_amount").change(function () {
                    gbpAmount();
                });
            </script>
            <script>
                $(function () {
                    $('.percent').percentageLoader({
                        valElement: 'p',
                        strokeWidth: 5,
                        bgColor: '#d9d9d9',
                        ringColor: '#37c2df',
                        textColor: '#9f9f9f',
                        fontSize: '18px',
                        fontWeight: 'bold'
                    });
                });
            </script>
@stop