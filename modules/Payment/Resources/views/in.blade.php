@extends('site.layouts.master')

@section('title', 'Donation' )

@section('style')
<link rel="stylesheet" href="{{ asset('modules/payment/css/payment-custom.css') }}">
@stop

@section('content')
<?php $base_path_campaign = Config::get('config.thumb_campaign_upload_path');?>
<div class="container donatePage-content">
    <div class="row">
        <div class="col-sm-12 col-md-7 donation-form-col-width">
            <div class="leftColHeading">Donation Summary</div>
            <input type="hidden" id="new-amount" value="{{ $amountToStripe }}">
            <div class="paymentDetails">
                <div class="payment-row clearfix">
                    <span class="label">Donation</span>
                    <span id="amount-display-one" class="value">&pound;{{ $amount }}
                    </span>
                </div>
                <div class="payment-row clearfix">
                    <div class="col-sm-1">
                        <input type="checkbox" name="checkboxG4" id="checkboxG4" class="css-checkbox" value="{{ $amountToStripe }}" onchange="document.getElementById('sendNewSms').disabled = !this.checked;"/><label for="checkboxG4" class="css-label"></label>
                    </div>
                    <div class="col-sm-9 charity-amount-check">
                        This transaction will cost the charity I would like to pay towards the cost.
                    </div>
                    <div class="col-sm-2 pull-right">
                        <span id="amount-display-two" class="paySec">&pound;{{ $amountToStripe-$amount }}</span>
                    </div>
                </div>
                
                <div class="payment-row clearfix">
                    <span class="label">Total</span>
                    <span id="amount-display-three" class="value">&pound;{{ $amount }}</span>
                </div>
                <form id="billing-form" action="{{URL::to('donation/'.$slug)}}" method="POST">
                <div class="payment-row clearfix" style="background: #37c2df">
                    <div class="col-sm-3 giftaid text-bold"><i>giftaid it</i></div>
                    <div class="col-sm-3">Increase your donation to</br><span class="giftAddAmount">{{Helper::showMoney(($amount*125)/100,"sterling",2)}}</span></br> at no extra cost to you</div>
                    <div class="col-sm-3 giftaidCheckBox">
                        <input name="is_giftaid" id="is_giftaid" class="css-checkbox" type="checkbox"><label for="is_giftaid" class="css-label"></label>
                    </div>
                    <div class="col-sm-3">
                        Tick here If You would like 
                        <span class="charity-name">charity name</span>
                    </div>
                </div>
                <?php $donate_amount = $amountToStripe * 100 ?>
                <div class="green-btn-stripe">
                    <input type="hidden" id="actual-amount" name="actual-amount" value="{{ $amount }}">
                    <input type="hidden" id="amount-by-user" name="amount-by-user" value="{{ $amount }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <script
                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                        data-key="{{ $publishable_key }}"
                        data-name="GiveBrite"
                        data-description="The Power of Giving"
                        data-image="{{ asset('site/images/default.png') }}"
                        data-locale="auto"
                        data-currency="GBP"
                        data-email="{{ (Auth::check() && Session::has('user_session_data')) ? Session::get('user_session_data')['email'] : '' }}"
<!--                        redirect_url="http://vhost.com/GiveBright/public/donation/hrdya"-->
                    </script>
            </form>
            <div class='payment-errors'></div>
        </div>

        </div>        
        </div>
        <div class="col-sm-12 col-md-4 donateRightCol col-md-offset-2">
            <ul class="clearfix listEplore">
                <li>
                    
                    <a href="{{URL::route('campaign.show', $campaign->slug)}}"><div style='background-image:url("{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($base_path_campaign.$campaign->campaign_image ) ?  asset($base_path_campaign.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}")' class="imgContainer">
                    </div></a>
                    <div class="listDetails clearfix">
                        <div class="detialsLeft">
                            <div class="price">{{Helper::showMoney( $campaign->goal_amount_monetary) }}</div>
                           <div class="type">
                               <a href="{{URL::route('campaign.show', $campaign->slug)}}">   {{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 15, $end = '...') :''}}  </a>                                      
                           </div>
                           <div class="location">
                               {{Helper::showAddress($campaign->cityName,$campaign->locationName,15)}}
                           </div>
                        </div>
                        <div class="detialsRight">
                            <?php $goalPercent = !empty($totalAmountRaised) ? floor(($totalAmountRaised / $campaign->goal_amount_monetary) * 100) : 0; ?>
                            <div class="progressOuter inherit donate-progress-bar" style="width:70px;height:70px;margin:18px auto; margin-top: 50px;">
                                <div class="percent" style="width:70px;height:70px;">
                                    <p style="display:none;">{{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="donate-value">
                        <span class="aValue" style='color:#37c2df'>{{Helper::showMoney($totalAmountRaised,$campaign->campaign_currency)}}</span>
                        <span>of</span>
                        <span class="oValue">{{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}</span>
                    </div>
                    @if(!empty($totalDonors))
                    <div class="campaignDetials">
                        <div class="shareDonor" >
                          <span class="aValue" style='color:#37c2df'> {{ !empty($totalDonors) ? $totalDonors : 0 }} </span> <span>{{ !empty($totalDonors) && $totalDonors > 1 ? "backers" : "backer" }}</span>
                        </div>
                    </div>
                    @endif
                </li>
            </ul>
        </div>
        </div>
        </div>  
        @stop
        @section('script')
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&signed_in=true&libraries=drawing&callback=initMap" async defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
<script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
<script src="{{ asset('modules/payment/js/payment.js') }}"></script>
<script>
$(function () {
    $('.percent').percentageLoader({
        valElement: 'p',
        strokeWidth: 5,
	bgColor: '#d9d9d9',
        ringColor: '#37c2df',
	textColor: '#9f9f9f',
	fontSize: '18px',
	fontWeight: 'bold'
    });
});
</script>
        @stop
Blog  <?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

