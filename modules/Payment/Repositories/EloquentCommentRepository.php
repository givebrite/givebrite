<?php

namespace Modules\Payment\Repositories;

Use Modules\Payment\Entities\Comment as CommentModel;
use DB;

class EloquentCommentRepository
{

    /**
     * Create model
     * @return UserActivationModel
     */
    public function getModel()
    {
        return new CommentModel();
    }

    /**
     * Insert into the user_activation
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    /**
     * Find from the user_activation
     * @return type
     */
    public function findAll()
    {
        return $this->getModel()->all();
    }

    public function findByCharity($charity_id, $limit = 0)
    {
        $query = DB::table('comments')
            ->join('campaigns', 'campaigns.id', '=', 'comments.campaign_id')
            ->join('campaign_donations', 'campaign_donations.campaign_id', '=', 'campaigns.id')
            ->join('user_profile', 'user_profile.user_id', '=', 'comments.user_id')
            ->join('users', 'users.id', '=', 'user_profile.user_id')
            ->where(function ($que) use ($charity_id) {
                $que->where('campaigns.user_id', $charity_id);
                $que->where('campaigns.status', 1);
                $que->where('users.status', 1);
                $que->orWhere('campaigns.charity_id', $charity_id);
            })
            ->groupBy('comments.id')
            ->select(
                'comments.is_anonymous', 'comments.content', 'campaign_donations.donation_amount_monetory', 'user_profile.first_name', 'user_profile.sur_name', 'user_profile.profile_pic', 'user_profile.slug'
            );
        if($limit){
            return $query->orderBy('comments.updated_at', 'desc')->offset(10)->limit($limit)->get();
        }else{
            return $query->orderBy('comments.updated_at', 'desc')->offset(10)->limit(1000)->get();
        }




        //return $query->orderBy('comments.updated_at', 'desc')->limit(2)->offset(0)->get();
    }

    public function findByCampaignId($campaignId)
    {
        return DB::table('comments')
            ->where('campaign_id', $campaignId)
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
