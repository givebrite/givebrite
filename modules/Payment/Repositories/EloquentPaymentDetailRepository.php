<?php

namespace Modules\Payment\Repositories;

Use Modules\Payment\Entities\PaymentDetail as PaymentDetailModel;

class EloquentPaymentDetailRepository
{
    /**
     * Create model
     * @return UserActivationModel
     */
    public function getModel()
    {
        return new PaymentDetailModel();
    }

    /**
     * Insert into the user_activation
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    /**
     * Find from the user_activation
     * @return type
     */
    public function findAll()
    {
        return $this->getModel()->all();
    }

}
