<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';
    
    protected $fillable = ['campaign_id', 'user_id', 'content', 'is_anonymous','donation_id'];
}
