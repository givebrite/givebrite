<?php

namespace Modules\Payment\Acme\Providers;

use Illuminate\Support\ServiceProvider;

class BillingServiceProvider extends ServiceProvider
{
    public function register() {
        $this->app->bind('Modules\Payment\Acme\Billing\BillingInterface','Modules\Payment\Acme\Billing\StripeBilling');
    }
}
