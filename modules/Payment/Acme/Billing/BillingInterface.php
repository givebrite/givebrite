<?php
namespace Modules\Payment\Acme\Billing;

interface BillingInterface
{
    public function charge(array $data);
}