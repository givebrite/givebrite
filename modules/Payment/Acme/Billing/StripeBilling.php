<?php

namespace Modules\Payment\Acme\Billing;

use Config;
use Helper;
use Modules\Campaigns\Entities\CampaignPayment as CampaignPaymentModel;
use Modules\Campaigns\Repositories\EloquentCampaignLaunchRepository as CampaignPaymentRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Stripe\Charge;
use Stripe\Stripe;

class StripeBilling implements BillingInterface {

	public function charge(array $data) {
		
		$error = "Payment Unsuccessful";
		//creates payment on stripe
		
		try {

			//creates campaign model
			$campaign_payment_model = new CampaignPaymentRepo();

			$campaignType = Helper::checkCampaignType($data['campaign_id']);
			// Creating New Instnace For Campaign repo
			$campaignRepo = new CampaignRepo();

			$campaign = $campaignRepo->findById($data['campaign_id']);
			if ($campaignType == 'Charity') {
				$charityId = !empty($campaign->charity_id) ? $campaign->charity_id : $campaign->user_id;
				$campaign_payment_response = CampaignPaymentModel::where('user_id', $charityId)
					->where('campaign_id', 0)
					->where('payment_option_id', 3)
					->where('status', 'yes')
					->first();

			} else {
				$campaign_payment_response = $campaign_payment_model->findByCampaignId($data['campaign_id']);
			}

			$connect_user_id = $campaign_payment_response->connect_stripe_user_id;
			$secret_key = Config::get('app.stripe_secret_key');

			//set stripe key
			Stripe::setApiKey($secret_key);

			$amount = $data['amount'];
			$token = $data['token'];
			$email = $data['email'];
			$app_fees = $data['app_fees'];
			$campaign_name = $data['campaign_name'];
			$campaign_ID = $data['campaign_id'];
			$user_id = $data['user_id'];
			$is_giftaid = $data['is_giftaid'];
			$donation_type_title = $data['donation_type_title'];
			$donation_id = $data['donation_id'];
			
			Charge::create([
				'amount' => $amount,
				'currency' => "GBP",
				'description' => "Donation Ref: ".$donation_id." | ".$campaign_name,
				'source' => $token,
				'application_fee' => $app_fees,
				"metadata" => array("campaign_name" => $campaign_name,"campaign_id"=>$campaign_ID,"user_id"=>$user_id,
					'is_giftaid'=>$is_giftaid,'donation_type_title'=>$donation_type_title,'donation_id'=>$donation_id),
			], array("stripe_account" => $connect_user_id)
			);
			return 'success';
		}

		//check for exceptions
		 catch (\Stripe\Error\Card $e) {
			$error = "Card was Declined";
		} catch (\Stripe\Error\InvalidRequest $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\Authentication $e) {
			$error = "Authentication with Stripe's API failed (maybe you changed API keys recently)";
		} catch (\Stripe\Error\ApiConnection $e) {
			$error = "Network communication with Stripe failed";
		} catch (\Stripe\Error\Base $e) {

		} catch (Exception $e) {

		}
		return $error;
	}

}
