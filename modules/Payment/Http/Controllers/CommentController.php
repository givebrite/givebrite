<?php

namespace Modules\Payment\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Modules\Payment\Repositories\EloquentCommentRepository as CommentRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as CampaignDonationRepo;
use Auth;
use Session;
use Toastr;
use Illuminate\Support\Facades\Redirect;
use Modules\Payment\Http\Controllers\StripeController;

class CommentController extends Controller
{

    function __construct()
    {
        
    }

    /**
     * function for make comment get request
     * @return type
     */
    public function getComment($slug)
    {
        return View::make("payment::confirmation")->with('slug', $slug);
    }

    public function postComment($slug)
    {
        $data = Input::all();
        // Applying validation rules.
        $rules = array(
            'comment' => 'required'
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            // If validation falis redirect back to login.
            return Redirect::back()->withInput()->withErrors($validator);
        }

        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $content = Input::get('comment');

            $stripe = new StripeController();
            $campaign = $stripe->getCampaignObject($slug);
            $campaign_id = $campaign->campaignId;

            $donationRepo = new CampaignDonationRepo();
            $donation = $donationRepo->getNewestDonationByCampaignAndUser($campaign_id, $user_id);
            $donationId = !empty($donation) ? $donation->id : 0;

            $comment_model = new CommentRepo();
            $all_comments = $comment_model->findAll();

            foreach ($all_comments as $comment) {
                if ($comment->donation_id == $donationId || $donationId == 0) {
                    return Redirect::route('404');
                }
            }

            $comment_data = array(
                'campaign_id' => $campaign_id,
                'donation_id' => $donationId,
                'user_id' => $user_id,
                'content' => $content
            );
            $is_anonymous = (Input::get('named')) ? 0 : 1;
            $comment_data['is_anonymous'] = $is_anonymous;

            $comment_model->create($comment_data);
        }

        Session::forget('slug');
        Session::forget('amount');

        Toastr::success("Thanks for commenting", $title = null, $options = []);
        return Redirect::route('campaign.show', $slug);
    }

}
