<?php

namespace Modules\Payment\Http\Controllers;

use App;
use App\CampaignDonor;
use App\DonationType;
use Auth;
use Config;
use Helper;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Modules\Campaigns\Repositories\EloquentCampaignLaunchRepository as CampaignPaymentRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Modules\Payment\Repositories\EloquentPaymentDetailRepository as PaymentDetailRepo;
use Pingpong\Modules\Routing\Controller;
use Request;
use Session;
use Toastr;

class StripeController extends Controller
{

    /**
     * function for donate get request
     * @param type $id
     * @return type
     */
    public function getDonate($slug)
    {
        if (!Auth::check()) {
            //set user session
            Session::forget('create_campaign');
            Session::set('campaign', $slug);
            Session::set('loginuri', Request::segment(2));
            return redirect('/register');
        }
        $campaign = $this->getCampaignObject($slug);
        $campaignDonationRepo = new DonationRepo();
        $donation_type = new CampaignDonor();
        $donation = $donation_type->getAllWithJoin($campaign->campaignId);
        $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaign->campaignId);
        $totalAmountRaised = $totalAmountRaisedQuery[0]->totalAmountRaised;
        $totalDonorQuery = $campaignDonationRepo->findTotalDonorsByCampaignId($campaign->campaignId);
        $totalDonors = !empty($totalDonorQuery) ? count($totalDonorQuery) : 0;
        Session::forget('campaign');

        return view('payment::donate', compact('totalDonors', 'totalAmountRaised', 'campaign', 'donation'));
    }

    /**
     * Function for Post Donate request
     * @return type
     */
    public function postDonate($slug)
    {   
        $data = Input::all();
        
        if (isset(Session::all()['user_session_data']) && Session::all()['user_session_data']['id']) {
            $user_id = Session::all()['user_session_data']['id'];

        } else {

            $user_id = '';
        }

        Session::forget('donationAmount');
        //get request rules
        $rules = array(
            'amount' => 'required|numeric|min:1|max:99999999',
        );

        //check for validations
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {

            Toastr::Error("Input validation failed", $title = null, $options = []);
            // If validation falis redirect back to login.
            return Redirect::back()->withInput()->withErrors($validator);
        }

        //get form input

        if (isset($data['default'])) {
            $donation_type_id = (int)$data['default'];
            $d = new DonationType();
            $donation_type = $d->findById($donation_type_id);
        } else {
            $donation_type = FALSE;
        }

        $amount = $data['amount'];
        $campaign = $this->getCampaignObject($slug);
        $campaign_id = $campaign->campaignId;

        $amountToStripe = Helper::getTotalAmountToBeCharged($amount, $campaign_id);
        
        //create CampaignPayment model and find stripe publishable key by campaign id
        $campaign_payment_model = new CampaignPaymentRepo();
        $campaign_payment_response = $campaign_payment_model->findByCampaignId($campaign_id);
        $publishable_key = Config::get('app.stripe_publishable_key');
        
        
        $campaignDonationRepo = new DonationRepo();
        $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaign->campaignId);
        $totalAmountRaised = $totalAmountRaisedQuery[0]->totalAmountRaised;
        $totalDonorQuery = $campaignDonationRepo->findTotalDonorsByCampaignId($campaign->campaignId);
        $totalDonors = !empty($totalDonorQuery) ? count($totalDonorQuery) : 0;
        
        // Getting UserProfile data for address information
        $user_profile = new UserProfileRepo();
        $user_profile_data = $user_profile->findByUserId($user_id);
        return view('payment::donation', compact('totalDonors', 'campaign', 'totalAmountRaised', 'amountToStripe', 'amount', 'slug', 'publishable_key', 'donation_type', 'user_profile_data'));
    }

    /**
     * Function for donation post request
     *
     * @return type
     */
    // public function postDonation($slug)
    // {
    //     $data = Input::only('amount-by-user', 'actual-amount', 'is_giftaid', 'is_fees_paid_by_donor', 'transaction_fee', 'donation_type_id');

    //     //get request rules
    //     $rules = array(
    //         'amount-by-user' => 'required|numeric|min:1',
    //         'actual-amount' => 'required|numeric|min:1',
    //     );

    //     //check for validations
    //     $validator = Validator::make($data, $rules);
    //     if ($validator->fails()) {

    //         Toastr::Error("Input validation failed", $title = null, $options = []);
    //         // If validation falis redirect back to login.
    //         return Redirect::back()->withInput()->withErrors($validator);
    //     }

    //     $billing = App::make('Modules\Payment\Acme\Billing\BillingInterface');

    //     $email = Input::get('stripeEmail');
    //     $amount = Input::get('actual-amount');
    //     $token = Input::get('stripeToken');
    //     $amountToStripe = Input::get('amount-by-user');

    //     $campaign = $this->getCampaignObject($slug);
    //     $campaign_id = $campaign->campaignId;
    //     $newAmountToStripe = $amountToStripe * 100;
    //     $app_fees = Helper::getApplicationFeeFromDB($amount, $campaign_id);
    //     $new_app_fees = $app_fees * 100;
    //     $payment_response = $billing->charge([
    //         'email' => $email,
    //         'amount' => $newAmountToStripe,
    //         'token' => $token,
    //         'campaign_id' => $campaign_id,
    //         'app_fees' => $new_app_fees,
    //     ]);
    //     if ($payment_response == "success") {
    //         $user_id = Auth::user()->id;
    //         if ($app_fees) {
    //             $app_fees = $new_app_fees / 100;
    //         }
    //         $donation_data = array(
    //             'user_id' => $user_id,
    //             'campaign_id' => $campaign_id,
    //             'donation_amount_monetory' => $amount,
    //             'application_fees_charge' => $app_fees,
    //             'donation_type_id' => $data['donation_type_id'],
    //             'status' => 1,
    //             'is_giftaid' => !empty($data['is_giftaid']) ? 1 : 0,
    //             'is_fees_paid_by_donor' => $data['is_fees_paid_by_donor'],
    //             'transaction_fee' => $data['transaction_fee']
    //         );

    //         $donation_model = new DonationRepo();
    //         $donation_response = $donation_model->create($donation_data);

    //         $payment_detail_data = array(
    //             'donation_id' => $donation_response->id,
    //             'payment_id' => $token,
    //         );

    //         //Save to campaign_donation_details table as the address might change
    //         //Later after the mails are sent Updated over skype  
            
    //         //$donation_model->saveCurrentUserDetails($donation_response->id,$user_id);
            
    //         //Save to campaign_donation_details table as
            
    //         $payment_detail_model = new PaymentDetailRepo();
    //         $payment_detail_model->create($payment_detail_data);

    //         $emailParams = array(
    //             'user_id' => $user_id,
    //             'campaignSlug' => $slug,
    //             'amount' => $amount,
    //         );
    //         $this->sendDonationEmail($emailParams, true);
    //         $this->sendDonationEmail($emailParams, false);

    //         Toastr::Success("Thanks for your donation", $title = null, $options = []);
    //         return Redirect::to("comment/$slug");
    //     }

    //     Toastr::Error($payment_response, $title = null, $options = []);
    //     //return Redirect::route('donate');
    //     return Redirect::to("donate/$slug");
    // }

    public function postDonation($slug)
    {           

        $data = Input::only('amount-by-user', 'actual-amount', 'is_giftaid', 'is_fees_paid_by_donor', 'transaction_fee', 'donation_type_id','donation_type_title');

        //get request rules
        $rules = array(
            'amount-by-user' => 'required|numeric|min:1',
            'actual-amount' => 'required|numeric|min:1',
        );

        //check for validations
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {

            Toastr::Error("Input validation failed", $title = null, $options = []);
            // If validation falis redirect back to login.
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $billing = App::make('Modules\Payment\Acme\Billing\BillingInterface');

        $email = Input::get('stripeEmail');
        $amount = Input::get('actual-amount');
        $token = Input::get('stripeToken');
        $campaignName = Input::get('campaign-name');
        $amountToStripe = Input::get('amount-by-user');
        $donation_type_title = !empty($data['donation_type_title']) ? $data['donation_type_title'] : 'Not selected';

        $campaign = $this->getCampaignObject($slug);
        $campaign_id = $campaign->campaignId;
        $newAmountToStripe = $amountToStripe * 100;
        $app_fees = Helper::getApplicationFeeFromDB($amount, $campaign_id);
        $new_app_fees = $app_fees * 100;
        
        //save to database first
        $user_id = Auth::user()->id;

        $donation_data = array(
                'user_id' => $user_id,
                'campaign_id' => $campaign_id,
                'donation_amount_monetory' => $amount,
                'donation_type_id' => $data['donation_type_id'],
                'status' => 0,
                'is_giftaid' => !empty($data['is_giftaid']) ? 1 : 0,
                'is_fees_paid_by_donor' => $data['is_fees_paid_by_donor'],
                'transaction_fee' => $data['transaction_fee']
        );

        $donation_model = new DonationRepo();
        $donation_response = $donation_model->create($donation_data);
        //

        $payment_response = $billing->charge([
            'email' => $email,
            'amount' => $newAmountToStripe,
            'token' => $token,
            'campaign_id' => $campaign_id,
            'campaign_name'=>$campaignName,
            'is_giftaid' => !empty($data['is_giftaid']) ? 'Yes' : 'No',
            'donation_id' => $donation_response->id,
            'donation_type_title' => $donation_type_title,
            'app_fees' => $new_app_fees,
            'user_id' => Auth::user()->id
            
        ]);

        if ($payment_response == "success") {
                                   
            if ($app_fees) {
                $app_fees = $new_app_fees / 100;
            }

            // $donation_data = array(
            //     'application_fees_charge' => $app_fees,
            //     'status' => 1,
            // );
           
            $donation_model = new DonationRepo();
            //Find and update
            $donation_model->findByDonationId($donation_response->id,$app_fees);

            $payment_detail_data = array(
                'donation_id' => $donation_response->id,
                'payment_id' => $token,
            );

            //also save the donation details in new table campaign_donation_details with the latest address details
            //New update
                
            $house_number = '';
            $street_name = '';
            $address_1 = '';
            $address_2 = '';
            

            $address = Helper::splitAddress(Input::get('address'));
            
            if(!empty($address['houseNumber'])){

                $house_number = $address['houseNumber'];
            }

            if(!empty($address['streetName'])){

                $street_name = $address['streetName'];
            }

            if(!empty($address['additionToAddress1'])){

                $address_1 = $address['additionToAddress1'];
            }

            if(!empty($address['additionToAddress2'])){

                $address_2 = $address['additionToAddress2'];
            }

            $data = array(
            
                'donation_id'  => $donation_response->id,
                'user_id'      => $user_id,
                'postcode'     => Input::get('postcode'),
                'house_number' => $house_number,
                'street_name'  => $street_name ,
                'addition_to_address'=> $address_1.' '.$address_2,
                'address'      =>Input::get('address'),
                'locality'     =>Input::get('locality'),
                'country'      =>Input::get('country')
                
            );

            //Saves to campaign_donation_details
            $donation_model->saveDonationCurrentDetails($data);

            /*
            Address save 
            */    
                     
            $payment_detail_model = new PaymentDetailRepo();
            $payment_detail_model->create($payment_detail_data);

            $emailParams = array(
                'user_id' => $user_id,
                'campaignSlug' => $slug,
                'amount' => $amount,
            );
            $this->sendDonationEmail($emailParams, true);
            $this->sendDonationEmail($emailParams, false);


            Toastr::Success("Thanks for your donation", $title = null, $options = []);
            return Redirect::to("comment/$slug");
        }

        Toastr::Error($payment_response, $title = null, $options = []);
        //return Redirect::route('donate');
        return Redirect::to("donate/$slug");
    }
    
    public function getCampaignObject($slug)
    {
        $campaignDetail = [];
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $campaignDetails = $campaignRepo->findBySlug($slug);
        if (!empty($campaignDetails)) {
            $campaign = $campaignDetails[0];
        } else {
            $campaign = [];
        }
        return $campaign;
    }

    /**
     * for sending email after donation
     * @param array $emailParams
     * @param boolean $is_donar
     */
    public function sendDonationEmail($emailParams, $is_donar)
    {
        $campaign_slug = $emailParams['campaignSlug'];
        $campaignRepo = new CampaignRepo();
        $campaignDetails = $campaignRepo->findIdBySlug($campaign_slug);
        $campaign_name = (!empty($campaignDetails)) ? $campaignDetails->campaign_name : "";
        $user = new UserRepo();
        $donar_name = "";
        if ($is_donar) {
            $user_id = $emailParams['user_id'];
            $subject = "Thanks for your donation";
            $template = 'donar';
        } else {
            $user_id = (!empty($campaignDetails)) ? $campaignDetails->user_id : "";
            $subject = "Good News! Donation Received";
            $template = 'creater';
            $donar_id = $emailParams['user_id'];
            $user_response = $user->findById($donar_id);
            $donar_name = (!empty($user_response)) ? $user_response->name : "";
        }
        if (Helper::recieveUpdates($user_id) == true) {

            $user_response = $user->findById($user_id);
            $email = (!empty($user_response)) ? $user_response->email : "";

            $user_profile = new UserProfileRepo();
            $user_profile_response = $user_profile->findByUserId($user_id);
            $first_name = (!empty($user_profile_response)) ? $user_profile_response->first_name : "";
            $user_slug = (!empty($user_profile_response)) ? $user_profile_response->slug : "";

            $amount = $emailParams['amount'];

            Mail::send("payment::email.$template", array('campaign_name' => $campaign_name, 'first_name' => $first_name, 'campaign_slug' => $campaign_slug, 'amount' => $amount, 'donar_name' => $donar_name, 'user_slug' => $user_slug), function ($message) use ($email, $subject) {
                $message->to($email, 'Admin')->subject($subject);
            });
        }
    }

    public function setDonationAmount()
    {
        $donationAmount = Input::get('donation_amount');
        Session::forget('donationAmount');
        Session::set('donationAmount', $donationAmount);
    }

    public function postUpdateAddress()
    {
        $data = Input::all();
        $user_id = $data['user_id'];
        $user_profile = new UserProfileRepo();
        $info = $user_profile->updateCharityInfoByUserId($data, $user_id);

    }

    //Donation address will be saved in a new table
    public function postAddDonationAddress(){

       //saves to the new donation table campaign_donation_details

        $data = Input::all();
        $user_id = $data['user_id'];
        $user_profile = new UserProfileRepo();
        $info = $user_profile->updateCharityInfoByUserId($data, $user_id);
    }

}