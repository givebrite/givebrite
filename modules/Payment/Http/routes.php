<?php

Route::group(['namespace' => 'Modules\Payment\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'Modules\Auth\Http\Middleware\IsUserMiddleware','App\Http\Middleware\IsUnsecure']], function() {
    Route::post('donation/{slug}', [
        'uses' => 'StripeController@postDonation',
        'as' => 'donation'
    ]);


    Route::get('comment/{slug}', ['as' => 'comment', 'uses' => 'CommentController@getComment']);
    
    Route::post('comment/{slug}', [
        'uses' => 'CommentController@postComment',
        'as' => 'comment'
    ]);
});

Route::group(['namespace' => 'Modules\Payment\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsUserMiddleware','App\Http\Middleware\IsUnsecure']], function() {
    Route::get('donate/{slug}', ['as' => 'campaign.donation', 'uses' => 'StripeController@getDonate']);
    Route::post('donate/amount/save', ['as' => 'amount.save', 'uses' => 'StripeController@setDonationAmount']);

});

Route::group(['namespace' => 'Modules\Payment\Http\Controllers', 'middleware' => ['App\Http\Middleware\IsUnsecure']], function() {
    Route::post('donate/{slug}', [
        'uses' => 'StripeController@postDonate',
        'as' => 'donate'
    ]);
});

Route::group(['namespace' => 'Modules\Payment\Http\Controllers'], function(){
    Route::post('update_profile', ['uses' => 'StripeController@postUpdateAddress']);
    Route::post('add_donation_address', ['uses' => 'StripeController@postAddDonationAddress']);
});