<?php
$param = Helper::getQueryString();
if (!empty($param)) {
    $param = "?" . $param;
} else {
    $param = "";
}
$totalSumRecieved = 0;
$totalSumGiven = 0;
$totalSelfDonated = 0;
//    if ($userRole == 'User') {
if (!empty($allDonation) && count($allDonation) > 0) {
    foreach ($allDonation as $all) {

        if (!empty($all->donorId) && $all->donorId == $all->recieverId) {
            $totalSelfDonated = $totalSelfDonated + $all->donation_amount_monetory;
        } elseif (!empty($all->donorId) && $all->donorId == $user['id']) {
            $totalSumGiven = $totalSumGiven + $all->donation_amount_monetory;
        } else {
            $totalSumRecieved = $totalSumRecieved + $all->donation_amount_monetory;
        }
    }
}

?>
<div class='myDonationPage'>
    <div class="col-md-12 innerRightCol">
        <div class="clearfix">
            <div class="dash-heading">
                <div class="subHeading">Donations</div>
                <span class="csvBtn btn pull-right">
                    <a href="<?php echo e(URL::to('downloadExcel/csv/'.$campaignId)); ?>">Download as CSV</a>
                </span>
            </div>

            <!-- <h2 class="heading pull-left">
                Donations
                <span class="csvBtn btn">
                    <a href="<?php // echo e(URL::to('downloadExcel/csv/'.$campaignId)); ?>">Download as CSV</a>
                </span>
            </h2> -->
            <div class="pull-right donationToatl">
                <p>Total Recieved<span>{{Helper::showMoney($totalSumRecieved)}}</span></p>
                <p>Total Given<span>{{Helper::showMoney($totalSumGiven)}}</span></p>
                <p>Self Donated<span>{{Helper::showMoney($totalSelfDonated)}}</span></p>
            </div>
        </div>
        <div class="viewFilter clearfix">
            <h2 class="pull-left">Showing donations for </h2>
            {!! Form::open(array('route' => 'dashboard.campaign-donations','method'=>'get')) !!}
          
            <div class="col-sm-3">
                {!! Form::select('campaign_id', [''=>'All Campaigns'] + $userCampaigns ,(!empty($campaignId)) ? $campaignId : '' ,['class'=>'form-control','id'=>'campaign_select'])!!}
            </div>


            @if($user['role'] != "Charity")
               <div class="col-sm-3">
                    {!! Form::select('charity_id',[''=>'All Charites'] + $charityLinkedWithCampaign,(!empty($charityId)) ? $charityId : '' ,['class'=>'form-control','id'=>'charity_select'])!!}
                </div>
            @endif
            <div class="col-sm-2">
                <button class='btn btn-info'>Search</button>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="donationTable">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Campaign</th>
                    <th>Given/Recieved</th>
                </tr>
                </thead>
                <tbody>
                @if (!empty($donationList) && count($donationList)>0 )
                    @foreach ($donationList as $donation)
                        <tr>
                            <th scope='row'><a
                                        href="{{URL::route('donar.profile',$donation->userSlug)}}">{{ !empty($donation->name) ? $donation->name : "" }}</a>
                            </th>
                            <td>
                                {{ Carbon\Carbon::parse(!empty($donation->created_at) ? Helper::showDate($donation->created_at) : "")->diffForHumans() }}
                            </td>
                            <td>
                                {{ !empty($donation->donation_amount_monetory) ? Helper::showMoney($donation->donation_amount_monetory) : "" }}
                                @if(!empty($donation->is_giftaid))
                                    <span class="giftAid">
                                <br/>{{"+" }}
                                        {{Helper::showMoney(!empty($donation->donation_amount_monetory) ? ($donation->donation_amount_monetory*25)/100 : '',"sterling",2)}}
                                        {{"Gift Aid"}}
                            </span>
                                @endif
                            </td>

                        <!--                        <td>{{ !empty($donation->campaign_name) ? $donation->campaign_name : "" }}</td>-->
                            <td>{!! HTML::link('/campaign/'.$donation->slug,$donation->campaign_name) !!}</td>
                            <?php
                            if (!empty($donation->donorId) && $donation->donorId == $donation->recieverId) {
                                $type = "Self-Donate";
                            } elseif (!empty($donation->donorId) && $donation->donorId == $user['id']) {
                                $type = "Given";
                            } else {
                                $type = "Recieved";
                            }
                            ?>
                            <td>{{!empty($type) ? $type : "NA"}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr class="text-center text-bold">
                        <td colspan="4"> No Record Found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        <div class="pagination-wrap">
            <nav aria-label="Page navigation">
              <ul class="pagination inner-pagination">
                    {{--@include('site/partials/pagination', ['paginator' => $donationList])--}}
                     <?php echo $donationList->setPath("/dashboard/campaigns/donation")->appends($paginatorAppends); ?>
                </ul>
            </nav>
        </div>

    </div>
</div>