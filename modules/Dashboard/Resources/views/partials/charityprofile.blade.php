<div class="col-md-9 innerRightCol updatePassword profile createCampaignForm">
    <div class="formOuter form-sec row charityProfile">
        {!! Form::model($profile, ['method' => 'PUT', 'files' => true, 'route' => ['dashboard.profile-update', $profile->id],'id'=>'update-profile']) !!}
        <h2 class="heading col-sm-12">Profile Settings</h2>
        <div class="row">
            <div class="form-row col-sm-12">
                <?php $imagePath = isset($profile->charity_logo) && !empty($profile->charity_logo) && File::exists($base_path . $profile->charity_logo) ? asset($base_path . $profile->charity_logo) : asset('images/default-images/default_logo.png'); ?>
                <div id='viewProfileImage' class="uploadProPic col-sm-6" style='background-image:url("{{$imagePath}}")'>
                    @if(!empty($profile->charity_logo) && File::exists($base_path.$profile->charity_logo ))
                    <div class='btn btn-remove pull-right'>
                        <img id="remove-profile-image" src="{{asset('site/images/color-cross.png')}}">
                    </div>
                    @endif
                    <div class="btn fileUpload">
                        <label>Upload</label>
                        {!! Form::file('charity_logo', ['accept'=>"image/gif,image/jpeg,image/png",'class' => 'file-input file-input-block','id'=>'upload_pic']) !!}  
                    </div>
                    {!! $errors->first('charity_logo', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            <div class="text-danger" id="upload_pic_error">@if($errors->first('upload_pic')){{ $errors->first('upload_pic') }} @endif</div>
            <div class="form-row col-sm-6">
                {!! Form::label('first_name', 'First Name') !!}
                {!! Form::text('first_name', null, array('placeholder' => 'First Name')) !!}                                                            
                {!! $errors->first('first_name', '<div class="text-danger text-left">:message</div>') !!}
            </div>
            <div class="form-row col-sm-6">
                {!! Form::label('sur_name', 'Last Name',['class'=>'required']) !!}
                {!! Form::text('sur_name', null, array('placeholder' => 'Surname')) !!}                            
                {!! $errors->first('sur_name', '<div class="text-danger text-left">:message</div>') !!}
            </div>
            <div class="form-row col-sm-12">
                {!! Form::label('email', 'Email Address',['class'=>'required']) !!}
                {!! Form::text('email', null, array('placeholder' => 'Email', 'disabled' => 'disabled')) !!}   
                {!! $errors->first('email', '<div class="text-danger text-left">:message</div>') !!}
            </div>
            <div class="form-row col-sm-12 pwdSetting">
                <label>Password </label>
                <p>To change your password, <a href="{{URL::route('dashboard.password-edit',$profile->user_id)}}">click here</a></p>
            </div>
            <div class="form-row col-sm-12 pwdSetting">
                <label>Charity Registration Number <span class="astrikBlock">*</span></label>
                <p>{!! Form::text('registration_no',!empty($profile ->registration_no) ? $profile ->registration_no : "", ['id' => 'reg_number']) !!}</p>
            </div>
        </div>
        <div class="formRow">
            <div class="inputBlock">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <div class="secheading">Charity Name <span class="astrikBlock">*</span></div>
                        {!! Form::text('charity_name', null, array('placeholder' => 'Charity Name','class'=>'input-large')) !!}   
                        {!! $errors->first('charity_name', '<div class="text-danger text-left">:message</div>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="address">
            <div class="formRow">
                <div class="inputBlock">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="secheading">Postcode</div>
                            {!! Form::text('postcode', null, array('placeholder' => '','class'=>'input-large','id'=>'postcode')) !!}                                                   
                            {!! $errors->first('postcode', '<div class="text-danger text-left">:message</div>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="formRow">
                <div class="inputBlock">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="secheading">Address <span class="astrikBlock">*</span></div>
                            {!! Form::text('address', null, array('placeholder' => '','class'=>'input-large')) !!}                                                    
                            {!! $errors->first('address', '<div class="text-danger text-left">:message</div>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="show-Location" id="location-div">
                <div class="formRow">    
                    <div class="inputBlock">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="secheading">Country</div>
                                {!! Form::text('country', null, array('placeholder' => '','class'=>'input-large')) !!}                                                   
                                {!! $errors->first('country', '<div class="text-danger text-left">:message</div>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="formRow">
                    <div class="inputBlock">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="secheading">Locality</div>
                                {!! Form::text('locality', null, array('placeholder' => '','class'=>'input-large')) !!}                                                   
                                {!! $errors->first('locality', '<div class="text-danger text-left">:message</div>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <div class="formRow">
            <div class="inputBlock">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <div class="secheading">Contact Telephone <span class="astrikBlock">*</span></div>
                        {!! Form::text('contact_telephone', null, array('placeholder' => 'Phone','class'=>'input-large phone')) !!}   
                        {!! $errors->first('contact_telephone', '<div class="text-danger text-left">:message</div>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="formRow">
            <div class="inputBlock">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <div class="secheading">Website <span class="astrikBlock">*</span></div>
                        {!! Form::text('web', null, array('placeholder' => 'http://www.example.com','class'=>'input-large')) !!}   
                        {!! $errors->first('web', '<div class="text-danger text-left">:message</div>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="formRow">
            <div class="inputBlock">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="secheading">Description <span class="astrikBlock">*</span></div>
                        {!! Form::textarea('description', null, array('placeholder' => 'Description','class'=>'input-large','rows'=>4)) !!}                                                    
                        {!! $errors->first('description', '<div class="text-danger text-left">:message</div>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row col-sm-12">
            <button href="#" class="blue-btn-lg fullwidth">Save Settings</button>
        </div>
    </div>
    {!! Form::close() !!}   
    <div class="details">
        <input name="lat" type="hidden" value="">
        <input name="lng" type="hidden" value="">
        <input name="formatted_address" type="hidden" value="">
    </div>
</div>
