<div class="col-md-9 innerRightCol updatePassword profile">
    
    <div class="dash-heading">
        <div class="subHeading">Profile Settings</div>
    </div>
   
    {!! Form::model($profile, ['method' => 'PUT', 'files' => true, 'route' => ['dashboard.profile-update', $profile->id],'id'=>'update-profile']) !!}
    <div class="formOuter form-sec row">
        <div class="form-row col-sm-12">
            <?php $imagePath=isset($profile->profile_pic) && !empty($profile->profile_pic) ? ((substr( $profile->profile_pic, 0, 4 ) === "http") ? $profile->profile_pic : ( (File::exists($base_path.$profile->profile_pic )) ? asset($base_path.$profile->profile_pic) : asset('images/default-images/default_profile.png')))  :  asset('images/default-images/default_profile.png'); ?>
            <div class="uploadProPic" id="viewProfileImage" style='background-image:url("{{$imagePath}}")'>
                @if(!empty($profile->profile_pic) && File::exists($base_path.$profile->profile_pic ))
                    <div class='btn btn-remove pull-right'>
                        <img id="remove-profile-image" src="{{asset('site/images/color-cross.png')}}">
                    </div>
                @endif
                <div class="btn fileUpload">
                    <label>Upload</label>
                    {!! Form::file('profile_pic', ['accept'=>"image/gif,image/jpeg,image/png",'class' => 'file-input file-input-block','id'=>'upload_pic']) !!}  
                </div>
                {!! $errors->first('charity_logo', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        <div class="text-danger" id="upload_pic_error">@if($errors->first('profile_pic')) {{ $errors->first('profile_pic') }} @endif</div>
        <div class="form-row col-sm-6">
            <label>First Name</label>
            {!! Form::text('first_name', null, array('placeholder' => 'First Name')) !!}                                                            
            {!! $errors->first('first_name', '<div class="text-danger text-left">:message</div>') !!}</div>
        <div class="form-row col-sm-6">
            <label>Last Name</label>
                {!! Form::text('sur_name', null, array('placeholder' => 'Last Name')) !!}                            
            <div class="text-danger" id="sur_name_error">@if($errors->first('sur_name')) {{ $errors->first('sur_name') }} @endif</div>
        </div>
        <div class="form-row col-sm-12">
            <label>Email Address</label>
                {!! Form::text('email', null, array('placeholder' => 'Email', 'disabled' => 'disabled')) !!}   
                {!! $errors->first('email', '<div class="text-danger text-left">:message</div>') !!}
         </div>
        <div class="address">
            <!--edited-->
            <div class="address">
                <div class="form-row col-xs-7 col-md-9 col-sm-9 col-lg-9 clearfix">
                    <div class="inputBlock">
                        {!! Form::text('postcode', null, ['placeholder' => 'Enter Your Postcode','id'=>'postcode','postCodeStatus'=>'']) !!}
                        <!-- <span class="minor pull-right location-link-span">
                            <a class="location-link">Enter Address manually</a>
                         </span> -->

                         <p class="text-primary pull-left" id="postCodeMessage"></p>
                        
                    </div>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 col-lg-3 searchPostCode">
                                                   
                    <a href="javascript:void(0)" id="searchPostCode">Search</a>

                </div>
            </div>

            <!--select dropdown-->
             
             <div class="form-row col-sm-12 hidden" id="AddressSelectMain">
               
               <div class="register-select"> 
                
                    <select name="addressSelect" id="addressSelect" class="valid" >
                        
                        <option value="">Please Select your address</option>

                    </select>

               </div> 
                
               <div class="select__arrow"></div>
                
            </div>
             <!--select dropdown-->   

            <!--edited-->
            <div class="form-row col-sm-12">
                <div class="inputBlock">
                    {!! Form::text('address', null, ['placeholder' => 'Address','id'=>'address']) !!}
                    {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            <div class="show-Location" id="location-div">
                <div class="form-row col-sm-12">
                    <div class="inputBlock">
                        {!! Form::text('locality', null, ['placeholder' => 'Locality','id'=>'district']) !!}
                        {!! $errors->first('locality', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>

                <div class="form-row col-sm-12 clearfix">
                    <div class="inputBlock">
                        {!! Form::text('country', null, ['placeholder' => 'Country','id'=>'country']) !!}
                        {!! $errors->first('country', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>
                
            </div>
            <input name="lat" type="hidden">
            <input name="lng" type="hidden">
        </div>
        </div>
        <div class="form-row col-sm-12 pwdSetting">
            <label>Password </label>
            <p>To change your password, <a href="{{URL::route('dashboard.password-edit',$profile->user_id)}}">click here</a></p>
        </div>
        <div class="form-row col-sm-12">
            <button href="#" class="blue-btn-lg fullwidth">Save Settings</button>
        </div>
    </div>
{!! Form::hidden('id', $profile->id, null) !!}
{!! Form::close() !!}
<div class="details">
    <input name="lat" type="hidden" value="">
    <input name="lng" type="hidden" value="">
    <input name="formatted_address" type="hidden" value="">
</div>
</div>
<script type="text/javascript">
    
    $('#searchPostCode').click(function(e) {
          
          //console.log(this.value)
          var string = $('#postcode').val();

          $.ajax({
                url: "https://api.ideal-postcodes.co.uk/v1/postcodes/"+string+"?api_key=iddqd",
                dataType: "json",
                type: "get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    $('#postCode').attr('postcodestatus',200);
                    /*
                    $('#postCode').val(data.result[0]['postcode']);
                    $('#postCodeMessage').html('post code verified');
                    $('#country').val(data.result[0]['country']); 
                    $('#district').val(data.result[0]['district']);
                    */
                    $('#postcode').val(data.result[0]['postcode']);
                    $('#AddressSelectMain').removeClass('hidden');
                    $('#addressSelect').html('<option>Please Select your address </option>');
                        
                    $.each(data.result, function(index, element) {

                    $('#addressSelect').append('<option value="'+ element.line_1+' , '+element.district+' ,'+element.country+' ">'+ element.line_1+' , '+element.district+' ,'+element.country+' </option>');
                    
                    });
                    $("#addressSelect").show();  
                    

                    console.log(data)
                },
                error:function (xhr, ajaxOptions, thrownError){

                    if(xhr.status==404) {
                        $('#postcode').attr('postcodestatus',404);
                        $('#AddressSelectMain').addClass('hidden');
                        $('#postCodeMessage').html('Please enter a valid postcode');
                        $('#address').val('');
                        $('#country').val(''); 
                        $('#district').val('');
                        console.log('No post code found');
                    }
                }
            });

            //console.log(this.value);
      });

      $('#addressSelect').on('change', function() {

          var address = this.value;
          var arr = address.split(',');
          $('#address').val(arr[0]); $('#district').val(arr[1]); $('#country').val(arr[2]);  
          $(".registerHiddenFields").show();
          console.log(arr);

      });  

</script>
