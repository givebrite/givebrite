@extends('site.layouts.master')

@section('title', 'Dashboard')

@section('content')

@if(!(Helper::checkIsActivated()))
<div class="container custom-container">
    <div class="email-verification">
        @if(Session::get('user_session_data')['status'] != '0')
        @if(Session::get('user_session_data')['role'] == 'User')
        <h1>Email verification link has been sent</h1>
        <h2>Email not verified?</h2>
        <a href="{{ url('resend') }}">Resend account verification link ?</a>
        @else
        <h2>Thank you for registering your charity. One of our team members will verify your account and notify you as soon as it is activated.</h2>
        @endif
        @else
        <h2>Your Account has been blocked by admin</h2>
        @endif

    </div>
    <div class="blur">
        @include('site.partials.breadcrumb')
        <div class="row">
            @include('site.partials.dummy_dashboard')
        </div>
    </div>
</div>

@else
<div class="container {{ Helper::checkIsActivated() == true ? '' : 'custom-container' }}">
    @include('site.partials.breadcrumb')
    <div class="row">
        <div class="col-md-3 col-lg-3" id='left-sidebar'>
            @include('site.partials.dashboard_sidebar')
        </div>
        <div class="col-md-9 col-lg-9 innerRightCol">
<!--            <h2 class="heading">
                @yield('dashboard-title')
            </h2>-->
            @include('site.partials.flashes')

            @yield('content-dashboard')

        </div>
    </div>
</div>
@endif
@stop

@section('script')
<script src='{{ asset('modules/dashboard/js/common.js').'?num='.mt_rand(1000000, 9999999) }}'></script>
@stop