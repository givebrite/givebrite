@extends('site.layouts.master')

@section('title', 'Profile')

@section('content')
<?php $base_path = $user['role'] == 'Charity' ? Config::get('config.thumb_charity_logo_upload_path') : Config::get('config.thumb_profile_image_upload_path');?>
<div class="container">
    @include('site.partials.breadcrumb')
    <div class="row">
        <div class="col-md-3">
            @include('site.partials.dashboard_sidebar')
        </div>
        <div class="col-md-9 innerRightCol">
            <input type="hidden" id="romove-profile-url" value="{{URL::route('profile-image.remove')}}">
            @include('site.partials.flashes')
            @if(!empty($user['role']=='Charity'))
            @include('dashboard::partials.charityprofile')
            @else
            @include('dashboard::partials.userprofile')
            @endif

        </div>
    </div>
</div>
@stop

@section('script')
@if(!empty($user['role']=='Charity'))
{!! JsValidator::formRequest('Modules\Auth\Http\Requests\RegisterCharityCreateRequest','#update-profile') !!}
@else
<?php
$validator = JsValidator::make(
	[
		'first_name' => 'required|min:2|max:55',
		'sur_name' => 'max:55',
		'profile_pic' => 'mimes:jpeg,png,gif,jpg',
		'country' => 'required',
		'address' => 'required',
		'locality' => 'required',
		'postcode' => 'required',
	], [
		'first_name.required' => 'First name is required',
		'first_name.min' => 'First name should contain at least 2 character',
		'first_name.max' => 'First name must not exceed 55 character',
		'sur_name.max' => 'Sur name must not exceed 55 character',
		'profile_pic.mimes' => 'Image format is not valid',
		'country.required' => 'Country is required',
		'address.required' => 'Address is required',
		'locality.required' => 'Locality is required',
		'postcode.required' => 'Postcode is required',
	]
);
?>
{!! $validator->selector('#update-profile') !!}
@endif
<script>

    $(document).ready(function () {
        $(".dash_head").click(function () {
            $(".innerListbox").toggle(500);
        });
    });

    $("form#update-profile").submit(function(){
    $(".text-danger").text("");
    });</script>
<script type='text/javascript' src="{{ asset('js/jquery.maskedinput.js') }}"></script>
<script src='{{ asset('modules/dashboard/js/browse-pic.js') }}'></script>
<script src='{{ asset('modules/campaigns/js/common.js') }}'></script>
<script type="text/javascript">
    var site_url = '<?php echo url(); ?>';
    if(window.location.href.replace(site_url + '/', '') == 'dashboard/profile' || window.location.href.replace(site_url + '/', '') == 'dashboard/profile/'){
        $(".profile-sec").addClass('active');
    }else{
        $(".profile-sec").removeClass('active');
    }
</script>
@stop