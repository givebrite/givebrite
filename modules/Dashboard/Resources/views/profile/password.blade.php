@extends('site.layouts.master')

@section('title', 'Profile')

@section('content')
<div class="container">
    @include('site.partials.breadcrumb')
    <div class="row">
        <div class="col-md-3">
            @include('site.partials.dashboard_sidebar')
        </div>
        <div class="col-md-9 innerRightCol">
            @include('site.partials.flashes')
            <div class="row col-md-12">
                <div class="col-md-8">
                    <h2 class="heading">Change Password</h2>      
                    <div class="form-sec">
                        {!! Form::model($profile, ['method' => 'PUT', 'route' => ['dashboard.password-update', $profile->id],'id'=>'passowrd-update']) !!}
                        <div class="form-row">
                            <div class="inputBlock">
                                {!! Form::password('current_password', array('placeholder' => 'Current Password')) !!}   
                                {!! $errors->first('current_password', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="inputBlock">
                                {!! Form::password('password',  array('placeholder' => 'New Password')) !!}                                                    
                                 {!! $errors->first('password', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>                     
                        <div class="form-row">
                            <div class="inputBlock">
                                {!! Form::password('password_confirmation', array('placeholder' => 'Confirm Password')) !!}                                                   
                                {!! $errors->first('password_confirmation', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>                    
                        <div class="buttonBlock form-row">
                            <input type="submit" value="Update Password" class="fullwidth blue-btn-lg text-center">
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>            
        </div>
    </div>
</div>
@stop

@section('script')
<script>
    $("form#passowrd-update").submit(function(){
        $(".text-danger").text("");
    });
</script>
<script src='{{ asset('modules/campaigns/js/common.js') }}'></script>
 <?php
  $validator = JsValidator::make([
  'current_password' => 'required',
  'password' => 'required|min:6|confirmed',
   'password_confirmation' => 'required|min:6',
  ],[
      'current_password.required'=>'Current password is required',
      'password.required'=>'',
      'password.min'=>Lang::get('auth::validations.user-auth.password-min'),
      'password.confirmed'=>Lang::get('auth::validations.user-auth.password-confirm'),
      'password_confirmation.required'=>'Password confirmation is required',
      'password_confirmation.min'=>Lang::get('auth::validations.user-auth.password-min')
  ]
          );
  ?>
{!! $validator !!}
{!! $validator->selector('#passowrd-update') !!}
@stop