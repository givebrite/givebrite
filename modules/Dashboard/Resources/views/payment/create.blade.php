@extends('dashboard::layouts.master')

@section('title', 'Donation')
@section('dashboard-title', 'Stripe Settings')
@section('style')
<link rel="stylesheet" href="{{ asset('modules/campaigns/css/bootstrap-select.css') }}">
@stop
@section('content-dashboard')
<div class="col-md-12">
   <div class="stripe-account-div">
        @if(!empty($paymentOptions) && $paymentOptions->status == 'yes')
            @if(!empty($paymentOptions->user_email))
                <div>Your Stripe login email : <span>{{ !empty($paymentOptions->user_email) ? $paymentOptions->user_email : ""}}</span></div>
            @endif
            <div class="disconnect clearfix">
                <a href="{{ URL::route('stripe.disable',$userId) }}" class="btn btn-info">Disconnect</a>
            </div>
        @else
        <div class="connect clearfix">
            <div>Connect Stripe to start taking donations</div>
            <a href="{{ $stripe_connect_link }}" class="btn btn-info">Connect</a
        </div>
        @endif
    </div>
</div>
@stop
@section('script')
<script>
    $( window ).load(function() {
    // Run code
    @if(empty($paymentOptions))
        setTimeout(function(){ 
            window.location.href = "{{ $stripe_connect_link }}";
        }, 5000);
    @endif
        
    });
</script>
@stop