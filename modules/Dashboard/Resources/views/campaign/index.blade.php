@extends('dashboard::layouts.master')
@section('title', 'My Campaigns')
@section('content-dashboard')
    <?php $campaign_base_path = Config::get('config.thumb_campaign_upload_path'); ?>
    <div class="col-md-12 innerRightCol myCampaignPage">
        
        <div class="dash-heading">
            <div class="subHeading">Your Campaigns</div>
        </div>
        <div class="campaignList">
            <div class="row">
                <!--    <ul class="row dashIndex listEplore">-->
                @if (!empty($campaignsList) && count($campaignsList)>0 )
                    @foreach ($campaignsList as $campaign)
                        <?php $updateRoute = !empty($campaign->stage) && $campaign->stage < 5 ? URL::route('campaigns.edit', $campaign->id) : URL::route('campaign.show', $campaign->slug);
                        ?>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="campain-list">
                                <a href="{{URL::route('campaign.show', $campaign->slug)}}">
                                    <div class="campain-img-wrap">
                                        @if(file_exists(public_path('/images/thumb-campaigns/'.$campaign->campaign_image)))
                                            <img src="{{ asset('/images/thumb-campaigns/'.$campaign->campaign_image) }}"
                                                 alt="img"/>
                                        @else
                                            <img src="{{ asset('/images/default-images/default-campaign-new.jpg') }}"
                                                 alt="img"/>
                                        @endif
                                        <h3>{{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 55, $end = '...') :''}}</h3>
                                        <div class="campaign-title-bg"></div>
                                        <div class="campaign-progress"
                                             style="width:{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}%;"></div>
                                    </div>
                                    <div class="conpain-des">
                                        <div class="price-section"><span
                                                    class="price-big">£{{Helper::amountRaised($campaign->id)}}</span><span
                                                    class="price-small">of £{{$campaign->goal_amount_monetary}}
                                                raised</span></div>
                                        <p class="conpain-text-detail">{{ strip_tags($campaign->campaign_description) }}</p>
                                    </div>
                                </a>
                                <div class="campain-bottom">
                                    <div>
                            <span class="edit-button">
                                <a href="{{Url::to('campaigns/'. $campaign->id .'/edit')}}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </span>
                                        @if($campaign->status)
                                            <span class="view-button">
                                <a href="{{Url::to('campaign/pause/'. $campaign->id)}}">
                                    <i class="fa fa-pause"></i>
                                </a>
                            </span>
                                        @else
                                            <span class="view-button">
                                <a href="{{Url::to('campaign/resume/'. $campaign->id)}}">
                                    <i class="fa fa-play"></i>
                                </a>
                            </span>
                                        @endif
                                        <span class="delete-button">
                                <a href="{{Url::to('campaign/delete/'. $campaign->id)}}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>

                                        <span class="donation-number">
                                {{$campaign->NumberOfDonor}} <span>donors</span><br>
                                <a href="{{Url::to('dashboard/campaigns/donation/'. $campaign->id)}}">View Donations</a>
                            </span>
                                    </div>
                                </div>
                                <div class="post-update-link">
                                    <a href="#" data-toggle="modal" data-target="#myModal"
                                       onclick="form_function({{$campaign->id}})"><i class="fa fa-pencil-square-o"></i>Post
                                        an update</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-center"> No Campaign Found.</div>
            @endif
            <!--</ul>-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="upd_title">Update Box</h4>
                </div>
                {!! Form::open(['route' => ['campaign-updates.store'], 'id'=>'update-form']) !!}
                <div class="modal-body">
                    <div class='container'>
                        <div class="">
                            {!! Form::textarea('updates', null, ['placeholder'=>'New Update ..','class' => '']) !!}
                            {!! $errors->first('updates', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success">Save</button>
                    <button class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    <script>

        $(function () {
            $('.percent').percentageLoader({
                valElement: 'p',
                strokeWidth: 5,
                bgColor: '#d9d9d9',
                ringColor: '#37c2df',
                textColor: '#9f9f9f',
                fontSize: '15px',
                fontWeight: 'bold'
            });
        });</script>
    <script type="text/javascript">
        function form_function(id) {
            $('#update-form').attr('action', site_url + '/campaigns/' + id + '/save-updates');
        }
        var site_url = '<?php echo url(); ?>';
        if (window.location.href.replace(site_url + '/', '') == 'dashboard/campaigns' || window.location.href.replace(site_url + '/', '') == 'dashboard/campaigns/') {
            $(".camp-sec").addClass('active');
        } else {
            $(".camp-sec").removeClass('active');
        }


        $(document).ready(function () {
            $(".dash_head").click(function () {
                $(".innerListbox").toggle(500);
            });
        });

    </script>
@stop