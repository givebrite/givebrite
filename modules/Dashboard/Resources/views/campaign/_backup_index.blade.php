@extends('dashboard::layouts.master')
@section('title', 'My Campaigns')
@section('content-dashboard')
<?php $campaign_base_path = Config::get('config.thumb_campaign_upload_path'); ?>
<div class="col-md-12 innerRightCol myCampaignPage">
    <h2 class="heading">Campaigns</h2>
    <div class="subHeading">Your Campaigns</div>
    <div class="campaignList">
        <ul class="clearfix listEplore">
            @if ( count($campaignsList)>0 )       
            @foreach ($campaignsList as $campaign)
            <?php
            $leftAmount = $campaign->goal_amount_monetary - $campaign->totalAmountRaised;
            if (!empty($campaign->stage) && $campaign->stage == 5) {
                $updateRoute = URL::route('campaign.show', $campaign->slug);
            } else {
                if (!empty($campaign->stage) && $campaign->stage >= 3) {
                    // Redirect To Payment Screen
                    $updateRoute = URL::route('launch.create', $campaign->id);
                } elseif (!empty($campaign->stage) && $campaign->stage >= 2) {
                    // Redirect To Design Screen
                    $updateRoute = URL::route('design.create', $campaign->id);
                }
            }
            ?>
            <li>
                <div class="row">
                    <div class="col-sm-4">
                        <a href="{{$updateRoute}}"><div class="imgContainer" style="background: rgba(0, 0, 0, 0) url('{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($campaign_base_path.$campaign->campaign_image ) ?  asset($campaign_base_path.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}') no-repeat scroll center center / cover;"></div></a>
                        <div class="listDetails clearfix">
                            <div class="detialsLeft">
                                <div class="price">    {{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}</div>
                                <div class="type">
                                    <a href="{{$updateRoute}}">   {{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 15, $end = '...') :''}}  </a>                                      
                                </div>
                                <div class="location">
                                    {{Helper::showAddress($campaign->cityName,$campaign->locationName,15)}}
                                </div>
                            </div>
                            <div class="progressOuter inherit" style="width:50px;height:50px;">
                                <div class="percent" style="width:50px;height:50px;">
                                    <p style="display:none;">{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 myDonationDetails">
                        <div class="item border-btm clearfix">
                            <p class="pull-left donationAmount">{{Helper::showMoney($campaign->totalAmountRaised,$campaign->campaign_currency)}} raised so far
                                @if($leftAmount>0)   
                                <span>{{Helper::showMoney($leftAmount,$campaign->campaign_currency)." remaining"}}</span>
                                @endif</p>
                            <p class="pull-right grayButton"><a href="{{URL::route('dashboard.campaign-donations',['campaign_id'=>$campaign->id])}}">View Donations</a></p>
                        </div>
                        @if(!empty($campaign->userWeb))
                        <div class="item border-btm  clearfix">
                            <p class="pull-left visitLink">{{!empty($campaign->userWeb) ? $campaign->userWeb : "" }}</p>
                            <p class="pull-right grayButton"><a href="{{URL::route('dashboard.profile')}}">Update</a></p>
                        </div>
                        @endif
                        <div class="item clearfix">
                            <p class="pull-left donationAmount">Created {{ !empty($campaign->created_at) ? Helper::showDate($campaign->created_at) : "" }}</span></p>
                        </div>
                        <div class="item clearfix btnBox">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if($campaign->campaign_duration == 'ongoing' || Helper::isExpired($campaign->end_date) == false)
                            @if ($campaign->status == 0)
                            <a href="{{$updateRoute}}" class="btn btn-blue hidden" id="update_{{$campaign->id}}">{{!empty($campaign->stage) && $campaign->stage < 5  ? "Continue To Launch" : "Update"}}</a>
                            <a href="{{ URL::route('dashboard.update-campaign-status',$campaign->id) }}" class="btn btn-info changeCampaignStatus">Start Campaign</a>
                            @else
                            <a href="{{$updateRoute}}" class="btn btn-blue" id="update_{{$campaign->id}}">{{!empty($campaign->stage) && $campaign->stage < 5  ? "Continue To Launch" : "Update"}}</a>
                            <a href="{{ URL::route('dashboard.update-campaign-status',$campaign->id) }}" class="btn btn-danger changeCampaignStatus">Stop Campaign</a>
                            @endif
                            @else
                            <a class="btn btn-info disabled">Expired<a>
                                    @endif
                                    <!--  <a href="" class="delete">Delete</a>-->
                                    </div>
                                    </div>
                                    </div>

                                    </li>
                                    @endforeach
                                    @else
                                    <li><div class="text-center"> No Campaign Found.</div></li>
                                    @endif
                                    </ul>
                                    <div class="paginationOuter">
                                        <nav>
                                            @include('site/partials/pagination', ['paginator' => $campaignsList])
                                        </nav>
                                    </div>
                                    </div>
                                    @if(!empty($user['role']) && $user['role']="Charity" && !empty($charityFundraisers))
                                    <div class="subHeading">Fundraisers for your Charity</div>
                                    <div class="campaignList">
                                        <ul class="clearfix listEplore">
                                            <li>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <a href="{{URL::route('campaign.show', $charityFundraisers[0]->slug)}}"><div class="imgContainer" style="background: rgba(0, 0, 0, 0) url('{{isset($charityFundraisers[0]->campaign_image) && !empty($charityFundraisers[0]->campaign_image) && File::exists($campaign_base_path.$charityFundraisers[0]->campaign_image ) ?  asset($campaign_base_path.$charityFundraisers[0]->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}') no-repeat scroll center center / cover;"></div></a>
                                                        <div class="listDetails clearfix">
                                                            <div class="detialsLeft">
                                                                <div class="price">{{Helper::showMoney($charityFundraisers[0]->goal_amount_monetary,$charityFundraisers[0]->campaign_currency)}}</div>
                                                                <div class="type"><a href="{{URL::route('campaign.show',$charityFundraisers[0]->slug)}}">   {{ $charityFundraisers[0]->campaign_name ? str_limit(ucfirst($charityFundraisers[0]->campaign_name) , $limit = 15, $end = '...') : ''}}  </a></div>
                                                                <div class="location">
                                                                    {{Helper::showAddress($charityFundraisers[0]->cityName,$charityFundraisers[0]->locationName,15)}}
                                                                </div>
                                                            </div>

                                                            <div class="progressOuter inherit" style="width:50px;height:50px;">
                                                                <div class="percent" style="width:50px;height:50px;">
                                                                    <p style="display:none;">{{Helper::showGoalPercentage($charityFundraisers[0]->id, $charityFundraisers[0]->goal_amount_monetary )}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-8 myDonationDetails">
                                                        <div class="item border-btm clearfix">
                                                            <p class="pull-left donationAmount">{{Helper::showMoney($charityFundraisers[0]->totalAmountRaised,$charityFundraisers[0]->campaign_currency)." raised so far"}} 
                                                                <?php $fundLeftAmount = $charityFundraisers[0]->goal_amount_monetary - $charityFundraisers[0]->totalAmountRaised; ?>
                                                                @if($fundLeftAmount>0)   
                                                                <span>{{Helper::showMoney($fundLeftAmount,$charityFundraisers[0]->campaign_currency)." remaining"}}</span>
                                                                @endif
                                                            </p>
                                                            <p class="pull-right grayButton"><a href="{{URL::route('dashboard.campaign-donations',['campaign_id'=>$charityFundraisers[0]->id])}}">View Donations</a></p>
                                                        </div>
                                                        <div class="item clearfix">
                                                            <p class="pull-left donationAmount">Created {{ !empty($charityFundraisers[0]->created_at) ? Helper::showDate($charityFundraisers[0]->created_at) : "" }}</span></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </li>

                                        </ul>
                                    </div>
                                    @endif    
                                    </div>
                                    @stop
                                    @section('script')
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
                                    <script src="{{ asset('js/jQuery.circleProgressBar.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
                                    <script>

$(function () {
    $('.percent').percentageLoader({
        valElement: 'p',
        strokeWidth: 5,
        bgColor: '#d9d9d9',
        ringColor: '#37c2df',
        textColor: '#9f9f9f',
        fontSize: '15px',
        fontWeight: 'bold'
    });

});
                                    </script>
                                    @stop