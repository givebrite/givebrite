@extends('dashboard::layouts.master')

@section('title', 'Donation')

@section('dashboard-title', '')
@section('style')
    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/bootstrap-select.css') }}">
    <link href="{{ asset('modules/campaigns/css/select2.css') }}" rel="stylesheet" />
    <style type="text/css">
        
        .select2-container{

            width: 180px !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 4px;
            height: 35px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 35px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 26px;
            position: absolute;
            top: 5px;
            right: 4px;
            width: 20px;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 4px;
            height: 35px;
         }

    </style>
@stop
@section('content-dashboard')
    @if($userRole == 'User')
        @include('dashboard::partials.userdonation')
    @else
        @include('dashboard::partials.charitydonation')
    @endif
@stop
@section('script')
    <script src="{!! asset('modules/campaigns/js/bootstrap-select.js') !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>$('.selectpicker').selectpicker();</script>
    <script type="text/javascript">
        var site_url = '<?php echo url(); ?>';
        if (window.location.href.replace(site_url + '/', '') == 'dashboard/campaigns/donation' || window.location.href.replace(site_url + '/', '') == 'dashboard/campaigns/donation/') {
            $(".dona-sec").addClass('active');
        } else {
            $(".dona-sec").removeClass('active');
        }

         $(document).ready(function () {
            
            $('#charity_select').select2();
            $('#campaign_select').select2();

            $(".dash_head").click(function () {
                $(".innerListbox").toggle(500);
            });
        });


    </script>
@stop