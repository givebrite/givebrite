@extends('dashboard::layouts.master')

@section('title', 'Comments')

@section('dashboard-title', '')
@section('style')
<link rel="stylesheet" href="{{ asset('modules/campaigns/css/bootstrap-select.css') }}">
@stop
@section('content-dashboard')
<?php
$param = Helper::getQueryString();
if (!empty($param)) {
	$param = "?" . $param;
} else {
	$param = "";
}
$totalSum = 0;
if (!empty($commentList) && count($commentList) > 0) {
	foreach ($commentList as $comment) {
		$totalSum = $totalSum+!empty($comment->donation_amount_monetory) ? $comment->donation_amount_monetory : 0;
	}
}
?>
<div class='myDonationPage'>
    <div class="col-md-12 innerRightCol">
        
        <div class="dash-heading">
                <div class="subHeading">Comments</div>
        </div>

        <!-- <div class="clearfix">
            <h2 class="heading pull-left">Comments</span></h2>
        </div> -->

        <div class="viewFilter clearfix">
            <h2 class="pull-left ">Showing Comments for </h2>
            {!! Form::open(array('route' => 'dashboard.comments-list','method'=>'get')) !!}
            <div class="col-sm-4">
                {!! Form::select('campaign_id', [''=>'All Campaigns'] + $userCampaigns ,(!empty($campaignId)) ? $campaignId : '' ,['class'=>'form-control selectpicker'])!!}
            </div>
            <div class="col-sm-2">
                <button class='btn btn-info'>Search</button>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="commentList" >
            <div class="innerShadowBox" id="info">
                @if(!empty($commentList) && count($commentList)>0)
                @foreach($commentList as $comment)
                <div class='comment'>
                    <div class="postPublish">Published by {{ !empty($comment->name) ? ucfirst($comment->name) : "" }}  - {{ !empty($comment->created_at) ? Helper::showDate($comment->created_at) : ''  }}</div>
                    <div class="postdescription readmore-content text-fade"><h4>{!! isset($comment->content)? $comment->content :'' !!}</h4></div>
                </div>
                @endforeach
                @else
                <div class='comment'>
                    <div class="postPublish"></div>
                    <div class="postdescription">No Comment Found </div>
                </div>
                @endif
            </div>
        </div>
        <div class="paginationOuter">
            <nav>
                @include('site/partials/pagination', ['paginator' => $commentList])
            </nav>
        </div>
    </div>
</div>
@stop
@section('script')
<script type='text/javascript' src="{{ asset('js/readmore.js') }}"></script>
<script src="{!! asset('modules/campaigns/js/bootstrap-select.js') !!}"></script>
<script>$('.selectpicker').selectpicker();</script>
<script>$('.readmore-content').readmore({speed: 500});</script>
<script type="text/javascript">
    var site_url = '<?php echo url(); ?>';
    if(window.location.href.replace(site_url + '/', '') == 'dashboard/comments' || window.location.href.replace(site_url + '/', '') == 'dashboard/comments/'){
        $(".comm-sec").addClass('active');
    }else{
        $(".comm-sec").removeClass('active');
    }

     $(document).ready(function () {
            $(".dash_head").click(function () {
                $(".innerListbox").toggle(500);
            });
        });


</script>
@stop


