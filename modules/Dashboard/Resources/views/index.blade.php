@extends('dashboard::layouts.master')

@section('title', 'Dashboard')

@section('content-dashboard')
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/reset.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/layout.css')}}"/>
    <?php $campaign_base_path = Config::get('config.thumb_campaign_upload_path');?>
    <div class="col-sm-12">
        <div class="dashboard-title">
            @if(!empty($totalAmount->totalAmountRaised))
                <h1>£{{$totalAmount->totalAmountRaised}}</h1>
            @else
                <h1>£0</h1>
            @endif
            <span class="total-donation">total donations<br/>in the last 30 days</span>
        </div>
        <br/>
        <!-- Graph of the dashboard -->
        <div id="chart-container" class="chart-container"></div>
    </div>

    <div class="col-sm-12">
        <div class="dash-heading">
            <div class="subHeading">{{ trans('dashboard::dashboard.current_campaign') }}</div>
            <a href="{{url('/dashboard/campaigns')}}" class="view-all-cat" style="">View All</a>
        </div>
        <div class="campaignList">
            <div class="row">
                <!--    <ul class="row dashIndex listEplore">-->
                @if (!empty($campaignsList) && count($campaignsList)>0 )
                    @foreach ($campaignsList as $campaign)
                        <?php $updateRoute = !empty($campaign->stage) && $campaign->stage < 5 ? URL::route('campaigns.edit', $campaign->id) : URL::route('campaign.show', $campaign->slug);
                        ?>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="campain-list">
                                <a href="{{URL::route('campaign.show', $campaign->slug)}}">
                                    <div class="campain-img-wrap">
                                        @if(file_exists(public_path('/images/thumb-campaigns/'.$campaign->campaign_image)))
                                            <img src="{{ asset('/images/thumb-campaigns/'.$campaign->campaign_image) }}"
                                                 alt="img"/>
                                        @else
                                            <img src="{{ asset('/images/default-images/default-campaign-new.jpg') }}"
                                                 alt="img"/>
                                        @endif
                                        <h3>{{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 55, $end = '...') :''}}</h3>
                                        <div class="campaign-title-bg"></div>
                                        <div class="campaign-progress"
                                             style="width:{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}%;"></div>
                                    </div>
                                    <div class="conpain-des">
                                        <div class="price-section"><span
                                                    class="price-big">£{{Helper::amountRaised($campaign->id)}}</span><span
                                                    class="price-small">of £{{$campaign->goal_amount_monetary}}
                                                raised</span></div>
                                        <p class="conpain-text-detail">{{ strip_tags($campaign->campaign_description) }}</p>
                                    </div>
                                </a>
                                <div class="campain-bottom">
                                    <div>
                            <span class="edit-button">
                                <a href="{{Url::to('campaigns/'. $campaign->id .'/edit')}}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </span>
                                        @if($campaign->status)
                                            <span class="view-button">
                                <a href="{{Url::to('campaign/pause/'. $campaign->id)}}">
                                    <i class="fa fa-pause"></i>
                                </a>
                            </span>
                                        @else
                                            <span class="view-button">
                                <a href="{{Url::to('campaign/resume/'. $campaign->id)}}">
                                    <i class="fa fa-play"></i>
                                </a>
                            </span>
                                        @endif
                                        <span class="delete-button">
                                <a href="{{Url::to('campaign/delete/'. $campaign->id)}}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>

                                        <span class="donation-number">
                                {{$campaign->NumberOfDonor}} <span>donors</span><br>
                                <a href="{{Url::to('dashboard/campaigns/donation/'. $campaign->id)}}">View Donations</a>
                            </span>
                                    </div>
                                </div>
                                <div class="post-update-link">
                                    <a href="#" data-toggle="modal" data-target="#myModal"
                                       onclick="form_function({{$campaign->id}})"><i class="fa fa-pencil-square-o"></i>Post
                                        an update</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-center"> No Campaign Found.</div>
            @endif
            <!--</ul>-->
            </div>
        </div>
    </div>
    <!-- <div class="subHeading col-sm-12">{{ trans('dashboard::dashboard.my_account') }}</div> -->
    <!-- <div class="row">
    <div class="col-sm-3 text-center">
        <div class="imgBlock">
            <a href="{{ URL::route('dashboard.profile') }}"><i class="fa fa-cog" aria-hidden="true"></i></a>
        </div>
        <div class="iconText">{{ trans('dashboard::dashboard.profile') }}</div>
    </div>
        <div class="col-sm-3 text-center">
            <div class="imgBlock">
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>
            <div class="iconText">{{ trans('dashboard::dashboard.facebook') }}</div>
        </div>
        <div class="col-sm-3 text-center">
            <div class="imgBlock">
                <a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
            <div class="iconText">{{ trans('dashboard::dashboard.twitter') }}</div>
        </div>
</div> -->
    <!-- <div class="subHeading">{{ trans('dashboard::dashboard.campaigns') }}</div>
<div class="row">
    <div class="col-sm-3 text-center">
        <div class="imgBlock">
            <a href="{{ URL::route('dashboard.campaign-list') }}"> <i class="fa fa-bullseye" aria-hidden="true"></i></a>
        </div>
        <div class="iconText">{{ trans('dashboard::dashboard.campaigns') }}</div>
    </div>
    <div class="col-sm-3 text-center">
        <div class="imgBlock">
            <a href="{{ URL::route('dashboard.campaign-donations') }}"><i class="fa fa-gratipay" aria-hidden="true"></i></a>
        </div>
        <div class="iconText">{{ trans('dashboard::dashboard.donations') }}</div>
    </div>
    <div class="col-sm-3 text-center">
        <div class="imgBlock">
            <a href="{{ URL::route('dashboard.comments-list') }}"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>
        </div>
        <div class="iconText">{{ trans('dashboard::dashboard.comments') }}</div>
    </div>
</div> -->

    <!-- Trigger the modal with a button -->
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="upd_title">Update Box</h4>
                </div>
                {!! Form::open(['route' => ['campaign-updates.store'], 'id'=>'update-form']) !!}
                <div class="modal-body">
                    <div class='container'>
                        <div class="">
                            {!! Form::textarea('updates', null, ['placeholder'=>'New Update ..','class' => '']) !!}
                            {!! $errors->first('updates', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success">Save</button>
                    <button class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop
@section('script')
    <script>
        var site_url = '<?php echo url(); ?>';</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
    <script>
        $(function () {
            $('.percent').percentageLoader({
                valElement: 'p',
                strokeWidth: 5,
                bgColor: '#d9d9d9',
                ringColor: '#37c2df',
                textColor: '#9f9f9f',
                fontSize: '15px',
                fontWeight: 'bold',
            });
        });
        function form_function(id) {
            $('#update-form').attr('action', site_url + '/campaigns/' + id + '/save-updates');
        }
    </script>
    <script src="{{asset('site/js/fusioncharts.js')}}"></script>
    <script src="{{asset('site/js/fusioncharts.charts.js') }} "></script>
    <!--<script  src="{{asset('site/js/fusioncharts.maps.js') }}"></script>-->
    <script src="{{asset('site/js/fusioncharts.theme.management-dashboard.js') }}"></script>
    <script src="{{asset('site/js/dashboard.js') }}"></script>
    <script type="text/javascript">
        if (window.location.href.replace(site_url + '/', '') == 'dashboard' || window.location.href.replace(site_url + '/', '') == 'dashboard/') {
            $(".account-sec").addClass('active');
        } else {
            $(".account-sec").removeClass('active');
        }
        $(document).ready(function () {
            $(".dash_head").click(function () {
                $(".innerListbox").toggle(500);
            });
        });
    </script>
    <script>
        FusionCharts.ready(function () {
            var revenueChart = new FusionCharts({
                type: 'msarea',
                renderAt: 'chart-container',
                width: '100%',
                height: '300',
                dataFormat: 'json',
                dataSource: {
                    "chart": {
                        "labeldisplay" : "none",
                        "numberPrefix": "£",
                        //Cosmetics
                        "paletteColors": "#ECECEA",
                        "baseFontColor": "#888888",
                        "baseFont": "Helvetica Neue,Arial",
                        "captionFontSize": "14",
                        "subcaptionFontSize": "14",
                        "subcaptionFontBold": "0",
                        "showBorder": "0",
                        "bgColor": "#ffffff",
                        "showShadow": "0",
                        "canvasBgColor": "#ffffff",
                        "canvasBorderAlpha": "0",
                        "divlineAlpha": "100",
                        "divlineColor": "#C6C6C6",
                        "divlineThickness": "1",
                        "divLineIsDashed": "1",
                        "divLineDashLen": "1",
                        "divLineGapLen": "1",
                        "usePlotGradientColor": "0",
                        "showplotborder": "0",
                        "showXAxisLine": "1",
                        "xAxisLineThickness": "2",
                        "xAxisLineColor": "#979797",
                        "showAlternateHGridColor": "0",
                        "showAlternateVGridColor": "0",
                        "legendBgAlpha": "0",
                        "legendBorderAlpha": "0",
                        "legendShadow": "0",
                        "legendItemFontSize": "10",
                        "legendItemFontColor": "#666666",
                        "anchorBgColor": "#48c253",
                        "theme": 'management',
                    },
                    "categories": [{
                        "category": <?php echo json_encode($dayArray); ?>
                    }
                    ],
                    "dataset": [
                        {
                            "showValues": "0",
                            "data": <?php echo json_encode($amount_per_day); ?>
                        },

                        {


                            "renderAs": "line",
                            "showValues": "0",
                            "data": <?php echo json_encode($amount_per_day); ?>
                        }
                    ]
                }
            });

            revenueChart.render();
        });
    </script>
@stop