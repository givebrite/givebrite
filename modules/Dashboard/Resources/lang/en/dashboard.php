<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [    
    'current_campaign' =>  'Current Campaigns',
    'profile' =>  'Profile',
    'facebook' =>  'Facebook',
    'twitter' =>  'Twitter',
    'campaigns' =>  'My Campaigns',
    'donations' =>  'Donations',
    'comments' =>  'Comments',
    'rewards' =>  'Rewards',
    'insights' =>  'Insights',
    'marketing' =>  'Marketing',
    'events' =>  'Events',
    'social_media_marketing' =>  'Social Media Marketing',
    'send_texts' =>  'Send Texts',
    'send_emails' =>  'Send Emails',
    'order_print_material' =>  'Order Print Material',
    'my_account' =>  'My Account'   
];
