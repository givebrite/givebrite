<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailToCampaignPaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_payments', function(Blueprint $table)
        {
			$table->string('user_email');
            $table->string('user_country');
            $table->string('user_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_payments', function(Blueprint $table)
        {
			$table->dropColumn('user_email');

        });
    }

}
