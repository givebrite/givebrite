<?php

Route::group(['prefix' => '', 'namespace' => 'Modules\Dashboard\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'Modules\Auth\Http\Middleware\IsActivatedMiddleware', 'App\Http\Middleware\IsUnsecure']], function() {

    // Dashboard Routes
    // Payment Routes
    Route::get('/stripe_connect', [
        'as' => 'stripe.connect',
        'uses' => 'PaymentController@create'
    ]);
    Route::get('/disaconnect_stripe/{charity_id}', [
        'as' => 'stripe.disable',
        'uses' => 'PaymentController@disableStripe'
    ]);
    Route::get('/save_stripe', [
        'as' => 'stripe.store',
        'uses' => 'PaymentController@store'
    ]);
    // My Account Section
    Route::get('dashboard/profile', ['as' => 'dashboard.profile', 'uses' => 'UserProfileController@profile']);
    Route::get('dashboard/profile/{id}/edit', ['as' => 'dashboard.profile-edit', 'uses' => 'UserProfileController@edit']);
    Route::put('dashboard/profile/{id}/update', ['as' => 'dashboard.profile-update', 'uses' => 'UserProfileController@update']);
    // Update Password Routes 
    Route::get('dashboard/profile/{id}/edit-password', ['as' => 'dashboard.password-edit', 'uses' => 'UserProfileController@fetchPassword']);
    Route::put('dashboard/profile/{id}/update-password', ['as' => 'dashboard.password-update', 'uses' => 'UserProfileController@updatePassword']);

    //Campaigns Section
    Route::get('dashboard/campaigns', ['as' => 'dashboard.campaign-list', 'uses' => 'CampaignController@index']);
    Route::post('dashboard/campaigns/{campaignId}/status', ['as' => 'dashboard.update-campaign-status', 'uses' => 'CampaignController@updateStatus']);
    Route::get('dashboard/campaigns/donation', ['as' => 'dashboard.campaign-donations', 'uses' => 'DonationController@index']);
    Route::get('dashboard/campaigns/donation/{campaignId}', ['as' => 'dashboard.campaign-donations-one', 'uses' => 'DonationController@getDonationOnCampaign']);
    Route::get('dashboard/campaigns/export', ['as' => 'dashboard.campaign-donations-export', 'uses' => 'DonationController@exportDonation']);
    Route::get('dashboard/comments', ['as' => 'dashboard.comments-list', 'uses' => 'CampaignController@viewComment']);
        
    
    // Tools Section
    Route::get('dashboard/profile/remove_image', ['as' => 'profile-image.remove', 'uses' => 'UserProfileController@removeImage']);
});


Route::group(['prefix' => '', 'namespace' => 'Modules\Dashboard\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'App\Http\Middleware\IsUnsecure']], function() {
    // Index Page
    Route::get('dashboard', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);
});

Route::group(['prefix' => '', 'namespace' => 'Modules\Auth\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'App\Http\Middleware\IsUnsecure']], function() {
    Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
});

Route::group(['prefix' => '', 'namespace' => 'Modules\Dashboard\Http\Controllers', 'middleware' => ['App\Http\Middleware\IsUnsecure']], function() {
    Route::get('/stripe_mail_connect/{token}', [
        'as' => 'stripe.connect_mail',
        'uses' => 'PaymentController@connectStripeFromMail'
    ]);
    Route::get('/save_mail_stripe', [
        'as' => 'stripe.store_mail',
        'uses' => 'PaymentController@storeStripeFromMail'
    ]);
});

Route::group(['prefix' => '', 'namespace' => 'Modules\Dashboard\Http\Controllers'], function(){
    Route::get('/dashboard/downloadcsv', 'DonationController@export');
});
