<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Session;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Input;
use Excel;
use Helper;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;

class DonationController extends DashboardController
{

    public function __construct()
    {

    }

    /**
     * Function to set variable for views
     *
     * @author AK 03/10/16
     */
    public function setUp()
    {

    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return view('campaigns::errors.404');
    }

    public function index()
    {
        
        // Get Current User Detail From Session
        $user = Session::get('user_session_data');
        $userRole = $user['role'];

        $campaignId = !empty(trim(Input::get('campaign_id'))) ? trim(Input::get('campaign_id')) : 0;
        $charityId = !empty(trim(Input::get('charity_id'))) ? trim(Input::get('charity_id')) : 0;
        //Create EloquentDonationRepo Instance
        $donationRepo = new DonationRepo();
        $campaignRepo = new CampaignRepo();
//        $userCampaigns = $donationRepo->getUserCampaigns($user['id']);

        $userCampaigns = $donationRepo->findCampaignByCharityId($user['id'],$campaignId);
        $charityLinkedWithCampaign = $campaignRepo->getUserConnectedCharities($user['id']);
                                  //$campaignRepo->getAllCharities();
        $paginatorAppends = [];
        // Get ALL Donation List
        
        $donationListQuery = $donationRepo->getAllDonation($user['id'], $campaignId, $charityId, false, $userRole);
        
        //dd($donationListQuery->get()); 
       
        if ($userRole == 'User') {

            $allDonation = $donationListQuery->get();
            
            $donationList = $donationListQuery->paginate(Config('config.detail_donation_recieved_pagination'))->appends(Input::except('page'));
        } else {
          
          //  echo '<pre>';
            $allDonation = $donationListQuery;
          ///  print_r($userCampaigns);die;
            $donationList = $donationListQuery;

            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            //Create a new Laravel collection from the array data
            $collection = new Collection($donationList);

            //Define how many items we want to be visible in each page
            $perPage = Config('config.detail_donation_recieved_pagination');

            //Slice the collection to get the items to display in current page

            $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();

            //Create our paginator and pass it to the view
            $paginatedSearchResults = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
            // Setting Value to the views
            $donationList = $paginatedSearchResults;

        if($campaignId){
        
            $paginatorAppends["campaign_id"] = $campaignId;
        }

        if($charityId){
            
            $paginatorAppends["charityId"] = $charityId;
        }

        }

       
        return view("dashboard::donation.index", compact('allDonation','paginatorAppends', 'user', 'donationList', 'campaignId', 'charityId', 'userCampaigns', 'charityLinkedWithCampaign', 'userRole'));
    }

    public function getDonationOnCampaign($campaignId)
    {   
        // Get Current User Detail From Session
        $user = Session::get('user_session_data');
        $userRole = $user['role'];

        # $campaignId = !empty(trim(Input::get('campaign_id'))) ? trim(Input::get('campaign_id')) : 0;
        $charityId = !empty(trim(Input::get('charity_id'))) ? trim(Input::get('charity_id')) : 0;
        //Create EloquentDonationRepo Instance
        $donationRepo = new DonationRepo();
        $campaignRepo = new CampaignRepo();
        $userCampaigns = $donationRepo->getUserCampaigns($user['id']);
        $charityLinkedWithCampaign = $campaignRepo->getAllCharities();
        // Get ALL Donation List
        $donationListQuery = $donationRepo->findByCampaignId($campaignId);

        $allDonation = $donationListQuery->get();

        $donationList = $donationListQuery->paginate(Config('config.detail_donation_recieved_pagination'));

        return view("dashboard::donation.camp_donation", compact('allDonation', 'user', 'donationList', 'campaignId', 'charityId', 'userCampaigns', 'charityLinkedWithCampaign'));
    }

    /**
     * Function to Export All Contacts
     *
     * @author  AK 22 Feb 2016
     * @return \Illuminate\Http\Response
     */
    /**
     * Function to Export All Contacts
     *
     * @author  AK 22 Feb 2016
     * @return \Illuminate\Http\Response
     */
    public function export()
    {

        // Get Current User Detail From Session
        $user = Session::get('user_session_data');
        $userRole = $user['role'];

        $campaignId = !empty(trim(Input::get('campaign_id'))) ? trim(Input::get('campaign_id')) : 0;
        $charityId = !empty(trim(Input::get('charity_id'))) ? trim(Input::get('charity_id')) : 0;

        //Create EloquentDonationRepo Instance
        $donationRepo = new DonationRepo();
        // Get Donations
        if ($userRole == 'User') {
            $donationList = $donationRepo->getAllDonation($user['id'], $campaignId, $charityId, false, $userRole)->get();
        } else {
            $donationList = $donationRepo->getAllDonation($user['id'], $campaignId, $charityId, false, $userRole);
        }
        // converting std class object to array
        $donationRecieved = [];
        foreach ($donationList as $donation) {

            $donationRecieved[] = [
                'Campaign Name' => !empty($donation->campaign_name) ? $donation->campaign_name : "",
                'Title' => !empty($donation->salutation) ? $donation->salutation : "",
                'First Name' => !empty($donation->first_name) ? $donation->first_name : "",
                'Last Name' => !empty($donation->sur_name) ? $donation->sur_name : "",
                'PostCode' => !empty($donation->postcode) ? $donation->postcode : "",
                'Address' => !empty($donation->address) ? $donation->address : "",
                'Email' => !empty($donation->email) ? $donation->email : "",
                'Town' => !empty($donation->locality) ? $donation->locality : "",
                'Donation Amount' => !empty($donation->donation_amount_monetory) ? Helper::showMoney($donation->donation_amount_monetory) : "",
                'Donation Type' => !empty($donation->title) ? $donation->title : "",
                'Gift Aid' => !empty($donation->is_giftaid) ? "Yes" : "No",
                'paid_fees' => !empty($donation->is_fees_paid_by_donor) ? "Yes" : "No",
//                'paid_fees_amount' => !empty($donation->transaction_fee) ? $donation->transaction_fee : 0,
                'Donated On' => !empty($donation->created_at) ? Helper::showDate($donation->created_at) : "",
                'Given/Recieved' => !empty($donation->donorId) && $donation->donorId == $donation->recieverId ? "Self-Donate" : (!empty($donation->donorId) && $donation->donorId == $user['id'] ? "Given" : "Recieved"),
                'Donation comment' => !empty($donation->content) ? $donation->content : ""
            ];
        }

        $countColumn = count($donationRecieved) + 2;
        Excel::create('Donation', function ($excel) use ($donationRecieved, $countColumn) {
            $excel->sheet('Donation', function ($sheet) use ($donationRecieved, $countColumn) {
                $sheet->fromArray($donationRecieved);
                $sheet->freezeFirstRow();
            });
        })->export('csv');
        return true;
    }
}
