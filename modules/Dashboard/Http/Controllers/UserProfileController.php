<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Pingpong\Modules\Routing\Controller;
use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Modules\Campaigns\Entities\CampaignDesign as CampaignDesignModel;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Auth\Resources\lang\en;
use Redirect;
use Auth;
use View;
use Illuminate\Support\Facades\Session;
use Pingpong\Admin\Uploader\ImageUploader;
use Config;
use Modules\Auth\Http\Requests\UpdateProfileRequest;
use Modules\Auth\Http\Requests\RegisterUpdateRequest;
use Toastr;
use Illuminate\Support\Facades\Hash;
use Helper;
use File;

class UserProfileController extends Controller {

    protected $auth,
            $user,
            $profile,
            $campaigns,
            $campaignDesign,
            $users,
            $profileImageStoragePath,
            $charityLogoStoragePath,
            $uploader;

    function __construct(Guard $auth, UserRepo $user, UserProfileRepo $profile, CampaignModel $campaigns, CampaignDesignModel $campaignDesign, ImageUploader $uploader) {
        $this->auth = $auth;
        $this->user = $user;
        $this->profile = $profile;
        $this->campaigns = $campaigns;
        $this->campaignDesign = $campaignDesign;
        $this->uploader = $uploader;
        $this->profileImageStoragePath = Config::get('config.profile_image_upload_path');
        $this->charityLogoStoragePath = Config::get('config.charity_logo_upload_path');
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return Redirect::route('dashboard.index');
    }

    /**
     * Display user profile
     *
     * @param  null
     * @return \Illuminate\Http\Response
     */
    public function profile() {
        
        try {
            //Get Login User Details
            $user = Session::get('user_session_data');
            $profile = $this->profile->getProfileData($user['id']);

            if (is_object($profile) && !empty($profile)) {

                if ($profile->user_id == $user['id']) {
                    return view::make('dashboard::profile.edit', compact('profile', 'user'));
                }

                Session::flash('error', 'You can not access this link');
                return $this->redirectNotFound();
            }

            return $this->redirectNotFound();
        } catch (\Exception $e) {
            Session::flash('error', 'There is an exception to access this link');
            return $this->redirectNotFound();
        }
    }

    /**
     * Update user profile
     *
     * @param  Request $input
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request, $id) {
        
        //Get Login User Details
        $user = Session::get('user_session_data');
        // Check Role
        if ($user['role'] == 'Charity') {
            // Update Charity Details
            $userProfileRepo = new UserProfileRepo();
            $res = $userProfileRepo->findByRegNumber($request->registration_no, $id);
            if ($res == 0) {
                $charityProfileData = $request->only('charity_logo', 'first_name', 'sur_name', 'charity_name', 'registration_no', 'address', 'postcode', 'country', 'locality', 'lat', 'lng', 'contact_telephone', 'web', 'description');
                $updateCharityProfile = $this->updateProfile($charityProfileData, $id, $user['role']);
            } else {
                Toastr::error("Registration Number is already taken", $title = null, $options = []);
                return Redirect::route('dashboard.profile');
            }
        } else {
            // Update User Details
            $userProfileData = $request->only('profile_pic', 'first_name', 'sur_name', 'postcode', 'address', 'country', 'locality', 'lat', 'lng', 'contact_telephone');
            $updateCharityProfile = $this->updateProfile($userProfileData, $id, $user['role']);
        }

        $updateSession = Helper::refreshSession();
        //return view with message
        Toastr::Success("Profile updated successfully", $title = null, $options = []);

        return Redirect::route('dashboard.profile');
    }

    /**
     * Update user password
     *
     * @param  null
     * @return \Illuminate\Http\Response
     */
    public function fetchPassword($id) {

        try {
            // Fetch User Data        
            $profile = $this->user->findById($id);

            if (is_object($profile) && !empty($profile)) {
                return view::make('dashboard::profile.password', compact('profile'));
            }

            return $this->redirectNotFound();
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update user password
     *
     * @param  null
     * @return \Illuminate\Http\Response
     */
    public function updatePassword() {

        try {
            //get request rules
            $request_rule = new RegisterUpdateRequest();
            $rules = [
                'current_password' => 'required|min:6',
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6'
            ];
            //get form input
            $input = Input::only(
                            'current_password', 'password', 'password_confirmation'
            );

            //check validations on form inputs
            $validator = Validator::make($input, $rules, $request_rule->messages());
            if ($validator->fails()) {
                Toastr::Error("Input validation failed", $title = null, $options = []);
                return Redirect::back()->withInput()->withErrors($validator);
            }

            $id = Input::get('user_id');
            $current_password = Input::get('current_password');
            $new_password = Input::get('password');

            //creates User model and user entry in the users table
            $user_model = new UserRepo();
            $user_response = $user_model->findById(Auth::user()->id);
            if (Hash::check($current_password, $user_response->password)) {
                $user_data = array(
                    'password' => Hash::make($new_password)
                );
                $email = $user_response->email;
                $user_response = $user_model->updateByEmail($user_data, $email);
                //return view with message
                Toastr::Success("Password updated successfully", $title = null, $options = []);
                return Redirect::back();
            } else {
                //return view with message
                Toastr::Error("Password is wrong", $title = null, $options = []);
                return Redirect::back();
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Function To update Charity profile info
     * 
     */
    public function updateProfile($data, $id, $role) {

        //split address to streetname and number
        
        $address = Helper::splitAddress($data['address']);
        
        if(!empty($address['houseNumber'])){

            $data['house_number'] = $address['houseNumber'];
        
        }  else {

            $data['house_number'] = '';
        }

        if(!empty($address['streetName'])){

            $data['street_name'] = $address['streetName'];
        }else {

            $data['street_name'] = '';
        }

        if(!empty($address['additionToAddress1'])){

            $data['addition_to_address'] = $address['additionToAddress1'].' '.$address['additionToAddress2'];
        }else {

            $data['addition_to_address'] = '';
        }

        // Creating Instance For User Profile Repo
        $userProfileRepo = new UserProfileRepo();

        // Check If File Exist && Upload File
        if ($role == "Charity") {
            unset($data['charity_logo']);
            if (\Input::hasFile('charity_logo')) {
                // upload image
                $image = $this->uploader->upload('charity_logo');
                $image->save($this->charityLogoStoragePath);
                $data['charity_logo'] = $image->getFilename();
                $image_detail = array(
                    'inputFile' => Input::file('charity_logo'),
                    'image' => $image,
                    'thumb_size' => Config::get('config.thumb_charity_size'),
                    'thumb_path' => Config::get('config.thumb_charity_logo_upload_path'),
                    'large_size' => Config::get('config.large_charity_size'),
                    'large_path' => Config::get('config.large_charity_logo_upload_path')
                );
                Helper::createThumb($image_detail);
            }
            $userData = [
                'name' => $data['charity_name']
            ];
        } else {
            unset($data['profile_pic']);
            if (\Input::hasFile('profile_pic')) {
                // upload image
                $image = $this->uploader->upload('profile_pic');
                $image->save($this->profileImageStoragePath);
                $data['profile_pic'] = $image->getFilename();
                $image_detail = array(
                    'inputFile' => Input::file('profile_pic'),
                    'image' => $image,
                    'thumb_size' => Config::get('config.thumb_user_size'),
                    'thumb_path' => Config::get('config.thumb_profile_image_upload_path'),
                    'large_size' => Config::get('config.large_user_size'),
                    'large_path' => Config::get('config.large_profile_image_upload_path')
                );
                Helper::createThumb($image_detail);
            }
            $userData = [
                'name' => $data['first_name'] . " " . $data['sur_name']
            ];
        }


        $updateData = $userProfileRepo->update($data, $id);

        $userRepo = new UserRepo();
        $updateUserData = $userRepo->update($userData, $updateData->user_id);
        // Save Data in Users Table
//        if (!empty($data['email']) && $updateData['user_id']) {
//            // Update Email
//            $userRepo = new UserRepo();
//            $userData['email'] = $data['email'];
//            $updateEmail = $userRepo->update($userData, $updateData['user_id']);
//        }

        return $updateData;
    }

    /**
     * Remove Profile Image Via Ajax
     */
    public function removeImage() {
        //Get Login User Details
        $user = Session::get('user_session_data');
        // Creating Instance For User Profile Repo
        $userProfileRepo = new UserProfileRepo();
        if (!empty($user['id'])) {
            $profileDetail = $userProfileRepo->findByUserId($user['id']);
            if ($user['role'] == 'Charity') {
                $base_path_original = $this->charityLogoStoragePath;
                $base_path_thumb = Config::get('config.thumb_charity_logo_upload_path');
                $base_path_large = Config::get('config.large_charity_logo_upload_path');

                $charityLogo = $profileDetail->charity_logo;
                if (File::exists(public_path($base_path_original . $charityLogo))) {
                    unlink(public_path($base_path_original . $charityLogo));
                    unlink(public_path($base_path_thumb . $charityLogo));
                    unlink(public_path($base_path_large . $charityLogo));
                }

                // Update Profile
                echo json_encode(['responseCode' => 200, 'responseData' => asset('images/default-images/default_logo.png')]);
            } else {
                $base_path_original = $this->profileImageStoragePath;
                $base_path_thumb = Config::get('config.thumb_profile_image_upload_path');
                $base_path_large = Config::get('config.large_profile_image_upload_path');

                $profileImage = $profileDetail->profile_pic;
                if (File::exists(public_path($base_path_original . $profileImage))) {
                    unlink(public_path($base_path_original . $profileImage));
                    unlink(public_path($base_path_thumb . $profileImage));
                    unlink(public_path($base_path_large . $profileImage));
                }

                // Update Profile
                echo json_encode(['responseCode' => 200, 'responseData' => asset('images/default-images/default_profile.png')]);
            }
        } else {

            echo json_encode(['responseCode' => 0]);
        }
    }

//    public function createThumb($image)
//    {
//        $thumb_image = $image;
//        $large_image = $image;
//
//        $resized_thumb_image = Helper::resizeImage($thumb_image->image, Config::get('config.thumb_charity_size'));
//        if ($resized_thumb_image) {
//            $thumb_image->save(Config::get('config.thumb_charity_logo_upload_path'));
//        }
//        $resized_large_image = Helper::resizeImage($large_image->image, Config::get('config.large_charity_size'));
//        if ($resized_large_image) {
//            $large_image->save(Config::get('config.large_charity_logo_upload_path'));
//        }
//    }
}
