<?php

namespace Modules\Dashboard\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Redirect;
use Config;
use LogsConfig as LogsConfig;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Session;
use Input;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Toastr;

class CampaignController extends DashboardController {

    /**
     * Display user dashboard
     *
     * @param  null
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if (!empty($this->auth->user()->id)) {
            // Get Current User Detail From Session
            $user = Session::get('user_session_data');
            //$campaignsList = $this->campaigns->getCampaignWithJoin($user['id'], false, false, true)->paginate(Config('config.dashboard_home_pagination'));
            $campaignsList = $this->campaigns->getCampaignWithJoinDashboard($user['id'], true, true, false)->get();
            if (!empty($user['role']) && $user['role'] == "Charity") {
                // Get Charity Fundraisers
                $charityFundraisers = $this->campaigns->getCharityFundraisers($user['id']);
            }
            return view('dashboard::campaign.index', compact('campaignsList', 'user', 'charityFundraisers'));
        }
        //return view with message
        Toastr::Error(trans('dashboard::campaign/messages.index.error'), $title = null, $options = []);

        return Redirect::to('login');
    }

    /**
     * Modify Compaign status
     *
     * @param  integer compaign id
     * @return Boolean true/false
     */
    public function updateStatus(Request $request, $id) {

        $campaign = $this->campaigns->findById($id);
        $userId = !empty($this->auth->user()->id) ? $this->auth->user()->id : 0;
        if (isset($campaign) && !empty($campaign) && !empty($userId) && $campaign->user_id == $userId) {

            if ($campaign->status) {
                $campaign->status = 0;
                $message = "Campaign disabled successfully";
                $class = "info";
            } else {
                $campaign->status = 1;
                $message = "Campaign enabled successfully";
                $class = "danger";
            }

            if (!$campaign->save()) {
                LogsConfig::displayLogsError(
                        Config::get('config.error_type'), __FILE__ . '(' . __LINE__ . ')'
                        . ' Campaign status has not changed'
                );

                //return view with message
                Toastr::Error(trans('dashboard::campaign/messages.status.error'), $title = null, $options = []);

                return Redirect::route('dashboard.campaign-list');
            }

            LogsConfig::displayLogsError(
                    Config::get('config.info_type'), __FILE__ . '(' . __LINE__ . ')'
                    . ' Campaign status has been changed'
            );
            // Check If Ajax Request
            if ($request->ajax()) {
                echo json_encode(['responseCode' => 200, 'responseMessage' => $message, 'responseClass' => $class, 'campaignId' => $id]);
                die;
            }
            //return view with message
            Toastr::Success(trans('dashboard::campaign/messages.status.success'), $title = null, $options = []);

            return Redirect::back();
        }

        LogsConfig::displayLogsError(
                Config::get('config.error_type'), __FILE__ . '(' . __LINE__ . ')'
                . ' Campaign has not found'
        );
        //return view with message
        Toastr::error(trans('dashboard::campaign.messages.campaign.error'), $title = null, $options = []);

        //return Redirect::route('dashboard.campaign-list');
        return Redirect::back();
    }

    public function viewComment() {
        // Get Current User Detail From Session
        $user = Session::get('user_session_data');
        $userRole = $user['role'];

        $campaignId = !empty(trim(Input::get('campaign_id'))) ? trim(Input::get('campaign_id')) : 0;
        $charityId = !empty(trim(Input::get('charity_id'))) ? trim(Input::get('charity_id')) : 0;

        //Create EloquentCampaignRepo Instance
        $campaignRepo = new CampaignRepo();
        $donationRepo = new DonationRepo();
        $userCampaigns = $donationRepo->getUserCampaigns($user['id']);
        $charityLinkedWithCampaign = $donationRepo->getUserCampaignLinkedCharity($user['id']);
        // Get Comments 
        $commentListQuery = $campaignRepo->getCommentsList($user['id'], $campaignId, $charityId);

        $commentList = $commentListQuery->paginate(Config('config.detail_donation_recieved_pagination'))->appends(Input::except('page'));

        return view("dashboard::comments.index", compact('commentList', 'campaignId', 'charityId', 'userCampaigns', 'charityLinkedWithCampaign'));
    }

}
