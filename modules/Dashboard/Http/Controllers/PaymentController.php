<?php namespace Modules\Dashboard\Http\Controllers;

use Config;
use Input;
use Modules\Campaigns\Repositories\EloquentCampaignLaunchRepository as CampaignLaunchRepo;
use Modules\Auth\Entities\UserActivation as UserActivationModel;
use Toastr;
use Redirect;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Campaigns\Entities\CampaignPayment as CampaignPaymentModel;
use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Modules\Auth\Entities\UserProfile as UserProfileModel;
use Mail;

class PaymentController extends DashboardController
{

    /**
     * Function To Connect Stripe From Link Send From Mail
     */
    public function connectStripeFromMail($token = null)
    {
        $user = \Auth::user();
        if (empty($user)) {
            // Redirect To Login 
            return Redirect::to('/login?redirect_url=stripe.connect_mail&token=' . $token);
        } else {
            $userId = \Auth::id();
            // Validate Token
            $token = explode("&&", $token);
            if (!empty($token[0])) {
                $tokenCode = $token[0];
                $tokenEmail = !empty($token[1]) ? $token[1] : "";
                $checkToken = UserActivationModel::where('token_code', $tokenCode)->where('user_id', $userId)->where('otp_verified', 0)->first();
                if (!empty($checkToken) && count($checkToken) > 0) {
                    UserActivationModel::where('user_id', $userId)->where('token_code', $tokenCode)
                        ->update(['otp_verified' => 1]);
                } else {
                    //return view with message
                    Toastr::Error("Invalid token", $title = null, $options = []);
                    return Redirect::route('404');
                }
            }

            // Show Stripe Setup Screen
            $campaignId = "0";
            $paymentOptionId = 3;

            $userProfile = UserProfileModel::where('id', $userId)->first();
            $paymentOptionVal = $userProfile->is_payment_account;

            // Creting Instance Of Campaign Repo
            $paymentOptions = CampaignPaymentModel::where(['user_id' => $userId, 'campaign_id' => 0])->first();
            $stripe_connect_link = $this->setStripeConnectLink(true);
            return view('site.stripe_connect', compact('userId', 'stripe_connect_link', 'paymentOptions', 'paymentOptionVal'));
        }
    }

    public function storeStripeFromMail()
    {
        $saveStripe = $this->store(true);
        if ($saveStripe) {
            return view('site.stripe_success');
        } else {
            return view('site.404');
        }
    }

    /**
     * Function to connect stripe account for charity
     */
    public function create()
    {
        $userId = \Auth::id();
        $campaignId = "0";
        $paymentOptionId = 3;

        $userProfile = UserProfileModel::where('id', $userId)->first();
        if (!empty($userProfile)) {
            $paymentOptionVal = $userProfile->is_payment_account;
        } else {
            $paymentOptionVal = 0;
        }


        // Creting Instance Of Campaign Repo
        $paymentOptions = CampaignPaymentModel::where(['user_id' => $userId, 'campaign_id' => 0])->first();
        $stripe_connect_link = $this->setStripeConnectLink();
        return view('dashboard::payment.create', compact('userId', 'stripe_connect_link', 'paymentOptions', 'paymentOptionVal'));
    }

    public function store($mail = false)
    {
        $client_id = Config::get('app.stripe_client_id');
        $secret_key = Config::get('app.stripe_secret_key');

        $userId = \Auth::id();
        $token_uri = 'https://connect.stripe.com/oauth/token';
        $code = Input::get('code');
        if (!empty($code)) { // Redirect w/ code
            $token_request_body = array(
                'grant_type' => 'authorization_code',
                'client_id' => $client_id,
                'code' => $code,
                'client_secret' => $secret_key
            );

            $req = curl_init($token_uri);

            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true);
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
            curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($req, CURLOPT_SSL_VERIFYHOST, false);

            // TODO: Additional error handling
            $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);

            $resp = json_decode(curl_exec($req), true);
            curl_close($req);
            if ($resp) {
                if (isset($resp['error'])) {
                    return Redirect::route('404');
                }
            }


            $customer = \Stripe\Stripe::setApiKey($resp['access_token']);

            $customerInfo = \Stripe\Account::retrieve($resp['stripe_user_id']);
            if (!empty($customerInfo->email)) {
                $charity_connect_data['user_id'] = $userId;
                $charity_connect_data['campaign_id'] = 0;
                $charity_connect_data['status'] = 'yes';
                $charity_connect_data['payment_option_id'] = 3;

                $charity_connect_data['connect_access_token'] = $resp['access_token'];
                $charity_connect_data['connect_livemode'] = $resp['livemode'];
                $charity_connect_data['connect_refresh_token'] = $resp['refresh_token'];
                $charity_connect_data['connect_token_type'] = $resp['token_type'];
                $charity_connect_data['connect_stripe_publishable_key'] = $resp['stripe_publishable_key'];
                $charity_connect_data['connect_stripe_user_id'] = $resp['stripe_user_id'];
                $charity_connect_data['connect_scope'] = $resp['scope'];
                $charity_connect_data['percent_application_fees'] = Config::get('config.app_fee_for_charity');

                $charity_connect_data['user_email'] = $customerInfo->email;
                $charity_connect_data['user_name'] = !empty($customerInfo->display_name) ? $customerInfo->display_name : "";
                $charity_connect_data['user_country'] = $customerInfo->country;

                $charity_connect_data['is_default'] = 1;
                $campaign_launch = new CampaignLaunchRepo();
                // Delete Previous Entries For Stripe Connection
                CampaignPaymentModel::where('user_id', $userId)->where('campaign_id', 0)->where('payment_option_id', 3)->delete();

                $campaign_launch->create($charity_connect_data);

                // Update User Profile(Set is_payment_account=1)
                UserProfileModel::where('user_id', $userId)
                    ->update(['is_payment_account' => 1]);
                Toastr::success("Stripe account connected successfully", $title = null, $options = []);
                if ($mail) {
                    return true;
                } else {
                    return Redirect::Route('stripe.connect');
                }

            } else {
                Toastr::error("Invalid Stripe Account.", $title = null, $options = []);
                if ($mail) {
                    return true;
                } else {
                    return Redirect::Route('stripe.connect');
                }
            }


        }
        Toastr::error("Stripe not configured properly", $title = null, $options = []);
        return Redirect::route('404');
    }

    public function setStripeConnectLink($mail = false)
    {
        $client_id = Config::get('app.stripe_client_id');
        // Show OAuth link

        if ($mail) {
            $redirectUrl = url() . '/save_mail_stripe';
        } else {
            $redirectUrl = url() . '/save_stripe';
        }

        //dd($redirectUrl);
        $authorize_request_body = array(
            'response_type' => 'code',
            'scope' => 'read_write',
            'client_id' => $client_id,
            'redirect_uri' => $redirectUrl,
        );
        $authorize_uri = 'https://connect.stripe.com/oauth/authorize';

        $url = $authorize_uri . '?' . http_build_query($authorize_request_body);
        return $url;
    }

    public function disableStripe($charityId)
    {

        // Check IF Any Active Campaign For That Charity
        $checkCharityActiveCampaigns = CampaignModel::where(['user_id' => $charityId, 'status' => 1, 'stage' => 5])->orWhere(['charity_id' => $charityId, 'status' => 1, 'stage' => 5])->first();

        if (!empty($checkCharityActiveCampaigns) && count($checkCharityActiveCampaigns) > 0 && !empty($checkCharityActiveCampaigns->end_date) && Helper::isExpired($checkCharityActiveCampaigns->end_date)) {
            // Return Error
            Toastr::error("You can't disconnect stripe. Some campaigns are still active.", $title = null, $options = []);
            return Redirect::back();
        } else {
            // Update Status
            $status = "";
            $paymentModel = CampaignPaymentModel::where(['user_id' => $charityId, 'campaign_id' => 0])->first();
            $currentStatus = $paymentModel->status;
            $statusToChange = (!empty($currentStatus) && $currentStatus == 'yes') ? 'no' : 'yes';
            $updateStripe = CampaignPaymentModel::where(['user_id' => $charityId, 'campaign_id' => 0])->update(['status' => $statusToChange]);
            if ($statusToChange == 'no') {
                $message = "Stripe Disconnected Successfully";
            } else {
                $message = "Stripe Connected Successfully";
            }

            Toastr::success($message, $title = null, $options = []);
            return Redirect::back();
        }
    }
}
