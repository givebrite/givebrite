<?php

namespace Modules\Dashboard\Http\Controllers;

use Mockery\Exception;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Modules\Campaigns\Repositories\EloquentCampaignDesignRepository as CampaignDesignRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Modules\Auth\Resources\lang\en;
use Redirect;
use Auth;
use View;
use Config;
use Session;

class DashboardController extends Controller
{

    protected $auth,
        $user,
        $profile,
        $campaigns,
        $campaignDesign,
        $donation;

    function __construct(Guard $auth, UserRepo $user, UserProfileRepo $profile, CampaignRepo $campaignRepo, CampaignDesignRepo $campaignDesign, DonationRepo $donation)
    {
        $this->auth = $auth;
        $this->user = $user;
        $this->profile = $profile;
        $this->campaigns = $campaignRepo;
        $this->campaignDesign = $campaignDesign;
        $this->donation = $donation;
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return $this->redirect('profile');
    }

    /**
     * Display user dashboard
     *
     * @param  null
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        
        // Get Current User Detail From Session
        $user = Session::get('user_session_data');
        $DonationIn30Days = $this->campaigns->Donation30days();
        $donation_per_day = $this->donation->DonationPerDay($user['id']);
        $datesArray = $this->getDatesArray();
        $amount_per_day = array();
        foreach ($datesArray as $date) {
            $tmp = array();

            foreach ($donation_per_day as $donation) {

                if ($donation->Date == $date) {

                    $tmp = array('value' => (string)$donation->total_inday);
                }
            }
            array_push($amount_per_day, $tmp);
        }

        for ($i = 0; $i < count($amount_per_day); $i++) {
            if (empty($amount_per_day[$i])) {
                $amount_per_day[$i] = array('value' => '0');
            }
        }
        $dayArray = $this->getDay($datesArray);
        $totalAmount = $this->campaigns->ToalAmountRaised($user['id']);
        $campaignsList = $this->campaigns->getCampaignWithJoinDashboard($user['id'], true, true, false)->paginate(Config('config.dashboard_home_pagination'));
        return view('dashboard::index', compact('campaignsList', 'DonationIn30Days', 'totalAmount', 'amount_per_day', 'dayArray'));
    }


    function getDatesArray()
    {
        $d = array();
        for ($i = 29; $i >= 0; $i--)
            $d[] = date("y-m-d", strtotime('-' . $i . ' days'));
        return $d;
    }

    function getDay($date)
    {
        $dayArray = array();
        foreach ($date as $dt) {
            $timestamp = strtotime($dt);
            $day = date('l', $timestamp);
            $day = substr($day, 0, 1);
            $temp_array = array('label' => $day);
            array_push($dayArray, $temp_array);
        }
        return $dayArray;

    }

}
