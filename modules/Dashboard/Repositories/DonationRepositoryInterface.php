<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Dashboard\Repositories;

interface DonationRepositoryInterface {

    public function selectByUserAndCampaign($id);

    public function find($id);
    
    public function findByUser($id);
    
    //author : Harsh
    public function create(array $data);
}
