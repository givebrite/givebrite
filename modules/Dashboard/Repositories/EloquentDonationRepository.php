<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Dashboard\Repositories;

use DB;
use Modules\Auth\Entities\UserProfile as UserProfileModel;
use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Modules\Dashboard\Entities\Donation as DonationModel;

class EloquentDonationRepository implements DonationRepositoryInterface
{

    public function getModel()
    {
        return new DonationModel();
    }

    public function selectByUserAndCampaign($id)
    {
        return $this->getModel()
            ->leftJoin('users', function ($join) {
                $join->on(
                    'users.id', '=', 'campaign_donations.user_id'
                );
            })
            ->leftJoin('campaigns', function ($join) {
                $join->on(
                    'campaigns.id', '=', 'campaign_donations.campaign_id'
                );
            })
            ->where('users.id', $id)
            ->get([
                'campaign_donations.id',
                'campaign_donations.donation_amount_time',
                'campaign_donations.donation_amount_monetory',
                'users.name',
                'campaigns.campaign_name',
                'campaign_donations.created_at',
            ]);
    }

    public function findByDonationId($donation_id,$app_fees){

        return DB::table('campaign_donations')
            ->where('id', $donation_id)
            ->update(['application_fees_charge' => $app_fees,'status'=>1]);

    }
    
    public function find($id)
    {
        return $this->getModel()->findById($id);
    }

    public function findByUser($id)
    {
        return $this->getModel()->where('user_id', $id)->get();
    }

    public function findByCampaignId($campaignId)
    {
        return DB::table('campaign_donations as donation')
            ->join('users as user', 'user.id', '=', 'donation.user_id')
            ->join('user_profile as profile', 'user.id', '=', 'profile.user_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->leftjoin('comments as comment', 'comment.donation_id', '=', 'donation.id')
            ->select('user.salutation', 'donation.id as donationId', 'donation.user_id as donorId', 'user.id as userId', 'campaign.id as campaignId', 'campaign.user_id as recieverId', 'campaign.campaign_name', 'campaign.slug', 'user.name', 'donation.donation_amount_monetory', 'donation.created_at', 'donation.is_giftaid', 'profile.slug as userSlug', 'profile.locality as locality', 'profile.address as address', 'profile.postcode as postcode', 'comment.content', 'comment.is_anonymous')
            ->where('donation.campaign_id', $campaignId)
            ->where('donation.status', 1)
            ->where('user.status', 1)
            ->orderBy('donation.created_at', 'desc');
    }

    public function getAllDonation($userId, $campaignId = 0, $charityId = 0, $individual_compaign_chk = false, $userRole)
    {   
        //dd($userRole);
        if ($userRole == 'User') {

            //The comment join was causing the issue 
            $result = $this->getAllDonationUsersNoComments($userId, $campaignId, $charityId, $individual_compaign_chk);

        } elseif ($userRole == 'Charity') {
            $result = array();

            $listofConnectedCampaigns = $this->findCampaignByCharityId($userId);
           
            if ($campaignId != 0) {
            $listofConnectedCampaigns=  array($campaignId=>$listofConnectedCampaigns[$campaignId]);
              
            }
            foreach ($listofConnectedCampaigns as $id => $name) {
                $res[] = DB::table('campaign_donations as cmpd')
                    ->leftjoin('campaigns as cmp', 'cmp.id', '=', 'cmpd.campaign_id')
                    ->leftjoin('users as user', 'user.id', '=', 'cmpd.user_id')
                    ->leftjoin('user_profile as profile', 'profile.user_id', '=', 'cmpd.user_id')
                    ->leftjoin('comments as cmt', 'cmt.donation_id', '=', 'cmpd.id')
                    ->leftjoin('campaign_donor as cd', 'cmp.id', '=', 'cd.campaign_id')
                    ->leftjoin('donation_type as dt', 'cmpd.donation_type_id', '=', 'dt.id')
                    ->select('cmpd.id as donationId', 'cmpd.user_id as donorId', 'cmpd.is_fees_paid_by_donor', 'cmpd.transaction_fee', 'user.id as userId', 'user.email', 'user.salutation', 'cmp.id as campaignId', 'cmp.user_id as recieverId', 'cmp.campaign_name', 'cmp.slug', 'user.name', 'cmpd.donation_amount_monetory', 'cmpd.created_at', 'cmpd.is_giftaid', 'profile.slug as userSlug', 'profile.locality as locality', 'profile.address as address', 'profile.first_name', 'profile.sur_name', 'profile.postcode as postcode', 'cmt.content','dt.title','cmpd.is_offline'
                    )
                    ->where(['cmpd.campaign_id'=> $id,'cmp.status'=>1])
                    ->orderBy('cmpd.id', 'desc')->groupBy('cmpd.id')->get();

            }

            // foreach ($listofConnectedCampaigns as $id => $name) {
            //     $res[] = DB::table('campaign_donations as cmpd')
            //         ->leftjoin('campaigns as cmp', 'cmp.id', '=', 'cmpd.campaign_id')
            //         ->leftjoin('users as user', 'user.id', '=', 'cmpd.user_id')
            //         ->leftjoin('user_profile as profile', 'profile.user_id', '=', 'cmpd.user_id')
            //         ->leftjoin('comments as cmt', 'cmt.donation_id', '=', 'cmpd.id')
            //         ->leftjoin('campaign_donor as cd', 'cmp.id', '=', 'cd.campaign_id')
            //         ->leftjoin('donation_type as dt', 'cd.donor_type_id', '=', 'dt.id')
            //         ->select('cmpd.id as donationId', 'cmpd.user_id as donorId', 'cmpd.is_fees_paid_by_donor', 'cmpd.transaction_fee', 'user.id as userId', 'user.email', 'user.salutation', 'cmp.id as campaignId', 'cmp.user_id as recieverId', 'cmp.campaign_name', 'cmp.slug', 'user.name', 'cmpd.donation_amount_monetory', 'cmpd.created_at', 'cmpd.is_giftaid', 'profile.slug as userSlug', 'profile.locality as locality', 'profile.address as address', 'profile.first_name', 'profile.sur_name', 'profile.postcode as postcode', 'cmt.content','dt.title','cmpd.is_offline'
            //         )
            //         ->where(['cmpd.campaign_id'=> $id,'cmp.status'=>1,'cmp.user_id'=>$userId])
            //         ->orderBy('cmpd.id', 'desc')->groupBy('cmpd.id')->get();

            // }
            
            if(!empty($res)){

                foreach ($res as $r) {
                    foreach ($r as $n)
                        array_push($result, $n);
                }
            }    
            

        }//echo '<pre>';print_r($result);die;
        return $result;

    }
    public function getAllDonationgift($userId, $campaignId = 0, $charityId = 0, $individual_compaign_chk = false, $userRole)
    {
        if ($userRole == 'User') {
            $result = $this->getAllDonationUsers($userId, $campaignId, $charityId, $individual_compaign_chk);
        } elseif ($userRole == 'Charity') {
            $result = array();

            $listofConnectedCampaigns = $this->findCampaignByCharityId($userId);
           
            if ($campaignId != 0) {
            $listofConnectedCampaigns=  array($campaignId=>$listofConnectedCampaigns[$campaignId]);
              
            }
             foreach ($listofConnectedCampaigns as $id => $name) {
                $res[] = DB::table('campaign_donations as cmpd')
                    ->leftjoin('campaigns as cmp', 'cmp.id', '=', 'cmpd.campaign_id')
                    ->leftjoin('users as user', 'user.id', '=', 'cmpd.user_id')
                    ->leftjoin('user_profile as profile', 'profile.user_id', '=', 'cmpd.user_id')
                    ->leftjoin('comments as cmt', 'cmt.donation_id', '=', 'cmpd.id')
                    ->leftjoin('campaign_donor as cd', 'cmp.id', '=', 'cd.campaign_id')
                    ->leftjoin('donation_type as dt', 'cd.donor_type_id', '=', 'dt.id')
                    ->select('cmpd.id as donationId', 'cmpd.user_id as donorId', 'cmpd.is_fees_paid_by_donor', 'cmpd.transaction_fee', 'user.id as userId', 'user.email', 'user.salutation', 'cmp.id as campaignId', 'cmp.user_id as recieverId', 'cmp.campaign_name', 'cmp.slug', 'user.name', 'cmpd.donation_amount_monetory', 'cmpd.created_at', 'cmpd.is_giftaid', 'profile.slug as userSlug', 'profile.locality as locality', 'profile.address as address', 'profile.first_name', 'profile.sur_name', 'profile.postcode as postcode', 'cmt.content','dt.title'
                    )
                    ->where(['cmpd.campaign_id'=> $id,'cmp.status'=>1,'is_giftaid'=>1])
                    ->orderBy('cmpd.id', 'desc')->get();

            }
            
                
            foreach ($res as $r) {
                foreach ($r as $n)
                    array_push($result, $n);
            }

        }//echo '<pre>';print_r($result);die;
        return $result;

    }


    public function findCampaignByCharityId($userId)
    {
       // DB::enableQueryLog();
         return $query = DB::table('campaigns as cmp')
            //added join showing campaigns that have atleast one donation
            ->join('campaign_donations', 'cmp.id', '=', 'campaign_donations.campaign_id')
            ->where(['cmp.status'=>1])
            ->where(function ($quer) use ($userId) {
                        
                        $quer->where('cmp.user_id', $userId);
                        $quer->orWhere('cmp.charity_id', $userId);
                    })
            ->orderBy('cmp.created_at', 'desc')
            ->lists('cmp.campaign_name', 'cmp.id');
                   // dd(DB::getQueryLog());

    }
      
    public function getAllDonationUsersNoComments($userId, $campaignId, $charityId, $individual_compaign_chk)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('users as user', 'user.id', '=', 'donation.user_id')
            ->join('user_profile as profile', 'profile.user_id', '=', 'donation.user_id')
            ->leftjoin('donation_type as dtype', 'dtype.id', '=', 'donation.donation_type_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->where(function ($que) use ($userId, $campaignId, $charityId, $individual_compaign_chk) {
                if (!empty($userId)) {
                    $que->Where(function ($quer) use ($userId) {
                        $quer->where('donation.user_id', $userId);
                        $quer->orWhere('campaign.user_id', $userId);
                        $quer->orWhere('campaign.charity_id', $userId);
                    });
                }
                if ($individual_compaign_chk) {
                    $que->where('campaign.charity_id', '=', '');
                }
                if ($campaignId != 0) {
                    $que->where('donation.campaign_id', $campaignId);
                }
                if ($charityId != 0) {
                    $que->where('campaign.charity_id', $charityId);
                }
            })
            ->select('donation.id as donationId', 'donation.user_id as donorId',
                'donation.is_fees_paid_by_donor', 'donation.transaction_fee',
                'user.id as userId', 'user.email', 'campaign.id as campaignId',
                'campaign.user_id as recieverId', 'campaign.campaign_name',
                'campaign.slug', 'user.name', 'user.salutation',
                'donation.donation_amount_monetory', 'donation.created_at',
                'donation.is_giftaid', 'profile.slug as userSlug',
                'profile.first_name', 'profile.sur_name',
                'profile.locality as locality', 'profile.address as address',
                'profile.postcode as postcode','dtype.title','donation.is_offline','donation.status'
            )
            ->where('donation.status', 1)
            ->where('user.status', 1)
            ->orderBy('donation.id', 'desc');
        return $query;
    }

    public function getAllDonationUsers($userId, $campaignId, $charityId, $individual_compaign_chk)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('users as user', 'user.id', '=', 'donation.user_id')
            ->join('user_profile as profile', 'profile.user_id', '=', 'donation.user_id')
            ->join('comments as cmt', 'cmt.donation_id', '=', 'donation.id')
            ->leftjoin('donation_type as dtype', 'dtype.id', '=', 'donation.donation_type_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->where(function ($que) use ($userId, $campaignId, $charityId, $individual_compaign_chk) {
                if (!empty($userId)) {
                    $que->Where(function ($quer) use ($userId) {
                        $quer->where('donation.user_id', $userId);
                        $quer->orWhere('campaign.user_id', $userId);
                        $quer->orWhere('campaign.charity_id', $userId);
                    });
                }
                if ($individual_compaign_chk) {
                    $que->where('campaign.charity_id', '=', '');
                }
                if ($campaignId != 0) {
                    $que->where('donation.campaign_id', $campaignId);
                }
                if ($charityId != 0) {
                    $que->where('campaign.charity_id', $charityId);
                }
            })
            ->select('donation.id as donationId', 'donation.user_id as donorId',
                'donation.is_fees_paid_by_donor', 'donation.transaction_fee',
                'user.id as userId', 'user.email', 'campaign.id as campaignId',
                'campaign.user_id as recieverId', 'campaign.campaign_name',
                'campaign.slug', 'user.name', 'user.salutation',
                'donation.donation_amount_monetory', 'donation.created_at',
                'donation.is_giftaid', 'profile.slug as userSlug',
                'profile.first_name', 'profile.sur_name',
                'profile.locality as locality', 'profile.address as address',
                'profile.postcode as postcode', 'cmt.content', 'dtype.title'
            )
            ->where('donation.status', 1)
            ->where('user.status', 1)
            ->orderBy('donation.id', 'desc');
        return $query;
    }

    /**
     * This Function is used to get given donations based on campaignId Or Campaigns Linked to Specific Charity Id.
     * @param type $campaignId
     * @param type $charityId
     */
    public function donationGivenList($userId, $campaignId = 0, $charityId = 0)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('users as user', 'user.id', '=', 'donation.user_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->select('donation.id as donationId', 'donation.user_id', 'user.id as userId', 'campaign.campaign_name', 'campaign.id as campaignId', 'user.name', 'donation.donation_amount_monetory', 'donation.created_at')
            ->where('campaign.user_id', $userId)
            ->where('donation.status', 1)
            ->where('user.status', 1)
            ->orderBy('donation.created_at', 'desc');

        return $query;
    }

    /**
     * This Function is used to get recieved donations based on campaignId Or Campaigns Linked to Specific Charity Id.
     * @param type $campaignId
     * @param type $charityId
     */
    public function donationRecievedList($userId, $campaignId = 0, $charityId = 0)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('users as user', 'user.id', '=', 'donation.user_id')
            ->join('user_profile as profile', 'user.id', '=', 'profile.user_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->select('donation.id as donationId', 'donation.user_id', 'campaign.campaign_name', 'user.id as userId', 'campaign.id as campaignId', 'campaign.campaign_name', 'user.name', 'donation.donation_amount_monetory', 'donation.created_at', 'profile.slug as userSlug')
            ->where(function ($que) use ($campaignId, $charityId) {
                if ($campaignId != 0) {
                    $que->where('donation.campaign_id', $campaignId);
                }
                if ($charityId != 0) {
                    $que->where('campaign.charity_id', $charityId);
                }
            })
            ->where('campaign.user_id', $userId)
            ->where('donation.status', 1)
            ->where('user.status', 1)
            ->orderBy('donation.created_at', 'desc');

        return $query;
    }

    public function latest3Donation($campaignId)
    {

        $query = DB::table('campaign_donations as donation')
            ->leftJoin('users as user', 'user.id', '=', 'donation.user_id')
            ->leftJoin('comments as cmt', 'cmt.donation_id', '=', 'donation.id')
            ->where('donation.campaign_id', $campaignId)
            ->orderBy('donation.id', 'desc')
            ->groupBy('donation.id')
            ->select('donation.is_giftaid', 'donation.donation_amount_monetory', 'donation.created_at as donationTime', 'user.name', 'cmt.content', 'cmt.created_at', 'cmt.is_anonymous', 'donation.is_offline')
            ->paginate(Config('config.latest_donation_pagination'));

        return $query;
    }
    
    public function donationFilterDate($campaignId,$date)
    {

        $query = DB::table('campaign_donations as donation')
            ->leftJoin('users as user', 'user.id', '=', 'donation.user_id')
            ->leftJoin('comments as cmt', 'cmt.donation_id', '=', 'donation.id')
            ->where('donation.campaign_id', $campaignId)
            
            ->where(function ($que) use ($date) {
                if (!empty($date)) {
                    $que->where('donation.created_at','>=', $date);
                }
                
            })
            ->orderBy('donation.id', 'desc')
            ->groupBy('donation.id')
            ->select('donation.is_giftaid', 'donation.donation_amount_monetory', 'donation.created_at as donationTime', 'user.name', 'cmt.content', 'cmt.created_at', 'cmt.is_anonymous', 'donation.is_offline')
          ->get();

        return $query;
    }

    public function latest3Donation1($campaignId)
    {

        $query = DB::table('campaign_donations as donation')
            ->leftJoin('users as user', 'user.id', '=', 'donation.user_id')
            ->leftJoin('comments as cmt', 'cmt.donation_id', '=', 'donation.id')
            ->where('donation.campaign_id', $campaignId)
            ->orderBy('donation.id', 'desc')
            ->groupBy('donation.id')
            ->select('donation.is_giftaid', 'donation.donation_amount_monetory', 'donation.created_at as donationTime', 'user.name', 'cmt.content', 'cmt.created_at', 'cmt.is_anonymous', 'donation.is_offline')
          ->get();

        return $query;
    }

    /**
     * Insert into the user_activation
     * @param array $data
     * @return type
     * author - Harsh
     */
    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    //Saves the donation details after stripe payment to keep the address details
    //Safe if the user changes his address  
    public function saveCurrentUserDetails($donationId,$userId){

        //Get user address details
        $data = DB::table('user_profile')
            ->where('user_profile.user_id', $userId)
            ->select('user_profile.house_number','user_profile.street_name','user_profile.addition_to_address','user_profile.address','user_profile.postcode','user_profile.locality','user_profile.country')
          ->get();
        
        $id = DB::table('campaign_donation_details')->insertGetId([
            
            'donation_id'  => $donationId, 
            'user_id'      => $userId,
            'postcode'     => $data[0]->postcode,
            'house_number' => $data[0]->house_number,
            'addition_to_address' => $data[0]->addition_to_address,
            'address'  => $data[0]->address,
            'locality' => $data[0]->locality,
            'country'  => $data[0]->country
            
        ]); 

        return $id; 

    }

    public function getUserCampaigns($userId)
    {
        $campaignModel = new CampaignModel();
        return $campaignModel
            ->leftjoin('campaign_donations', function ($join) {
                $join->on(
                    'campaign_donations.campaign_id', '=', 'campaigns.id'
                );
            })
            ->where('campaigns.stage', '5')
            ->where(function ($que) use ($userId) {
                $que->where('campaign_donations.user_id', $userId);
                $que->orWhere('campaigns.user_id', '=', $userId);
                $que->orWhere('campaigns.charity_id', '=', $userId);
            })
            ->distinct('campaigns.id')
            ->lists('campaigns.campaign_name', 'campaigns.id')->toArray();
        //  return CampaignModel::where('user_id', $userId)->orWhere('charity_id', $userId)->lists('campaign_name', 'id')->toArray();
    }

    public function getCampaignByUser($userId,$campaignId)
    {
          $campaignModel = new CampaignModel();
                       // echo $campaignId;
            if($campaignId ==0){
                $res = $campaignModel->leftjoin('campaign_donations', function ($join) {
                $join->on('campaign_donations.campaign_id', '=', 'campaigns.id');
            })
                ->where(['campaigns.user_id'=> $userId,'campaigns.status'=>1])->orWhere('campaigns.charity_id', $userId)
                ->lists('campaigns.campaign_name', 'campaigns.id')->toArray();
               return $res;     
               }
            $res = $campaignModel->leftjoin('campaign_donations', function ($join) {
                $join->on('campaign_donations.campaign_id', '=', 'campaigns.id');
            })
                ->where(['campaigns.user_id'=> $userId,'campaigns.status'=>1,'campaigns.id'=>$campaignId])//->orWhere('campaigns.charity_id', $userId)
                   // ->groupBy('campaigns.campaign_name')
                ->lists('campaigns.campaign_name', 'campaigns.id')->toArray();
//print_r($res);die;
        
return $res; 
    }

    public function getUserCampaignLinkedCharity($userId)
    {
        $userProfileModel = new UserProfileModel();
        return $userProfileModel
            ->join('user_activation', function ($join) {
                $join->on(
                    'user_activation.user_id', '=', 'user_profile.user_id'
                );
            })
            ->where('user_activation.is_activated', '=', 1)
            ->lists('user_profile.charity_name', 'user_profile.user_id')->toArray();
    }

    public function findTotalDonorsByCampaignId($campaignId)
    {
        return DB::table('campaign_donations')
            ->where('campaign_donations.campaign_id', $campaignId)
            ->groupBy('campaign_donations.user_id')
            ->get();
    }

    public function findTotalDonorsCountByCampaignId($campaignId)
    {
        $res = DB::table('campaign_donations')
            ->where('campaign_donations.campaign_id', $campaignId)
            ->count();
        return $res;
    }

    public function findTotalAmountRaisedForCampaignId($campaignId)
    {
        return DB::table('campaign_donations')
            ->select(DB::raw('sum(campaign_donations.donation_amount_monetory) as totalAmountRaised'))
            ->where('campaign_donations.campaign_id', $campaignId)
            ->get();
    }

    /**
     *
     */
    public function getNewestDonationByCampaignAndUser($campaignId, $userId)
    {
        return $this->getModel()->where('campaign_id', $campaignId)->where('user_id', $userId)->orderBy('created_at', 'desc')->first();
    }

    public function findTotalAmountRaisedByMultipleCampaignIds($campaignIds)
    {
        return DB::table('campaign_donations')
            ->select(DB::raw('sum(campaign_donations.donation_amount_monetory) as totalAmountRaised'))
            ->whereIn('campaign_donations.campaign_id', $campaignIds)
            ->groupBy('campaign_donations.campaign_id')
            ->get();
    }

    /**
     * Function to get all donations on admin
     * @param none
     * @return type
     * @author himanshi
     */
    public function getAllDonationOnAdmin($from = 0, $to = 0)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->join('users', 'users.id', '=', 'donation.user_id')
            ->select('donation.id', 'users.name', 'campaign.campaign_name', 'donation.donation_amount_monetory', 'donation.created_at', 'donation.is_giftaid')
            ->where('donation.status', 1)
            ->where(function ($que) use ($from, $to) {
                if ($from && $to) {
                    $que->whereBetween('donation.created_at', array($from, $to));
                }
            })
            ->orderBy('donation.created_at', 'desc');
        return $query;
    }

    /**
     * This Function is used to get given donations based on campaignId Or Campaigns Linked to Specific Charity Id.
     * @param type $campaignId
     * @param type $charityId
     * @author himanshi
     */
    public function donationAdminGivenList($userId, $from = 0, $to = 0)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('users as user', 'user.id', '=', 'donation.user_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->select('donation.id as donationId', 'donation.user_id', 'user.id as userId', 'campaign.campaign_name', 'campaign.id as campaignId', 'user.name', 'donation.donation_amount_monetory', 'donation.created_at')
            ->where('campaign.user_id', $userId)
            ->where('donation.status', 1)
            ->where(function ($que) use ($from, $to) {
                if ($from && $to) {
                    $que->whereBetween('donation.created_at', array($from, $to));
                }
            })
            ->orderBy('donation.created_at', 'desc');
        return $query;
    }

    /**
     * This Function is used to get recieved donations based on campaignId Or Campaigns Linked to Specific Charity Id.
     * @param type $campaignId
     * @param type $charityId
     * @author himanshi
     */
    public function donationAdminRecievedList($userId, $from = 0, $to = 0)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('users as user', 'user.id', '=', 'donation.user_id')
            ->join('user_profile as profile', 'user.id', '=', 'profile.user_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->select('donation.id as donationId', 'donation.user_id', 'campaign.campaign_name', 'user.id as userId', 'campaign.id as campaignId', 'campaign.campaign_name', 'user.name', 'donation.donation_amount_monetory', 'donation.created_at', 'profile.slug as userSlug')
            ->where(function ($que) use ($from, $to) {
                if ($from && $to) {
                    $que->whereBetween('donation.created_at', array($from, $to));
                }
            })
            ->where('campaign.user_id', $userId)
            ->where('donation.status', 1)
            ->orderBy('donation.created_at', 'desc');

        return $query;
    }

    /**
     * Function to get All mdonations
     * @params none
     * @return type
     * @author himanshi
     */
    public function findAll()
    {
        return DonationModel::all();
    }

    public function findByDonarOnActiveCampaign($id)
    {
        $query = DB::table('campaign_donations as donation')
            ->join('campaigns as campaign', 'campaign.id', '=', 'donation.campaign_id')
            ->select('donation.donation_amount_monetory')
            ->where(function ($que) use ($id) {
                $que->where('donation.user_id', $id);
                $que->where('campaign.stage', 5);
                $que->where('campaign.status', 1);
            });

        return $query->get();
    }

    public function DonationPerDay($userId)
    {
        $query = DB::select('SELECT SUM(campaign_donations.donation_amount_monetory) AS total_inday, DATE_FORMAT(created_at, \'%y-%m-%d\') as Date FROM campaign_donations WHERE user_id = ? AND created_at > (CURDATE() - INTERVAL 1 MONTH) GROUP BY (Date)', [$userId]);
        return $query;
    }
    /*
    * Update
    * Storing the donation details along with latest address 
    */
    public function saveDonationCurrentDetails($data){

        return DB::table('campaign_donation_details')->insertGetId($data);
        

    }

}
