<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Dashboard\Entities;

use Pingpong\Presenters\Model;

class Donation extends Model
{

    protected $table = 'campaign_donations';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'campaign_id',
        'donation_amount_time',
        'donation_amount_monetory',
        'status',
        'application_fees_charge',
        'donation_type_id',
        'is_giftaid',
        'is_offline',
        'is_fees_paid_by_donor',
        'transaction_fee',
    ];

}
