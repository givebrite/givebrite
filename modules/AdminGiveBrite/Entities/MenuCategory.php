<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Admingivebrite\Entities;

use Pingpong\Presenters\Model;

class MenuCategory extends Model
{
    protected $table = 'menu_categories';
    
   /**
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'status',
        'position',
        'created_at',
        'updated_at'
    ];
}