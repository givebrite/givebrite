<?php

namespace Modules\Admingivebrite\Entities;

use Pingpong\Presenters\Model;

class CampaignCharity extends Model
{
    protected $table = 'campaign_charities';
    
   /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'charity_logo',
        'registration_no',
        'charity_name',
        'address',
        'post_code',
        'contact_telephone',
        'web',
        'end_date',
        'created_at',
        'updated_at',
    ];
    
  
    
}
