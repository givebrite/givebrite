<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Admingivebrite\Entities;

use Pingpong\Presenters\Model;

class Menus extends Model
{
    protected $table = 'menus';
    
   /**
     * @var array
     */
    protected $fillable = [
        'cat_id',
        'title',
        'slug',
        'link',
        'position',
        'status',
        'created_at',
        'updated_at'
    ];
}