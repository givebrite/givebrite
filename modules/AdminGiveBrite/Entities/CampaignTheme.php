<?php

namespace Modules\Admingivebrite\Entities;

use Pingpong\Presenters\Model;

class CampaignTheme extends Model
{
    protected $table = 'campaign_themes';
    
   /**
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
        'created_at',
        'updated_at',
    ];
    
    
}
