<?php

namespace Modules\Admingivebrite\Entities;

use Pingpong\Presenters\Model;

class Banner extends Model
{
    protected $table = 'banners';
    
   /**
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'cta_type',
        'cta',
        'position',
        'status',
        'created_at',
        'updated_at'
    ];

    

    
}
