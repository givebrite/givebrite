<?php

namespace Modules\Admingivebrite\Entities;

use Pingpong\Presenters\Model;

class CampaignLocation extends Model{
    
    protected $table = 'campaign_locations';
    
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id'
    ];
    
    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeNewest($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * @param $query
     * @param $id
     *
     * @return mixed
     */
    public function scopeBySlugOrId($query, $id)
    {
        return $query->whereId($id)->orWhere('slug', '=', $id);
    }
}
