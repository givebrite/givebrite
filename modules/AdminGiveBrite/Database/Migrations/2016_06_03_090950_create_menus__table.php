<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')
                    ->references('id')->on('menu_categories')
                    ->onDelete('cascade');
            $table->string('title');
            $table->string('slug');
            $table->text('link');
            $table->integer('position');
            $table->integer('status')->default(1);           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }

}
