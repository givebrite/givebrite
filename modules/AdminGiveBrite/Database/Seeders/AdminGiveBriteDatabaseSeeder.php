<?php namespace Modules\Admingivebrite\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdminGiveBriteDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		 $this->call("AdminArticleTableSeeder");
		 $this->call("AdminMenuCategoryTableSeeder");
//		 $this->call("AdminArticleTableSeeder");
	}

}
