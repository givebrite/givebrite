<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Modules\AdminGiveBrite\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginAdminMiddleware', 'App\Http\Middleware\IsUnsecure']], function () {
    Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminGiveBriteController@index']);
//     Route::get('/users/{id}',['as' => 'admin.users.show', 'uses' => 'UsersController@show'] );

    Route::put('/users/{id}/edit-user', [
        'as' => 'admin.users.update',
        'uses' => 'UsersController@userUpdate'
    ]);

    Route::delete('/users/{id}/destroy-user', [
        'as' => 'admin.users.destroy',
        'uses' => 'UsersController@userDestroy'
    ]);

    Route::get('/users/{id}', [
        'as' => 'user-detail',
        'uses' => 'UsersController@userShow'
    ]);

    Route::get('/users', [
        'as' => 'admin.users.index',
        'uses' => 'UsersController@users'
    ]);


    Route::get('/charity-users', [
        'as' => 'charity-users-list',
        'uses' => 'UsersController@charityUsers'
    ]);

    Route::get('/charity-user-deactivate/{userId}', [
        'as' => 'charity-user-deactivate',
        'uses' => 'UsersController@deactivateUserStatus'
    ]);

    Route::get('/charity-user-activate/{userId}', [
        'as' => 'charity-user-activate',
        'uses' => 'UsersController@activateUserStatus'
    ]);


});

Route::group(['prefix' => 'admingivebrite', 'namespace' => 'Modules\AdminGiveBrite\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginAdminMiddleware', 'App\Http\Middleware\IsUnsecure']], function () {
    Route::get('/', 'AdminGiveBriteController@index');

    /* Category routes starts */
    Route::get('/campaign-categories', [
        'as' => 'campaign-categories',
        'uses' => 'CampaignCategoriesController@index'
    ]);

    Route::get('/campaign-category-create', [
        'as' => 'campaign-category-create',
        'uses' => 'CampaignCategoriesController@create'
    ]);

    Route::post('/campaign-category-store', [
        'as' => 'campaigncategory.store',
        'uses' => 'CampaignCategoriesController@store'
    ]);

    Route::get('/campaign-category-edit/{id}', [
        'as' => 'campaign-category-edit',
        'uses' => 'CampaignCategoriesController@edit'
    ]);

    Route::put('/campaign-category-update/{id}', [
        'as' => 'campaigncategory.update',
        'uses' => 'CampaignCategoriesController@update'
    ]);

    Route::DELETE('/campaign-category-delete/{id}', [

        'as' => 'campaign-category-delete',
        'uses' => 'CampaignCategoriesController@destroy'
    ]);

    /* Category routes ends */

    /* Campaign routes starts */
    Route::get('/campaign-list', [
        'as' => 'campaign-list',
        'uses' => 'CampaignsController@index'
    ]);


    Route::get('/users/campaigns/{id}', [
        'as' => 'campaign-users',
        'uses' => 'CampaignsController@userCampaigns'
    ]);

    Route::get('/users/campaign-status/{id}/{status}/{userid}', [
        'as' => 'campaign-status',
        'uses' => 'CampaignsController@campaignStatus'
    ]);

    Route::get('/users/campaign-delete/{id}', [
        'as' => 'campaign-delete',
        'uses' => 'CampaignsController@deleteCampaign'
    ]);

    /* Campaign routes ends */

    /* Location routes starts */
    Route::get('/campaign-locations', [
        'as' => 'campaign-locations',
        'uses' => 'CampaignLocationsController@index'
    ]);
    Route::get('/campaign/location/create', [
        'as' => 'location-create',
        'uses' => 'CampaignLocationsController@create'
    ]);
    Route::get('/campaign/location/edit/{id}', [
        'as' => 'campaign-location-edit',
        'uses' => 'CampaignLocationsController@edit'
    ]);
    Route::post('/campaign-location-store', [
        'as' => 'campaignlocation.store',
        'uses' => 'CampaignLocationsController@store'
    ]);
    Route::put('/campaign-location-update/{id}', [
        'as' => 'campaignlocation.update',
        'uses' => 'CampaignLocationsController@update'
    ]);
    Route::DELETE('/campaign-location-delete/{id}', [
        'as' => 'campaign-location-delete',
        'uses' => 'CampaignLocationsController@destroy'
    ]);
    /* Location routes ends */


    /* Campaign Themes routes starts */
    Route::get('/campaign-themes', [
        'as' => 'campaign-themes',
        'uses' => 'CampaignThemesController@index'
    ]);
    Route::get('/campaign/theme/create', [
        'as' => 'campaign-theme-create',
        'uses' => 'CampaignThemesController@create'
    ]);
    Route::get('/campaign/theme/edit/{id}', [
        'as' => 'campaign-theme-edit',
        'uses' => 'CampaignThemesController@edit'
    ]);
    Route::post('/campaign-theme-store', [
        'as' => 'campaigntheme.store',
        'uses' => 'CampaignThemesController@store'
    ]);
    Route::put('/campaign-theme-update/{id}', [
        'as' => 'campaigntheme.update',
        'uses' => 'CampaignThemesController@update'
    ]);
    Route::get('/campaign-theme-delete/{id}', [
        'as' => 'campaign-theme-delete',
        'uses' => 'CampaignThemesController@destroy'
    ]);
    /* Campaign Design routes ends */
    Route::get('/campaign-charity-list', [
        'as' => 'campaign-charity-list',
        'uses' => 'CampaignsController@getAllCampaignCharities'
    ]);

    Route::get('/campaign-reward-list', [
        'as' => 'campaign-reward-list',
        'uses' => 'CampaignRewardsController@index'
    ]);


    /* Location routes starts */
    Route::get('/campaign-donations', [
        'as' => 'campaign-donations',
        'uses' => 'CampaignDonationsController@index'
    ]);
    Route::get('/receive/donation/{id}', [
        'as' => 'user-received-donations',
        'uses' => 'CampaignDonationsController@userReceiveDonations'
    ]);
    Route::get('/users/donations/{id}', [
        'as' => 'user-given-donations',
        'uses' => 'CampaignDonationsController@userGivenDonations'
    ]);
    Route::get('/users/searchdonations/{id}', [
        'as' => 'search-campaigns-donations',
        'uses' => 'CampaignDonationsController@index'
    ]);

    /* Location routes ends */

    /* Menu routes starts */

    Route::get('/menu-categories', [
        'as' => 'menu-categories',
        'uses' => 'MenuCategoriesController@index'
    ]);

    Route::get('/menu-category-create', [
        'as' => 'menucategory.create',
        'uses' => 'MenuCategoriesController@create'
    ]);

    Route::post('/menu-category-store', [
        'as' => 'menucategory.store',
        'uses' => 'MenuCategoriesController@store'
    ]);

    Route::get('/menu-category-edit/{id}', [
        'as' => 'menu-category-edit',
        'uses' => 'MenuCategoriesController@edit'
    ]);

    Route::put('/menu-category-update/{id}', [
        'as' => 'menucategory.update',
        'uses' => 'MenuCategoriesController@update'
    ]);

    Route::delete('/menu-category-delete/{id}', [
        'as' => 'admin.menu-category.destroy',
        'uses' => 'MenuCategoriesController@destroy'
    ]);

    Route::get('/menus', [
        'as' => 'menus-index',
        'uses' => 'MenuController@index'
    ]);

    Route::get('/menus/create', [
        'as' => 'menus.create',
        'uses' => 'MenuController@create'
    ]);

    Route::post('/menus/store', [
        'as' => 'menus.store',
        'uses' => 'MenuController@store'
    ]);

    Route::get('/menus/edit/{id}', [
        'as' => 'menus.edit',
        'uses' => 'MenuController@edit'
    ]);

    Route::put('/menus/update/{id}', [
        'as' => 'menus.update',
        'uses' => 'MenuController@update'
    ]);

    Route::delete('/menus/delete/{id}', [
        'as' => 'admin.menus.destroy',
        'uses' => 'MenuController@destroy'
    ]);

    Route::get('/content', [
        'as' => 'content.index',
        'uses' => 'ContentController@index'
    ]);

    Route::get('/content/create', [
        'as' => 'content.create',
        'uses' => 'ContentController@create'
    ]);

    Route::post('/content/store', [
        'as' => 'content.store',
        'uses' => 'ContentController@store'
    ]);

    Route::get('/content/edit/{id}', [
        'as' => 'content.edit',
        'uses' => 'ContentController@edit'
    ]);

    Route::put('/content/update/{id}', [
        'as' => 'content.update',
        'uses' => 'ContentController@update'
    ]);

    Route::delete('/content/delete/{id}', [
        'as' => 'admin.content.destroy',
        'uses' => 'ContentController@destroy'
    ]);

    /* Menu routes ends */

    /** User block route */
    Route::get('/users', [
        'as' => 'users-list',
        'uses' => 'AdminGiveBriteController@users'
    ]);

    /** User block route */
    Route::get('/users/{id}', [
        'as' => 'user-detail',
        'uses' => 'AdminGiveBriteController@userShow'
    ]);

    Route::get('/user-status/{id}/{status}', [
        'as' => 'user-status',
        'uses' => 'AdminGiveBriteController@userStatusUpdate'
    ]);

    // Banner ManagMent
    Route::get('/update_status/{bannerId}', [
        'as' => 'banner.update.status',
        'uses' => 'BannerController@updateStatus'
    ]);
    Route::resource('banners', 'BannerController');
});
