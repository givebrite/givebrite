<?php namespace Modules\Admingivebrite\Requests;

use App\Http\Requests\Request;

use Input;

class BannerUpdateRequest extends Request{
    
    /**
     * determine if the user is authroized or not
     * 
     * @param type none
     */
    public function authorize() {
        return true;
    }
    
     /**
     * Get the validations
     * 
     * @return string
     */
    public function rules() {
        $rules = [
            'name' => 'required|max:255',
            'cta' => 'url',
            'image'=>'image'
        ];
        return $rules;
    }
    
    public function messages(){
         $messages = [
            'name.required' => 'Banner name is required.',
            'cta.url' => 'URL format is not valid.',
            'cta.required' => 'URL is required.',
            'image.image'=>'Image format is mot valid.',
            'image.required'=>'Image is required.'
        ];
        return $messages;
    }
}

