<?php namespace Modules\Admingivebrite\Requests;

use App\Http\Requests\Request;

use Input;

class MenusRequest extends Request{
    
    /**
     * determine if the user is authroized or not
     * 
     * @param type none
     */
    public function authorize() {
        return true;
    }
    
    /**
     * Get the validations
     * 
     * @return string
     */
    public function rules() {
        
        $segments = $this->segments();
        $id = intval(end($segments));
        
        $rules = [
            'title' => 'required',
            'slug' => 'required|unique:menus'.($id ? ",id,$id" : ''),
            'cat_id' => 'required',
            'position' => 'required',
            'link' =>'required'
        ];
        return $rules;
    }
}

