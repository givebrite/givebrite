<?php namespace Modules\Admingivebrite\Requests;

use App\Http\Requests\Request;

use Input;

class CampaignLocationCreateRequest extends Request{
    
    /**
     * determine if the user is authroized or not
     * 
     * @param type none
     */
    public function authorize() {
        return true;
    }
    
    /**
     * Get the validations
     * 
     * @return string
     */
    public function rules() {
        $rules = [
            'name' => 'required|max:255'
        ];
        return $rules;
    }
}

