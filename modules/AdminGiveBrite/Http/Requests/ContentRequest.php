<?php namespace Modules\Admingivebrite\Requests;

use App\Http\Requests\Request;

use Input;

class ContentRequest extends Request{
    
    /**
     * determine if the user is authroized or not
     * 
     * @param type none
     */
    public function authorize() {
        return true;
    }
    
    /**
     * Get the validations
     * 
     * @return string
     */
    public function rules($id = null) {
        
        $segments = $this->segments();
        $id = intval(end($segments));

        $rules = [
            'title' => 'required',
            'slug' => 'required|unique:contents'.($id ? ",id,$id" : '')
        ];
        return $rules;
    }
}

