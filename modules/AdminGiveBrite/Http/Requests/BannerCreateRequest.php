<?php namespace Modules\Admingivebrite\Requests;

use App\Http\Requests\Request;

use Input;

class BannerCreateRequest extends Request{
    
    /**
     * determine if the user is authroized or not
     * 
     * @param type none
     */
    public function authorize() {
        return true;
    }
    
    /**
     * Get the validations
     * 
     * @return string
     */
    public function rules() {
        $rules = [
            'name' => 'required|max:255',
            'cta' => 'url',
            'image'=>'required|image'
        ];
        return $rules;
    }
    
    public function messages(){
        $messages = [
            'name.required' => 'Banner name is required.',
            'cta.url' => 'URL format is mot valid.',
            'cta.required' => 'URL is required.',
            'image.image'=>'Image format is not valid.',
            'image.required'=>'Image is required.'
        ];
        return $messages;
    }
}

