<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Repositories\MenuCategoryRepository as MenuCategoryRepo;
use Modules\Admingivebrite\Requests\MenuCategoryRequest as MenuCategoryRequest;
use View;
use Redirect;
Use Config;

class MenuCategoriesController extends Controller {

    protected $categories;

    function __construct(MenuCategoryRepo $menuCategory) {
        $this->categories = $menuCategory;
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return Redirect::route('menu-categories');
    }

    
    /**
     * Display index
     *
     * @return Response
     */
    public function index() {
        $positions = Config::get('config.menu_positions');
        $categories = $this->categories->selectAll(
                Config('config.paginationperpage')
                );
        return view('admingivebrite::menucategories.index', compact('categories', 'positions'));
    }

    
    /**
     * Create a new menu category
     *
     * @return Response
     */
    public function create() {
        
        $positions = Config::get('config.menu_positions');
        
        return view('admingivebrite::menucategories.create', compact('positions'));
    }

    
    /**
     * Store a newly created menu in storage.
     *
     * @return Response
     */
    public function store(MenuCategoryRequest $request) {

        try {
            $categoryData = $request->only('title', 'slug', 'position');
            $category = $this->categories->create($categoryData);
            
            if (isset($category->id) && !empty($category) && !empty($category->id)) {
                return Redirect::route('menu-category-edit', $category->id)
                                ->withFlashMessage('Category is successfully created');
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }

        return view('admingivebrite::menucategories.create');
    }

    
    /**
     * Show the form for editing the specified menu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        try {
            $positions = Config::get('config.menu_positions');
            $model = $this->categories->find($id);
            return view('admingivebrite::menucategories.edit', compact('category', 'model', 'positions'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    
    /**
     * Update the specified menu in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(MenuCategoryRequest $request, $id) {

        try {
            $model = $this->categories->find($id);

            if (isset($model) && !empty($model)) {
                $categoryData = $request->only('title', 'slug', 'position');
                if ($this->categories->update($categoryData, $id)) {
                    return Redirect::route('menu-category-edit', $id)
                                    ->withFlashMessage('Category is successfully updated');
                }
            }

            return Redirect::route('menu-category-edit', $category->id)
                            ->withFlashMessage('Category is not updated');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id) {
        try {

            $this->categories->delete($id);

            return Redirect::route('menu-categories')
                            ->withFlashMessage('Category is successfully deleted');
            
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

}
