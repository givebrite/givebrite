<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
Use Modules\Auth\Entities\User as UserModel;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Modules\Admingivebrite\Repositories\EloquentBannerRepository as BannerRepo;
use Redirect;
use Auth;
use Illuminate\Support\Facades\Session;
use Modules\Auth\Repositories\Users\EloquentUserActivationRepository as UserActivationRepo;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Modules\Auth\Repositories\Roles\EloquentRoleRepository as RoleRepo;
use Modules\Auth\Entities\UserProfile as UserProfileModel;
use Mail;
use Illuminate\Http\Request;
use Pingpong\Admin\Validation\User\Update;


class AdminGiveBriteController extends Controller {

    public function index() {
         //get logged in user required info
        $this->setLoggedInUserInfo(Auth::user()->id);
        //count campains
        $campaignRepo = new CampaignRepo();
        $campaigns = $campaignRepo->findAll()->count();
        //count donations
        $CampaignDonationRepo = new DonationRepo();
        $donations = $CampaignDonationRepo->findAll()->count();
      
        //count donations
        $BannerRepo = new BannerRepo();
        $banners = $BannerRepo->findAll()->count();
        return view('admin::index', compact('campaigns', 'donations','banners'));
    }

  
    /**
     * Function to update user status
     * @param type $id
     * @param type $status
     * @return none
     */
    public function userStatusUpdate($id, $status) {
        $userActivation = new UserActivationRepo();
        $status = ($status == 1 ? "0" : '1' );       
        $user_profile_model = new RoleUserRepo();
        $userRepo = new UserRepo();
        $user_profile_response = $user_profile_model->findByUserId($id);
        
        $user = $userRepo->findById($id);
        $userProfile = UserProfileModel::where('user_id',$id)->first();
        
        $email = $user->email;
        $name = $userProfile->charity_name;
        $subject = "Start Receiving Donations";
        $user_slug = (!empty($userProfile)) ? $userProfile->slug : "";
        $role_id = $user_profile_response->role_id;       
        $user_role_name = ($role_id == 3 ? "charity" : 'user' );       
        $message = ($status ? "Activated the $user_role_name successfully" : "Blocked the $user_role_name succesfully" );
        // Send Mail To Charity For Stripe Connect
        if($role_id == 3 && $status == 1){
        
        //creates UserActivation model and find all entries from the user_activation table
        $user_active_users = $userActivation->findAll();

        //get random verification token
        $confirmation_code = str_random(30);

        //check if verification token already exists, regenerate if already exists
        if ($user_active_users) {
            foreach ($user_active_users as $user_active_user) {
                while ($user_active_user->token_code === $confirmation_code) {
                    $confirmation_code = str_random(30);
                }
            }
        }
        
        //creates user activation data array
        $user_activation_data = array(
            'user_id' => $id,
            'token_code' => $confirmation_code,
            'is_activated' => 1
        );

        //loop through all entries of user_activation table
        foreach ($user_active_users as $user_active_user) {
            //check if user with some verification token already exists
            if ($user_active_user->user_id == $id) {

                //delete if any entries are already in the table
                $userActivation->delete($user_active_user->id);
            }
        }

        //creates user activation entry in the user_activation table
        $user_active_response = $userActivation->create($user_activation_data);

        //creates token string with verification token and user-email
        $confirmation_code_string = $confirmation_code . '&&' . md5($email);
        // Send Mail To Charity
        Mail::send('auth::email.connect_stripe', array('user_slug' => $user_slug, 'url' => url().'/stripe_connect','confirmation_code_string'=>$confirmation_code_string), function($message)use($email,$name, $subject){
            $message->to($email, $name)->subject($subject);
        });
            
        }
        $userRepo->updateStatusById($status, $id);
        $userActivation->updateByUserId($status, $id);
        return redirect('admingivebrite/users')
                        ->withFlashMessage($message);
    }

    /**
     * Function to get Individual User result along with role
     * @param type $id
     * @param type $status
     * @return none
     */
    public function users() { 
        $userRepo = new UserRepo();
        $users = $userRepo->getAllUserDetailsByRoles('2');
        
        $no = $users->firstItem();
        
        return view('admin::users.index',  compact('users', 'no'));
    }

    
     /**
     * set all user info of a logged in user into an array
     * @param type $user_id
     * @return type array
     */
    public function setLoggedInUserInfo($user_id)
    {
        //creates User model and find user from the users table by id
        $user_model = new UserRepo();
        $user_response = $user_model->findById($user_id);

        //creates user_session data array
        $user_session = array(
            'id' => $user_id,
            'email' => $user_response->email,
            'user_name' => $user_response->name,
            'active' => $this->isActive($user_id)
        );

        //creates User Profile model and find user's profile data from the user_profile table by id
        $user_profile_model = new UserProfileRepo();
        $user_profile_response = $user_profile_model->findByUserId($user_id);

        $user_session['charity_name'] = (!empty($user_profile_response->charity_name)) ? $user_profile_response->charity_name : "";
        $user_session['first_name'] = (!empty($user_profile_response->first_name)) ? $user_profile_response->first_name : "";
        $user_session['sur_name'] = (!empty($user_profile_response->sur_name)) ? $user_profile_response->sur_name : "";
        $user_session['recieve_updates'] = (!empty($user_profile_response->recieve_updates)) ? $user_profile_response->recieve_updates : "";
        $user_session['slug'] = (!empty($user_profile_response->slug)) ? $user_profile_response->slug : "";
        
        $role_user_model = new RoleUserRepo();
        $role_user_response = $role_user_model->findByUserId($user_id);


        $role_model = new RoleRepo();
        $role_response = $role_model->findById($role_user_response->role_id);

        $user_session['role'] = $role_response->name;
        
        Session::set('user_session_data', $user_session);
    }
    
     /**
     * Check if registered user has verified its account
     * @param type $user_id
     * @return boolean
     */
    public function isActive($user_id)
    {
        $user_activation_model = new UserActivationRepo();
        $user_active_response = $user_activation_model->findByUserId($user_id);
        if($user_active_response)
        {
        if ($user_active_response->is_activated == 1) {
            return true;
        }
        }
            return false;
    }
}
