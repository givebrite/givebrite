<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Entities\CampaignTheme as CampaignThemeModel;
use Modules\Admingivebrite\Repositories\EloquentCampaignThemeRepository as CampaignThemeRepo;
use Modules\Admingivebrite\Requests\CampaignThemeCreateRequest as CampaignThemeCreateRequest;
use Modules\Admingivebrite\Requests\CampaignThemeUpdateRequest as CampaignThemeUpdateRequest;
use View;
use Input;
use Redirect;

class CampaignThemesController extends Controller {

  
    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index() {
        $campaignRepo = new CampaignThemeRepo();
        $campaignTheme = $campaignRepo->findAll();
        return view('admingivebrite::campaignthemes.index', compact('campaignTheme'));
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create() {
        return view('admingivebrite::campaignthemes.create');
    }

    /**
     * Store a newly created campaign in storage.
     *
     * @return Response
     */
    public function store(CampaignThemeCreateRequest $request) {
        $campaignThemeData = $request->only('name', 'value');
  
        // Creating Campaign Repository Instance
        $campaignThemeRepo = new CampaignThemeRepo();
        $saveTheme = $campaignThemeRepo->create($campaignThemeData);      
        return Redirect::route('campaign-theme-edit', $saveTheme->id)
                        ->withMessage('Theme Created Successfully.');
    }

    /**
     * Show the form for editing the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
    
        // Creating Campaign Repository Instance
        $campaignThemeRepo = new CampaignThemeRepo();
        try {
            $model = $campaignThemeRepo->findById($id);
            return view('admingivebrite::campaignthemes.edit', compact('model'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

      /**
     * Update the specified campaign in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignThemeUpdateRequest $request, $id) {
        $campaignThemeRepo = new CampaignThemeRepo();
        try {
            $location = $campaignThemeRepo->findById($id);
            $campaignThemeData = $request->only('name','value');
            $location->update($campaignThemeData);            
            return Redirect::route('campaign-theme-edit', $location->id)
                            ->withMessage('Theme Updated Successfully.');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show() {
        return view('campaigns::index');
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $campaignThemeRepo = new CampaignThemeRepo();
        try {
            $campaignThemeRepo->delete($id);
            return Redirect::route('campaign-themes');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
        //return view('campaigns::index');
    }

 

}
