<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Repositories\ContentRepository as ContentRepo;
use Modules\Admingivebrite\Requests\ContentRequest as ContentRequest;
use View;
use Redirect;
use Config;

class ContentController extends Controller {

    protected $content;

    function __construct(ContentRepo $content) {
        $this->content = $content;
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return Redirect::route('content.index');
    }

    /**
     * Display index
     *
     * @return Response
     */
    public function index() {
        $content = $this->content->selectAll(100);     
        return view('admingivebrite::content.index', compact('content'));
    }

    /**
     * Create a new menu category
     *
     * @return Response
     */
    public function create() {
        return view('admingivebrite::content.create');
    }

    /**
     * Store a newly created menu in storage.
     *
     * @return Response
     */
    public function store(ContentRequest $request) {

        try {
            $categoryData = $request->only('title', 'slug', 'description');            
            $category = $this->content->create($categoryData);

            if (isset($category->id) && !empty($category) && !empty($category->id)) {
                return Redirect::route('content.edit', $category->id)
                                ->withFlashMessage('Article is successfully created');
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }

        return view('admingivebrite::content.create');
    }

    /**
     * Show the form for editing the specified menu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        try {
            $model = $this->content->find($id);
            return view('admingivebrite::content.edit', compact('category', 'model'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified menu in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(ContentRequest $request, $id) {

        try {
            $model = $this->content->find($id);

            if (isset($model) && !empty($model)) {
                $categoryData = $request->only('title', 'slug', 'description');

                if ($this->content->update($categoryData, $id)) {
                    return Redirect::route('content.edit', $id)
                                    ->withFlashMessage('Article is successfully updated');
                }
            }

            return Redirect::route('content.edit', $category->id)
                            ->withFlashMessage('Article is not updated');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id) {
        try {

            $this->content->delete($id);

            return Redirect::route('content.index')
                            ->withFlashMessage('Article is successfully deleted');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

}
