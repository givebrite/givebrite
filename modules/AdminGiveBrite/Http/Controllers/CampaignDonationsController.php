<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Auth;
use View;
use Input;
use Redirect;

class CampaignDonationsController extends Controller {

   

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return view('campaigns::errors.404');
    }

     /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index() {      
        $donationRepo = new DonationRepo(); 
        $from = !empty(trim(Input::get('from'))) ? date("Y-m-d",  strtotime(trim(Input::get('from')))) : 0; 
        $to = !empty(trim(Input::get('to'))) ? date("Y-m-d",  strtotime(trim(Input::get('to')))): 0;
        $campaignDonation = $donationRepo->getAllDonationOnAdmin($from,$to)->paginate(config('config.paginationperpage'));       
        return view("admingivebrite::campaigndonations.index",compact('campaignDonation','from','to'));
    }
    
    
    
    /**
     * Display a listing of user given donations
     *
     * @return Response
     */
    public function userGivenDonations($userId) {
       
        $donationRepo = new DonationRepo();  
        $from = !empty(trim(Input::get('from'))) ? date("Y-m-d",  strtotime(trim(Input::get('from')))) : 0; 
        $to = !empty(trim(Input::get('to'))) ? date("Y-m-d",  strtotime(trim(Input::get('to')))): 0;
        $type = "given";
        $campaignDonation = $donationRepo->donationAdminGivenList($userId,$from,$to)->paginate(config('config.paginationperpage'));                              
        return view('admingivebrite::campaigndonations.userdonation', compact('userId','from','to','campaignDonation','type'));
    }

    /**
     * Display a listing of user received donations
     *
     * @return Response
     */
    public function userReceiveDonations($userId) {
       
        $donationRepo = new DonationRepo();       
        $from = !empty(trim(Input::get('from'))) ? trim(Input::get('from')) : 0;
        $to = !empty(trim(Input::get('to'))) ? trim(Input::get('to')) : 0;
        $type = "receive";
        $campaignDonation = $donationRepo->donationAdminRecievedList($userId,$from,$to)->paginate(config('config.paginationperpage'));       
                         
        return view('admingivebrite::campaigndonations.userdonation', compact('userId','from','to','campaignDonation','type'));
    }


}
