<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Repositories\EloquentBannerRepository as BannerRepo;
use Modules\Admingivebrite\Requests\BannerCreateRequest as BannerCreateRequest;
use Modules\Admingivebrite\Requests\BannerUpdateRequest as BannerUpdateRequest;
use View;
use Redirect;
use Config;
use Helper;

class BannerController extends Controller
{

    /**
     * @var ImageUploader
     */
    protected $uploader;
    protected $imageStoragePath;
    protected $bannerRepo;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader, BannerRepo $banner)
    {
        $this->uploader = $uploader;
        $this->imageStoragePath = Config::get('config.banner_upload_path');
        $this->bannerRepo = $banner;
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return Redirect::route('banner.index');
    }

    public function setup()
    {
        $bannerPositionsUsed = $this->bannerRepo->findUsedPostions();
        $postionArray = ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'];
        $bannerPositionsAvailable = array_diff($postionArray, $bannerPositionsUsed);
        View::share('bannerPositionsAvailable', $bannerPositionsAvailable);
    }

    /**
     * Display index
     *
     * @return Response
     */
    public function index()
    {
        $this->setup();
        $banners = $this->bannerRepo->findAll();
        return view('admingivebrite::banner.index', compact('banners', 'bannerPositionsAvailable'));
    }

    /**
     * Create a new menu category
     *
     * @return Response
     */
    public function create()
    {

        $this->setup();
        return view('admingivebrite::banner.create', compact('bannerPositionsAvailable'));
    }

    /**
     * Store a newly created menu in storage.
     *
     * @return Response
     */
    public function store(BannerCreateRequest $request)
    {
        try {
            $bannerData = $request->only('name', 'image', 'cta_type', 'cta', 'position', 'status');
            // Upload Files
            $bannerPositionsUsed = $this->bannerRepo->findUsedPostions();
            unset($bannerData['image']);

            if (\Input::hasFile('image')) {
                $image = $this->uploader->upload('image');
                $image->save($this->imageStoragePath);
                $bannerData['image'] = $image->getFilename();
                
                $image_detail = array(
                    'inputFile' => Input::file('image'),
                    'image' => $image,
                    'thumb_size' => Config::get('config.thumb_banner_size'),
                    'thumb_path' => Config::get('config.thumb_banner_upload_path'),
                    'large_size' => Config::get('config.large_banner_size'),
                    'large_path' => Config::get('config.large_banner_upload_path')
                );
                Helper::createThumb($image_detail);
            }
            // Check For Add Banner Condition
            if ($this->canAddBanner()) {
                // Check If Position is free
                if(in_array($bannerData['position'], $bannerPositionsUsed)){
                    return Redirect::route('admingivebrite.banners.create')
                                ->withFlashMessage('You can not create two banner for same position')
                                ->withFlashType('danger');
                }
                $banner = $this->bannerRepo->create($bannerData);
                if (isset($banner->id) && !empty($banner) && !empty($banner->id)) {
                    return Redirect::route('admingivebrite.banners.edit', $banner->id)
                                    ->withFlashMessage('Banner is successfully created');
                }
            } else {
                return Redirect::route('admingivebrite.banners.create')
                                ->withFlashMessage('Admin can not add more than 5 banners.')
                                ->withFlashType('danger');
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }

        return view('admingivebrite::banner.create');
    }

    /**
     * Show the form for editing the specified menu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        try {
            $bannerPositionsUsed = $this->bannerRepo->findUsedPostions($id);
            $postionArray = ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'];
            $bannerPositionsAvailable = array_diff($postionArray, $bannerPositionsUsed);
            $banner = $this->bannerRepo->findById($id);
            return view('admingivebrite::banner.create', compact('banner', 'bannerPositionsAvailable'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified menu in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(BannerUpdateRequest $request, $id)
    {
        try {
            $banner = $this->bannerRepo->findById($id);
            // Upload Files
            $bannerPositionsUsed = $this->bannerRepo->findUsedPostions();
            if (isset($banner) && !empty($banner)) {
                $bannerData = $request->only('name', 'image', 'cta_type', 'cta', 'position', 'status');
                // Upload Files
                unset($bannerData['image']);

                if (\Input::hasFile('image')) {
                    // upload image
                    $image = $this->uploader->upload('image');
                    $image->save($this->imageStoragePath);
                    $bannerData['image'] = $image->getFilename();
                    
                    $image_detail = array(
                        'inputFile' => Input::file('image'),
                        'image' => $image,
                        'thumb_size' => Config::get('config.thumb_banner_size'),
                        'thumb_path' => Config::get('config.thumb_banner_upload_path'),
                        'large_size' => Config::get('config.large_banner_size'),
                        'large_path' => Config::get('config.large_banner_upload_path')
                    );
                    
                    Helper::createThumb($image_detail);
                }
                // Check If Position is free
                if(in_array($bannerData['position'], $bannerPositionsUsed)){
                    unset($bannerData['position']);
                }
                if ($this->bannerRepo->update($bannerData, $id)) {
                    return Redirect::route('admingivebrite.banners.edit', $id)
                                    ->withFlashMessage('Banner is successfully updated');
                }
            }

            return Redirect::route('admingivebrite.banners.edit', $id)
                            ->withFlashMessage('Banner is not updated');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            // Check For Add Banner Condition
            if ($this->canUpdateStatusOrDestroyBanner()) {
                $this->bannerRepo->delete($id);
                return Redirect::route('admingivebrite.banners.index')
                                ->withFlashMessage('Banners is successfully deleted');
            } else {
                return Redirect::route('admingivebrite.banners.index')
                                ->withFlashMessage('Admin can not delete last banner.')
                                ->withFlashType('danger');
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    public function updateStatus($id)
    {
        try {

            $banner = $this->bannerRepo->findById($id);
            // Upload Files
            $bannerPositionsUsed = $this->bannerRepo->findUsedPostions();
            if (!empty($banner->status) && $banner->status == 'yes') {
                if ($this->canUpdateStatusOrDestroyBanner()) {
                    $data['status'] = 'no';
                } else {
                    return Redirect::route('admingivebrite.banners.index')
                                    ->withFlashMessage('Admin can not unpublish last banner.')
                                    ->withFlashType('danger');
                }
            } else {
                $data['status'] = 'yes';
                // Check If Position is free
                if(in_array($banner->position, $bannerPositionsUsed)){
                    return Redirect::route('admingivebrite.banners.index')
                                ->withFlashMessage('You can not create two banner for same position')
                                ->withFlashType('danger');
                }
            }
            
            if ($this->bannerRepo->update($data, $id)) {
                return Redirect::route('admingivebrite.banners.index')
                                ->withFlashMessage('Banners is successfully updates');
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    public function canAddBanner()
    {
        $banners = $this->bannerRepo->findActiveBanners();
        if (!empty($banners) && count($banners) == 5) {
            return false;
        } else {
            return true;
        }
    }

    public function canUpdateStatusOrDestroyBanner()
    {
        $banners = $this->bannerRepo->findActiveBanners();
        if (!empty($banners) && count($banners) <= 1) {
            return false;
        } else {
            return true;
        }
    }

//    public function createThumb($image)
//    {
//        $thumb_image = $image;
//        $large_image = $image;
//
//        $resized_thumb_image = Helper::resizeImage($thumb_image->image, Config::get('config.thumb_charity_size'));
//        if ($resized_thumb_image) {
//            $thumb_image->save(Config::get('config.thumb_charity_logo_upload_path'));
//        }
//        $resized_large_image = Helper::resizeImage($large_image->image, Config::get('config.large_charity_size'));
//        if ($resized_large_image) {
//            $large_image->save(Config::get('config.large_charity_logo_upload_path'));
//        }
//    }
}
