<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Repositories\MenuCategoryRepository as MenuCategoryRepo;
use Modules\Admingivebrite\Repositories\MenusRepository as MenusRepo;
use Modules\Admingivebrite\Requests\MenusRequest as MenusRequest;
use View;
use Redirect;
use Config;

class MenuController extends Controller {

    protected $categories,
            $menus;

    function __construct(MenuCategoryRepo $menuCategory, MenusRepo $menus) {
        $this->categories = $menuCategory;
        $this->menus = $menus;
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return Redirect::route('menus-index');
    }

    
    /**
     * Display index
     *
     * @return null
     */
    public function index() {
        $menus = $this->menus->selectJoinCategory(
                100
                );     
                
              
       return view('admingivebrite::menus.index', compact('menus'));
    }

    
    /**
     * Create a new menu
     *
     * @return Response
     */
    public function create() {        
        
        $categories = $this->categories->selectAll();
        
        return view('admingivebrite::menus.create', compact('categories'));
    }

    /**
     * Store a newly created menu in storage.
     *
     * @return Response
     */
    public function store(MenusRequest $request) {

        try {
            $menuData = $request->only('cat_id', 'title', 'slug', 'link', 'position', 'status');
            $menu = $this->menus->create($menuData);

            if (isset($menu->id) && !empty($menu) && !empty($menu->id)) {
                return Redirect::route('menus.edit', $menu->id)
                                ->withFlashMessage('Menu is successfully created');
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }

        return view('admingivebrite::menus.create');
    }

    /**
     * Show the form for editing the specified menu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        try {
            $categories = $this->categories->selectAll();
            $model = $this->menus->find($id);
            return view('admingivebrite::menus.edit', compact('categories', 'model'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified menu in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(MenusRequest $request, $id) {

        try {
            $model = $this->menus->find($id);

            if (isset($model) && !empty($model)) {

                $menuData = $request->only('cat_id', 'title', 'slug', 'link', 'position', 'status');
                
                if ($this->menus->update($menuData, $id)) {
                    return Redirect::route('menus.edit', $id)
                                ->withFlashMessage('Menu is successfully updated');
                }
            }

            return Redirect::route('menus.edit', $id)
                                ->withFlashMessage('Menu is not updated');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }
    
        
    /**
     * Remove the specified user from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            
            $this->menus->delete($id);

            return Redirect::route('menus-index')
                            ->withFlashMessage('Menu is successfuly deleted');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

}
