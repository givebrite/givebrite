<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
Use Modules\Auth\Entities\User as UserModel;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Modules\Admingivebrite\Repositories\EloquentBannerRepository as BannerRepo;
use Redirect;
use Auth;
use Illuminate\Support\Facades\Session;
use Modules\Auth\Repositories\Users\EloquentUserActivationRepository as UserActivationRepo;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Modules\Auth\Repositories\Roles\EloquentRoleRepository as RoleRepo;
use Modules\Auth\Entities\UserProfile as UserProfileModel;
use Mail;
use Illuminate\Http\Request;
use Pingpong\Admin\Validation\User\Update;

class UsersController extends Controller
{


    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return $this->redirect('users.index');
    }

    /**
     * Function to get Individual User result along with role
     * @param type $id
     * @param type $status
     * @return none
     */
    public function users()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $userRepo = new UserRepo();
        
        if($search != NULL)
        {
            $users = $userRepo->findByEmailLike($search, '2');

        }
        else
        {
            $users = $userRepo->getAllUserDetailsByRoles('2');
        }
        $no = $users->firstItem();

        return view('admin::users.index', compact('users', 'no'));
    }

    public function deactivateUserStatus($userId)
    {
        $status = 0;
        $userActivationRepo = new UserActivationRepo();
        $result = $userActivationRepo->updateByUserId($status, $userId);
        if ($result) {
            return Redirect::back();
        }

    }

    public function activateUserStatus($userId)
    {
        $status = 1;
        $userModel = new UserRepo();
        $res = $userModel->findById($userId);
        $userActivationRepo = new UserActivationRepo();
        $result = $userActivationRepo->updateByUserId($status, $userId);
        if ($result) {
            $url = url() . "/charity/" . $res->slug;
            Mail::send('admin::email.charity_connect', ['url' => $url], function ($m) use ($res) {
                $m->from('info@givebrite.com', 'GiveBrite');

                $m->to($res->email)->subject('Charity Activated');
            });
            return Redirect::back();
        }
    }

    /**
     * Function to get charity User result along with role
     * @param type $id
     * @param type $status
     * @return none
     */
    public function charityUsers()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $userRepo = new UserRepo();
        
        if($search != NULL)
        {
            //echo 'here';
            $users = $userRepo->findByEmailLike($search, '3');
            //echo '<pre>'; print_r($users); die;
        }
        else
        {
            $users = $userRepo->getAllUserDetailsByRoles('3');
        }
        $no = $users->firstItem();

        return view('admin::users.index', compact('users', 'no'));
    }


    public function userShow($id)
    {
        try {
            $userRepo = new UserRepo();
            $user = $userRepo->findById($id);

            $userProfileRepo = new UserProfileRepo();
            $profile = $userProfileRepo->findByUserId($id);
            return view('admin::users.show', compact('user', 'profile'));

        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified user in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function userUpdate(Update $request, $id)
    {
        try {
            $data = !$request->has('password') ? $request->except('password') : $request->all();

            $user = UserModel::find($id);
            $user->update($data);

            $user->roles()->sync((array)\Input::get('role'));

            if (\Input::get('role') == '3') {
                return redirect('admin/charity-users');
            }

            return redirect('admin/users');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Destroy charity user.
     *
     * @param int $id
     *
     * @return Response
     */
    public function userDestroy($id)
    {
        try {
            $user_model = new UserRepo();
            $user = $user_model->findById($id);
            $userSlug = $user->slug;

            $userModel = UserModel::destroy($id);

            if ($userSlug == 'charity') {
                return redirect('admin/charity-users');
            } else {
                return redirect('admin/users');
            }
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Show the form for creating a new user.
     *
     * @return Response
     */
//    public function create()
//    {
//        return $this->view('users.create');
//    }

    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
//    public function store(Create $request)
//    {
//        $data = $request->all();
//
//        $user = $this->repository->create($data);
//
//        $user->addRole($request->get('role'));
//
//        return $this->redirect('users.index');
//    }

    /**
     * Display the specified user.
     *
     * @param int $id
     *
     * @return Response
     */
//    public function show($id)
//    {
//        try {
//            $user = $this->repository->findById($id);
//
//            return $this->view('users.show', compact('user'));
//        } catch (ModelNotFoundException $e) {
//            return $this->redirectNotFound();
//        }
//    }

    /**
     * Show the form for editing the specified user.
     *
     * @param int $id
     *
     * @return Response
     */
//    public function edit($id)
//    {
//        try {
//            $user = $this->repository->findById($id);
//
//            $role = $user->roles->lists('id');
//
//            return $this->view('users.edit', compact('user', 'role'));
//        } catch (ModelNotFoundException $e) {
//            return $this->redirectNotFound();
//        }
//    }

    /**
     * Update the specified user in storage.
     *
     * @param int $id
     *
     * @return Response
     */
//    public function update(Update $request, $id)
//    {
//        try {
//            $data = !$request->has('password') ? $request->except('password') : $this->inputAll();
//
//            $user = $this->repository->findById($id);
//
//            $user->update($data);
//
//            $user->roles()->sync((array) \Input::get('role'));
//
//            return $this->redirect('users.index');
//        } catch (ModelNotFoundException $e) {
//            return $this->redirectNotFound();
//        }
//    }

    /**
     * Remove the specified user from storage.
     *
     * @param int $id
     *
     * @return Response
     */
//    public function destroy($id)
//    {
//        try {
//            $this->repository->delete($id);
//
//            return $this->redirect('users.index');
//        } catch (ModelNotFoundException $e) {
//            return $this->redirectNotFound();
//        }
//    }
}
