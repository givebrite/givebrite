<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Repositories\EloquentCampaignLocationRepository as CampaignLocationRepo;
use Modules\Admingivebrite\Requests\CampaignLocationCreateRequest as CampaignLocationCreateRequest;
use Modules\Admingivebrite\Requests\CampaignLocationUpdateRequest as CampaignLocationUpdateRequest;

use View;
use Input;
use Redirect;


class CampaignLocationsController extends Controller {

   

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index() {
        $CampaignLocationRepo = new CampaignLocationRepo();
       // $campaignList = $campaignRepo->joinWithCharity('campaign_charities');
        $campaignLocation = $CampaignLocationRepo->findAllLocation()->paginate(config('config.paginationperpage'));  
        return view('admingivebrite::campaignlocations.index', compact('campaignLocation'));
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create() {
        $CampaignLocationRepo = new CampaignLocationRepo();
        $location = $CampaignLocationRepo->getModel()->where('parent_id','=','0')->lists('name','id');
        $location =  $location->toArray();
       // echo "<prE>";print_r($location);die;
        return view('admingivebrite::campaignlocations.create',compact('location'));
    }
 
   
    /**
     * Store a newly created campaign in storage.
     *
     * @return Response
     */
    public function store(CampaignLocationCreateRequest $request) {
        $campaignLocationData = $request->only('name','parent_id');
        // Creating Campaign Repository Instance
        $campaignLocaRepo = new CampaignLocationRepo();
        $saveLocation = $campaignLocaRepo->create($campaignLocationData);
        return Redirect::route('campaign-location-edit',$saveLocation->id)
                        ->withFlashMessage('Campaign Location Created Successfully.')
                        ->withFlashType('success');
    }

    /**
     * Show the form for editing the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        // Creating Campaign Repository Instance
        $campaignLocationRepo = new CampaignLocationRepo();
        $checkLocation =  $this->checkLocation($id);
        $page = !empty(trim(Input::get('page'))) ? Input::get('page') : 1;
        if($checkLocation){ 
            return Redirect::route('campaign-locations',array('page'=>$page))
                        ->withFlashMessage('You cannot edit this.  This location is already used in campaign designs')
                        ->withFlashType('warning');
            
        } 
        $location = $campaignLocationRepo->getModel()->where('parent_id','=','0')->lists('name','id');
        $location =  $location->toArray();        
        try {
            $model = $campaignLocationRepo->findById($id); 
            return view('admingivebrite::campaignlocations.edit', compact('model','location'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

      /**
     * Update the specified campaign in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignLocationUpdateRequest $request, $id) {
        $campaignLocationRepo = new CampaignLocationRepo();
        try {
            $location = $campaignLocationRepo->findById($id);
            $campaignLocationData = $request->only('name','parent_id');
            $location->update($campaignLocationData);            
            return Redirect::route('campaign-location-edit', $location->id)
                            ->withFlashMessage('Location Updated Successfully.');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }


    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show() {
        return view('campaigns::index');
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $campaignLocationRepo = new CampaignLocationRepo();
        $checkLocation =  $this->checkLocation($id);
        $page = !empty(trim(Input::get('page'))) ? Input::get('page') : 1;
        if($checkLocation){ 
            return Redirect::route('campaign-locations',array('page'=>$page))
                    ->withFlashMessage('You cannot delete this. This location is already used in campaign designs')
                    ->withFlashType('warning');
        }
        try {
            $campaignLocationRepo->delete($id);
            return Redirect::route('campaign-locations',array('page'=>$page));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
        //return view('campaigns::index');
    }


     /**
     * Function to check categories
     * @param type $id
     * @return type
     */
    public function checkLocation($id){
        $campaignLocationRepo = new CampaignLocationRepo();
        $checkLocation  = $campaignLocationRepo->findDesionsByLocation($id);      
        return $checkLocation;      
    }

}
