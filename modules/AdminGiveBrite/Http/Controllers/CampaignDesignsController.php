<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Entities\Campaign as CampaignModel;
use Modules\Admingivebrite\Entities\CampaignCharity as CampaignCharityModel;
use Modules\Admingivebrite\Entities\CampaignDesign as CampaignDesignModel;
use Modules\Admingivebrite\Entities\CampaignReward as CampaignRewardModel;
use Modules\Admingivebrite\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Admingivebrite\Repositories\EloquentCampaignCharityRepository as CampaignCharityRepo;
use Modules\Admingivebrite\Repositories\EloquentCampaignRewardRepository as CampaignRewardRepo;
use Modules\Admingivebrite\Repositories\EloquentCampaignDesignRepository as CampaignDesignRepo;
use Modules\Admingivebrite\Requests\CampaignDetailCreateRequest as CampaignDetailCreateRequest;
use Modules\Admingivebrite\Requests\CampaignDetailUpdateRequest as CampaignDetailUpdateRequest;
use View;
use Input;
use Redirect;

class CampaignDesignsController extends Controller {

    /**
     * @var ImageUploader
     */
    protected $uploader;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader) {
        $this->uploader = $uploader;
    }

    /**
     * Function to set variable for views
     * 
     * @author AK 03/10/16
     */
    public function setUp() {
        $categories = ['cat1', 'cat2', 'cat3', 'cat4'];
        //Share variable to view
        View::share('categories', $categories);
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index() {
        $campaignRepo = new CampaignDesignRepo();
        $campaignDesign = $campaignRepo->findAll();
        return view('admingivebrite::campaigndesigns.index', compact('campaignDesign'));
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create() {
        $this->setUp();
        return view('campaigns::create', compact('categories'));
    }

    /**
     * Store a newly created campaign in storage.
     *
     * @return Response
     */
    public function store(CampaignDetailCreateRequest $request) {
        $userId = \Auth::id(); //Changed By Login User Id
        $campaignDetailData = $request->only('campaign_name', 'goal_amount_type', 'goal_amount_time', 'goal_amount_monetary', 'campaign_type', 'campaign_category', 'registered_charity', 'end_date', 'charity_id');
        $campaignDetailData['user_id'] = $userId;
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $saveCampaign = $campaignRepo->create($campaignDetailData);
        if (!empty($saveCampaign->id) && !empty($campaignDetailData['registered_charity']) && $campaignDetailData['registered_charity'] == 'yes') {
            $CampaignCharityData = $request->only('charity_logo', 'registration_no', 'charity_name', 'address', 'post_code', 'contact_telephone', 'web');
            unset($CampaignCharityData['charity_logo']);
            if (\Input::hasFile('charity_logo')) {
                // upload image
                $this->uploader->upload('charity_logo')->save('images/campaigns');
                $CampaignCharityData['charity_logo'] = $this->uploader->getFilename();
            }
            //Add Campaign Id To Campaign Charity Data
            $CampaignCharityData['campaign_id'] = $saveCampaign->id;
            // Creating Campaign Charity Repository Instance
            $campaignCharityRepo = new CampaignCharityRepo();
            $saveCampaignCharity = $campaignCharityRepo->create($CampaignCharityData);
        }


        return Redirect::route('campaign-edit', $saveCampaign->id)
                        ->withMessage('Campaign Created Successfully.');
    }

    /**
     * Show the form for editing the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        $this->setUp();
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        try {
            $model = $campaignRepo->findById($id);
            $campaignCharityRepo = new CampaignCharityRepo();
            $campaignCharity = $campaignCharityRepo->findByCampaignId($model->id);
            return view('admingivebrite::campaigns.edit', compact('categories', 'model', 'campaignCharity'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified campaign in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignDetailUpdateRequest $request, $id) {
        // Creating Campaign Repository Instance
        $userId = \Auth::id(); //Changed By Login User Id
        $campaignRepo = new CampaignRepo();
        try {
            $campaign = $campaignRepo->findById($id);
            $campaignDetailData = $request->only('campaign_name', 'goal_amount_type', 'goal_amount_time', 'goal_amount_monetary', 'campaign_type', 'campaign_category', 'end_date', 'registered_charity', 'charity_id');
            $campaignDetailData['user_id'] = $userId;
            $campaign->update($campaignDetailData);
            if (!empty($campaign->id) && !empty($campaignDetailData['registered_charity']) && $campaignDetailData['registered_charity'] == 'yes') {
                $CampaignCharityData = $request->only('charity_logo', 'registration_no', 'charity_name', 'address', 'post_code', 'contact_telephone', 'web');
                unset($CampaignCharityData['charity_logo']);
                if (\Input::hasFile('charity_logo')) {
                    // upload image
                    $this->uploader->upload('charity_logo')->save('images/campaigns');
                    $CampaignCharityData['charity_logo'] = $this->uploader->getFilename();
                }
                //Add Campaign Id To Campaign Charity Data
                $CampaignCharityData['campaign_id'] = $campaign->id;
                // Creating Campaign Charity Model Instance
                $campaignCharity = CampaignCharityModel::firstOrNew(['campaign_id' => $campaign->id]);
                if (!empty($campaignCharity->id)) {
                    $saveCampaignCharity = $campaignCharity->update($CampaignCharityData);
                } else {
                    $saveCampaignCharity = $campaignCharity->create($CampaignCharityData);
                }
            }
            return Redirect::route('campaign-edit', $campaign->id)
                            ->withMessage('Campaign Updated Successfully.');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show() {
        return view('campaigns::index');
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $campaignRepo = new CampaignRepo();
        try {
            $campaignRepo->delete($id);
            return Redirect::route('campaign-list');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
        //return view('campaigns::index');
    }

    /**
     * Function to get all campaign charities list
     * 
     * @param none
     * @return Response
     */
    public function getAllCampaignCharities() {

        $campaignCharity = new CampaignCharityRepo();
        //  $campaignCharityList =  $campaignCharity->findAll();
        $campaignCharityList = $campaignCharity->joinWithCampaign();
        return view('admingivebrite::campaigns.charitylist', compact('campaignCharityList'));
    }

    /**
     * Function to get all campaign rewards
     * @params none
     * @return Response
     */
    public function getAllCampaignRewards() {

        $campaignReward = new CampaignCharityRepo();
        $campaignRewardList = $campaignReward->findAll();
        return view('admingivebrite::campaigns.rewardlist', compact('campaignRewardList'));
    }

}
