<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;

use View;
use Input;
use Redirect;

class CampaignsController extends Controller {

   
  
    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index() 
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';
        $campaignRepo = new CampaignRepo();
        // $campaignList = $campaignRepo->joinWithCharity('campaign_charities');
        if($search != NULL)
        {
            $campaignList = $campaignRepo->getCampaignsByName($search);

        }
        else
        {
            $campaignList = $campaignRepo->getAllCampaigns();
        }
             
        return view('admingivebrite::campaigns.index', compact('campaignList'));
    }


   
    
    public function userCampaigns($user_id){
        $campaignRepo = new CampaignRepo();
        $campaignList = $campaignRepo->getCampaignsByUser($user_id);            
        return view('admingivebrite::campaigns.index', compact('campaignList'));
    }
    
    /**
     * Function to update campaign status
     * @param type $id
     * @param type $status
     * @return type
     */
    public function campaignStatus($id,$status,$userid=0) {
        $status = ($status== 1 ? 0 : 1 ); 
        $campaignRepo = new CampaignRepo();
        $campaignRepo->campaignActivation($id,$status);   
        if($userid){
            $returnUrl = "admingivebrite/users/campaigns/$userid";
        }
        else{
            $returnUrl = "admingivebrite/campaign-list";
        }
        return redirect($returnUrl)
               ->withFlashMessage('Successfully Updated');
                   
    }

    
//    public function disableCampaign($campaignId) {
//        $campaignObj = new CampaignRepo();
//        $res = $campaignObj->campaignActivation($campaignId, 0);
//        if ($res) {
//            Toastr::success("Campaign paused successfully", $title = null, $options = []);
//        }
//        return redirect()->back();
//    }

    public function deleteCampaign($id) {
        $campaignObj = new CampaignRepo();
        $res = $campaignObj->delCampaign($id);
        
        if ($res) {
            $message = "Campaign Deleted Successfully.";
        } else {
            $message = "Opps! this campaign can not be deleted as it has Donations.";
        }
        
        return redirect()->back()->withFlashMessage($message);
    }
}
