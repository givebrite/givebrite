<?php

namespace Modules\Admingivebrite\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admingivebrite\Entities\CampaignCategory as CampaignCategoryModel;
use Modules\Admingivebrite\Repositories\EloquentCampaignCategoryRepository as CampaignCategoryRepo;
use Modules\Admingivebrite\Requests\CampaignCategoryCreateRequest as CampaignCategoryCreateRequest;
use Modules\Admingivebrite\Requests\CampaignCategoryUpdateRequest as CampaignCategoryUpdateRequest;

use View;
use Input;
use Redirect;

class CampaignCategoriesController extends Controller {


    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound() {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index() { 
        $campaignCategoryRepo = new CampaignCategoryRepo();
       // $campaignList = $campaignRepo->joinWithCharity('campaign_charities');
        $campaignCategory = $campaignCategoryRepo->getModel()->paginate(config('config.paginationperpage'));       
        return view('admingivebrite::campaigncategories.index', compact('campaignCategory'));
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create() {
        return view('admingivebrite::campaigncategories.create');
    }

    /**
     * Store a newly created campaign in storage.
     *
     * @return Response
     */
    public function store(CampaignCategoryCreateRequest $request) {
        $campaignCategoryData = $request->only('name','slug');
        // Creating Campaign Repository Instance
        $campaignCategoryRepo = new CampaignCategoryRepo();
        $saveCategory = $campaignCategoryRepo->create($campaignCategoryData);
        return Redirect::route('campaign-category-edit',$saveCategory->id)
                        ->withFlashMessage('Campaign  Category Created Successfully.')
                        ->withFlashType('success');
    }

    /**
     * Show the form for editing the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        // Creating Campaign Repository Instance
        $campaignCategoryRepo = new CampaignCategoryRepo();  
        $checkCategory =  $this->checkCategory($id);
        $page = !empty(trim(Input::get('page'))) ? Input::get('page') : 1;
        if($checkCategory){ 
            return Redirect::route('campaign-categories', array('page' => $page))
                        ->withFlashMessage('You cannot edit this. This category is already used in campaign')
                        ->withFlashType('warning');
        }               
        try {
            $model = $campaignCategoryRepo->findById($id);
            return view('admingivebrite::campaigncategories.edit', compact('model'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }


     /**
     * Update the specified campaign in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignCategoryUpdateRequest $request, $id) {
        $campaignCategoryRepo = new CampaignCategoryRepo();
        try {
            $category = $campaignCategoryRepo->findById($id);
            $campaignCategoryData = $request->only('name','slug');
            $category->update($campaignCategoryData);            
            return Redirect::route('campaign-category-edit', $category->id)
                            ->withFlashMessage('You cannot delete this. This category is already used in campaign')
                            ->withFlashType('warning');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    

    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show() {
        return view('campaigns::index');
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $campaignCategoryRepo = new CampaignCategoryRepo();
        $checkCategory =  $this->checkCategory($id);
        $page = !empty(trim(Input::get('page'))) ? Input::get('page') : 1;
        if($checkCategory){ 
            return Redirect::route('campaign-categories', array('page' => $page))
                        ->withFlashMessage('You cannot edit this. This category is already used in campaign')
                        ->withFlashType('warning');
        } 
        try {
            $campaignCategoryRepo->delete($id);
            return Redirect::route('campaign-categories', array('page' => $page));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
        //return view('campaigns::index');
    }

    /**
     * Function to check categories
     * @param type $id
     * @return type
     */
    public function checkCategory($id){
        $campaignCategoryRepo = new CampaignCategoryRepo();
        $checkCategory  = $campaignCategoryRepo->findCampaignByCategory($id);      
        return $checkCategory;      
    }


}
