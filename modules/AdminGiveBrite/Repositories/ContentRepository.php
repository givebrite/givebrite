<?php
namespace Modules\Admingivebrite\Repositories;

use Modules\Admingivebrite\Entities\Content as ContentModel;

class ContentRepository implements MenuInterface {
        
    private function getModel() {
        return new ContentModel();
    }
    
    public function create(array $data) {
        return $this->getModel()->create($data);
    }

    public function find($id) {
        return $this->getModel()->findOrFail($id);
    }

    public function findByUrl($url) {
        
        $query = $this->getModel();
        if (isset($url) && !empty($url)) {
            $query = $query->where('slug', $url);
        }
        
        return $query->first();
    }
    
    public function selectAll($paginate = null) {
        
        $query = $this->getModel();
        if (isset($paginate) && !empty($paginate)) {
           $query = $query->select('*')
                        ->orderBy('id', 'DESC')
                        ->paginate($paginate) ;
        } else {
            $query = $query->all();
        }       
           
        return $query;
    }

    public function update(array $data, $id) {
        $content = $this->find($id);
        
        if (isset($content) && !empty($content)) {
            $content->update($data);            
            return $content->id;
        }
        
        return false;
    }
    
    public function delete($id) {
        $content = $this->find($id);
        
        if (isset($content) && !empty($content)) {
            
            if ($content->delete($id)) {
                return true;
            }                       
        }
        
        return false;
    }   

}
