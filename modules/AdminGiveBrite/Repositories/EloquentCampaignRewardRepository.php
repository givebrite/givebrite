<?php

namespace Modules\Admingivebrite\Repositories;
use Modules\Admingivebrite\Entities\CampaignReward as CampaignRewardModel;

class EloquentCampaignRewardRepository
{
    public function getModel()
    {
        return new CampaignRewardModel();
    }
    
    public function findByCampaignId($campaignId){
        return $this->getModel()->where('campaign_id', $campaignId)->first();
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
    
    public function findAll(){
        return CampaignRewardModel::all();
    }
}
