<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Admingivebrite\Repositories;

use Modules\Admingivebrite\Entities\MenuCategory as MenuCategoryModel;

class MenuCategoryRepository implements MenuInterface {

    private function getModel() {
        return new MenuCategoryModel();
    }

    public function create(array $data) {
        return $this->getModel()->create($data);
    }

    public function find($id) {
        return $this->getModel()->findOrFail($id);
    }

    public function selectAll($paginate = null) {

        $query = $this->getModel();
        if (isset($paginate) && !empty($paginate)) {
            $query = $query->select('*')
                    ->orderBy('id', 'DESC')
                    ->paginate($paginate);
        } else {
            $query = $query->all();
        }

        return $query;
    }

    public function update(array $data, $id) {
        $menuCategory = $this->find($id);

        if (isset($menuCategory) && !empty($menuCategory)) {
            $menuCategory->update($data);
            return $menuCategory->id;
        }

        return false;
    }

    public function delete($id) {
        $menuCategory = $this->find($id);

        if (isset($menuCategory) && !empty($menuCategory)) {

            if ($menuCategory->delete($id)) {
                return true;
            }
        }

        return false;
    }

}
