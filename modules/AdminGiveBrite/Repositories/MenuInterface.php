<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Modules\Admingivebrite\Repositories;

interface MenuInterface {
    
    public function selectAll();

    public function find($id);        
    
    public function create(array $data);
    
    public function update(array $data, $id);
}