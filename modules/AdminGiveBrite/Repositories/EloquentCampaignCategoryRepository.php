<?php

namespace Modules\Admingivebrite\Repositories;

use Modules\Admingivebrite\Entities\CampaignCategory as CampaignCategoryModel;
use DB;

class EloquentCampaignCategoryRepository {

    public function getModel() {
        return new CampaignCategoryModel();
    }

    public function findById($id) {
        return CampaignCategoryModel::findOrFail($id);
    }

    public function create(array $data) {
        return $this->getModel()->create($data);
    }

    public function findAll() {
        return CampaignCategoryModel::all();
    }

    public function delete($id){
        return CampaignCategoryModel::destroy($id);
    }
    
    /**
     * Function for finding maximum amount raised campaigns of charity and for charity
     * @param type $id
     * @param type $charity_id
     * @return type
     */
    public function findCampaignByCategory($id)
    {
        $query = DB::table('campaigns')
                 ->where('campaign_category','=', $id)
                 ->lists('campaign_category');
        return $query;
    }
 

}
