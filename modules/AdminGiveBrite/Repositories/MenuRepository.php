<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Admingivebrite\Repositories;

use Modules\Admingivebrite\Entities\Menus as MenusModel;

class MenusRepository implements MenuInterface {

	private function getModel() {
		return new MenusModel();
	}

	public function create(array $data) {
		return $this->getModel()->create($data);
	}

	public function find($id) {
		return $this->getModel()->findOrFail($id);
	}

	public function selectAll($data = null) {

		$query = $this->getModel();
		if ($data) {
			$query = $query->leftJoin('menu_categories', function ($join) {
				$join->on(
					'menus.cat_id', '=', 'menu_categories.id'
				);
			})->where('menu_categories.position', $data)
				->where('menu_categories.status', 1)
				->where('menus.status', 1)
				->orderBy('menus.position', 'asc')
				->get(['menus.*']);
		} else {
			$query = $query->all();
		}

		return $query;
	}

	public function update(array $data, $id) {
		$menus = $this->find($id);

		if (isset($menus) && !empty($menus)) {
			$menus->update($data);
			return $menus->id;
		}

		return false;
	}

	public function selectJoinCategory($paginate = null) {

		$query = $this->getModel()
			->leftJoin('menu_categories', function ($join) {
				$join->on(
					'menus.cat_id', '=', 'menu_categories.id'
				);
			});
		if (isset($paginate) && !empty($paginate)) {
			$query = $query
				->select([
					'menus.*',
					'menu_categories.title as cat_title',
				])
				->orderBy('id', 'DESC')
				->paginate($paginate);
		} else {
			$query = $query->get([
				'menus.*',
				'menu_categories.title as cat_title',
			]);
		}

		return $query;
	}

	public function delete($id) {
		$menus = $this->find($id);

		if (isset($menus) && !empty($menus)) {

			if ($menus->delete($id)) {
				return true;
			}
		}

		return false;
	}

}
