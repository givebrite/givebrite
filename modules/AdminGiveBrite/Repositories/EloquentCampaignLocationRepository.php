<?php

namespace Modules\Admingivebrite\Repositories;

use Modules\Admingivebrite\Entities\CampaignLocation as CampaignLocationModel;
use DB;

class EloquentCampaignLocationRepository {

    public function getModel() {
        return new CampaignLocationModel();
    }

    public function findById($id) {
        return CampaignLocationModel::findOrFail($id);
    }

    public function create(array $data) {
        return $this->getModel()->create($data);
    }

    public function findAll() {
        return CampaignLocationModel::all();
    }

    public function delete($id){
        return CampaignLocationModel::destroy($id);
    }
    
    public function findAllLocation(){
        $query = DB::table('campaign_locations as a')
                 ->leftjoin('campaign_locations as b','a.parent_id','=','b.id')
                 ->select('a.*','b.name as parent_name')  ;                           
        return $query;               
    }
    
     /**
     * Function for finding maximum amount raised campaigns of charity and for charity
     * @param type $id
     * @param type $charity_id
     * @return type
     */
    public function findDesionsByLocation($id)
    {
        $query = DB::table('campaign_design')
                 ->where('location','=', $id)
                 ->orWhere('city', '=', $id)                
                 ->lists('campaign_id');
        return $query;
    }
    

}
