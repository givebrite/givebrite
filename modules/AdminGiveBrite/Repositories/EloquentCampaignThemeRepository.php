<?php

namespace Modules\Admingivebrite\Repositories;

use Modules\Admingivebrite\Entities\CampaignTheme as CampaignThemeModel;

class EloquentCampaignThemeRepository {

    public function getModel() {
        return new CampaignThemeModel();
    }

    public function findById($id) {
        return CampaignThemeModel::findOrFail($id);
    }

    public function create(array $data) {
        return $this->getModel()->create($data);
    }

    public function findAll() {
        return CampaignThemeModel::all();
    }

    public function delete($id){
        return CampaignThemeModel::destroy($id);
    }
 

}


