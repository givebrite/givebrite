<?php

namespace Modules\Admingivebrite\Repositories;

use Modules\Admingivebrite\Entities\Banner as BannerModel;
use DB;

class EloquentBannerRepository {

    public function getModel() {
        return new BannerModel();
    }

    public function findById($id) {
        return BannerModel::findOrFail($id);
    }

    public function create(array $data) {
        return $this->getModel()->create($data);
    }
    
    public function update(array $data, $id)
    {
        // Getting Banner Model Instance
        $banner = $this->findById($id);
        // Update stage for campaign 
        $banner->update($data);
        return true;
    }

    public function findAll() {
        return BannerModel::all();
    }

    public function delete($id){
        return BannerModel::destroy($id);
    }
  
    public function findUsedPostions($id=0)
    {
        return BannerModel::where('id','!=',$id)->where('status','yes')->lists('position','position')->toArray();
    }
    
    public function findActiveBanners()
    {
        return BannerModel::where('status','yes')->orderBy('position')->get();
    }
}
