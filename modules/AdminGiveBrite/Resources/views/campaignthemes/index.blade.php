@extends('admin::layouts.master')

@section('content-header')
<h1>

    {!! $title or 'Campaigns Themes' !!} ({!! $campaignTheme->count() !!})
    &middot;
    <small>{!! link_to_route('campaign-theme-create', 'Add New') !!}</small>

</h1>
@stop

@section('content')
<table class="table">
    <thead>
        <th>No</th>
        <th>Name</th>			
        <th>Color</th>
        <th>Created At</th>
        <th class="text-center">Action</th>
    </thead>
    <tbody>
        @foreach ($campaignTheme as $theme)
        <tr>
            <td>{!! $theme->id !!}</td>
            <td>{!! $theme->name !!}</td>
            <td>{!! $theme->value !!}</td>
            <td>{!! $theme->created_at !!}</td>
            <td class="text-center">
                <a href="{!! url('admingivebrite/campaign/theme/edit/') !!}/{{$theme->id}}">Edit</a>
                <a href="{!! url('admingivebrite/campaign-theme-delete/') !!}/{{$theme->id}}">Delete</a>

            </td>
        </tr>

        @endforeach
    </tbody>
</table>
@stop