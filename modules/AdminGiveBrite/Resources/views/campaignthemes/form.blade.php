@if(isset($model)) 
{!! Form::model($model, ['method' => 'PUT', 'files' => true, 'route' => ['campaigntheme.update', $model->id]]) !!}
@else
{!! Form::open(['files' => true, 'route' => 'campaigntheme.store']) !!}
@endif

<div class="form-group">
    {!! Form::label('name', 'Color Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! Form::label('Choose Color Value', 'Choose Color Value:') !!}
    <div class="input-group my-colorpicker2">
        {!! Form::text('value', null, ['class' => 'form-control']) !!}
        <div class="input-group-addon">
            <i></i>
        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!} 

@section('style')  
<link href="https://almsaeedstudio.com/themes/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
@stop

@section('script')
<script src="{{ asset('site/js/bootstrap-colorpicker.min.js') }}"></script>
<script>
    $(".my-colorpicker2").colorpicker({
        format: 'hex'
    });

</script>
@stop