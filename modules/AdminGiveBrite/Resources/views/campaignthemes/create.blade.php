@extends('admin::layouts.master')

@section('content-header')
	
	
	<h1>
		Add New
		&middot;
		<small>{!! link_to_route('campaign-themes', 'Back') !!}</small>
	</h1>

@stop

@section('content')
	<div>
		@include('admingivebrite::campaignthemes.form')
	</div>

@stop
