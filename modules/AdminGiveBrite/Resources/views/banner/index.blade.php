@extends('admin::layouts.master')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('site/css/backend.css') }}">
@stop


@section('content-header')
@if(!empty($bannerPositionsAvailable) && count($bannerPositionsAvailable)>0)
<div class='add-btn pull-right'>
    <a class='btn btn-primary' href="{{URL::route('admingivebrite.banners.create')}}">Add New</a>
</div>
@endif
<h1>
    All Banners
    <small>(numbers of banner can't be more than 5.)</small>
</h1>

@stop

@section('content')
<?php $bannerBasePath=Config::get('config.banner_upload_path');?>
<table class="table">
    <thead>
    <th>Banner Name</th>
    <th>Call To Action</th>
    <th>Image</th>
    <th>Position</th>
    <th>Activation</th>
    <th>Action</th>
</thead>
<tbody>
   @if(!empty($banners) && count($banners) >0)
   @foreach($banners as $banner)
    <tr>
       <td>{{!empty($banner->name) ? $banner->name : "NA"}}</td>
       <td>{{!empty($banner->cta) ? $banner->cta : "NA"}}</td>
       <td>
           @if(!empty($banner->image))
           <img src='{{isset($banner->image) && !empty($banner->image) && File::exists($bannerBasePath.$banner->image ) ?  asset($bannerBasePath.$banner->image)  : asset('images/default-images/default-campaign.jpg')}}' height='100' width='200'>
           @endif
       </td>
       <td>{{!empty($banner->position) ? $banner->position : "NA"}}</td>
       <td>
           @if(!empty($banner->status) && $banner->status == 'yes')
           <a href="{{URL::route('banner.update.status',$banner->id)}}" class='btn btn-micro active hasTooltip'><span class="icon-publish"></span></a>
           @else
           <a href='{{URL::route('banner.update.status',$banner->id)}}' class='btn btn-micro active hasTooltip'><span class="icon-unpublish"></span></a>
           @endif
       </td>
       <td>
           <a href='{{URL::route('admingivebrite.banners.edit',$banner->id)}}'><button class='btn btn-primary'>Edit</button></a>
           <td>
                {!! Form::open( array('route' => array('admingivebrite.banners.destroy', $banner->id ),'role' => 'form','method' => 'Delete','onClick'=>"return confirm('Are you sure you want to delete?')")) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
       </td>
   </tr>
   @endforeach
   @else
    <tr><td colspan='5' class='text-center'>No Record Found</td></tr>
   @endif
</tbody>
</table>
@stop

@section('script')
@stop
