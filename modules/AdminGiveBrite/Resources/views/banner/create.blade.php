@extends('admin::layouts.master')

@section('content-header')


<h1>
    @if(isset($banner))
        Edit Banner
    @else
        Add Banner 
    @endif
    
    &middot;
    <small><a href="{{URL::route('admingivebrite.banners.index')}}">Back</a></small>
</h1>

@stop

@section('content')

@if(isset($banner)) 
{!! Form::model($banner, ['method' => 'PUT', 'files' => true, 'route' => ['admingivebrite.banners.update', $banner->id]]) !!}
@else
{!! Form::open(['files' => true, 'route' => 'admingivebrite.banners.store']) !!}
@endif

<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Banner Name']) !!}
    {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
    {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('cta', 'Call To Action') !!}
    {!! Form::text('cta', null, ['class' => 'form-control','placeholder'=>'http://example.com']) !!}
    {!! $errors->first('cta', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('cta_type', 'Cta Type:') !!}
    {!! Form::select('cta_type',['external'=>'External','internal'=>'Internal'],null,['class'=>'form-control selectpicker'])!!}      
    {!! $errors->first('cta_type', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    {!! Form::select('position',$bannerPositionsAvailable,null,['class'=>'form-control selectpicker'])!!}      
    {!! $errors->first('position', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status',['yes'=>'Yes','no'=>'No'],null,['class'=>'form-control selectpicker'])!!}      
    {!! $errors->first('status', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!} 
@stop

@section('script')
@stop
