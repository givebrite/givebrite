@extends('admin::layouts.master')

@section('content-header')
<h1>

    {!! $title or 'All Campaign Categories' !!} ({!! $campaignCategory->count() !!})
    &middot;
    <small>{!! link_to_route('campaign-category-create', 'Add New') !!}</small>

</h1>
@stop

@section('content')
<table class="table">
    <thead>
        <th>Category Name</th>
        <th class="text-center">Edit</th>
        <th class="text-center">Delete</th>
    </thead>
    <tbody>
        <?php $page =  app('request')->input('page') ; ?>
        @if($page)
            <?php  $pageno = "?page=".$page ;  ?>
        @endif
        
        @foreach ($campaignCategory as $category)
        <tr>
            <td>{!! $category->name !!}</td>
            <td class="text-center">
                <a href="{!! url('admingivebrite/campaign-category-edit/') !!}/{{$category->id}}{{($page ? $pageno : '') }}">Edit</a>
            </td>  
            <td class="text-center">
                @include('admingivebrite::partials.modal', ['data' => $category, 'name' => 'campaign-category-delete'])    
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
 <div class="paginationOuter text-center">
            
               {!! $campaignCategory->render() !!}
           
        </div>
@stop

