@extends('admin::layouts.master')

@section('content-header')
	
	
	<h1>
		Add New
		&middot;
		<small>{!! link_to_route('campaign-categories', 'Back') !!}</small>
	</h1>

@stop

@section('content')
	<div>
		@include('admingivebrite::campaigncategories.form')
	</div>

@stop
