<?php

Route::group(['prefix' => 'admingivebrite', 'namespace' => 'Modules\AdminGiveBrite\Http\Controllers'], function() {
    Route::get('/', 'AdminGiveBriteController@index');
  
    Route::get('/campaign-categories', [
        'as' => 'campaign-categories',
        'uses' => 'CampaignCategoriesController@index'
    ]);
    
    Route::get('/campaign-category-create', [
        'as' => 'campaign-category-create',
        'uses' => 'CampaignCategoriesController@create'
    ]);
      
    Route::get('/campaign-list', [
        'as' => 'campaign-list',
        'uses' => 'CampaignsController@index'
    ]);

  
    
    Route::get('/campaign-edit/{id}', [
        'as' => 'campaign-edit',
        'uses' => 'CampaignsController@edit'
    ]);

    Route::post('/campaign-store', [
        'as' => 'campaigns.store',
        'uses' => 'CampaignsController@store'
    ]);

    Route::put('/campaign-update/{id}', [
        'as' => 'campaigns.update',
        'uses' => 'CampaignsController@update'
    ]);

    Route::get('/campaign-delete/{id}', [
        'as' => 'campaign-delete',
        'uses' => 'CampaignsController@destroy'
    ]);

    Route::get('/campaign-charity-list', [
        'as' => 'campaign-charity-list',
        'uses' => 'CampaignsController@getAllCampaignCharities'
    ]);

    Route::get('/campaign-reward-list', [
        'as' => 'campaign-reward-list',
        'uses' => 'CampaignRewardsController@index'
    ]);
});
