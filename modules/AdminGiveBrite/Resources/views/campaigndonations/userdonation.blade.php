@extends('admin::layouts.master')

@section('content-header')
<h1>

    {!! $title or 'All Campaign Donations' !!} ({!! $campaignDonation->count() !!})

</h1>
@stop

@section('content')


<div class="row viewFilter donation clearfix">
    <?php $route = ($type == 'given' ? 'user-given-donations' : 'user-received-donations'); ?>
    {!! Form::open(array('route' => [$route , $userId ],'method'=>'get')) !!}
    <div class="col-sm-3">
        <h3 class="pull-left search">Filter Donation From Date</h3>
    </div>
       <div class="col-sm-3">
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
            <input type="text" name="from" value="<?php echo ($from ? date('m/d/Y',  strtotime($from)) : date('m/d/Y')); ?>" class="form-control pull-right" id="datepicker">
            </div>
            </div>
        <div class="col-sm-1">
              <h4 class="pull-left search">to </h4>
        </div>
        <div class="col-sm-3">
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="to" value="<?php echo ($to ? date('m/d/Y',  strtotime($to)) : ''); ?>" class="form-control pull-right" id="datepicker2">
                </div>
           
        </div>
        <div class="col-sm-1">
            {!! Form::hidden('type', "$type", ['class' => 'form-control']) !!}
            <button class='btn btn-info'>Search</button>
        </div>
    {!! Form::close() !!}
</div>
@if($campaignDonation->count()) 
<table class="table">
    <thead>
    <th>Name</th>
    <th>Campaign</th>
    <th>Amount</th>
    <th>Given/Received</th>
</thead>
<tbody>

    @foreach ($campaignDonation as $donation)
    <tr>
        <td>{!! $donation->name !!}</td>
        <td>{!! $donation->campaign_name !!}</td>
        <td>{!! ($donation->donation_amount_monetory ? Helper::showMoney($donation->donation_amount_monetory): "") !!}</td>
        <td>{!! $donation->user_id==$userId ? "Given" : "Received" !!}</td>
    </tr>

    @endforeach
</tbody>
</table>
<div class="paginationOuter text-center">

    {!! $campaignDonation->render() !!}

</div>

@else
<h4>No Record Found</h4>
@endif
@stop
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('site/css/backend.css') }}">
@stop
@section('script')
<script>
// set default dates
var start = new Date();
// set end date to max one year period:
var end = new Date(new Date().setYear(start.getFullYear()+1));

$('#datepicker').datepicker({
    endDate   : end
// update "toDate" defaults whenever "fromDate" changes
}).on('changeDate', function(){
    // set the "toDate" start to not be later than "fromDate" ends:
    $('#datepicker2').datepicker('setStartDate', new Date($(this).val()));
}); 

$('#datepicker2').datepicker({
    startDate : start,
    endDate   : end
// update "fromDate" defaults whenever "toDate" changes
}).on('changeDate', function(){
    // set the "fromDate" end to not be later than "toDate" starts:
    $('#datepicker').datepicker('setEndDate', new Date($(this).val()));
});
</script>
@stop