@extends('admin::layouts.master')

@section('content-header')


<h1>
    Add New
    &middot;
    <small>{!! link_to_route('menus-index', 'Back') !!}</small>
</h1>

@stop

@section('content')
<div>
    @include('admingivebrite::menus.form')
</div>
@stop
