<?php
$cat = null;
$status = 1;
?>
@if(isset($model)) 
{!! Form::model($model, ['method' => 'PUT', 'files' => true, 'route' => ['menus.update', $model->id]]) !!}
<?php
$cat = $model->cat_id;
$status = $model->status;
?>
@else
{!! Form::open(['files' => true, 'route' => 'menus.store']) !!}
@endif

<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control', 'readonly' => true]) !!}
    {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('categories', 'Choose Category:') !!}
    <div class="selectOuter">
        <select class="form-control" name="cat_id">
            @foreach($categories as $item)
            <option value="{{$item->id}}" {!! $cat==$item->id?'selected':'' !!} >{{$item->title}}</option>
            @endforeach
        </select>
        {!! $errors->first('categories', '<div class="text-danger">:message</div>') !!}
    </div>  
</div>
<div class="form-group">
    {!! Form::label('link', 'Link or Url:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
    {!! $errors->first('link', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <div class="selectOuter">
        <select class="form-control" name="status">
            <option value="1">Published</option>
            <option value="0" {!! $status==0?'selected':'' !!} >Unpublished</option>
        </select>
        {!! $errors->first('status', '<div class="text-danger">:message</div>') !!}
    </div>  
</div>
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    {!! Form::text('position', null, ['class' => 'form-control']) !!}
    {!! $errors->first('position', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!} 
