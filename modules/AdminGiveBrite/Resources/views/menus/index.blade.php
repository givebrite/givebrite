@extends('admin::layouts.master')

@section('content-header')
<h1>
    {!! 'Menus' !!} ({!! $menus->count() !!})
</h1>
@stop

@section('content')

@if ($menus->count()>0)
<table class="table">
    <thead>
        <th>{{ trans('admingivebrite::menus/titles.category') }}</th>
        <th>{{ trans('admingivebrite::menus/titles.name') }}</th>
        <th>{{ trans('admingivebrite::menus/titles.slug') }}</th>
        <th>{{ trans('admingivebrite::menus/titles.position') }}</th>
        <th>{{ trans('admingivebrite::menus/titles.status') }}</th>
        <th>{{ trans('admingivebrite::menus/titles.created_at') }}</th>
        <th class="text-center">{{ trans('admingivebrite::menus/titles.action') }}</th>
    </thead>
    <tbody>
        @foreach ($menus as $item)
        <tr>
            <td>{!! $item->cat_title !!}</td>
            <td>{!! $item->title !!}</td>
            <td>{!! $item->slug !!}</td>
            <td>{!! $item->position !!}</td>
            <td>{!! $item->status?'<i class="fa fa-check"></i>':'<i class="fa fa-close"></i>' !!}</td>
            <td>{!! $item->created_at !!}</td>
            <td class="text-center">
                <a href="{!! url('admingivebrite/menus/edit/') !!}/{{$item->id}}">
                    {{ trans('admingivebrite::menus/titles.edit') }}
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="paginationOuter text-center">            
       {!! $menus->render() !!}
</div>
@else
<h3>No Menus found</h3>
@endif
@stop

