<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Administrator | @yield('title', 'Dashboard')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    @include('admingivebrite::partials.style')
    @yield('style')
</head>
<body class="skin-blue fixed">

    @include('admingivebrite::partials.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        @include('admingivebrite::partials.sidebar')

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content-header')
            </section>

            <!-- Main content -->
            <section class="content">
                @include('admingivebrite::partials.flashes')
                @yield('content')
            </section>
        </aside>
        <!-- /.right-side -->
    </div>
    <!-- ./wrapper -->

    <!-- add new calendar event modal -->
    @include('admingivebrite::partials.script')
    @yield('script')
</body>
</html>
