@if(isset($model)) 
{!! Form::model($model, ['method' => 'PUT', 'files' => true, 'route' => ['campaigns.update', $model->id]]) !!}
@else
{!! Form::open(['files' => true, 'route' => '/campaign-store']) !!}
@endif

<div class="form-group">
    {!! Form::label('campaign_name', 'Campaign Name:') !!}
    {!! Form::text('campaign_name', null, ['class' => 'form-control']) !!}
    {!! $errors->first('campaign_name', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! Form::label('goal_amount_type', 'Goal Amount Type:') !!}
    {!! Form::text('goal_amount_type', null, ['class' => 'form-control']) !!}
    {!! $errors->first('goal_amount_type', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('goal_amount_time', 'Goal Amount Time:') !!}
    {!! Form::text('goal_amount_time', null, ['class' => 'form-control']) !!}
    {!! $errors->first('goal_amount_time', '<div class="text-danger">:message</div>') !!}
</div><div class="form-group">
    {!! Form::label('goal_amount_monetary', 'Goal Amount Monetary:') !!}
    {!! Form::text('goal_amount_monetary', null, ['class' => 'form-control']) !!}
    {!! $errors->first('goal_amount_monetary', '<div class="text-danger">:message</div>') !!}
</div><div class="form-group">
    {!! Form::label('campaign_type', 'Campaign Type:') !!}
    {!! Form::text('campaign_type', null, ['class' => 'form-control']) !!}
    {!! $errors->first('campaign_type', '<div class="text-danger">:message</div>') !!}
</div><div class="form-group">
    {!! Form::label('campaign_category', 'Campaign Category:') !!}
    {!! Form::select('campaign_category', $categories , isset($categories) ? $categories : null, ['class' => 'form-control']) !!}
    {!! $errors->first('campaign_category', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::date('start_date', null, ['class' => 'form-control']) !!}
    {!! $errors->first('start_date', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
    {!! $errors->first('end_date', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('campaign_rewards', 'Campaign Rewards:') !!}
    {!! Form::text('campaign_rewards', null, ['class' => 'form-control']) !!}
    {!! $errors->first('campaign_rewards', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('registered_charity', 'Registered Charity:') !!}
    {!! Form::text('registered_charity', null, ['class' => 'form-control']) !!}
    {!! $errors->first('registered_charity', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('stage', 'Stage:') !!}
    {!! Form::text('stage', null, ['class' => 'form-control']) !!}
    {!! $errors->first('stage', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!} 
