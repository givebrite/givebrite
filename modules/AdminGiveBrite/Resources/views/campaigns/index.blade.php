@extends('admin::layouts.master')

@section('content-header')
	<h1>
		All User Campaign's
		<small></small>
	</h1>
@stop

<?php // echo "<prE>"; print_r($categories);die; ?>


@section('content')

@if($campaignList->count()) 
	<table class="table">
		<thead>
			<th>Campaign Name</th>
                        <th>Goal Amount Monetary</th>
                        <th>Campaign Category</th>
                        <th>Pause/start</th>
                        <th>Delete</th>
		</thead>
		<tbody>
			@foreach ($campaignList as $campaign)
			<tr>
                                <td>{!! $campaign->campaign_name !!}</td>
                                <td>{!! ($campaign->goal_amount_monetary ? Helper::showMoney($campaign->goal_amount_monetary): "") !!} </td>
                                <td>{!! $campaign->name !!}</td>
                                <?php  $status = ($campaign->status == '1' ? '1' : '0'); ?>
                                <td><a title="{!!($status ? 'Pause' : 'Start')!!}" href="{!! route('campaign-status', array($campaign->id,$status,($campaign->user_id ? $campaign->user_id : 0)))!!}" class="btn btn-micro active hasTooltip" data-original-title="{!!($status ? 'Pause' : 'Start')!!}">
                                       @if($status)
                                       <span class="icon-publish"></span>
                                       @else
                                        <span class="icon-unpublish"></span>
                                       @endif
                                    </a>
                                </td>
                                <td>
                                    <a title="Delete" href="{!! route('campaign-delete', array($campaign->id))!!}" class="btn btn-micro active hasTooltip" data-original-title="Delete">
                                       <span class="icon-unpublish"></span>
                                    </a>
                                </td>
			</tr>
			
			@endforeach
		</tbody>
	</table>
        <div class="paginationOuter text-center">            
               {!! $campaignList->render() !!}
           
        </div>
@else
<h4>No Campaign Found</h4>
@endif
@stop
<link rel="stylesheet" type="text/css" href="{{ asset('site/css/backend.css') }}">