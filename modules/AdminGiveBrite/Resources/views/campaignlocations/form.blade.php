@if(isset($model)) 
{!! Form::model($model, ['method' => 'PUT', 'files' => true, 'route' => ['campaignlocation.update', $model->id]]) !!}
@else
{!! Form::open(['files' => true, 'route' => 'campaignlocation.store']) !!}
@endif

<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('parent', 'Parent:') !!}
    {!! Form::select('parent_id',[''=>'Please select'] +  $location , null, ['class' => 'form-control']) !!}
    {!! $errors->first('parent', '<div class="text-danger">:message</div>') !!}
</div>


<div class="form-group">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!} 
