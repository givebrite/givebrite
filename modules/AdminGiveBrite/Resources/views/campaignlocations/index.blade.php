@extends('admin::layouts.master')

@section('content-header')
<h1>

    {!! $title or 'All Campaign Locations' !!} ({!! $campaignLocation->count() !!})
    &middot;
    <small>{!! link_to_route('location-create', 'Add New') !!}</small>

</h1>
@stop
@section('content')
@if($campaignLocation->count()) 
<table class="table">
    <thead>
        <th>Name</th>
        <th>State</th>
        <th class="text-center">Edit</th>
        <th class="text-center">Delete</th>
    </thead>
    <tbody>
        <?php $page =  app('request')->input('page') ; ?>
        @if($page)
            <?php  $pageno = "?page=".$page ;  ?>
        @endif
        @foreach ($campaignLocation as $location)
        <tr>
            <td>{!! $location->name !!}
            </td>
            <td>
             @if($location->parent_name)
              {!! $location->parent_name !!}
             @else
              N/a
            @endif
            </td>
 
            <td class="text-center">
                <a href="{!! url('admingivebrite/campaign/location/edit/') !!}/{{$location->id}}{{($page ? $pageno : '') }}">Edit</a>
            </td>        
            <td class="text-center">
                @include('admingivebrite::partials.modal', ['data' => $location, 'name' => 'campaign-location-delete'])    
            </td>
        </tr>

        @endforeach
    </tbody>
</table>
 <div class="paginationOuter text-center">
            
               {!! $campaignLocation->render() !!}
           
        </div>
@else
<h4>No Record Found</h4>
@endif
@stop