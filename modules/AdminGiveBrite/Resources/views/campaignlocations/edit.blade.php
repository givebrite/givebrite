@extends('admin::layouts.master')

@section('content-header')
	<h1>
		Edit
		&middot;
		<small>{!! link_to_route('campaign-locations', 'Back') !!}</small>
	</h1>
@stop

@section('content')
	<div>
		@include('admingivebrite::campaignlocations.form')
	</div>

@stop