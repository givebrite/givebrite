<?php 
$status=1;
?>
@if(isset($model)) 
{!! Form::model($model, ['method' => 'PUT', 'files' => true, 'route' => ['content.update', $model->id]]) !!}
<?php
      $status = $model->status;
        ?>

@else
{!! Form::open(['files' => true, 'route' => 'content.store']) !!}
@endif

<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control', 'readonly' => true]) !!}
    {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control textarea']) !!}
    {!! $errors->first('description', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
</div>

{!! Form::close() !!} 

@section('style')
<link href="{!! asset('css/summernote.css') !!}" rel="stylesheet" type="text/css"/>
@stop


@section('script')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        if ($('#description').length) {
        $('#description').summernote({
             height: 200
            });
    }
    });
   
</script>
@stop