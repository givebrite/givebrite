@extends('admin::layouts.master')

@section('content-header')
<h1>
    Edit
    &middot;
    <small>{!! link_to_route('content.index', 'Back') !!}</small>
</h1>
@stop

@section('content')
<div>
    @include('admingivebrite::content.form')
</div>

@stop
