@extends('admin::layouts.master')

@section('content-header')
<h1>

    {!! 'Article content' !!} ({!! $content->count() !!})
    &middot;
    <small>{!! link_to_route('content.create', 'Add New') !!}</small>

</h1>
@stop

@section('content')

@if ($content->count()>0)
<table class="table">
    <thead>
        <th>{{ trans('admingivebrite::content/titles.name') }}</th>
        <th>{{ trans('admingivebrite::content/titles.slug') }}</th>
        <th>{{ trans('admingivebrite::content/titles.status') }}</th>
        <th>{{ trans('admingivebrite::content/titles.created_at') }}</th>
        <th class="text-center">{{ trans('admingivebrite::content/titles.action') }}</th>
    </thead>
    <tbody>
        @foreach ($content as $item)
        <tr>
            <td>{!! $item->title !!}</td>
            <td>{!! $item->slug !!}</td>
            <td>{!! $item->status?'<i class="fa fa-check"></i>':'<i class="fa fa-close"></i>' !!}</td>
            <td>{!! $item->created_at !!}</td>
            <td class="text-center">
                <a href="{!! url('admingivebrite/content/edit/') !!}/{{$item->id}}">
                    {{ trans('admingivebrite::content/titles.edit') }}
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="paginationOuter text-center">            
       {!! $content->render() !!}
</div>
@else
<h3>No Article found</h3>
@endif

@stop

