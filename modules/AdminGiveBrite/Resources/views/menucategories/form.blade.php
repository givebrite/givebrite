<?php $selPosition = null;
?>
@if(isset($model)) 
{!! Form::model($model, ['method' => 'PUT', 'files' => true, 'route' => ['menucategory.update', $model->id]]) !!}
<?php $selPosition = $model->position;
        ?>

@else
{!! Form::open(['files' => true, 'route' => 'menucategory.store']) !!}
@endif

<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control', 'readonly' => true]) !!}
    {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
</div>

<div class="form-group">
    {!! Form::label('position', 'Choose Position:') !!}
    <div class="selectOuter">        
    <select class="form-control" name="position">
        <option value="">Select</option>
        @foreach($positions as $key=>$value) 
        <option value=<?php echo $key;?> {!! $selPosition==$key?'selected':'' !!}>{!! $value !!}</option>        
        @endforeach        
    </select>
    {!! $errors->first('position', '<div class="text-danger">:message</div>') !!}
    </div>  
</div>

<div class="form-group">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!} 
