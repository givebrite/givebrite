@extends('admin::layouts.master')

@section('content-header')
<h1>

    {!! 'Menu Categories' !!} ({!! $categories->count() !!})
    
</h1>
@stop

@section('content')

@if ($categories->count()>0)
<table class="table">
    <thead>
        <th>{{ trans('admingivebrite::menus_categories/titles.name') }}</th>
        <th>{{ trans('admingivebrite::menus_categories/titles.slug') }}</th>
        <th>{{ trans('admingivebrite::menus_categories/titles.position') }}</th>
        <th>{{ trans('admingivebrite::menus_categories/titles.created_at') }}</th>
        <th class="text-center">{{ trans('admingivebrite::menus_categories/titles.action') }}</th>
    </thead>
    <tbody>
        @foreach ($categories as $category)
        <tr>
            <td>{!! $category->title !!}</td>
            <td>{!! $category->slug !!}</td>
            <td>{!! isset($positions[$category->position])?$positions[$category->position]:'' !!}</td>            
            <td>{!! $category->created_at !!}</td>
            <td class="text-center">
                <a href="{!! url('admingivebrite/menu-category-edit/') !!}/{{$category->id}}">
                    {{ trans('admingivebrite::menus_categories/titles.edit') }}
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="paginationOuter text-center">            
       {!! $categories->render() !!}
</div>
@else
<h3>No Category found</h3>
@endif

@stop

