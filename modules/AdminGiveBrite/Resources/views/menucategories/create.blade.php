@extends('admin::layouts.master')

@section('content-header')


<h1>
    Add New
    &middot;
    <small>{!! link_to_route('menu-categories', 'Back') !!}</small>
</h1>

@stop

@section('content')
<div>
    @include('admingivebrite::menucategories.form')
</div>
@stop
