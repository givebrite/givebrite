@extends('adming::layouts.master')

@section('content-header')
	<h1>
		 Campaign's Charity List
		<small></small>
	</h1>
@stop

@section('content')
	<table class="table">
		<thead>
			<th>No</th>
			<th>Campaign Name</th>
                        <th>Registration No</th>
                        <th>Charity Name</th>
                        <th>Contact</th>
                        <th>Web</th>			
			<th>Created At</th>
			
		</thead>
		<tbody> 
			@foreach ($campaignCharityList as $charity)
			<tr>
				<td>{!! $charity->id !!}</td>
                                <td>{!! $charity->campaign_name !!}</td>
				<td>{!! $charity->registration_no !!}</td>
                                <td>{!! $charity->charity_name !!}</td>
                                <td>{!! $charity->contact_telephone !!}</td>
				<td>{!! $charity->web !!}</td>
				<td>{!! $charity->created_at !!}</td>
				
			</tr>
			
			@endforeach
		</tbody>
	</table>
@stop