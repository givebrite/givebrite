<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [    
    'status' => array(
        'success' => 'Campaign status is successfully updated',
        'error' => 'Campaign status is not updated',
    ),
    'edit' => array(
        'success' => 'Campaign is successfully updated',
        'error' => 'Campaign is not updated',
    ),
    'delete' => array(
        'success' => 'Campaign status is successfully deleted',
        'error' => 'Campaign status is not deleted',
    ) 
];
