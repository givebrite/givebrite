<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostcodeToUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profile', function(Blueprint $table)
        {
			$table->string('country');
            $table->string('locality');
            $table->string('lat');
            $table->string('lng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profile', function(Blueprint $table)
        {
			$table->dropColumn('country');
            $table->dropColumn('locality');
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }

}
