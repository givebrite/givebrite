<?php

namespace Modules\Auth\Repositories\Roles;

Use Modules\Auth\Entities\Role as RoleModel;

class EloquentRoleRepository
{
    /**
     * Create model
     * @return UserProfileModel
     */
    public function getModel()
    {
        return new RoleModel();
    }
    
    
    /**
     * Find from the user_profile by user_id
     * @param type $id
     * @return type
     */
    public function findById($id) {
        return $this->getModel()->where('id', $id)->first();
    }
    
    public function findByUserId($id) {
        return $this->getModel()->where('user_id', $id)->first();
    }
}
