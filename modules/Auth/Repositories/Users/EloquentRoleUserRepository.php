<?php

namespace Modules\Auth\Repositories\Users;

Use Modules\Auth\Entities\RoleUser as RoleUserModel;

class EloquentRoleUserRepository {

    /**
     * Create model
     * @return UserActivationModel
     */
    public function getModel() {
        return new RoleUserModel();
    }

    /**
     * Insert into the user_activation
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        $this->getModel()->create($data);
    }

    /**
     * Find from the user_activation
     * @return type
     */
    public function findAll() {
        return $this->getModel()->all();
    }

    /**
     * Find from the user_activation by user_id
     * @param type $id
     * @return type
     */
    public function findByUserId($id) {
        return $this->getModel()->where('user_id', $id)->first();
    }
   
}
