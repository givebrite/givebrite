<?php

namespace Modules\Auth\Repositories\Users;

Use Modules\Auth\Entities\User as UserModel;
use Modules\Auth\Entities\UserProfile as UserProfileModel;

class EloquentUserRepository
{

    /**
     * Create model
     * @return UserModel
     */
    public function getModel()
    {
        return new UserModel();
    }

    /**
     * Insert into the user
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    /**
     * Find from the user
     * @return type
     */
    public function findAll()
    {
        return $this->getModel()->all();
    }

    /**
     * Find from the user by id
     * @param type $id
     * @return type
     */
    public function findById($id)
    {
        return $this->getModel()
            ->leftjoin('role_user', function ($join) {
                $join->on(
                    'users.id', '=', 'role_user.user_id'
                );
            })
            ->leftjoin('roles', function ($join) {
                $join->on(
                    'role_user.role_id', '=', 'roles.id'
                );
            })
            ->leftjoin('user_profile', function ($join) {
                $join->on('user_profile.user_id', '=', 'users.id');
            })
            ->find($id);
    }

    /**
     * Find from user by social id
     * @param type $sign_up_tag
     * @param type $sign_up_id
     * @return type
     */
    public function findBySocialId($sign_up_tag, $sign_up_id)
    {
        return $this->getModel()->where('sign_up_tag', $sign_up_tag)->where('sign_up_id', $sign_up_id)->first();
    }

//    public function updateSocialIdByEmail(array $data, $email) {
//        $user = $this->findByEmail($email);
//        $user->update(['sign_up_tag' => $data['sign_up_tag'], 'sign_up_id' => $data['sign_up_id']]);
//        return $user;
//    }

    /**
     * find from user by email
     * @param type $email
     * @return type
     */
    public function findByEmail($email)
    {
        return $this->getModel()->where('email', $email)->first();
    }
    
    public function findByEmailLike($email, $roleId = 2)
    {
        
        $query = $this->getModel()
            ->leftjoin('role_user', function ($join) {
                $join->on(
                    'users.id', '=', 'role_user.user_id'
                );
            })
            ->leftjoin('roles', function ($join) {
                $join->on(
                    'role_user.role_id', '=', 'roles.id'
                );
            })
            ->leftjoin('user_activation', function ($join) {
                $join->on(
                    'user_activation.user_id', '=', 'users.id'
                );
            })
            ->where('role_user.role_id', $roleId)
            ->Where('email', 'like', '%' . $email . '%')
            ->select('users.*', 'user_activation.is_activated', 'roles.name as role_name')
            ->orderBy('id', 'desc')
            ->paginate(config('config.paginationperpage')
            );
            return $query;
    }

    /**
     * update from user by email
     * @param array $data
     * @param type $email
     * @return type
     */
    public function updateByEmail(array $data, $email)
    {
        $user = $this->findByEmail($email);
        $user->update($data);
        return $user;
    }

    public function update(array $data, $id)
    {
        // Getting Campaign Model Instance
        $profile = $this->findById($id);
        // Update stage for campaign 
        $profile->update($data);
        return $profile;
    }

    /**
     * Update field by user id
     * @param string $status
     * @param type $id
     * @return type
     */
    public function updateStatusById($status, $id)
    {
        return $this->getModel()->where('id', $id)->update(['status' => $status]);
    }

    /**
     * Function for getting all users details on admin
     * @param none
     * @return type
     */
    public function getAllUserDetailsAdmin()
    {
        $query = $this->getModel()
            ->leftjoin('role_user', function ($join) {
                $join->on(
                    'users.id', '=', 'role_user.user_id'
                );
            })
            ->leftjoin('roles', function ($join) {
                $join->on(
                    'role_user.role_id', '=', 'roles.id'
                );
            })
            ->leftjoin('user_activation', function ($join) {
                $join->on(
                    'user_activation.user_id', '=', 'users.id'
                );
            })
            ->select('users.*', 'user_activation.is_activated', 'roles.name as role_name')
            ->orderBy('id', 'desc')
            ->paginate(config('config.paginationperpage')
            );
        return $query;
    }

    /**
     * Function for getting all users details on admin
     * @param none
     * @return type
     */
    public function getAllUserDetailsByRoles($roleId = 2)
    {
        $query = $this->getModel()
            ->leftjoin('role_user', function ($join) {
                $join->on(
                    'users.id', '=', 'role_user.user_id'
                );
            })
            ->leftjoin('roles', function ($join) {
                $join->on(
                    'role_user.role_id', '=', 'roles.id'
                );
            })
            ->leftjoin('user_activation', function ($join) {
                $join->on(
                    'user_activation.user_id', '=', 'users.id'
                );
            })
            ->where('role_user.role_id', $roleId)
            ->select('users.*', 'user_activation.is_activated', 'roles.name as role_name')
            ->orderBy('id', 'desc')
            ->paginate(config('config.paginationperpage')
            );
        return $query;
    }

}
