<?php

namespace Modules\Auth\Repositories\Users;

Use Modules\Auth\Entities\PasswordReset as PasswordResetModel;
use Illuminate\Support\Facades\DB;

class EloquentPasswordResetRepository {

    /**
     * Create model
     * @return UserActivationModel
     */
    public function getModel() {
        return new PasswordResetModel();
    }

    /**
     * Insert into the user_activation
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        return $this->getModel()->create($data);
    }
    
    /**
     * Find from the password_resets
     * @return type
     */
    public function findAll() {
        return $this->getModel()->all();
    }
    
    /**
     * matches if token exists in the table
     * @param array $data
     * @return type
     */
    public function postMatchToken(array $data)
    {
        return $this->getModel()->where('email', $data['email'])->where('token', $data['token'])->first();
    }
    
    public function preMatchToken(array $data)
    {
        return $this->getModel()->where('token', $data['token'])->first();
    }
    
    /**
     * delete from the table
     * @param type $email
     */
    public function delete($email)
    {
        DB::table('password_resets')->where('email', '=', $email)->delete();
    }

}
