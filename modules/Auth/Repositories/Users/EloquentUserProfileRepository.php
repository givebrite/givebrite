<?php

namespace Modules\Auth\Repositories\Users;

Use Modules\Auth\Entities\UserProfile as UserProfileModel;
Use DB;
use Illuminate\Support\Str as Str;

class EloquentUserProfileRepository
{

    /**
     * Create model
     * @return UserProfileModel
     */
    public function getModel()
    {
        return new UserProfileModel();
    }

    /**
     * Insert into the user_profile
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    public function findById($id, $columns = array('*'))
    {
        return $this->getModel()->findOrFail($id, $columns);
    }

    public function FindByIDs($id)
    {
        return $this->getModel()->where('user_id', $id)->first();

    }

    public function findByIdActivated($id)
    {
        $res = DB::table('user_profile as profile')
            ->leftjoin('user_activation as activeuser', 'activeuser.user_id', '=', 'profile.user_id')
            ->where('activeuser.is_activated', '=', '1')
            ->where('profile.user_id', '=', $id)
            ->get();

        return $res;
    }

    public function getProfileData($id)
    {
        return DB::table('user_profile as profile')
            ->leftjoin('users as user', 'user.id', '=', 'profile.user_id')
            ->select('profile.*', 'user.email')
            ->where('profile.user_id', $id)
            ->first();
    }

    public function updateOffline($userID,$profileData){

        $profile = $this->FindByIDs($userID);

        $profile = $this->findById($profile->id);
        $profile->update($profileData);
        return $profile;

    }

    public function getUserIdFromUniqueUpdateKey($uniqueKey){

        return DB::table('user_profile_update')
            ->where('unique_update_key', $uniqueKey)
            ->where('status', 1)
            ->pluck('user_id');

    }

    public function updateLinkStatusFromUniqueUpdateKey($uniqueKey){

        return DB::table('user_profile_update')->where('unique_update_key', $uniqueKey)
          ->update(['status' => 0]);
    }

    public function copyData(){

        $datas = DB::select("SELECT D.id AS donation_id,U.id  AS  user_id,P.postcode,P.house_number,P.street_name,P.addition_to_address,P.address,P.locality,P.country FROM campaign_donations D JOIN users U ON U.id = D.user_id JOIN user_profile P ON P.user_id = U.id LIMIT 1");

        foreach($datas as $data){

            DB::table('campaign_donation_details')->insert([
                'donation_id' => $data->donation_id, 'user_id' => $data->user_id,
                'postcode' => $data->postcode,'house_number' => $data->house_number, 
                'street_name'=>$data->street_name,'addition_to_address'=>$data->addition_to_address,
                'address' => $data->address,'locality'=>$data->locality,'country'=>$data->country]);

        }

        return 1;
    }

    public function usersWithGIftAidSelected(){

        return DB::select("SELECT DISTINCT(U.email),CD.is_giftaid,UP.id AS profile_id,U.id as user_id,U.salutation,UP.first_name,UP.sur_name,UP.house_number,UP.street_name,UP.addition_to_address,UP.address,UP.postcode,UP.country,UP.locality FROM campaign_donations  CD JOIN users U  ON U.id = CD.user_id  JOIN user_profile UP ON UP.user_id = U.id WHERE CD.is_giftaid = 1 ORDER BY UP.id DESC");
    }


    public function createUniqueUpdateKey($user_id,$uniqueKey){

        return DB::table('user_profile_update')->insertGetId(
            ['user_id' => $user_id, 'unique_update_key' => $uniqueKey]
        );

    }

    public function getUniqueUpdateKey($user_id){

        return DB::table('user_profile_update')->where('user_id',$user_id)->pluck('unique_update_key');

    }   


    /**
     * Find from the user_profile by user_id
     * @param type $id
     * @return type
     */
    public function findByUserId($id)
    {
        return $this->getModel()->where('user_id', $id)->first();
    }

    public function update(array $data, $id)
    {
        // Getting Campaign Model Instance
        $profile = $this->findById($id);
        // Update stage for campaign 
        $profile->update($data);
        return $profile;
    }

    public function updateCharityInfoByUserId($charityInfo = [], $userId)
    {
        $userProfileModel = $this->findByUserId($userId);

        // Update stage for campaign 
        $userProfile = $userProfileModel->update($charityInfo);
        return $userProfile;
    }

    public function getUniqueSlug($name, $slug = "", $id = 0)
    {
        $uniqueSlug = empty($slug) ? Str::slug($name, '_') : $slug;

        $checkUnique = UserProfileModel::where('slug', $uniqueSlug)->where('id', '!=', $id)->first();
        if ($checkUnique) {
            // Get Previous Unique no added
            $uniqueSlug = $uniqueSlug . "-" . rand(1, 9999);
            $this->getUniqueSlug($name, $uniqueSlug, $id);
        }
        return $uniqueSlug;
    }

    /**
     * Find from the user_activation
     * @return type
     */
    public function findByRole($id, $paginate_value, $filters = array())
    {
        $query = DB::table('user_profile as profile')
            ->join('user_activation', function ($join) {
                $join->on('user_activation.user_id', '=', 'profile.user_id')
                    ->where('user_activation.is_activated', '=', 1);
            })
            ->join('users', function ($join) {
                $join->on('users.id', '=', 'profile.user_id')
                    ->where('users.status', '=', 1);
            })
            ->join('role_user', function ($join) use ($id) {
                $join->on('profile.user_id', '=', 'role_user.user_id')
                    ->where('role_user.role_id', '=', $id);
            })
            ->where(function ($que) use ($filters) {
                if (isset($filters['search_keyword']) && !empty($filters['search_keyword'])) {
                    $que->orWhere('profile.charity_name', 'like', '%' . trim($filters['search_keyword']) . '%');
                }
            })
            ->where('profile.is_payment_account', '1')
            ->orderBy('profile.created_at', 'desc');

        return $query->paginate($paginate_value);
    }

    public function findBySlug($slug)
    {
        return $this->getModel()->where('slug', $slug)->first();
    }

    public function findByCharitySearch($id = 0, $filters = array(), $sort = array())
    {
        $query = DB::table('user_profile as profile')
            ->join('role_user', function ($join) use ($id) {
                $join->on('profile.user_id', '=', 'role_user.user_id')
                    ->where('role_user.role_id', '=', $id);
            })->join('users as user', 'user.id', '=', 'profile.user_id')
            ->where('user.status',1)
            ->where(function ($que) use ($filters) {
                if (isset($filters['search_keyword']) && !empty($filters['search_keyword'])) {
                    $que->orWhere('profile.charity_name', 'like', '%' . trim($filters['search_keyword']) . '%');
                }
            });
        return $query;
    }

}
