<?php

namespace Modules\Auth\Repositories\Users;

Use Modules\Auth\Entities\Follower as FollowerModel;

class EloquentFollowerRepository
{

    /**
     * Create model
     * @return UserProfileModel
     */
    public function getModel()
    {
        return new FollowerModel();
    }

    /**
     * Insert into the user_profile
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    public function find(array $user_info)
    {
        return $this->getModel()->where('user_id', $user_info['user_id'])
                                ->where('follower_id', $user_info['follower_id'])->first();
        
    }
    
    public function update(array $user_info)
    {
        $followed_user = $this->find($user_info);
        // Update follow 
        $updated_followed_user = $followed_user->update($user_info);
        return $updated_followed_user;
    }

}
