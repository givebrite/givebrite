<?php

namespace Modules\Auth\Repositories\Users;

Use Modules\Auth\Entities\UserActivation as UserActivationModel;

class EloquentUserActivationRepository {

    /**
     * Create model
     * @return UserActivationModel
     */
    public function getModel() {
        return new UserActivationModel();
    }

    /**
     * Insert into the user_activation
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        return $this->getModel()->create($data);
    }

    /**
     * Find from the user_activation
     * @return type
     */
    public function findAll() {
        return $this->getModel()->all();
    }

    /**
     * Update field by id
     * @param array $data
     * @param type $id
     * @return type
     */
    public function updateById(array $data, $id) { 
        return $this->getModel()->where('id', $id)->update(['is_activated' => $data['is_activated']]);
    }
    
//
//
//    public function updateById(array $data, $id) {
//        $user_activation = $this->findById($id);
//        $response = $user_activation->update($data);
//        return $response;
//    }
//
//    public function findById($id) {
//        return $this->getModel()->where('id', $id)->first();
//    }

    /**
     * Find from the user_activation by user_id
     * @param type $id
     * @return type
     */
    public function findByUserId($id) {
        return $this->getModel()->where('user_id', $id)->first();
    }
    
    /**
     * delete from user_activation by id
     * @param type $id
     */
    public function delete($id)
    {
        $this->getModel()->destroy($id);
    }
    
    
    /**
     * Update field by user id
     * @param string $activation
     * @param type $userId
     * @return type
     */
    public function updateByUserId($activation, $userId) { 
        return $this->getModel()->where('user_id', $userId)->update(['is_activated' => $activation]);
    }

}
