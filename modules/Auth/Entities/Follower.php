<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'followers';
    
    protected $fillable = ['user_id', 'follower_id', 'is_followed'];
}
