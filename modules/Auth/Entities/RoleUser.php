<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_user';
    
    protected $fillable = ['role_id', 'user_id'];
    
    public function roleName()
    {
        return $this->hasOne('Modules\Auth\Entities\Role','role_id','id');
    } 
}
