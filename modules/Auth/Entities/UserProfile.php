<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_profile';
    
    protected $fillable = ['user_id', 'first_name', 'sur_name', 'recieve_updates','registered_charity','charity_logo','registration_no','charity_name','contact_telephone','web','address','postcode','description','profile_pic','slug','created_at','updated_at','country','locality','lat','lng','is_payment_account','house_number','street_name','addition_to_address',];

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}
