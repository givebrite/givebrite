<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_activation';
    
    protected $fillable = ['user_id', 'token_code', 'is_activated'];
}
