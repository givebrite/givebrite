<?php

return [

   'create' => array(
        'error' => 'User Profile is not created, please try again.',
        'success' => 'User Profile is successfully updated'
    ),
];
