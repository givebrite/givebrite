<?php

return [
	'user-auth' => [
		'first_name-required' => 'First name is required',
		'first_name-min' => 'First name should contain atleast 2 characters',
		'sur_name-required' => 'Surname is required',
		'sur_name-min' => 'Sur name should contain atleast 2 characters',
		'email-required' => 'Email is required.',
		'email-email' => 'Email must be a valid email address.',
		'email-unique' => 'Email has already been taken',
		'password-required' => 'Password is required',
		'password-min' => 'Password should contain atleast 6 characters',
		'password-confirm' => 'Password and Confirm Password does not match',
		'registration-no-required' => 'Charity Registration Number is required',
		'registration-no-numeric' => 'Registration Number should contain numbers only',
		'charity-name-required' => 'Charity name is required',
		'charity-name-min' => 'Charity name should contain atleast 2 characters',
		'address-required' => 'Address is required',
		'postcode-required' => 'Postcode is required',
		'postcode-numeric' => 'Postcode should contain numbers only',
		'contact-telephone-required' => 'Telephone is required',
		'contact-telephone-numeric' => 'Telephone should contain numbers only',
		'contact-telephone-min' => 'Telephone should contain atleast 11 numbers',
		'web-required' => 'Website is required',
		'web-url-format' => 'Url is invalid format',
		'description-required' => 'Description is required',
		
	],
];
