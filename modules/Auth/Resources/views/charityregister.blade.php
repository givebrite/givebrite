@extends('site.layouts.master')

@section('title', 'Charity Register')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="login-tab">
                    <div class="tab-top-wrap">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a href="{{url('/register')}}" aria-controls="register">Register</a>
                            </li>
                            <li role="presentation">
                                <a href="{{url('/login')}}" aria-controls="login">Login</a>
                            </li>
                            <li role="presentation" class="active">
                                <a href="{{url('/charityregister')}}" aria-controls="charityreg">Charity
                                    Registration</a>
                            </li>

                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 hidden-md hidden-lg">
                            <div class="login-right">
                                <h2>Why register?</h2>
                                <div class="why-register">
                                    <ul>
                                        <li><i class="fa fa-check"></i> <span>Its free, easy and secure</span></li>
                                        <li><i class="fa fa-check"></i>
                                            <span>Receive funds directly into your account</span></li>
                                        <li><i class="fa fa-check"></i> <span>Gift Aid enabled</span></li>
                                        <li><i class="fa fa-check"></i>
                                            <span>Donors have the option to pay your fees</span></li>
                                        <li><i class="fa fa-check"></i> <span>Insights into your donations</span></li>
                                        <li><i class="fa fa-check"></i> <span>Enable donation types to keep better track of your donations</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="login-left">
                                <h2>Register your charity</h2>
                                {!! Form::open(array('url' => 'register', 'novalidate' => 'novalidate','files'=>true,'id'=>'register-form-charity')) !!}
                                {!! Form::hidden('sign_up_option','charity') !!}
                                <div class="signUp-form">
                                    <div class="form-sec">
                                        <div class="form-row">
                                            <div class="browse-field">
                                                <!-- <input type="file" name="myFile" id="myFile">-->
                                                {!! Form::file('charity_logo', ['accept'=>"image/gif,image/jpeg,image/png",'class' => 'form-control file-input','id'=>'myFile']) !!}
                                                <label for="myFile">
                                                    {!! HTML::image('images/upload-photo.png') !!}
                                                    <span>Upload your logo</span>
                                                    <p class="upload-text">Images play a very important part in your
                                                        campaigns</p>
                                                </label>
                                                {!! $errors->first('charity_logo', '<div class="text-danger">:message</div>') !!}
                                            </div>
                                            <div class="inputBlock image-upload-wrapper" class="charityLogo">
                                                <?php
                                                $imageName = Session::get('charity_logo');
                                                $basepath = Config::get('config.thumb_charity_logo_upload_path');
                                                ?>
                                                <img id='viewProfileImage' class="img-border"
                                                     src="{{ !empty($imageName) && File::exists($basepath.$imageName ) ? asset($basepath.$imageName) : ''}}"
                                                     height="175px" style="max-height:175px;max-width:240px; display: block important;"/>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            {!! Form::text('charity_name', null, ['class' => 'form-control','placeholder' => 'Charity Name']) !!}
                                        </div>
                                        <div class="form-row">
                                            {!! Form::text('registration_no', null, ['class' => 'form-control','placeholder' => 'Charity Registration Number']) !!}
                                            {!! $errors->first('registration_no', '<div class="text-danger">:message</div>') !!}
                                        </div>
                                        <div class="form-row">
                                            {!! Form::text('web', null, ['class' => 'form-control','placeholder' => 'Website']) !!}
                                            {!! $errors->first('web', '<div class="text-danger">:message</div>') !!}
                                        </div>
                                        <div class="form-row">
                                            {!! Form::text('contact_telephone', null, ['placeholder' => 'Telephone', 'class' => 'phone form-control']) !!}
                                            {!! $errors->first('contact_telephone', '<div class="text-danger">:message</div>') !!}
                                        </div>
                                        <div class="form-row">
                                            {!! Form::textarea('description', null, ['class' => 'form-control','placeholder' => 'Describe your charity', 'size' => '50x4']) !!}
                                            {!! $errors->first('description', '<div class="text-danger">:message</div>') !!}
                                        </div>
                                        <div class="form-row">
                                            <label>Security Information</label>
                                        </div>

                                        <div class="form-row">
                                            {!! Form::email('first_name', Session::has('email') ? Session::get('first_name') : '', ['class' => 'form-control','placeholder' => 'First Name']) !!}
                                            {!! $errors->first('first_name', '<div class="text-danger">:message</div>') !!}
                                        </div>

                                        <div class="form-row">
                                            {!! Form::email('email', Session::has('email') ? Session::get('email') : '', ['class' => 'form-control','placeholder' => 'Email Address']) !!}
                                            {!! $errors->first('email', '<div class="text-danger">:message</div>') !!}
                                        </div>
                                        <div class="form-row">
                                                <div class="inputBlock"> {!! Form::email('email_confirm', Session::has('email') ? Session::get('email') : '', ['class' => 'form-control','placeholder' => 'Confirm E-mail Address']) !!}
                                                    {!! $errors->first('email_confirm', '
                                                    <div class="text-danger">:message</div>
                                                    ') !!} </div>
                                            </div>
                                            
                                        <div class="form-row">
                                            {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) !!}
                                            {!! $errors->first('password', '<div class="text-danger">:message</div>') !!}
                                        </div>
                                        <div class="form-row">
                                            <label>Address Information</label>
                                        </div>
                                         
                                         <!--postcode-->
                                         <div class="row">
                                                <div class="form-row multiinputs clearfix">
                                                    
                                                    <div class="col-xs-7 col-sm-6 col-md-9 col-lg-9">
                                                 {!! Form::text('postcode', null, ['class' => 'form-control','placeholder' => 'Postcode','id'=>'postCode','postCodeStatus'=>'404']) !!}
                                                {!! $errors->first('postcode', '<div class="text-danger">:message</div>') !!}
                                                
                                                 <span class="minor pull-right location-link-span">
                                                    <a class="location-link">Enter Address manually</a>
                                                 </span>
                                                 <p class="text-primary" id="postCodeMessage"></p>

                                                    </div>

                                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 searchPostCode">
                                                   
                                                        <a href="javascript:void(0)" id="searchPostCode">Search</a>

                                                    </div>

                                                   
                                                </div>
                                            </div>
                                            <!--postcode-->

                                            <!--select dropdown-->
                                             
                                             <div class="form-row hidden" id="AddressSelectMain">
                                               
                                               <div class="register-select"> 
                                                
                                                <select name="addressSelect" id="addressSelect" class="valid" >
                                                    
                                                    <option value="">Please Select your address</option>

                                                </select>

                                               </div> 
                                                
                                                <div class="select__arrow"></div>
                                                
                                            </div>
                                             <!--select dropdown-->   

                                        <!-- <div class="form-row">
                                            <div class="inputBlock">
                                                {!! Form::text('postcode', null, ['class' => 'form-control','placeholder' => 'Postcode','id'=>'postCode','postCodeStatus'=>'']) !!}
                                                {!! $errors->first('postcode', '<div class="text-danger">:message</div>') !!}
                                                <p class="text-primary" id="postCodeMessage"></p>
                                            </div>
                                        </div> -->


                                        <!-- <div class="form-row">
                                            {!! Form::hidden('address', null, ['class' => 'form-control','placeholder' => 'Address' , 'id' => 'address']) !!}
                                            {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
                                        </div> -->
                                        @if($errors->any())
                                        <div class="registerHiddenFields">
                                        @else
                                        <div class="registerHiddenFields hidden">
                                        @endif

                                           <div class="form-row">
                                                <div class="inputBlock">
                {!! Form::text('address', null, ['class' => 'form-control','placeholder' => 'Address','id'=>'address']) !!}
                                        {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
                                                </div>
                                            </div>


                                            
                                            <div class="form-row">
                                                <div class="inputBlock">
{!! Form::text('locality', null, ['class' => 'form-control','placeholder' => 'Locality','id'=>'district']) !!}
                                        {!! $errors->first('locality', '<div class="text-danger">:message</div>') !!}
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="inputBlock">
{!! Form::text('country', null, ['class' => 'form-control','placeholder' => 'Country','Country', 'id'=>'country']) !!}
                                        {!! $errors->first('country', '<div class="text-danger">:message</div>') !!}
                                                </div>
                                            </div>
                                        </div>
                                            
                                        <div class="row">
                                            <div class="form-row col-sm-12 col-lg-4">
                                                {!! Form::submit('Register', array('class'=>'fullwidth blue-btn-lg text-center')) !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 hidden-xs hidden-sm">
                            <div class="login-right">
                                <h2>Why register?</h2>
                                <div class="why-register">
                                    <ul>
                                        <li><i class="fa fa-check"></i> <span>Its free, easy and secure</span></li>
                                        <li><i class="fa fa-check"></i>
                                            <span>Receive funds directly into your account</span></li>
                                        <li><i class="fa fa-check"></i> <span>Gift Aid enabled</span></li>
                                        <li><i class="fa fa-check"></i>
                                            <span>Donors have the option to pay your fees</span></li>
                                        <li><i class="fa fa-check"></i> <span>Insights into your donations</span></li>
                                        <li><i class="fa fa-check"></i> <span>Enable donation types to keep better track of your donations</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    </div>
@stop

@section('script')
    {!! JsValidator::formRequest('Modules\Auth\Http\Requests\LoginCreateRequest','#login-form') !!}

    <script src="{{ asset('modules/campaigns/js/common.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    {!! JsValidator::formRequest('Modules\Auth\Http\Requests\RegisterUserCreateRequest','#register-form') !!}
    {!! JsValidator::formRequest('Modules\Auth\Http\Requests\RegisterCharityCreateRequest','#register-form-charity') !!}
    <script src="{{ asset('modules/auth/js/form-custom.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    <script type='text/javascript'
            src="{{ asset('js/jquery.maskedinput.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    <script src="{{ asset('modules/auth/js/register-browse-pic.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    <script type="text/javascript">
      
        $( "#register-form-charity" ).submit(function( event ) {
            
            var postcode = $('#postCode').val(); 
            var status = document.getElementById("postCode").getAttribute("postCodeStatus");    
            
            if(status == 404){

                //if the postcode is not empty try to validate
                if(postcode != ''){

                    $( "#searchPostCode" ).trigger( "click" );
                    $('#postCodeMessage').html('');
                    return false;

                } else {

                    $('#postCodeMessage').html('Please enter a valid postcode');
                    return false;
                }

                
            }
          
            return true;

        });
  

      // $('#postCode').keyup(function(e) {
      //     //debugger;
      //     //console.log(this.value)

      //     $.ajax({
      //           url: "https://api.ideal-postcodes.co.uk/v1/postcodes/"+this.value+"?api_key=iddqd",
      //           dataType: "json",
      //           type: "get",
      //           contentType: "application/json; charset=utf-8",
      //           success: function (data) {

      //               $('#postCode').attr('postcodestatus',200);
      //               $('#postCode').val(data.result[0]['postcode']);
      //               $('#postCodeMessage').html('post code verified');
      //               $('#country').val(data.result[0]['country']); 
      //               $('#district').val(data.result[0]['district']);
      //               console.log(data)
      //           },
      //           error:function (xhr, ajaxOptions, thrownError){

      //               if(xhr.status==404) {
      //                   $('#postCode').attr('postcodestatus',404);
      //                   $('#postCodeMessage').html('post code not valid');
      //                   $('#country').val(''); 
      //                   $('#district').val('');
      //                   console.log('No post code found');
      //               }
      //           }
      //       });

      //       //console.log(this.value);
      // });

      $('#searchPostCode').click(function(e) {
          
          //console.log(this.value)
          $('#postCode').attr('postCodeStatus',404);
          var string = $('#postCode').val();

          $.ajax({
                url: "https://api.ideal-postcodes.co.uk/v1/postcodes/"+string+"?api_key=iddqd",
                dataType: "json",
                type: "get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    $('#postCode').attr('postCodeStatus',200);
                    /*
                    $('#postCode').val(data.result[0]['postcode']);
                    $('#postCodeMessage').html('post code verified');
                    $('#country').val(data.result[0]['country']); 
                    $('#district').val(data.result[0]['district']);
                    */
                    $('#postCode').val(data.result[0]['postcode']);
                    // $('#postCodeMessage').html('post code verified');
                    $('#AddressSelectMain').removeClass('hidden');
                    $('#addressSelect').html('<option>Please Select your address </option>');
                        
                    $.each(data.result, function(index, element) {

                    $('#addressSelect').append('<option value="'+ element.line_1+' , '+element.district+' ,'+element.country+' ">'+ element.line_1+' , '+element.district+' ,'+element.country+' </option>');
                    
                    });

                    $("#addressSelect").show();  
                    

                    console.log(data)
                },
                error:function (xhr, ajaxOptions, thrownError){

                    if(xhr.status==404) {
                        $('#postCode').attr('postCodeStatus',404);
                        $('#AddressSelectMain').addClass('hidden');
                        $('#postCodeMessage').html('Please enter a valid postcode');
                        $('#address').val('');
                        $('#country').val(''); 
                        $('#district').val('');
                        console.log('No post code found');
                    }
                }
            });

            //console.log(this.value);
      });

       $('#addressSelect').on('change', function() {

          var address = this.value;
          var arr = address.split(',');
          $('#address').val(arr[0]);
          $('#district').val(arr[1]);
          $('#country').val(arr[2]); 
          $(".registerHiddenFields").removeClass('hidden');
          console.log(arr);
       })
        
        $('.location-link').click(function(){

              $("#addressSelect").hide(); $('#postCodeMessage').html('');  
              $('#address').val(''); $('#district').val(''); $('#country').val('');  $('#postCode').val('');
              $(".registerHiddenFields").toggleClass('hidden');
       
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewProfileImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#myFile").change(function(){
        $("#viewProfileImage").css("display",""); 
        readURL(this);

        });

    </script>
@stop