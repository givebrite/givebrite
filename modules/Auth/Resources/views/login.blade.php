@extends('site.layouts.master')

@section('title', 'Login')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="login-tab">
                <div class="tab-top-wrap">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation">
                            <a href="{{url('/register')}}" aria-controls="register" >Register</a>
                        </li>
                        <li role="presentation" class="active">
                            <a href="{{url('/login')}}" aria-controls="login" >Login</a>
                        </li>
                        <li role="presentation">
                            <a href="{{url('/charityregister')}}" aria-controls="charityreg" >Charity Registration</a>
                        </li>

                    </ul>
                </div>
                <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="login">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="login-left">
                                    <h2>Login with:</h2>
                                    <h4>Choose a social media account</h4>
                                    <div class="social-buttons">{{--<a class="btn btn-fb" href="{{ url('auth/login/facebook') }}"><i class="fa fa-facebook"></i> <span>Login with Facebook</span></a> <a class="btn btn-tw" href="{{ url('auth/login/twitter') }}"><i class="fa fa-twitter"></i> <span>Login with Twitter</span></a>--}} <a class="btn btn-google-plus" href="{{ url('auth/login/google') }}"><i class="fa fa-google"></i> <span>Login with Google</span></a> </div>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" align="center">
                                <div class="or-text">or</div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                <div class="signUp-form login-right">
                                    <h2>Login with email address</h2>
                                    <!-- <h4>Enter your details</h4> -->
                                    {!! Form::open(array('url' => 'login', 'novalidate' => 'novalidate', 'files'=>true,'id'=>'login-form')) !!}
                                    <div class="form-sec">
                                        <div class="form-row">
                                            <div class="inputBlock">
                                                <!-- <input type="hidden" name="token" value="{{Input::get('token')}}"> -->
                                                @if($redirectUrl != '')
                                                    <input type="hidden" name="redirect" value="campaign.donation">
                                                @else
                                                    <input type="hidden" name="redirect" value="{{$redirectUrl}}">
                                                @endif
                                                <input type="hidden" name="token" value="{{$redirectUrl}}">
                                                {!! Form::email('email', Session::has('email') ? Session::get('email') : '', ['class' => 'form-control','placeholder' => 'E-mail Address']) !!}
                                                {!! $errors->first('email', '
                                                <div class="text-danger">:message</div>
                                                ') !!} </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="inputBlock"> {!! Form::password('password', ['placeholder' => 'Password','class' => 'form-control']) !!}
                                                {!! $errors->first('password', '
                                                <div class="text-danger">:message</div>
                                                ') !!} </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-row col-sm-12 col-lg-4 pull-right">
                                            <a href="{{url('/forgot')}}" class="pull-right">Forgot Password</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-row col-sm-12 col-lg-4"> {!! Form::submit('Login', array('class'=>'fullwidth blue-btn-lg text-center')) !!} </div>
                                        </div>
                                        <!--<div id="user3-form">
                                                <div class="text-center alreadyAccount form-row">Not Registered? <a href="{{ url('register') }}">Sign Up Now</a></div>
                                                <div class="text-center alreadyAccount form-row"><a href="{{ url('forgot') }}">Forgot Password ?</a></div>
                                            </div>-->
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @stop

@section('script')
{!! JsValidator::formRequest('Modules\Auth\Http\Requests\LoginCreateRequest','#login-form') !!}
<script src='{{ asset('modules/campaigns/js/common.js') }}'></script>
<script src="{{ asset('modules/campaigns/js/common.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
{!! JsValidator::formRequest('Modules\Auth\Http\Requests\RegisterUserCreateRequest','#register-form') !!}
{!! JsValidator::formRequest('Modules\Auth\Http\Requests\RegisterCharityCreateRequest','#register-form-charity') !!}
<script src="{{ asset('modules/auth/js/form-custom.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
<script type='text/javascript' src="{{ asset('js/jquery.maskedinput.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
<script src="{{ asset('modules/auth/js/register-browse-pic.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
<script type="text/javascript">

</script>
@stop