@extends('site.layouts.master')

@section('title', 'Offline profile update')

@section('style')
    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <style>
        @media (max-width: 767px) {
            .tab-top-wrap {
                border-bottom: none;
            }
        }
        .inputBlock{
            width: 100%;
        }
        .form-sec .form-row.multiinputs .inputBlock{
            width: 100%;
        }
        #donation_amt
        {
            padding: 0 50px;
        }
        .form-sec .form-row.multiinputs .inputBlock:last-child {
            margin-left: 0%;
        }
        #donor_name
        {
            display: none;
        }
        .registerHiddenFields{

            display:none;
        }
        .msg{margin-top:10%; margin-bottom:10%;}
    </style>
@stop

@section('content')

<div class="container">
    <div class="col-sm-12 contact-wrapper">
        <div class="laungSlogan createHeading">

            <div class="col-lg-12 msg">

            <h2 align="center">{{$msg}}</h2>
            

        </div>
        
        </div>
    </div>
</div>
</div>


@stop

@section('script')
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&signed_in=true&libraries=drawing&callback=initMap"
            async defer></script>
    <script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/campaign-detail.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>



    <script src="{{asset('site/js/fusioncharts.js')}}"></script>
    <script src="{{asset('site/js/fusioncharts.charts.js') }} "></script>
    <script src="{{asset('site/js/fusioncharts.theme.management-dashboard.js') }}"></script>
     
    
@stop