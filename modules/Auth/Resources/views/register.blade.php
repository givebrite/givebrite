@extends('site.layouts.master')

@section('title', 'Register')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="login-tab">
                    <div class="tab-top-wrap">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="{{url('/register')}}" aria-controls="register">Register</a>
                            </li>
                            <li role="presentation">
                                <a href="{{url('/login')}}" aria-controls="login">Login</a>
                            </li>
                            <li role="presentation">
                                <a href="{{url('/charityregister')}}" aria-controls="charityreg">Charity
                                    Registration</a>
                            </li>

                        </ul>
                    </div>
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="register">
                            <?php $base_path = Config::get('config.profile_image_upload_path'); ?>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="login-left">
                                    <h2>Register with:</h2>
                                    <h4>Choose a social media account</h4>
                                    <div class="social-buttons">
                                        {{--<a target="_blank" class="btn btn-fb" href="{{ url('auth/login/facebook') }}">--}}
                                            {{--<i class="fa fa-facebook"></i>--}}
                                            {{--<span>Register with Facebook</span>--}}
                                        {{--</a>--}}
                                        {{--<a target="_blank" class="btn btn-tw" href="{{ url('auth/login/twitter') }}">--}}
                                            {{--<i class="fa fa-twitter"></i>--}}
                                            {{--<span>Register with Twitter</span>--}}
                                        {{--</a>--}}
                                        <a target="_blank" class="btn btn-google-plus"
                                           href="{{ url('auth/login/google') }}">
                                            <i class="fa fa-google"></i>
                                            <span>Register with Google</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" align="center">
                                <div class="or-text">or</div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="login-right">
                                    <div class="social-media-buttons">
                                        <h2>Register with your email</h2>
                                        <!-- <h4>Enter your details</h4> -->
                                    </div>
                                    {!! Form::open(array('url' => 'register', 'novalidate' => 'novalidate', 'files'=>true,'id'=>'register-form')) !!}
                                    <div class="signUp-form">
                                        <div class="form-sec">
                                            <div id="user1-form">
                                                {!! Form::hidden('sign_up_tag', Session::has('sign_up_tag') ? Session::get('sign_up_tag') : '') !!}
                                                {!! Form::hidden('sign_up_id', Session::has('sign_up_id') ? Session::get('sign_up_id') : '') !!}
                                                {!! Form::hidden('social_profile_image', Session::has('social_profile_image') ? Session::get('social_profile_image') : '') !!}

                                                <div class="row">
                                                <div class="form-row multiinputs clearfix">
                                                    <div class="register-select col-sm-12 col-md-3 col-lg-3 ">
                                                    {!! Form::select('salutation', array('Mr' => 'Mr', 'Miss' => 'Miss', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Dr' => 'Dr', 'Prof' => 'Prof', 'Maulana' => 'Maulana')) !!}
                                                        <div class="select__arrow"></div>
                                                    </div>

                                                    <div class="col-sm-12 col-md-9 col-lg-9">
                                                        {!! Form::text('first_name', Session::has('first_name') ? Session::get('first_name') : '', ['class' => 'form-control','placeholder' => 'First Name']) !!}
                                                        {!! $errors->first('first_name', '
                                                        <div class="text-danger">:message</div>
                                                        ') !!}
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="inputBlock">
                                                    {!! Form::text('sur_name', Session::has('sur_name') ? Session::get('sur_name') : '', ['class' => 'form-control','placeholder' => 'Surname']) !!}
                                                    {!! $errors->first('sur_name', '
                                                    <div class="text-danger">:message</div>
                                                    ') !!}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="inputBlock"> {!! Form::email('email', Session::has('email') ? Session::get('email') : '', ['class' => 'form-control','placeholder' => 'E-mail Address']) !!}
                                                    {!! $errors->first('email', '
                                                    <div class="text-danger">:message</div>
                                                    ') !!} </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="inputBlock"> {!! Form::email('email_confirm', Session::has('email') ? Session::get('email') : '', ['class' => 'form-control','placeholder' => 'Confirm E-mail Address']) !!}
                                                    {!! $errors->first('email_confirm', '
                                                    <div class="text-danger">:message</div>
                                                    ') !!} </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="inputBlock"> {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) !!}
                                                    {!! $errors->first('password', '
                                                    <div class="text-danger">:message</div>
                                                    ') !!} </div>
                                            </div>
                                           
                                           <div class="row">
                                                <div class="form-row multiinputs clearfix">
                                                    
                                                    <div class="col-xs-7 col-sm-7 col-md-9 col-lg-9">
                                            {!! Form::text('postcode', null, ['class' => 'form-control','placeholder' => 'Postcode' ,'id'=>'postCode','postCodeStatus'=>'404']) !!}
                                            {!! $errors->first('postcode', '<div class="text-danger">:message</div>') !!}   
                                                 
                                                 <span class="minor pull-right location-link-span">
                                                    <a class="location-link">Enter Address manually</a>
                                                 </span>

                                                 <p class="text-primary" id="postCodeMessage"></p>

                                                    </div>

                                                    <div class="col-xs-3 col-sm-7 col-md-3 col-lg-3 searchPostCode">
                                                   
                                                        <a href="javascript:void(0)" id="searchPostCode">Search</a>

                                                    </div>

                                                   
                                                </div>
                                            </div>
                                      
                                            <!--select dropdown-->
                                             
                                             <div class="form-row hidden" id="AddressSelectMain">
                                               
                                               <div class="register-select"> 
                                                
                                                <select name="addressSelect" id="addressSelect" class="valid" required="">
                                                    
                                                    <option value="">Please Select your address</option>

                                                </select>

                                               </div> 
                                                
                                                <div class="select__arrow"></div>
                                                
                                            </div>
                                             <!--select dropdown--> 

<!-- 
                                             <div class="form-row"> 
                                               <input type="hidden" id="address" name="address" placeholder="Address" class="form-control">
                                                
                                            </div>  -->
                                            @if($errors->first('address') || $errors->first('locality') || $errors->first('locality'))
                                            <div>
                                            @else
                                            <div class="registerHiddenFields hide-Location">
                                            @endif
                                            
                                            <div class="form-row">
                                                <div class="inputBlock">
                {!! Form::text('address', null, ['class' => 'form-control','placeholder' => 'Address','id'=>'address']) !!}
                                        {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
                                                </div>
                                            </div>


                                            <div class="form-row">
                                                <div class="inputBlock">
{!! Form::text('locality', null, ['class' => 'form-control','placeholder' => 'Locality','id'=>'district']) !!}
                                        {!! $errors->first('locality', '<div class="text-danger">:message</div>') !!}
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="inputBlock">
{!! Form::text('country', null, ['class' => 'form-control','placeholder' => 'Country','Country', 'id'=>'country']) !!}
                                        {!! $errors->first('country', '<div class="text-danger">:message</div>') !!}
                                                </div>
                                            </div>

                                            </div>

                                            <div id="user2-form">
                                                <div class="form-row privacyPolicy">
                                                    By signing up you agree to the
                                                    <a href="{{ url('/pages/terms-conditions') }}">Terms</a> and
                                                    <a href="{{ url('/pages/privacy') }}">Privacy Policy</a></div>
                                                <div class="form-row">
                                                    <div class="inputBlock checkboxSec"> <span>
                                                        <input type="checkbox" checked="checked" value="1" name="receive_update"
                                                               id="checkboxG4" class="css-checkbox"/>
                                                        <label for="checkboxG4" class="css-label">Receive updates from us?</label>
                                                    </span></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="buttonBlock form-row col-sm-12 col-lg-9">
                                                    {!! Form::submit('Sign Up with Email Address', array('class'=>'fullwidth blue-btn-lg text-center')) !!}
                                                </div>
                                            </div>
                                        <!--<div class="text-center alreadyAccount form-row">Have an account already? <a href="{{ url('login') }}">Login Now</a></div>-->
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="details">
                                <input name="lat" type="hidden" value="">
                                <input name="lng" type="hidden" value="">
                                <input name="formatted_address" type="hidden" value="">
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    {!! JsValidator::formRequest('Modules\Auth\Http\Requests\LoginCreateRequest','#login-form') !!}
    <script src='{{ asset('modules/campaigns/js/common.js') }}'></script>
    <script src="{{ asset('modules/campaigns/js/common.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    {!! JsValidator::formRequest('Modules\Auth\Http\Requests\RegisterUserCreateRequest','#register-form') !!}
    {!! JsValidator::formRequest('Modules\Auth\Http\Requests\RegisterCharityCreateRequest','#register-form-charity') !!}
    <script src="{{ asset('modules/auth/js/form-custom.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    <script type='text/javascript'
            src="{{ asset('js/jquery.maskedinput.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    <script src="{{ asset('modules/auth/js/register-browse-pic.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    <script type="text/javascript">
        $('#searchPostCode').click(function(e) {
          
          //console.log(this.value)
          $('#postCode').attr('postCodeStatus',404);
          var string = $('#postCode').val();
          
          $.ajax({
                url: "https://api.ideal-postcodes.co.uk/v1/postcodes/"+string+"?api_key=iddqd",
                dataType: "json",
                type: "get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    $('#postCode').attr('postcodestatus',200);
                    $(".registerHiddenFields").hide();
                    $('#postCode').val(data.result[0]['postcode']);
                    $('#address').addClass('hidden');
                    $('#AddressSelectMain').removeClass('hidden');
                    $('#postCodeMessage').html('');
                    $('#addressSelect').html('<option>Please Select your address </option>');
                        
                    $.each(data.result, function(index, element) {

                    $('#addressSelect').append('<option value="'+ element.line_1+' , '+element.district+' ,'+element.country+' ">'+ element.line_1+' , '+element.district+' ,'+element.country+' </option>');
                    
                    });
                    $("#addressSelect").show();  
                    

                    console.log(data)
                },
                error:function (xhr, ajaxOptions, thrownError){

                    if(xhr.status==404) {
                        $('#postCode').attr('postcodestatus',404);
                        $('#AddressSelectMain').addClass('hidden');
                        $('#postCodeMessage').html('Please enter a valid postcode');
                        $('#address').val('');
                        $('#country').val(''); 
                        $('#district').val('');
                        console.log('No post code found');
                    }
                }
            });

            //console.log(this.value);
      });

       $('#addressSelect').on('change', function() {

          var address = this.value;
          var arr = address.split(',');
          $('#address').addClass('hidden');
          $('#address').val(arr[0]); $('#district').val(arr[1]); $('#country').val(arr[2]);  
          $(".registerHiddenFields").show();
          console.log(arr);

       });

        $('.location-link').click(function(){

             $(".registerHiddenFields").toggle();
       
        });

        $( "#register-form" ).submit(function( event ) {
            
            
            var postcode = $('#postCode').val(); 
            var status = document.getElementById("postCode").getAttribute("postCodeStatus");    
            
            if(status == 404){

                //if the postcode is not empty try to validate
                if(postcode != ''){

                    $( "#searchPostCode" ).trigger( "click" );
                    $('#postCodeMessage').html('');
                    return false;

                } else {

                    $('#postCodeMessage').html('Please enter a valid postcode');
                    return false;
                }

                
            }
            
            return true;

        });
  

    </script>
@stop