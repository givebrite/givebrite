@extends('site.layouts.master')

@section('title', 'Forgot Password')

@section('content')

<div class="container donatePage-content">
    <div class="row">
        <div class="accountWrapper">
            <?php
            $validator = JsValidator::make([
                        'email' => 'required|email',
            ]);
            ?>
            {!! $validator !!}
            {!! $validator->selector('#passowrd-update') !!}
            {!! Form::open(array('route' => ['forgot'], 'novalidate' => 'novalidate','id'=>'forgot-password')) !!}
            <div class="signUp-form">
                <div class="headingForm text-center">Forgot Password?</div>
                <div class="form-sec">
                    <div class="form-row">
                        <div class="inputBlock">
                            {!! Form::email('email', null, ['placeholder' => 'E-mail Address']) !!}
                            {!! $errors->first('email', '<div class="text-danger">:message</div>') !!}
                       </div>
                    </div>
                    <div class="buttonBlock form-row">
                        {!! Form::submit('Recover Password', array('class'=>'fullwidth blue-btn-lg text-center')) !!}
                    </div>
                    <div class="text-center alreadyAccount form-row">Remember your password? <a href="{{ url('login') }}">Login Now</a></div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('script')
<script src='{{ asset('modules/campaigns/js/common.js') }}'></script>

@stop