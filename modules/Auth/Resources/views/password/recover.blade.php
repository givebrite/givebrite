<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
    </head>
    <body style="margin:0;">
        <table cellpadding="0" cellspacing="0" width="750" style="margin: 0 auto; font-family: sans-serif;">
            <tr style="background: #f6f5f2 none repeat scroll 0 0;">
                <td style="padding:30px 30px 30px 30px; font-size:14px;text-align:center">
                    <div style="">
       			<img src="{{$message->embed(base_path().'/public/site/images/color-logo.png')}}" alt="" />
       		    </div>
                    <div style="margin: 30px 0; background-color:#fff; border-radius:10px; padding:35px 35px 20px 35px; color:#494949;display:inline-block;width:500px;text-align:left;">
                        <div style="font-size:20px;">
                            Misplaced your password I see..?
                        </div>

                        <div style="height:30px;"></div>

                        <div style="font-weight:bold; font-size:20px; margin-bottom:15px;">
                            Don't Worry
                        </div>
                        <div style="color:#494949;line-height:20px;">
                            Just click the button to reset your password <br>
                        </div>
                        <div style="height:25px;"></div>

                        <div style="display:inline-block; border-radius:10px;background-color:#37c2df;">
                            <a href="{{URL::route('reset.password',$forgot_token)}}" style="display:block; color:#fff;text-decoration:none;padding:20px 40px 20px 20px; line-height:1;"><span style="font-size:21px; font-weight:bold;padding:0 20px 0 0;">Reset Password</span></a>
                        </div>

                        <div style="height:25px;"></div>

                        <div style="font-size:20px; color:#19a8ea; font-weight:bold;">
                            Keep Sharing!
                        </div>

                        <div style="height:50px;"></div>

                    </div>
                    <div style="color:#b8b8b8;font-size:12px;line-height:1.4">
                        <span style="display:block">GiveBrite HQ</span>
                        <span style="display:block">855 Give Brite, 1st Floor, Swan Building, Swan 855</span>
                        <span style="display:block">Street, Northern Quarter, Manchester, M4 5JW</span>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>