@extends('site.layouts.master')

@section('title', 'Reset Password')

@section('content')

<div class="container donatePage-content">
    <div class="row">
        <div class="accountWrapper">
            {!! Form::open(array('route' => ['reset'], 'novalidate' => 'novalidate')) !!}
            {!! Form::hidden('token', $token ) !!}
            <div class="signUp-form">
                <div class="headingForm">Fill your registered email and new password</div>
                <div class="form-sec">
                    <div class="form-row">
                        <div class="inputBlock">
                            {!! Form::email('email', $email, ['placeholder' => 'E-mail Address', 'readonly']) !!}
                            {!! $errors->first('email', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="inputBlock">
                            {!! Form::password('password', ['placeholder' => 'Password']) !!}
                            {!! $errors->first('password', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="inputBlock">
                            {!! Form::password('password_confirmation', ['placeholder' => 'Confirm Password']) !!}
                            {!! $errors->first('password_confirmation', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                    <div class="buttonBlock form-row">
                        {!! Form::submit('Reset', array('class'=>'fullwidth blue-btn-lg text-center')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop