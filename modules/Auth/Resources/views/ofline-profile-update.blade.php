@extends('site.layouts.master')

@section('title', 'givebrite.com - Online Fundraising & Donation Platform')

@section('style')
    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <style>
        @media (max-width: 767px) {
            .tab-top-wrap {
                border-bottom: none;
            }
        }
        .inputBlock{
            width: 100%;
        }
        .form-sec .form-row.multiinputs .inputBlock{
            width: 100%;
        }
        #donation_amt
        {
            padding: 0 50px;
        }
        .form-sec .form-row.multiinputs .inputBlock:last-child {
            margin-left: 0%;
        }
        #donor_name
        {
            display: none;
        }
        .registerHiddenFields{

            display:none;
        }
        
        @media only screen and (max-width: 500px) {
        body {
            #postcode{margin-left:2%;}
        }
}
    </style>
@stop

@section('content')

{!! Form::open(array('novalidate' => 'novalidate', 'id' => 'offline-profile-update','method' => 'post')) !!}
<div class="container-fluid">
    <div class="col-sm-12 col-lg-8 col-lg-offset-2 contact-wrapper">
        <div class="laungSlogan createHeading">
            <h2 align="center">Update Profile</h2>
            <h4></h4>

        </div>
        <div class="row"> 
            
            <div class="form-sec col-md-4 col-lg-6 col-lg-offset-3">
                
               
                    
                <div class="form-row">
                    <div class="form-row multiinputs clearfix">
                        <div class="register-select col-sm-12 col-md-3 col-lg-3 ">
                        {!! Form::select('salutation', array('Mr' => 'Mr', 'Miss' => 'Miss', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Dr' => 'Dr', 'Prof' => 'Prof', 'Maulana' => 'Maulana')) !!}
                            <div class="select__arrow"></div>
                        </div>

                        <div class="col-sm-12 col-md-9 col-lg-9">
                            {!! Form::text('first_name', $user->first_name, ['class' => 'form-control','placeholder' => 'First Name','required'=>'required']) !!}
                            {!! $errors->first('first_name', '
                            <div class="text-danger">:message</div>
                            ') !!}
                        </div>
                     </div>
                </div>
               

                <div class="form-row col-sm-12">
                        {!! Form::text('sur_name', $user->sur_name, array('placeholder' => 'Surname','required'=>'required')) !!}                            
                    <div class="text-danger" id="sur_name_error">@if($errors->first('sur_name')) {{ $errors->first('sur_name') }} @endif</div>
                </div>

                <div class="form-row col-sm-12">
                        {!! Form::text('email', $user->email, array('placeholder' => 'Email', 'disabled' => 'disabled')) !!}   
                        {!! $errors->first('email', '<div class="text-danger text-left">:message</div>') !!}
                </div>

                <div class="row form-row">
                    
                    <div class="form-row multiinputs clearfix">
                        
                        <div class="col-xs-7 col-sm-7 col-md-9 col-lg-9">
                
                            {!! Form::text('postcode', $user->postcode, ['placeholder' => 'Postcode','id'=>'postcode','postCodeStatus'=>'404']) !!}
                            {!! $errors->first('postcode', '<div class="text-danger text-left">:message</div>') !!}
                                 <p class="text-primary pull-left" id="postCodeMessage"></p>

                        </div>

                        <div class="col-xs-3 col-sm-7 col-md-3 col-lg-3 searchPostCode">
                       
                            <a href="javascript:void(0)" id="searchPostCode">Search</a>

                        </div>

                       
                    </div>
                </div>

                <div class="address">
                    <!--edited-->
                    

                    <!--select dropdown-->
                     
                     <div class="form-row col-sm-12 hidden" id="AddressSelectMain">
                       
                       <div class="register-select"> 
                        
                            <select name="addressSelect" id="addressSelect" class="valid" >
                                
                                <option value="">Please Select your address</option>

                            </select>

                       </div> 
                        
                       <div class="select__arrow"></div>
                        
                    </div>
                     <!--select dropdown-->   

                    <!--edited-->
                    <div class="form-row col-sm-12">
                        <div class="inputBlock">
                            {!! Form::text('address', $user->address, ['placeholder' => 'Address','id'=>'address']) !!}
                            {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                    <div class="show-Location" id="location-div">
                        <div class="form-row col-sm-12">
                            <div class="inputBlock">
                                {!! Form::text('locality', $user->locality, ['placeholder' => 'Locality','id'=>'district']) !!}
                                {!! $errors->first('locality', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>

                        <div class="form-row col-sm-12 clearfix">
                            <div class="inputBlock">
                                {!! Form::text('country',  $user->country, ['placeholder' => 'Country','id'=>'country']) !!}
                                {!! $errors->first('country', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>
                        
                    </div>
                    <input name="lat" type="hidden">
                    <input name="lng" type="hidden">
                </div>
        

                <div class="form-row col-sm-12">
                    <button href="#" class="blue-btn-lg">Save Settings</button>
                </div>

        
        
                </div>
            </div>
        </div>
    </div>
</div>
</div>
{!! Form::close() !!}

@stop

@section('script')
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&signed_in=true&libraries=drawing&callback=initMap"
            async defer></script>
    <script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/campaign-detail.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>



    <script src="{{asset('site/js/fusioncharts.js')}}"></script>
    <script src="{{asset('site/js/fusioncharts.charts.js') }} "></script>
    <script src="{{asset('site/js/fusioncharts.theme.management-dashboard.js') }}"></script>
     <script type="text/javascript">
        $('#searchPostCode').click(function(e) {
          
          //console.log(this.value)
          var string = $('#postcode').val();

          $.ajax({
                url: "https://api.ideal-postcodes.co.uk/v1/postcodes/"+string+"?api_key=iddqd",
                dataType: "json",
                type: "get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    $('#postcode').attr('postcodestatus',200);
                    /*
                    $('#postCode').val(data.result[0]['postcode']);
                    $('#postCodeMessage').html('post code verified');
                    $('#country').val(data.result[0]['country']); 
                    $('#district').val(data.result[0]['district']);
                    */
                    $('#postcode').val(data.result[0]['postcode']);
                    // $('#postCodeMessage').html('post code verified');
                    $('#AddressSelectMain').removeClass('hidden');
                    $('#addressSelect').html('<option>Please Select your address </option>');
                        
                    $.each(data.result, function(index, element) {

                    $('#addressSelect').append('<option value="'+ element.line_1+' , '+element.district+' ,'+element.country+' ">'+ element.line_1+' , '+element.district+' ,'+element.country+' </option>');
                    
                    });
                    $("#addressSelect").show();  
                    

                    console.log(data)
                },
                error:function (xhr, ajaxOptions, thrownError){

                    if(xhr.status==404) {
                        $('#postcode').attr('postcodestatus',404);
                        $('#AddressSelectMain').addClass('hidden');
                        $('#postCodeMessage').html('Please enter a valid postcode');
                        $('#address').val('');
                        $('#country').val(''); 
                        $('#district').val('');
                        console.log('No post code found');
                    }
                }
            });

            //console.log(this.value);
      });

       $('#addressSelect').on('change', function() {

          var address = this.value;
          var arr = address.split(',');
          $('#address').val(arr[0]); $('#district').val(arr[1]); $('#country').val(arr[2]);  
          $(".registerHiddenFields").show();
          console.log(arr);

       });

        $('.location-link').click(function(){

              $("#addressSelect").hide(); $('#postCodeMessage').html(''); 
              $('#address').val(''); $('#district').val(''); $('#country').val('');  $('#postCode').val('');
              $(".registerHiddenFields").toggle();
       
        });

        $( "#register-form" ).submit(function( event ) {
            
            var status = document.getElementById("postCode").getAttribute("postCodeStatus");    
            
            if(status == 404){

                $('#postCodeMessage').html('Please enter a valid postcode');
                return false;
            }
            return true;

        });
  

    </script>
    
@stop