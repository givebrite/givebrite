@extends('site.layouts.master')

@section('title', 'Resend')

@section('content')

<div class="container donatePage-content">
    <div class="row">
        <div class="accountWrapper">
            {!! Form::open(array('url' => 'resend', 'novalidate' => 'novalidate')) !!}
            <div class="signUp-form">
                <div class="headingForm">Fill your registered email</div>
                <div class="form-sec">
                    <div class="form-row">
                        <div class="inputBlock">
                            {!! Form::email('email', null, ['placeholder' => 'E-mail Address']) !!}
                            {!! $errors->first('email', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                    <div class="buttonBlock form-row">
                        {!! Form::submit('Submit', array('class'=>'fullwidth blue-btn-lg text-center')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop