<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
    </head>
    <body style="margin:0;">
        <table cellpadding="0" cellspacing="0" width="750" style="margin: 0 auto; font-family: sans-serif;">
            <tr style="background: #f6f5f2 none repeat scroll 0 0;">
                <td style="padding:30px 30px 30px 30px; font-size:14px;text-align:center">
                    <div style="">
       			<img src="{{$message->embed(base_path().'/public/site/images/color-logo.png')}}" alt="" />
       		    </div>
       		    <div style="margin: 30px 0; background-color:#fff; border-radius:10px; padding:35px 35px 20px 35px; color:#494949;display:inline-block;width:500px;text-align:left;">
                        <div style="font-size:20px;">
                            Thank you for registering.
                        </div>

                    <div style="height:30px;">
                         A GiveBrite team member will be verifying your charity and will notify you soon as your account is active.
                    </div>

                        <div style="font-weight:bold; font-size:20px; margin-bottom:15px;">
                        </div>
                        
                        <div style="height:25px;"></div>
                        
                        <div style="height:50px;"></div>

                    </div>
                    <div style="color:#b8b8b8;font-size:12px;line-height:1.4">
                        <span style="display:block">GiveBrite HQ</span>
                        <span style="display:block">855 Give Brite, 1st Floor, Swan Building, Swan 855</span>
                        <span style="display:block">Street, Northern Quarter, Manchester, M4 5JW</span>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>