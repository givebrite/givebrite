<?php

namespace Modules\Auth\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Auth;
use Toastr;
use Config;

class IsUserMiddleware
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    protected $role;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, RoleUserRepo $role)
    {
        $this->auth = $auth;
        $this->role = $role;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($this->auth->check()) {
            $user_role = $this->role->findByUserId(Auth::user()->id);
            if ($user_role) {
                if ($user_role->role_id == Config::get('config.role_charity')) {
                    return redirect('/dashboard');
                }
            }
        }
        return $next($request);
    }

}
