<?php

namespace Modules\Auth\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Route;
use Session;

class IsLoginMiddleware
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        //Get Login User Details
        $user = Session::get('user_session_data');
        
        if ($this->auth->check()) {
            if(!empty($user) && count($user)>0){
                return $next($request);
            }else{
                // Logout And Destroy Session
                Auth::logout();
                // Remove Values From Session
                Session::flush();
                return redirect('/login');
            }
            
        } else {
            if (Route::getCurrentRoute()->getPath() === "campaigns/create") {
                Session::forget('campaign');
                $create_campaign = true;
                Session::set('create_campaign', $create_campaign);
            }
            return redirect('/login');
        }
    }

}
