<?php

namespace Modules\Auth\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Modules\Auth\Http\Controllers\LoginController;
use Auth;
use Toastr;

class IsActivatedMiddleware
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    protected $login;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, LoginController $login)
    {
        $this->auth = $auth;
        $this->login = $login;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            if( $this->login->isActive(Auth::user()->id))
            {
                return $next($request);
            }
            return redirect('/dashboard');
        }
        return redirect('/login');
    }

}
