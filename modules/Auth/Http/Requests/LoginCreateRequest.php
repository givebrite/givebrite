<?php 

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\Request;
use Lang;

class LoginCreateRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Basic Contact Validation  
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];
        return $rules;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        // Basic Contact Validation  
        $messages = [
           'email.required'=>Lang::get('auth::validations.user-auth.email-required'),
           'email.email'=>Lang::get('auth::validations.user-auth.email-email'),
           'password.required'=>Lang::get('auth::validations.user-auth.password-required'),
           'password.min'=>Lang::get('auth::validations.user-auth.password-min'),
        ];

        return $messages;
        
    }
}
