<?php 

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\Request;
use Lang;
use Session;

class UpdateUserProfileOfflineRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['first_name']='required|min:2|max:55';
        $rules['sur_name']='required|max:55';
        $rules['postcode'] = 'required';
        $rules['address'] = 'required';
        $rules['country'] = 'required';
        $rules['locality'] = 'required';
        
       return $rules;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        // Basic Contact Validation  
        $messages = [
            'first_name.required'=>'First name is required.',
            'address.required'=>'Address is required.',
            'postcode.required'=>'Postcode is required.',
            'sur_name.required' => Lang::get('auth::validations.user-auth.sur_name-required'),
            'sur_name.min' => Lang::get('auth::validations.user-auth.sur_name-min'),
            'country.required'=>'Country is required.',
            'locality.required'=>'Locality is required.',
            'email.required'=>'Email is required.',
            'charity_name.required'=>'Charity name is required.',
            'contact_telephone.required'=>'Contact telephone is required.',
            'contact_telephone.min' => Lang::get('auth::validations.user-auth.contact-telephone-min'),
            'web.required'=>'Web is required.',
            'description.required'=>'Description is required.',
        ];

        return $messages;
        
    }
}
