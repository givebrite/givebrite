<?php

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\Request;
use Lang;

class RegisterUserCreateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        // Basic Contact Validation  
        $rules = [
            'first_name' => 'required|min:2|max:55',
            'sur_name' => 'required|max:55',
//            'profile_pic' => 'image',
            'email' => 'required|email|unique:users',
            'email_confirm' => 'required|same:email',
            'password' => 'required|min:6|max:55',
            'address'=>'required',
            'country'=>'required',
            'locality'=>'required',
            'postcode'=> 'required'
        ];
        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages() {

        // Basic Contact Validation  
        $messages = [
            'first_name.required' => Lang::get('auth::validations.user-auth.first_name-required'),
            'first_name.min' => Lang::get('auth::validations.user-auth.first_name-min'),
            'sur_name.required' => Lang::get('auth::validations.user-auth.sur_name-required'),
            'sur_name.min' => Lang::get('auth::validations.user-auth.sur_name-min'),
            'email.required' => Lang::get('auth::validations.user-auth.email-required'),
            'email.email' => Lang::get('auth::validations.user-auth.email-email'),
            'email.unique' => Lang::get('auth::validations.user-auth.email-unique'),
            'password.required' => Lang::get('auth::validations.user-auth.password-required'),
            'password.min' => Lang::get('auth::validations.user-auth.password-min'),
            'postcode.required' => Lang::get('auth::validations.user-auth.postcode-required'),
            'country.required' => Lang::get('campaigns::validations.campaign-design.location-required'),
            'locality.required' => Lang::get('campaigns::validations.campaign-design.city-required')
        ];

        return $messages;
    }

}
