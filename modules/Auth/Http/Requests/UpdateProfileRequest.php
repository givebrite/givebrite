<?php 

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\Request;
use Lang;
use Session;

class UpdateProfileRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //Get Login User Details
        $user= Session::get('user_session_data');
        // Basic Contact Validation 
        if($user['role']=='Charity'){
            $rules['charity_name']='required|min:2|max:55';
            $rules['charity_logo']='mimes:jpeg,png,gif,jpg';
            $rules['contact_telephone']='required|min:11';
            $rules['web']=['required', 'regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'];
            $rules['description']='required|max:500';
        }
        else{
            $rules['first_name']='required|min:2|max:55';
            $rules['sur_name']='max:55';
            $rules['profile_pic']= 'mimes:jpeg,png,gif,jpg';
        }
        $rules['postcode'] = 'required';
        $rules['address'] = 'required';
        $rules['country'] = 'required';
        $rules['locality'] = 'required';
        
       return $rules;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        // Basic Contact Validation  
        $messages = [
            'first_name.required'=>'First name is required.',
            'address.required'=>'Address is required.',
            'postcode.required'=>'Postcode is required.',
            'country.required'=>'Country is required.',
            'locality.required'=>'Locality is required.',
            'email.required'=>'Email is required.',
            'charity_name.required'=>'Charity name is required.',
            'contact_telephone.required'=>'Contact telephone is required.',
            'contact_telephone.min' => Lang::get('auth::validations.user-auth.contact-telephone-min'),
            'web.required'=>'Web is required.',
            'description.required'=>'Description is required.',
        ];

        return $messages;
        
    }
}
