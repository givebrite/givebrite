<?php

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\Request;
use Lang;

class RegisterCharityCreateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**git commit --amend -m "GB-70,GB-71 ,GB-72"
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        // Basic Contact Validation  
        $rules = [
            'email' => 'required|email|unique:users',
            'email_confirm' => 'required|same:email',
            'password' => 'required|min:6|max:55',
            'registration_no' => 'required|numeric|max:9999999999999999',
            'charity_name' => 'required|min:2|max:55',
            'address' => 'required|max:255',
            'contact_telephone' => 'required|min:11',
            'charity_logo' => 'required|mimes:jpeg,png,gif,jpg',
            'web' => ['required', 'regex:/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i'],
            'description' => 'required|max:500',
            'country' => 'required',
            'locality' => 'required',
            'postcode' => 'required',
        ];
        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages() {

        // Basic Contact Validation  
        $messages = [
            'email.required' => Lang::get('auth::validations.user-auth.email-required'),
            'email.email' => Lang::get('auth::validations.user-auth.email-email'),
            'password.required' => Lang::get('auth::validations.user-auth.password-required'),
            'password.min' => Lang::get('auth::validations.user-auth.password-min'),
            'charity_name.required' => Lang::get('auth::validations.user-auth.charity-name-required'),
            'charity_name.min' => Lang::get('auth::validations.user-auth.charity-name-min'),
            'address.required' => Lang::get('auth::validations.user-auth.address-required'),
            'postcode.required' => Lang::get('auth::validations.user-auth.postcode-required'),
            'country.required' => Lang::get('campaigns::validations.campaign-design.location-required'),
            'locality.required' => Lang::get('campaigns::validations.campaign-design.city-required'),
            'postcode.numeric' => Lang::get('auth::validations.user-auth.postcode-numeric'),
            'contact_telephone.required' => Lang::get('auth::validations.user-auth.contact-telephone-required'),
            'contact_telephone.min' => Lang::get('auth::validations.user-auth.contact-telephone-min'),
            'web.required' => Lang::get('auth::validations.user-auth.web-required'),
            'web.url' => Lang::get('auth::validations.user-auth.web-url-format'),
            'description.required' => Lang::get('auth::validations.user-auth.description-required'),
            'registration_no.required' => Lang::get('auth::validations.user-auth.registration-no-required'),
            'registration_no.numeric' => Lang::get('auth::validations.user-auth.registration-no-numeric'),
        ];

        return $messages;
    }

}
