<?php 

namespace Modules\Auth\Http\Requests;

use App\Http\Requests\Request;

class SocialCreateRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Basic Contact Validation  
        $rules = [
            'email' => 'email|unique:users'
        ];
        return $rules;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        // Basic Contact Validation  
        $messages = [
           
        ];

        return $messages;
        
    }
}
