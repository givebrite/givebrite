<?php

Route::group(['namespace' => 'Modules\Auth\Http\Controllers'], function () {

Route::get('offline-profile-update/{unique_key}', ['uses' => 'RegisterController@offlineProfileUpdate']);
Route::post('offline-profile-update/{unique_key}', ['uses' => 'RegisterController@offlineProfileUpdatePost']);

});

Route::group(['namespace' => 'Modules\Auth\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\LoginMiddleware', 'App\Http\Middleware\IsUnsecure']], function () {

    //Route::get('/', 'AuthController@index');
    ////Routing for login
    // GET route

    // Route::get('login', [
    // 	'uses' => 'LoginController@getLoginDefault',
    // 	'as' => 'login',
    // ]);
    
    Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
    Route::get('/callback', 'SocialAuthController@callback');
    Route::get('/callback/{provider}', 'SocialAuthController@callback');
    Route::get('login', [
        'uses' => 'LoginController@getLogin',
        'as' => 'login',
    ]);

            
    Route::post('login', [
        'uses' => 'LoginController@postLogin',
        'as' => 'login',
    ]);

    // Routing for login using social media
    Route::get('/auth/callback/{media}', [
        'uses' => 'SocialController@handleProviderCallback',
        'as' => 'auth.getSocialAuthCallback',
    ]);

    Route::get('auth/{operation}/{media}', [
        'uses' => 'SocialController@redirectToProvider',
        'as' => 'auth.getSocialAuth',
    ]);

    //Routing for registration
    // GET route
    //Route::get('register', 'RegisterController@getRegister');
    Route::get('copy-data', ['uses' => 'RegisterController@copyData']);
    Route::get('offline-profile-update-mail', ['uses' => 'RegisterController@sendMailUpdateDetails']);
    

    Route::get('register', [
        'uses' => 'RegisterController@getRegister',
        'as' => 'register',
    ]);

    Route::get('charityregister', [
        'uses' => 'RegisterController@getCharity',
        'as' => 'charityregister',
    ]);

    //POST route
    Route::post('register', [
        'uses' => 'RegisterController@postRegister',
        'as' => 'register',
    ]);

//    Route::get('offlineregister', [
//        'uses' => 'RegisterController@addOfflineAddress',
//        'as' => 'offline.register',
//    ]);

    //Forgot routes
    Route::get('forgot', [
        'uses' => 'RegisterController@getForgot',
        'as' => 'forgot',
    ]);

    //POST route
    Route::post('forgot', [
        'uses' => 'RegisterController@postForgot',
        'as' => 'forgot',
    ]);

    Route::get('reset/{token}', [
        'uses' => 'RegisterController@getReset',
        'as' => 'reset.password',
    ]);

    Route::post('reset', [
        'uses' => 'RegisterController@postReset',
        'as' => 'reset',
    ]);

    Route::post('updateProfile', ['uses' => 'RegisterController@updateUser', 'as' => 'updateUser']);
});

Route::group(['namespace' => 'App\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'Modules\Auth\Http\Middleware\IsActivatedMiddleware', 'App\Http\Middleware\IsUnsecure']], function () {
    Route::get('/406', 'SiteController@page406');
});

Route::group(['namespace' => 'Modules\Auth\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'Modules\Auth\Http\Middleware\IsUserMiddleware', 'App\Http\Middleware\IsUnsecure']], function () {
    //Resend account verification
    Route::get('resend', [
        'uses' => 'RegisterController@getResendAccountVerification',
        'as' => 'resend',
    ]);
});

Route::group(['namespace' => 'Modules\Auth\Http\Controllers', 'App\Http\Middleware\IsUnsecure'], function () {

    //Account verification route
    Route::get('register/verify/{verification_code}', [
        'uses' => 'RegisterController@verifyAccount',
        'as' => 'email.verify',
    ]);

    Route::get('unsubscribe/{slug}', [
        'uses' => 'RegisterController@unsubscribe',
        'as' => 'unsubscribe',
    ]);
});
