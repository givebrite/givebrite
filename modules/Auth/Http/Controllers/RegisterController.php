<?php

//

namespace Modules\Auth\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Config;
use Helper;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\Auth\Http\Controllers\LoginController;
use Modules\Auth\Http\Requests\RegisterCharityCreateRequest;
use Modules\Auth\Http\Requests\RegisterUpdateRequest;
use Modules\Auth\Http\Requests\UpdateUserProfileOfflineRequest;
use Modules\Auth\Http\Requests\RegisterUserCreateRequest;
use Modules\Auth\Repositories\Users\EloquentPasswordResetRepository as PasswordResetRepo;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Modules\Auth\Repositories\Users\EloquentUserActivationRepository as UserActivationRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Pingpong\Admin\Uploader\ImageUploader;
use Pingpong\Modules\Routing\Controller;
use Redirect;
use Toastr;

class RegisterController extends Controller
{

    protected $charityLogoStoragePath, $uploader, $user, $userProfile, $userActivation, $passwordReset, $roleUser, $login;

    function __construct(ImageUploader $uploader, UserRepo $user, UserProfileRepo $userProfile, UserActivationRepo $userActivation, PasswordResetRepo $passwordReset, RoleUserRepo $roleUser, LoginController $login)
    {
        $this->uploader = $uploader;
        $this->charityLogoStoragePath = Config::get('config.charity_logo_upload_path');
        $this->user = $user;
        $this->userProfile = $userProfile;
        $this->userActivation = $userActivation;
        $this->passwordReset = $passwordReset;
        $this->roleUser = $roleUser;
        $this->login = $login;
    }

    /**
     * get Register route action
     */
    public function getRegister()
    {
        return view('auth::register');
    }

    public function getCharity()
    {
        
        return view('auth::charityregister');
    }

    public function copyData(){

        $data = new UserProfileRepo();
        $copyData = $data->copyData();
        dd($copyData);

    }
    
    //Get users that have gift aid selected and uncomplete details
    //This will be used just once 
    public function sendMailUpdateDetails(){

        //Raw::gets user details that have gift aid selected 

        //SELECT DISTINCT(U.email),U.salutation,UP.first_name,UP.sur_name,UP.house_number,UP.street_name,UP.addition_to_address,UP.address,UP.postcode,UP.country,UP.locality FROM campaign_donations  CD JOIN users U  ON U.id = CD.user_id  JOIN user_profile UP ON UP.user_id = U.id WHERE CD.is_giftaid = 1 
        $data = new UserProfileRepo();
        $usersWithGIftAidSelected = $data->usersWithGIftAidSelected();
        

        $mailSendTo = array();

        foreach($usersWithGIftAidSelected as $users){
            
            if(($users->salutation == '') || ($users->first_name == '') || ($users->sur_name == '') || 
                 ($users->address == '')){
                
                //$mailSendTo[] = $users->email;
                //insert unique field
                $uniqueKey = Helper::generateRandomString();
                //$data->createUniqueUpdateKey($users->user_id,$uniqueKey);
                $uniqueKey = $data->getUniqueUpdateKey($users->user_id);
                $email = $users->email.'-'.$uniqueKey;
                $first_name = $users->first_name;
                //Send mail here
                // Mail::send('email.profile_update_request', ["url"=>$uniqueKey], function ($m) use ($email,$first_name) {
                //     $m->from('info@givebrite.com', 'GiveBrite');
                //     $m->to($email, $first_name)->subject('URGENT ACTION: Update Your Gift Aid information ');
                // });
               

            } 
        }
        
     }



    public function offlineProfileUpdate($uniquekey){
        
        $profile = new UserProfileRepo();
        $userID  = $profile->getUserIdFromUniqueUpdateKey($uniquekey);
        
        if(!$userID){

            $msg = "The link has expired, please contact the site admin for more details";
            return view('auth::ofline-profile-update-empty',['msg'=>$msg]);

        }

        $user    = $this->user->findById($userID);
        // /dd($user);
        return view('auth::ofline-profile-update',['user'=>$user]);
    }

    public function offlineProfileUpdatePost($uniquekey){

        //check if the unique key is ok
        $profile = new UserProfileRepo();
        $userID  = $profile->getUserIdFromUniqueUpdateKey($uniquekey);
        
        if(!$userID){

            $msg = "The link has expired, please contact the site admin for more details";
            return view('auth::ofline-profile-update-empty',['msg'=>$msg]);

        }


        //get request rules
        $request_rule = new UpdateUserProfileOfflineRequest();
        $rules = $request_rule->rules();
        //get form input
        $input = Input::all();

        //check validations on form inputs
        $validator = Validator::make($input, $rules, $request_rule->messages());

        if ($validator->fails()) {

            Toastr::Error("Input validation failed", $title = null, $options = []);
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $house_number = '';
        $street_name = '';
        $address_1 = '';
        $address_2 = '';
        

        $address = Helper::splitAddress(Input::get('address'));
        
        if(!empty($address['houseNumber'])){

            $house_number = $address['houseNumber'];
        }

        if(!empty($address['streetName'])){

            $street_name = $address['streetName'];
        }

        if(!empty($address['additionToAddress1'])){

            $address_1 = $address['additionToAddress1'];
        }

        if(!empty($address['additionToAddress2'])){

            $address_2 = $address['additionToAddress2'];
        }

        $user = $this->user->findById($userID);
        
        //update user table 
        $data  = array('salutation'=>Input::get('salutation'), 'name' => Input::get('first_name').' '.Input::get('sur_name'),'gift_aid_details_update'=>1);

        $this->user->updateByEmail($data,$user->email);
        
        //update user profile table
        $profileData = array(
                             'first_name'  => Input::get('first_name'),
                             'sur_name'    => Input::get('sur_name'),
                             'house_number'=> $house_number,
                             'street_name' => $street_name,
                             'addition_to_address'=> $address_1.''.$address_2,
                             'address'     => Input::get('address'),
                             'postcode'    => Input::get('postcode'),
                             'locality'    => Input::get('locality'),
                             'country'     => Input::get('country')
                             
                            );

        $profile->updateOffline($userID,$profileData);
        
        //expire the link
        $profile->updateLinkStatusFromUniqueUpdateKey($uniquekey);

        if($profile){

            $msg = "Thanks for Updating your details !";
            return view('auth::ofline-profile-update-empty',['msg'=>$msg]);

        }

         $msg = "The update failed ! please try again";
         return view('auth::ofline-profile-update-empty',['msg'=>$msg]);
    }


    /**
     * function for user_registration
     * @return type
     */
    public function postRegister()
    {   
        $house_number = '';
        $street_name = '';
        $address_1 = '';
        $address_2 = '';
        

        $address = Helper::splitAddress(Input::get('address'));
        
        if(!empty($address['houseNumber'])){

            $house_number = $address['houseNumber'];
        }

        if(!empty($address['streetName'])){

            $street_name = $address['streetName'];
        }

        if(!empty($address['additionToAddress1'])){

            $address_1 = $address['additionToAddress1'];
        }

        if(!empty($address['additionToAddress2'])){

            $address_2 = $address['additionToAddress2'];
        }

        //print_r(Input::all());

        //dd($address);
        //check if user wants to sign up as charity or indiviual
        $sign_up_as_indiviual = Input::get('sign_up_option') == 'charity' ? '' : 'indiviual';

        //get request rules
        $request_rule = $sign_up_as_indiviual ? new RegisterUserCreateRequest() : new RegisterCharityCreateRequest();
        $rules = $request_rule->rules();
        //get form input
        $input = Input::all();

        //check validations on form inputs
        $validator = Validator::make($input, $rules, $request_rule->messages());
        // If Image Is Valid Than Upload Image
        if (empty($validator->messages()->getMessages()['charity_logo'])) {
            $charity_logo = "";
            if (\Input::hasFile('charity_logo') && !isset($validator->errors()->charity_logo)) {
                if (!empty(Session::get('charity_logo'))) {
                    $previousImage = Session::get('charity_logo');
                    Helper::unlinkImages($previousImage, 'charity');
                }
                // upload image
                $image = $this->uploader->upload('charity_logo');
                $image->save($this->charityLogoStoragePath);
                $charity_logo = $image->getFilename();
                $image_detail = array(
                    'inputFile' => Input::file('charity_logo'),
                    'image' => $image,
                    'thumb_size' => Config::get('config.thumb_charity_size'),
                    'thumb_path' => Config::get('config.thumb_charity_logo_upload_path'),
                    'large_size' => Config::get('config.large_charity_size'),
                    'large_path' => Config::get('config.large_charity_logo_upload_path'),
                );
                Helper::createThumb($image_detail);
                // Forget Previous session
                Session::forget('charity_logo');
                // Set Image Name In Session
                Session::set('charity_logo', $charity_logo);
            }
        }

        if ($validator->fails()) {
            Toastr::Error("Input validation failed", $title = null, $options = []);
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $user_name = $sign_up_as_indiviual ? Input::get('first_name') . ' ' . Input::get('sur_name') : Input::get('charity_name');
        $email = Input::get('email');
        $password = Input::get('password');
        $salutation = Input::get('salutation');

        //creates user data array for users table
        $user_data = array(
            'salutation' => $salutation,
            'name' => $user_name,
            'email' => $email,
            'password' => Hash::make($password),
        );
        
        $social_email_check_flag = false;
        $social_profile_image_flag = false;
        $social_profile_image = "";

        if ($sign_up_as_indiviual) {

            //checks if registration is through social media
            if (Input::get('sign_up_tag') != '' && Input::get('sign_up_id') != '') {
                $user_data['sign_up_tag'] = Input::get('sign_up_tag');
                $user_data['sign_up_id'] = Input::get('sign_up_id');

                //check if social media user email and email coming from form are same
                $social_email = Session::has('social_email') ? Session::get('social_email') : '';
                $social_email_check_flag = ($social_email == $email) ? 'true' : 'false';

                //get social media image with complete path
                $social_profile_image_flag = true;
                $social_profile_image = Input::get('social_profile_image');
            }
        }

        //creates User model and user entry in the users table
        $user_response = $this->user->create($user_data);

        //get user id
        $user_id = $user_response->id;

        $role_id = $sign_up_as_indiviual ? Config::get('config.role_user') : Config::get('config.role_charity');

        //create role user data array for role_user table
        $role_user_data = array(
            'role_id' => $role_id,
            'user_id' => $user_id,
        );

        //creates UserProfile model and user profile entry in the user_profile table
        $this->roleUser->create($role_user_data);

        $name = (!$sign_up_as_indiviual) ? Input::get('charity_name') : $user_name;
        $unique_slug = $this->userProfile->getUniqueSlug($name, 0);
        
        //TODO :: SPlit the address

        if (!$sign_up_as_indiviual) {

            $user_profile_data = array(
                'user_id' => $user_id,
                'charity_logo' => $charity_logo,
                'registration_no' => Input::get('registration_no'),
                'charity_name' => Input::get('charity_name'),
                'house_number' => $house_number,
                'street_name' => $street_name,
                'addition_to_address' => $address_1.' '.$address_2,
                'address' => Input::get('address'),//Input::get('address'),
                'postcode' => Input::get('postcode'),
                'contact_telephone' => Input::get('contact_telephone'),
                'web' => Input::get('web'),
                'description' => Input::get('description'),
                'slug' => $unique_slug,
                'country' => Input::get('country'),
                'locality' => Input::get('locality'),
                'lat' => round(Input::get('lat'), 4),
                'lng' => round(Input::get('lng'), 4),
            );

            $user_profile_response = $this->userProfile->create($user_profile_data);

            $this->setUserActiveStatus($user_id, 0);

            //logs the user in and redirect to dashboard
            $redirect_url = $this->login->logUserIn($user_response);
            //call to account verification mail function
            $this->sendAccountVerification($user_id, $email);
            //return view with message
            Toastr::success("Thanks for signing up with GiveBrite!", $title = null, $options = []);
            // Forget Previous session
            Session::forget('charity_logo');
            //return view
            
        

            if (Session::has('campaign')) {
                return Redirect::route($redirect_url, Session::get('campaign'));
            }
            return Redirect::route($redirect_url);
        }

        //check for receive update from form input
        $receive_update = 1;

        //create user profile data array for user_profile table
                     
        $user_profile_data = array(
            'user_id' => $user_id,
            'first_name' => Input::get('first_name'),
            'sur_name' => Input::get('sur_name'),
            'recieve_updates' => $receive_update,
            'slug' => $unique_slug,
            'postcode' => empty(Input::get('postcode')) ? '' : Input::get('postcode'),
            'house_number' => $house_number,
            'street_name' => $street_name,
            'addition_to_address' => $address_1.' '.$address_2,
            'address' => Input::get('address'),//Input::get('address'),
            'country' => empty(Input::get('country')) ? '' : Input::get('country'),
            'locality' => empty(Input::get('locality')) ? '' : Input::get('locality'),
            'lat' => round(Input::get('lat'), 4),
            'lng' => round(Input::get('lng'), 4),
        );

        //check if social media image exists and makes entry with complete url
        if ($social_profile_image_flag) {
            $user_profile_data['profile_pic'] = $social_profile_image;
        }

        //creates UserProfile model and user profile entry in the user_profile table
        $user_profile_response = $this->userProfile->create($user_profile_data);

        //if social media user email and email coming from form are same, no need to send email verification link
        if ($social_email_check_flag == 'true') {

            $this->setUserActiveStatus($user_id, 1);

            //logs the user in and redirect to dashboard
            if (Session::has('loginuri')) {
                $redirect_url = $this->login->logUserIn($user_response);
                $redirect_url = Session::get('loginuri');
            } else {
                $redirect_url = $this->login->logUserIn($user_response);
            }
            //return view with message
            Toastr::success("Thanks for signing up with GiveBrite!", $title = null, $options = []);
            //return view
            if (Session::has('campaign')) {
                return Redirect::route($redirect_url, Session::get('campaign'));
            }
            return Redirect::route($redirect_url);
        } else {
            //call to account verification mail function
            //$this->sendAccountVerification($user_id, $email);
            //return view with message
            $this->setUserActiveStatus($user_id, 1);

            Toastr::success("Thanks for signing up!", $title = null, $options = []);
            //logs the user in and redirect to dashboard
            if (Session::has('loginuri')) {
                $redirect_url = $this->login->logUserIn($user_response);
                $redirect_url = 'donate';
                return Redirect::route($redirect_url, Session::get('loginuri'));
            } else {
                $redirect_url = $this->login->logUserIn($user_response);
            }
            //return view
            if (Session::has('campaign')) {
                return Redirect::route($redirect_url, Session::get('campaign'));
            }
            return Redirect::route($redirect_url);
        }
    }

    /**
     * function for user account verification
     * @param string $verification_code
     * @return type
     */
    public function verifyAccount($verification_code)
    {

        //creates a array of verification token and corresponding user encrypted email by string splitter "&&"
        $verification_info = explode("&&", $verification_code);

        // check if array key exists
        if (array_key_exists(1, $verification_info)) {

            //get verification token
            $verification_code = $verification_info[0];

            //get encrypted email
            $verification_encrypted_email = $verification_info[1];

            $activation_users = $this->userActivation->findAll();

            //loop through fetched values
            foreach ($activation_users as $activation_user) {

                //check if token in table matches received token
                if ($activation_user->token_code == $verification_code) {

                    //get the day difference in created date and present date
                    $created = new Carbon($activation_user->created_at);
                    $now = Carbon::now();
                    $difference = ($created->diff($now)->format('%D') < 1) ? 1 : $created->diff($now)->format('%D');
                    //check difference if is less than 7 days

                    if ($difference < 7) {

                        ////creates User model and find entries from the users table by ID
                        $user_response = $this->user->findById($activation_user->user_id);

                        //check if results are fetched
                        if ($user_response) {

                            //check if email matches
                            if (md5($user_response->email) == $verification_encrypted_email) {

                                if ($activation_user->is_activated == 1) {
                                    //return view with message
                                    Toastr::Error("Token already used", $title = null, $options = []);
                                    return Redirect::route('404');
                                }

                                //call to function for account activation confirmation
                                $this->confirmedAccount($activation_user->id);

                                //logs the user in and redirect to dashboard
                                $redirect_url = $this->login->logUserIn($user_response);

                                $user_session_data = Session::get('user_session_data');
                                $user_session_data['active'] = true;
                                Session::set('user_session_data', $user_session_data);
                                //return view with message
                                Toastr::success("Thanks for verifying your email!", $title = null, $options = []);
                                //return view
                                if (Session::has('campaign')) {
                                    return Redirect::route($redirect_url, Session::get('campaign'));
                                }
                                return Redirect::route($redirect_url);
                            }
                        }
                    } else {
                        //return view with message
                        Toastr::Error("Your email verification link expired! Resend account verification link", $title = null, $options = []);
                        return Redirect::route('404');
                    }
                }
            }

            //return view with message
            Toastr::Error("Token Mismatch", $title = null, $options = []);
            return Redirect::route('404');
        }
        //return view with message
        Toastr::Error("Sorry, Account not verified", $title = null, $options = []);
        return Redirect::route('404');
    }

    /**
     * function for account activation confirmation
     * @param int $id
     */
    protected function confirmedAccount($id)
    {
        //create user activation data array for user_activation table
        $user_updated_data = array('is_activated' => 1);

        //creates UserActivation model and update is_activated field in the user_activation table
        $user_response = $this->userActivation->updateById($user_updated_data, $id);
    }

    /**
     * get ResendAccountVerification route action
     * @return type
     */
    public function getResendAccountVerification()
    {
        //get email
        $email = Session::get('user_session_data')['email'];
        //get user id
        $user_id = Session::get('user_session_data')['id'];

        //create Login class object and check if user is active
        $active_user = $this->login->isActive($user_id);

        if (!$active_user) {

            //call to account verification mail function
            $this->sendAccountVerification($user_id, $email);

            //return view with message
            Toastr::Success("Please check your email for email verification link.", $title = null, $options = []);
            return redirect('/dashboard');
        } else {
            //return view with message
            Toastr::Error("Entered Email is already activated", $title = null, $options = []);
            return Redirect::back();
        }
    }

    /**
     * function for checking if email already exists in the table
     * @param email $email
     * @return type
     */
    public function isEmailExist($email)
    {
        //creates user model and find user from users table by email
        $user_response = $this->user->findByEmail($email);

        //check if user object exists
        if ($user_response) {
            //return user object
            return $user_response;
        }
    }

    /**
     * function for sending mail with account verification token
     * @param int $user_id
     * @param email $email
     */
    public function sendAccountVerification($user_id, $email)
    {
        // Check User Role
        $userRole = new RoleUserRepo();
        $userRole = $userRole->findByUserId($user_id);
        if ($userRole->role_id == '3') {
            //send mail
            Mail::send('auth::email.charity', array(), function ($message) use ($email) {
                $message->to($email, 'Admin')->subject('Thank You For Registering');
            });
        } else {
            $user_profile_response = $this->userProfile->findByUserId($user_id);
            $first_name = $user_profile_response->first_name;

            //creates UserActivation model and find all entries from the user_activation table
            $user_active_users = $this->userActivation->findAll();

            //get random verification token
            $confirmation_code = str_random(30);

            //check if verification token already exists, regenerate if already exists
            if ($user_active_users) {
                foreach ($user_active_users as $user_active_user) {
                    while ($user_active_user->token_code === $confirmation_code) {
                        $confirmation_code = str_random(30);
                    }
                }
            }

            //creates user activation data array
            $user_activation_data = array(
                'user_id' => $user_id,
                'token_code' => $confirmation_code,
                'is_activated' => 1,
            );

            //loop through all entries of user_activation table
            foreach ($user_active_users as $user_active_user) {

                //check if user with some verification token already exists
                if ($user_active_user->user_id == $user_id) {

                    //delete if any entries are already in the table
                    $this->userActivation->delete($user_active_user->id);
                }
            }

            //creates user activation entry in the user_activation table
            $user_active_response = $this->userActivation->create($user_activation_data);

            //creates token string with verification token and user-email
            $confirmation_code_string = $confirmation_code . '&&' . md5($email);

            //send mail
            if ($userRole->role_id != '2') {
                Mail::send('auth::email.verify', array('confirmation_code' => $confirmation_code_string, 'first_name' => $first_name), function ($message) use ($email) {
                    $message->to($email, 'Admin')->subject('Verify your email address');
                });
            }
        }
    }

    /**
     * get forgot route action
     * @return type
     */
    public function getForgot()
    {
        return view('auth::password.forgot');
    }

    /**
     * function for get email for password reset
     * @return type
     */
    public function postForgot()
    {
        //get request rules
        $request_rule = new RegisterUpdateRequest();
        $rules = [
            'email' => 'required|email',
        ];

        //get form input
        $input = Input::only(
            'email'
        );

        //check validations on form inputs
        $validator = Validator::make($input, $rules, $request_rule->messages());

        //check if validation fails
        if ($validator->fails()) {
            Toastr::Error("Input validation failed", $title = null, $options = []);
            return Redirect::back()->withInput()->withErrors($validator);
        }

        //get form input email
        $email = Input::get('email');

        //check if email already exists
        $user_response = $this->isEmailExist($email);

        //check if user response exists
        if ($user_response) {

            //creates PasswordReset object and find all entries
            $password_reset_users = $this->passwordReset->findAll();

            //creates forgot password token
            $forgot_token = str_random(16);

            //check if password token already exists, regenerate if already exists
            if ($password_reset_users) {
                foreach ($password_reset_users as $password_reset_user) {
                    while ($password_reset_user->token === $forgot_token) {
                        $forgot_token = str_random(16);
                    }
                }
            }

            //creates data array for password_resets
            $password_reset_data = array(
                'email' => $email,
                'token' => $forgot_token,
            );

            //delete if any previous entry exists corresponding to the given email
            foreach ($password_reset_users as $password_reset_user) {
                if ($password_reset_user->email == $email) {
                    $this->passwordReset->delete($password_reset_user->email);
                }
            }

            //creates a new entry in password_resets
            $password_reset_response = $this->passwordReset->create($password_reset_data);

            //check if response exists
            if ($password_reset_response) {
                $forgotEmail = Input::get('email');
                //send mail
                Mail::send('auth::password.recover', array('forgot_token' => $forgot_token), function ($message) use ($forgotEmail) {
                    $message->to($forgotEmail, 'Admin')->subject('Reset your password');
                });

                //return view with email
                Toastr::Success("Please follow the link sent to your registered email for resetting your password", $title = null, $options = []);
                //return redirect('forgot');
                return Redirect::route('forgot');
            }
        } else {
            //return view with email
            Toastr::Error("Entered email does not match our records", $title = null, $options = []);
            //return redirect('forgot');
            return Redirect::route('forgot');
        }
    }

    public function getReset($token)
    {
        //create data array for password reset
        $password_reset_data = array(
            'token' => $token,
        );

        //create PasswordReset model and match forgot token
        $password_reset_response = $this->passwordReset->preMatchToken($password_reset_data);

        if ($password_reset_response) {
            $created = new Carbon($password_reset_response->created_at);
            $now = Carbon::now();

            $difference = $created->diff($now)->format('%H');

            if ($difference < 7) {
                $email = $password_reset_response->email;
                return view('auth::password.reset', compact('email', 'token'));
            }
            //return view message
            Toastr::Error("Forgot password link expired, Resend password reset link", $title = null, $options = []);
            //return redirect('forgot');
            return Redirect::route('forgot');
        }
        //return view message
        Toastr::Error("Token mismatch, Resend password reset link", $title = null, $options = []);
        //return redirect('forgot');
        return Redirect::route('404');
    }

    /**
     * function for post request for new password
     * @return type
     */
    public function postReset()
    {
        //get request rules
        $request_rule = new RegisterUpdateRequest();
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];

        //dd($rules);
        //get form input
        $input = Input::only(
            'email', 'password', 'password_confirmation'
        );

        //check validations on form inputs
        $validator = Validator::make($input, $rules, $request_rule->messages());
        if ($validator->fails()) {
            Toastr::Error("Input validation failed", $title = null, $options = []);
            return Redirect::back()->withInput()->withErrors($validator);
        }

        //get email and token from form input
        $email = Input::get('email');
        $token = Input::get('token');

        //create data array for password reset
        $password_reset_data = array(
            'email' => $email,
            'token' => $token,
        );

        //create PasswordReset model and match forgot token
        $password_reset_response = $this->passwordReset->postMatchToken($password_reset_data);

        if ($password_reset_response) {
            $created = new Carbon($password_reset_response->created_at);
            $now = Carbon::now();

            $difference = $created->diff($now)->format('%H');

            if ($difference < 7) {

                //create data array for password_reset table
                $updated_password = array(
                    'password' => Hash::make(Input::get('password')),
                );

                //creates User model and update password in users table by email
                $user_response = $this->user->updateByEmail($updated_password, $email);

                //check if response exists
                if ($user_response) {

                    $this->passwordReset->delete($email);
                    //logs the user in and redirect to dashboard
                    $redirect_url = $this->login->logUserIn($user_response);

                    //return view
                    if (Session::has('campaign')) {
                        return Redirect::route($redirect_url, Session::get('campaign'));
                    }
                    return Redirect::route($redirect_url);
                }
                return Redirect::route('404');
            }
            //return view message
            Toastr::Error("Forgot password link expired, Resend password reset link", $title = null, $options = []);
            //return redirect('forgot');
            return Redirect::route('forgot');
        }
        //return view message
        Toastr::Error("Token mismatch, Resend password reset link", $title = null, $options = []);
        //return redirect('forgot');
        return Redirect::route('404');
    }

    protected function setUserActiveStatus($user_id, $active_status)
    {
        $confirmation_code = 'Default';

        $user_activation_data = array(
            'user_id' => $user_id,
            'token_code' => $confirmation_code,
            'is_activated' => $active_status,
        );

        $this->userActivation->create($user_activation_data);
    }

    public function unsubscribe($slug)
    {
        $user_response = $this->userProfile->findBySlug($slug);
        if ($user_response) {
            $id = $user_response->user_id;
            $user_profile_update_data = array(
                'recieve_updates' => 0,
            );
            $update_response = $this->userProfile->update($user_profile_update_data, $id);
            if ($update_response) {
                //return view with email
                Toastr::Success("You have successfully unsubscribed from email notification", $title = null, $options = []);
                //return redirect('forgot');
                return Redirect::route('site.index');
            }
        }
    }

    public function updateUser()
    {
        $user = new UserProfileRepo();
        $data = Input::all();
        $id = Auth::getUser();
        return json_encode($id);
        die;
        //$user->update($data, $id)
    }

//    public function addOfflineAddress()
//    {
//        echo json_encode(array("msg" => "Here"));
//        //get request rules
//        $request_rule = new RegisterUserCreateRequest();
//        $rules = $request_rule->rules();
//
//        //get form input
//        $input = Input::all();
//
//        //check validations on form inputs
//        $validator = Validator::make($input, $rules, $request_rule->messages());
//        // If Image Is Valid Than Upload Image
////        if (empty($validator->messages()->getMessages()['charity_logo'])) {
////            $charity_logo = "";
////            if (\Input::hasFile('charity_logo') && !isset($validator->errors()->charity_logo)) {
////                if (!empty(Session::get('charity_logo'))) {
////                    $previousImage = Session::get('charity_logo');
////                    Helper::unlinkImages($previousImage, 'charity');
////                }
////                // upload image
////                $image = $this->uploader->upload('charity_logo');
////                $image->save($this->charityLogoStoragePath);
////                $charity_logo = $image->getFilename();
////                $image_detail = array(
////                    'inputFile' => Input::file('charity_logo'),
////                    'image' => $image,
////                    'thumb_size' => Config::get('config.thumb_charity_size'),
////                    'thumb_path' => Config::get('config.thumb_charity_logo_upload_path'),
////                    'large_size' => Config::get('config.large_charity_size'),
////                    'large_path' => Config::get('config.large_charity_logo_upload_path'),
////                );
////                Helper::createThumb($image_detail);
////                // Forget Previous session
////                Session::forget('charity_logo');
////                // Set Image Name In Session
////                Session::set('charity_logo', $charity_logo);
////            }
////        }
//
//        if ($validator->fails()) {
//            Toastr::Error("Input validation failed", $title = null, $options = []);
//            return Redirect::back()->withInput()->withErrors($validator);
//        }
//
//        $user_name = Input::get('first_name') . ' ' . Input::get('sur_name');
//        $email = Input::get('email');
//        $password = Input::get('password');
//
//        //creates user data array for users table
//        $user_data = array(
//            'name' => $user_name,
//            'email' => $email,
//            'password' => Hash::make($password),
//        );
//
//        //creates User model and user entry in the users table
//        $user_response = $this->user->create($user_data);
//
//        //get user id
//        $user_id = $user_response->id;
//
//        $role_id = $sign_up_as_indiviual ? Config::get('config.role_user') : Config::get('config.role_charity');
//
//        //create role user data array for role_user table
//        $role_user_data = array(
//            'role_id' => $role_id,
//            'user_id' => $user_id,
//        );
//
//        //creates UserProfile model and user profile entry in the user_profile table
//        $this->roleUser->create($role_user_data);
//
//        $name = (!$sign_up_as_indiviual) ? Input::get('charity_name') : $user_name;
//        $unique_slug = $this->userProfile->getUniqueSlug($name, 0);
//
//        if (!$sign_up_as_indiviual) {
//
//            $user_profile_data = array(
//                'user_id' => $user_id,
//                'charity_logo' => $charity_logo,
//                'registration_no' => Input::get('registration_no'),
//                'charity_name' => Input::get('charity_name'),
//                'address' => Input::get('address'),
//                'postcode' => Input::get('postcode'),
//                'contact_telephone' => Input::get('contact_telephone'),
//                'web' => Input::get('web'),
//                'description' => Input::get('description'),
//                'slug' => $unique_slug,
//                'country' => Input::get('country'),
//                'locality' => Input::get('locality'),
//                'lat' => round(Input::get('lat'), 4),
//                'lng' => round(Input::get('lng'), 4),
//            );
//
//            $user_profile_response = $this->userProfile->create($user_profile_data);
//
//            $this->setUserActiveStatus($user_id, 0);
//
//            //logs the user in and redirect to dashboard
//            $redirect_url = $this->login->logUserIn($user_response);
//            //call to account verification mail function
//            $this->sendAccountVerification($user_id, $email);
//            //return view with message
//            Toastr::success("Thanks for signing up with GiveBrite!", $title = null, $options = []);
//        }
//
//    }
}
