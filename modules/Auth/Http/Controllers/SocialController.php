<?php

namespace Modules\Auth\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Socialite as Socialize;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Auth\Repositories\Users\EloquentUserActivationRepository as UserActivationRepo;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Modules\Auth\Http\Requests\SocialCreateRequest;
use Illuminate\Support\Facades\Validator;
use Config;
use Session;
use Toastr;
use Modules\Auth\Http\Controllers\LoginController;
use Exception;

class SocialController extends Controller
{
    protected $user, $userProfile, $userActivation, $roleUser, $login;

    function __construct(UserRepo $user, UserProfileRepo $userProfile, UserActivationRepo $userActivation, RoleUserRepo $roleUser, LoginController $login)
    {
        $this->user = $user;
        $this->userProfile = $userProfile;
        $this->userActivation = $userActivation;
        $this->roleUser = $roleUser;
        $this->login = $login;
    }

    /**
     * Redirect the user to the Social authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($operation, $media)
    {

        //check if request coming from register page or login page
        Session::put('social_operation', $operation);
        //just to handle providers that doesn't exist
        if (!config("services.$media")) {
            abort('404');
        }

        //rewrite the config.service.php for redirect url
        Config::set("services.$media.redirect", route('auth.getSocialAuthCallback', $media));

        if ($media == "facebook") {
            return Socialize::driver($media)->scopes(['email', 'user_friends', 'public_profile', 'basic_info'])->redirect();
        }
        return Socialize::driver($media)->redirect();
    }

    /**
     * Obtain the user information from Social.
     *
     * @return Response
     */
    public function handleProviderCallback($media)
    {

        //rewrite the config.service.php for redirect url
        Config::set("services.$media.redirect", route('auth.getSocialAuthCallback', $media));

        try {
            $user = Socialize::driver($media)->user();
        } catch (Exception $e) {
            //return view with message
            Toastr::Error("Sorry, unable to continue with $media, Please try again", $title = null, $options = []);
            return Redirect('/');
        }

        //creates user if not exists, finds if exists (on the basis of social id)
        $authUser = $this->findUser($user, $media);
        // Set Social User Session For UserAccessToken
        Session::set('social_user', $user);
        //checks if exists
        if ($authUser) {
            //checks the operation (login/register)
            if (Session::get('social_operation') == 'login') {

                //logs the user in and redirect to dashboard
                $redirect_url = $this->login->logUserIn($authUser);

                //get logged in user required info
                //return view
                if (Session::has('campaign')) {
                    return Redirect::route($redirect_url, Session::get('campaign'));
                }
                return Redirect::route($redirect_url);
            } else if (Session::get('social_operation') == 'register') {
                //return view with message
                Toastr::Error("Already registered please login to continue", $title = null, $options = []);
                return redirect('login');
            }
        } else {

            //checks the operation (login/register)
            if (Session::get('social_operation') == 'login') {

                //creates a user in the DB and returns response if email exists, returns null if email dont exists in the social user data array
                $login_response = $this->loginUser($user, $media);

                if ($login_response != null) {

                    //logs the user in and redirect to dashboard
                    $redirect_url = $this->login->logUserIn($login_response);
                    //return view
                    if (Session::has('campaign')) {
                        return Redirect::route($redirect_url, Session::get('campaign'));
                    }
                    return Redirect::route($redirect_url);
                }

                Toastr::Error("Email is mandatory, $media is not providing email address", $title = null, $options = []);
            }
            //creates array of rest of the user information for form autofill and returns
            $create_response = $this->createUser($user);

            //sets email get by social media in session
            Session::put('social_email', $create_response['email']);

            //returns to register page with form autofill information
            return Redirect::to('register')
                ->with('first_name', $create_response['first_name'])
                ->with('sur_name', $create_response['sur_name'])
                ->with('email', $create_response['email'])
                ->with('sign_up_tag', $media)
                ->with('sign_up_id', $create_response['sign_up_id'])
                ->with('social_profile_image', $create_response['social_profile_image']);
        }
    }

    /**
     * function for checking if user already exists (on the basis of social id)
     * @param type $user_social_data
     * @param type $media
     * @return type
     */
    private function findUser($user_social_data, $media)
    {
        //creates User model and find user by social media id
        $authUser = $this->user->findBySocialId($media, $user_social_data->id);

        //check if user exists
        if ($authUser) {
            return $authUser;
        }
    }

    /**
     * function for creating user in the table (if email already exists updates the user with social tag and social id)
     * @param type $user_social_data
     * @param type $media
     * @return type
     */
    public function loginUser($user_social_data, $media)
    {
        //spliting the user name and getting first name and last name
        $user_name_arr = explode(" ", $user_social_data->name);
        $user_name_arr_length = count($user_name_arr);

        $first_name = $user_name_arr[0];
        $sur_name = ($user_name_arr_length > 1) ? $user_name_arr[$user_name_arr_length - 1] : "";

        //check if email is there in the object
        if ($user_social_data->email) {

            //creates user object
            $email_with_tag = $this->user->findByEmail($user_social_data->email);

            if ($email_with_tag) {
                if ($email_with_tag->sign_up_tag && $email_with_tag->sign_up_id) {
                    return $email_with_tag;
                }

                //updates the social_tag and social_id in the users table
                $user_updated_data = array(
                    'sign_up_tag' => $media,
                    'sign_up_id' => $user_social_data->id
                );

                $user_response = $this->user->updateByEmail($user_updated_data, $user_social_data->email);

                //create user activation data array for user_activation table
                $user_activation_updated_data = array('is_activated' => 1);

                //creates UserActivation model and update is_activated field in the user_activation table
                $user_activation_response = $this->userActivation->findByUserId($user_response->id);
                if ($user_activation_response) {
                    $this->userActivation->updateById($user_activation_updated_data, $user_activation_response->id);
                }

                return $user_response;
            }

            //else creates a user in the DB
            $user_data = array(
                'name' => $user_social_data->name,
                'email' => $user_social_data->email,
                'password' => Hash::make('Default'),
                'sign_up_tag' => $media,
                'sign_up_id' => $user_social_data->id
            );


            $user_response = $this->user->create($user_data);
            $user_id = $user_response->id;

            $name = $user_social_data->name;
            $unique_slug = $this->userProfile->getUniqueSlug($name, 0);

            $user_profile_data = array(
                'user_id' => $user_id,
                'first_name' => $first_name,
                'sur_name' => $sur_name,
                'slug' => $unique_slug
            );

            //get user profile image in DB
            if ($user_social_data->avatar) {
                $user_profile_data['profile_pic'] = $user_social_data->avatar;
            }

            $this->userProfile->create($user_profile_data);

            $confirmation_code = "Default";

            $user_activation_data = array(
                'user_id' => $user_id,
                'token_code' => $confirmation_code,
                'is_activated' => 1
            );

            $this->userActivation->create($user_activation_data);

            $role_user_data = array(
                'role_id' => Config::get('config.role_user'),
                'user_id' => $user_id
            );

            $this->roleUser->create($role_user_data);

            return $user_response;
        } else {
            return null;
        }
    }

    /**
     * function for creating an array of user information
     * @param type $user_social_data
     * @return type
     */
    public function createUser($user_social_data)
    {

        //spliting the user name and getting first name and last name
        $user_name_arr = explode(" ", $user_social_data->name);
        $user_name_arr_length = count($user_name_arr);

        //get user info
        $first_name = $user_name_arr[0];
        $sur_name = ($user_name_arr_length > 1) ? $user_name_arr[$user_name_arr_length - 1] : "";
        $email = ($user_social_data->email) ? $user_social_data->email : "";
        $sign_up_id = $user_social_data->id;
        $social_profile_image = $user_social_data->avatar;

        //create data array
        $user_data = array(
            'first_name' => $first_name,
            'sur_name' => $sur_name,
            'email' => $email,
            'sign_up_id' => $sign_up_id,
            'social_profile_image' => $social_profile_image
        );

        //return data array
        return $user_data;
    }

}
