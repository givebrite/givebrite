<?php

namespace Modules\Auth\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Lang;
use Modules\Auth\Http\Requests\LoginCreateRequest;
use Modules\Auth\Repositories\Roles\EloquentRoleRepository as RoleRepo;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Modules\Auth\Repositories\Users\EloquentUserActivationRepository as UserActivationRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Pingpong\Modules\Routing\Controller;
use Redirect;
use Toastr;

class LoginController extends Controller {

	/**
	 * get Login route action
	 * @return type
	 */
	public function getLogin() {
		$redirectUrl = Input::get('redirect_url');
		if(Session::has('loginuri')){
			$redirectUrl = Session::get('loginuri');
		}
		return view('auth::login', compact(['redirectUrl', 'slug']));
	}

	public function getLoginDefault() {
		$slug = '';
		$redirectUrl = Input::get('redirect_url');
		return view('auth::login', compact(['redirectUrl', 'slug']));
	}

	/**
	 * Login function
	 * @return type
	 */
	public function postLogin() {
		// Getting all post data
		$data = Input::all();
	
		// Applying validation rules.
		//get request rules
		$request_rule = new LoginCreateRequest();
		$rules = $request_rule->rules();
		$redirectUrl = $data['redirect'];
		$validator = Validator::make($data, $rules, $request_rule->messages());
		if ($validator->fails()) {
			// If validation falis redirect back to login.
			Toastr::Error("Input validation failed", $title = null, $options = []);
			return Redirect::to('/login')->withInput(Input::except('password'))->withErrors($validator);
		} else {
			$userdata = array(
				'email' => Input::get('email'),
				'password' => Input::get('password'),
			);
			// doing login.
			if (Auth::validate($userdata)) {
				if (Auth::attempt($userdata)) {
					//get logged in user required info
					$this->setLoggedInUserInfo(Auth::user()->id);
					// Auto Logout User if Account is Blocked
					$user = Session::get('user_session_data');
					if (isset($user['status']) && $user['status'] == '0') {
						Auth::logout();
						// Remove Values From Session
						Session()->flush();
						Toastr::Error("Your account has been blocked by admin", $title = null, $options = []);
						return redirect('login');
					}
					//set user session
					if (Session::has('campaign')) {
						return Redirect::intended('donate/' . Session::get('campaign'));
					}
					if (Session::has('create_campaign')) {
						Session::forget('create_campaign');
						return Redirect::route('campaigns.create');
					} elseif (!empty($redirectUrl)) {
						$token = !empty($data['token']) ? $data['token'] : "";
						return Redirect::Route($redirectUrl, $token);
					} else {
						return Redirect::intended('/dashboard');
					}
				}
			} else {
				// if any error send back with message.
				Toastr::Error($this->messages()['credentials-error'], $title = null, $options = []);
				return redirect('login');
			}
		}
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function logout() {
		Auth::logout();
		// Remove Values From Session
		Session()->flush();
		return redirect('/login');
	}

	/**
	 * Check if registered user has verified its account
	 * @param type $user_id
	 * @return boolean
	 */
	public function isActive($user_id) {
		$user_activation_model = new UserActivationRepo();
		$user_active_response = $user_activation_model->findByUserId($user_id);
		if ($user_active_response) {
			if ($user_active_response->is_activated == 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * set all user info of a logged in user into an array
	 * @param type $user_id
	 * @return type array
	 */
	public function setLoggedInUserInfo($user_id) {
		//creates User model and find user from the users table by id
		$user_model = new UserRepo();
		$user_response = $user_model->findById($user_id);

		//creates user_session data array
		$user_session = array(
			'id' => $user_id,
			'email' => $user_response->email,
			'user_name' => $user_response->name,
			'active' => $this->isActive($user_id),
			'status' => $user_response->status,
		);

		//creates User Profile model and find user's profile data from the user_profile table by id
		$user_profile_model = new UserProfileRepo();
		$user_profile_response = $user_profile_model->findByUserId($user_id);

		$user_session['charity_name'] = (!empty($user_profile_response->charity_name)) ? $user_profile_response->charity_name : "";
		$user_session['first_name'] = (!empty($user_profile_response->first_name)) ? $user_profile_response->first_name : "";
		$user_session['sur_name'] = (!empty($user_profile_response->sur_name)) ? $user_profile_response->sur_name : "";
		$user_session['recieve_updates'] = (!empty($user_profile_response->recieve_updates)) ? $user_profile_response->recieve_updates : "";
		$user_session['slug'] = (!empty($user_profile_response->slug)) ? $user_profile_response->slug : "";

		$role_user_model = new RoleUserRepo();
		$role_user_response = $role_user_model->findByUserId($user_id);

		$role_model = new RoleRepo();
		$role_response = $role_model->findById($role_user_response->role_id);
		$user_session['role'] = $role_response->name;
		if ($user_session['role'] == 'Charity') {
			$user_session['charity_logo'] = $user_profile_response->charity_logo;
		} else {
			$user_session['profile_pic'] = $user_profile_response->profile_pic;
		}
		Session::set('user_session_data', $user_session);
	}

	public function logUserIn($user_response) {
        if (Session::has('loginuri')) {
            $redirect_url = Session::get('loginuri');
        }else{
            $redirect_url = "dashboard.index";
        }

		if (!Auth::check()) {
			Auth::login($user_response, true);
			$this->setLoggedInUserInfo(Auth::user()->id);
		}
		if (Session::has('create_campaign')) {
			Session::forget('create_campaign');
			$redirect_url = "campaigns.create";
		}
		if (Session::has('campaign')) {
			$redirect_url = "campaign.donation";
		}

		return $redirect_url;
	}

	public function messages() {

		// Basic Contact Validation
		$messages = [
			'credentials-error' => Lang::get('auth::messages.login-credentials.error'),
			'activation-error' => Lang::get('auth::messages.login-activation.error'),
		];

		return $messages;
	}

}
