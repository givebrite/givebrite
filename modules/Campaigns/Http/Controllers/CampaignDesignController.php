<?php

namespace Modules\Campaigns\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Modules\Campaigns\Entities\CampaignLocation as CampaignLocationRepo;
use Modules\Campaigns\Entities\CampaignDesign as CampaignDesignModel;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Campaigns\Repositories\EloquentCampaignDesignRepository as CampaignDesignRepo;
use Modules\Campaigns\Requests\CampaignDesignCreateRequest as CampaignDesignCreateRequest;
use Modules\Campaigns\Requests\CampaignDesignUpdateRequest as CampaignDesignUpdateRequest;
use View;
use Input;
use Redirect;
use Toastr;
use Geocoder;
use Config;
use Helper;
use Session;

class CampaignDesignController extends Controller
{

    /**
     * @var ImageUploader
     */
    protected $uploader;
    protected $imageStoragePath;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader)
    {
        ini_set('max_execution_time', 300);
        $this->uploader = $uploader;
        $this->imageStoragePath = Config::get('config.upload_path');
    }

    /**
     * Function to set variable for views
     *
     * @author AK 03/10/16
     */
    public function setUp()
    {
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $campaignLocations = $campaignRepo->getCampaignLocations();
        $campaignThemes = $campaignRepo->getCampaignThemes();
        $cityByLocation = [];
        //Share variable to view
        View::share('campaignLocations', $campaignLocations);
        View::share('campaignThemes', $campaignThemes);
        View::share('cityByLocation', $cityByLocation);
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index()
    {
        $campaignRepo = new CampaignRepo();
        return view('campaigns::index');
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create($campaignId)
    {

        $this->setUp();
        // Creating Campaign Design Repository Instance
        $campaignDesignRepo = new CampaignDesignRepo();
        // Check If Design Exist For Campaign-Id
        $campaignDesign = $campaignDesignRepo->findByCampaignId($campaignId);
        // Getting Campaign Repo
        $campaignRepo = new CampaignRepo();
        $campaignStage = $campaignRepo->findById($campaignId)->stage;
        
        if ($campaignStage < 2) {
            return Redirect::back();
        }
        return view('campaigns::design', compact('campaignId', 'campaignDesign', 'campaignStage', 'cityByLocation'));
    }

    /**
     * Store a newly created campaign design in storage.
     *
     * @return Response
     */
    public function store(CampaignDesignCreateRequest $request, $campaignId)
    {   
        
        // Getting Request Data
        $campaignDesignData = $request->only('theme_id', 'postcode', 'campaign_image', 'campaign_description', 'location', 'city', 'media_type', 'facebook_url', 'media_url', 'lat', 'lng');
        $campaignData = $request->only('locality', 'country');
        $campaignDesignData['campaign_id'] = $campaignId;
        
        // Upload Files
        unset($campaignDesignData['campaign_image']);

        if (Session::has('image')) {
            $campaignImage = Session::get('image');
            $campaignDesignData['campaign_image'] = $campaignImage;
        }
        $campaignDesignRepo = new CampaignDesignRepo();
        $campaignLocation = new CampaignLocationRepo();
        // Save Locality and City if not exist Otherwise get id
        $campaignLocations = $this->setupCountryAndLocality($campaignData['country'], $campaignData['locality']);
        $campaignDesignData['city'] = !empty($campaignLocations['locality_id']) ? $campaignLocations['locality_id'] : 0;
        // Use As Country
        $campaignDesignData['location'] = !empty($campaignLocations['country_id']) ? $campaignLocations['country_id'] : 0;

        $campaignDesignData['lattitude'] = $campaignDesignData['lat'];
        $campaignDesignData['longitude'] = $campaignDesignData['lng'];
        //  dd($campaignDesignData);
        $saveLocation = $campaignDesignRepo->create($campaignDesignData);

        Session::forget('image');

        // Update Campaign Stage To Stage 3
        $campaignRepo = new CampaignRepo();
        
        $updateStage = $campaignRepo->updateCampaignStage($campaignId, 4); //Directly Redirect To Stage 4 For Now
        Toastr::success("Campaign design added successfully.", $title = null, $options = []);
        // Redirect To Edit Campaign
        return Redirect::route('launch.create', $campaignId);
    }

    /**
     * Show the form for editing the specified campaign design.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified campaign design in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignDesignUpdateRequest $request, $campaignId, $designId)
    {
        // Creating Campaign Design Repository Instance
        $campaignDesignRepo = new CampaignDesignRepo();

        // Creating Campaign Location Repository Instance
        $campaignLocation = new CampaignLocationRepo();

        try {
            $campaignDesign = $campaignDesignRepo->findById($designId);
            // Getting Request Data
            $campaignDesignData = $request->only('theme_id', 'postcode', 'campaign_description', 'location', 'city', 'media_type', 'facebook_url', 'media_url', 'lat', 'lng', 'campaign_image1');


            if ($campaignDesignData['campaign_image1'] != '') {
                $campaignDesignData['campaign_image'] = $campaignDesignData['campaign_image1'];
            } else {
                if (Session::has('image')) {
                    $campaignImage = Session::get('image');
                    $campaignDesignData['campaign_image'] = $campaignImage;
                }
            }

            $campaignData = $request->only('locality', 'country');

            // Save Locality and City if not exist Otherwise get id
            $campaignLocations = $this->setupCountryAndLocality($campaignData['country'], $campaignData['locality']);

            // Use Locality As city
            $campaignDesignData['city'] = !empty($campaignLocations['locality_id']) ? $campaignLocations['locality_id'] : 0;
            // Use Country As Location
            $campaignDesignData['location'] = !empty($campaignLocations['country_id']) ? $campaignLocations['country_id'] : 0;
            $campaignDesignData['lattitude'] = $campaignDesignData['lat'];
            $campaignDesignData['longitude'] = $campaignDesignData['lng'];
            $saveLocation = $campaignDesign->update($campaignDesignData);

            // Forget Session Of Campaign Image
            Session::forget('image');

            Toastr::success("Campaign design updated successfully.", $title = null, $options = []);
            //return Redirect::route('reward.create', $campaignDesign->campaign_id);

            $campaignRepo = new CampaignRepo();
            $campaignModel = $campaignRepo->findById($campaignId);

            if ($campaignModel->stage >= 5) {
                return redirect('campaign/' . $campaignModel->slug);
            }

            return Redirect::route('launch.create', $campaignDesign->campaign_id);
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show()
    {
        return view('campaigns::index');
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy()
    {
        return view('campaigns::index');
    }

    /**
     * Function To Get City For Location ID
     */
    public function getCityByLocation()
    {
        // Update Campaign Stage To Stage 3
        $campaignRepo = new CampaignRepo();
        $locationId = Input::get('locationId');
        if (!empty($locationId)) {
            $cityByLocation = $campaignRepo->getCampaignCityByLocation($locationId);
            $cityLocationView = view('campaigns::partials.citylocation', compact('cityByLocation'))->render();
            echo json_encode(['responseCode' => '200', 'responseView' => $cityLocationView]);
            die;
        } else {
            dd();
        }
        dd($cityByLocation);
    }

    public function getGeoLocation($address)
    {
        try {
            return $geocode = Geocoder::geocode($address);
            // The GoogleMapsProvider will return a result
        } catch (\Exception $e) {
            // No exception will be thrown here
            echo $e->getMessage();
        }
    }

    public function setupCountryAndLocality($country, $locality)
    {
        // Save Country IF Not Exist Otherwise get country Id
        $saveCountry = CampaignLocationRepo::firstOrCreate(['name' => $country]);
        if (!empty($saveCountry->id)) {
            $saveLocation = CampaignLocationRepo::firstOrCreate(['name' => $locality, 'parent_id' => $saveCountry->id]);
        }

        return ['country_id' => $saveCountry->id, 'locality_id' => $saveLocation->id];
    }

}
