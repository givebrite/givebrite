<?php

namespace Modules\Campaigns\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Campaigns\Entities\Team as TeamModel;

use Modules\Campaigns\Repositories\EloquentTeamRepository as TeamRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;

use Illuminate\Support\Facades\Mail;

use View;
use Input;
use Redirect;
use Toastr;
use Lang;
use Config;
use Illuminate\Support\Str as Str;
use Session;
use Helper;
use Image;

class TeamDetailController extends Controller
{

    
    /**
     * @var ImageUploader
     */
    protected $uploader;
    protected $imageStoragePath;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader)
    {
        ini_set('max_execution_time', 300);
        $this->uploader = $uploader;
        $this->imageStoragePath = Config::get('config.upload_path');
    }


    public function setUp()
    {
        //Get Login User Details
        $user = Session::get('user_session_data');
       
    }

    public function createTeam(){

       
        return view('campaigns::team.create');

    }

    public function acceptInvitation($team_id,$email){

       $TeamRepo = new TeamRepo();
       $status = $TeamRepo->acceptInvitation($team_id,$email);
       
       $msg = 'Thanks for accepting the request !';

       return view('campaigns::empty-template',['msg'=>$msg]);

    }

    public function confirmInvitation($team_id,$email){

       $TeamRepo = new TeamRepo();
       $status = $TeamRepo->acceptInvitation($team_id,$email);
       
       $msg = 'Thanks for confirming the request !';
       
       //send mail to user

       Mail::send('campaigns::email.team-request-confirmation', ['email'=>$user['email'],'team_id'=>$team_id], function ($m) use ($user) {
          $m->from('info@givebrite.com', 'GiveBrite');

          $m->to($user['email'], 'Confirmation')->subject('New Query/Join Team');
       });

       return view('campaigns::empty-template',['msg'=>$msg]);

    }


    public function inviteUserToTeam($email,$team_id){

        //save email to team members
        $TeamRepo = new TeamRepo();
        $id = $TeamRepo->inviteUserToTeam($email,$team_id,'sent');
        //Send mail

        Mail::send('campaigns::email.team-invite',['email'=>$email,'team_id'=>$team_id], function ($m) use ($email) {
            
            $m->from('info@givebrite.com', 'GiveBrite');

            $m->to($email, 'Team Invite')->subject('New Invite/Join Team');
        });

        return $id;

    }

    public function joinTeam($team_id){

      //The user is trying to join team
      $user = Session::get('user_session_data');
      
      //dd($user);

      $TeamRepo = new TeamRepo();
      
      $inviteSent = $TeamRepo->inviteUserToTeam($user['email'],$team_id,'recieved');
      $email = $user['email'];

      Mail::send('campaigns::email.team-invite-request', ['email'=>$user['email'],'team_id'=>$team_id], function ($m) use ($user) {
          $m->from('info@givebrite.com', 'GiveBrite');

          $m->to($user['email'], 'Test')->subject('New Query/Join Team');
      });

      Toastr::success("Your request has been sent.", $title = null, $options = []);    
      return redirect()->back();

    }

    public function joinTeamPost(){

      if (Input::has('selectedTeam'))
      {
          $user = Session::get('user_session_data');
          $TeamRepo = new TeamRepo();
          $inviteSent = $TeamRepo->inviteUserToTeam($user['email'],Input::get('selectedTeam'),'recieved');
          
          Toastr::success("Your request has been sent.", $title = null, $options = []);    
          return redirect()->back();
      }
      
      Toastr::error("Your did not select a team. please select a team", $title = null, $options = []);    
      return redirect()->back();

    }

    /* 
    * Displays the team pages
    * 
    */
    public function teamPages(){

        $user = Session::get('user_session_data');
        $TeamRepo = new TeamRepo();
        $teamPages = $TeamRepo->getPageDetailsWithTeam($user['id']);
        
        $members = array();

        foreach($teamPages as $pages){

            $members[] = $TeamRepo->findMembersBasic($pages->team_id);
            

        }

        return view('campaigns::team.index',['teamPages'=>$teamPages,'members'=>$members]);
    }

    //Check the url for availability
    public function checkUrlAvailability(){

      $TeamRepo = new TeamRepo();
      $teamPages = $TeamRepo->findByTeamUrl(Input::get('name'));

      if($teamPages){

          //Url is not available
          $data = array('msg' => 'unavailable');
          echo json_encode($data);

      } else {

          //Url is available
          $data = array('msg' => 'available');
          echo json_encode($data);


      }

    }


    public function storeTeam(){
       
       //dd(Input::all());
       $user = Session::get('user_session_data');

       $data = Input::all();
      
       $data['cover_photo'] = Input::get('teamCoverImageUrl'); 
       
       $data['team_logo'] = Input::get('teamLogoImageUrl'); 
       
       $data['requires_invitation'] = 0;

       if (Input::has('pay_type')){
          $data['requires_invitation'] = 1; //Invitation required
       } 

       $data['team_description'] = Input::get('campaign_description');
       $data['created_by_user']  = $user['id'];
       $data['team_url']  = str_replace(" ","_",Input::get('team_url'));

       $TeamRepo = new TeamRepo();
       /**
        * undocumented constant
       **/
     
       $team = $TeamRepo->create($data);
       
       if($team){

        Toastr::success("Team Created Successfully.", $title = null, $options = []);
        
          //Get the user campaigns
         $campaignRepo = new CampaignRepo();

         $campaign = $campaignRepo->getallUserActiveCampaigns($user['id']);

         return view('campaigns::team.manage',['team_id'=>$team->team_id,'team_url'=>$data['team_url'],'campainDetails'=>$campaign]);

       }
       
       Toastr::error("Some error occured. Please try again", $title = null, $options = []);
       
       $redirect->back();

    }

    public function manageTeam(){
        
        $user = Session::get('user_session_data');
        $data = Input::all(); unset($data['_token']); unset($data['team_email']);  unset($data['team']);  
        unset($data['team_url']);
        $data['created_by_user'] = $user['id'];
        $team_url = Input::get('team_url');

        $TeamRepo = new TeamRepo();
        
        $teamCampaigns = $TeamRepo->saveTeamCampaign($data);

        if($teamCampaigns){

            Toastr::success("Details Successfully updated.", $title = null, $options = []);    
            return redirect('team-details/'.$team_url.'');

        } else {

            Toastr::error("Team could not be added to the campaign.", $title = null, $options = []);    
            return redirect()->back();
        }

    }

    public function deleteTeam($id){

      $TeamRepo = new TeamRepo();
      $teamPage = $TeamRepo->findById($id);
      $user = Session::get('user_session_data');

      if($teamPage){

          if($teamPage->created_by_user == $user['id']){

              $TeamRepo->delete($id);

              Toastr::success("Team has been deleted.", $title = null, $options = []);    
              return redirect('dashboard/team-pages');
          
          } else {

            Toastr::error("Team could not be deleted.", $title = null, $options = []);    
            return redirect('dashboard/team-pages');

          }

      } else {

          Toastr::error("Team page not found.", $title = null, $options = []);    
          return redirect('dashboard/team-pages');
      }

      
    }

    public function disableTeam($id,$status){

      $TeamRepo = new TeamRepo();
      $teamPage = $TeamRepo->findById($id);
      $user = Session::get('user_session_data');
      
      if($status == 1){ $status = 0; } else { $status = 1;}

      if($teamPage){

          if($teamPage->created_by_user == $user['id']){
                            
              $teamPage = $TeamRepo->findById($id);
              $teamPage->status = $status;
              $teamPage->save();

              Toastr::success("Team status has been updated.", $title = null, $options = []);    
              return redirect()->back();
          
          } else {

            Toastr::error("The action cannot be performed.", $title = null, $options = []);    
            return redirect()->back();

          }

      } else {

          Toastr::error("Team page not found.", $title = null, $options = []);    
          return redirect()->back();
      }

      
    }

    //Team updates new what the are up to 
    public function saveUpdate($team_id){

      
      $TeamRepo = new TeamRepo();
      $user = Session::get('user_session_data');
      
      $teamPage = $TeamRepo->saveUpdate($team_id,$user['id'],Input::get('updates'));
      
      if($teamPage){

          Toastr::success("The update has been saved.", $title = null, $options = []);    
          return redirect()->back();  
      }
      
      Toastr::error("Please try again , could not update.", $title = null, $options = []);    
      return redirect()->back();
      

    }

    public function editStepOneView($id){

      $TeamRepo    = new TeamRepo();
      $teamDetails =  $TeamRepo->findById($id);
      
      return view('campaigns::team.edit-step-one',['teamDetails'=>$teamDetails]);
      
    }

    public function updateStepOneView($id){

       $user = Session::get('user_session_data');

       //Get the user campaigns
       $campaignRepo = new CampaignRepo();
       $TeamRepo    = new TeamRepo();
       #getTeam details
       $teamDetails = $TeamRepo->findById($id);
       $teamDetails->team_name = Input::get('team_name');

       //$teamDetails->team_url = Input::get('team_url');
       $teamDetails->cover_photo = Input::get('teamCoverImageUrl'); 
       $teamDetails->team_logo = Input::get('teamLogoImageUrl'); 
       $teamDetails->team_description = Input::get('campaign_description');
       
       $teamDetails->requires_invitation = 0;

       if (Input::has('pay_type'))
       {
          $teamDetails->requires_invitation = 1; //Invitation required
       } 

       $teamDetails->save();
       
       #list of users in the team and new invites
       
       $teamMembers = $TeamRepo->findTeamMembers($id);       
       
       // /dd($teamMembers);

       $campaign = $campaignRepo->getallUserActiveCampaigns($user['id']);

       return view('campaigns::team.edit-step-two',['team_id'=>$id,'team_url'=>$teamDetails->team_url,'campainDetails'=>$campaign,'teamMembers'=>$teamMembers]);

    }


    public function approverequest($id){

        $TeamRepo    = new TeamRepo();
        $approved = $TeamRepo->approveRequest($id);
        
        if($approved){

            Toastr::success("The request is updated", $title = null, $options = []);    
            return redirect('team/'.$id.'/step-two-edit');

        } else {

            Toastr::error("The request is could not be updated. please try again", $title = null, $options = []);    
            return redirect('team/'.$id.'/step-two-edit');

        } 
        
    }
    
    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return view('campaigns::errors.404');
    }   

}
