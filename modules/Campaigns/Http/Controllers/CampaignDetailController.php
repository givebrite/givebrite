<?php

namespace Modules\Campaigns\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepository;
use Modules\Campaigns\Entities\CampaignCharity as CampaignCharityModel;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Campaigns\Repositories\EloquentCampaignCharityRepository as CampaignCharityRepo;
use Modules\Campaigns\Requests\CampaignDetailCreateRequest as CampaignDetailCreateRequest;
use Modules\Campaigns\Requests\CampaignDetailUpdateRequest as CampaignDetailUpdateRequest;
use View;
use Input;
use Redirect;
use Toastr;
use Lang;
use Config;
use Illuminate\Support\Str as Str;
use Session;
use Helper;

class CampaignDetailController extends Controller
{

    /**
     * @var ImageUploader
     */
    protected $uploader;
    protected $imageStoragePath;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader)
    {
        $this->uploader = $uploader;
        $this->imageStoragePath = Config::get('config.upload_path');
    }

    /**
     * Function to set variable for views
     *
     * @author AK 03/10/16
     */
    public function setUp()
    {
        //Get Login User Details
        $user = Session::get('user_session_data');
        // If User is Charity Get Charity Details
        if ($user['role'] == 'Charity' && Helper::isCharityVarified($user['id'])) {
            $userProfileRepo = new UserProfileRepository();
            $charityDetail = $userProfileRepo->findByUserId($user['id']);
        } else {
            $charityDetail = [];
        }
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $categories = $campaignRepo->getCampaignCategories();
        $charityList = $campaignRepo->getAllCharities();
        $charityLogo = $campaignRepo->getAllCharitiesLogo();
        View::share('categories', $categories);
        View::share('charityList', $charityList);
        View::share('charityLogo', $charityLogo);
        View::share('charityDetail', $charityDetail);
        View::share('user', $user);
    }

    
    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create()
    {   
        $this->setUp();
        // get Goal AmountPrice
        $goalAmount = Input::get('amount');
        // Getting the fundraise for (charity name)
        $term = Input::get('charity', false);
        if ($term) {
            $charity_id = $term;
            $charity_info = new UserProfileRepository();
            $charity_link_default = $charity_info->findByIdActivated($term)[0];
        } else {
            $charity_id = false;
            $charity_link_default = false;
        }
        $campaignStage = 1;

        return view('campaigns::create', compact('categories', 'charity_id', 'campaignStage', 'charityList', 'user', 'charityDetail', 'goalAmount', 'charity_link_default'));
    }

    
    public function searchCharities(){

        $charityList = $campaignRepo->getAllCharities();
        $charityLogo = $campaignRepo->getAllCharitiesLogo();

        dd($charityList); 

    }


    /**
     * Store a newly created campaign in storage.
     *
     * @return Response
     */
    public function store(CampaignDetailCreateRequest $request)
    {

        //Get User Detail From Session
        $user = Session::get('user_session_data');
        $userId = $user['id'];
        $campaignDetailData = $request->only('campaign_name', 'pay_type', 'goal_amount_type', 'goal_amount_time', 'goal_amount_monetary', 'campaign_category', 'campaign_duration', 'end_date', 'charity_id');

        $campaignDetailData['user_id'] = $userId;
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        //Auto Generate Unique Slug
        $campaignDetailData['slug'] = $campaignRepo->getUniqueSlug($campaignDetailData['campaign_name'], 0);
        // Campaign Duration Check
        if ($campaignDetailData['campaign_duration'] == 'ongoing') {
            unset($campaignDetailData['end_date']);
        }
        // Setting Current date to startdate
        $campaignDetailData['start_date'] = date("m/d/y H:i:s");
        $saveCampaign = $campaignRepo->create($campaignDetailData);
        // Update Campaign Stage To Stage 2
        $updateStage = $campaignRepo->updateCampaignStage($saveCampaign->id, 2);
        Toastr::success("Campaign Created Successfully.", $title = null, $options = []);

        // Forget Previous session
        Session::forget('image');
        if (Session::has('charity_id')) {
            Session::forget('charity_id');
        }
        return Redirect::route('design.create', $saveCampaign->id);
    }

    /**
     * Show the form for editing the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->setUp();
        // Creating Campaign Repository Instance

        $campaignRepo = new CampaignRepo();
        try {
            $campaign = $campaignRepo->findById($id);
            $campaignId = $campaign->id;
            $campaignStage = $campaign->stage;
            $charity_id = null;
            return view('campaigns::create', compact('campaignId', 'categories', 'campaign', 'campaignCharity', 'campaignStage', 'charityList', 'charityLogo', 'user', 'charityDetail','charity_id'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified campaign in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignDetailUpdateRequest $request, $id)
    {
        //Get User Detail From Session
        $user = Session::get('user_session_data');
        $userId = $user['id'];
        $campaignRepo = new CampaignRepo();
        try {
            $campaign = $campaignRepo->findById($id);
            $campaignDetailData = $request->only('campaign_name', 'goal_amount_type', 'goal_amount_time', 'goal_amount_monetary', 'campaign_type', 'campaign_category', 'pay_type', 'end_date', 'campaign_duration', 'charity_id');
            $campaignDetailData['user_id'] = $userId;
            //Auto Generate Unique Slug
            //$campaignDetailData['slug'] = $campaignRepo->getUniqueSlug($campaignDetailData['campaign_name'], $id);
            // Campaign Duration Check
            if ($campaignDetailData['campaign_duration'] == 'ongoing') {
                unset($campaignDetailData['end_date']);
            }
            // Setting Current date to startdate
            $campaignDetailData['start_date'] = date("m/d/y H:i:s");
            $campaign->update($campaignDetailData);

            Toastr::success("Campaign updated successfully.", $title = null, $options = []);

            return Redirect::route('design.create', $campaign->id);
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($slug)
    {
        $this->setup();
        $campaignDetail = [];
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findBySlug($slug);
        if (!empty($campaign)) {
            return view('campaigns::detail', compact('campaign'));
        } else {
            return $this->redirectNotFound();
        }
    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy()
    {
        return view('campaigns::index');
    }

}
