<?php

namespace Modules\Campaigns\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Pingpong\Admin\Uploader\ImageUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Modules\Campaigns\Entities\CampaignReward as CampaignRewardModel;
use Modules\Campaigns\Repositories\EloquentCampaignRewardRepository as CampaignRewardRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Campaigns\Requests\CampaignRewardCreateRequest as CampaignRewardCreateRequest;
use Modules\Campaigns\Requests\CampaignRewardUpdateRequest as CampaignRewardUpdateRequest;
use Modules\Dashboard\Repositories\EloquentDonationRepository as Donation;
use Modules\Auth\Repositories\Users\EloquentUserRepository as RegisterUser;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use View;
use Input;
use Redirect;
use Illuminate\Support\Facades\Request;
use Toastr;
use Helper;

class CampaignRewardController extends Controller
{

    /**
     * @var ImageUploader
     */
    protected $uploader;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * Function to set variable for views
     *
     * @author AK 03/10/16
     */
    public function setUp()
    {
        $shippingLocations = ['1' => 'Haryana', '2' => 'Punjab', '3' => 'Delhi', '4' => 'UP', '5' => 'MP'];
        //Share variable to view
        View::share('shippingLocations', $shippingLocations);
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index()
    {
        $campaignRepo = new CampaignRepo();
        return view('campaigns::index');
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create($campaignId)
    {
        $this->setUp();
        // Creating Instance For CampaignRewardRepo
        $campaignRewardRepo = new CampaignRewardRepo();
        // Getting Campaign Rewards
        $campaignRewards = $campaignRewardRepo->findByCampaignId($campaignId);
        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findById($campaignId);
        $campaignStage = $campaign->stage;
        $rewardRequired = $campaign->campaign_rewards;
        return view('campaigns::reward', compact('campaignId', 'campaignRewards', 'shippingLocations', 'rewardRequired', 'campaignStage'));
    }

    /**
     * Store a newly created campaign reward in storage.
     *
     * @return Response
     */
    public function store(CampaignRewardCreateRequest $request, $campaignId)
    {
        $this->setUp();
        // Getting Request Data
        $campaignRewardData = $request->only('reward_name', 'pledge_amount', 'pledge_description', 'shipping_required');
        // Add Campign Id to Input
        $campaignRewardData['campaign_id'] = $campaignId;
        // Creating Campaign Design Repository Instance
        $campaignRewardRepo = new CampaignRewardRepo();
        if (!empty($campaignRewardData['shipping_required']) && $campaignRewardData['shipping_required'] == 'yes') {
            $shippingLocationData = $request->only('shipping_cost', 'location');
            $location = explode("#", $shippingLocationData['location'][0]);
            $locationCost = array_combine($location, $shippingLocationData['shipping_cost']);
            $campaignRewardData['location_cost'] = json_encode($locationCost);
        }
        $saveCampaignRewards = $campaignRewardRepo->create($campaignRewardData);
        // Update Campaign Stage To Stage 4
        $campaignRepo = new CampaignRepo();
        $updateStage = $campaignRepo->updateCampaignStage($campaignId, 4);
        // Return Rewards Panel view
        $campaignRewards = $campaignRewardRepo->findByCampaignId($campaignId);
        Toastr::success("Campaign reward added successfully.", $title = null, $options = []);
        $rewardPanelView = view('campaigns::partials.rewardpanel', compact('campaignRewards', 'shippingLocations'))->render();
        echo json_encode(['responseCode' => '200', 'responseMessage' => 'Reward saved successfully.', 'responseView' => $rewardPanelView]);
        die;
    }

    /**
     * Show the form for editing the specified campaign design.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->setUp();
        // Creating Campaign Design Repository Instance
        $campaignRewardRepo = new CampaignRewardRepo();
        try {
            if (!empty($id)) {
                $campaignReward = $campaignRewardRepo->findById($id);
                // Return Rewards Form view
                $rewardFormView = view('campaigns::partials.rewardform', compact('campaignReward', 'shippingLocations'))->render();
            } else {
                $campaignId = Input::get('campaignId');
                $rewardFormView = view('campaigns::partials.rewardform', compact('campaignId', 'shippingLocations'))->render();
            }
            echo json_encode(['responseCode' => '200', 'responseView' => $rewardFormView]);
            die;
        } catch (ModelNotFoundException $e) {
            $this->redirectNotFound();
        }
    }

    /**
     * Update the specified campaign reward in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignRewardUpdateRequest $request, $id)
    {
        $this->setUp();
        // Creating Campaign Design Repository Instance
        $campaignRewardRepo = new CampaignRewardRepo();
        try {
            $campaignReward = $campaignRewardRepo->findById($id);
            // Getting Request Data
            $campaignRewardData = $request->only('reward_name', 'pledge_amount', 'pledge_description', 'shipping_required');
            if (!empty($campaignRewardData['shipping_required']) && $campaignRewardData['shipping_required'] == 'yes') {
                $shippingLocationData = $request->only('shipping_location', 'shipping_cost', 'location');
                $location = explode("#", $shippingLocationData['location'][0]);
                $locationCost = array_combine($location, $shippingLocationData['shipping_cost']);
                $campaignRewardData['location_cost'] = json_encode($locationCost);
            }
            $campaignReward->update($campaignRewardData);
            // Return Rewards Panel view
            $campaignRewards = $campaignRewardRepo->findByCampaignId($campaignReward->campaign_id);
            Toastr::success("Campaign reward updated successfully.", $title = null, $options = []);
            $rewardPanelView = view('campaigns::partials.rewardpanel', compact('campaignRewards', 'shippingLocations'))->render();
            echo json_encode(['responseCode' => '200', 'responseMessage' => 'Reward updated successfully.', 'responseView' => $rewardPanelView]);
            die;
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show()
    {
        return view('campaigns::index');
    }

    /**
     * Remove the specified campaign reward from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($rewardId)
    {
        // Creating Campaign Design Repository Instance
        $campaignRewardRepo = new CampaignRewardRepo();
        try {
            $campaignId = $campaignRewardRepo->delete($rewardId);
            $campaignRewards = $campaignRewardRepo->findByCampaignId($campaignId);
            // Return Rewards Panel view
            $rewardPanelView = view('campaigns::partials.rewardpanel', compact('campaignRewards'))->render();
            Toastr::success("Campaign reward deleted successfully.", $title = null, $options = []);
            echo json_encode(['responseCode' => '200', 'responseMessage' => 'Reward deleted successfully.', 'responseView' => $rewardPanelView]);
            die;
        } catch (ModelNotFoundException $e) {
            $this->redirectNotFound();
        }
    }

    /**
     * Store a reward shipping Location in storage.
     *
     * @return Response
     */
    public function saveRewardShippingLocation($locationData = [], $rewardId)
    {
        return true;
        $campaignRewardRepo = new CampaignRewardRepo();
        $saveLocations = $campaignRewardRepo->saveLocations($locationData, $rewardId);
        echo json_encode($locationData);
        die;
        return true;
    }

    /**
     * This Function is used to update reward is required for campaign or not
     */
    public function updateReward(Request $request, $campaignId)
    {
        $input = Input::all();
        $campaignRepo = new CampaignRepo();
        try {
            $campaign = $campaignRepo->findById($campaignId);
            $campaign->update($input);
            Toastr::success("Campaign reward updated successfully.", $title = null, $options = []);
            echo json_encode(['responseCode' => 200, 'responseMessage' => 'Campaign detail updated successfully.']);
            die;
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    public function updateOffline()
    {
        $data = Input::all();
        $donation = new Donation();
        if ($donation->create($data)) {
            echo true;
        } else {
            echo false;
        }
    }

    public function updateOfflineUser()
    {
        $data = Input::all();
        $data['name'] = Input::get('first_name').' '.Input::get('last_name');
        $data['password'] = Hash::make($data['password']);
        $data['salutation'] = Input::get('salutation');
        $email = $data['email'];
        $new_user = new RegisterUser();
        $get_user = $new_user->findByEmail($data['email']);
        if (empty($get_user)) {
            $return_data = $new_user->create($data);
            if (!empty($return_data)) {

                $house_number = '';
                $street_name = '';
                $address_1 = '';
                $address_2 = '';
                

                $address = Helper::splitAddress(Input::get('address'));
                
                if(!empty($address['houseNumber'])){

                    $house_number = $address['houseNumber'];
                }

                if(!empty($address['streetName'])){

                    $street_name = $address['streetName'];
                }

                if(!empty($address['additionToAddress1'])){

                    $address_1 = $address['additionToAddress1'];
                }

                if(!empty($address['additionToAddress2'])){

                    $address_2 = $address['additionToAddress2'];
                }

               $userProfile = new UserProfileRepo(); 
               $unique_slug = $userProfile->getUniqueSlug($data['name'], 0);

               //Save details in user_profile table  
               $user_profile_data = array(
                    'user_id' => $return_data->id,
                    'first_name' => Input::get('first_name'),
                    'sur_name' => Input::get('last_name'),
                    'recieve_updates' => 0,
                    'slug' => $unique_slug,
                    'postcode' => empty(Input::get('postcode')) ? '' : Input::get('postcode'),
                    'house_number' => $house_number,
                    'street_name' => $street_name,
                    'addition_to_address' => $address_1.' '.$address_2,
                    'address' => Input::get('address'),//Input::get('address'),
                    'country' => empty(Input::get('country')) ? '' : Input::get('country'),
                    'locality' => empty(Input::get('locality')) ? '' : Input::get('locality'),
                    'lat' => round(Input::get('lat'), 4),
                    'lng' => round(Input::get('lng'), 4),
                );

                //User Role

                //creates UserProfile model and user profile entry in the user_profile table
                $user_profile_response = $userProfile->create($user_profile_data);
                // Mail::send('auth::email.charity', array(), function ($message) use ($email) {
                //     $message->to($email, 'Admin')->subject('You have been Registering');
                // });

                echo $return_data->id;
            }
        }else{
            //echo true;
            echo $get_user->id;
        }


    }
    public function checkofflinedonar()
    {
        $data = Input::all();
        $new_user = new RegisterUser();
        $get_user = $new_user->findByEmail($data['email']);
        if (empty($get_user)) {
            echo 'new';
        }else{
            echo 'old';
        }
        die;
    }
}
