<?php

namespace Modules\Campaigns\Http\Controllers;

use Config;
use DB;
use Helper;
use Illuminate\Http\Request;
use Input;
use Mail;
use Modules\Auth\Entities\UserProfile as UserProfileModel;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Modules\Campaigns\Repositories\EloquentCampaignLaunchRepository as CampaignLaunchRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Campaigns\Requests\CampaignPaymentCreateRequest as CampaignPaymentCreateRequest;
use Modules\Campaigns\Requests\CampaignPaymentUpdateRequest as CampaignPaymentUpdateRequest;
use Pingpong\Admin\Uploader\ImageUploader;
use Pingpong\Modules\Routing\Controller;
use Redirect;
use Session;
use Toastr;
use View;
use App\DonationType;
use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Cornford\Googlmapper\Mapper;

class CampaignLaunchController extends Controller
{

    /**
     * @var ImageUploader
     */
    protected $uploader;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * Function to set variable for views
     *
     * @author AK 03/10/16
     */
    public function setUp()
    {
        //Share variable to view
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return view('campaigns::errors.404');
    }

    /**
     * Display a listing of campaigns.
     *
     * @return Response
     */
    public function index()
    {
        $campaignRepo = new CampaignRepo();
        return view('campaigns::index');
    }

    /**
     * Show the form for creating a new campaign.
     *
     * @return Response
     */
    public function create($campaignId)
    {           
        $this->setUp();
        // GEt User Id From Auth Session
        $userId = \Auth::id();
        // Creting Instance Of Campaign Repo
        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findById($campaignId);
        $campaignStage = $campaign->stage;
        if ($campaignStage < 4) {
            return Redirect::back();
        }

        // Getting the Donation type information
        $donation_type = DB::table('campaign_donor')->where('campaign_id', $campaignId)
            ->join('donation_type', 'donation_type.id', '=', 'campaign_donor.donor_type_id')
            ->get();
        
        //Update : Check if the campaign has a payment method attached
        $ispaymentOptionsAttached = $campaignRepo->findActivePaymentOptionsForCampaign($userId, $campaignId);    

        $paymentOptions = $campaignRepo->findActivePaymentOptions($userId, $campaignId);
        
        $stripe_connect_link = $this->setStripeConnectLink();
        $role = new RoleUserRepo();
        $role_response = $role->findByUserId($userId);
        if ($role_response) {
            if (($role_response->role_id == 2 || $role_response->role_id == 1) && empty($campaign->charity_id)) {
                $percent_application_fees = Config::get('config.app_fee_for_individual');
            } else {
                $percent_application_fees = Config::get('config.app_fee_for_charity');
            }
            $campaign_payment_session = array(
                'user_id' => $userId,
                'campaign_id' => $campaignId,
                'payment_option_id' => 3, //hard-coded-for-stripe-for-now
                'percent_application_fees' => $percent_application_fees,
                'is_default' => 1,
            );
            Session::set('campaign_payment_session', $campaign_payment_session);
            return view('campaigns::launch', compact('role_response', 'campaign', 'campaignId', 'campaignStage', 'paymentOptions', 'stripe_connect_link', 'donation_type'));
        }
    }

    /**
     * Store a newly created campaign launch in storage.
     *
     * @return Response
     */
    public function store(CampaignPaymentCreateRequest $request, $campaignId)
    {
        // GEt User Id From Auth Session
        $userId = \Auth::id();
        $data = $request->only('live_secret_key', 'live_publishable_key', 'payment_option_id');
        $data['user_id'] = $userId;
        $data['campaign_id'] = $campaignId;
        $data['status'] = 'yes';
        // Creting Instance Of Launch Repo
        $launchRepo = new CampaignLaunchRepo();
        $saveCampaignPayment = $launchRepo->create($data);
        if (!empty($saveCampaignPayment)) {
            $payment['id'] = $data['payment_option_id'];
            $payment['campaignPaymentId'] = $saveCampaignPayment->id;
            $payment['status'] = 'yes';
            $payment['name'] = $saveCampaignPayment->name;
            $payment = (object)$payment;
            $responseView = view('campaigns::partials.enablepayment', compact('payment'))->render();
            echo json_encode(['responseCode' => 200, 'responseMessage' => 'Account setup successfully.', 'responseView' => $responseView]);
            die;
        } else {
            echo json_encode(['responseCode' => 0, 'responseMessage' => 'Error']);
            die;
        }
    }

    /**
     * Show the form for editing the specified campaign launch.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified campaign launch in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CampaignPaymentUpdateRequest $request, $designId)
    {

    }

    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show()
    {

    }

    /**
     * Remove the specified campaign from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy()
    {
        return view('campaigns::index');
    }

    public function disablePayment(Request $request)
    {
        $campaignPaymentId = $request->get('campaignPaymentId');
        $payment['status'] = $request->get('status');
        // Creting Instance Of Launch Repo
        $launchRepo = new CampaignLaunchRepo();
        $updatePaymentOption = $launchRepo->update($payment, $campaignPaymentId);
        $payment['id'] = $updatePaymentOption->payment_option_id;
        $payment['name'] = $updatePaymentOption->name;
        $payment['campaignPaymentId'] = $campaignPaymentId;
        $payment = (object)$payment;
        $responseView = view('campaigns::partials.enablepayment', compact('payment'))->render();
        if (!empty($updatePaymentOption)) {
            echo json_encode(['responseCode' => 200, 'responseMessage' => 'Account updated successfully.', 'responseView' => $responseView, 'payment_status' => $payment->status]);
            die;
        } else {
            echo json_encode(['responseCode' => 0, 'responseMessage' => 'Error']);
            die;
        }
    }

    public function launchCampaign($campaignId, Request $request)
    {   
        
        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findById($campaignId);

        // Save donation types
        $data = $request->all();
        $donation_types_sync = [];

        if (isset($data['donation_types_enabled']) && isset($data['donation_type'])) {

            $donation_types = $data['donation_type'];

            foreach ($donation_types as $key => $name) {

                $donationTypeExists = DonationType::where('title', $name)->first();

                if (!isset($donationTypeExists)) {
                    //$campaign->donationTypes()->save(new DonationType(['title' => $name]));
                    $donationType = new DonationType;
                    $donationType->title = $name;
                    $donationType->save();
                    $donation_types_sync[] = $donationType->id;
                } else {
                    //if(!$campaign->donationTypes->contains($donationTypeExists->id)) $campaign->donationTypes()->save($donationTypeExists);
                    $donation_types_sync[] = $donationTypeExists->id;
                }
            }

            if (isset($donation_types_sync)) {

            }
        }
        $campaign->donationTypes()->sync($donation_types_sync);

        // GEt User Id From Auth Session
        $userId = \Auth::id();
        //Get Login User Details
        $user = Session::get('user_session_data');
        // Redirect Back User if Account is not activated.
        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findById($campaignId);

        if (!(Helper::checkIsActivated())) {
            $message = ($user['status'] != '0') ? "Your account is not activated yet" : "Your Account has been blocked by admin";
            Toastr::error($message, $title = null, $options = []);
            return $this->create($campaignId);
        }
        $campaignType = Helper::checkCampaignType($campaignId);
        // Creating Instance Of Campaign Repo
        $campaignLaunchRepo = new CampaignLaunchRepo();
        $activePaymentOptions = $campaignLaunchRepo->findActivePaymentOptions($userId, $campaignId);

        if ($campaignType == "Charity") {
            if ($user['role'] == 'Charity') {
                $charity = UserProfileModel::where('user_id', $campaign->user_id)->first();
            } else {
                $charity = UserProfileModel::where('user_id', $campaign->charity_id)->first();
            }
            if ($charity->is_payment_account != 1) {
                Toastr::error("Please enable stripe payment option.", $title = null, $options = []);
                return Redirect::back();
            }
        }

        if (count($activePaymentOptions) > 0 || $campaignType == 'Charity') {
            // Update Campaign Stage To Stage 2
            $updateStage = $campaignRepo->updateCampaignStage($campaignId, 5);
            // Sending Mail To Creator
            if (Helper::recieveUpdates($user['id']) == true) {
                // Setting Up Email Parameters
                $emailParams['user'] = [
                    'email' => $user['email'],
                    'from' => "GiveBrite",
                    'subject' => 'A New Campaign Created',
                    'campaignName' => !empty($updateStage->campaign_name) ? $updateStage->campaign_name : "",
                    'campaignUrl' => !empty($updateStage->slug) ? url() . "/" . $updateStage->slug : "",
                    'campaignId' => $updateStage->id,
                    'userId' => $user['id'],
                    'createrName' => $user['user_name'],
                    'campaignSlug' => $updateStage->slug,
                    'userSlug' => $user['slug'],
                ];
                // Send Mail About Campaign Creation To User
                Mail::send('campaigns::email.campaign', $emailParams, function ($message) use ($emailParams) {
                    $message->to($emailParams['user']['email'], ucfirst($emailParams['user']['createrName']))->subject($emailParams['user']['subject']);
                });
            }

            Toastr::success("Campaign Launch Successfully.", $title = null, $options = []);
            return Redirect::route('campaign.show', $updateStage->slug);

        } else {
            
            Toastr::error("Please enable stripe payment option.", $title = null, $options = []);
            return Redirect::back();
        }
    }

    public function setStripeConnectLink()
    {
        $client_id = Config::get('app.stripe_client_id');
        // Show OAuth link
        $authorize_request_body = array(
            'response_type' => 'code',
            'scope' => 'read_write',
            'client_id' => $client_id,
        );

        $authorize_uri = 'https://connect.stripe.com/oauth/authorize';

        $url = $authorize_uri . '?' . http_build_query($authorize_request_body);
        return $url;
    }

    public function connectStripeAccount()
    {
        $client_id = Config::get('app.stripe_client_id');
        $secret_key = Config::get('app.stripe_secret_key');

        $token_uri = 'https://connect.stripe.com/oauth/token';
        $code = Input::get('code');
        if (!empty($code)) {
            // Redirect w/ code
            $token_request_body = array(
                'grant_type' => 'authorization_code',
                'client_id' => $client_id,
                'code' => $code,
                'client_secret' => $secret_key,
            );

            $req = curl_init($token_uri);
            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true);
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

            // TODO: Additional error handling
            $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
            $resp = json_decode(curl_exec($req), true);
            curl_close($req);

            if ($resp) {
                if (isset($resp['error'])) {
                    return Redirect::route('404');
                }
            }

            if (Session::has('campaign_payment_session')) {
                $campaign_payment_data = Session::get('campaign_payment_session');
                $campaign_payment_data['status'] = 'yes';
                $campaign_payment_data['connect_access_token'] = $resp['access_token'];
                $campaign_payment_data['connect_livemode'] = $resp['livemode'];
                $campaign_payment_data['connect_refresh_token'] = $resp['refresh_token'];
                $campaign_payment_data['connect_token_type'] = $resp['token_type'];
                $campaign_payment_data['connect_stripe_publishable_key'] = $resp['stripe_publishable_key'];
                $campaign_payment_data['connect_stripe_user_id'] = $resp['stripe_user_id'];
                $campaign_payment_data['connect_scope'] = $resp['scope'];

                $campaign_launch = new CampaignLaunchRepo();
                $campaign_response = $campaign_launch->findByCampaignId($campaign_payment_data['campaign_id']);
                if ($campaign_response) {
                    return Redirect::route('404');
                }
                $campaign_launch->create($campaign_payment_data);
            }

            $campaignId = $campaign_payment_data['campaign_id'];
            $userId = $campaign_payment_data['user_id'];

            $campaignRepo = new CampaignRepo();
            $paymentOptions = $campaignRepo->findActivePaymentOptions($userId, $campaignId);

            $campaignStage = $campaignRepo->findById($campaignId)->stage;

            Toastr::success("Stripe account connected successfully", $title = null, $options = []);
            return Redirect::route('launch.create', $campaignId);
        }
        Toastr::error("Stripe not configured properly", $title = null, $options = []);
        return Redirect::route('404');
    }

    /**
     *
     *strava code
     */
    public function connectStrava($id)
    {

        if (isset($_GET['start'])) {
            return Redirect::away('https://www.strava.com/oauth/authorize?client_id=' . config("config.stravaClientId") . '&response_type=code&redirect_uri=http://givebrite.com/campaigns/launch/connectStrava/' . $id . '&approval_prompt=force');
            //return Redirect::away('https://www.strava.com/oauth/authorize?client_id=' . config("config.stravaClientId") . '&response_type=code&redirect_uri=http://givebrite.dev/campaigns/launch/connectStrava/' . $id . '&approval_prompt=force');
        } else if (isset($_GET['code'])) {
            // for get access token
            $access_tokenCMD = "curl -X POST https://www.strava.com/oauth/token -F client_id=" . config('config.stravaClientId') . " -F client_secret='" . config('config.stravaClientSecretId') . "' -F code='" . $_GET['code'] . "'";
            exec($access_tokenCMD, $data);
            $tokenData = json_decode($data[0]);
            if (isset($tokenData->access_token)) {
                $campaignRepo = new CampaignRepo();
                $campaignModel = $campaignRepo->findById($id);
                $campaignModelData = array();
                $campaignModelData['strava_access_token'] = $tokenData->access_token;
                if ($campaignModel->update($campaignModelData)) {
                    Toastr::success("Strava connected properly", $title = null, $options = []);
                } else {
                    Toastr::error("Strava not connect properly", $title = null, $options = []);
                }
            } else {
                Toastr::error("Strava not connect properly", $title = null, $options = []);
            }
        }
        return Redirect::route('launch.create', $id);
    }


    public function disconnectStrava($id)
    {

        if (isset($id)) {
            $campaignRepo = new CampaignRepo();
            $campaignModel = $campaignRepo->findById($id);
            if ($campaignModel->strava_access_token != null) {
                /** /
                 * $disconnectCMD = 'curl -X POST https://www.strava.com/oauth/deauthorize -H "Authorization: Bearer ' . $campaignModel->strava_access_token . '"';
                 *
                 * exec($disconnectCMD, $data);
                 * $tokenData = json_decode($data[0]);
                 * if ($tokenData->access_token == $campaignModel->strava_access_token) {
                 * /**/

                $campaignModelData = array();
                $campaignModelData['strava_access_token'] = null;
                if ($campaignModel->update($campaignModelData)) {
                    Toastr::success("Strava disconnected properly", $title = null, $options = []);
                } else {
                    Toastr::error("Strava not disconnect properly", $title = null, $options = []);
                }
                /** / } else {
                 * Toastr::error("Strava not disconnect properly", $title = null, $options = []);
                 * } /**/
            }
        }
        return Redirect::route('launch.create', $id);

    }
    /**/

}
