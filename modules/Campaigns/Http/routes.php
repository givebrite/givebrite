<?php

Route::group(['prefix' => '', 'namespace' => 'Modules\Campaigns\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'Modules\Auth\Http\Middleware\IsUnderDevelopmentMiddleware', 'App\Http\Middleware\IsUnsecure']], function() {

    // Routes For Campaign Details Page Step-2
    Route::get('campaigns/{campaignId}/edit', ['as' => 'campaigns.edit', 'uses' => 'CampaignDetailController@edit']);
    Route::put('campaigns/{campaignId}/update', ['as' => 'campaigns.update', 'uses' => 'CampaignDetailController@update']);
    // Routes For Campaign Design Page Step-3
    Route::get('campaigns/{campaignId}/create-design', ['as' => 'design.create', 'uses' => 'CampaignDesignController@create']);
    
    Route::post('campaigns/{campaignId}/save-design', ['as' => 'design.store', 'uses' => 'CampaignDesignController@store']);
    Route::get('campaigns/{campaignId}/edit-design/{designId}', ['as' => 'design.edit', 'uses' => 'CampaignDesignController@edit']);
    Route::put('campaigns/{campaignId}/update-design/{designId}', ['as' => 'design.update', 'uses' => 'CampaignDesignController@update']);

    // Routes For Campaign Reward Page Step-4
    /** Remove For Now
      Route::get('campaigns/{campaignId}/create-reward', ['as' => 'reward.create', 'uses' => 'CampaignRewardController@create']);
      Route::post('campaigns/{campaignId}/save-reward', ['as' => 'reward.store', 'uses' => 'CampaignRewardController@store']);
      Route::post('campaigns/{campaignId}/update-reward', ['as' => 'update.campaign-reward', 'uses' => 'CampaignRewardController@updateReward']);
     * */
      
    // Routes For Campaign Launch Stage-5
    Route::get('campaigns/{campaignId}/create-payment', ['as' => 'launch.create', 'uses' => 'CampaignLaunchController@create']);
    Route::post('campaigns/{campaignId}/save-payment', ['as' => 'launch.store', 'uses' => 'CampaignLaunchController@store']);
    Route::get('campaigns/{campaignId}/launch', ['as' => 'campaign.launch', 'uses' => 'CampaignLaunchController@launchCampaign']);
    
    // Route To Get City For Location
});

// Routes Without Campaign-Id
Route::group(['prefix' => '', 'namespace' => 'Modules\Campaigns\Http\Controllers', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'App\Http\Middleware\IsUnsecure']], function() {
    

    Route::get('campaigns/create', ['as' => 'campaigns.create', 'uses' => 'CampaignDetailController@create']);

    Route::get('search/autocomplete/{string}', 'CampaignDetailController@searchCharities');

    /*  
      New routes for team 
    */
      
    //Route::get('dashboard/team-pages', ['uses' => 'TeamDetailController@teamPages']);
    Route::get('dashboard/team-pages', ['as' => 'dashboard.team-pages', 'uses' => 'TeamDetailController@teamPages']);

    Route::get('team/invite-user/{email}/{team_id}', ['uses' => 'TeamDetailController@inviteUserToTeam']);
    Route::get('team/create', ['uses' => 'TeamDetailController@createTeam']);
    Route::post('team/store', [ 'uses' => 'TeamDetailController@storeTeam']);
    Route::post('team/check-url-availability', [ 'uses' => 'TeamDetailController@checkUrlAvailability']);

    Route::post('team/manage',['uses' => 'TeamDetailController@manageTeam']);

    Route::get('team/{id}/delete', ['uses' => 'TeamDetailController@deleteTeam']);
    Route::get('team/{id}/status/{status}',  ['uses' => 'TeamDetailController@disableTeam']);
    
    Route::get('team/join-team/{id}', ['uses' => 'TeamDetailController@joinTeam']);
    Route::post('team/team-join', ['uses' => 'TeamDetailController@joinTeamPost']);
    
    //accept invitation 
    Route::get('accept-invitation/{team_id}/{email}', ['uses' => 'TeamDetailController@acceptInvitation']);
    Route::get('confirm-invitation/{team_id}/{email}', ['uses' => 'TeamDetailController@confirmInvitation']);
    

    Route::get('team/request-approve/{id}', ['uses' => 'TeamDetailController@approverequest']);

    

    Route::get('team/{id}/add', ['uses' => 'TeamDetailController@teamPages']);

    //team page update routes
    Route::get('team/{id}/step-one-edit', ['uses' => 'TeamDetailController@editStepOneView']);
    Route::post('team/{id}/update-step-one', ['uses' => 'TeamDetailController@updateStepOneView']);
    

    Route::get('team/{id}/step-two-edit', ['uses' => 'TeamDetailController@editSteptwoView']);

        

    Route::post('team/{id}/save-updates', ['as' => 'team-updates.store', 'uses' => 'TeamDetailController@saveUpdate']);
    /*  
      New routes for team 
    */
    

    Route::post('campaigns/store', ['as' => 'campaigns.store', 'uses' => 'CampaignDetailController@store']);
    Route::get('campaigns/launch/connectStrava/{campaignId}', ['as' => 'campaign.connectStrava', 'uses' => 'CampaignLaunchController@connectStrava']);
    Route::get('campaigns/launch/disconnectStrava/{campaignId}', ['as' => 'campaign.disconnectStrava', 'uses' => 'CampaignLaunchController@disconnectStrava']);
    /** Remove For Now
      Route::get('campaigns/{campaignId}/edit-reward', ['as' => 'reward.edit', 'uses' => 'CampaignRewardController@edit']);
      Route::get('campaigns/destroy-reward/{rewardId}', ['as' => 'reward.destroy', 'uses' => 'CampaignRewardController@destroy']);
      Route::put('campaigns/update-reward/{rewardId}', ['as' => 'reward.update', 'uses' => 'CampaignRewardController@update']);
     * */
    Route::post('campaigns/disable-payment', ['as' => 'campaigns.disable-payment', 'uses' => 'CampaignLaunchController@disablePayment']);
    Route::get('getCityByLocation', ['as' => 'campaign.get-city', 'uses' => 'CampaignDesignController@getCityByLocation']);

    Route::get('campaigns/stripe/connect', ['as' => 'campaign.stripe.connect', 'uses' => 'CampaignLaunchController@connectStripeAccount']);
    Route::post('offlinedonation', ['as' => 'campaign.offlinedonation','uses' => 'CampaignRewardController@updateOffline']);
    
    Route::post('offlineregister', ['as' => 'campaign.offlinedonation','uses' => 'CampaignRewardController@updateOfflineUser']);
    
    Route::post('offlineusercheck', ['as' => 'campaign.offlineusercheck','uses' => 'CampaignRewardController@checkofflinedonar']);
});
