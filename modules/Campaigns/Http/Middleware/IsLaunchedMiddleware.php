<?php namespace Modules\Auth\Http\Middleware; 

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Redirect;

class IsLaunchedMiddleware 
{
    
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $slug=$request->route('campaign_slug');
        // Getting Id Of Logged In User
        $loginUserId=\Auth::id();
        $campaignRepo=new CampaignRepo();
        $campaign=$campaignRepo->findBySlug($slug);
        if(!empty($campaign[0]->stage) && ($campaign[0]->stage == 5)){
            // Redirect To Campaign Creation Pages
             return $next($request);
        }else{
            return Redirect::intended('/');
        }
    }
    
}
