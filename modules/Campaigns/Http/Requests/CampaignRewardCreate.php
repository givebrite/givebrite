<?php namespace Modules\Campaigns\Requests;

use App\Http\Requests\Request;
use Input;
use Toastr;

class CampaignRewardCreateRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Basic CampaignReward Validation  
        $rules = [
           'reward_name' => 'required',
           'shipping_required' => 'required',
           'pledge_amount' =>'required|numeric',
           'pledge_description' => 'required' 
        ];
        if(count($rules)>0){
            Toastr::error("Please fill all inputs.", $title = null, $options = []);
        }
        return $rules;
        
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        // Basic CampaignReward Validation  
        $messages = [
           
        ];

        return $messages;
        
    }
}
