<?php namespace Modules\Campaigns\Requests;

use App\Http\Requests\Request;
use Input;
use Toastr;
use Helper;
use Session;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;

class CampaignPaymentCreateRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user= Session::get('user_session_data');
        $campaignId = $this->route('campaignId');
        if(!empty($user) && $user['id']){
            $campaignRepo = new CampaignRepo();
            $findCampaignById = $campaignRepo->findById($campaignId);
            if(!empty($findCampaignById->user_id) && $findCampaignById->user_id == $user['id'] && $findCampaignById->stage < 6){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        self::authorize();
        // Basic Contact Validation  
        $rules = [
            'live_secret_key'=>'required',
            'live_publishable_key'=>'required'
        ];
        if(count($rules)>0){
            Toastr::error("Please fill all inputs.", $title = null, $options = []);
        }
        return $rules;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        // Basic Contact Validation  
        $messages = [
           'live_secret_key.required'=>'Secret key is required.',
           'live_publishable_key.required'=>'Publishable key is required.'
        ];

        return $messages;
        
    }
}
