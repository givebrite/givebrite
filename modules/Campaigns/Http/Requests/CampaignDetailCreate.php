<?php

namespace Modules\Campaigns\Requests;

use App\Http\Requests\Request;
use Input;
use Lang;
use Session;
use Toastr;
use Helper;

class CampaignDetailCreateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        // Basic Contact Validation  
        $rules = [
            'campaign_name' => 'required|min:2|max:35',
            'campaign_category' => 'required|numeric',
            'goal_amount_monetary' => 'required|numeric|min:1|max:99999999'
        ];

        $duration = trim($this->request->get('campaign_duration'));
        if ($duration == 'enddate') {
            $rules['end_date'] = 'required|after:' . date("Y-m-d H:i:s", strtotime("-1 days"));
        }
        /*
          if(count($rules)>0){
          Toastr::error("Please fill all inputs.", $title = null, $options = []);
          }
         */
        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages() {

        // Basic Contact Validation  
        $messages = [
            'campaign_name.required' => Lang::get('campaigns::validations.campaign-detail.campaign_name-required'),
            'campaign_name.min' => Lang::get('campaigns::validations.campaign-detail.campaign_name-min'),
            'campaign_name.max' => Lang::get('campaigns::validations.campaign-detail.campaign_name-max'),
            'goal_amount_monetary.required' => Lang::get('campaigns::validations.campaign-detail.goal_amount_monetary-required'),
            'goal_amount_monetary.min' => Lang::get('campaigns::validations.campaign-detail.goal_amount_monetary-min'),
            'goal_amount_monetary.max' => Lang::get('campaigns::validations.campaign-detail.goal_amount_monetary-max'),
            'goal_amount_monetary.numeric' => Lang::get('campaigns::validations.campaign-detail.goal_amount_monetary-numeric'),
            'end_date.required' => Lang::get('campaigns::validations.campaign-detail.end_date-required'),
            'end_date.after' => Lang::get('campaigns::validations.campaign-detail.end_date-after')
        ];
        return $messages;
    }

}
