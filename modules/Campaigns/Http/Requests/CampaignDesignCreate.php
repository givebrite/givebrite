<?php

namespace Modules\Campaigns\Requests;

use App\Http\Requests\Request;
use Input;
use Toastr;
use Pingpong\Admin\Uploader\ImageUploader;
use Config;
use Helper;
use Session;
use Validator;
use Lang;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;

class CampaignDesignCreateRequest extends Request {

    /**
     * @var ImageUploader
     */
    protected $uploader;
    protected $imageStoragePath;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader) {
        ini_set('memory_limit', '1048M');
        ini_set("upload_max_filesize", "50");
        ini_set('post_max_size', '100M');
        $this->uploader = $uploader;
        $this->imageStoragePath = Config::get('config.upload_path');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        $user = Session::get('user_session_data');
        $campaignId = $this->route('campaignId');
        if (!empty($user) && $user['id']) {
            $campaignRepo = new CampaignRepo();
            $findCampaignById = $campaignRepo->findById($campaignId);
            if (!empty($findCampaignById->user_id) && $findCampaignById->user_id == $user['id'] && $findCampaignById->stage < 6) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        self::authorize();
        // Basic Contact Validation  
        $rules = [
            'theme_id' => 'required',
            'campaign_description' => 'required|min:50',
            'country' => 'required',
            'locality' => 'required',
            'postcode' => 'required'
        ];
        $country = Input::get('country');
        $locality = Input::get('locality');
//        if(!empty($country) && !empty($locality)){
//            $rules['postcode'] = ['regex:/^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$/'];
//        }
        $mediaType = trim(Input::get('media_type'));
        if ($mediaType == "facebook") {
            $rules['facebook_url'] = 'required|url';
        } elseif ($mediaType == "youtube") {
            $rules['media_url'] = 'required|url';
        } elseif (!Session::has('image')) {
            $rules['campaign_image'] = 'required|mimes:jpeg,png,gif,jpg|max:25000';
        } else {
            $rules['campaign_image'] = 'mimes:jpeg,png,gif,jpg|max:25000';
        }
//        
//       Validator::replacer('image_dimension', function($message, $attribute, $rule, $parameters) {
//            return str_replace('validation.image_dimension', 'The image size must not be greater than '.Config::get('config.campaign_image.width').'*'.Config::get('config.campaign_image.height').'.', $message);
//        });
//        
        if (\Input::hasFile('campaign_image') && !Session::has('image')) {
            //echo $v= ini_get('memory_limit');die;
            $data['campaign_image'] = Input::file('campaign_image');
            $imageRule['campaign_image'] = $rules['campaign_image'];
            $validator = Validator::make($data, $imageRule, $this->messages());
            if (!$validator->fails()) {
                $campaignImage = $data['campaign_image'];
                $uploadImage = $this->uploadImage($campaignImage);
            }
        }
        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages() {

        // Basic Contact Validation  
        $messages = [
            'postcode.required' => Lang::get('campaigns::validations.campaign-design.postcode-required'),
            'postcode.regex' => Lang::get('campaigns::validations.campaign-design.postcode-regex'),
            'theme_id.required' => Lang::get('campaigns::validations.campaign-design.theme_id-required'),
            'facebook_url.required' => Lang::get('campaigns::validations.campaign-design.facebook_url-required'),
            'media_url.required' => Lang::get('campaigns::validations.campaign-design.media_url-required'),
            'facebook_url.url' => Lang::get('campaigns::validations.campaign-design.facebook_url-url'),
            'media_url.url' => Lang::get('campaigns::validations.campaign-design.media_url-url'),
            'campaign_image.required' => Lang::get('campaigns::validations.campaign-design.campaign_image-required'),
            'campaign_image.mimes' => Lang::get('campaigns::validations.campaign-design.campaign_image-mimes'),
            'campaign_image.min' => Lang::get('campaigns::validations.campaign-design.campaign_image-min'),
            'campaign_image.max' => Lang::get('campaigns::validations.campaign-design.campaign_image-max'),
            'campaign_description.required' => Lang::get('campaigns::validations.campaign-design.campaign_description-required'),
            'campaign_description.min' => Lang::get('campaigns::validations.campaign-design.campaign_description-min'),
            'country.required' => Lang::get('campaigns::validations.campaign-design.location-required'),
            'locality.required' => Lang::get('campaigns::validations.campaign-design.city-required')
        ];

        return $messages;
    }

    public function uploadImage($campaignImage) {
        // upload image
        $image = $this->uploader->upload('campaign_image');
        $image->save($this->imageStoragePath);
        $imageName = $image->getFilename();

        $image_detail = array(
            'inputFile' => $campaignImage,
            'image' => $image,
            'thumb_size' => Config::get('config.thumb_campaign_size'),
            'thumb_path' => Config::get('config.thumb_campaign_upload_path'),
            'large_size' => Config::get('config.large_campaign_size'),
            'large_path' => Config::get('config.large_campaign_upload_path')
        );
        Helper::createThumb($image_detail);
        // Forget Previous session
        Session::forget('image');
        // Set Image Name In Session
        Session::set('image', $imageName);
    }

}
