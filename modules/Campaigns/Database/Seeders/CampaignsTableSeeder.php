<?php

namespace Modules\Campaigns\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class CampaignsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        DB::table('campaigns')->insert([
            
            'user_id' => 115,
            'campaign_name' => 'Fariyad',
            'goal_amount_type' => 'monetary',
            'goal_amount_monetary' => 200,
            'campaign_category' => 1,
            'charity_id' => 1,
        ]);
    }

}
