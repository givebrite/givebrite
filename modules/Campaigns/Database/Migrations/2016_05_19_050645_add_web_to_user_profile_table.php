<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebToUserProfileTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profile', function(Blueprint $table)
        {
             $table->enum('registered_charity', ['yes', 'no'])->after('sur_name')->default('no');
             $table->string('charity_logo')->after('recieve_updates')->nullable();
             $table->string('registration_no')->after('charity_logo')->nullable();
             $table->string('charity_name')->after('registration_no')->nullable();
             $table->string('contact_telephone')->after('charity_name')->nullable();
             $table->string('web')->after('contact_telephone')->nullable();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profile', function(Blueprint $table)
        {

        });
    }

}
