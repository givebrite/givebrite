<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveMediaUrlFromCampaignsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns', function(Blueprint $table)
		{   
                    Schema::table('campaigns', function(Blueprint $table)
                    {
                         if (Schema::hasColumn('campaigns', 'registered_charity')) {
                            $table->dropColumn(['registered_charity']);
                         }
                         if (Schema::hasColumn('campaigns', 'media_url')) {
                            $table->dropColumn(['media_url']);
                         }
                         if (Schema::hasColumn('campaigns', 'facebook_url')) {
                              $table->dropColumn(['facebook_url']);
                         }
                    });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campaigns', function(Blueprint $table)
		{

		});
	}

}
