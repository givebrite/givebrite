<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignRewardsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_rewards', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('campaign_id')->unsigned();
            $table->string('reward_name');
            $table->float('pledge_amount');
            $table->text('pledge_description');
            $table->enum('shipping_required',['yes','no'])->default('no');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_rewards');
    }

}
