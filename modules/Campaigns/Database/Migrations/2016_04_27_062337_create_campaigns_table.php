<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('campaign_name');
            $table->enum('goal_amount_type', ['time', 'monetary'])->default('monetary');
            $table->string('goal_amount_time')->nullable();
            $table->string('goal_amount_monetary')->nullable();
            $table->enum('campaign_type', ['cause', 'business'])->default('cause');
            $table->integer('campaign_category')->unsigned();
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->integer('charity_id')->unsigned();
            $table->enum('campaign_rewards', ['yes', 'no'])->default('no');
            $table->enum('stage', ['2', '3','4','5'])->default('2');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaigns');
    }

}
