<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartDateToCampaignsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function(Blueprint $table)
        {
            $table->string('start_date')->nullable()->after('facebook_url');
            $table->string('end_date')->nullable()->after('start_date');
            $table->enum('stage', ['1','2', '3','4','5'])->default('1')->after('registered_charity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {

        });
    }

}
