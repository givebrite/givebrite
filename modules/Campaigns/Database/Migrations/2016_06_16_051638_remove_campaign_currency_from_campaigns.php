<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCampaignCurrencyFromCampaigns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns', function(Blueprint $table)
		{   
                    Schema::table('campaigns', function(Blueprint $table)
                    {
                         if (Schema::hasColumn('campaigns', 'campaign_currency')) {
                            $table->dropColumn(['campaign_currency']);
                         }
                    });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('', function(Blueprint $table)
		{

		});
	}

}
