<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCampaignPayments extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_payments', function(Blueprint $table)
        {
            $table->string('connect_access_token');
            $table->string('connect_livemode');
            $table->string('connect_refresh_token');
            $table->string('connect_token_type');
            $table->string('connect_stripe_publishable_key');
            $table->string('connect_stripe_user_id');
            $table->string('connect_scope');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_payments', function(Blueprint $table)
        {

        });
    }

}
