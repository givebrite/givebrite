<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignStravaActivities extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_strava_activities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('campaign_id')->unsigned();
            $table->integer('activity_id');
            $table->string('activity_name');
            $table->string('distance');
            $table->string('sports');
            $table->datetime('start_date')->nullable();
            $table->string('moving_time');
            $table->string('average_speed');
            $table->string('start_lat');
            $table->string('start_long');
            $table->string('end_lat');
            $table->string('end_long');
            $table->longText('summary_polyline'); 
            $table->string('gmap_img');
            $table->longText('graph_data');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_strava_activities');
    }

}
