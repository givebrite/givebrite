<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRewardIdFromCampaignRewardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaign_rewards', function(Blueprint $table)
		{
                     if (Schema::hasColumn('campaign_rewards', 'reward_id')) {
                        $table->dropColumn(['reward_id']);
                     }
                     if (Schema::hasColumn('campaign_rewards', 'reward_name')) {
                          $table->dropColumn(['reward_name']);
                     }
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campaign_rewards', function(Blueprint $table)
		{

		});
	}

}
