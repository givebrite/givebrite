<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCampaignDescriptionFromCampaignDesignTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaign_design', function(Blueprint $table)
		{   
                    Schema::table('campaign_design', function(Blueprint $table)
                    {
                         if (Schema::hasColumn('campaign_design', 'campaign_description')) {
                            $table->dropColumn(['campaign_description']);
                         //   $table->text('campaign_description')->after('campaign_image')->nullable();
                         }
                         
                    });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campaign_design', function(Blueprint $table)
		{

		});
	}

}
