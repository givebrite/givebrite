<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignPaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_payments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('campaign_id');
            $table->integer('payment_option_id');
            $table->string('live_secret_key')->nullable();
            $table->string('live_publishable_key')->nullable();
            $table->enum('status',['yes','no'])->default('no');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_payments');
    }

}
