<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationCostToCampaignRewardsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_rewards', function(Blueprint $table)
        {
            $table->text('location_cost')->nullable()->after('shipping_required');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_rewards', function(Blueprint $table)
        {

        });
    }

}
