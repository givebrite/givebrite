<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePhoneFromUserProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_profile', function(Blueprint $table)
		{   
                    Schema::table('user_profile', function(Blueprint $table)
                    {
                         if (Schema::hasColumn('user_profile', 'phone')) {
                            $table->dropColumn(['phone']);
                         }
                         
                    });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_profile', function(Blueprint $table)
		{

		});
	}

}
