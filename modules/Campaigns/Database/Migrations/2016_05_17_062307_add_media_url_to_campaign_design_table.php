<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMediaUrlToCampaignDesignTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_design', function(Blueprint $table)
        {
             $table->enum('media_type',['image','facebook','youtube'])->after('theme_id')->default('image');
             $table->string('media_url')->after('campaign_image')->nullable();
             $table->string('facebook_url')->after('media_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_design', function(Blueprint $table)
        {

        });
    }

}
