<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEmailFromCampaignsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns', function(Blueprint $table)
		{   
                    Schema::table('campaigns', function(Blueprint $table)
                    {
                         if (Schema::hasColumn('campaigns', 'charity_id')) {
                            $table->dropColumn(['charity_id']);
                         }
                    });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campaigns', function(Blueprint $table)
		{

		});
	}

}
