<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRewardIdToCampaignRewardsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_rewards', function(Blueprint $table)
        {
            $table->integer('reward_id')->nullable()->after('campaign_id');
            $table->string('reward_name')->nullable()->after('reward_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_rewards', function(Blueprint $table)
        {

        });
    }

}
