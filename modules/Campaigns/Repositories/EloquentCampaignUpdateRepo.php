<?php

namespace Modules\Campaigns\Repositories;

use Modules\Campaigns\Entities\CampaignUpdate as CampaignUpdateModel;
use DB;

class EloquentCampaignUpdateRepository {

    public function getModel() {
        return new CampaignUpdateModel();
    }

    public function findByCampaignId($campaignId) {
        return $this->getModel()->where('campaign_id', $campaignId)->first();
    }

    public function findById($optionId) {
        return $this->getModel()->where('id', $optionId)->first();
    }

    public function create(array $data) {
        $save = $this->getModel()->create($data);
        return $this->findById($save->id);
    }

    public function getUpdatesByCampaignId($campaignId = 0) {
        return DB::table('campaign_updates as update')
                        ->join('users as user', 'user.id', '=', 'update.user_id')
                        ->join('campaigns as campaign', 'campaign.id', '=', 'update.campaign_id')
                        ->select('update.*', 'user.name', 'campaign.campaign_name')
                        ->where('update.campaign_id', $campaignId)
                        ->orderBy('update.created_at', 'desc');
    }

    public function getCountOneCampaign($campaignId) {
        return $this->getModel()->where('campaign_id', $campaignId)->count();
    }

}
