<?php

namespace Modules\Campaigns\Repositories;

use Modules\Campaigns\Entities\Team as TeamModel;
use Helper;
use DB;

class EloquentTeamRepository
{

    public function getModel()
    {
        return new TeamModel();
    }

    public function userCreatedTeams($id)
    {
        return TeamModel::where('created_by_user',$id)->get();
    }

    public function findById($id)
    {
        return TeamModel::findOrFail($id);
    }

    public function delete($id)
    {
        return TeamModel::destroy($id);
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    public function update(array $data, $id)
    {
        // Getting Campaign Model Instance
        $team = $this->findById($id);
        // Update stage for campaign 
        $team->update($data);

        return true;
    }

    public function updateStatus($status, $id){

        $team = $this->findById($id);
        $team->status = $status;
        return true;
    }

    public function acceptInvitation($team_id,$email){

        $id = DB::table('team_members')->where('team_id',$team_id)->where('invite_email',$email)
              ->update(['invite_accepted'=>'1']);

        return $id;
    }


    //Move to next model
    public function saveTeamCampaign($data){

        $id = DB::table('team_campaigns')->insertGetId($data);

        return $id;
    }

    public function inviteUserToTeam($email,$team_id,$type){

        $data['team_id'] = $team_id;
        $data['invite_email'] = $email;
        $data['invite_type'] = $type;

        $id = DB::table('team_members')->insertGetId($data);

        return $id;

    }

    public function approveRequest($id){

        return DB::table('team_members')->where('id', '=', $id)
            ->update(array('invite_accepted' => 1));
  
    }

    /**
     *
     * @param type $slug
     * @return type
     */
    public function findBySlug($slug)
    {   
        $team =  DB::table('teams')
                    ->join('team_campaigns', 'teams.team_id', '=', 'team_campaigns.team_id')
                    ->join('campaigns', 'team_campaigns.campaign_id', '=', 'campaigns.id')
                    ->select('teams.*','campaigns.slug')
                    ->where('team_url',$slug)->get();
        
            return $team[0];    
                           
    }

     

    //Get the team details associated with the campain
    public function findByCampaignId($id){


        return DB::table('teams')
                 ->join('team_campaigns', 'teams.team_id', '=', 'team_campaigns.team_id')
                 ->select('teams.team_id','teams.team_name','teams.team_url','teams.team_logo','team_campaigns.campaign_id')
                 ->where('team_campaigns.campaign_id',$id)->get();

    }    

    public function saveUpdate($team_id,$user_id,$update){

        return DB::table('team_updates')->insertGetId(
            ['team_id' => $team_id, 'user_id' => $user_id, 'update'=>$update]
        );

    }

    public function findTeamUpdates($team_id){

       return DB::table('team_updates')->join('user_profile','team_updates.user_id','=','user_profile.user_id')->where(
            ['team_id' => $team_id]
        )->get();

    }

    //Move to eloquent
    public function getPageDetailsWithTeam($user_id){

        return DB::select("SELECT T.team_id,T.`team_name`,T.team_url,T.status,T.`cover_photo`,C.`id` AS campaign_id ,C.`campaign_name`,C.`goal_amount_monetary` , (SELECT COALESCE(SUM(donation_amount_monetory),0) FROM `campaign_donations` WHERE `campaign_id` = 'C.id' ) AS totalRaised FROM teams T  JOIN  `team_campaigns` TC ON  TC.`team_id` = T.`team_id`
            JOIN `campaigns` C ON C.id = TC.`campaign_id` WHERE T.created_by_user = '$user_id'");

    }

    public function findTeamMembers($team_id){

        // return  DB::table('team_members')
        //         ->join('users', 'team_members.invite_sent_to_email', '=', 'users.email')
        //         ->join('user_profile', 'users.id', '=', 'user_profile.user_id')
        //         ->leftjoin('campaign_donations', 'users.id', '=', 'campaign_donations.user_id')
        //         ->where('invite_accepted',1)
        //         ->select('user_profile.profile_pic','user_profile.first_name','user_profile.sur_name','campaign_donations.donation_amount_monetory')
        //         ->toSql();
            return DB::select("SELECT TM.team_id,TM.id as invite_id, TM.invite_email as email, TM.invite_type,TM.invite_accepted, P.profile_pic,P.first_name,P.sur_name,(SELECT  COALESCE(SUM(donation_amount_monetory), 0)  FROM campaign_donations WHERE  user_id = U.id)   AS totalDonations,(SELECT  count(*) FROM campaign_donations WHERE  user_id = U.id)   AS totalDonationsCount FROM team_members TM
                INNER JOIN users U ON TM.invite_email = U.email  
                INNER JOIN user_profile P ON U.id = P.user_id
                WHERE TM.team_id='$team_id' AND invite_accepted = '1'");
       

    }

    public function findMembersBasic($team_id){

        return DB::select("SELECT TM.team_id,TM.id AS invite_id, TM.invite_email AS email, TM.invite_type,TM.invite_accepted, P.profile_pic,P.first_name,P.sur_name FROM team_members TM JOIN users U ON TM.invite_email = U.email JOIN user_profile P ON U.id = P.user_id WHERE TM.team_id='$team_id' AND invite_accepted = '1'");
    }


    public function findByTeamUrl($teamUrl){


        return DB::table('teams')->select('teams.team_id')->where('teams.team_url',$teamUrl)->get();

    } 

}
