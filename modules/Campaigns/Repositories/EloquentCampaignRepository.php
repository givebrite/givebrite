<?php

namespace Modules\Campaigns\Repositories;

use Modules\Campaigns\Entities\Campaign as CampaignModel;
use Modules\Campaigns\Entities\CampaignCategory as CampaignCategoryModel;
use Modules\Campaigns\Entities\CampaignTheme as CampaignThemeModel;
use Modules\Campaigns\Entities\CampaignLocation as CampaignLocationModel;
use Modules\Campaigns\Entities\CampaignReward as CampaignRewardModel;
use Modules\Campaigns\Entities\PaymentOption as PaymentOptionModel;
use Pingpong\Admin\Entities\User as UserModel;
use Modules\Campaigns\Entities\CampaignPayment as CampaignPaymentModel;
use DB;
use Illuminate\Support\Str as Str;
use Helper;
use Carbon\Carbon;

class EloquentCampaignRepository
{

    public function getModel()
    {
        return new CampaignModel();
    }

    public function findById($id)
    {
        return CampaignModel::findOrFail($id);
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    public function update(array $data, $id)
    {
        // Getting Campaign Model Instance
        $campaign = $this->findById($id);
        // Update stage for campaign 
        $campaign->update($data);
        return true;
    }

    public function getCampaignCategories()
    {
        return CampaignCategoryModel:: lists('name', 'id')->toArray();
    }

    public function getCampaignCategoriesWithSlug()
    {
        return CampaignCategoryModel:: lists('name', 'slug')->toArray();
    }

    public function getCampaignThemes()
    {
        return CampaignThemeModel:: lists('value', 'id')->toArray();
    }

    public function getCampaignLocations()
    {
        return CampaignLocationModel:: where('parent_id', '=', '0')->orderBy('name', 'asc')->lists('name', 'id')->toArray();
    }

    public function updateCampaignStage($campaignId, $stage)
    {
        // Getting Campaign Model Instance
        $campaign = $this->findById($campaignId);
        if ($campaign->stage < $stage) {
            $data['stage'] = $stage;
            // Update stage for campaign 
            $campaign->update($data);
        }
        return $campaign;
    }

    /*
     * @author Ajay Saini
     */

    public function getCampaignCityByLocation($locationId)
    {
        return CampaignLocationModel:: where('parent_id', '=', $locationId)->orderBy('name', 'asc')->lists('name', 'id')->toArray();
    }

     public function findActivePaymentOptionsForCampaign($userId, $campaignId){

        //if the campaign has payment attached
        $hasPaymentOption = DB::table('campaign_payments')
                   ->where('user_id','=',$userId)
                   ->where('campaign_id','=',$campaignId)
                   ->get();
        
        if(empty($hasPaymentOption)){

            //get users default payment option if available
            $defaultPaymentOption = DB::table('campaign_payments')
                   ->where('user_id','=',$userId)
                   ->where('campaign_id','=','0')
                   ->get();

            if(empty($defaultPaymentOption)){

                return 1;
            }       
            //Save a new row for the campaign
            unset($defaultPaymentOption[0]->id);
            unset($defaultPaymentOption[0]->created_at);
            unset($defaultPaymentOption[0]->updated_at);
            
            $defaultPaymentOption[0]->campaign_id = $campaignId;
            
            $data = [
                        'user_id'  => $defaultPaymentOption[0]->user_id,
                        'campaign_id'    => $defaultPaymentOption[0]->campaign_id,
                        'payment_option_id'       => $defaultPaymentOption[0]->payment_option_id,
                        'status'  => $defaultPaymentOption[0]->status,
                        'connect_access_token'    => $defaultPaymentOption[0]->connect_access_token,
                        'connect_livemode'       => $defaultPaymentOption[0]->connect_livemode,
                        'connect_refresh_token'  => $defaultPaymentOption[0]->connect_refresh_token,
                        'connect_token_type'    => $defaultPaymentOption[0]->connect_token_type,
                        'connect_stripe_publishable_key' => $defaultPaymentOption[0]->connect_stripe_publishable_key,
                        'connect_stripe_user_id'  => $defaultPaymentOption[0]->connect_stripe_user_id,
                        'connect_scope'    => $defaultPaymentOption[0]->connect_scope,
                        'percent_application_fees' => $defaultPaymentOption[0]->percent_application_fees,
                        'is_default'       => $defaultPaymentOption[0]->is_default,
                        'user_email'       => $defaultPaymentOption[0]->user_email,
                        'user_country'       => $defaultPaymentOption[0]->user_country,
                        'user_name'       => $defaultPaymentOption[0]->user_name,
                        
                    ];                        
            
            DB::table('campaign_payments')->insertGetId($data);  

            return  1;     
        }

        return 1;           
    }


    /*
     * @author Ajay Saini
     */

    public function findActivePaymentOptions($userId, $campaignId, $paymentOptionId = 3)
    {
        return DB::table('payment_options as paymentOption')
            ->leftJoin('campaign_payments as campaignPayments', 

                function ($join) use ($userId, $campaignId) {
                    $join->on('campaignPayments.payment_option_id', '=', 'paymentOption.id');
                    $join->on('campaignPayments.user_id', '=', DB::raw("'" . $userId . "'"));
                    $join->on('campaignPayments.campaign_id', '=', DB::raw("'" . $campaignId . "'"));
                    if (!empty($paymentOptionId)) {
                        $join->on('campaignPayments.payment_option_id', '=', $paymentOptionId);
                    }
            })
            ->select('paymentOption.id', 'paymentOption.name', 'paymentOption.label', 'paymentOption.description', 'campaignPayments.status', 'campaignPayments.id as campaignPaymentId')
            ->where('paymentOption.status', 'yes')
            ->orderBy('paymentOption.created_at', 'asc')
            ->get();
    }

    /**
     *
     * @param type $userId
     * @param type $campaignId
     * @param type $paymentOptionID
     */
    public function findConnectedPaymentAccount($userId, $campaignId, $paymentOptionID)
    {
        return CampaignPaymentModel::where('user_id', $userId)
            ->where('campaign_id', $campaignId)
            ->where('payment_option_id', $paymentOptionID)
            ->where('status', 'yes')
            ->first();
    }
public function findByids($slug)
    {
        return DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->join('role_user as userRole', 'user.id', '=', 'userRole.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('campaign_themes as campaignTheme', 'campaignTheme.id', '=', 'campaignDesign.theme_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->leftjoin('user_profile as charityProfile', 'charityProfile.user_id', '=', 'campaign.charity_id')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_rewards as campaignReward', 'campaignReward.campaign_id', '=', 'campaign.id')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select(
                'campaign.id as campaignId', 'campaign.user_id', 'campaign.campaign_name', 'campaign.goal_amount_type', 'campaign.goal_amount_time', 'campaign.goal_amount_monetary', 'campaign.campaign_currency', 'campaign.created_at', 'campaign.updated_at', 'campaign.campaign_type', 'campaign.campaign_category', 'campaignDesign.media_type', 'campaignDesign.media_url', 'campaignDesign.facebook_url', 'campaign.start_date', 'campaign.end_date', 'campaign.charity_id', 'campaign.campaign_rewards', 'campaign.stage', 'campaign.slug', 'campaignReward.id as rewardId', 'campaignReward.reward_name', 'campaignReward.location_cost', 'campaignCategory.name as categoryName', 'campaignDesign.theme_id', 'campaignDesign.campaign_image', 'campaignDesign.campaign_description', 'campaignDesign.location', 'campaignDesign.city', 'campaignDesign.lattitude', 'campaignDesign.longitude', 'campaignTheme.name as themeName', 'campaignTheme.value as themeValue', 'user.name as createrName', 'userProfile.web as userWeb', 'userProfile.slug as userSlug', 'userProfile.profile_pic as userProfilePic', 'userProfile.charity_logo as charityLogo', 'userRole.role_id as createrRole', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'), DB::raw('count(campaignDonation.id) as totalDonors'), 'charityProfile.user_id as charityId', 'charityProfile.registration_no as charityRegistration', 'charityProfile.charity_name as charityName', 'charityProfile.web as charityWeb', 'charityProfile.description as charityDescription', 'charityProfile.contact_telephone as charityConatct', 'charityProfile.charity_logo', 'charityProfile.slug as charitySlug','campaign.strava_access_token as stravaAccessToken'
            )
            ->where('campaign.id', '=', $slug)
            ->where('campaign.stage', '=', 5)
            ->where('user.status', '=', 1)
            ->groupBy('campaignDonation.user_id')
            ->where('campaign.status', '=', 1)
            ->first();
    }
    /**
     *
     * @return type
     */
    public function getAllCharities()
    {
        return UserModel::
        join('role_user as userRole', 'userRole.user_id', '=', 'users.id')
            ->join('user_activation as activation', 'activation.user_id', '=', 'users.id')
            ->join('user_profile as profile', 'profile.user_id', '=', 'users.id')
            ->where('userRole.role_id', '3')
            ->where('users.status', '1')
            ->where('activation.is_activated', '1')
            ->where('profile.is_payment_account', '1')
            ->orderBy('name', 'asc')
            ->lists('users.name', 'users.id')->toArray();
    }

    /**
     *
     * @return type
     */
    public function getUserConnectedCharities($userID)
    {
        return DB::table('campaigns')
            ->join('users', 'users.id', '=', 'campaigns.charity_id')
            ->where('campaigns.user_id',$userID)
            ->orderBy('name', 'asc')
            ->lists('users.name', 'users.id');
    }

    public function getAllCharitiesLogo()
    {
        return UserModel::
        join('role_user as userRole', 'userRole.user_id', '=', 'users.id')
            ->join('user_activation as activation', 'activation.user_id', '=', 'users.id')
            ->join('user_profile as profile', 'profile.user_id', '=', 'users.id')
            ->where('userRole.role_id', '3')
            ->where('users.status', '1')
            ->where('activation.is_activated', '1')
            ->where('profile.is_payment_account', '1')
            ->lists('profile.charity_logo', 'users.id')->toArray();
    }

    /**
     *
     * @param type $slug
     * @return type
     */
    public function findBySlug($slug)
    {
        return DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->join('role_user as userRole', 'user.id', '=', 'userRole.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('campaign_themes as campaignTheme', 'campaignTheme.id', '=', 'campaignDesign.theme_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->leftjoin('user_profile as charityProfile', 'charityProfile.user_id', '=', 'campaign.charity_id')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_rewards as campaignReward', 'campaignReward.campaign_id', '=', 'campaign.id')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select(
                'campaign.id as campaignId', 'campaign.user_id', 'campaign.campaign_name', 'campaign.goal_amount_type', 'campaign.goal_amount_time', 'campaign.goal_amount_monetary', 'campaign.campaign_currency', 'campaign.created_at', 'campaign.updated_at', 'campaign.campaign_type', 'campaign.campaign_category', 'campaignDesign.media_type', 'campaignDesign.media_url', 'campaignDesign.facebook_url', 'campaign.start_date', 'campaign.end_date', 'campaign.charity_id', 'campaign.campaign_rewards', 'campaign.stage', 'campaign.slug', 'campaignReward.id as rewardId', 'campaignReward.reward_name', 'campaignReward.location_cost', 'campaignCategory.name as categoryName', 'campaignDesign.theme_id', 'campaignDesign.campaign_image', 'campaignDesign.campaign_description', 'campaignDesign.location', 'campaignDesign.city', 'campaignDesign.lattitude', 'campaignDesign.longitude', 'campaignTheme.name as themeName', 'campaignTheme.value as themeValue', 'user.name as createrName', 'userProfile.web as userWeb', 'userProfile.slug as userSlug', 'userProfile.profile_pic as userProfilePic', 'userProfile.charity_logo as charityLogo', 'userRole.role_id as createrRole', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'), DB::raw('count(campaignDonation.id) as totalDonors'), 'charityProfile.user_id as charityId', 'charityProfile.registration_no as charityRegistration', 'charityProfile.charity_name as charityName', 'charityProfile.web as charityWeb', 'charityProfile.description as charityDescription', 'charityProfile.contact_telephone as charityConatct', 'charityProfile.charity_logo', 'charityProfile.slug as charitySlug','campaign.strava_access_token as stravaAccessToken'
            )
            ->where('campaign.slug', '=', $slug)
            ->where('campaign.stage', '=', 5)
            ->where('user.status', '=', 1)
            ->groupBy('campaignDonation.user_id')
            ->where('campaign.status', '=', 1)
            ->get();
    }

    /**
     *
     * @param type $userId
     * @param type $paginate
     * @return type
     */
    public function getCampaignWithJoin($userId = 0, $launch = "true", $status = 'true')
    {

        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'campaignCategory.name as CampCategory', 'user.name', 'userProfile.web as userWeb', 'userProfile.profile_pic', 'campaignDesign.location', 'campaignDesign.lattitude', 'campaignDesign.longitude', 'campaignDesign.city', 'campaignDesign.campaign_image', 'campaignDesign.campaign_description', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'))
            ->where(function ($que) use ($userId, $launch, $status) {
                if ($userId != 0) {
                    $que->where('campaign.user_id', $userId);
                }
                if ($launch == true) {
                    $que->where('campaign.stage', 5);
                    $que->where('user.status', 1);
                }
                if ($status == true) {
                    $que->where('campaign.status', 1);
                }
            })
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');
        return $query;
    }

    public function getCampaignWithJoinNearlyExpired($userId = 0, $launch = "true", $status = 'true')
    {

        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'campaignCategory.name as CampCategory', 'user.name', 'userProfile.web as userWeb', 'userProfile.profile_pic', 'campaignDesign.location', 'campaignDesign.lattitude', 'campaignDesign.longitude', 'campaignDesign.city', 'campaignDesign.campaign_image', 'campaignDesign.campaign_description', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'))
            ->where(function ($que) use ($userId, $launch, $status) {
                if ($userId != 0) {
                    $que->where('campaign.user_id', $userId);
                }
                if ($launch == true) {
                    $que->where('campaign.stage', 5);
                    $que->where('user.status', 1);
                }
                if ($status == true) {
                    $que->where('campaign.status', 1);
                }
            })
            ->orderBy('campaign.end_date', 'desc')
            ->groupBy('campaign.id');
        return $query;
    }

    public function getCampaignNearlyCompleteJoin($launch = "true", $status = 'true')
    {

        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'campaignCategory.name as CampCategory', 'user.name', 'userProfile.web as userWeb', 'userProfile.profile_pic', 'campaignDesign.location', 'campaignDesign.lattitude', 'campaignDesign.longitude', 'campaignDesign.city', 'campaignDesign.campaign_image', 'campaignDesign.campaign_description', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'))
            ->where(function ($que) use ($launch, $status) {
                $que->where('campaign.goal_amount_monetary', ">", 0.8);
                if ($launch == true) {
                    $que->where('campaign.stage', 5);
                    $que->where('user.status', 1);
                }
                if ($status == true) {
                    $que->where('campaign.status', 1);
                }
            })
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');
        return $query;
    }

    public function getallUserActiveCampaigns($userId = 0, $launch = "true", $status = 'true'){

        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*')
            ->where(function ($que) use ($userId, $launch, $status) {
                if ($userId != 0) {
                    $que->where('campaign.user_id', $userId);
                }
                if ($launch == true) {
                    $que->where('campaign.stage', 5);
                    $que->where('user.status', 1);
                }
            })
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');
        return $query->get();
    }



    public function getCampaignWithJoinDashboard($userId = 0, $launch = "true", $status = 'true')
    {

        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'campaignCategory.name as CampCategory', 'user.name', 'userProfile.web as userWeb', 'userProfile.profile_pic', 'campaignDesign.location', 'campaignDesign.lattitude', 'campaignDesign.longitude', 'campaignDesign.city', 'campaignDesign.campaign_image', 'campaignDesign.campaign_description', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'), DB::raw('count(campaignDonation.id) as NumberOfDonor'))
            ->where(function ($que) use ($userId, $launch, $status) {
                if ($userId != 0) {
                    $que->where('campaign.user_id', $userId);
                }
                if ($launch == true) {
                    $que->where('campaign.stage', 5);
                    $que->where('user.status', 1);
                }
            })
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');
        return $query;
    }

    /**
     * Function To get Campaign listing raising fund for charity
     */
    public function getCharityFundraisers($userId = 0)
    {
        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'userProfile.web as userWeb', 'campaignDesign.location', 'campaignDesign.city', 'campaignDesign.campaign_image', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'))
            ->where(function ($que) use ($userId) {
                if ($userId != 0) {
                    $que->where('campaign.charity_id', $userId);
                    $que->where('campaign.user_id', '!=', $userId);
                }
            })
            ->where('campaign.stage', 5)
            ->where('user.status', 1)
            ->orderBy('totalAmountRaised', 'desc')
            ->groupBy('campaign.id')->limit(1)->get();
        return $query;
    }

    /**
     *
     * @param type $campaignName
     * @param type $slug
     * @param type $id
     * @return string
     */
    public function getUniqueSlug($campaignName, $slug = "", $id = 0)
    {
        $uniqueSlug = empty($slug) ? Str::slug($campaignName, '_') : $slug;

        $checkUnique = CampaignModel::where('slug', $uniqueSlug)->where('id', '!=', $id)->first();
        if ($checkUnique) {
            // Get Previous Unique no added
            $uniqueSlug = $uniqueSlug . "-" . mt_rand(1, 9999);
            $this->getUniqueSlug($campaignName, $uniqueSlug, $id);
        }
        return $uniqueSlug;
    }

    /**
     * Function for find by slug
     * @param type $slug
     * @return type
     */
    public function findIdBySlug($slug)
    {
        return $this->getModel()->where('slug', $slug)->first();
    }

    public function getCampaignList($userId = 0, $filters = array(), $sort = array(),$pagination = 0)
    {
        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as design', 'campaign.id', '=', 'design.campaign_id')
            ->join('campaign_categories as categories', 'categories.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_donations as donation', 'campaign.id', '=', 'donation.campaign_id')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'design.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'design.city')
            ->where(function ($que) use ($userId, $filters) {
                if (!empty($userId)) {
                    $que->where('campaign.user_id', $userId);
                }
                if (!empty($filters['campaign_category'])) {
                    $que->where('categories.slug', $filters['campaign_category']);
                }
                if (!empty($filters['campaign_type'])) {
                    $que->where('campaign.campaign_type', $filters['campaign_type']);
                }
                if (isset($filters['search_keyword']) && !empty($filters['search_keyword'])) {
                    $que->Where(function ($que) use ($filters) {
                        $que->orWhere('campaign.campaign_name', 'like', '%' . trim($filters['search_keyword']) . '%');
                        $que->orWhere('campaign.slug', 'like', '%' . trim($filters['search_keyword']) . '%');
                        $que->orWhere('city.name', 'like', '%' . trim($filters['search_keyword']) . '%');
                        $que->orWhere('location.name', 'like', '%' . trim($filters['search_keyword']) . '%');
                    });
                }
            })
            ->select('campaign.*', 'design.location', 'design.campaign_description as desc', 'design.city', 'design.campaign_image', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(donation.donation_amount_monetory) as totalAmountRaised'))
            ->where('campaign.stage', 5)
            ->where('campaign.status', 1)
            ->where('user.status', 1)
            // ->havingRaw('sum(donation.donation_amount_monetory) > 500')
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');

        return $query;
    }

    /**
     * This Function is used to get recieved comments based on campaignId Or Campaigns Linked to Specific Charity Id.
     * @param type $campaignId
     * @param type $charityId
     */
    public function getCommentsList($userId, $campaignId = 0, $charityId = 0)
    {
        $query = DB::table('comments as comment')
            ->join('users as user', 'user.id', '=', 'comment.user_id')
            ->join('campaigns as campaign', 'campaign.id', '=', 'comment.campaign_id')
            ->select('comment.id as commentId', 'user.id as userId', 'campaign.id as campaignId', 'campaign.campaign_name', 'user.name', 'comment.content', 'comment.created_at')
            ->where(function ($que) use ($campaignId, $charityId) {
                if ($campaignId != 0) {
                    $que->where('comment.campaign_id', $campaignId);
                }
                if ($charityId != 0) {
                    $que->where('campaign.charity_id', $charityId);
                }
            })
            ->where('campaign.user_id', $userId)
            ->where('user.status', 1)
            ->orderBy('comment.created_at', 'desc');

        return $query;
    }

    /**
     * This Function is used to find campaigns by area.
     */
    public function findCampaignsByArea($lattitude, $longitude, $minDistance = 0)
    {
        $query = DB::table('campaigns as campaign')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'userProfile.web as userWeb', 'campaignDesign.location', 'campaignDesign.city', 'campaignDesign.campaign_image', 'location.name as locationName', 'city.name as cityName'
                , DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised')
            )
            ->where('campaign.stage', 5)
            ->where('campaign.status', 1)
            ->where('user.status', 1)
            ->where(DB::raw('111.1111 *DEGREES(ACOS(COS(RADIANS(' . $lattitude . '))
                                        * COS(RADIANS(campaignDesign.lattitude))
                                        * COS(RADIANS(' . $longitude . ' - campaignDesign.longitude))
                                        + SIN(RADIANS(' . $lattitude . '))
                                        * SIN(RADIANS(campaignDesign.lattitude))))'
            ), '<=', $minDistance)
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');
        return $query;
    }

    /**
     * Function for finding maximum amount raised campaigns of charity and for charity
     * @param type $id
     * @param type $charity_id
     * @return type
     */
    public function findByCharity($charity_id = 0, $id = 0)
    {

        // $query = DB::table('campaigns')
        //         ->join('users as user', 'user.id', '=', 'campaigns.user_id')
        //         ->leftjoin('campaign_design', 'campaign_design.campaign_id', '=', 'campaigns.id')
        //         ->join('campaign_donations', 'campaign_donations.campaign_id', '=', 'campaigns.id')
        //         ->join('user_profile', 'user_profile.user_id', '=', 'campaigns.user_id')
        //         ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaign_design.location')
        //         ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaign_design.city')
        //         ->where(function($que) use ( $id, $charity_id ) {
        //             $que->where('campaigns.charity_id', $charity_id);
        //             $que->where('campaigns.status', 1);
        //         })
        //         ->where('user.status', 1)
        //         ->groupBy('campaigns.id')
        //         ->select(
        //         'campaigns.id', 'campaigns.user_id', 'campaigns.campaign_name', 'campaign_design.city', 'campaign_design.location', 'campaign_design.city', 'campaign_design.campaign_image', 'campaigns.goal_amount_monetary', 'location.name as locationName', 'city.name as cityName', 'user_profile.first_name', 'user_profile.sur_name', 'user_profile.charity_name', 'campaigns.slug', DB::raw('sum(campaign_donations.donation_amount_monetory) as totalAmountRaised')
        // );
        $query = DB::table('campaigns')
            ->join('users as user', 'user.id', '=', 'campaigns.user_id')
            ->leftjoin('campaign_design', 'campaign_design.campaign_id', '=', 'campaigns.id')
            //->join('campaign_donations', 'campaign_donations.campaign_id', '=', 'campaigns.id')
            ->join('user_profile', 'user_profile.user_id', '=', 'campaigns.user_id')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaign_design.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaign_design.city')
            ->where('charity_id', '=', $charity_id)
            ->where('campaigns.status', 1)
            ->select(
                'campaigns.id', 'campaigns.user_id', 'campaigns.campaign_name', 'campaign_design.city', 'campaign_design.location', 'campaign_design.city', 'campaign_design.campaign_image', 'campaigns.goal_amount_monetary', 'location.name as locationName', 'city.name as cityName', 'user_profile.first_name', 'user_profile.sur_name', 'user_profile.charity_name', 'campaigns.slug')
            ->get();

        return $query;
        //return $query->limit(2)->offset(0)->get();
    }

    public function findByCharityLimit($charity_id = 0, $id = 0){
        $query = DB::table('campaigns')
            ->join('users as user', 'user.id', '=', 'campaigns.user_id')
            ->leftjoin('campaign_design', 'campaign_design.campaign_id', '=', 'campaigns.id')
            //->join('campaign_donations', 'campaign_donations.campaign_id', '=', 'campaigns.id')
            ->join('user_profile', 'user_profile.user_id', '=', 'campaigns.user_id')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaign_design.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaign_design.city')
            ->where('charity_id', '=', $charity_id)
            ->where('campaigns.status', 1)
            ->select(
                'campaigns.id', 'campaigns.user_id', 'campaigns.campaign_name', 'campaign_design.city', 'campaign_design.location', 'campaign_design.city', 'campaign_design.campaign_image', 'campaigns.goal_amount_monetary', 'location.name as locationName', 'city.name as cityName', 'user_profile.first_name', 'user_profile.sur_name', 'user_profile.charity_name', 'campaigns.slug')
            ->limit(10)
            ->get();
        return $query;
    }

    public function findByDonar($id = 0, $paginate)
    {
        $query = DB::table('campaigns')
            ->join('users as user', 'user.id', '=', 'campaigns.user_id')
            ->leftjoin('campaign_design', 'campaign_design.campaign_id', '=', 'campaigns.id')
            ->join('campaign_donations', function ($join) use ($id) {
                $join->on('campaigns.id', '=', 'campaign_donations.campaign_id')
                    ->where('campaign_donations.user_id', '=', $id);
            })
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaign_design.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaign_design.city')
            ->orderBy('campaigns.created_at', 'desc')
            ->where('campaigns.status', 1)
            ->where('user.status', 1)
            ->groupBy('campaigns.id')
            ->select(
                'campaigns.id', 'campaigns.campaign_name', 'campaign_design.city', 'campaign_design.location', 'campaign_design.city', 'campaign_design.campaign_image', 'campaigns.goal_amount_monetary', 'location.name as locationName', 'city.name as cityName', 'campaigns.slug'
            );

        return $query->paginate($paginate);
        //return $query->get();
    }

    public function getFbCampaigns($fbIds = [])
    {
        $query = DB::table('campaigns as campaign')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'userProfile.web as userWeb', 'campaignDesign.location', 'campaignDesign.city', 'campaignDesign.campaign_image', 'location.name as locationName', 'city.name as cityName'
                , DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised')
            )
            ->where('campaign.stage', 5)
            ->whereIn('user.sign_up_id', $fbIds)
            ->where('campaign.status', 1)
            ->where('user.status', 1)
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');
        return $query;
    }

    /**
     * Function to get All Campaigns
     * @params none
     * @return type
     * @author himanshi
     */
    public function findAll()
    {
        return CampaignModel::all();
    }

    /**
     * Function to get All Campaign with categories
     * @param none
     * @return array
     * @author himanshi
     */
    public function getAllCampaigns()
    {
        return $this->getModel()
            ->leftjoin('campaign_categories', function ($join) {
                $join->on(
                    'campaigns.campaign_category', '=', 'campaign_categories.id'
                );
            })
            ->select('campaigns.campaign_name', 'campaigns.goal_amount_monetary', 'campaigns.status', 'campaigns.id', 'campaign_categories.name')
            ->paginate(config('config.paginationperpage')
            );
    }
    
    public function getCampaignsByName($search)
    {
        return $this->getModel()
            ->leftjoin('campaign_categories', function ($join) {
                $join->on(
                    'campaigns.campaign_category', '=', 'campaign_categories.id'
                );
            })
            ->where('campaigns.campaign_name', 'like', '%' . $search . '%')
            ->select('campaigns.campaign_name', 'campaigns.goal_amount_monetary', 'campaigns.status', 'campaigns.id', 'campaign_categories.name')
            ->paginate(config('config.paginationperpage')
            );

    }

    /**
     * Function to get All Campaign with categories By user
     * @param int user_id
     * @return array
     * @author himanshi
     */
    public function getCampaignsByUser($user_id)
    {
        return $this->getModel()
            ->leftjoin('campaign_categories', function ($join) {
                $join->on(
                    'campaigns.campaign_category', '=', 'campaign_categories.id'
                );
            })
            ->select('campaigns.*', 'campaign_categories.name')
            ->where('user_id', '=', $user_id)
            ->paginate(config('config.paginationperpage')
            );
    }

    /**
     * Function to update campaign activation status
     * @param type $id
     * @param type $status
     * @return none
     * @author himanshi
     */
    public function campaignActivation($id, $status)
    {
        return $this->getModel()
            ->where('id', $id)
            ->update(['status' => $status]);
    }

    public function getFundRaiserForCharity()
    {

        $query = DB::table('campaigns as campaign')
            ->join('role_user as userRole', 'userRole.user_id', '=', 'campaign.user_id')
            ->leftjoin('campaign_design as campaignDesign', 'campaignDesign.campaign_id', '=', 'campaign.id')
            ->join('users as user', 'user.id', '=', 'campaign.user_id')
            ->join('user_profile as userProfile', 'userProfile.user_id', '=', 'campaign.user_id')
            ->join('campaign_categories as campaignCategory', 'campaignCategory.id', '=', 'campaign.campaign_category')
            ->leftjoin('campaign_locations as location', 'location.id', '=', 'campaignDesign.location')
            ->leftjoin('campaign_locations as city', 'city.id', '=', 'campaignDesign.city')
            ->leftjoin('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->select('campaign.*', 'userProfile.web as userWeb', 'campaignDesign.location', 'campaignDesign.lattitude', 'campaignDesign.longitude', 'campaignDesign.city', 'campaignDesign.campaign_image', 'location.name as locationName', 'city.name as cityName', DB::raw('sum(campaignDonation.donation_amount_monetory) as totalAmountRaised'))
            ->where(function ($que) {
                $que->where('user.status', 1);
                $que->where('campaign.status', 1);
                $que->where('campaign.stage', 5);
            })
            ->where(function ($que) {
                $que->where('userRole.id', 3);
                $que->orWhere('campaign.charity_id', '!=', '');
            })
            ->orderBy('campaign.created_at', 'desc')
            ->groupBy('campaign.id');

        return $query;
    }

    public function getcharity_Details($charity_id)
    {

        $query = DB::table('campaigns as campaign')
            ->join('campaign_donations as campaignDonation', 'campaignDonation.campaign_id', '=', 'campaign.id')
            ->join('comments', 'comments.campaign_id', '=', 'campaign.id')
            ->join('user_profile', 'user_profile.user_id', '=', 'comments.user_id')
            ->join('users', 'users.id', '=', 'user_profile.user_id')
            ->where(function ($que) use ($charity_id) {
                $que->where('campaign.charity_id', $charity_id);
                $que->where('campaign.status', 1);
                $que->where('users.status', 1);
                $que->orWhere('campaign.charity_id', $charity_id);
            })
            ->orderBy('comments.updated_at', 'desc')
            ->limit(2)
            ->groupBy('comments.id')
            ->select(
                'comments.is_anonymous', 'comments.content', 'campaignDonation.donation_amount_monetory', 'user_profile.first_name', 'user_profile.sur_name', 'user_profile.profile_pic', 'user_profile.slug'
            )
            ->get();

        return $query;
    }

    public function getComments($id)
    {

        $query = DB::table('comments')
            ->join('campaigns', 'campaigns.id', '=', 'comments.campaign_id')
            ->join('campaign_donations', 'campaign_donations.campaign_id', '=', 'campaigns.id')
            ->join('user_profile', 'user_profile.user_id', '=', 'comments.user_id')
            ->join('users', 'users.id', '=', 'user_profile.user_id')
            ->where(function ($que) use ($id) {
                $que->where('campaigns.user_id', $id);
                $que->where('campaigns.status', 1);
                $que->where('users.status', 1);
                $que->where('comments.campaign_id', $id);
                $que->orWhere('campaigns.charity_id', $id);
            })
            ->limit(1)
            ->offset(0)
            ->orderBy('comments.updated_at', 'desc')
            ->groupBy('comments.id')
            ->select(
                'comments.is_anonymous', 'comments.content', 'campaign_donations.donation_amount_monetory', 'user_profile.first_name', 'user_profile.sur_name', 'user_profile.profile_pic', 'user_profile.slug'
            )
            ->get();


        return $query;
    }

    public function delCampaign($campaignId)
    {
        $query = DB::table('campaign_donations as donation')
            ->where('donation.campaign_id', $campaignId)
            ->where('donation.status', 1)
            ->count();
        if ($query > 0) {
            return FALSE;
        } else {
            $data = $this->getModel()->find($campaignId);
            if ($data) {
                $res = $data->delete();
                return $res;
            } else {
                return FALSE;
            }
        }
    }

    public function Donation30days()
    {
        $query = DB::table('campaign_donations')
            ->where('created_at', '>', Carbon::now()->subDays(30))
            ->select('donation_amount_monetory', 'created_at')
            ->get();
        return $query;
    }

    public function ToalAmountRaised($userId)
    {
        $query = DB::table('campaign_donations')
            ->where('campaign_donations.user_id', '=', $userId)
            ->where('created_at', '>', Carbon::now()->subDays(30))
            ->select(DB::raw('sum(donation_amount_monetory) as totalAmountRaised'))
            ->get();
        return $query[0];
    }

}
