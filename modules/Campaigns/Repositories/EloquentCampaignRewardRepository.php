<?php

namespace Modules\Campaigns\Repositories;
use Modules\Campaigns\Entities\CampaignReward as CampaignRewardModel;
use Modules\Campaigns\Entities\CampaignRewardShippingLocation as CampaignRewardShippingModel;

class EloquentCampaignRewardRepository
{
    public function getModel()
    {
        return new CampaignRewardModel();
    }
    
    public function findByCampaignId($campaignId){
        return $this->getModel()->where('campaign_id', $campaignId)->get();
    }
    
    public function findById($rewardId)
    {
        return $this->getModel()->where('id', $rewardId)->first();
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
    
    public function delete($rewardId)
    {
        // Getting Campaign Reward Model Instance
        $campaignReward= $this->findById($rewardId);
        // delete reward for campaign Reward
        $campaignReward->delete($rewardId);
        return $campaignReward->campaign_id;
    }
    
    public function update(array $data, $id)
    {
         // Getting Campaign Reward Model Instance
        $campaignReward= $this->findById($id);
        // Update stage for campaign Reward
        $campaignReward->update($data);
        return true;
    }
    
    public function saveLocations(array $data,$id)
    {
        dd('hi');
        foreach($data['shipping_location'] as $key=>$location){
            dd($data['shipping_cost'][$key]);die;
        }
        
    }
}