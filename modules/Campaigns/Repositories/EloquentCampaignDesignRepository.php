<?php

namespace Modules\Campaigns\Repositories;

use Modules\Campaigns\Entities\CampaignDesign as CampaignDesignModel;

class EloquentCampaignDesignRepository {

    public function getModel() {
        return new CampaignDesignModel();
    }

    public function findByCampaignId($campaignId) {
        return $this->getModel()->where('campaign_id', $campaignId)->first();
    }

    public function findById($designId) {
        return $this->getModel()->where('id', $designId)->first();
    }

    public function create(array $data) {
        return $this->getModel()->create($data);
    }

    public function update(array $data, $id) {
        // Getting Campaign Design Model Instance
        $campaignDesign = $this->findById($id);
        // Update stage for campaign design 
        $campaignDesign->update($data);
        return true;
    }

}
