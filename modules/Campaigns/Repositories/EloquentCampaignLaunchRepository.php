<?php

namespace Modules\Campaigns\Repositories;
use Modules\Campaigns\Entities\CampaignPayment as CampaignPaymentModel;
use DB;

class EloquentCampaignLaunchRepository
{
    public function getModel()
    {
        return new CampaignPaymentModel();
    }
    
    public function findByCampaignId($campaignId){
        return $this->getModel()->where('campaign_id', $campaignId)->first();
    }
    
    public function findById($optionId)
    {
        return $this->getModel()->where('id', $optionId)->first();
    }

    public function create(array $data)
    {
        $save=$this->getModel()->create($data);
        return $this->getCampaignPaymentDetail($save->id);
    }
    
    public function getCampaignPaymentDetail($campaignPaymentId)
    {
        return DB::table('campaign_payments as campaignPayment')
                ->join('payment_options as paymentOption','paymentOption.id','=','campaignPayment.payment_option_id')
                ->select('campaignPayment.id','campaignPayment.status','campaignPayment.payment_option_id','paymentOption.name')
                ->where('campaignPayment.id',$campaignPaymentId)
                ->first();
    }
    
    public function update(array $data, $id)
    {
         // Getting Campaign Design Model Instance
        $campaignPayment= $this->findById($id);
        // Update stage for campaign design 
        $campaignPayment->update($data);
        return $this->getCampaignPaymentDetail($id);
    }
    
    public function getCampaignPaymentOptions($campaignId)
    {
        return DB::table('campaign_payments')
                ->where('campaign_id',$campaignId)
                ->where('status','yes')
                ->get();
    }
    
    public function findActivePaymentOptions($userId,$campaignId)
    {
        return DB::table('campaign_payments')
                ->where('campaign_id',$campaignId)
                ->where('user_id',$userId)
                ->where('status','yes')
                ->get();
    }
}