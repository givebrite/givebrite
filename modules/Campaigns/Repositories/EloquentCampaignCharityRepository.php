<?php

namespace Modules\Campaigns\Repositories;
use Modules\Campaigns\Entities\CampaignCharity as CampaignCharityModel;

class EloquentCampaignCharityRepository
{
    public function getModel()
    {
        return new CampaignCharityModel();
    }
    
    public function findByCampaignId($campaignId){
        return $this->getModel()->where('campaign_id', $campaignId)->first();
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
    
    public function update(array $data, $id)
    {
         // Getting Campaign Charity Model Instance
        $campaignCharity= $this->findById($id);
        // Update stage for campaign charity 
        $campaignCharity->update($data);
        return true;
    }
    
    
}
