<?php

namespace Modules\Campaigns\Repositories;
use Modules\Campaigns\Entities\CampaignStravaActivity as CampaignStravaActModel;

class EloquentCampaignStravaActivityRepository
{ 
    public function getModel()
    {
        return new CampaignStravaActModel();
    }
    
    

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
    
    public function findByCampaignId($cId)
    { 
      
        return CampaignStravaActModel::where('campaign_id', '=', $cId)->get();
    }
    
    public function delete($cId)
    {
        // Getting Campaign Reward Model Instance
        $stravaActivities = $this->findByCampaignId($cId);
        foreach($stravaActivities as $stravaActivity)
        {
            $stravaActivity->delete();
        }
        return true;
    }
     public function SumofCampaignId($cId)
    {
        return CampaignStravaActModel::where('campaign_id', '=', $cId)->sum('distance');
                
    }
    
    
}
