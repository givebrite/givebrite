<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class Campaign extends Model
{

    protected $table = 'campaigns';
    protected $appends = ['distance'];

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'campaign_name',
        'goal_amount_type',
        'goal_amount_monetary',
        'goal_amount_time',
        'campaign_type',
        'campaign_category',
        'campaign_duration',
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
        'registered_charity',
        'pay_type',
        'charity_id',
        'campaign_rewards',
        'stage',
        'slug',
        'strava_access_token',
    ];

    public function donationTypes()
    {
        return $this->belongsToMany('App\DonationType', 'campaign_donor', 'campaign_id', 'donor_type_id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeNewest($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * @param $query
     * @param $id
     *
     * @return mixed
     */
    public function scopeBySlugOrId($query, $id)
    {
        return $query->whereId($id)->orWhere('slug', '=', $id);
    }

    public static function getGoalAmountMonetary($id)
    {
        $model = self::whereId($id)->get();

        return $model;
    }

}
