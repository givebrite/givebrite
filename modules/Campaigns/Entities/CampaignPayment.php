<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class CampaignPayment extends Model
{

    protected $table = 'campaign_payments';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'campaign_id',
        'payment_option_id',
        'status',
        'created_at',
        'updated_at',
        'percent_application_fees',
        'is_default',
        'connect_access_token',
        'connect_livemode',
        'connect_refresh_token',
        'connect_token_type',
        'connect_stripe_publishable_key',
        'connect_stripe_user_id',
        'connect_scope',
        'user_email',
        'user_name',
        'user_country'
    ];

}
