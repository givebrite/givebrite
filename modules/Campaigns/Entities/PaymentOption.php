<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class PaymentOption extends Model
{
    protected $table = 'payment_options';
    
   /**
     * @var array
     */
    protected $fillable = [
        'name',
        'label',
        'value',
        'created_at',
        'updated_at',
    ];
    
    
}
