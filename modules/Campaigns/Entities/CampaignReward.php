<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class CampaignReward extends Model
{
    protected $table = 'campaign_rewards';
    
   /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'reward_name',
        'pledge_amount',
        'pledge_description',
        'shipping_required',
        'location_cost',
        'created_at',
        'updated_at',
    ];
    
    
}
