<?php namespace Modules\Campaigns\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CampaignStravaActivity extends Model {

    protected $table = 'campaign_strava_activities';
    
   /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'activity_id',
        'activity_name',
        'distance',
        'sports',
        'start_date',
        'moving_time',
        'average_speed',
        'start_lat',
        'start_long',
        'end_lat',
        'end_long',
        'summary_polyline',
        'gmap_img',
        'graph_data',
        'created_at',
        'updated_at',
    ];

}