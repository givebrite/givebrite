<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class Team extends Model
{

    protected $primaryKey = 'team_id';
    /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'team_name',
        'team_url',
        'cover_photo',
        'team_logo',
        'team_description',
        'created_by_user',
        'requires_invitation',
        
    ];

    

}
