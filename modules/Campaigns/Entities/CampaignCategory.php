<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class CampaignCategory extends Model
{
    protected $table = 'campaign_categories';
    
   /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'created_at',
        'updated_at',
    ];
    
    
}
