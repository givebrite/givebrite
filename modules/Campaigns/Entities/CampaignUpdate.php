<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class CampaignUpdate extends Model
{
    protected $table = 'campaign_updates';
    
   /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'user_id',
        'updates',
        'created_at',
        'updated_at',
    ];
    
    
}
