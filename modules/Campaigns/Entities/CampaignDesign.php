<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class CampaignDesign extends Model
{
    protected $table = 'campaign_design';
    
   /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'theme_id',
        'campaign_image',
        'media_type',
        'facebook_url',
        'media_url',
        'campaign_description',
        'location',
        'city',
        'lattitude',
        'longitude',
        'created_at',
        'updated_at',
        'postcode'
    ];
    
    
}
