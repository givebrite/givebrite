<?php

namespace Modules\Campaigns\Entities;

use Pingpong\Presenters\Model;

class CampaignLocation extends Model
{
    protected $table = 'campaign_locations';
    
   /**
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id',
        'created_at',
        'updated_at',
    ];
    
    
}
