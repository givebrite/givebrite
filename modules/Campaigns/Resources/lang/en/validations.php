<?php

return [
    'campaign-detail'=>[
        'campaign_name-required'=>'Campaign name is required',
        'campaign_name-min'=>'Campaign name should be at least 2 characters long',
        'campaign_name-max'=>'Campaign name must not be greater than 35 characters',
        'goal_amount_monetary-required' =>'Goal amount is required.',
        'goal_amount_monetary-min' =>'Goal amount must be at least 1',
        'goal_amount_monetary-max' =>'Goal amount must not greater than 99999999',
        'goal_amount_monetary-numeric'=>'Goal amount must be numeric',
        'goal_amount_monetary-regex'=>'Goal amount format is not valid',
        'end_date-required'=>'End date is required',
        'end_date-after'=>'End date must be greater than current date',
     ],
    'campaign-design'=>[
        'theme_id-required'=>'Theme is required',
        'facebook_url-required'=>'Photo or vedio is required',
        'media_url-required'=>'Photo or vedio is required',
        'facebook_url-url'=>'Url format is not valid',
        'media_url-url'=>'Url format is not valid',
        'campaign_image-required'=>'Campaign image is required',
        'campaign_image-mimes'=>'Only jpeg,bmp,png,gif formats are allowed',
        'campaign_image-min'=>'Campaign image size should be greater than 200kb',
        'campaign_image-max'=>'Campaign image size should be less than 4mb',
        'campaign_description-required'=>'Description is required',
        'campaign_description-min'=>'Description should be at least 50 word',
        'location-required'=>'Country is required',
        'city-required'=>'Town is required',
        'postcode-required'=>'Postcode is required',
        'postcode-regex'=>'Postcode is not valid',
    ],
    'campaign-rewards'=>[
        
    ],
    'campaign-payments'=>[
        
    ]
    
    
];