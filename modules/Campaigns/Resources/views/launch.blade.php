@extends('site.layouts.master')

@section('title', 'Campaign')

@section('style')
    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css').'?num='.mt_rand(1000000, 9999999) }}">
    
    <style>
        .stripe-before-connent {
            background: #00a4ee none repeat scroll 0 0;
            padding: 15px 25px;
            color: #fff;
            font-size: 19px;
        }
        .stripe-before-connent:hover {
            color: #fff;
        }

        .stripe-disable-btn{

                background: #37c2df;
                padding: 15px 25px;
                font-size: 19px;
                color: #fff;
                margin-left: 17px;
        }
        .stripe-disable-btn:hover{
            color: #fff;
        }

    </style>

@stop

@section('content')

    <div class="container launchPage">
        @include("campaigns::partials.progressbar")
        <form method="get" action="{{ URL::route('campaign.launch', $campaignId) }}" name="launchForm" id="launchForm">
            <div class="custom-page-wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="laungSlogan">
                            <h4>You are ready to go!</h4>
                            <!--
                            <p>The more payment options you enable the more your chances of donations increases</p>
                            -->
                        </div>
                        <div class="campaign-desc">
                            <h3>Donation types will help you keep track of your donations</h3>
                            <ul>
                                <li><i class="fa fa-check"></i> Creating a donation type allows the donor to choose the
                                    area of work they would like to support in a campaign.
                                </li>
                                <li><i class="fa fa-check"></i> Save admin costs - easily filter your donation types
                                </li>
                                <li><i class="fa fa-check"></i> Download CSV files to track donation types in a campaign
                                </li>
                                <li><i class="fa fa-check"></i> One campaign serves multiple areas of work</li>
                                <li><i class="fa fa-check"></i>Gives donors a choice of the area of work they want to
                                    support
                                </li>
                                <li><i class="fa fa-check"></i>Examples of donation types are: admin, general donation,
                                    medical aid, Zakah
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row margin-top-10">

                    <div class="col-sm-12 col-xs-12 col-lg-8">
                        <div class="switch col-sm-2 col-xs-2 col-md-2 col-lg-1">

                            @if(empty($donation_type))
                                <input id="cmn-toggle-1" value="1" class="cmn-toggle cmn-toggle-round" type="checkbox"
                                       name="donation_types_enabled">
                                <label id="switchselect1" for="cmn-toggle-1" class="switchselect lightbg"></label>
                            @else
                                <input id="cmn-toggle-1" value="1" class="cmn-toggle cmn-toggle-round" type="checkbox"
                                       name="donation_types_enabled" checked="checked">
                                <label id="switchselect1" for="cmn-toggle-1" class="switchselect darkbg"></label>
                            @endif
                        </div>
                        <div class="col-sm-10 col-xs-10 col-md-10 col-lg-11">
                            <div class="secheading">Turn On Donation Types</div>
                            <p>In case you need to differentiate the different types of donations that you will be
                                receiving</p>
                        </div>

                    </div>

                </div>
                @if(empty($donation_type))
                <div id="donation-type" style="display:none">
                @else
                  <div id="donation-type">
                @endif
                    <div class="row margin-top-10">
                        <div class="col-sm-12 col-xs-12 col-lg-6">

                            <div class="inputBlock donation-type">
                                {!! Form::text('campaign_name', null, ['class'=>'input-large','id'=>'donation_text','placeholder' => 'Donation types']) !!}
                                {!! $errors->first('campaign_name', '<div class="text-danger">:message</div>') !!}
                                {!! Form::hidden('id', $campaignId, ['id' =>'cmp_id']) !!}
                            </div>
                            <a href="" id="add_type" class="plus-sign"><i class="fa fa-plus"></i></a>


                        </div>

                    </div>

                    <div class="row margin-top-10">
                        <div class="col-sm-5 donation-table">
                            @if(empty($donation_type))
                                <table class="table table-striped" id="donate_tbl" style="display: none;">
                                    @else
                                        <table class="table table-striped" id="donate_tbl">
                                            @endif


                                            <thead>
                                            <tr>
                                                <th>Donation types</th>
                                                <th align="center">Default</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($donation_type))
                                                @foreach($donation_type as $d)

                                                    <tr id="donor{{$d->id}}">
                                                        <input type="hidden" name="donation_type[]"
                                                               value="{{$d->title}}">
                                                        <td>{{$d->title}}</td>
                                                        <td align="center">
                                                            <div class="control-group">
                                                                <label class="control control--radio">
                                                                <input type="radio" name="dnt_type" value="{{$d->title}}" />
                                                                    <div class="control__indicator"></div>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td><a id="btn{{$d->title}}"
                                                               onclick="$('#donor{{ $d->id }}').remove(); return false"
                                                               href="" class="deleteDonation"><i
                                                                        class="fa fa-trash"></i></a></td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="disablePayment" value="{{URL::route('campaigns.disable-payment')}}">
                @if(!empty($paymentOptions) && $role_response->role_id != '' && ($campaign->charity_id==0 || !isset($campaign->charity_id)))


                    <div class="margin-top-10 payment-gateway">
                        <h3>Payment Gateways</h3>
                        <p>At least one payment gateway must be enabled before continuing</p>
                        <hr/>

                        <div class="row margin-top-10">

                            <div class="col-sm-12 col-xs-12 col-lg-8">
                                
                                
                                <div class="switch col-sm-2 col-xs-2 col-md-2 col-lg-1">
                                    <!-- configure for other payment gatways -->
                                    @if($paymentOptions[0]->status != 'yes')
                                    <input id="cmn-toggle-2" value="1" class="cmn-toggle cmn-toggle-round"
                                           type="checkbox" name="payment_getway">
                                    <label id="switchselect2" for="cmn-toggle-2" class="switchselect lightbg"></label>
                                    @else
                                    <input id="cmn-toggle-2" value="1" class="cmn-toggle cmn-toggle-round"
                                           type="checkbox" name="payment_getway" checked="checked">
                                    <label id="switchselect2" for="cmn-toggle-2" class="switchselect darkbg"></label>
                                    @endif
                                </div>


                                <div class="col-sm-10 col-xs-10 col-md-10 col-lg-11">
                                    <div class="secheading">
                                        <input type="hidden" id="disablePayment"
                                               value="{{URL::route('campaigns.disable-payment')}}">

                                        @foreach($paymentOptions as $payment)
                                            <?php $class = ($payment->status == "yes") ? "" : "disabled-link"; ?>
                                            <div id='enable-button-{{$payment->id}}'>
                                                <div class="col-sm-4">
                                                    @include('campaigns::partials.enablepayment')
                                                </div>
                                                <div class="col-sm-8">
                                                    {!! HTML::image('site/images/stripe-new.png') !!}
                                                    <p>Stripe will take 1.4% + 20p from the total transaction</p>
                                                </div>
                                                <div class="col-lg-4 col-sm-12"></div>

                                            </div>
                                    </div>

                                    <span class="pay_error" id="pay_error" style="color:red;"></span>
                                </div>


                            </div>

                        </div>
                        {{--<div class="row">--}}
                        {{--<div class="col-sm-2 col-xs-2 col-lg-1">--}}
                        {{--<div class="switch col-sm-2 col-xs-2 col-md-2 col-lg-1">--}}
                        {{--<input id="cmn-toggle-4" value="1" class="cmn-toggle cmn-toggle-round" type="checkbox" name="payment_getway">   --}}
                        {{--<label id="switchselect2" for="cmn-toggle-4" class="switchselect lightbg"></label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-3 col-xs-3 col-lg-2">                --}}
                        {{--<span class="btn btn-connect-dark">Connect</span>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-12 col-xs-12 col-lg-7">      --}}
                        {{--{!! HTML::image('site/images/paypal-new.png') !!}        --}}
                        {{--<p>3.4% + £0.20p</p>--}}
                        {{--</div>--}}

                        {{--</div>--}}
                    </div>




                @endforeach
            @endif

            <!--implement by Amit Sinha stava code-->
                <div class="payment-gateway">
                    <h3>Training Apps</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-sm-2 col-xs-2 col-lg-1">
                            <div class="switch col-sm-2 col-xs-2 col-md-2 col-lg-1">
                                @if($campaign->strava_access_token)
                                    <input id="cmn-toggle-3" value="1" class="cmn-toggle cmn-toggle-round"
                                           type="checkbox"
                                           name="payment_getway" checked>
                                    <label id="switchselect2" for="cmn-toggle-3" class="switchselect darkbg"></label>
                                @else
                                    <input id="cmn-toggle-3" value="1" class="cmn-toggle cmn-toggle-round"
                                           type="checkbox"
                                           name="payment_getway">
                                    <label id="switchselect3" for="cmn-toggle-3" class="switchselect lightbg"></label>
                                @endif

                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3 col-lg-2">
                    <span>
                        @if($campaign->strava_access_token)
                            <a href="/campaigns/launch/disconnectStrava/{{ $campaignId }}" class="btn btn-connect-red">Disconnect</a>
                        @else
                            <a href="/campaigns/launch/connectStrava/{{ $campaignId }}?start=true"
                               class="btn btn-connect-red">Connect</a>
                        @endif

                    </span>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-lg-7">
                            {!! HTML::image('site/images/strava.png') !!}
                            <p>Update your donors with your training</p>
                        </div>

                    </div>
                </div>

                <div class="row">
                    @if(Helper::checkIsActivated() == false)
                        <?php
                        if ($role_response->role_id == 3) {
                            $msgText = "In order to launch a campaign we need to verify your charity. Please get in touch for more infromation.";
                        }
                        ?>
                        <span class="launch-note label label-info">{{!empty($msgText) ? $msgText : "In order to launch a campaign you need to verify your email."}} @if($role_response->role_id != 3)
                                <a href="{{ url('resend') }}">{{ " Click here " }}</a>  {{ "to resend verification email."}} @endif </span>
                    @endif
                    <div class="mob-center col-sm-12">
                        <a id='launch-link' class="mt20 lt-green-lg {{ !empty($class) ? $class : '' }}" href=""
                           onclick="$('#launchForm').submit(); return false;">Launch Campaign</a>
                        <p class="payment-text">At least one payment gateway must be enabled before continuing</p>
                    </div>
                </div>
            </div> <!--Custom page wrapper -->
            <input type="hidden" name="default_dnt_type" id="default_dnt_type" value="">
        </form>
    </div>

@stop

@section('script')
    <script type="text/javascript">
       
       jQuery('body').on('change', '[name="dnt_type"]', function(){

            jQuery('#default_dnt_type').val(jQuery(this).val());
            console.log( jQuery(this).val() );

       })

    </script>


    <script src="{!! asset('modules/campaigns/js/campaign-launch.js') !!}"></script>

@stop
