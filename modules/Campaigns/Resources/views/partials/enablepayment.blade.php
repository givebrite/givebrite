<?php $activeClass = !empty($payment->status) && $payment->status == 'yes' ? 'activeBtn' : '';?>
@if(!empty($payment->status))

    <a class="stripe-connect btn stripe-disable-btn {{$activeClass}}"  onClick="managePaymentSetup({{!empty($payment->campaignPaymentId) ? $payment->campaignPaymentId : 0 }},{{!empty($payment->id) ? $payment->id : 0}},'{{!empty($payment->status) ? $payment->status : ''}}')">{{!empty($payment->status) && $payment->status=='yes' ? 'Disable' : 'Enable' }} </a>

{!! Form::open(['route' => ['campaigns.disable-payment'],'id'=>'disable-payment-form-'.$payment->campaignPaymentId]) !!}
<input type="hidden" name="campaignPaymentId" value="{{!empty($payment->campaignPaymentId) ? $payment->campaignPaymentId : 0 }}">
<input type="hidden" name="optionId" value="{{!empty($payment->id) ? $payment->id : 0}}">
<input type="hidden" name="status" value="{{!empty($payment->status) && $payment->status=='yes' ? 'no' : 'yes'}}">
{!! Form::close() !!}
@else
<div class="col-sm-6 text-center">
    <a class="btn stripe-before-connent {{$activeClass}}" href="{{ $stripe_connect_link }}">Connect</a>
</div>
@endif