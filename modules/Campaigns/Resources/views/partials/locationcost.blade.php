@if(!empty($campaignReward->location_cost))
<?php $locationSelectArray = json_decode($campaignReward->location_cost); ?>
@foreach($locationSelectArray as $locationIndex=>$cost)
<?php $locationIndexArray=explode(',',$locationIndex);?>
<div class="row">
<div class="col-md-6 col-sm-12">
    <div class="innerLable">Shipping location(s) <span>*</span></div>
    <div class="inputBlock">
        <div class="reward-select">
            <select multiple name='shipping_location[]' class='location-select selectpicker' onClick="manageSelect()">
                @if(!empty($shippingLocations))
                @foreach($shippingLocations as $key=>$location)
                <option value='{{$key}}' {{ in_array($key,$locationIndexArray) ? 'selected' : '' }} >{{$location}}</option>
                @endforeach
                @endif
            </select>   
            {!! $errors->first('shipping_location', '<div class="text-danger">:message</div>') !!}
            <div class='text-danger' id='err_shipping_location'></div>
        </div> 
    </div>
</div>
<div class="col-md-6 col-sm-12 shippingCostBlock">
    <div class="innerLable">Shipping Cost <span>*</span></div>
    <div class="inputBlock">
        <p class="gbpBtn">
            {!! Form::text('shipping_cost[]', !empty($cost) ? $cost : '', ['placeholder' => '&euro;','class'=>'reward-input']) !!}<button class="lt-green-lg mobile-btn" disabled>GBP</button>
        </p>
        {!! $errors->first('shipping_cost', '<div class="text-danger">:message</div>') !!}   
        <div class='text-danger' id='err_shipping_cost'></div>
    </div>
</div>
</div>
@endforeach
@else
<div class="row">
<div class="col-md-6 col-sm-12">
    <div class="innerLable">Shipping location(s) <span>*</span></div>
    <div class="inputBlock">
        <div class="reward-select">
            <select multiple name='shipping_location[]' class='location-select selectpicker'  onClick="manageSelect()">
                @if(!empty($shippingLocations))
                @foreach($shippingLocations as $key=>$location)
                <option value='{{$key}}'>{{$location}}</option>
                @endforeach
                @endif
            </select>   
            {!! $errors->first('shipping_location', '<div class="text-danger">:message</div>') !!}
            <div class='text-danger' id='err_shipping_location'></div>
        </div> 
    </div>
</div>
<div class="col-md-6 col-sm-12 shippingCostBlock">
    <div class="innerLable">Shipping Cost <span>*</span></div>
    <div class="inputBlock">
        <p class="gbpBtn">
            {!! Form::text('shipping_cost[]', null, ['placeholder' => '&euro;','class'=>'reward-input']) !!}<button class="lt-green-lg mobile-btn">GBP</button>
        </p>
        {!! $errors->first('shipping_cost', '<div class="text-danger">:message</div>') !!}   
        <div class='text-danger' id='err_shipping_cost'></div>
    </div>
</div>
</div>
@endif
