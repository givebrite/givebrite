@if(isset($campaignReward))
{!! Form::model($campaignReward, ['method' => 'PUT', 'route' => ['reward.update', $campaignReward->id],'id'=>'reward-form',"onSubmit"=>"updateReward($campaignReward->id)"]) !!}
@else
{!! Form::open([ 'route' => ['reward.store', !empty($campaignId) ? $campaignId : '' ],'id'=>'reward-form']) !!}
@endif
<div id="add-reward-div" style="display:none">
    <div class="inputBlock"> 
        {!! Form::text('reward_name', null, ['class' => 'formRow fullwidth mb15 giftBtn white-bg border-radius4 text-left  blue-btn-lg','placeholder'=>'Reward Name']) !!}
        {!! $errors->first('reward_name', '<div class="text-danger">:message</div>') !!}
        <div class='text-danger' id='err_reward_name' style="margin-top:1px;"></div>
    </div>
    <div class="innerForm">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="innerLable">Pledge amount  <span>*</span></div>
                <div class="inputBlock">
                    <p class="gbpBtn">
                        {!! Form::text('pledge_amount', null, ['placeholder' => '&euro;']) !!}<button class="lt-green-lg mobile-btn" disabled>GBP</button>
                    </p>
                    {!! $errors->first('pledge_amount', '<div class="text-danger">:message</div>') !!}
                    <div class='text-danger' id='err_pledge_amount'></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11 col-sm-12">
                <div class="innerLable">Pledge description  <span>*</span></div>
                <div class="inputBlock">
                    {!! Form::text('pledge_description', null, ['class'=>'input-large','placeholder' => 'Description']) !!}
                    {!! $errors->first('pledge_description', '<div class="text-danger">:message</div>') !!}
                    <div class='text-danger' id='err_pledge_description'></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11 col-sm-12">
                <div class="innerLable">Requisres shipping? <span>*</span></div>
                <div class="inputBlock">
                    <div class="inputBlock">
                        <div class="customRadio toogleSwitch">
                            <div>
                                {!! Form::radio('shipping_required', 'yes') !!}
                                <label class="yesSec" for="shipping_required">
                                    <div class="toogleText">Yes</div>
                                </label>
                            </div><div>
                                {!! Form::radio('shipping_required', 'no',['checked'=>true]) !!}
                                <label class="noSec" for="shipping_required">
                                    <div class="toogleText">No</div>
                                </label>
                            </div>
                            {!! $errors->first('shipping_required', '<div class="text-danger">:message</div>') !!}
                            <div class='text-danger' id='err_shipping_required'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id='saveLocation' class='col-md-10'>
                <input type='hidden' name='location[]' id='location' value=''>
                @include("campaigns::partials.locationcost")
            </div>
            <div class="col-md-2 col-sm-12">
                <div class="innerLable"></div>
                <div class="inputBlock">
                    <a onClick='addLocation()'>+ Add Locations</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <a onClick='saveReward()' class='blue-btn width120'> Save</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div><!-- Add Reward Div-->
<!-- Div To Be Appended -->
<div id='to-be-append' style='display:none'>
    @include("campaigns::partials.locationcost")
</div>