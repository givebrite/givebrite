@if(!empty($campaignRewards) && count($campaignRewards)>0)
@foreach($campaignRewards as $reward)
<div class="fullwidth mb15 giftBtn border-radius4 text-left blue-btn-lg">
    <i class="fa fa-star" aria-hidden="true"></i>
    <a  onClick="editReward('{{URL::route('reward.edit', $reward->id)}}')">{{ !empty($reward->reward_name) ? $reward->reward_name : ""}}</a>
    <a onClick="deleteReward('{{URL::route('reward.destroy', $reward->id)}}')"><i class="fa fa-times" aria-hidden="true"></i></a>
</div>
@endforeach
@endif