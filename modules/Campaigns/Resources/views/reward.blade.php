@extends('site.layouts.master')

@section('title', 'Campaign')

@section('style')
<link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css').'?num='.mt_rand(1000000, 9999999) }}">
<link rel="stylesheet" href="{{ asset('modules/campaigns/css/bootstrap-select.css').'?num='.mt_rand(1000000, 9999999) }}">
@stop

@section('content')
<div class="container customPage">
    @include('campaigns::partials.progressbar')
    <div class="row">
        <div class="col-sm-12">
            <div class="laungSlogan">
                <h4>Rewards</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="createCampaignForm">
                {!! Form::open([ 'route' => ['update.campaign-reward', $campaignId],'id'=>'confirm-reward-form']) !!}
               <div class="formRow">
                    <div class="secheading">Would you like to reward your donors? <span class="astrikBlock">*</span></div>
                    <div class="inputBlock">
                        <div class="customRadio toogleSwitch">
                            <div>
                                {!! Form::radio('campaign_rewards','yes',['checked'=>!empty($rewardRequired) && $rewardRequired == 'yes' ? 'true' : '' ] ) !!}
                                <label class="yesSec" for="campaign_rewards">
                                    <div class="toogleText">Yes</div>
                                </label>
                            </div><div>
                                {!! Form::radio('campaign_rewards', 'no') !!}
                                <label class="noSec" for="campaign_rewards">
                                    <div class="toogleText">No</div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <div class="formRow">
                    <div id="campaign-reward-list">
                        @include("campaigns::partials.rewardpanel")
                    </div>
                    <div id="campaign-reward-form">
                        @include("campaigns::partials.rewardform")
                    </div>
                </div> 

                <div class="addReward">
                    <button class="green-btn width180" id='add-reward-button' onClick="editReward('{{URL::route('reward.edit', 0)}}',{{!empty($campaignId) ? $campaignId : ''}})">+ Add Reward</button>              
                </div>
    
                <div class="buttonBlock">
                    <a class="blue-btn-lg text-center" href='{{!empty($campaignId) ? URL::route('launch.create', $campaignId) : "" }}'>Next <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@stop

@section('script')
<script src="{!! asset('modules/campaigns/js/campaign-reward.js') !!}"></script>
<script src="{!! asset('modules/campaigns/js/bootstrap-select.js') !!}"></script>
@stop