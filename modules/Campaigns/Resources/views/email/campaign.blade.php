<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
        <title>:: Welcome to Site ::</title>
        <style type="text/css">
            body {
                background: #f6f5f2;
                font-family: Arial, Helvetica, sans-serif;
                color: #b8b8b8;
                font-size: 14px;
            }
            .setW100P {
                width: 650px;
            }
            * {
                margin: 0px;
                padding: 0px;
                box-sizing: border-box;
            }
            .ExternalClass * {
                line-height: 121%;
            }
            table {
                border-collapse: collapse;
            }
            table td {
                border-collapse: collapse;
            }
            img {
                border: none;
                outline: none;
            }
            @media(max-width:750px) {
                .setW100P {
                    width: 493px !important;
                }
            }
            @media(max-width:500px) {
                .setW100P {
                    width: 80% !important;
                }
            }
        </style>
    </head>
    <body>
        <div align="center" style="background: #f6f5f2; font-family: Arial, Helvetica, sans-serif; color: #b8b8b8; font-size: 14px;">
            <table style="border:none; font-size: 14px; width:650px;" align="center">
                <tr>
                    <td align="center" valign="top" style="padding-top:30px; padding-bottom:60px;">
                        <img src="{{$message->embed(base_path().'/public/site/images/email-logo.png')}}" /></td>
                </tr>
                <tr>
                    <td align="center" valign="top" bgcolor="#ffffff" style="border-radius:3px; ">
                        <table align="left" class="table-left-padd" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td style="border-bottom:1px solid #f2f2f2; padding:29px 0px 10px 40px;"><!--1px solid #f2f2f2--> 
                                    <img src="{{$message->embed(base_path().'/public/site/images/awesome-work.png')}}" /></td>
                            </tr>
                            <tr>
                                <td style="padding:15px 20px 20px 40px;"> Christina, your campaign is live! You are ready to get the word out. <br />
                                    <br />
                                    Here&acute;s your campaign link <br />
                                    <br />
                                    <strong style="color:#00be43;">We’re here to help you kick things off</strong> <br />
                                    <br />
                                    The first step to a successful campaign is to reach out to everyone you know. Your close family and friends will be your first backers. It’s important to ask them to share your campaign with their networks as well - this will help build momentum. </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:35px;" align="center"> Unsubscribe </td>
                </tr>
                <tr>
                    <td style="padding-top:25px;" align="center"> GiveBrite HQ<br />
                        Abbey House, 10 Abbey Hills Road, <br />
                        Greater Manchester, OL8 2BS </td>
                </tr>
            </table>
        </div>
    </body>
</html>
