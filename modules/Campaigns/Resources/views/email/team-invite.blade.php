<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>:: Welcome to Site ::</title>
    <style type="text/css">
        body {
            background: #f6f5f2;
            font-family: Arial, Helvetica, sans-serif;
            color: #b8b8b8;
            font-size: 14px;
        }

        .setW100P {
            width: 650px;
        }

        * {
            margin: 0px;
            padding: 0px;
            box-sizing: border-box;
        }

        .ExternalClass * {
            line-height: 121%;
        }

        table {
            border-collapse: collapse;
        }

        table td {
            border-collapse: collapse;
        }

        img {
            border: none;
            outline: none;
        }

        @media (max-width: 750px) {
            .setW100P {
                width: 493px !important;
            }
        }

        @media (max-width: 500px) {
            .setW100P {
                width: 80% !important;
            }
        }
        .mailBtn a{

                background: #37c2df none repeat scroll 0 0;
                border: 1px solid #2786b4;
                border-radius: 8px;
                box-shadow: 0 -2px 0 0 #2c9bb2 inset;
                color: #fff !important;
                display: inline-block !important;
                font-family: "montserratbold";
                font-size: 20px;
                height: 56px;
                line-height: 52px !important;
                padding: 0 25px !important;
                text-shadow: 0 1px 2px #666;
                text-decoration: none;

        }
    </style>
</head>
<body>
<div align="center"
     style="background: #f6f5f2; font-family: Arial, Helvetica, sans-serif; color: #b8b8b8; font-size: 14px;">
    <table style="border:none; font-size: 14px; width:650px;" align="center">
        <tr>
            <td align="center" valign="top" style="padding-top:30px; padding-bottom:60px;">
                <img src="{{$message->embed(base_path().'/public/site/images/email-logo.png')}}"/></td>
        </tr>
        <tr>
            <td align="center" valign="top" bgcolor="#ffffff" style="border-radius:3px;">
                <table align="left" class="table-left-padd" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="padding:35px;">Dear User<br><br>You have been invited to join the team to raise funds for 
                        <br><br>     
                        Please click the link below to join the team </td>

                    </tr>
                    <tr>
                        <!--change url-->
                        <td style="padding:0px 35px 35px 35px;" class="mailBtn">
                            <a href="givebrite.dev/accept-invitation/{{$team_id}}/{{$email}}">Confirm</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #f2f2f2; padding:29px 0px 10px 40px;">
                            <!--1px solid #f2f2f2-->
                            <img src="{{$message->embed(base_path().'/public/site/images/thank-you.png')}}"/></td>
                    </tr>
                    <tr>
                        <td style="padding:15px 20px 20px 40px;"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-top:35px;" align="center"> Unsubscribe</td>
        </tr>
        <tr>
            <td style="padding-top:25px;" align="center"> GiveBrite HQ<br/>
                Abbey House, 10 Abbey Hills Road, <br/>
                Greater Manchester, OL8 2BS
            </td>
        </tr>
    </table>
</div>
</body>
</html>
