@extends('dashboard::layouts.master')

@section('title', 'Comments')

@section('dashboard-title', '')
@section('style')
<link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
<link rel="stylesheet" href="{{ asset('modules/campaigns/css/bootstrap-select.css') }}">
@stop
@section('content-dashboard')
<div class="col-md-12 innerRightCol myCampaignPage">
        
    <div class="dash-heading">
            <div class="subHeading">Team Pages</div>
        </div>

    <div class="campaignList">
        <div class="row">

            <!--column starts-->
           <?php $count = count($teamPages) ?>
           <div class="col-sm-12 col-md-12 col-lg-12">
            
            @if($count > 0)

                @foreach($teamPages as $teamPage)
                <div class="row">
                    <div class="col-lg-4">
                        <div class="campain-list">
                            <a href="{{url('team-details')}}/{{$teamPage->team_url}}">
                                <div class="campain-img-wrap">
                                  <img alt="img" src="{{ asset('/images/thumb-campaigns/'.$teamPage->cover_photo) }}">
                                </div>
                               
                            </a>

                        </div>
                    </div>

                     <div class="col-lg-4">
                      <div class="team-info">
                      <h3>{{$teamPage->team_name}} </h3>
                      <h4>£{{$teamPage->totalRaised}}<span>of £{{$teamPage->goal_amount_monetary}} raised</span></h4>
                       <div class="campain-bottom-inner">
                            <span class="edit-button">
                                <a href="{{url('team')}}/{{$teamPage->team_id}}/step-one-edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </span>
                            <span class="view-button">
                                <a href="{{url('team')}}/{{$teamPage->team_id}}/status/{{$teamPage->status}}">
                                    @if($teamPage->status == 1) 
                                        <i class="fa fa-pause"></i>
                                    @else
                                        <i class="fa fa-play"></i>
                                    @endif    
                                </a>
                            </span>
                             <span class="add-button">
                                <a href="{{url('team')}}/{{$teamPage->team_id}}/add">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </span>
                            <span class="delete-button">
                                <a href="{{url('team')}}/{{$teamPage->team_id}}/delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>

                        </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="team-info">
                        <h3>Members </h3>
                       <!--  <h4>Total <span> 50</span></h4> -->
                        
                        <div class="profile-wrapper">
                            <div class="team-info">
                                
                            @foreach($members as $member)
                                <?php $count = 0; ?>
                                @foreach($member as $value)
                                    @if($value->team_id == $teamPage->team_id)
                                        <?php $count++ ?>
                                        
                                        @if($count > 0)
                                        <span class="user-profile-charity team-members-thump">
                                            
                                            @if($value->profile_pic == '')

                                                {!! HTML::image('images/userDP.png') !!}
                                            

                                            @elseif(file_exists(public_path('modules/auth/thumb-profile-images/'.$value->profile_pic)))
                                                
                                                {!! HTML::image('modules/auth/thumb-profile-images/'.$value->profile_pic) !!}
                                            
                                            @else
                                                {!! HTML::image('images/userDP.png') !!}
                                            @endif
                                            <br>


                                        </span>
                                       @endif
                                        <?php $count = 0; ?>
                                    @endif

                                @endforeach
                            @endforeach    

                            @if($count  == 0)   
                                <p class="text-muted"> No Members added </p>
                            @endif    

                            </div>    
                        </div>


                         



                        </div>
                    </div>

                </div>
                <hr>
                @endforeach

            @else

                    
                <div id="info" class="innerShadowBox">
                        <div class="comment">
                        <div class="postPublish"></div>
                        <div class="postdescription"> No Team Pages </div>
                    </div>
                </div>

                @endif
            </div>
            <!--column ends-->   
            
        </div>    

    </div>

</div>
@stop
@section('script')
<script type='text/javascript' src="{{ asset('js/readmore.js') }}"></script>
<script src="{!! asset('modules/campaigns/js/bootstrap-select.js') !!}"></script>
<script>$('.selectpicker').selectpicker();</script>
<script>$('.readmore-content').readmore({speed: 500});</script>
<script type="text/javascript">
    var site_url = '<?php echo url(); ?>';
    if(window.location.href.replace(site_url + '/', '') == 'dashboard/comments' || window.location.href.replace(site_url + '/', '') == 'dashboard/comments/'){
        $(".comm-sec").addClass('active');
    }else{
        $(".comm-sec").removeClass('active');
    }

    $(document).ready(function () {
        $(".dash_head").click(function () {
            $(".innerListbox").toggle(500);
        });
    });

</script>
@stop


