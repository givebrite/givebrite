<div class="card">
    <div class="tab-top-wrap">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href='javascript:void(0)'>
                    <span class="campaign-step">Step 1</span>
                    <span class="tab-active">Details</span><i class="fa fa-check"></i>
                </a>
            </li>

            @if(!empty($team_id))
                <li role="presentation" class="active">
                    <a href="javascript:void(0)">

                        <span class="campaign-step">Step 2</span>
                        <span class="tab-active">Manage Campaigns</span>
                        <i class="fa fa-check"></i>
            @else
                <li role="presentation">
                    <a href="#">
                        <span class="campaign-step">Step 2</span>
                        <span class="tab-active">Manage Campaigns</span>
                        @endif
                    </a>
                </li>
               
            <button class="btn btn-success pull-right" id="">Save</button>
        </ul>
    </div>
</div>
{{--<div class="row border-bg">--}}
{{--<div class="col-sm-4 text-center col-xs-4">--}}
{{--<div class="iconOuter {{ !empty($campaignStage) && $campaignStage >= 1  ? 'iconOuter-active': 'iconOuter-Inactive'}}">--}}
{{--<img src="{{ asset('site/images/settings-img.png') }}" alt="" /></a>--}}
{{--</div>--}}
{{--<div class="iconText">Campaign Details</div>--}}
{{--</div>--}}
{{--<div class="col-sm-4 text-center col-xs-4">--}}
{{--<div class="iconOuter {{ !empty($campaignStage) && $campaignStage >= 2  ? 'iconOuter-active': 'iconOuter-Inactive'}}">--}}
{{--@if(!empty($campaignStage) && $campaignStage >= 2)--}}
{{--<a href='{{!empty($campaignId) ? URL::route('design.create', $campaignId) : "" }}'> <img src="{{ asset('site/images/design-img.png') }}" alt="" /></a>--}}
{{--@else--}}
{{--<img src="{{ asset('site/images/design-img-default.png') }}" alt="" />--}}
{{--@endif--}}
{{--</div>--}}
{{--<div class="iconText">Page Design</div>--}}
{{--</div>--}}
{{--<div class="col-sm-4 text-center col-xs-4">--}}
{{--<div class="iconOuter {{ !empty($campaignStage) && $campaignStage >= 4  ? 'iconOuter-active': 'iconOuter-Inactive'}}">--}}
{{--@if(!empty($campaignStage) && $campaignStage >= 4)--}}
{{--<a href='{{!empty($campaignId) ? URL::route('launch.create', $campaignId) : "" }}'>  <img src="{{ asset('site/images/launch-img.png') }}" alt="" /></a>--}}
{{--@else--}}
{{--<img src="{{ asset('site/images/launch-img-default.png') }}" alt="" />--}}
{{--@endif --}}
{{--</div>--}}
{{--<div class="iconText">Launch Campaign</div>--}}
{{--</div>--}}
{{--</div>--}}