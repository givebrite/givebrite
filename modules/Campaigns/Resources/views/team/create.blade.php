@extends('site.layouts.master')

@section('title', 'Campaign')

@section('style')

    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('modules/campaigns/css/Jcrop.css') }}">
    <link href="//cdn.quilljs.com/1.2.2/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.2.2/quill.bubble.css" rel="stylesheet">
    <style type="text/css">
        #editor-container {
            height: 188px;
        }

        .crop-form-wraper img {
            width: 100%;
        }
        .addPhoto .team-logo{

            padding-left:50px;
            padding-right:50px;
            padding-top:50 !important;
            padding-bottom:0px !important;
            
            
        }

        .team-logo{

                max-width: 250px;
                max-height: 250px;
                overflow: hidden;
        }

        .loader{
            position: absolute;
            /* border: 1px solid #333; */
            width: 150px;
            font-size: 55px;
            z-index: 999;
            top: 45%;
            left: 50%;
            color: #fff;
        }

    </style>

@stop

@section('content')
   
    <div class="container customPage">
        @include('campaigns::team.partials.progressbar')
        <div class="custom-page-wrapper">
            
          
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="laungSlogan createHeading">
                            <h4>Create a team page</h4>
                        </div>
                    </div>
                </div>
                 <form method="post" id="createTeam-form" action="{{url('team/store')}}" enctype="multipart/form-data"> 
                {!! Form::token() !!}

                <div class="row">
                    <div class="col-sm-12">
                        <div class="createCampaignForm">
                            <div class="formRow">
                                <div class="secheading">What do you want to call this team? <span
                                            class="astrikBlock">*</span></div>
                                <div class="inputBlock campaignname">
                                    {!! Form::text('team_name', null, ['class'=>'input-large','placeholder' => 'Team Name', 'id' => 'team_name','required'=>'required']) !!}

                                    <span class="lengthlimt">35</span>
                                    {!! $errors->first('team_name', '<div class="text-danger">:message</div>') !!}
                                </div>
                            </div>

                            <div class="formRow">
                                <div class="secheading">Team url? 
                                <span class="astrikBlock">*</span> 
                                    <div class="inputBlock campaignname">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <br>
                                                <h3>http://givebrite.com/team/</h3>
                                            </div>
                                            <div class="col-md-6">
                                                   {!! Form::text('team_url', null, ['class'=>'input-large','placeholder' => 'Team Url', 'id' => 'team_url','required'=>'required']) !!}

                                                    <!-- <span class="lengthlimt">35</span> -->
                                                    {!! $errors->first('team_url', '<div class="text-danger">:message</div>') !!}
                                                    <div id="teamurl_error" class="text-danger"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="formRow">

                            </div>
                            
                        </div>  
                    </div>
                </div>

                <div class="fundraiseBlock">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-lg-7">
                                <div class="switch col-sm-2 col-xs-2 col-md-2 col-lg-1">
                                <br><br>
                                     <input id="cmn-toggle-1" value="1"
                                           class="cmn-toggle cmn-toggle-round"
                                           type="checkbox"
                                           name="pay_type">
                                    <label for="cmn-toggle-1"
                                           class="switchselect lightbg"></label>

                                           
                                </div>
                                <br><br>
                                <div class="col-sm-10 col-xs-10 col-md-10 col-lg-11">
                                    <div class="secheading">Private (Invitation Only) 
                                    </div>
                                    <p>Users will require approval before they can join the team</p>
                                    <br><br>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>            

                <!--image-->
                 <div class='clearfix'></div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="secheading col-sm-12">Upload a cover photo? <span class="astrikBlock">*</span></div>
                        <div class="addPhotoOuter col-sm-3">
                            
                            <div class="addPhoto">
                                <ul>
                                    <li>
                                        <div class="browse-field">
                                            <!-- <input type="file" name="myFile" id="myFile">-->
                                            <input type="hidden" name="teamCoverImageUrl" id="teamCoverImageUrl" value=""> 
                                            {!! Form::file('cover_photo', ['accept'=>"image/gif,image/jpeg,image/png",'class' => 'file-input','id'=>'coverPhoto']) !!}
                                            <label for="coverPhoto" id="testImg">
                                                {!! HTML::image('images/upload-photo.png') !!}
                                                <span>Upload a photo</span>
                                                <p class="upload-text">Images play a very important part in your
                                                    campaigns</p>
                                            </label>
                                            {!! $errors->first('charity_logo', '<div class="text-danger">:message</div>') !!}
                                        </div>

                                    </li>
                                </ul>
                                <div class="text-danger" id="requiredCampainImage"></div>
                            </div>
                        </div>
                        
                        <?php
                        if (Session::has('image')) {
                            $imageName = Session::get('image');
                        } else {
                            if (!empty($campaignDesign->campaign_image)) {
                                $imageName = $campaignDesign->campaign_image;
                            } else {
                                $imageName = 'blank.png';
                            }
                        }
                        $display = ($imageName == 'blank.png') ? 'none' : 'block';
                        $imageUrl = Helper::showImage('campaign', $imageName);
                        ?>

                        <div class='customURLbox col-sm-8'>
                            <div id='image-div' class='inputBlock campaign-profile'>
                                <div class="crop-button-wrapper hidden" id="crop-buttons-one">
                                    <span id="cropit" class="crop-btn" data-toggle="modal"
                                          data-target=".bd-example-modal-lg"><i class="fa fa-crop"></i>Crop</span>
                                    <a id="deleteit" class="del-btn" data-toggle="modal"><i
                                                class="fa fa-trash"></i>Remove
                                    </a>
                                </div>
                                <input type="hidden" id="prev-image"
                                       value="">
                                <img id='viewCoverPhoto' class="img-responsive img-border crop-image"
                                     src="" style="display:{{$display}};">
                            </div>
                        </div>
                    </div>


                    <div class='clearfix'></div>
                    <div>
                        <span class="image-note label label-info">We recommend that you upload an image that is 850px wide and 360px tall.</span>
                    </div>
                </div>
                <!--Image-->

                <!--image-->
                <div class='clearfix'></div>
                <div class="col-sm-12">
                    <div class="row">
                       
                        <div class="addPhotoOuter col-sm-3">
                            
                            <div class="addPhoto">
                                <ul>
                                    <li>
                                        <div class="browse-field team-logo">
                                            <!-- <input type="file" name="myFile" id="myFile">-->
                                            <input type="hidden" name="teamLogoImageUrl" id="teamLogoImageUrl" value="">
                                            {!! Form::file('team_logo', ['accept'=>"image/gif,image/jpeg,image/png",'class' => 'file-input','id'=>'teamLogoImage']) !!}
                                            
                                            <label for="teamLogoImage" id="testImg2">
                                                {!! HTML::image('images/upload-photo.png') !!}
                                                <span>Team Logo</span>
                                                
                                            </label>
                                            {!! $errors->first('charity_logo', '<div class="text-danger">:message</div>') !!}
                                        </div>

                                    </li>
                                </ul>
                                {!! $errors->first('team_logo', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>
                        
                        <div class='customURLbox col-sm-8'>
                            <div id='image-div-two' class='inputBlock team-logo'>
                                <div class="crop-button-wrapper hidden" id="crop-buttons-two">
                                    <span id="cropit-two" class="crop-btn" data-toggle="modal"
                                          data-target=".team-logo-model"><i class="fa fa-crop"></i>Crop</span>
                                    <a id="deleteit" class="del-btn" data-toggle="modal"><i
                                                class="fa fa-trash"></i>Remove
                                    </a>
                                </div>
                                <input type="hidden" id="prev-image" value="">
                                <img id='viewteamLogoImage' class="img-responsive img-border crop-image"
                                     src="" style="display:{{$display}};">
                            </div>
                        </div>
                    </div>


                    <div class='clearfix'></div>
                    <div>
                        <span class="image-note label label-info">We recommend that you upload an image that is 500px wide and 500px tall.</span>
                    </div>
                </div>
                <!--Image-->

                <div class="col-sm-12">
                    <div class="row">
                        <div class="editorOuter">
                            <div class="secheading col-sm-12">Tell us your story? <span class="astrikBlock">*</span>
                            </div>
                        </div>
                        <div class="campaign-desc col-sm-12 col-md-6 col-lg-6 hidden-sm hidden-lg">
                            <h3>To raise the most money for a campaign, make sure you:</h3>
                            <ul>
                                <li><i class="fa fa-check"></i> Describe who will benefit from your campaign</li>
                                <li><i class="fa fa-check"></i> Tell donors what the funds will be used for</li>
                                <li><i class="fa fa-check"></i> Highlight how soon you need the funds</li>
                                <li><i class="fa fa-check"></i> Explain what the support means to you</li>
                                <li><i class="fa fa-check"></i> Express how grateful you will be for the help</li>
                            </ul>
                        </div>

                    
                        <div class="coloePicker formRow col-sm-12 col-md-6 col-lg-6">
                            {!! Form::hidden('campaign_description', null,['id' => 'camp_desc']) !!}
                            <div id="editor-container">
                            </div>
                            <div id="campaign_description_error" class="text-danger">
                                @if($errors->first('campaign_description'))
                                    {{ $errors->first('campaign_description') }}
                                @endif
                            </div>
                        </div>
                        <div class="campaign-desc col-sm-12 col-md-6 col-lg-6 hidden-xs">
                            <h3>To raise the most money for a campaign, make sure you:</h3>
                            <ul>
                                <li><i class="fa fa-check"></i> Describe who will benefit from your campaign</li>
                                <li><i class="fa fa-check"></i> Tell donors what the funds will be used for</li>
                                <li><i class="fa fa-check"></i> Highlight how soon you need the funds</li>
                                <li><i class="fa fa-check"></i> Explain what the support means to you</li>
                                <li><i class="fa fa-check"></i> Express how grateful you will be for the help</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-12">
                    <div class="row">
                        <div class="buttonBlock">
                            <button class="blue-btn-lg text-center" id='submit'>Save and Continue
                                <!-- <i class="fa fa-arrow-right" aria-hidden="true"></i> --></button>
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}

            
            
        </div>
    </div>
    
    <!-- Large modal -->
    <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close hidden model_campaign_image" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title-crop">Crop Image</h4>
                    <div class="loader hidden"><i class="fa fa-circle-o-notch fa-6 fa-spin" aria-hidden="true"></i></div>
                </div>
                <div class="crop-form-wraper">
                    {!! Form::open(array('id' => 'cropfrm' ,'url' => '', 'files' => true)) !!}
                    <div class="hidden-input">
                        {!! Form::hidden('x','0',array('id' => 'x')) !!}
                        {!! Form::hidden('y','0',array('id' => 'y')) !!}
                        {!! Form::hidden('w','360',array('id' => 'w')) !!}
                        {!! Form::hidden('h','240',array('id' => 'h')) !!}
                        {!! Form::hidden('imgpath','',array('id' => 'imgpath')) !!}
                        {!! Form::hidden('imgName','',array('id' => 'imgName')) !!}
                    </div>
                    <div class="crop-img-show">
                        <img id='viewCampaignImage1'/>
                    </div>
                    <div class="modal-footer-crop">
                        <button id="cancel-btn" type="button" class="btn btn-default model_campaign_image hidden" data-dismiss="modal">Cancel
                        </button>
                        <input type="submit" value="Crop" class="btn btn-success">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <!-- Large modal -->
    <div class="modal fade team-logo-model" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close hidden model_campaign_image_two" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title-crop">Crop Image</h4>
                    <div class="loader hidden"><i class="fa fa-circle-o-notch fa-6 fa-spin" aria-hidden="true"></i></div>
                </div>
                <div class="crop-form-wraper">
                    {!! Form::open(array('id' => 'cropfrm-team-logo' ,'url' => '', 'files' => true)) !!}
                    <div class="hidden-input">
                        {!! Form::hidden('x','0',array('id' => 'xx')) !!}
                        {!! Form::hidden('y','0',array('id' => 'yy')) !!}
                        {!! Form::hidden('w','360',array('id' => 'ww')) !!}
                        {!! Form::hidden('h','240',array('id' => 'hh')) !!}
                        {!! Form::hidden('imgPathTeam','',array('id' => 'imgPathTeam')) !!}
                        {!! Form::hidden('imgTeamName','',array('id' => 'imgTeamName')) !!}
                    </div>
                    <div class="crop-img-show">
                        <img id='viewTeamImage'/>
                    </div>
                    <div class="modal-footer-crop">
                        <button id="cancel-btn-logo" type="button" class="model_campaign_image_two btn btn-default hidden" data-dismiss="modal">Cancel
                        </button>
                        <input type="submit" value="Crop" class="btn btn-success">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    

@stop

@section('script')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDr8eQbTjFc-rAYCatXxfKd4-T4zW5_2uk&sensor=false&amp;?components=country:UK&libraries=places"></script>
    <script src="{!! asset('modules/team/js/team-design.js') !!}"></script>
    <script src="{!! asset('modules/campaigns/js/jquery.geocomplete.min.js') !!}"></script>
    
    <script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{!! asset('modules/campaigns/js/design-validation.js') !!}"></script>


    <script type="text/javascript" src="{{ asset('modules/campaigns/js/Jcrop.js') }}"></script>
   <!--  <script src="{!! asset('modules/campaigns/js/common.js') !!}"></script> -->
   <script src="{!! asset('modules/campaigns/js/common-new.js') !!}"></script>
    <!-- <script src="{!! asset('modules/campaigns/js/tinymce.js') !!}"></script> -->
    <script src="//cdn.quilljs.com/1.2.2/quill.js"></script>
    <script src="//cdn.quilljs.com/1.2.2/quill.min.js"></script>
    <script src="{!! asset('modules/campaigns/js/quilljs.js') !!}"></script>
    <script type="text/javascript">
            
            $(document).ready(function () {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

            });
            
            $('#createTeam-form').submit(function (e){

                var fileName = $("#coverPhoto").val();

                if(fileName) { // returns true if the string is not empty
                    
                
                } else { // no file was selected
                                        
                    $('#requiredCampainImage').html('Cover image is required');
                    $(window).scrollTop(200);
                    e.preventDefault();
                }
                $('.requiredCampainImage').html('');

                //Check for url availability
                var team_url = { name: $('#team_url').val() } 
                
                $.ajax({
                    url: site_url + '/team/check-url-availability',
                    method: 'post',
                    dataType: 'json',
                    data: team_url,
                    success: function (response) {
                        if (response.msg == "unavailable") {
                            $('#teamurl_error').html('Team url already taken');
                            e.preventDefault();
                        }

                        if (response.msg == "available") {

                            return true;                            
                        }

                    },
                    failure: function (response) {
                        alert('please refresh the page');
                    }

                });
            });


            $("#cropfrm").submit(function (e) {
                $(".loader").removeClass('hidden');
                e.preventDefault();
                var data = $("#cropfrm").serialize();
                $.ajax({
                    url: site_url + '/team/crop',
                    method: 'post',
                    dataType: 'json',
                    data: data,
                    success: function (response) {
                        if (response.msg == "success") {
                            $("#croppedImage").val(response.imageName);
                            $("#cancel-btn").click();
                            $(".model_campaign_image").removeClass('hidden');
                            imgurl = site_url + '/images/thumb-campaigns/' + response.imageName;
                            $('#viewCoverPhoto').attr('src',imgurl);
                            $("#teamCoverImageUrl").val(response.imageName);
                            $(".loader").addClass('hidden');
                        }
                    },
                    failure: function (response) {
                        console.log(response);
                    }

                });
            });

            $("#cropfrm-team-logo").submit(function (e) {
                $(".loader").removeClass('hidden');
                e.preventDefault();
                var data = $("#cropfrm-team-logo").serialize();
                $.ajax({
                    url: site_url + '/team/crop-logo',
                    method: 'post',
                    dataType: 'json',
                    data: data,
                    success: function (response) {
                        if (response.msg == "success") {
                            $("#croppedImage").val(response.imageName);
                            $("#cancel-btn-logo").click();
                            $(".model_campaign_image_two").removeClass('hidden');
                            imgurl = site_url + '/images/thumb-campaigns/' + response.imageName;
                            $('#viewteamLogoImage').attr('src',imgurl);
                            $("#teamLogoImageUrl").val(response.imageName);
                            $(".loader").addClass('hidden');
                        }
                    },
                    failure: function (response) {
                        console.log(response);
                    }

                });
            });


    </script>
    <script>
       
       $("#cmn-toggle-1").click(function () {
                        
            if ($("#cmn-toggle-1").prop('checked')) {
            
                $("#pay_type").val('1');
                $('.switchselect').addClass('darkbg');
                $('.switchselect').removeClass('lightbg');
            
            } else {
                
                $("#pay_type").val('0');
                $('.switchselect').addClass('lightbg');
                $('.switchselect').removeClass('darkbg');
            }   

        });
        
        timer = 0;

        function mySearch (){ 
            var xx = $(input).val();
            console.log(xx); 
        }
        
        $('#team_url').keyup('keyup', function(e){

            console.log(this.value);
            
        });

       var data = $(".lengthlimt").html(); 
       
       $("#team_name").on('keyup',function(e){
            
            if (this.value.length == 35) {
                if (e.keyCode === 8) {
                    console.log('Backspace');
                } else {
                    e.preventDefault();
                }
            } else {

                var teamUrl = this.value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '_');
                $('#team_url').val(teamUrl);

                console.log(teamUrl);
                
                //perform ajax ro check url availability


            }

            if (Number($(this).val().length) < 35) {
                var newdata = (data - Number($(this).val().length));
                $(".lengthlimt").html(newdata);
            } else {
                $(".lengthlimt").html('0');

            }
        });


        //validate 


    </script>
@stop