@extends('site.layouts.master')

@section('title', 'Campaign')

@section('style')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <link rel="stylesheet" type="text/css" href="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/css/Jcrop.css">
    <link href="//cdn.quilljs.com/1.2.2/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.2.2/quill.bubble.css" rel="stylesheet">
    <style type="text/css">
        #editor-container {
            height: 188px;
        }

        .crop-form-wraper img {
            width: 100%;
        }
    </style>

@stop

@section('content')
   
    <div class="container customPage">
        @include('campaigns::team.partials.progressbar')
        <div class="custom-page-wrapper">
            
            <div class="row">
           {!! Form::open(array('url' => 'team/manage', 'method' => 'post' , 'files' => true)) !!}
            <input type="hidden" name="team_id" value="{{$team_id}}">
                    <div class="col-sm-12">
                        <div class="laungSlogan createHeading">
                            <h4>Manage Team Campaign</h4>
                        </div>
                        <div class="secheading">Choose a campaign to add to the team ? <span class="astrikBlock">*</span></div>
                        <br><br>
                         <!--list the user active campains-->
                        <div class="row">
                            <div class="col-lg-5 col-md-5 manage-campaign-list">

                                <ul>
                                    
                                    @foreach($campainDetails as $details)

                                        <li>
                                            <label><input type="radio" name="campaign_id" value="{{$details->id}}">
                                              <span>{{$details->campaign_name}}</span>
                                            </label>
                                            <a href="#" target="_blank">
                                                <i>{{ url('campaign') }}/{{$details->slug}}</i>
                                            </a>
                                        </li>

                                            
                                    @endforeach

                                    
                                    
                                </ul>
                            </div>
                            <div class="col-lg-2 col-md-2 text-center">
                                <div class="or-text">or</div>
                            </div>
                            <div class="col-lg-4 col-md-4 manage-add-new-campaign"> 
                                <div class="secheading">Start fresh with a new campaign</div>
                                <br>
                                <a class="btn btn-success" target="_blank" href="{{ url('campaigns/create') }}">New Campaign</a>
                            </div>
                        </div>

                        <!--limit the user active campains-->
                        
                        <!--add team members-->
                        <div class="laungSlogan createHeading">
                            <h4>Team Members</h4>
                        </div>
                        <div class="secheading">Invite friends to join this campaign ? <span class="astrikBlock">*</span></div>
                        <div class="formRow">
                            <div class="inputBlock campaignname">

                        <!--new -->
                        <div id="donation-type">
                            <div class="row margin-top-10">
                                <div class="col-sm-12 col-xs-12 col-lg-12">

                                    <div class="inputBlock donation-type">
                                       {!! Form::text('team_email', null, ['class'=>'input-large','placeholder' => 'Email Address', 'id' => 'add-email']) !!}
                                                                                
                                    </div>
                                    <a id="inviteEmail" class="plus-sign addEmail" href="javascript:void(0)">
                                    <i class="fa fa-plus"></i></a>


                                </div>

                            </div>

                            
                        </div>    
                        <!--new -->

                               <!--  {!! Form::text('team_email', null, ['class'=>'input-large','placeholder' => 'Email Address', 'id' => 'add-email']) !!}
                                <a class="addEmail" id="inviteEmail" href="javascript:void(0)"> <i class="fa fa-plus"></i></a>
 -->

                                <input type="hidden" name="team" id="team_id" value="{{$team_id}}">
                                <input type="hidden" name="team_url" id="team_id" value="{{$team_url}}">
                                
                            </div>
                        </div>
                        <!--add team members-->

                        <!--invite list-->
                        <div class="mail-list">
                            <table id="tblEntAttributes">
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <!-- <th>Status</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>
                        </div>
                        <!--invite list-->

                    </div>
               
           
            </div>
              <div class="form-group col-sm-12">
                    <div class="row">
                        <div class="buttonBlock">
                            <button class="blue-btn-lg text-center" id='submit'>Save and Continue
                                <!-- <i class="fa fa-arrow-right" aria-hidden="true"></i> --></button>
                        </div>
                    </div>
                </div>

            {!! Form::close() !!}
  
        </div>
    </div>
  

@stop

@section('script')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDr8eQbTjFc-rAYCatXxfKd4-T4zW5_2uk&sensor=false&amp;?components=country:UK&libraries=places"></script>
    <script src="{!! asset('modules/campaigns/js/jquery.geocomplete.min.js') !!}"></script>
    
    <script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{!! asset('modules/campaigns/js/design-validation.js') !!}"></script>


    <script type="text/javascript" src="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/js/Jcrop.js"></script>
    <script src="{!! asset('modules/campaigns/js/common.js') !!}"></script>
    <!-- <script src="{!! asset('modules/campaigns/js/tinymce.js') !!}"></script> -->
    <script>
       $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

       $("#inviteEmail").click(function(){
           

           var team_id = $('#team_id').val();

           $.ajax({
                url: "/team/invite-user/"+$('#add-email').val()+"/"+team_id+"",
                dataType: "json",
                type: "get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                
                    var divOne = "<tr><td><i>"+$('#add-email').val()+" (Invite Sent)</i></td></tr>";
                    
                    $("#tblEntAttributes tbody").append(divOne);
                    

                },

                error:function (xhr, ajaxOptions, thrownError){

                    if(xhr.status==404) {

                          console.log('No post code found');
                    }
                }
            });
       
       });

    </script>
@stop