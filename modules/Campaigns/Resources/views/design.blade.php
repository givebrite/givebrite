@extends('site.layouts.master')

@section('title', 'Campaign')

@section('style')

    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('modules/campaigns/css/Jcrop.css') }}">
    <link href="//cdn.quilljs.com/1.2.2/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.2.2/quill.bubble.css" rel="stylesheet">
    <style type="text/css">
        #editor-container {
            height: 188px;
        }

        .crop-form-wraper img {
            width: 100%;
        }

        .loader{
            position: absolute;
            /* border: 1px solid #333; */
            width: 150px;
            font-size: 55px;
            z-index: 999;
            top: 45%;
            left: 50%;
            color: #fff;
        }

    </style>

@stop

@section('content')
    <?php $base_path = Config::get('config.thumb_campaign_upload_path');?>
    @if(!empty($campaignDesign->id))
        {!! Form::model($campaignDesign, ['id'=>'campaign-design','method' => 'PUT', 'files' => true, 'route' => ['design.update', $campaignDesign->campaign_id,$campaignDesign->id]]) !!}
    @else
        {!! Form::open(['id'=>'campaign-design','files' => true, 'route' => ['design.store', $campaignId]]) !!}
    @endif
    <div class="container customPage">
        @include('campaigns::partials.progressbar')
        <div class="custom-page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="laungSlogan">
                        <h4>Customise your page</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="colorPickerOuter">
                        <div class="secheading">Choose a colour scheme? <span class="astrikBlock">*</span></div>
                        @if(!empty($campaignThemes))
                            <div class="customradio-theme">
                                <?php $i = 0;?>
                                @foreach($campaignThemes as $key=>$theme)
                                    <?php
                                    if (empty($campaignDesign->theme_id) && $i == 0) {
                                        $checked = "checked";
                                    } else {
                                        $checked = "";
                                    }
                                    $i++;
                                    ?>
                                    <div class="colorBox">
                                        {!! Form::radio('theme_id', $key, false, array('id'=>'theme_'.$key,$checked)) !!}
                                        <label for="theme_{{$key}}">
                                            <span style='background-color:{{!empty($theme) ? $theme : ""}}'></span>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div id="theme_id_error" class="text-danger">
                        @if($errors->first('theme_id'))
                            {{ $errors->first('theme_id') }}
                        @endif
                    </div>
                </div>
                <div class='clearfix'></div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="secheading col-sm-12">Upload a cover photo? <span class="astrikBlock">*</span></div>
                        <div class="addPhotoOuter col-sm-3">
                            {!! Form::hidden('media_type', !empty($campaignDesign->media_type) ? $campaignDesign->media_type : "image" ) !!}
                            {!! Form::hidden('campaign_image1', '',['id'=>'croppedImage'])!!}
                            <div class="addPhoto">
                                <ul>
                                    <li>
                                        <div class="browse-field">
                                            <!-- <input type="file" name="myFile" id="myFile">-->
                                            {!! Form::file('campaign_image', ['accept'=>"image/gif,image/jpeg,image/png",'class' => 'file-input','id'=>'campaignImage']) !!}
                                            <label for="campaignImage" id="testImg">
                                                {!! HTML::image('images/upload-photo.png') !!}
                                                <span>Upload a photo</span>
                                                <p class="upload-text">Images play a very important part in your
                                                    campaigns</p>
                                            </label>
                                            <div class="text-danger"
                                                 id="charity_logo_error">@if($errors->first('charity_logo')) {{ $errors->first('charity_logo') }}@endif</div>
                                            {!! $errors->first('charity_logo', '<div class="text-danger">:message</div>') !!}
                                           </div>

                                    <!--                            <div class="file-upload pcUpload">
                                                                    {!! Form::file('campaign_image', ['accept'=>"image/gif,image/jpeg,image/png",'class' => 'file-input','id'=>'campaignImage']) !!}
                                            <span>Computer</span>
                                        </div>-->
                                    </li>
                                </ul>
                                {!! $errors->first('campaign_image', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>
                        <?php
                        if (Session::has('image')) {
                            $imageName = Session::get('image');
                        } else {
                            if (!empty($campaignDesign->campaign_image)) {
                                $imageName = $campaignDesign->campaign_image;
                            } else {
                                $imageName = 'blank.png';
                            }
                        }
                        $display = ($imageName == 'blank.png') ? 'none' : 'block';
                        $imageUrl = Helper::showImage('campaign', $imageName);
                        ?>

                        <div class='customURLbox {{($imageName == 'blank.png') ? "hidden-xs" : ""}}  col-sm-8'>
                            <div id='image-div' class='inputBlock campaign-profile'>
                                <div class="crop-button-wrapper" id="crop-buttons" style="display: none;">
                                    <span id="cropit" class="crop-btn" data-toggle="modal"
                                          data-target=".bd-example-modal-lg"><i class="fa fa-crop"></i>Crop</span>
                                    <button id="deleteit" class="del-btn" data-toggle="modal"><i
                                                class="fa fa-trash"></i>Remove
                                    </button>
                                </div>
                                <input type="hidden" id="prev-image"
                                       value="{{(serialize(Input::file('campaign_image')))}}">
                                <img id='viewCampaignImage' class="img-responsive img-border crop-image"
                                     src="{{asset($imageUrl)}}" style="display:{{$display}};">
                            </div>
                        </div>
                    </div>


                    <div class='clearfix'></div>
                    <div>
                        <span class="image-note label label-info">We recommend that you upload an image that is 850px wide and 360px tall.</span>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <div class="editorOuter">
                            <div class="secheading col-sm-12">Tell us your story? <span class="astrikBlock">*</span>
                            </div>
                        </div>
                        <div class="campaign-desc col-sm-12 col-md-6 col-lg-6 hidden-sm hidden-lg">
                            <h3>To raise the most money for a campaign, make sure you:</h3>
                            <ul>
                                <li><i class="fa fa-check"></i> Describe who will benefit from your campaign</li>
                                <li><i class="fa fa-check"></i> Tell donors what the funds will be used for</li>
                                <li><i class="fa fa-check"></i> Highlight how soon you need the funds</li>
                                <li><i class="fa fa-check"></i> Explain what the support means to you</li>
                                <li><i class="fa fa-check"></i> Express how grateful you will be for the help</li>
                            </ul>
                        </div>

                    <!-- <div class="coloePicker formRow col-sm-12 col-md-6 col-lg-6">
                        {!! Form::textarea('campaign_description', null, ['class' => 'input-large textarea']) !!}
                            <div id="campaign_description_error" class="text-danger">
@if($errors->first('campaign_description'))
                        {{ $errors->first('campaign_description') }}
                    @endif
                            </div>
                        </div> -->
                        <div class="coloePicker formRow col-sm-12 col-md-6 col-lg-6">
                            {!! Form::hidden('campaign_description', null,['id' => 'camp_desc']) !!}
                            <div id="editor-container">
                            </div>
                            <div id="campaign_description_error" class="text-danger">
                                @if($errors->first('campaign_description'))
                                    {{ $errors->first('campaign_description') }}
                                @endif
                            </div>
                        </div>
                        <div class="campaign-desc col-sm-12 col-md-6 col-lg-6 hidden-xs">
                            <h3>To raise the most money for a campaign, make sure you:</h3>
                            <ul>
                                <li><i class="fa fa-check"></i> Describe who will benefit from your campaign</li>
                                <li><i class="fa fa-check"></i> Tell donors what the funds will be used for</li>
                                <li><i class="fa fa-check"></i> Highlight how soon you need the funds</li>
                                <li><i class="fa fa-check"></i> Explain what the support means to you</li>
                                <li><i class="fa fa-check"></i> Express how grateful you will be for the help</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="createCampaignForm">
                <div class="row">
                    <div class="secheading col-sm-12">What is your Postcode ? <span class="astrikBlock">*</span></div>
                    <div class="formRow col-sm-8">
                        <div class="inputBlock">
                            {!! Form::text('postcode', null, ['class' => 'input-large','placeholder'=>'Type in an address','id'=>'postcode']) !!}
                            <span class="minor pull-right location-link-span"><a class='location-link'>Enter Address manually</a></span>
                        </div>
                    </div>
                    <?php
                    if (!empty($campaignDesign->location) && !empty($campaignDesign->city)) {
                        $hideDivClass = "show-Location";
                    } else {
                        $hideDivClass = "hide-Location";
                    }
                    ?>
                </div>
                <div class="{{$hideDivClass}}" id="location-div">
                    <div class="row">
                        <div class="secheading col-sm-12">Country <span class="astrikBlock">*</span></div>
                        <div class="formRow col-sm-8">
                            <div class="inputBlock">
                                {!! Form::text('country', !empty($campaignDesign->location) ? Helper::getLocationById($campaignDesign->location) : "", ['class' => 'input-large']) !!}
                                <input name="lat" type="hidden"
                                       value="{{!empty($campaignDesign->lattitude) ? $campaignDesign->lattitude : '' }}">
                                <div id="country_error" class="text-danger">
                                    @if($errors->first('country'))
                                        {{ $errors->first('country') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="formRow col-sm-8">
                            <div class="secheading">Town <span class="astrikBlock">*</span></div>
                            <div class="inputBlock">
                                {!! Form::text('locality', !empty($campaignDesign->city) ? Helper::getLocationById($campaignDesign->city) : "", ['class' => 'input-large']) !!}
                                <input name="lng" type="hidden"
                                       value="{{!empty($campaignDesign->longitude) ? $campaignDesign->longitude : '' }}">
                                <div id="locality_error" class="text-danger">
                                    @if($errors->first('locality'))
                                        {{ $errors->first('locality') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="row">
                    <div class="buttonBlock">
                        <button class="blue-btn-lg text-center" id='submit'>Save and Continue
                            <!-- <i class="fa fa-arrow-right" aria-hidden="true"></i> --></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    <div class="details">
        <input name="lat" type="hidden" value="">
        <input name="lng" type="hidden" value="">
        <input name="formatted_address" type="hidden" value="">
    </div>

    <!-- Large modal -->
    <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close hidden model_campaign_image" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title-crop">Crop Image</h4>
                    <div class="loader hidden"><i class="fa fa-circle-o-notch fa-6 fa-spin" aria-hidden="true"></i></div>
                </div>
                <div class="crop-form-wraper">
                    {!! Form::open(array('id' => 'cropfrm' ,'url' => 'campaign/crop/'.$campaignId, 'files' => true)) !!}
                    <div class="hidden-input">
                        {!! Form::hidden('x','0',array('id' => 'x')) !!}
                        {!! Form::hidden('y','0',array('id' => 'y')) !!}
                        {!! Form::hidden('w','360',array('id' => 'w')) !!}
                        {!! Form::hidden('h','240',array('id' => 'h')) !!}
                        {!! Form::hidden('imgpath','',array('id' => 'imgpath')) !!}
                        {!! Form::hidden('imgName','',array('id' => 'imgName')) !!}
                    </div>
                    <div class="crop-img-show">
                        <img id='viewCampaignImage1'/>
                    </div>
                    <div class="modal-footer-crop">
                        <button id="cancel-btn" type="button" class="model_campaign_image btn btn-default hidden" data-dismiss="modal">Cancel
                        </button>
                        <input type="submit" value="Crop" class="btn btn-success">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@stop

@section('script')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDr8eQbTjFc-rAYCatXxfKd4-T4zW5_2uk&sensor=false&amp;?components=country:UK&libraries=places"></script>
    <script src="{!! asset('modules/campaigns/js/campaign-design.js') !!}"></script>
    <script src="{!! asset('modules/campaigns/js/jquery.geocomplete.min.js') !!}"></script>
    @if(!empty($campaignDesign->id))
        {!!
        JsValidator::formRequest('Modules\Campaigns\Requests\CampaignDesignUpdateRequest','#campaign-design')
        !!}
    @else
        {!!
        JsValidator::formRequest('Modules\Campaigns\Requests\CampaignDesignCreateRequest','#campaign-design')
        !!}
    @endif
    <script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{!! asset('modules/campaigns/js/design-validation.js') !!}"></script>


    <script type="text/javascript" src="{!! asset('modules/campaigns/js/Jcrop.js') !!}"></script>
    <script src="{!! asset('modules/campaigns/js/common.js') !!}"></script>
    <!-- <script src="{!! asset('modules/campaigns/js/tinymce.js') !!}"></script> -->
    <script src="//cdn.quilljs.com/1.2.2/quill.js"></script>
    <script src="//cdn.quilljs.com/1.2.2/quill.min.js"></script>
    <script src="{!! asset('modules/campaigns/js/quilljs.js') !!}"></script>
    <script>
        $("#cropfrm").submit(function (e) {
            $(".loader").removeClass('hidden');
            e.preventDefault();
            var data = $("#cropfrm").serialize();
            $.ajax({
                url: site_url + '/campaign/crop/30',
                method: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {
                    if (response.msg == "success") {
                        $("#croppedImage").val(response.imageName);
                        imgPathB = site_url + '/images/thumb-campaigns/' + response.imageName;
                        $("#cancel-btn").click();
                        $("#viewCampaignImage").attr('src', imgPathB);
                        $(".model_campaign_image").removeClass('hidden');
                        $(".loader").addClass('hidden');
                        /** /
                         $("#viewCampaignImage").attr('src', site_url + '/images/thumb-campaigns/' + imgName);
                         $("#cancel-btn").click();
                         /**/
                    }
                },
                failure: function (response) {
                    console.log(response);
                }

            });
        });


    </script>
@stop
