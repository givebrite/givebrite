@extends('site.layouts.master')

@section('title', 'Campaign')

@section('style')
    <link type="text/css" rel="stylesheet"
          href="{{ asset('modules/campaigns/css/campaign.css').'?num='.mt_rand(1000000, 9999999) }}">
    <link href="{{ asset('modules/campaigns/css/select2.css') }}" rel="stylesheet" />
   
@stop

@section('content')

    <?php $base_path = Config::get('config.upload_path');?>
    @if(isset($campaign))
        {!! Form::model($campaign, ['method' => 'PUT', 'files' => true, 'route' => ['campaigns.update', $campaign->id],'id'=>'campaign-detail']) !!}
    @else
        {!! Form::open(['id'=>'campaign-detail','files' => true, 'route' => 'campaigns.store']) !!}
    @endif

    <div class="customPage">
        <div class="container">
            @include('campaigns::partials.progressbar')
            <div class="row">
                <div class="col-sm-12">
                    <!-- <br>
                    <a  href = "{{url('team/create')}}" class="btn btn-success text-center">Create a team </a> -->
                    <div class="laungSlogan createHeading">
                        <h4>Create a Campaign</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="createCampaignForm">
                        <div class="formRow">
                            <div class="secheading">How much do you want to raise? <span class="astrikBlock">*</span>
                            </div>
                            <div class="inputBlock">
                                <p class="gbpBtn">
                                    <input type='hidden' name='goal_amount_type' value='monetary' placeholder="&pound;">
                                    <span class="sterling">&pound;</span>
                                    {!! Form::text('goal_amount_monetary',!empty($goalAmount) ? $goalAmount : null , ['id'=>'goal_amount_monetary']) !!}
                                </p>
                                {!! $errors->first('goal_amount_monetary', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="secheading">What do you want to call this campaign? <span
                                        class="astrikBlock">*</span></div>
                            <div class="inputBlock campaignname">
                                {!! Form::text('campaign_name', null, ['class'=>'input-large','placeholder' => 'Campaign Title', 'id' => 'campaing_name']) !!}

                                <span class="lengthlimt">35</span>
                                {!! $errors->first('campaign_name', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="secheading">Campaign category <span class="astrikBlock">*</span></div>
                            <div class="inputBlock">
                                <div class="selectOuter">
                                    {!! Form::select('campaign_category', [''=>'Please Select...']+$categories, null) !!}
                                    <div class="select__arrow"></div>
                                </div>
                            </div>
                            {!! $errors->first('campaign_category', '<div class="text-danger">:message</div>') !!}
                        </div>


                        <div class="formRow">
                            <div class="inputBlock">
                                <div class="customRadio largeBox">
                                    <div>
                                        {!! Form::radio('campaign_duration', 'ongoing',['checked'=>'true']) !!}<label
                                                class="ongoingSec" for="campaign_duration">
                                            <div class="imgBlock"></div>
                                            <div class="selectboxheading">Ongoing</div>
                                            <p>This creates urgency and should always be used when money is needed
                                                before a certain time.</p>
                                        </label>
                                    </div>
                                    <div class="end-date-box">
                                        {!! Form::radio('campaign_duration', 'enddate') !!}<label class="endDateSec"
                                                                                                  for="campaign_duration">
                                            <div class="imgBlock"></div>
                                            <div class="selectboxheading">End Date
                                            </div>
                                            <p>This creates urgency and should always be used when money is needed
                                                before a certain time.</p>
                                        </label>
                                        {!! Form::text('end_date', null, ['class'=>'input-sm','id' => 'datepicker','placeholder'=>'End Date','readonly'=>'readonly']) !!}
                                        <i class="fa fa-calendar customCalender" aria-hidden="true"></i>
                                    </div>
                                </div>
                                {!! $errors->first('end_date', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div>

                    <!-- <div class="formRow">
                      <div class="inputBlock">
                        <div class="customRadio clearfix">
                          <div class="campaign-option">
                            {!! Form::radio('campaign_type', 'cause',['checked'=>'true']) !!} <label class="causeLable" for="campaign_type"><span  class="bottomtext">Cause<span></label>
                          </div>
                          <div class="campaign-option">
                            {!! Form::radio('campaign_type', 'business') !!} <label class="bussLable" for="campaign_type"><span class="bottomtext">Business<span></label>
                          </div>
                        </div>
                            {!! $errors->first('campaign_type', '<div class="text-danger">:message</div>') !!}
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if(!empty($user['role']) && $user['role'] != 'Charity' && !empty($charityList) && count($charityList)>0)
        <div class="fundraiseBlock">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-lg-7">
                        <div class="switch col-sm-2 col-xs-2 col-md-2 col-lg-1">
                            @if(isset($campaign))
                                {!! Form::hidden('pay_type', $campaign->pay_type ,['id' => 'pay_type'])!!}
                            @else
                                {!! Form::hidden('pay_type', '' ,['id' => 'pay_type'])!!}
                            @endif

                            @if(isset($campaign) && $campaign->charity_id)
                                <input id="cmn-toggle-1" value="1"
                                       class="cmn-toggle cmn-toggle-round"
                                       type="checkbox"
                                       name="pay_type" checked="checked">
                                <label for="cmn-toggle-1" class="switchselect darkbg"></label>
                            @else
                                @if($charity_id && !empty($charity_link_default))

                                    <input id="cmn-toggle-1" value="1"
                                           class="cmn-toggle cmn-toggle-round"
                                           type="checkbox"
                                           name="pay_type" checked="checked">
                                    <label for="cmn-toggle-1"
                                           class="switchselect darkbg"></label>
                                @else
                                    <input id="cmn-toggle-1" value="1"
                                           class="cmn-toggle cmn-toggle-round"
                                           type="checkbox"
                                           name="pay_type">
                                    <label for="cmn-toggle-1"
                                           class="switchselect lightbg"></label>
                                @endif

                            @endif
                        </div>
                        <div class="col-sm-10 col-xs-10 col-md-10 col-lg-11">
                            <div class="secheading">I would like my funds to go to a Charity?
                            </div>
                            <p>By turning on this feature it means all funds raised will go
                                directly to your chosen
                                charity</p>
                        </div>
                        <!-- <div class="inputBlock">
                        @if( isset($campaign) && $campaign->charity_id)
                            {!! Form::hidden('charity_id', $campaign->charity_id, array('id' => 'charity_id')) !!}
                        @else
                            @if($charity_id && !empty($charity_link_default))
                                {!! Form::hidden('charity_id', $charity_id, array('id' => 'charity_id')) !!}
                            @else
                                {!! Form::hidden('charity_id', '', array('id' => 'charity_id')) !!}
                            @endif
                        @endif
                        <!--<div class="selectOuter">  $charityList

                        {!! Form::select('charity_id', ['' => 'Select'] + $charityList, !empty($charity_id) ? $charity_id : null ) !!}
                                </div>
                            {!! $errors->first('charity_id', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div> -->

                   @if(isset($campaign) && $campaign->charity_id || ($charity_id != '' && !empty($charity_link_default)))
                        <div class="col-sm-12 col-xs-12 col-lg-6 dropdown-wrapper" id="charity_list">
                    @else
                        <div class="col-sm-12 col-xs-12 col-lg-6 dropdown-wrapper" id="charity_list" style="display: none;">
                    @endif
                        <!-- <input type="text" id="search-charity" hidden>
                            <dl id="sample" class="dropdown">
                                @if(isset($campaign) && $campaign->charity_id)
                                    <dt><a href=""><span>
                                                    @if(file_exists(public_path('modules/auth/thumb-charity-logo/'.$charityLogo[$campaign->charity_id])))
                                                    <img height="50" width="155"
                                                         class="flag"
                                                         src="{{asset('modules/auth/thumb-charity-logo/'.$charityLogo[$campaign->charity_id])}}"
                                                         alt=""/>
                                                @endif
                                                {{$charityList[$campaign->charity_id]}}
                                        </span></a></dt>
                                @elseif($charity_id)
                                    @if(!empty($charity_link_default))
                                        <dt><a href=""><span>
                                                    @if(file_exists(public_path('modules/auth/thumb-charity-logo/'.$charity_link_default->charity_logo)))
                                                        <img height="50" width="155"
                                                             class="flag"
                                                             src="{{asset('modules/auth/thumb-charity-logo/'.$charity_link_default->charity_logo)}}"
                                                             alt=""/>
                                                    @endif
                                                    {{$charity_link_default->charity_name}}
                                        </span></a></dt>
                                    @else
                                        <dt><a href=""><span class="dropdown-list">Select</span></a>
                                        </dt>
                                    @endif
                                @else
                                    <dt><a href=""><span
                                                    class="dropdown-list">Select</span></a>
                                    </dt>
                                @endif

                                <dd>
                                    <ul>
                                        @foreach($charityList as $key => $value)
                                            <li><a href="">
                                                    @if(file_exists(asset('modules/auth/thumb-charity-logo/'.$charityLogo[$key])))
                                                    <div class="charity_logo_dd">
                                                        <img class="flag"
                                                             src="{{asset('modules/auth/thumb-charity-logo/'.$charityLogo[$key])}}"
                                                             alt=""/>
                                                    </div>
                                                    @endif
                                                    <div class="charity_name_dd">
                                                        {{ $value }}
                                                    </div>
                                                    <span class="value">{{$key}}</span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </dd>
                            </dl> -->

                            <!--select-->
                            
                            
                            <!--select-->
                    
                            <!--search-->
                            <div class="createCampaignForm">
                               <div class="formRow">
                                    <div class="secheading">Search Charities</div>
                                    <div class="inputBlock campaignname">
                                        
                                    <select style="width:100%" name="charity_id" id="charity_select">
                                        <option value="">Search charity</option>
                                        @foreach($charityList as $key => $value)
                                            <option value="{{$key}}" data-image="http://givebrite.dev/images/default-images/default_logo.png">{{ $value }}</option>
                                        @endforeach

                                    </select>


                                       <div class="text-danger"></div>
                                    </div>
                                </div>
                            </div>    
                            <!--search-->
                            
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="container">
                <div class="row">
                    <div class="buttonBlock col-md-12">
                        <button class="blue-btn-lg text-center" id="submit">Save and Continue
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            @stop
            @section('script')


                <script type='text/javascript'
                        src="{{ asset('js/jquery.maskedinput.js') }}"></script>
                <script type='text/javascript'
                        src="{!! asset('modules/campaigns/js/common.js').'?num='.mt_rand(1000000, 9999999) !!}"></script>
                <script type='text/javascript'
                        src="{!! asset('modules/campaigns/js/campaign-detail.js').'?num='.mt_rand(1000000, 9999999) !!}"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>        
                @if(isset($campaign))
                    {!! JsValidator::formRequest('Modules\Campaigns\Requests\CampaignDetailUpdateRequest','#campaign-detail') !!}
                @else
                    {!! JsValidator::formRequest('Modules\Campaigns\Requests\CampaignDetailCreateRequest','#campaign-detail') !!}
                @endif
                <script type='text/javascript'
                        src="{!! asset('modules/campaigns/js/detail-validation.js').'?num='.mt_rand(1000000, 9999999) !!}"></script>
                
                <script type="text/javascript">
                  
                  $('#charity_select').select2();

                  // function charitySelect (state) {
                  //     if (!state.id) { return state.text; }
                      
                  //     var stateimage = "http://givebrite.dev/images/default-images/default_logo.png";

                  //     var $state = $(
                  //       '<span><img width="50px" src="'+stateimage+'" class="img-flag" /> ' + state.text + '</span>'
                  //     );

                  //     return $state;
                  // };

                  // $('#charity_select').select2({
                  //       templateResult: charitySelect,
                  //       templateSelection:charitySelect
                  // });

                </script>
        
                <script type="text/javascript">

                    $(".dropdown img.flag").addClass("flagvisibility");
                    $(".dropdown dt a").click(function (e) {
                        e.preventDefault();
                        $(".dropdown dd ul").toggle();
                    });

                    $(".dropdown dd ul li a").click(function (e) {
                        e.preventDefault();
                        var text = $(this).html();
                        $(".dropdown dt a span").removeClass('dropdown-list');
                        $(".dropdown dt a span").html(text);
                        $(".dropdown dd ul").hide();
                        $("#charity_id").val(getSelectedValue("sample"));
                    });

                    function getSelectedValue(id) {
                        return $("#" + id).find("dt a span.value").html();
                    }

                    $(document).bind('click', function (e) {
                        
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown"))
                            $(".dropdown dd ul").hide();
                    });


                    // This will handle the toggle button.
                    $(".dropdown img.flag").toggleClass("flagvisibility");

                    $("#cmn-toggle-1").click(function () {
                        if ($("#cmn-toggle-1").prop('checked')) {
                            $("#pay_type").val('1');
                            $("#charity_list").show();
                            $('.switchselect').addClass('darkbg');
                            $('.switchselect').removeClass('lightbg');
                       
                        } else {
                            $("#pay_type").val('0');
                            $("#charity_id").val('');
                            $("#charity_list").hide();
                            $('.switchselect').addClass('lightbg');
                            $('.switchselect').removeClass('darkbg');
                        }
                    });

                    // This will handle the countdown of Campaign Title
                    var data = $(".lengthlimt").html();


                    // Will evaulate recursive input

                    $("#campaing_name").keydown(function (e) {
                        if (this.value.length == 35) {
                            if (e.keyCode === 8) {
                                console.log('Backspace');
                            } else {
                                e.preventDefault();
                            }
                        } else {
                            this.value = this.value.substring(0, 35);
                        }

                        if (Number($(this).val().length) < 35) {
                            var newdata = (data - Number($(this).val().length));
                            $(".lengthlimt").html(newdata);
                        } else {
                            $(".lengthlimt").html('0');

                        }
                    });

                </script>
@stop
