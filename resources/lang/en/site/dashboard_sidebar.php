<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'menus' => [
        'my_account_title' => 'My Account',
        'campaigns_title' => 'Campaigns',
        'tools_title' => 'Tools',
        'my_account' => [
            //'account_active' => '<span class="account-active">Account Active</span> <br><span class="text-fade"> Current Account Status</span>',
            'account_active' => '<span class="account-active">Dashboard</span> <br><span class="text-fade"> Account Active</span>',
            'account' => '<span class="account-active">Dashboard</span> <br><span class="text-fade"> Account Deactive</span>',
            'stripe' => '<span class="stripe_dash">Stripe Settings</span>',
            'profile' => 'Profile',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'logout' => 'Logout',
            'stripe' => 'Stripe Settings',
        ],
        'campaigns' => [
            'my_campaigns' => 'My Campaigns',
            'team_pages' => 'Team Pages',
            'donations' => 'Donations',
            'comments' => 'Comments',
            'rewards' => 'Rewards',
            'insights' => 'Insights',
        ],
        'tools' => [
            'events' => 'Events',
            'social_media_marketing' => 'Social Media Marketing',
            'send_texts' => 'Send Texts',
            'send_emails' => 'Send Emails',
            'order_print_material' => 'Order Print Material',
        ],
    ],
];
