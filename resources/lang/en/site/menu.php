<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return [
    'menus' => [
        'dashboard' => 'Dashboard',
        'causes' => 'Causes',
        'charities' => 'Charities',
        'sign_up' => 'Sign Up',
        'how_it_works' => 'How It Works',
        'campaign' => 'Campaign',
        'about_us' => 'About Us',
        'sign_up' => 'Sign Up',
        'how_it_works' => 'How It Works',
        'campaign' => 'Campaign',
        'about_us' => 'About Us',
        
        'footer1' => [
            'learn_more' => 'Learn More',
            'about' => 'About',
            'fundraise_online' => 'Fundraise Online',
            'fundrasing_ideas' => 'Fundrasing Ideas'
        ],
        'footer2' => [
            'support' => 'Support',
            // 'about' => 'About',
            'faq' => 'FAQ',
            'privacy_policy' => 'Privacy Policy',
            'terms_conditions' => 'Terms &amp; Conditions'
        ],
        'footer3' => [
            'support' => 'Support',
            'about' => 'About',
            'faq' => 'FAQ',
            'privacy_policy' => 'Privacy Policy',
            'terms_conditions' => 'Terms &amp; Conditions'
        ],
        'footer4' => [
            'address' => 'Address',
        ],

    ],
    'sidebar1' => [
        'how_it_works' => 'How It Works',
        'success_stories' => 'Success Stories',
        'pricing' => 'Pricing',
        'faq' => 'FAQ',
        'contact_us' => 'Contact Us',
    ]
];

