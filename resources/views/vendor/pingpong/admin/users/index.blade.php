@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'All Users' !!} ({!! $users->count() !!})
		&middot;
		<small>{!! link_to_route('admin.users.create', 'Add New') !!}</small>
		<link href="{{asset('site/css/backend.css')}}" rel="stylesheet" type="text/css">
	</h1>
@stop

@section('content')

	@if(isset($search))
		@include('admin::users.search-form')
	@endif

	<table class="table">
		<thead>
			<th>No</th>
			<th>Name</th>
			<th>Email</th>
			<th>Created At</th>
			<th>Status</th>
			<th class="text-center">Action</th>
		</thead>
		<tbody>
			@foreach ($users as $user)
			<tr>
				<td>{!! $no !!}</td>
				<td>{!! $user->name !!}</td>
				<td>{!! $user->email !!}</td>
				<td>{!! $user->created_at !!}</td>
				<td>
					@if($user->is_activated)
						<a class="btn active " href="{{route('charity-user-deactivate', $user->id)}}"><span class="icon-publish"></span></a>
					@else
						<a class="btn active " href="{{route('charity-user-activate', $user->id)}}"><span class="icon-unpublish"></span></a>
					@endif
				</td>
				<td class="text-center">
					<a href="{!! route('admin.users.edit', $user->id) !!}">Edit</a>
					&middot;
                                        <a href="users/{!! $user->id !!}">Show</a>
					&middot;
					@include('admin::partials.modal', ['data' => $user, 'name' => 'users'])
				</td>
			</tr>
			<?php $no++ ;?>
			@endforeach
		</tbody>
	</table>

	<div class="text-center">
		{!! pagination_links($users) !!}
	</div>
@stop
