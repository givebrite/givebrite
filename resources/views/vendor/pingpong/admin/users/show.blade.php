@extends($layout)

@section('content-header')
<h1>
    {!! $user->name !!}'s Profile
    &middot;
    <small>{!! link_to_route('admin.users.index', 'Back') !!}</small>
</h1>
@stop

@section('content')

@if(!empty($profile))
    <div class="form-row col-sm-4">
        <?php $imagePath=isset($profile->profile_pic) && !empty($profile->profile_pic) ? ((substr( $profile->profile_pic, 0, 4 ) === "http") ? $profile->profile_pic : ( (File::exists($base_path.$profile->profile_pic )) ? asset($base_path.$profile->profile_pic) : asset('images/default-images/default_profile.png')))  :  asset('images/default-images/default_profile.png'); ?>

        <img id="remove-profile-image" src="{{$imagePath}}">

    </div>
@endif



    <table class="table">
            <tbody>
                    <tr>
                            <td>Name: </td>
                            <td>{!! $user->name !!}</td>
                    </tr>
                    <tr>
                            <td>Email: </td>
                            <td>{!! $user->email !!}</td>
                            
                    </tr>
                    <tr>
                            <td>Created: </td>
                            <td>{!! $user->created_at !!}</td>
                            
                    </tr>
                    @if(!empty($profile))
                    <tr>
                            <td>Address: </td>
                            <td>{!! $profile->address !!}</td>
                            
                    </tr>
                    <tr>
                            <td>Locality: </td>
                            <td>{!! $profile->locality !!}</td>
                            
                    </tr>
                    <tr>
                            <td>Postcode: </td>
                            <td>{!! $profile->postcode !!}</td>
                            
                    </tr>
                    <tr>
                            <td>Country: </td>
                            <td>{!! $profile->country !!}</td>
                            
                    </tr>
                        
                    @endif
            </tbody>
    </table>
    @stop
