<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>:: Welcome to Site ::</title>
    <style type="text/css">
        body {
            background: #f6f5f2;
            font-family: Arial, Helvetica, sans-serif;
            color: #b8b8b8;
            font-size: 14px;
        }

        .setW100P {
            width: 650px;
        }

        * {
            margin: 0px;
            padding: 0px;
            box-sizing: border-box;
        }

        .ExternalClass * {
            line-height: 121%;
        }

        table {
            border-collapse: collapse;
        }

        table td {
            border-collapse: collapse;
        }

        img {
            border: none;
            outline: none;
        }

        @media (max-width: 750px) {
            .setW100P {
                width: 493px !important;
            }
        }

        @media (max-width: 500px) {
            .setW100P {
                width: 80% !important;
            }
        }
    </style>
</head>
<body>
<div align="center"
     style="background: #f6f5f2; font-family: Arial, Helvetica, sans-serif; color: #b8b8b8; font-size: 14px;">
    <table style="border:none; font-size: 14px; width:650px;" align="center">
        <tr>
            <td align="center" valign="top" style="padding-top:30px; padding-bottom:60px;">
                <img src="{{$message->embed(base_path().'/public/site/images/email-logo.png')}}"/></td>
        </tr>
        <tr>
            <td align="center" valign="top" bgcolor="#ffffff" style="border-radius:3px;">
                <table align="left" class="table-left-padd" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-bottom:1px solid #f2f2f2; padding:29px 0px 10px 40px;">
                            <!--1px solid #f2f2f2-->
                            <img src="{{$message->embed(base_path().'/public/site/images/yip-img.png')}}"/></td>
                    </tr>
                    <tr>
                        <td style="padding:15px 20px 20px 40px;"> You are almost ready to start receiving donations
                            TODAY!<br/>
                            <br/>
                            Stripe is a fantastic, easy &amp; secure way to start receiving donations. Simply click on
                            the link below to complete your setup, it only takes 60 seconds! <br/><br/>
                            <span style="background:#00c7e5; display:block; width:153px; height:36px; padding:8px 0px; border-radius:3px; text-align:center;"><a
                                        href="{{$url}}"
                                        style="color:#fff; text-decoration:none;">Complete Setup</a></span>
                            <br/><br/>

                            Any issues please get in touch with one of our dedicated team members at <a
                                    href="emailto:support@givebrite.com" style="color:#b8b8b8; text-decoration:none;">support@givebrite
                                .com</a><br/>
                            <br/>
                            Always here to help.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-top:25px;" align="center"> GiveBrite HQ<br/>
                Abbey House, 10 Abbey Hills Road, <br/>
                Greater Manchester, OL8 2BS
            </td>
        </tr>
    </table>
</div>
</body>
</html>
