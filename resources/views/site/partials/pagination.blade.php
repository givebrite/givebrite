<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Number of links to show. Odd numbers work better
$linkCount = 4;
$pageCount = $paginator->lastPage();

if ($pageCount > 1) {
    $currentPage = $paginator->currentPage();
    $pagesEitherWay = floor($linkCount / 2);
    $paginationHtml = '<ul class="pagination1 col-sm-12">';

    // Previous item
    $previousDisabled = $currentPage == 1 ? 'disabled' : '';// May Be Removed
    // Remove Prev Button If locaion if first page
//    if($currentPage>1){
//        $paginationHtml .= '<li class="page-item">
//                            <a class="page-link" href="'.$paginator->url($currentPage - 1).'" aria-label="Previous">
//                                <span aria-hidden="true">Prev</span>
//                                <span class="sr-only">Previous</span>
//                            </a>
//                        </li>';
//    }
    // Set the first and last pages
    $startPage = ($currentPage - $pagesEitherWay) < 1 ? 1 : $currentPage - $pagesEitherWay;
    $endPage = ($currentPage + $pagesEitherWay) > $pageCount ? $pageCount : ($currentPage + $pagesEitherWay);

    // Alter if the start is too close to the end of the list
    if ($startPage > $pageCount - $linkCount) {
        $startPage = ($pageCount - $linkCount) + 1;
        $endPage = $pageCount;
    }

    // Alter if the end is too close to the start of the list
    if ($endPage <= $linkCount) {
        $startPage = 1;
        $endPage = $linkCount < $pageCount ? $linkCount : $pageCount;
    }

    // Loop through and collect
    for ($i = $startPage; $i <= $endPage; $i++) {
        $disabledClass = $i == $currentPage ? 'disabled' : '';
        $paginationHtml .= '<li class="page-item ' . $disabledClass . '">
                                <a class="page-link" href="' . $paginator->url($i) . '">' . $i . '</a>
                            </li>';
    }

    // Next item
    $nextDisabled = $currentPage == $pageCount ? 'disabled' : '';
    // Remove Next Button If Current Page Is Last
//    if($currentPage < $pageCount){
//        $paginationHtml .= '<li class="page-item">
//                            <a class="page-link" href="'.$paginator->url($currentPage + 1).'" aria-label="Next">
//                                <span aria-hidden="true">Next</span>
//                                <span class="sr-only">Next</span>
//                            </a>
//                        </li>';
//    }
    $paginationHtml .= '</ul>';

    echo $paginationHtml;
}