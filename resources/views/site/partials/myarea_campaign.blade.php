<?php $campaign_base_path = Config::get('config.thumb_campaign_upload_path');
?>

@if (!empty($areaCampaigns) && count($areaCampaigns)>0 )
<div class="container-fluid"  id="area-page-{{!empty($page) ? $page : '0'}}">
<ul class="row listEplore" >          
    @foreach ($areaCampaigns as $campaign)
    <li class="col-md-4 col-sm-6 col-xs-12">                            
        <a href="{{URL::route('campaign.show', $campaign->slug)}}"><div class="imgContainer" style="background: rgba(0, 0, 0, 0) url('{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($campaign_base_path.$campaign->campaign_image ) ?  asset($campaign_base_path.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}') no-repeat scroll center center / cover;">
        </div></a>
        <div class="listDetails clearfix">
            <div class="detialsLeft text-capitalize">
                <div class="price">
                    {{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}
                </div>
                <div class="type">
                    <a href="{{URL::route('campaign.show', $campaign->slug)}}">   {{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 10, $end = '...') :''}}  </a>                                      
                </div>
                <div class="location">
                    {{Helper::showAddress($campaign->cityName,$campaign->locationName,15)}}
                </div>
            </div>
            <div class="progressOuter inherit" style="width:70px;height:70px;">
                <div class="percent" style="width:70px;height:70px;">
                    <p style="display:none;">{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}</p>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>
</div>    
<div class="paginationOuter" id="pagination-div">
    <nav>
        @include('site/partials/pagination', ['paginator' => $areaCampaigns])
    </nav>
</div> 
@else 
<div class="text-center">No Record found</div>
@endif
