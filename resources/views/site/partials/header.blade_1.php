<div class="innerHeader">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="{{ asset('site/images/color-logo.png') }}" alt="givebrite" />
                        </a>
                    </div>

                    <div id="navbar" class="navbar-collapse collapse">
                        <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("header2"); ?>

                        @if ($menus->count()>0)
                        <ul class="nav navbar-nav">
                            @foreach ($menus as $menu)
                            <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                            @endforeach                            
                        </ul>
                        @endif

                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('/campaigns/create') }}" class="green-btn">Start Campaign</a></li>
                            <li class="dropdown logOut {{ (!Auth::check()) ? 'custom-dropdown-logout' : '' }}">

                                <?php
                                $user_name = "User";
                                
                                if (Helper::checkIsAdmin()) {
                                    if (!(Session::has('user_session_data'))) {
                                        Helper::setUserSessionBlade();
                                    }
                                }

                                if (Auth::check()) {
                                    if (Session::get('user_session_data')['first_name']) {
                                        $user_name = Session::get('user_session_data')['first_name'];
                                    } else if (Session::get('user_session_data')['charity_name']) {
                                        $user_name = Session::get('user_session_data')['charity_name'];
                                    }
                                }
                                ?>
                                <a href="{{ Auth::check() ? "#" : url('/register') }}" class="dropdown-toggle" data-toggle="{{ Auth::check() ? "dropdown" : "" }}" role="button" aria-haspopup="true" aria-expanded="false"><span class="{{ (Auth::check()) ? 'imgDP' : '' }}"></span>{{ Auth::check() ? $user_name : 'Sign Up' }} <span class="{{ (Auth::check()) ? "caret" : "" }}"><img src='{{Helper::showUserDp()}}' class='loginDp'></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ url('/dashboard') }}"><i class="fa fa-cog" aria-hidden="true"></i>My Account</a></li>
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i>Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </nav>
            </div> 
        </div>
    </div>
</div>
