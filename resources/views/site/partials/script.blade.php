<script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type='text/javascript' src="{{ asset('site/js/pace.js') }}"></script>
<script type='text/javascript' src="{{ asset('site/js/pace-start.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type='text/javascript' src="{{ asset('site/js/bootstrap.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('site/js/common.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src='{{ asset('modules/dashboard/js/common.js').'?num='.mt_rand(1000000, 9999999) }}'></script>
<script src='{{ asset('packages/pingpong/admin/adminlte/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}'></script>
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset('js/jsvalidation.js').'?num='.mt_rand(1000000, 9999999)}}"></script>
<!--
 <script src="{!! asset('modules/campaigns/js/livequery.js') !!}"></script>
-->
<script type="text/javascript" src="{{ asset('site/js/custom-button.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
<script>

        $('.progress-button').progressInitialize();
// Listen for clicks on the first three buttons, and start
        // the progress animations

        $('.progress-button').click(function(e){
        //e.preventDefault();

        // This function will show a progress meter for
        // the specified amount of time

        $(this).progressTimed(0.001);
        });</script>
<script>

            $(document).mouseup(function(e) 
            {
                var container = $(".list-group");

                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0) 
                {
                    container.fadeOut();
                }
            });

            $(function () {

            $('#slide-submenu').on('click', function () {
            $(this).closest('.list-group').css('right', '0');
            $(this).closest('.list-group').fadeOut('slide', function () {
            $('.mini-submenu').fadeIn();
            });
            });
            $('.mini-submenu').on('click', function () {
            $(this).next('.list-group').toggle('slide');
            $('.mini-submenu').hide();
            });
            $("#search-header").click(function () {
            $('html').one('click', function () {
            $(".search-wrap").show();
            $("#search-txt").focus();
            });
            });
            $(".search-wrap").focusout(function () {
            $(".search-wrap").hide();
            });
            });</script>
<script>
            $("#searchSubmit").submit(function(e){
            e.preventDefault();
            var text_val = $("#search-txt").val();
            search_url = site_url + '/campaigns/type/cause?keyword=' + text_val;
            window.location = search_url;
            });
</script>