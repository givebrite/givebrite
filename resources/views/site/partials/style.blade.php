<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<!-- Bootstrap -->
<link href="{{ asset('site/favicon.ico') }}" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link href="{{ asset('site/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('site/css/custom.css').'?num='.mt_rand(1000000, 9999999) }}" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" type="text/css">
<link href="{{ asset('packages/pingpong/admin/adminlte/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('site/css/pace.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('site/css/changes.css').'?num='.mt_rand(1000000, 9999999) }}" rel="stylesheet" type="text/css">
<link href="{{ asset('site/css/custom_style.css').'?num='.mt_rand(1000000, 9999999) }}" rel="stylesheet" type="text/css">
<link href="{{ asset('site/css/custom-button.css').'?num='.mt_rand(1000000, 9999999) }}" rel="stylesheet" type="text/css">
<link href="{{asset('site/css/responsivenew.css')}}" rel="stylesheet" type="text/css">