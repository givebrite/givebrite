<input type="hidden" id="get-updates-url" value="{{URL::route('campaign.update.list',$campaign->slug)}}">
@if(!empty($campaignUpdates) && count($campaignUpdates)>0)
    <div class="charityList">
        <div class="innerShadowBox" id="info">
            <?php $i = 1; ?>
            @foreach($campaignUpdates as $update)
                <div class='update <?php echo $i % 2 ? "odd" : "even" ?>'>
                <?php $i++; ?>
                <!--            <div class="postPublish">Published by {{ !empty($update->name) ? ucfirst($update->name) : "" }} - {{ !empty($update->created_at) ? Helper::showDate($update->created_at) : ''  }}</div>-->
                    <div class="postPublish">
                        {{ Carbon\Carbon::parse(!empty($update->created_at) ? $update->created_at: '')->diffForHumans() }}
                    </div>

                    <div class="postdescription readmore-content">{!! isset($update->updates)? $update->updates :'' !!}</div>
                </div>
            @endforeach
            <div class="paginationOuter" id="update-pagination-div">
                <nav>
                    @include('site/partials/pagination', ['paginator' => $campaignUpdates])
                </nav>
            </div>
        </div>
    </div>
@else
    <div class="innerShadowBox noupdates">
        The campaign creator has not posted any updates
    </div>
@endif

