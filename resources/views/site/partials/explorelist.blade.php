
<div class = "col-lg-3">
    <div class = "explore-list">
        <div class = "colHeading">Explore</div>
        @if ( count($categoriesArray)>0 )
        <ul class="explore-custom">
            @foreach ($categoriesArray as $key => $category)
            <li class = "{{ strtolower($category).'-sec' }}"> <a href = "{{ URL::route('campaign-by-category', $key)}}">{{ $category }}</a></li>
            @endforeach
        </ul>
        @endif
    </div>
</div>