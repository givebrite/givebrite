<div role="tabpanel" class="tab-pane" id="almostfunded">
    @if (!empty($almostFunded) &&  count($almostFunded)>0 )
    <div class="campain-main">
        <div class="row">
            @foreach ($almostFunded as $campaign)
            <div class="col-sm-4 col-md-4 col-lg-4">
                <a href="{{URL::route('campaign.show', $campaign->slug)}}">
                    <div class="campain-list">
                        <div class="campain-img-wrap">
                            @if(file_exists(public_path('/images/thumb-campaigns/'.$campaign->campaign_image)))
                            <img src="{{ asset('/images/thumb-campaigns/'.$campaign->campaign_image) }}" alt="img" />
                            @else
                            <img src="{{ asset('/images/default-images/default-campaign-new.jpg') }}" alt="img" />
                            @endif
                            <h3>{{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 35, $end = '...') :''}}</h3>
                            <div class="campaign-title-bg"></div>
                            <div class="campaign-progress" style="width:{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}%;"></div>
                        </div>
                        <div class="conpain-des">
                            <div class="price-section"> <span class="price-big">£{{Helper::amountRaised($campaign->id)}}</span><span class="price-small">of £{{$campaign->goal_amount_monetary}} raised</span></div>
                            <p class="conpain-text-detail">{{ strip_tags($campaign->campaign_description) }}</p>
                        </div>
                        <div class="campain-bottom">
                            <div class="campain-left-img">
                                @if(is_file(public_path('modules/auth/thumb-profile-images/'.$campaign->profile_pic)))
                                <img src="{{ asset('modules/auth/thumb-profile-images/'.$campaign->profile_pic) }}" alt="img" />
                                @else
                                <img src="{{ asset('images/userDP.png') }}" alt="img" />
                                @endif
                            </div>
                            <div class="comapin-bottem-detail">
                                <h5>{{$campaign->name}}</h5>
                                <div class="category-list"><span class="fa fa-tags"></span>{{$campaign->CampCategory}}</div>
                                <div class="category-list"><span class="fa fa-map-marker"></span> {{$campaign->locationName}}</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            <div class="pagination-wrap">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        @include('site/partials/pagination', ['paginator' => $campaignsList])
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    @else
    <div class="text-center">  No record found</div>
    @endif
</div>