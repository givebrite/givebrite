<!-- REVOLUTION JS FILES -->
<script src="{{ asset('slider/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('slider/js/jquery.themepunch.revolution.min.js') }}"></script>

 <!--  SLIDER REVOLUTION 5.0 EXTENSIONS  
    (Load Extensions only on Local File Systems ! 
     The following part can be removed on Server for On Demand Loading)  -->	
<script src="{{ asset('slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
