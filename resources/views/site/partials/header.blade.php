<div class="innerHeader {{ isset($logo) ? 'light' : 'dark' }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="header-additional-links">
                    <ul class="nav nav-bar">
                        <li><a href="{{ url('articles/how-it-works') }}">How it works</a></li>
                        <li><a href="{{ url('about-us') }}">About</a></li>
                        <li><a href="{{ url('register') }}">@if (Auth::check()) <a href="{{ url('dashboard') }}">Dashboard</a> @else Register @endif</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row header-row">
            <div class="col-lg-2 mobile-nav">
                <Div class="row">
                    <div class="col-xs-5 col-lg-12">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            @if (isset($logo))
                                <img src="{{ asset('site/images/logo_white.png') }}" alt="Give Brite" />
                            @else
                                <img src="{{ asset('site/images/logo_final.png') }}" alt="Give Brite" />
                            @endif
                        </a>

                    </div>
                    <div class="col-xs-7 hidden-lg">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>

            </div>

            <div class="col-lg-10 col-xs-12">
                <div id="navbar" class="row navbar-collapse collapse">

                    <div class="col-lg-6">
                        <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("header1");?>

                        @if ($menus->count()>0)
                        <nav class="navbar navbar-default">
                            <ul class="nav navbar-nav">
                                @foreach ($menus as $menu)
                                <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                                @endforeach

                            </ul>
                        </nav>
                        @endif
                    </div>

                    <div class="col-lg-6">

                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('/campaigns/create') }}" class="green-btn">Start Campaign</a></li>
                            <li class="drop {{ (!Auth::check()) ? 'dropdown-logout' : '' }}">

                                <?php
$user_name = "User";

if (Helper::checkIsAdmin()) {
	if (!(Session::has('user_session_data'))) {
		Helper::setUserSessionBlade();
	}
}

if (Auth::check()) {
	if (Session::get('user_session_data')['first_name']) {
		$user_name = Session::get('user_session_data')['first_name'];
	} else if (Session::get('user_session_data')['charity_name']) {
		$user_name = Session::get('user_session_data')['charity_name'];
	}
}
?>

                                <a href="{{ Auth::check() ? "#" : url('/login/') }}" class="blue-btn dropdown-toggle" data-toggle="{{ Auth::check() ? "dropdown" : "" }}" role="button" aria-haspopup="true" aria-expanded="false"><span class="{{ (Auth::check()) ? 'imgDP' : '' }}"></span>{{ Auth::check() ? $user_name : 'Login' }} <span class="{{ (Auth::check()) ? "caret" : "" }}"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ url('/dashboard') }}"><i class="fa fa-cog" aria-hidden="true"></i>My Account</a></li>
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i>Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div></div>
            </div>
        </div>
    </div>
</div>

