<?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("about-sidebar"); ?>

@if ($menus->count()>0)
<ul class="shortLinks">
    @foreach ($menus as $menu)
    <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
    @endforeach                            
</ul>
@endif
<!--
<ul class="shortLinks">
    <li><a href="{{ url('/how-it-works') }}">{{ trans('site/menu.sidebar1.how_it_works') }}</a></li>
    <li><a href="{{ url('/') }}">{{ trans('site/menu.sidebar1.success_stories') }}</a></li>
    <li><a href="{{ url('/') }}">{{ trans('site/menu.sidebar1.pricing') }}</a></li>
    <li><a href="{{ url('/faq') }}">{{ trans('site/menu.sidebar1.faq') }}</a></li>
    <li><a href="{{ url('/') }}">{{ trans('site/menu.sidebar1.contact_us') }}</a></li>
    <li><a href="{{ url('/how-it-works') }}">{{ trans('site/menu.sidebar1.how_it_works') }}</a></li>
    <li><a href="{{ url('/') }}">{{ trans('site/menu.sidebar1.success_stories') }}</a></li>
    <li><a href="{{ url('/') }}">{{ trans('site/menu.sidebar1.pricing') }}</a></li>
    <li><a href="{{ url('/faq') }}">{{ trans('site/menu.sidebar1.faq') }}</a></li>
    <li><a href="{{ url('/') }}">{{ trans('site/menu.sidebar1.contact_us') }}</a></li>
</ul>-->