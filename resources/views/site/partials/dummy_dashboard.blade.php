<div class="row">
    <div class="col-md-3">
        <div class="listBox">
            <h3>My Account</h3>
            <div class="innerListbox">
                <ul>
                    <li class="account-sec">
                        <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> Account Active</a>
                    </li>
                    <li class="profile-sec">
                        <a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Profile</a>
                    </li>
                    <li class="face-sec">
                        <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</a>
                    </li>
                    <li class="twitter-sec">
                        <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="listBox">
            <h3>Campaigns</h3>
            <div class="innerListbox">
                <ul>
                    <li class="camp-sec">
                        <a href="#">My Campaigns</a>
                    </li>
                    <li class="dona-sec">
                        <a href="#">Donations</a>
                    </li>
                    <li class="comm-sec">
                        <a href="#">Comments</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-9 innerRightCol">
        <h2 class="heading">Dashboard</h2>
        <div class="subHeading">Current Campaigns</div>
        <div class="campaignList">
            <ul class="clearfix listEplore">
                <li>
                    <div class="imgContainer"></div>
                    <div class="listDetails clearfix">
                        <div class="detialsLeft">
                            <div class="price">£50,000</div>
                            <div class="type">Campaign Title</div>
                            <div class="location">Location, City</div>
                        </div>
                        <div class="detialsRight">
                            <div class="valuePercentage">57%</div>
                        </div>
                    </div>
                    <div class="btn-block">
                        <button class="sm-blue-btn">Uptate</button>
                    </div>
                </li>
                <li>
                    <div class="imgContainer"></div>
                    <div class="listDetails clearfix">
                        <div class="detialsLeft">
                            <div class="price">£50,000</div>
                            <div class="type">Campaign Title</div>
                            <div class="location">Location, City</div>
                        </div>
                        <div class="detialsRight">
                            <div class="valuePercentage">57%</div>
                        </div>
                    </div>
                    <div class="btn-block">
                        <button class="sm-blue-btn">Uptate</button>
                    </div>
                </li>
                <li>
                    <div class="imgContainer"></div>
                    <div class="listDetails clearfix">
                        <div class="detialsLeft">
                            <div class="price">£50,000</div>
                            <div class="type">Campaign Title</div>
                            <div class="location">Location, City</div>
                        </div>
                        <div class="detialsRight">
                            <div class="valuePercentage">57%</div>
                        </div>
                    </div>
                    <div class="btn-block">
                        <button class="sm-blue-btn">Uptate</button>
                    </div>
                </li>
            </ul>
        </div>
        <div class="subHeading">My Account</div>
        <div class="row">
            <div class="col-sm-3 text-center">
                <div class="imgBlock">
                    <a href="#"><img src="{{ asset('site/images/profile.png') }}" alt="Profile" /></a>
                </div>
                <div class="iconText">Profile</div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="imgBlock">
                    <a href="#"><img src="{{ asset('site/images/facebook-lg.png') }}" alt="Profile" /></a>
                </div>
                <div class="iconText">Facebook</div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="imgBlock">
                    <a href="#"> <img src="{{ asset('site/images/twitter-lg.png') }}" alt="Profile" /></a>
                </div>
                <div class="iconText">Twitter</div>
            </div>
        </div>
        <div class="subHeading">Campaigns</div>
        <div class="row">
            <div class="col-sm-3 text-center">
                <div class="imgBlock">
                    <a href="#"> <img src="{{ asset('site/images/campaign-lg.png') }}" alt="Profile" /></a>
                </div>
                <div class="iconText">Campaigns</div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="imgBlock">
                    <a href="#"> <img src="{{ asset('site/images/donation-lg.png') }}" alt="Profile" /></a>
                </div>
                <div class="iconText">Donations</div>
            </div>
            <div class="col-sm-3 text-center">
                <div class="imgBlock">
                    <a href="#">  <img src="{{ asset('site/images/comment-lg.png') }}" alt="Profile" /></a>
                </div>
                <div class="iconText">Comments</div>
            </div>
        </div>
    </div>
</div>