<div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
	<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
	<div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
        <ul class="slides">	<!-- SLIDE  -->
            @if(!empty($activeBanners) && count($activeBanners) >0)
            @foreach($activeBanners as $banner)
           <li data-index="rs-{{$banner->id}}" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{isset($banner->image) && !empty($banner->image) && File::exists($bannerBasePath.$banner->image ) ?  asset($bannerBasePath.$banner->image)  : asset('images/default-images/default-campaign.jpg')}}"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="{{!empty($banner->name) ? $banner->name : ''}}" data-description="">
                <!-- MAIN IMAGE -->
                 @if(!empty($banner->cta)) <a href='{{ $banner->cta }}' target='{{!empty($banner->cta_type) && $banner->cta_type =="external" ? "blank" : ""}}'> @endif
                <img src="{{isset($banner->image) && !empty($banner->image) && File::exists($bannerBasePath.$banner->image ) ?  asset($bannerBasePath.$banner->image)  : asset('images/default-images/default-campaign.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
                     id="slide-{{$banner->id}}-layer-1" 
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                data-fontsize="['70','70','70','45']"
                    data-lineheight="['70','70','70','50']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"

                     data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                    data-start="1000" 
                    data-splitin="chars" 
                    data-splitout="none" 
                    data-responsive_offset="on" 

                    data-elementdelay="0.05" 

                    style="z-index: 5; white-space: nowrap;"> 
                    The Power of Giving
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" 
                     id="slide-{{$banner->id}}-layer-4" 
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                     data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" 
                                data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"

                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                    data-start="1500" 
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on" 


                    style="z-index: 6; white-space: nowrap;">
                    Make It Happen 
                
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" 
                    id="slide-{{$banner->id}}-layer-8" 
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['middle','middle','middle','middle']" data-voffset="['98','98','98','98']" 
                                data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-style_hover="cursor:default;"

                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" 
                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                    data-start="2000" 
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on" 


                    style="z-index: 3; white-space: nowrap;">
                    <a href="{{ URL::route('campaigns.create') }}" class="lt-green-lg mobile-btn">Launch Your Project</a>
                </div>
                @if(!empty($banner->cta))</a>@endif
			</li>
            @endforeach
            @endif
        </ul>    
    </div>
</div>