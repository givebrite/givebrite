<header class="fixedHeader">
    <div class="inner-header">
        <div class="nav-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                  <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>-->
                                    <a class="navbar-brand" href="{{url()}}"> <img src="{{ asset('site/images/inner-logo.png') }}" alt="Give Brite" /></a> </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->

                                <div class="navbar-collapse navbar-right">
                                <div class="header-additional-links">
                                    <ul class="nav nav-bar">
                                        <li><a href="{{url('/articles/how-it-works')}}">How it works</a></li>
                                        <li><a href="{{url('/about-us')}}">About</a></li>
                                        @if(Auth::check())
                                            <li><a href="{{url('/dashboard')}}"> Dashboard </a></li>
                                        @else
                                            <li><a href="{{url('/register')}}"> Register </a></li>
                                        @endif
                                    </ul>
                                </div>
                                    <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("header1");?>
                                    @if ($menus->count()>0)
                                    @if(Auth::check())
	                                    <ul class="nav navbar-nav login-menu">
                                    @else
                                    	<ul class="nav navbar-nav">
                                    @endif
                                        @foreach ($menus as $menu)
                                            <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    <div class="menu-toggle-wrap">

                                        <div class="mini-submenu"> <span class="menu-text">Menu</span> <span class="fa fa-bars"></span> </div>
                                        <div class="list-group menu-open-right" style="display:none;">
                                            <div class="menu-open-head">
                                                <div class="open-menu-head">
                                                    <ul class="header-right-nav">
                                                        <li><a href="{{url('/campaigns/create')}}">Start Fundraising</a></li>
                                                    </ul>
                                                    <span class="pull-right" id="slide-submenu"> <i class="fa fa-times"></i> </span> </div>
                                            </div>
                                            <div class="menu-right-menu">
                                                <ul>
                                                    @if(Auth::check())
                                                    <li><a href="{{url('/dashboard')}}">Welcome, {{Auth::user()->name}}</a></li>
                                                    <li><a href="{{url('/logout')}}">Logout</a></li>
                                                    @else
                                                    <li><a href="{{url('/login')}}">Login</a></li>
                                                    <li><a href="{{url('/register')}}">Register</a></li>
                                                    @endif
                                                    <li><a href="{{url('/campaigns/type/cause')}}">Search Campaign</a></li>
                                                    <li><a href="{{url('/charity')}}">Find a Charity</a></li>
                                                    <li><a href="{{url('/contact')}}">Contact Us</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-header">
                                        <span class="fa fa-search" id="search-header"></span>
                                         @if(!Auth::check())
                                        <a class="btn btn-success" href="{{url('/login')}}">Login</a>
                                    @endif
                                        <div class="search-wrap">
                                            <form id="searchSubmit">
                                                <div id="imaginary_container">
                                                    <div class="input-group stylish-input-group">
                                                        <input type="text" class="form-control"  placeholder="Search" id="search-txt" >
                                                        <span class="input-group-addon">
                                                            <button type="submit"> <span class="glyphicon glyphicon-search"></span> </button>
                                                        </span> </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.navbar-collapse -->
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>