<!-- LOADING FONTS AND ICONS -->
<link href="https://fonts.googleapis.com/css?family=Raleway:500,800" rel="stylesheet" property="stylesheet" type="text/css" media="all" />

<link href="{{ asset('slider/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" rel="stylesheet">
<link href="{{ asset('slider/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

<!-- REVOLUTION STYLE SHEETS -->
<link href="{{ asset('slider/css/settings.css') }}" rel="stylesheet">
<!-- REVOLUTION LAYERS STYLES -->
<link href="{{ asset('slider/css/layers.css') }}" rel="stylesheet">

<!-- REVOLUTION NAVIGATION STYLES -->
<link href="{{ asset('slider/css/navigation.css') }}" rel="stylesheet">
