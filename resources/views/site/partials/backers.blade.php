<div class="backersBlock listBox borderRadiusZero">
    <input type="hidden" id="get-backers-url" value="{{URL::route('campaign.donor.list',$campaign->slug)}}">
    <h3 class="">Backers</h3>
    <ul class="backersList">
        @if(!empty($backersList) && count($backersList)>0)
        @foreach($backersList as $backer)
        <li class="item">
            <div class="heading">
                <div class="tiltleBox">
                    <div class="bigTitle" style='color:{{ !empty($campaign->themeValue) ? $campaign->themeValue : '' }}'>{{!empty($backer->name) && ($backer->is_anonymous== 0) ? str_limit(ucfirst($backer->name) , $limit = 10, $end = '...') : 'Anonymous'}}</div>
                    <div class="subTitle">{{Helper::getElapsedTime($backer->created_at)}}</div>
                </div>
                <div class="amount">
                    <p>
                        {{Helper::showMoney(!empty($backer->donation_amount_monetory) ? $backer->donation_amount_monetory : '',$campaign->campaign_currency)}}
                    </p>
                    <p>
                        @if(!empty($backer->is_giftaid))
                        <span class="giftAid" style='color:{{ !empty($campaign->themeValue) ? $campaign->themeValue : '' }}'>    
                            <br/>{{"+" }}
                           {{Helper::showMoney(!empty($backer->donation_amount_monetory) ? ($backer->donation_amount_monetory*25)/100 : '',$campaign->campaign_currency,0)}}
                            {{"Gift Aid"}}
                        </span>
                        @endif
                    </p>
                </div>
            </div>
            <div class="description readmore-content large-word">
                <p>{{!empty($backer->content) ? $backer->content : ''}}</p>
            </div>
        </li>
        @endforeach
        @else
        <li class="item">No Record Found.</li>
        @endif
    </ul>
    @if(!empty($backersList) && count($backersList)>0)
    <div class="paginationOuter text-center" id="backers-pagination-div">
        <nav>
            @include('site/partials/pagination', ['paginator' => $backersList])
        </nav>
    </div>
    @endif
</div>