<?php $campaign_base_path = Config::get('config.thumb_campaign_upload_path');
?>
<div class="col-lg-9">
            <div class="tabOuter">

                <div class="tab-content ">
                    <div id="popular" class="tab-pane fade in active">
                        <ul class="clearfix listEplore">
                            @if ( count($campaigns)>0 )       
                            @foreach ($campaigns as $campaign)
                            <li class="clearfix">

                                <a href="{{URL::route('campaign.show', $campaign->slug)}}">
                                    <div class="imgContainer col-sm-3" style="background: rgba(0, 0, 0, 0) url('{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($campaign_base_path.$campaign->campaign_image ) ?  asset($campaign_base_path.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}') no-repeat scroll center center / cover;">

                                    </div></a>
                                <div class="listDetails clearfix col-sm-9">
                                    <div class="detialsLeft">
                                        <div class="type">
                                            <a href="{{URL::route('campaign.show', $campaign->slug)}}"> {{ $campaign->campaign_name?$campaign->campaign_name:''}}  </a>                                   
                                        </div>
                                        <div class="price">
                                            {{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}
                                        </div>
                                        <div class="location">
                                            {{Helper::showAddress($campaign->cityName,$campaign->locationName,50)}}
                                        </div>
                                    </div>
                                    <div class="detialsRight">
                                        <div class="progressOuter" style="width:120px;height:120px;margin:10px auto;">
                                            <div class="percent" style="width:100%;height:100%;">
                                                <p style="display:none;">{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                            @else
                            <li> <h4>No Record Found.</h4></li>
                            @endif    
                        </ul>
                        <div class="paginationOuter">
                            <nav>
                                @include('site/partials/pagination', ['paginator' => $campaigns])
                            </nav>
                        </div>
                    </div>
                    <div id="myarea" class="tab-pane fade">
                        <h3>Menu 1</h3>
                        <p>Some content in menu 1.</p>
                    </div>
                    <div id="myarea" class="tab-pane fade">
                        <h3>Menu 1</h3>
                        <p>Some content in menu 1.</p>
                    </div>
                </div>
            </div>
        </div>