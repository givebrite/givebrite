<?php $page = trim(Input::get('page')); ?>
@if(!empty($charityList) && count($charityList)>0)
@foreach($charityList as $key=>$charity)

<div class="charity-listing">
    <div class="border-btm col-sm-2">

        <p class="syria-img">
            <a href="{{URL::route('charity.detail',$charity->slug)}}">
                <img class="charity-logo" src="{{isset($charity->charity_logo) && !empty($charity->charity_logo) && File::exists($base_path_charity.$charity->charity_logo ) ?  asset($base_path_charity.$charity->charity_logo)  : asset('images/default-images/default_logo.png')}}" alt="Charity Logo" />

                <?php // asset('images/default-images/default_logo.png') ?>
            </a>
        </p>
    </div>
    <div class="bigBlack col-sm-10 charity-details">
        <a href="{{URL::route('charity.detail',$charity->slug)}}"><h4>{{ $charity->charity_name }}</h4></a>
        <div class="subCategory">{{ !empty($charity->address) ? ucfirst($charity->address) : "" }}</div>
        <div class="details">{{ $charity->description }}</div>
    </div>

    <div class="col-sm-12">
        <div class="border-bottom mt20"></div>  
    </div>
</div>
@endforeach
@elseif(!empty($page) && $page == 1)
<div class="text-center text-bold">No Record Found.</div>
@endif