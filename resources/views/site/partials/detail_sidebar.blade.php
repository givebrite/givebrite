<div class='price'><h3>$30000</h3></div>
<div class='donate clearfix'><button class="blue-btn-lg text-center">Donate&nbsp;&nbsp;Now</button></div>
<div class='share clearfix'><button class="blue-btn-lg text-center">Share</button></div>
<div class="mapBlock listBox">
    <h3>Information</h3>
    <ul>
        <li class="raised-sec">
            <span class="labelSec">£10,000 raised so far</span>
            <span class="valueSec">£3,200 remaining</span>
        </li>
        <li class="link-sec">
            <span class="labelSec">
                <a href="#">givebrite.com/tcdnw75e</a>
            </span>
        </li>
        <li class="date-sec">
            <span class="labelSec">Created December 7, 2015</span>
        </li>
        <li class="user-sec">
            <span class="labelSec">Mansoor Khan</span>
            <span class="valueSec">Follow user</span>
        </li>
        <li class="place-sec">
            <span class="labelSec">Oldham, Uk</span>
        </li>
        <li>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7007.671580634382!2d77.22010372308584!3d28.57469330179076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce25ebc547eb1%3A0x95ed9d3aecd20c9!2sKotla+Mubarakpur%2C+South+Extension+I%2C+New+Delhi%2C+Delhi!5e0!3m2!1sen!2sin!4v1461586000840" width="600" height="90" frameborder="0" style="border:0" allowfullscreen></iframe>
        </li>
    </ul>
</div>
<div class="listBox">
    <h3>Backers</h3>
    <div class="innerListbox">
        <ul>
            <li class="camp-sec">
                <a href="{{ url('/405') }}">{{ trans('site/dashboard_sidebar.menus.campaigns.my_campaigns') }}</a>
            </li>
            <li class="dona-sec">
                <a href="{{ url('/405') }}">{{ trans('site/dashboard_sidebar.menus.campaigns.donations') }}</a>
            </li>
            <li class="comm-sec">
                <a href="{{ url('/405') }}">{{ trans('site/dashboard_sidebar.menus.campaigns.comments') }}</a>
            </li>
            <li class="reward-sec">
                <a href="{{ url('/405') }}">{{ trans('site/dashboard_sidebar.menus.campaigns.rewards') }}</a>
            </li>
            <li class="insight-sec">
                <a href="{{ url('/405') }}">{{ trans('site/dashboard_sidebar.menus.campaigns.insights') }}</a>
            </li>
        </ul>
    </div>
</div>
