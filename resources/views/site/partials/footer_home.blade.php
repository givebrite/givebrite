<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="footerWrapper clearfix">
                <div class="col1">
                    <div class="footerLogo">
                        <a class="navbar-brand" href="{{ url('/') }}">  <img src="{{ asset('site/images/logo-grey.png') }}" alt="givebrite" />  </a>           
                    </div>  
                </div>

                <div class="col2">
                    <div class="footerLinksHeading">{{ trans('site/menu.menus.footer1.learn_more') }}</div>
                    <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("footer1"); ?>

                    @if ($menus->count()>0)
                    <ul>
                        @foreach ($menus as $menu)
                        <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                        @endforeach                            
                    </ul>
                    @endif
                </div>

                <div class="col3">
                    <div class="footerLinksHeading">{{ trans('site/menu.menus.footer2.support') }}</div>
                    <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("footer2"); ?>

                    @if ($menus->count()>0)
                    <ul>
                        @foreach ($menus as $menu)
                        <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                        @endforeach                            
                    </ul>
                    @endif
                </div>
                
                <div class="col4">
                    <div class="footerLinksHeading">{{ trans('site/menu.menus.footer4.address') }}</div>
                    
                    <ul>
                       
                        <li><a href="javascript:void(0)">Give Brite</a></li>
                        <li><a href="javascript:void(0)">10a Abbey Hills Road</a></li>
                        <li><a href="javascript:void(0)">Greater Manchester</a></li>
                        <li><a href="javascript:void(0)">OL8 2BS</a></li> 
                                          
                    </ul>
                    
                </div>
                             
                <div class="col5 pull-right">
                    <ul class="socialIcons">
                        <li>
                            <a href="{{ url('https://www.facebook.com/givebrite') }}" target="_blank"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                        </li>
                        <li>
                            <a href="{{ url('https://www.twitter.com/give_brite') }}" target="_blank"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
                        </li>
                        <li>
                            <a href="{{ url('https://www.youtube.com/givebrite') }}" target="_blank"><span><i class="fa fa-youtube" aria-hidden="true"></i></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>