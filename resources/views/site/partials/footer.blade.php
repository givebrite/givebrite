<div class="innerFooter">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footerWrapper clearfix">
                    <div class="col1">
                      <a class="navbar-brand" href="{{ url('/') }}">  <img src="{{ asset('site/images/logo-white.png') }}" alt="givebrite" /></a> 
                    </div>
                    <div class="col2">
                        <div class="footerLinksHeading">{{ trans('site/menu.menus.footer1.learn_more') }}</div>
                        
                        <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("footer1"); ?>

                        @if ($menus->count()>0)
                        <ul>
                            @foreach ($menus as $menu)
                            <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                            @endforeach                            
                        </ul>
                        @endif
                        
                    </div>
                    <div class="col3">
                        <div class="footerLinksHeading">{{ trans('site/menu.menus.footer2.support') }}</div>
                        
                        <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("footer2"); ?>

                        @if ($menus->count()>0)
                        <ul>
                            @foreach ($menus as $menu)
                            <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                            @endforeach                            
                        </ul>
                        @endif
                        
                    </div>
                    <div class="col4">
                        <div class="footerLinksHeading">{{ trans('site/menu.menus.footer3.support') }}</div>
                        <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("footer3"); ?>

                        @if ($menus->count()>0)
                        <ul>
                            @foreach ($menus as $menu)
                            <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                            @endforeach                            
                        </ul>
                        @endif
                    </div>
                    <div class="col5">
                        <div class="footerLinksHeading">{{ trans('site/menu.menus.footer3.support') }}</div>
                        <?php $menus = App::make('App\Helpers\MenuItemHelper')->getCustomMenu("footer4"); ?>

                        @if ($menus->count()>0)
                        <ul>
                            @foreach ($menus as $menu)
                            <li><a href="{{ url('/'.$menu->link) }}">{{ $menu->title }}</a></li>
                            @endforeach                            
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>