<div class="mapBlock listBox borderRadiusZero">
    <input type="hidden" id="lat-val" value="{{ !empty($campaign->lattitude) ? $campaign->lattitude : '' }}">
    <input type="hidden" id="long-val" value="{{ !empty($campaign->longitude) ? $campaign->longitude : '' }}">
    
    <h3>Information</h3>
    <ul>
        <?php $leftAmount = $campaign->goal_amount_monetary - $totalAmountRaised; ?>
        <li class="raised-sec">
            <span class="labelSec">{{Helper::showMoney($totalAmountRaised,$campaign->campaign_currency)." raised so far"}}</span>
            @if($leftAmount>0)   
            <span class="valueSec">{{Helper::showMoney($leftAmount,$campaign->campaign_currency)." remaining"}}</span>
            @endif
        </li>
        @if(!empty($campaign->userWeb) || !empty($campaign->charityWeb))
        <li class="link-sec">
            <span class="labelSec">
            @if(!empty($campaign->userWeb))
                <a target="blank" href="http://{{ $campaign->userWeb }}" class="blueThemeColor">{{ $campaign->userWeb ? str_limit($campaign->userWeb , $limit = 20, $end = '...') :''}}</a>
            @elseif(!empty($campaign->charityWeb))
                <a target="blank" href="http://{{ $campaign->charityWeb }}" class="blueThemeColor">{{ $campaign->charityWeb ? str_limit($campaign->charityWeb , $limit = 20, $end = '...') :''}}</a>
            @endif
            </span>
        </li>
        @endif
        <li class="date-sec">
            <span class="labelSec">Created On {{Helper::showDate($campaign->created_at)}}</span>
        </li>
        <li class="user-sec">
            <span class="labelSec">{{!empty($campaign->createrName) ? $campaign->createrName :''}}</span>
           <!-- <span class="valueSec"><a href="#">Follow user</a></span>-->
        </li>
        @if(!empty($campaign->locationName) || !empty($campaign->cityName))
        <li class="place-sec">
            <span class="labelSec">{{!empty($campaign->locationName) && !empty($campaign->cityName) ? ucfirst($campaign->cityName).", ".ucfirst($campaign->locationName) : '' }}</span>
        </li>
        @endif
        <li class="mapSec">
            <a href="http://maps.google.com/?q={{!empty($campaign->lattitude) ? $campaign->lattitude.','.$campaign->longitude: ''}}"  target="_blank">
            <div id="campaign-map" style="height: 100px" class="sell-form-map">
            </div>
            </a>
        </li>
    </ul>
</div>