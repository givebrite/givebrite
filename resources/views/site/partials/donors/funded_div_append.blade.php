
<?php $page= trim(Input::get('page'));?>
@if(!empty($fundList) && count($fundList)>0)
@foreach($fundList as $fund)
<li class="item col-xs-12 col-sm-6" id="fund-list-page-{{!empty($page) ? $page :0 }}">
    <div class="cardUser">
        <a href="{{URL::route('campaign.show', $fund->slug)}}"><div style='background-image:url("{{isset($fund->campaign_image) && !empty($fund->campaign_image) && File::exists($base_path_campaign.$fund->campaign_image ) ?  asset($base_path_campaign.$fund->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}")' class="donorImg"></div></a>
        <div class="cardDetail">
            <div class="charityAmount"><span>{{Helper::showMoney($fund->goal_amount_monetary)}}</span>
            </div>
            <a href="{{URL::route('campaign.show', $fund->slug)}}"><p class="campTitle">{{ $fund->campaign_name ? str_limit(ucfirst($fund->campaign_name) , $limit = 15, $end = '...') :''}}</p></a>
            <p class="location">
                    {{Helper::showAddress($fund->cityName,$fund->locationName,15)}}
            </p>
            <div class="progressOuter inherit" style="width:70px;height:70px;">
                <div class="percent" style="width:70px;height:70px;">
                    <p style="display:none;">{{Helper::showGoalPercentage($fund->id, $fund->goal_amount_monetary )}}</p>
                </div>
            </div>
        </div>
    </div>
</li>
@endforeach
@elseif(!empty($page) && $page == 1)
<li class="col-xs-12 text-bold">No Record Found.</li>
@endif
