
<?php $page= trim(Input::get('page'));?>
@if(!empty($campaignList) && count($campaignList)>0)
@foreach($campaignList as $campaign)
<li class="item col-xs-12 col-sm-6" id="raise-list-page-{{!empty($page) ? $page :0 }}">
    <div class="cardUser">
        <a href="{{URL::route('campaign.show', $campaign->slug)}}"><div style='background-image:url("{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($base_path_campaign.$campaign->campaign_image ) ?  asset($base_path_campaign.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}")' class="donorImg"></div></a>
        <div class="cardDetail">
            <div class="charityAmount"><span>{{Helper::showMoney($campaign->goal_amount_monetary)}}</span>
            </div>
            <a href="{{URL::route('campaign.show', $campaign->slug)}}"><p class="campTitle">{{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 15, $end = '...') :''}}</p></a>
            <p class="location">
                {{Helper::showAddress($campaign->cityName,$campaign->locationName,15)}}
            </p>
            <div class="progressOuter inherit" style="width:70px;height:70px;">
                <div class="percent" style="width:70px;height:70px;">
                    <p style="display:none;">{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}</p>
                </div>
            </div>
        </div>
    </div>
</li>
@endforeach
@elseif(!empty($page) && $page == 1)
<li class="col-xs-12 text-bold">No Record Found.</li>
@endif