<link href="{{ asset('site/favicon.ico') }}" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href='{{ asset('site/css/bootstrapnew.min.css')}}' rel='stylesheet' type='text/css'>
<link href='{{ asset('site/css/stylenew.css').'?num='.mt_rand(1000000, 9999999) }}' rel='stylesheet' type='text/css'>
<link href='{{ asset('site/css/responsivenew.css').'?num='.mt_rand(1000000, 9999999) }}' rel='stylesheet' type='text/css'>
<link href='{{ asset('site/css/font-awesomenew.min.css').'?num='.mt_rand(1000000, 9999999) }}' rel='stylesheet' type='text/css'>
