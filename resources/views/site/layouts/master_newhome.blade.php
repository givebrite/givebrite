<!doctype html>
<html>
    <!-- Head Section of the page  -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no"  />
        <title>@yield('title')</title>
        <script> site_url = '<?php echo url(); ?>'</script>
        @include('site.partials.new_style')
        @yield('style')

        <script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-105840375-1', 'auto');  ga('send', 'pageview');</script>
        
    </head>

    <body>
        <?php
        if (Session::has('user_session_data')) {
            $user_session_data = Session::get('user_session_data');
            if (!empty($user_session_data)) {
                if ($user_session_data['active'] == false && Helper::checkIsActivated()) {
                    $user_session_data['active'] = true;
                    Session::set('user_session_data', $user_session_data);
                }
            } else {
                Auth::logout();
            }
        } else {
            // Logout Session
            Auth::logout();
        }
        ?>
       
        @include('site.partials.new_header')
        <div class="clearfix"></div>


        @yield('content-header')

        @yield('topbar')

        @yield('content')

        @include('site.partials.footer_home')

        @yield('script')

    </body>
</html>