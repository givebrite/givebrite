<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/owl.carousel.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">
    <script>site_url = '<?php echo url(); ?>';</script>
            @include('site.partials.style')
            @yield('style')

    <script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-105840375-1', 'auto');  ga('send', 'pageview');</script>  
          
    <style type="text/css">
        
        .pagination1 li {
            color: #333;
            display: inline-block;
            text-align: center;
        }
        
        .pagination1 li.disabled a {
            background: none repeat scroll 0 0 #37c2df;
            color: #fff;
        }
        
        .pagination1 li a {
            background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
            border: medium none;
            border-radius: 50%;
            color: #37c2df;
            display: block;
            font-size: 18px;
            height: 31px;
            padding: 2px;
            text-align: center;
            width: 31px;
        }

        .search-dropdown{
            background: #fff;
            border-radius:4px; 
            overflow-y: scroll; 
            box-shadow: 0 8px 6px -6px black; 
            height: auto;
            max-height: 200px; 
            position: absolute; 
            z-index: 9; 
            top:100px;
            display: none;
            border: 1px solid #ccc;
            width: 94%;
        }
        
        .search-dropdown ul li{

            height: 30px;
            line-height: 30px;
            padding: 5px;
        }

        .search-dropdown ul li a{

            color: #898989;
        }

        .search-dropdown ul li a:hover{

            color: #aaa;
        }


    </style>
    <!-- <meta name="description" content="@yield('meta-description', 'Raise funds for your cause')"/>
     -->
</head>
<body>
<?php
if (Session::has('user_session_data')) {
    $user_session_data = Session::get('user_session_data');
    if (!empty($user_session_data)) {
        if (($user_session_data['active'] == false) && Helper::checkIsActivated()) {
            $user_session_data['active'] = true;
            Session::set('user_session_data', $user_session_data);
        }
    } else {
        Auth::logout();
    }
} else {
    // Logout Session
    Auth::logout();
}
?>

@include('site.partials.headerstrip')

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::render() !!}

<div class="sticky_header">
    @yield('content')

    @include('site.partials.footer_home')

    @include('site.partials.script')
    @yield('script')
</div>
<script>
    
</script>

</body>
</html>