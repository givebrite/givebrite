<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>@yield('title')</title>

        @include('site.partials.style')
        @yield('style')

    </head>
    <body>
        <?php
        if (Session::has('user_session_data')) {
            $user_session_data = Session::get('user_session_data');
            if (!empty($user_session_data)) {
                if ($user_session_data['active'] == false && Helper::checkIsActivated()) {
                    $user_session_data['active'] = true;
                    Session::set('user_session_data', $user_session_data);
                }
            } else {
                Auth::logout();
            }
        } else {
            // Logout Session
            Auth::logout();
        }
        ?>
        <div class="homeHeader">
            @include('site.partials.header', array('logo'=>'dark'))

            @yield('content-slider')
        </div>
        <div class="container">            
            @yield('content-header')
            @yield('content')
        </div>

        <div class="bottomPane text-center">
            <div class="container custom-footer">
                @yield('content-footer')
            </div>
        </div>

        @include('site.partials.footer_home')

        @include('site.partials.script')
        @yield('script')

    </body>
</html>