    @extends('site.layouts.master')

@section('title', !empty($userProfile->first_name) ? $userProfile->first_name : 'GiveBrite Donar')

@section('content')
<?php $base_path_user = Config::get('config.large_profile_image_upload_path'); ?>
<?php $base_path_campaign = Config::get('config.thumb_campaign_upload_path'); ?>

<div class="topPane text-center">
    <div class="container">
        <h3>{{ $userProfile->first_name." ".$userProfile->sur_name }} </h3>
        <p>Joined GiveBrite on {{ $userProfile->created_at->format('M d, Y') }}</p>
    </div>
</div>
<div class="container bgWhite profileDonorPage">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <div class="donorProfile">
                
                <div style='background-image:url("{{ (isset($userProfile->profile_pic) && !empty($userProfile->profile_pic)) ? ((substr( $userProfile->profile_pic, 0, 4 ) === "http") ? $userProfile->profile_pic :  (File::exists($base_path_user.$userProfile->profile_pic ) ? asset($base_path_user.$userProfile->profile_pic) : asset('images/default-images/default_profile.png'))) : asset('images/default-images/default_profile.png') }}")' class="profilePicOuter"></div>
            </div>
            <h3 class="profileTitle">Overview</h3>
            <div class="mapBlock listBox borderRadiusZero">
                <ul>
                    <li class="raised-sec">
                        <span class="labelSec">{{Helper::showMoney( $donated_amount_sum )}} donated so far</span>
                    </li>
                    <li class="date-sec">
                        <span class="labelSec">Created {{ $userProfile->created_at->format('M d, Y') }}</span>
                    </li>
                    <li class="user-sec">
                        <span class="labelSec">{{ $userProfile->first_name." ".$userProfile->sur_name }}</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-12 col-md-8">
            <input type="hidden" id="raised_campaign_url" value="{{URL::route('donor.raised.campaign',$userProfile->slug)}}">
            <div class="charityDetails-lower headTitleGray clearfix">
                <h4>Campaigns</h4>
                <ul class="donorList row">
                    <div class="scroll" id="raised-scroll">
                        @include("site.partials.donors.raised_div_append")
                    </div>
                    <div id="raised-pagination">
                        @include("site.partials.paginationdiv")
                    </div>
                    <div class="col-sm-12 loadMoreBlock loadMoreBlockFund load-more-donar-profile">
                        <p>
                            <button class="mobile-btn mobile-btn-fund" id="load-more-raised-campaign" style="display:none;">Load More Campaigns</button>
                        </p>
                    </div>
                </ul>
                
            </div>
            <div class="charityDetails-lower headTitleGray clearfix">
                <input type="hidden" id="funded_campaign_url" value="{{URL::route('donor.funded.campaign',$userProfile->slug)}}">
                <h4>Funded</h4>
                <ul class="donorList row">
                    <div id="funded-scroll">
                        @include("site.partials.donors.funded_div_append")
                    </div>
                    <div id="funded-pagination">
                        @include("site.partials.paginationdiv")
                    </div>    
                    <div class="col-sm-12 loadMoreBlock loadMoreBlockCampaign load-more-donar-profile">
                        <p>
                            <button class="mobile-btn mobile-btn-campaign" id="load-more-funded-campaign" style="display:none;">Load More Campaigns</button>
                        </p>
                    </div>
                </ul>
            </div>

        </div>
    </div>
</div>
@stop

@section('script')
<script src='{{ asset('site/js/donor-infinite-loading.js').'?num='.mt_rand(1000000, 9999999) }}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
<script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
<script>
        $(function () {
        $('.percent').percentageLoader({
        valElement: 'p',
                strokeWidth: 5,
                bgColor: '#d9d9d9',
                ringColor: '#37c2df',
                textColor: '#9f9f9f',
                fontSize: '18px',
                fontWeight: 'bold'
        });
        });
</script>
@stop
