    @extends('site.layouts.master_home')

    @section('title', 'Online Fundraising and Donation Platform - Give Brite')

    @section('style')
    @include('site.partials.slider_styles')
    <style>

        ul li.active{
            background:#bfbfbf;
        }
        .bigger-icon{
            font-size: 25px;
        }
    </style>
    @stop

    @section('content-slider')
    <?php $bannerBasePath = Config::get('config.large_banner_upload_path'); ?>
            
    @include('site.partials.revolution_slider')
    @stop

    @section('content-header')
    <div class="row">
        <div class="col-sm-3 text-center">
            <div class="howItWork">
                <div class="imgBlock">
                    <img src="{{ asset('site/images/low-fee.png') }}" alt="lower fees" />
                </div>
                <div class="textOuter">
                    <h4>Lower Fees</h4>
                    <p>We charge 3.5% to charities and 4.5% for fundraisers</p>
                </div>
            </div>
        </div>
        <div class="col-sm-3 text-center">
            <div class="howItWork">
                <div class="imgBlock">
                    <img src="{{ asset('site/images/quick-easy.png') }}" alt="quick easy" />
                </div>
                <div class="textOuter">
                    <h4>Quick & Easy</h4>
                    <p>Setup is fast and clear</p>
                </div>
            </div>
        </div>
        <div class="col-sm-3 text-center">
            <div class="howItWork">
                <div class="imgBlock">
                    <img src="{{ asset('site/images/secure.png') }}" alt="Secure" />
                </div>
                <div class="textOuter">
                    <h4>Secure</h4>
                    <p>Your data is yours and we will always have your back</p>
                </div>
            </div>
        </div>
        <div class="col-sm-3 text-center">
            <div class="howItWork">
                <div class="imgBlock">
                    <img src="{{ asset('site/images/heart.png') }}" alt="Heart" />
                </div>
                <div class="textOuter">
                    <h4>We Give Back</h4>
                    <p>We give back to the community</p>
                </div>
            </div>
        </div>
    </div>
    @stop

    @section('content')
    <div class="row">
        @include('site.partials.explorelist')
        <div class="col-lg-9">
            <div class="tabOuter" id='campaign_list_div'>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" id="popular_campaign_url" value="{{ URL::route("popular.campaign") }}">
                <input type="hidden" id="area_campaign_url" value="{{ URL::route("area.campaign") }}">
                <input type="hidden" id="facebook_campaign_url" value="{{ URL::route("fb.campaign") }}">
                <input type='hidden' id='current_latitude' value=''>
                <input type='hidden' id='current_longitude' value=''>
                <ul class="nav nav-tabs responsive row" id="campaign_list_tab">
                        <li class="active col-lg-4"><a data-toggle="tab" href="#popular" id='popular_campaign'>Popular</a></li>
                        <li class="col-lg-4"><a data-toggle="tab" href="#myarea" id='my_area_campaign'>My Area</a></li>
                        <li class="col-lg-4"><a data-toggle="tab" href="#myfacebook" id='my_fb_campaign' >My Friends</a></li>
                    </ul>
                <div class="tab-content responsive row">
                    <div id="popular" class="tab-pane active">
                        @include("site.partials.popular_campaign")  
                    </div>
                    <div id="myarea" class="tab-pane">
                        @include("site.partials.myarea_campaign")  
                    </div>
                    <div id="myfacebook" class="tab-pane">
                        @include("site.partials.myfb_campaign") 
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop

    @section('content-footer')
    <h3>Let’s Create Your <span>Own</span> Campaign</h3>
    <p>Easily create your own campaign and start accepting donations</p>
    <p>
        <a href="{{ URL::route('campaigns.create') }}" class="blue-btn-lg">Get Started</a>
    </p>
    @stop

    @section('script')
    <script src="{{ asset('js/map-start.js') }}"></script>
    <script src="{{ asset('site/js/getcurrentgeolocation.js').'?num='.mt_rand(1000000, 9999999) }}"></script>
    @include('site.partials.slider_script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
    <script src="{{ asset('js/explore-hide-show.js') }}"></script>
    <script type="text/javascript">
        var tpj=jQuery;			
        var revapi4;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_4_1").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_4_1");
            }else{
                revapi4 = tpj("#rev_slider_4_1").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"../../revolution/js/",
                    sliderLayout:"fullwidth",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        onHoverStop:"off",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style:"zeus",
                            enable:true,
                            hide_onmobile:true,
                            hide_under:600,
                            hide_onleave:true,
                            hide_delay:200,
                            hide_delay_mobile:1200,
                            tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            }
                        }
                        ,
                        bullets: {
                            enable:true,
                            hide_onmobile:true,
                            hide_under:600,
                            style:"metis",
                            hide_onleave:true,
                            hide_delay:200,
                            hide_delay_mobile:1200,
                            direction:"horizontal",
                            h_align:"center",
                            v_align:"bottom",
                            h_offset:0,
                            v_offset:30,
                            space:5,
                            tmp:'<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span>'
                        }
                    },
                    viewPort: {
                        enable:true,
                        outof:"pause",
                        visible_area:"80%"
                    },
                    responsiveLevels:[1240,1024,778,480],
                    gridwidth:[1240,1024,778,480],
                    gridheight:[487,600,500,400],
                    lazyType:"none",
                    parallax: {
                        type:"mouse",
                        origo:"slidercenter",
                        speed:2000,
                        levels:[2,3,4,5,6,7,12,16,10,50],
                    },
                    shadow:0,
                    spinner:"off",
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,
                    shuffle:"off",
                    autoHeight:"off",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
        });	/*ready*/
	</script>
    <script>
    $(function () {
        $('.percent').percentageLoader({
            valElement: 'p',
            strokeWidth: 5,
            bgColor: '#d9d9d9',
            ringColor: '#37c2df',
            textColor: '#9f9f9f',
            fontSize: '18px',
            fontWeight: 'bold'
        });

    });
    </script>
        <script src="https://openam.github.io/bootstrap-responsive-tabs/js/responsive-tabs.js"></script>
        <script type="text/javascript">
            $( 'ul.nav.nav-tabs  a' ).click( function ( e ) {
              e.preventDefault();
              $( this ).tab( 'show' );
            } );
            $(document).ready(function(){
                fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
            });
        </script>
    @stop
