@extends('site.layouts.master')

@section('title', 'Page')

@section('content')
<div class="stripe-connect-div">
    <div class="connect-stripe clearfix" style="">
        <div class="stripe-text text-center laungSlogan">
            <h4> We are setting you up...</h4>
        </div>
        <div class="stripe-loading-img text-center clearfix">
            <img src="{{ asset('images/defaultPic.gif') }}">
        </div>
        <div class="stripe-connect-button text-center clearfix">
            <img class="hidden-xs" src="{{asset('images/stripe_connect.png')}}">
            <img class="visible-xs" src="{{asset('images/stripe_connect_mobile.png')}}">
        </div>
        <div class="text-center">
            You will be redirected to stripe website shortly.If You are not redirected Please <a href="{{ $stripe_connect_link }}">Click Here.</a></p>
        </div>
    </div>
</div>
@stop

@section('script')
<script>
    $( window ).load(function() {
    // Run code
       setTimeout(function(){ 
            window.location.href = "{{ $stripe_connect_link }}";
        }, 5000);
    });
</script>
@stop


