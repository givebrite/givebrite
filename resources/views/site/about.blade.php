@extends('site.layouts.master')

@section('title', 'About Us' )

@section('content')
<div class="about">
<div class="container">
        <div class="row">
        
                    <div class="col-md-12 ">
                    
                    <h1 class="about">About Us</h1>
                                            
                        <p class="about-text">GiveBrite is an online fundraising platform and we're all about crowdfunding great ideas with a whole new kind of experience. It's driven us from the beginning, this compulsion to empower individuals with causes, charities with campaigns and businesses with innovative ideas through our incredible cutting edge technology that will turn ideas into reality, everyday. </p>
             
            
            
                    </div>
                        
                   <div class="col-md-6">
                   <h2>We’re on a mission… </h2>           
            <p>Our mission is simple. We want to give fundraisers with great ideas the power to connect with givers</p>
</div>
 <div class="col-md-6">
                   <h2>And we will get there!</h2>           
            <p>Our vision is to create a community of giving through empowering fundraisers by designing technology to truly revolutionise the way people donate forever.</p>
                     
                     </div>
                 
                  
                </div>
            </div>
  <hr class="colored" /> 
    <div class="principles">
              

        <div class="container">
            <div class="row">
               <div class='col-md-12'>
                  <h1>Our Principles</h1>
                  <div class="row">
                  <div class="col-md-6">
                  <h2>Young Team</h2>
                  <p>GiveBrite’s founders are young and so is our team. The synergy drives vibrancy and passion into our product, features and our tech company. </p>
                  </div>
                  <div class="col-md-6">
                  <h2>Innovation Inspired Design</h2>
                  <p>We strive to design and develop through remarkable and unparalleled level of technical innovation with an aim to make crowdfunding accessible, relevant and ultimately personal.   </p>
                  </div>
                  </div>
                  
                  <div class="buttons">
                     <a href="{{ url('/campaigns/create') }}" class="blue-btn dropdown-toggle">Launch your campaign</a>  <a href="{{ url('/campaigns') }}" class="green-btn dropdown-toggle">Explore</a>
                  </div>
                  
               </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        
                     
                     
                     
        	<div class="col-md-6">
            
            	<h2>Community of Giving</h2>
                <p>Create a community of givers, fundraisers and volunteers who can come together to share ideas and engage people with the campaigns that matter to them the most. </p>
                </div>
                <div class="col-md-6">
                <h2>Change the World</h2>
                <p>We think we can change the world through the power of giving! As Steve Jobs said &quot;the ones who are crazy enough to think they can change the world, are the ones who do&quot;.</p>
                </div>
                <div class="col-md-12">
              </div>
              </div>
              </div>
              <hr />
              <div class="container">
                <div class="row fees">
                	<div class="col-md-6" style="text-align:left">
                      <h2>Fees</h2>
                
                <p>We charge groundbreaking fees, unmatched by any other provider. The great news is, donors have the options to cover your fees, leaving more for your cause!</p>
               		</div>
                    <div class="col-md-2"> <div class="fee-container">
                    	<div class="fee-title">Charities</div>
                    	<div class="fee">3.5%</div></div>
					</div>
                    <div class="col-md-2"> <div class="fee-container">
                    	<div class="fee-title">Individuals</div>
                    	<div class="fee">4.5%</div></div>
					</div>
                    <div class="col-md-2"> <div class="fee-container">
                    	<div class="fee-title">Stripe Fees</div>
                    	<div class="fee">1.4%<span class="additional-fees"> +20p</span></div></div>
					</div>
                    
                </div>
                
                
            </div>
        </div>
    </div>
    
    
</div>

@stop