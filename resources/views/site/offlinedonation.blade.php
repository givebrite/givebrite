@extends('site.layouts.master')

@section('title', !empty($campaign->campaign_name) ? ucfirst($campaign->campaign_name) . ' - Offline Donation' : 'Offline Donation')

@section('style')
    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <style>
        @media (max-width: 767px) {
            .tab-top-wrap {
                border-bottom: none;
            }
        }
        .inputBlock{
            width: 100%;
        }
        .form-sec .form-row.multiinputs .inputBlock{
            width: 100%;
        }
        #donation_amt
        {
            padding: 0 50px;
        }
        .form-sec .form-row.multiinputs .inputBlock:last-child {
            margin-left: 0%;
        }
        #donor_name
        {
            display: none;
        }
    </style>
@stop

@section('content')

{!! Form::open(array('route' => ['donate.offline', $campaign->slug], 'novalidate' => 'novalidate', 'id' => 'offline_donation')) !!}
<div class="container">
    <div class="col-sm-12 contact-wrapper">
        <div class="laungSlogan createHeading">
            <h2>Add Offline Donation</h2>
            <h4></h4>

        </div>
        <div class="signUp-form"> 
            <div class="form-sec">
                <div id="user1-form" class="form-row col-sm-6">      
                    <div class="form-row multiinputs clearfix">
                        <label for="donation_amount">Donation amount <span class="astrikBlock">*</span></label>
                            <div class="inputBlock">
                                <span class="gbpBtn"></span>
                                <input type='hidden' name='goal_amount_type' value='monetary' placeholder="&pound;">
                                <span class="sterling">&pound;</span>
                                {!! Form::text('amount',Session::has('donationAmount') ? Session::get('donationAmount') : '' , ['id'=>'donation_amt']) !!}
                            
                            <p id="amt_error" class="text-danger"></p>
                            {!! $errors->first('goal_amount_monetary', '<div class="text-danger">:message</div>') !!}
                        </div>
                        @if(!empty($donation))
                        <div class="inputBlock">
                            <div class="choose-donation">
                                <span>Choose donation type</span>
                            </div>
                        </div>
                        @endif
                        <div class="inputBlock"> 
                            <div class="select-donation">
                                @foreach($donation as $d)
                                    <div class="select-donation-type">
                                        <label class="control control--radio">
                                            <input
                                                    @if($d->is_default)
                                                    checked
                                                    @endif
                                                    type="radio" name="default" value="{{$d->id}}">
                                            {{$d->title}}
                                            <div class="control__indicator"></div>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="form-row col-sm-6">
                          

                <div class="form-row">

                    <div class="inputBlock">
                        <label for="donation_amount">Email Address <span class="astrikBlock">*</span></label>
                        <input type="email" id="email_address" name="email" placeholder="Email Address"/>
                        <p id="donor_email_error" class="text-danger"></p>
                    </div>

                </div>

                <div class="inputBlock" id="donor_name"> 
                    
                    <div class="row">
                        <div class="form-row multiinputs clearfix">
                            <div class="register-select col-sm-12 col-md-3 col-lg-3 ">
                            {!! Form::select('salutation', array('Mr' => 'Mr', 'Miss' => 'Miss', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Dr' => 'Dr', 'Prof' => 'Prof', 'Maulana' => 'Maulana'),['id'=>'salutation']) !!}
                            <div class="select__arrow"></div>
                            </div>

                            <div class="col-sm-12 col-md-9 col-lg-9">
                                {!! Form::text('first_name','', ['class' => 'form-control','placeholder' => 'First Name','id'=>'first_name']) !!}
                                {!! $errors->first('first_name', '
                                <div class="text-danger">:message</div>
                                ') !!}
                                 <p id="donor_first_name_error" class="text-danger"></p>
                            </div>
                           
                        </div>

                        <div class="form-row clearfix">
                            <div class="inputBlock">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                {!! Form::text('last_name','', ['class' => 'form-control','placeholder' => 'Surname','id'=>'last_name']) !!}
                                {!! $errors->first('last_name', '
                                <div class="text-danger">:message</div>
                                ') !!}
                                <p id="donor_last_name_error" class="text-danger"></p>
                                 </div>
                                 
                            </div>
                        </div>

                    </div>

                    

                    <!-- <div class="row">
                        <div class="col-sm-12">
                            <label for="donation_amount">Donor Name <span class="astrikBlock">*</span></label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" id="first_name" name="firstname" placeholder="First Name"/>
                            <p id="donor_name_error" class="text-danger"></p>
                        </div>
                        <div class="col-sm-6">
                        <input type="text" id="last_name" name="lastname" placeholder="Last Name"/></div>
                    </div> -->

                 </div>

                <div class="form-row">
                    <div class="inputBlock">
                        <label class="control keep-donor">
                            <input checked type="checkbox" name="anonymous" value=""
                                   checked="false" id="isGiftAid">
                            Gift Aid
                            <div class="control_checkbox"></div>
                        </label>
                    </div>
                </div>

                <div class="form-row">
                    <div class="inputBlock">
                        <label class="control keep-donor">
                            <input checked type="checkbox" name="anonymous" value="Keep donor anonymous"
                                   checked="checked">
                            Keep donor anonymous
                            <div class="control_checkbox"></div>
                        </label>
                    </div>
                </div>
                <div class="form-row">
                    <div class="inputBlock">
                        <label class="control charity-text">
                            <input checked type="checkbox" name="default" value="Keep donor anonymous">
                            Tick here if you would like Charity Name to reclaim the tax you have paid on all your
                            donations made in the last four years, and any future donations you may make.*
                            <div class="control_checkbox"></div>
                        </label>
                    </div>
                </div>
                 <div class="form-row">
                    <div class="inputBlock">
                       <label for="donation_amount">Address Information</label>
                        
                        <!-- <input type="text" id="postcode" name="amount" placeholder="Postcode"/>
                        
                        <input type="text" id="address1" name="amount" placeholder="Address"/>
                        
                        <input type="text" id="Town" name="amount" placeholder="Town"/>
                         -->

                        <input type="hidden" id="newuser" name="newuser" value=""/>

                       

                        
                       <div class="row">
                            <div class="form-row multiinputs clearfix">
                                
                                <div class="col-sm-12 col-md-9 col-lg-9">
                        {!! Form::text('postcode', null, ['class' => 'form-control','placeholder' => 'Enter  Postcode' ,'id'=>'postCode','postCodeStatus'=>'404']) !!}
                             <p id="postcode_error" class="text-danger"></p>
                             <span class="minor pull-right location-link-span">
                                <a class="location-link">Enter Address manually</a>
                             </span>

                             <p class="text-primary" id="postCodeMessage"></p>

                                </div>

                                <div class="col-sm-12 col-md-3 col-lg-3 searchPostCode">
                               
                                    <a href="javascript:void(0)" id="searchPostCode">Search</a>

                                </div>

                               
                            </div>
                        </div>
                         <!--select dropdown-->
                                             
                         <div class="form-row hidden" id="AddressSelectMain">
                           
                           <div class="register-select"> 
                            
                            <select name="addressSelect" id="addressSelect" class="valid" >
                                
                                <option value="">Please Select your address</option>

                            </select>

                           </div> 
                            
                            <div class="select__arrow"></div>
                            
                        </div>
                         <!--select dropdown-->   

                        <div class="form-row registerHiddenFields hide-Location">
                            {!! Form::text('address', null, ['class' => 'form-control','placeholder' => 'Address','id'=>'address']) !!}
                            {!! $errors->first('address', '<div class="text-danger">:message</div>') !!}
                        </div>
                        <div class="form-row registerHiddenFields hide-Location">
                            {!! Form::text('locality', null, ['class' => 'form-control','placeholder' => 'Town', 'id'=>'district']) !!}
                            {!! $errors->first('locality', '<div class="text-danger">:message</div>') !!}
                        </div>
                        <div class="form-row registerHiddenFields hide-Location">
                            {!! Form::text('country', null, ['class' => 'form-control','placeholder' => 'Country', 'id'=>'country']) !!}
                            {!! $errors->first('country', '<div class="text-danger">:message</div>') !!}
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="form-row col-sm-12 col-lg-6"> 
                        {!! Form::submit('Add Donation', array('class'=>'fullwidth blue-btn-lg text-center')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
{!! Form::close() !!}

@stop

@section('script')
    <script type="text/javascript">
        // $("#donate_now").click(function(){
        //     $("#donation_amount").focus();
        // });
        $('#myModal').on('shown.bs.modal', function () {
            $('#donation_amount').focus();
        })
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&signed_in=true&libraries=drawing&callback=initMap"
            async defer></script>
    <script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/campaign-detail.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>



    <script src="{{asset('site/js/fusioncharts.js')}}"></script>
    <script src="{{asset('site/js/fusioncharts.charts.js') }} "></script>
    <script src="{{asset('site/js/fusioncharts.theme.management-dashboard.js') }}"></script>

    <script>
        camp_id = '<?php echo $campaign->campaignId; ?>';
        $(document).ready(function () {
            $.ajax({
                url: 'stravaActivity/' + camp_id,
                //dataType: "json",
                //method: "GET",
                success: function (data) {
                    $("#training").html(data);
                }
            });
            $("ul.wysihtml5-toolbar li a[data-wysihtml5-command=insertImage]").css("display", 'none');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        $(".textarea").wysihtml5();
        $(".textarea").css("color", "#333");

        $(window).scroll(function () {
            scroll = $(window).scrollTop();
            if (scroll >= 80) {
                $('.mobile-social').show();
            } else if (scroll <= 80) {
                $('.mobile-social').hide();
            }
        });
    </script>

    <script>
        //Post code
        

       $('#searchPostCode').click(function(e) {
              
              var string = $('#postCode').val();

              $.ajax({

                    url: "https://api.ideal-postcodes.co.uk/v1/postcodes/"+string+"?api_key=iddqd",
                    xhrFields: {
                         withCredentials: true
                    },
                    dataType: 'jsonp',
                    type: "get",
                    success:function(data){
                        
                        if(data.code == 2000){

                            $('#postCode').attr('postcodestatus',200);
                            $('#postCode').val(data.result[0]['postcode']);
                            //$('#postCodeMessage').html('post code verified');
                            $('#AddressSelectMain').removeClass('hidden');
                            $('#addressSelect').html('<option>Please Select your address </option>');
                                
                            $.each(data.result, function(index, element) {

                            $('#addressSelect').append('<option value="'+ element.line_1+' , '+element.district+' ,'+element.country+' ">'+ element.line_1+' , '+element.district+' ,'+element.country+' </option>');
                            
                            });
                            $("#addressSelect").show();  
                            
                            $('#postcode_error').addClass('hidden');
                            console.log(data)
                            
                        } else {

                           $('#postCode').attr('postcodestatus',404);
                            $('#AddressSelectMain').addClass('hidden');
                            $('#postcode_error').addClass('hidden');
                            $('#postCodeMessage').html('Please enter a valid postcode');
                            $('#address').val('');
                            $('#country').val(''); 
                            $('#district').val('');
                            console.log('No post code found');
                      }
                         
                     },
                     error:function(){
                         alert("Error");
                     }  
                });

                //console.log(this.value);
          });

           $('#addressSelect').on('change', function() {

              var address = this.value;
              var arr = address.split(',');
              $('#address').val(arr[0]); $('#district').val(arr[1]); $('#country').val(arr[2]);
              $('#AddressSelectMain').addClass('hidden');  
              $(".registerHiddenFields").show();
              console.log(arr);

           }); 

           $('.location-link').click(function(){

                  $("#addressSelect").hide(); $('#postCodeMessage').html(''); 
                  $('#address').val(''); $('#district').val(''); $('#country').val('');  $('#postCode').val('');
                  $(".registerHiddenFields").toggle();
           
            }); 

        function makepass() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 6; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }


        

        $(document).ready(function () {

            //Add post code
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#email_address').focusout(function() {
                
                var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
                if (testEmail.test(this.value))
                {
                    email_address = $("#email_address").val();
                    data_user = {
                        email: email_address,
                    }
                    $.ajax({
                        url: site_url + '/offlineusercheck',
                        dataType: "json",
                        method: "post",
                        data: data_user,
                        complete: function (data) {
                            console.log(data.responseText);
                            //alert(data.responseText); 
                            if (data.responseText == 'new') {
                                $('#donor_name').show();
                                $('#newuser').val('show');
                            }
                            if (data.responseText == 'old') {
                                $('#donor_name').hide();
                                $('#newuser').val('hide');
                            }
                        }
                    });
                }
                else
                {
                    $("#donor_email_error").html('Donor mail is required');
                    return false;
                }
            });
            $("#offline_donation").submit(function (e) {
                e.preventDefault();
                
                var isGiftAid = 0;

                if($('#isGiftAid:checkbox:checked').length > 0) {

                    isGiftAid = 1;
                }

                donation_amt = $("#donation_amt").val();
                if(donation_amt == ''){
                    $("#amt_error").html('Donation Amount is required');
                    return false;
                }
                donation_type = $("input[name=default]:checked").val()
                
                email_address = $("#email_address").val();
                
                if(email_address == ''){
                    $("#donor_email_error").html('Donor mail is required');
                    return false;
                }
                
                // Data for first Ajax request
                first_name = $("#first_name").val();
                last_name = $("#last_name").val();

                if($('#newuser').val() == 'show')
                {
                    if(first_name ==''){
                        $("#donor_first_name_error").html('Donor name is required');
                        return false;
                    }

                    if(last_name ==''){
                        $("#donor_last_name_error").html('Donor name is required');
                        return false;
                    }
                }

                var status = document.getElementById("postCode").getAttribute("postcodestatus");

                if((status == 404) & (isGiftAid == 1)){

                    $("#postcode_error").html('Valid postcode is required');
                    return false;

                    $('#postCode').val('');
                    
                }

                $('#postcode_error').addClass('hidden');
                address = $("#address1").val();
                town = $("#Town").val();
                postcode = $("postcode").val();

                // data for first ajax
                salutation = $('select[name="salutation"] option:selected').val();
                address  = $('#address').val();
                locality = $('#district').val();
                country  = $('#country').val();
                
                data_user = {
                    
                    salutation : salutation,
                    first_name: first_name,
                    last_name: last_name,
                    name: first_name + " " + last_name,
                    email: email_address,
                    password: makepass(),
                    status: 1,
                    address : address,
                    locality : locality,
                    country : country,
                    postcode : postcode
                }

                data_donation = {
                    donation_amount_monetory: donation_amt,
                    application_fees_charge: 0,
                    is_offline: 1,
                    status: 1,
                    campaign_id: camp_id,
                    is_giftaid : isGiftAid
                }
                
                $.ajax({
                    url: site_url + '/offlineregister',
                    dataType: "json",
                    method: "POST",
                    data: data_user,
                    success: function (res) {
                        if (res) {
                            data_donation['user_id'] = res;
                            $.ajax({
                                url: site_url + '/offlinedonation',
                                dataType: "json",
                                method: "POST",
                                data: data_donation,
                                success: function (res) {
                                    if (res) {
                                    } else {
                                        alert('Something, went wrong, please try again');
                                    }
                                    window.location = site_url + '/campaign/<?php echo $campaign->slug; ?>' ;
                                }
                            });
                        }
                    }
                });
            });
        });

    </script>
@stop