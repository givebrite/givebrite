@extends('site.layouts.master_newhome')

@section('title', 'Online Fundraising and Donation Platform - Give Brite')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/owl.theme.default.min.css')}}">
@stop

@section('content-header')
    <section class="why-us-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="why-us-scroll">
                        <ul class="why-us-main">
                            <li><span class="why-us-icon"><img src="{{ asset('site/images/why-us-icon1.png')}}"
                                                               alt="img"/></span>
                                <h4>Lower Fees</h4>
                                <p>over 50% of donors pay fees</p>
                            </li>
                            <li><span class="why-us-icon"><img src="{{ asset('site/images/why-us-icon2.png')}}"
                                                               alt="img"/></span>
                                <h4>Quick & Easy</h4>
                                <p>Setup is fast and clear</p>
                            </li>
                            <li><span class="why-us-icon"><img src="{{ asset('site/images/why-us-icon3.png')}}"
                                                               alt="img"/></span>
                                <h4>Secure</h4>
                                <p>We will always have your back</p>
                            </li>
                            <li><span class="why-us-icon"><img src="{{ asset('site/images/why-us-icon4.png')}}"
                                                               alt="img"/></span>
                                <h4>We Give Back</h4>
                                <p>We give back to the community</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@stop
@section('topbar')
    <section class="product-icon-wrap">
        <div class="container">
            <div class="row">
                <div class="product-small-main">
                    <!-- <div class=" carousel-inner product-small-list"> -->
                    <div class="owl-carousel owl-theme">
                    @if ( count($categoriesArray)>0 )

                        <!-- <ul id="owl-demo" class="owl-carousel"> -->
                        @foreach ($categoriesArray as $key => $category)
                            <!-- <li class="item active"> -->
                                <div class="item">
                                    <a class="{{ strtolower($category).'-sec' }}"
                                       href="{{ URL::route('campaign-by-category', $key)}}">
                                        {{ $category }}
                                    </a>
                                </div>
                                <!-- </li> -->
                        @endforeach
                        <!-- </ul> -->
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@stop
@section('content')
    <section class="campaign-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <!-- Nav tabs -->
                        <div class="tab-top-wrap">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#latestcampaigns"
                                                                          aria-controls="latestcampaigns" role="tab"
                                                                          data-toggle="tab"><span
                                                class="fa fa-star"></span><span
                                                class="tab-title">Latest Campaigns</span></a></li>
                                <li role="presentation"><a href="#almostfunded" aria-controls="almostfunded" role="tab"
                                                           data-toggle="tab"><span
                                                class="tab-title">Almost Funded</span></a></li>
                                <li role="presentation"><a href="#nearlyexpired" aria-controls="nearlyexpired"
                                                           role="tab" data-toggle="tab"><span class="tab-title">Nearly Expired</span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- Tab panes -->
                        <?php $campaign_base_path = Config::get('config.thumb_campaign_upload_path'); ?>
                        <div class="tab-content">
                            @include('site.partials.newpopular_campaign')
                            @include('site.partials.almostfunded_campaign')
                            @include('site.partials.nearlyexpired_campaign')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('script')
    <script src="{{ asset('site/js/jquery.js') }}"></script>
    <script src="{{ asset('site/js/bootstrap.min.js') }}"></script>
    <!-- <script src="{{ asset('site/js/owl.carousel.js') }}"></script> -->
    <script src="{{ asset('site/js/newowl.carousel.js') }}"></script>

    <script>
        $(document).ready(function () {
            $("#searchSubmit").submit(function (e) {
                e.preventDefault();
                var text_val = $("#search-txt").val();
                search_url = site_url + '/campaigns/type/cause?keyword=' + text_val;
                window.location = search_url;
            });

            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        loop: false,
                        items: 3,
                        nav: true,
                        navText: '',
                        dots: false
                    },
                    600: {
                        items: 4,
                        loop: false,
                        nav: true,
                        navText: '',
                        dots: false
                    },
                    1000: {
                        items: 12,
                        nav: true,
                        navText: '',
                        loop: false,
                        margin: 20,
                        dots: false
                    }
                }
            });
        });
    </script>
    <script>
        $(document).mouseup(function(e) 
            {
                var container = $(".list-group");

                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0) 
                {
                    container.fadeOut();
                }
            });


        $(function () {

            $('#slide-submenu').on('click', function () {
                $("body").removeClass('mobile-scroll');
                $(this).closest('.list-group').fadeOut('slide', function () {
                    $('.mini-submenu').fadeIn();
                });

            });

            $('.mini-submenu').on('click', function () {
                if ($(window).width() <= 767) {
                    $("body").addClass('mobile-scroll');
                }
                $(this).next('.list-group').toggle('slide');
                $('.mini-submenu').hide();
            });

            $('#myCarousel .carousel').carousel({
                interval: 5000 //changes the speed
            });

            $("#search-header").click(function () {
                $('html').one('click', function () {
                    $(".search-wrap").show();
                    $("#search-txt").focus();
                });
            });

            $(".search-wrap").focusout(function () {
                $(".search-wrap").hide();
            });

//        $(window).click(function () {
//            $(".search-wrap").hide();
//        });
            $('.product-small-main .owl-prev*').hide();

            $(".product-small-main .owl-next").click(function () {
                $('.product-small-main .owl-prev').show();
            });

        });

    </script>
    <script>
        $(window).scroll(function () {
            if ($(this).scrollTop() > 400) {
                $('header').addClass("sticky");
            }
            else {
                $('header').removeClass("sticky");
            }
        });
    </script>
@stop