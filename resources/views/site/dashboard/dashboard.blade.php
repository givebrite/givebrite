@extends('site.layouts.master')

@section('title', 'Dashboard')

@section('content')
<div class="container">
    @include('site.partials.breadcrumb')
    <div class="row">
        <div class="col-md-3">
            @include('site.partials.dashboard_sidebar')
        </div>
        <div class="col-md-9 innerRightCol">
            <h2 class="heading">Dashboard</h2>
            <div class="subHeading">Current Campaigns</div>
            <div class="campaignList">
                <ul class="clearfix listEplore">
                    @if ( count($campaignsList)>0 )       
                        @foreach ($campaignsList as $campaign)
                        <li>
                        <div class="imgContainer">
                            <img src="images/campaigns/{{ $campaign->campaign_image }}">
                        </div>
                        <div class="listDetails clearfix">
                            <div class="detialsLeft text-capitalize">
                                <div class="price">
                                    {{ $campaign->goal_amount_monetary?'&pound;'.number_format((float)$campaign
                                    ->goal_amount_monetary):0}}
                                </div>
                                <div class="type">
                                {{ $campaign->campaign_name?$campaign->campaign_name:''}}
                                </div>
                                <div class="location">
                                {{ $campaign->location?$campaign->location:''}}
                                {{ $campaign->city?', '.$campaign->city:''}}
                                </div>
                            </div>
                            <div class="detialsRight">
                                <div class="valuePercentage">
                                    {{ $campaign->id?$campaign->id.'%':''}}
                                </div>
                            </div>
                        </div>
                        <div class="btn-block">
                            <button class="sm-blue-btn">Update</button>
                        </div>
                    </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="subHeading">My Account</div>
            <div class="row">
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/profile.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Profile</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/facebook-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Facebook</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/twitter-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Twitter</div>
                </div>
            </div>
            <div class="subHeading">Campaigns</div>
            <div class="row">
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/campaign-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Campaigns</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/donation-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Donations</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/comment-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Comments</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/reward-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Rewards</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/insights-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Insights</div>
                </div>
            </div>
            <div class="subHeading">Marketing</div>
            <div class="row">
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/events-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Events</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/social-media.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Social Media 
                        Marketing</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/sendtext-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Send Texts</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/email-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Send Emails</div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="imgBlock">
                        <img src="{{ asset('site/images/print-lg.png') }}" alt="Profile" />
                    </div>
                    <div class="iconText">Order Print Material</div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop