@extends('site.layouts.master')

@section('title', !empty($campaign->campaign_name) ? ucfirst($campaign->campaign_name) : 'Campaign Detail')

@section('style')
    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <style>
        @media (max-width: 767px) {
            .tab-top-wrap {
                border-bottom: none;
            }
        }
        .red{
            color:red !important;
        }
    </style>
@stop

@section('content')
    <?php
    $base_path = Config::get('config.large_campaign_upload_path');
    $base_team_path = Config::get('config.large_team_upload_path');
    
    $base_path_profile = !empty($campaign->createrRole) && $campaign->createrRole == '3' ? Config::get('config.thumb_charity_logo_upload_path') : Config::get('config.thumb_profile_image_upload_path');

    $charityLogoBasePath = Config::get('config.thumb_charity_logo_upload_path');
    $goalPercent = !empty($totalAmountRaised) ? floor(($totalAmountRaised / $campaign->goal_amount_monetary) * 100) : 0;

    if ($campaign->themeValue != null) {
        $hex = $campaign->themeValue;
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        $bg_color = "rgba($r, $g, $b, 0.2)";
    }

    if (isset($campaign->charityId) && !empty($campaign->charityId)) {
        $charity_image_view = $campaign->charity_logo;
    } else {
        $charity_image_view = $campaign->charityLogo;
    }
    ?>
    <?php $base_path_charity = Config::get('config.thumb_campaign_upload_path'); ?>
    @if(isset(Session::all()['user_session_data']['id']) && Session::all()['user_session_data']['id'] == $campaign->user_id)
        <div class="edit-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('/team/'.$teamDetails->team_id.'/step-one-edit')}}" title="" class="edit-icon"><i
                                    class="fa fa-pencil"></i> <span>Edit</span> </a>
                                               

                        <a href="{{url('team')}}/{{$teamDetails->team_id}}/status/{{$teamDetails->status}}" title="" 
                        class="push-icon">

                         @if($teamDetails->status == 1) 
                                 <i class="fa fa-pause"></i> <span>Pause</span>
                            @else
                                <i class="fa fa-play"></i> <span>Play</span>
                            @endif   

                        </a>

                        <a href="{{url('team')}}/{{$teamDetails->team_id}}/delete" title="" class="delete-icon"><i
                                    class="fa fa-trash"></i> <span>Delete</span> </a>

                        <a href="#" data-toggle="modal" data-target="#myModal1" title="" class="post-icon"><i
                                    class="fa fa-edit"></i> <span>Post an update</span> </a>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="bg_new" style="background:@if(isset($bg_color)) {{$bg_color}} @else #f0fafc @endif">
        <div class="mobile-social hidden-lg hidden-md hidden-sm" style="display: none;">
            <div class="col-xs-6 fb-mbl">
                <a href="javascript:void(0);" class="facebook-btn"
                   onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer', 'toolbar=0,status=0,width=500,height=400');"
                ><i class="fa fa-facebook-square"></i>Share</a>
            </div>
            <div class="col-xs-6 dnt-mbl">
                <a href="{{route('campaign.donation', $campaign->slug)}}" class="donate-btn">Donate Now</a>
            </div>

        </div>

        <div class="container bgWhite campaign-detail-left">
            <div class="row">
                <div class="col-sm-12 hidden-xs">
                    <h4>{{!empty($teamDetails->team_name) ? ucfirst($teamDetails->team_name) : ''}}</h4>
                    <div class="created-text"><i
                                class="fa fa-clock-o"></i>Created {{$teamDetails->created_at}} {{ Carbon\Carbon::parse($teamDetails->created_at)->diffForHumans() }}
                    </div>

                <!--                <div class="subCategory">{{!empty($campaign->categoryName) ? ucfirst($campaign->categoryName) : ''}}</div>-->
                <!--                @if(!empty($campaign->createrName)) <div class="subCategory">Fundraiser: {{!empty($campaign->createrName) ? ucfirst($campaign->createrName) : ''}}</div>@endif-->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div class="coverOuter">
                    @if($campaign->media_type=='youtube' && !empty($campaign->media_url))
                    <div class="videoWrapper">
                        <div class="playBtn">
                            <iframe width="100%" height="100%" src="{{$campaign->media_url}}" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    @else
                    <!-- <div class="coverImage" style="background: rgba(0, 0, 0, 0) url('{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($base_path.$campaign->campaign_image ) ?  asset($base_path.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}') no-repeat scroll center center / cover ;"></div> -->
                        <div class="row">
                            {!! HTML::image(isset($teamDetails->cover_photo) && !empty($teamDetails->cover_photo) && File::exists($base_path.$teamDetails->cover_photo ) ?  asset($base_path.$teamDetails->cover_photo)  : asset('images/default-images/default-campaign.jpg'), 'Campaign Picture', array('class' => 'coverImage'))!!}
                        </div>
                    @endif


                    <div class="col-sm-12 hidden-lg hidden-sm row">
                        <h4>{{!empty($campaign->campaign_name) ? ucfirst($campaign->campaign_name) : ''}}</h4>
                        <div class="created-text"><i
                                    class="fa fa-clock-o"></i>Created {{ Carbon\Carbon::parse($campaign->created_at)->diffForHumans() }}
                        </div>
                    </div>

                    <div class="social-links-wrapper hidden-xs">
                        <a class="btn btn-tw" href="#"
                           onclick="popUp=window.open('https://twitter.com/share?url={{urlencode(Request::url())}}&text={{urlencode($campaign->campaign_name)}}', 'popupwindow', 'scrollbars=yes,width=500,height=400'); popUp.focus(); return false"><i
                                    class="fa fa-twitter"></i></a>
                        <a class="btn btn-fb" href="#"
                           onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer','toolbar=0,status=0,width=500,height=400');"
                           href="javascript: void(0)"><i class="fa fa-facebook"></i></a>
                        <a class="btn btn-donor"><i class="fa fa-heart"></i><span>{{ !empty($totalDonors) ? $totalDonors : 0 }}
                                <span>{{ !empty($totalDonors) && $totalDonors > 1 ? "donors" : "donor" }}</span></span></a>
                    </div>
                    {{--<!--                    <div class="coverImageContent" style="display:none;">--}}
                    {{--<div class="campaignDetials">--}}
                    {{--<div class="shareDonor">--}}
                    {{--<ul>--}}
                    {{--<li class="shares">1.1k <span>shares</span></li>--}}
                    {{--<li class="donors">{{ !empty($totalDonors) ? $totalDonors : 0 }} <span>{{ !empty($totalDonors) && $totalDonors > 1 ? "donors" : "donor" }}</span></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="progressOuter" style="width:150px;height:150px;margin:18px auto;">--}}
                    {{--<div class="percent" style="width:100%;height:100%;">--}}
                    {{--<p style="display:none;">{{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>-->--}}
                </div>


                <div class="hidden-lg hidden-sm">
                <!--  <div class="progressOuter progress-detail-class"
                         style="width:150px;height:150px;margin:25px auto 10px;">
                        <div class="percent progress-text-size" style="width:100%;height:100%;">
                            <p style="display:none;">{{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}</p>
                        </div>
                    </div> -->
                    <!-- This will be shown on the mobile view-->
                    <div class="progress-circle-wrapper">
                        <svg class="progress-circle-image" width="120" height="120" viewBox="0 0 120 120">
                            <circle class="progress-circle__meter" cx="60" cy="60" r="54" stroke-width="5"/>
                            <circle style="stroke:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif" class="progress-circle__value1" cx="60" cy="60" r="54" stroke-width="12"/>
                        </svg>
                        <div class="progress-text"
                             style="color:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif">
                            {{!empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent}}%
                        </div>
                    </div>
                    <div class="donate-value">
                        <span class="aValue"
                              style="color:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif"> <span
                                    class="pound_small">&pound;</span>{{Helper::showMoney($totalAmountRaised,$campaign->campaign_currency)}}</span>
                        of <span
                                class="oValue"> &pound;{{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}</span>
                    </div>
                    @if(!empty($campaign->campaign_duration) && $campaign->campaign_duration == "enddate" && Helper::isExpired($campaign->end_date) == true)
                        <div class="subCategory">
                            Campaign has been expired
                        </div>
                    @elseif($user['role'] != 'Charity')
                        <p class="donateNowBtn">
                            <a href="{{route('campaign.donation', $campaign->slug)}}" type="button"
                               class="lt-green-lg fullwidth text-center"
                               style="background:@if($campaign->themeValue != null) {{$campaign->themeValue}} !important; @endif">Donate Now</a>
                        </p>
                        <a href="#" class="share-facebook-btn fullwidth text-center" style="background:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif"
                           onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer', 'toolbar=0,status=0,width=500,height=400');"
                           href="javascript:void(0);"><i class="fa fa-facebook-square"></i>Share on Facebook</a>
                        <div class="profile-wrapper">

                        <span class="user-profile-charity">
                            @if(file_exists(public_path('modules/auth/thumb-profile-images/'.$campaign->userProfilePic)))
                                {!! HTML::image('modules/auth/thumb-profile-images/'.$campaign->userProfilePic) !!}
                            @else
                                {!! HTML::image('images/userDP.png') !!}
                            @endif
                            <span class="user-name">{{$campaign->createrName}}
                                <span class="user-profile">
                                    <span class="user-tags"><i class="fa fa-tags"></i>{{$campaign->categoryName}}</span>
                                    <span class="user-tags"><i class="fa fa-map-marker"></i>{{$campaign->locationName}}</span>
                                </span>
                            </span>

                        </span>
                        </div>
                    @endif
                </div>
                @if($campaign->charity_id)
                    <div class="raising-money">
                        <div class="col-sm-12 raising-money-for">Team Raising money for:</div>

                        <div class="border-btm col-sm-2">
                            <p class="syria-img">
                                <a href="{{url('/campaign/'. $campaign->slug)}}">
                                    
                                    <img class="charity-logo"
                                         src="{{ (!empty($campaignDetails[0]->campaign_image) && File::exists($base_path_charity.$campaignDetails[0]->campaign_image) && isset($campaignDetails[0]->campaign_image)) ? asset($base_path_charity.$campaignDetails[0]->campaign_image) : asset('images/default-images/default_logo.png') }}"
                                         alt="">
                                </a>
                            </p>
                        </div>

                        <div class="bigBlack col-sm-10 charity-details">
                            <a href="{{url('/campaign/'. $campaign->slug)}}"><h4>{{$campaign->campaign_name}}</h4>
                            </a>
                            
                        </div>

                        <div class="col-sm-12">
                            <div class="border-bottom mt20"></div>
                        </div>
                    </div>
                @endif
                <div class="tab-card ">

                    <div class="tab-top-wrap">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#about" aria-controls="register" role="tab" data-toggle="tab"><span
                                            class="tab-title">About</span></a>
                            </li>
                            <li role="presentation">
                                <a href="#updates" aria-controls="login" role="tab" data-toggle="tab"><span
                                            class="tab-title">Updates</span><span class="update-count">{{$count}}</span></a>
                            </li>
                            <?php if($campaign->stravaAccessToken != null){?>
                            <li role="presentation">
                                <a href="#training" aria-controls="login" role="tab" data-toggle="tab"><span
                                            class="tab-title">Training</span></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="about">

                            <div class="charityList" id="description-content large-word">
                                <!--<div class="postheading" >About</div>-->
                            <!--                                <div class="postPublish">{{!empty($campaign->createrName) ? "Published by ".$campaign->createrName." - ".Helper::showDate($campaign->created_at) :''}}</div>-->
                                <div class="postdescription readmore-content f19"> {!! isset($teamDetails->team_description) ? $teamDetails->team_description :'' !!}</div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="updates">
                            <div id="campaign-update-div">
                                @include("site.partials.campaign_updates")
                            </div>
                        </div>
                        <?php if($campaign->stravaAccessToken != null){ ?>
                        <div role="tabpanel" class="tab-pane" id="training">
                            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate col-md-offset-6"></span>
                        </div>
                        <?php } ?>

                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-3">
                <div class="hidden-xs">
                <!-- <div class="progressOuter progress-detail-class"
                         style="width:150px;height:150px;margin:25px auto 10px;">
                        <div class="percent" style="width:100%;height:100%;">
                        <p style="display:none;">{{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}</p>
                        </div>
                        
                    </div> -->
                    <div class="progress-circle-wrapper">
                        <svg class="progress-circle-image" width="120" height="120" viewBox="0 0 120 120">
                            <circle class="progress-circle__meter" cx="60" cy="60" r="54" stroke-width="5"/>
                            <circle style="stroke:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif" class="progress-circle__value" cx="60" cy="60" r="54" stroke-width="12"/>
                        </svg>
                        <div class="progress-text"
                             style="color:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif">
                            {{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}%
                        </div>
                    </div>
                    <div class=" donate-value
                        ">
                        <span class="aValue"
                              style="color:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif">&pound;{{Helper::showMoney($totalAmountRaised,$campaign->campaign_currency)}}</span>
                        of <span
                                class="oValue">&pound;{{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}</span>
                    </div>
                    @if(!empty($campaign->campaign_duration) && $campaign->campaign_duration == "enddate" && Helper::isExpired($campaign->end_date) == true)
                        <div class="subCategory">
                            Campaign has been expired
                        </div>
                    @elseif($user['role'] != 'Charity')
                        <p class="donateNowBtn">
                            <a href="{{URL::route('campaign.donation',$campaign->slug)}}" type="button"
                               class="lt-green-lg fullwidth text-center" id="donate_now"
                               style="background:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif">Donate
                                Now</a>
                        </p>
                        <a href="javascript: void(0)" class="share-facebook-btn fullwidth text-center" style="background:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif"
                           onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer','toolbar=0,status=0,width=500,height=400');"
                        ><i class="fa fa-facebook-square"></i>Share on Facebook</a>
                        <div class="profile-wrapper">


                        <span class="user-profile-charity">
                            @if($campaign->userProfilePic == '')
                                {!! HTML::image('images/userDP.png') !!}
                            @elseif(file_exists(public_path('modules/auth/thumb-profile-images/'.$campaign->userProfilePic)))
                                {!! HTML::image('modules/auth/thumb-profile-images/'.$campaign->userProfilePic) !!}
                            @else
                                {!! HTML::image('images/userDP.png') !!}
                            @endif
                            <span class="user-name">{{$campaign->createrName}}
                                <span class="user-profile">
                                    <span class="user-tags"><i class="fa fa-tags"></i>{{$campaign->categoryName}}</span>
                                    <span class="user-tags"><i class="fa fa-map-marker"></i>{{$campaign->locationName}}</span>
                                </span>
                            </span>

                        </span>
                      
                            
                        </div>

                </div>

                @elseif($user['role'] == 'Charity')
                    <span class="launch-note label label-info">Charity can't donate to any campaign.</span>
                @endif

                <?php $count = count($teamMembers) ?>
                <div class="join-team-section">
                    <div class="team-section-header">
                        <h5>Member (<?php echo $count; ?>)</h5><span>
                        <a href="{{url('/team/join-team')}}/{{$teamDetails->team_id}}" class="teamJoinBtn"> + Join Team</a></span>
                    </div>
                </div>

                <div class="latest-donations-aside">

                  <div class="team-members-list">
                        
                        @if($count > 0)
                        <ul>
                            
                            @foreach($teamMembers as $teamMember)    
                            
                            <li><span><img src="http://givebrite.dev/team/crown.png" alt=""></span>
                                <div class="team-member-circle-wrapper">
                                    <svg class="team-member-circle-image" width="73" height="73" viewBox="0 0 120 120">
                                        <circle class="team-member-circle__meter" cx="60" cy="60" r="54" stroke-width="8"></circle>
                                        <circle style="stroke-dasharray: 422.292; stroke-dashoffset: 203.575;" class="team-member-circle__value" cx="60" cy="60" r="54" stroke-width="12"></circle>

                                    </svg>
                                    <div class="team-member-img campain-left-img">
                                        
                                      @if($teamMember->profile_pic == '')

                                            {!! HTML::image('images/userDP.png') !!}
                                        

                                    @elseif(file_exists(public_path('modules/auth/thumb-profile-images/'.$teamMember->profile_pic)))
                                            
                                            {!! HTML::image('modules/auth/thumb-profile-images/'.$teamMember->profile_pic) !!}
                                        
                                        @else
                                            {!! HTML::image('images/userDP.png') !!}
                                        @endif




                                    </div>
                                </div>
                            <div class="team-member-info"><label><span>{{$teamMember->first_name }} {{$teamMember->sur_name }}</span><span class="team-amount">£ {{$teamMember->totalDonations}}</span><small>{{$teamMember->totalDonationsCount}} donation</small></label>
                            </div>
                            </li>

                            @endforeach

                         </ul>  

                        @else 
                            <span class="launch-note label label-info">There are no team members.</span>
                        @endif

                </div>



            <!--
                <p>
                    <a href="#" class="green-btn fullwidth text-center">share</a>
                </p>
                -->
            </div>
        </div>
   

    <!--<div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                {!! Form::open(array('route' => ['donate.offline', $campaign->slug], 'novalidate' => 'novalidate', 'id' => 'offline_donation')) !!}
                <div class="modal-header">
                    <h4 class="modal-title">Add Offline Donation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="modal-donation-amout">
                        <label for="donation_amount">Donation amount <span class="astrikBlock">*</span></label>
                        <div class="inputBlock">
                            <p class="gbpBtn">
                                <input type='hidden' name='goal_amount_type' value='monetary' placeholder="&pound;">
                                <span class="sterling">&pound;</span>
                                {!! Form::text('amount',Session::has('donationAmount') ? Session::get('donationAmount') : '' , ['id'=>'donation_amt']) !!}
                            </p>
                            <p id="amt_error" class="text-danger"></p>
                            {!! $errors->first('goal_amount_monetary', '<div class="text-danger">:message</div>') !!}
                        </div>
                        {{--<input type="text" id="donation_amt" name="amount"--}}
                        {{--value="{{ Session::has('donationAmount') ? Session::get('donationAmount') : ''}}"--}}
                        {{--placeholder="&pound;"/>--}}
                    </div>
                    @if(!empty($donation))
                        <div class="choose-donation">
                            <span>Choose donation type</span>
                        </div>
                    @endif
                    <div class="select-donation">
                        @foreach($donation as $d)
                            <div class="select-donation-type">
                                <label class="control control--radio">
                                    <input
                                            @if($d->is_default)
                                            checked
                                            @endif
                                            type="radio" name="default" value="{{$d->id}}">
                                    {{$d->title}}
                                    <div class="control__indicator"></div>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-donation-amout">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="donation_amount">Donor Name <span class="astrikBlock">*</span></label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" id="first_name" name="firstname" placeholder="First Name"/>
                                <p id="donor_name_error" class="text-danger"></p>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" id="last_name" name="lastname" placeholder="Last Name"/></div>
                        </div>
                    </div>
                    <div class="modal-donation-amout">
                        <label for="donation_amount">Email Address <span class="astrikBlock">*</span></label>
                        <input type="email" id="email_address" name="email" placeholder="Email Address"/>
                        <p id="donor_email_error" class="text-danger"></p>
                    </div>
                    <div class="modal-donation-amout">
                        <label class="control keep-donor">
                            <input checked type="checkbox" name="anonymous" value="Keep donor anonymous"
                                   checked="checked">
                            Keep donor anonymous
                            <div class="control_checkbox"></div>
                        </label>

                    </div>
                    <div class="modal-donation-amout">
                        <label class="control charity-text">
                            <input checked type="checkbox" name="default" value="Keep donor anonymous">
                            Tick here if you would like Charity Name to reclaim the tax you have paid on all your
                            donations made in the last four years, and any future donations you may make.*
                            <div class="control_checkbox"></div>
                        </label>
                    </div>
                    <div class="modal-donation-amout address-select">
                        <label for="donation_amount">Address Information</label>
                        <input type="text" id="address1" name="amount" placeholder="Address"/>
                        <input type="text" id="Town" name="amount" placeholder="Town"/>
                        <input type="text" id="postcode" name="amount" placeholder="Postcode"/>
                    </div>

                </div>
                <div class="modal-footer">
                {!! Form::submit('Add Donation', array('class'=>'btn btn-success')) !!}
                <!-- <a href="#" data-dismiss="modal">Cancel</a> 
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>-->

    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="upd_title">Update Box</h4>
                </div>
                <!--$teamDetails->team_id-->
                {!! Form::open(['route' => ['team-updates.store', $teamDetails->team_id], 'id'=>'update-form']) !!}
                <div class="modal-body">
                    <div class='container'>
                        <div class="">
                            {!! Form::textarea('updates', null, ['placeholder'=>'New Update ..','class' => '']) !!}
                            {!! $errors->first('updates', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success">Save</button>
                    <button class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
</div>
@stop
@section('script')
    <script type="text/javascript">
        // $("#donate_now").click(function(){
        //     $("#donation_amount").focus();
        // });
        $('#myModal').on('shown.bs.modal', function () {
            $('#donation_amount').focus();
        })
    </script> <script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.27&key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&libraries=drawing&callback=initMap"
            async defer></script>
   
    <script type='text/javascript' src="{{ asset('js/campaign-detail.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>



    <script src="{{asset('site/js/fusioncharts.js')}}"></script>
    <script src="{{asset('site/js/fusioncharts.charts.js') }} "></script>
    <script src="{{asset('site/js/fusioncharts.theme.management-dashboard.js') }}"></script>
   
@stop
