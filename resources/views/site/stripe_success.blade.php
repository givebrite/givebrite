@extends('site.layouts.master')

@section('title', 'Page')

@section('content')
<div class="stripe-account-div">
    <div class="connect-stripe clearfix" style="">
        <div class="stripe-text text-center laungSlogan">
            <h4> You are now ready to take donations!</h4>
        </div>
        <div class="stripe-loading-img text-center clearfix">
            <img src="{{ asset('site/images/color-check.png') }}">
        </div>

        <div class="stripe-connect-button text-center clearfix">
            <a href="{{URL::Route('campaigns.create')}}" class="btn btn-info">Create Campaign</a>
        </div>
    </div>
</div>
@stop




