@extends('site.layouts.master')

@section('title', 'Campaigns')

<?php $campaign_base_path = Config::get('config.thumb_campaign_upload_path'); ?>


@section('content')
<div class="campaignListVertical">
    <div class="container">
        <div class="row">
            <!-- Search Bar-->
            @if(!empty($campaignCategory))
            {!! Form::open(array('route' => ['campaign-by-category',$campaignCategory],'method'=>'get')) !!}
            @elseif(!empty($campaignType))
            {!! Form::open(array('route' => ['campaign-by-type',$campaignType],'method'=>'get')) !!}
            @else
            {!! Form::open(array('route' => 'campaign-list-home','method'=>'get')) !!}
            @endif
            <div class="col-xs-12">
                <p class="gbpBtn mainContent">
                    {!! Form::text('keyword',!empty($filters['search_keyword']) ? $filters['search_keyword'] : null , ['class'=>'dark form-control','placeholder' => 'Search By Name, City And Location...','id'=>'search_bar','autocomplete'=>'off']) !!}
                    <button class="lt-green-lg mobile-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    
                    <div class="search-dropdown">
                                              

                    </div>
                </p>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabOuter">
                    <div class="tab-content ">
                        <div id="popular" class="tab-pane fade in active">
                            <ul class="clearfix listEplore">
                                @if ( count($campaigns)>0 )       
                                @foreach ($campaigns as $campaign)
                                <li class="clearfix">

                                    <a href="{{URL::route('campaign.show', $campaign->slug)}}">
                                        <div class="imgContainer col-sm-3" style="background: rgba(0, 0, 0, 0) url('{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($campaign_base_path.$campaign->campaign_image ) ?  asset($campaign_base_path.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}') no-repeat scroll center center / cover;">

                                        </div></a>
                                    <div class="listDetails clearfix col-sm-9">
                                        <div class="detialsLeft">
                                            <div class="type">
                                                <a href="{{URL::route('campaign.show', $campaign->slug)}}"> 
                                                    {{ $campaign->campaign_name?$campaign->campaign_name:''}}  
                                                </a>                                   
                                            </div>
                                            <div class="price">
                                                {{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}
                                            </div>
                                            <div class="location">
                                                {{Helper::showAddress($campaign->cityName,$campaign->locationName,50)}}
                                            </div>

                                    <?php $data = Helper::substr_close_tags($campaign->desc, $limit = 150) ?> 

                                            {!! $data !!}
                                        </div>
<!--                                                                                <div class="detialsRight">
                                                        <div class="progressOuter" style="width:120px;height:120px;margin:10px auto;">
                                                            <div class="percent" style="width:100%;height:100%;">
                                                                <p style="display:none;">{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}</p>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                    </div>
                                </li>
                                @endforeach
                                @else
                                <li> <h4>No Record Found.</h4></li>
                                @endif    
                            </ul>
                            
                        </div>

                       

                        <div id="myarea" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Some content in menu 1.</p>
                        </div>
                        <div id="myarea" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Some content in menu 1.</p>
                        </div>

                         <div class="paginationOuter">
                            <nav>
                                @include('site/partials/pagination', ['paginator' => $campaigns])
                            </nav>
                         </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('content-footer')
<h3>Let’s Create Your <span>Own</span> Campaign</h3>
<p>Easily create your own campaign and start accepting donations</p>
<p>
    <a href="{{ URL::route('campaigns.create') }}" class="blue-btn-lg">Get Started</a>
</p>
@stop

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
<script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
<script src="{{ asset('js/explore-hide-show.js') }}"></script>
<script>
$(function () {
    $('.percent').percentageLoader({
        valElement: 'p',
        strokeWidth: 10,
        bgColor: '#d9d9d9',
        ringColor: '#37c2df',
        textColor: '#2C3E50',
        fontSize: '14px',
        fontWeight: 'normal'
    });

});


$( "#search_bar" ).keyup(function() {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "?keyword="+this.value+"",
        dataType: "json",
        type: "post",
        data:this.value,
        contentType: "application/json; charset=utf-8",
        
        success: function (data) {
            
            $(".search-dropdown").css("display", "block");
            $(".search-dropdown").html('<ul id="campaignList"></ul>');

            if(data['code'] == 200){

                $.each(data['campaigns'], function(key, item) 
                {   
                    // $('#campaignList').append('<li id="listitem">' + item.campaign_name + '</li>');
                    $('#campaignList').append(
                     '<li><a href="/campaign/'+item.slug+'">' + item.campaign_name + '</a></li>');               
                   

                });
                

            } else if(data['code'] == 404) {
                $(".search-dropdown").css("display", "none");
                $(".search-dropdown").html('<ul id="campaignList"></ul>');
            
            } else {

                $('#campaignList').append('<li id="listitem">No campaigns found</li>');
            
            }
            
            console.log(data);
            console.log(data['code']);

        },
        error:function (xhr, ajaxOptions, thrownError){

            if(xhr.status==404) {
               
               console.log('No post code found');
            }
        }
    });
    
});

</script>

@stop

