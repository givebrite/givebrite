@extends('site.layouts.master')

@section('title', 'Charities')

@section('style')
<link rel="stylesheet" href="{{ asset('css/charity-list.css') }}">
@stop

@section('content')
<?php $base_path_charity = Config::get('config.thumb_charity_logo_upload_path'); ?>
<?php $base_path_campaign = Config::get('config.thumb_campaign_upload_path'); ?>
<div class="charityListPage">
    <div class="container">
        <div class="row">
            <div class="headTitleGray col-sm-12">
                <h4>Charities and Non-Profit Organisations</h4>
            </div>
            <div class="col-xs-12">
                <p class="gbpBtn mainContent">
                    {!! Form::text('keyword',!empty($filters['search_keyword']) ? $filters['search_keyword'] : null , ['class'=>'dark form-control','placeholder' => 'Search for charity...','id' => 'searchBox']) !!}
                    <button class="lt-green-lg mobile-btn" id="charity-search">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </p>
            </div>


        </div>
        <div class="row charityDetails-upper">
            <div class="search-result col-sm-12">Search results for <span id="searchtxt"></span></div>

            <input type='hidden' id='charity_url' value='{{URL::route('charity.list_ajax')}}'>
            <div class="col-sm-12 col-lg-12 charitiesBlock border-btm">

                <div class="row  charities-upper">
                    <div class="scroll">
                        @include("site.partials.charity-div-append")
                    </div>

                    <div class="headTitleGray col-sm-12">
                    </div>
                </div>
                <div id="charity-pagination">
                    @include("site.partials.paginationdiv")
                </div>
                <!--                <div class="charityDetails-lower headTitleGray">
                                    <div class="col-sm-12 loadMoreBlock">
                                        <p>
                                            <button class=" mobile-btn" id='load-more-charity' style="display:none">Load More Charities</button>
                                        </p>
                                    </div>
                                    @if(count($campaignList) > 0)
                                    <h4>Fundraisers</h4>
                                    <ul class="donorList row">
                                        @foreach($campaignList as $campaign)
                                        <li class="item col-xs-12 col-sm-6 col-md-4 charity-fundraisers">
                                            <div class="cardUser">
                                                <a href="{{URL::route('campaign.show', $campaign->slug)}}"><div style='background-image:url("{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($base_path_campaign.$campaign->campaign_image ) ?  asset($base_path_campaign.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg') }}")' class="donorImg"></div></a>
                                                <div class="cardDetail">
                                                    <div class="charityAmount"><span>{{Helper::showMoney($campaign->goal_amount_monetary)}}</span>
                                                    </div>
                
                                                    <a href="{{URL::route('campaign.show', $campaign->slug)}}">  <p class="campTitle"> {{ $campaign->campaign_name ? str_limit(ucfirst($campaign->campaign_name) , $limit = 15, $end = '...') :''}} </p> </a>                                      
                                                    <p class="location">
                                                        {{Helper::showAddress($campaign->cityName,$campaign->locationName,15)}}
                                                    </p>
                
                                                    <div class="progressOuter inherit" style="width:70px;height:70px;">
                                                        <div class="percent" style="width:70px;height:70px;">
                                                            <p style="display:none;">{{Helper::showGoalPercentage($campaign->id, $campaign->goal_amount_monetary )}}</p>
                                                        </div>
                                                    </div>
                
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>-->
            </div>
        </div>
    </div>
</div>

@stop

@section('content-footer')
<h3>Fundraising for Registered Charities</h3>
<p>Easily create your own campaign and start accepting donations</p>
<p>
    <a href="{{URL::route('register')}}" class="blue-btn-lg">Get Started</a>
</p>
@stop
@section('script')
<script src='{{ asset('site/js/infinite-loading.js').'?num='.mt_rand(1000000, 9999999) }}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
<script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
<script>
$(function () {
    $('.percent').percentageLoader({
        valElement: 'p',
        strokeWidth: 5,
        bgColor: '#d9d9d9',
        ringColor: '#37c2df',
        textColor: '#9f9f9f',
        fontSize: '18px',
        fontWeight: 'bold',
    });
    var data = $("#searchBox").val();
    $("#searchtxt").html(data);
});
$(document).ready(function () {
    $("#searchBox").keyup(function () {
        var data = $("#searchBox").val();
        $("#searchtxt").html(data);
    });
});
</script>
<script src="{{ asset('js/explore-hide-show.js') }}"></script>
@stop

