@extends('site.layouts.master')

@section('title', !empty($charityDetail->charity_name) ? $charityDetail->charity_name : 'GiveBrite Charity')

@section('style')
<link rel="stylesheet" href="{{ asset('css/charity-list.css') }}">
@stop

@section('content')
<?php $base_path_charity = Config::get('config.thumb_charity_logo_upload_path'); ?>
<?php $base_path_user = Config::get('config.thumb_profile_image_upload_path'); ?>
<?php $base_path_campaign = Config::get('config.thumb_campaign_upload_path'); ?>

<input type="hidden" id='baseUrl' value="{{ url() }}" />
<div class="container bgWhite charityDetailsPage">
    <div class="row charityDetails-upper">

        <div class="col-sm-12 col-md-3 col-lg-3 border-btm">
            <p class="syria-img">
                <img class="charity-logo-detail" src="{{ (!empty($charityDetail->charity_logo) && File::exists($base_path_charity.$charityDetail->charity_logo ) && isset($charityDetail->charity_logo)) ? asset($base_path_charity.$charityDetail->charity_logo) : asset('images/default-images/default_logo.png') }}" alt="">
            </p>
            <div class="bigBlack hidden-sm  hidden-md  hidden-lg">
                <h4>{{ $charityDetail->charity_name }}</h4>
                <div class="subCategory">{{ $charityDetail->country }}</div>
            </div>
            <p>
                {{--<button name="{{ csrf_token() }}" class="lt-green-lg  text-center" id="{{ $charityDetail->id }}" onclick="setCharityIdForCampaignCreation(this)">Fundraise for us</button>--}}
                <a href="{{url('/campaigns/create?charity='.$charityDetail->id)}}" class="lt-green-lg text-center">Fundraise for us</a>
            </p>
            <div class="charity-Contlinks">
                @if(!empty($charityDetail->registration_no))
                <p class="charityNumber">
                    Registered charity number {{ $charityDetail->registration_no }}
                </p>
                @endif
                <p>
                    @if(substr($charityDetail->web, 0, 3) == 'WWW' || substr($charityDetail->web, 0, 3) == 'www')
                        <a target="blank" href="http://{{ $charityDetail->web }}" >{{ $charityDetail->web }}</a>
                    @else
                        <a target="blank" href="{{ $charityDetail->web }}" >{{ $charityDetail->web }}</a>
                    @endif
                </p>
                <p>
                    <a href="mailto:{{ $email }}" >{{ $email }}</a>
                </p>


            </div>
        </div>

        <div class="col-sm-12 col-md-9 col-lg-9 border-btm">
            <div class="bigBlack hidden-xs">
                <h4>{{ $charityDetail->charity_name }}</h4>
                <div class="subCategory">{{ $charityDetail->country }}</div>
            </div>
            <div class="charityProDetails">
                <h4>Charity Profile</h4>
                <div class="details">
                    {{ $charityDetail->description }}
                </div>
            </div>
        </div>
    </div>



    <div class="charityDetailListing">
        <div class="charityDetails-lower">
            <ul class="donorList row item-grid">
                @foreach($charityCampaigns as $charityCampaign)

           <?php  $goalAmountMonetary =  Modules\Campaigns\Entities\Campaign::getGoalAmountMonetary($charityCampaign->id); ?>
                <li class="item col-xs-12 col-sm-6 col-md-4">
                    <div class="cardUser">
                        <a href="{{URL::route('campaign.show', $charityCampaign->slug)}}"><div style='background-image:url("{{isset($charityCampaign->campaign_image) && !empty($charityCampaign->campaign_image) && File::exists($base_path_campaign.$charityCampaign->campaign_image) ?  asset($base_path_campaign.$charityCampaign->campaign_image)  : asset('images/default-images/default-campaign.jpg') }}")' class="donorImg"></div></a>
                        <div class="cardDetail">
                            <div class="charityAmount"><span>{{ (!empty($charityCampaign->first_name)) ? $charityCampaign->first_name : $charityCampaign->charity_name }} {{ (!empty($charityCampaign->sur_name)) ? $charityCampaign->sur_name : "" }}</span> has raised
                            {{Helper::showMoney($goalAmountMonetary[0]->goal_amount_monetary, $goalAmountMonetary[0]->campaign_currency) }}
                            </div>
                            <p class="campTitle"><a href="{{URL::route('campaign.show', $charityCampaign->slug)}}">{{ $charityCampaign->campaign_name ? str_limit(ucfirst($charityCampaign->campaign_name) , $limit = 20, $end = '...') :''}}</a></p>
                            <p class="location">{{Helper::showAddress($charityCampaign->cityName,$charityCampaign->locationName,25)}}</p>
                            <div class="progressOuter inherit" style="width:70px;height:70px;">
                                <div class="percent" style="width:70px;height:70px;">

                                    <p style="display:none;">
                                     {{Helper::showGoalPercentage($charityCampaign->id, $goalAmountMonetary[0]->goal_amount_monetary )}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                @endforeach
            </ul>
        </div>
    </div>
    <div class="">
        <div class="charityDetails-lower">
            <ul class="donorList row item-grid">
                @foreach($charityComments as $charityComment)
                <li class="item col-xs-12 col-sm-6 col-md-4">
                    <div class="cardAnonymous">
                        <div class="donorDetails">
                            <div class="imgOuter">
                                <img src="{{ (isset($charityComment->profile_pic) && !empty($charityComment->profile_pic) && $charityComment->is_anonymous == 0) ? ((substr( $charityComment->profile_pic, 0, 4 ) === "http") ? $charityComment->profile_pic :  (File::exists($base_path_user.$charityComment->profile_pic) ? asset($base_path_user.$charityComment->profile_pic) : asset('images/default-images/default_profile.png'))) : asset('images/default-images/default_profile.png') }}" alt="" />
                            </div>
                            <div class="text">
                                <p class="name">{{ ($charityComment->is_anonymous == 0) ? ($charityComment->first_name." ".$charityComment->sur_name) : "Anonymous" }}</p>
                                <p>{{ Helper::showMoney( $charityComment->donation_amount_monetory) }}</p>
                            </div>
                        </div>
                        <div class="donorMsg readmore-content1">
                            {{ $charityComment->content }}
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@stop
@section('script')
<script type='text/javascript' src="{{ asset('js/readmore.js') }}"></script>
<script src='{{ asset('js/charity.detail.js').'?num='.mt_rand(1000000, 9999999) }}'></script>
<script src='{{ asset('js/charity.follow.js').'?num='.mt_rand(1000000, 9999999) }}'></script>
<script src='{{ asset('js/masonry.pkgd.min.js') }}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
<script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>
<script>
                                    $(function () {
                                    $('.percent').percentageLoader({
                                    valElement: 'p',
                                            strokeWidth: 5,
                                            bgColor: '#d9d9d9',
                                            ringColor: '#37c2df',
                                            textColor: '#9f9f9f',
                                            fontSize: '18px',
                                            fontWeight: 'bold'
                                    });
                                    $('.readmore-content').readmore({speed: 500});
                                    });
</script>

@stop