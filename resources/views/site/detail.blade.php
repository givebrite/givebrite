@extends('site.layouts.master')

@section('title', !empty($campaign->campaign_name) ? ucfirst($campaign->campaign_name) : 'Campaign Detail')

@section('style')

    <link rel="stylesheet" href="{{ asset('modules/campaigns/css/campaign.css') }}">
    <meta property="og:title" content="Donate to {{$campaign->createrName}}'s Campaign" />
    <meta property="og:image" content="{{ asset('/images/large-campaigns/'.$campaign->campaign_image) }}" />
    <meta property="og:url" content="{{url('/campaign')}}/{{$campaign->slug}}" />
    <meta property="og:description" content="{{$campaign->createrName}} is fundraising to raise money for {{$campaign->slug}}" />

    <style>
        @media (max-width: 767px) {
            .tab-top-wrap {
                border-bottom: none;
            }
        }
        .red{
            color:red !important;
        }
        @media (max-width:320px)  { 
            
            .sticky_header {
                margin-top: 90px;
            } 
        }

        @media (max-width:481px)  { 

            .sticky_header {
                margin-top: 90px;
            } 
        }
        
    </style>
@stop

@section('content')
    <?php
    $base_path = Config::get('config.large_campaign_upload_path');
    $base_path_profile = !empty($campaign->createrRole) && $campaign->createrRole == '3' ? Config::get('config.thumb_charity_logo_upload_path') : Config::get('config.thumb_profile_image_upload_path');

    $charityLogoBasePath = Config::get('config.thumb_charity_logo_upload_path');
    $goalPercent = !empty($totalAmountRaised) ? floor(($totalAmountRaised / $campaign->goal_amount_monetary) * 100) : 0;

    if ($campaign->themeValue != null) {
        $hex = $campaign->themeValue;
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        $bg_color = "rgba($r, $g, $b, 0.2)";
        $btn_color = "rgba($r, $g, $b, 0.8)";
    }

    if (isset($campaign->charityId) && !empty($campaign->charityId)) {
        $charity_image_view = $campaign->charity_logo;
    } else {
        $charity_image_view = $campaign->charityLogo;
    }
    ?>
    <?php $base_path_charity = Config::get('config.thumb_charity_logo_upload_path'); ?>
    @if(isset(Session::all()['user_session_data']['id']) && Session::all()['user_session_data']['id'] == $campaign->user_id)
        <div class="edit-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('/campaigns/'.$campaign->campaignId.'/edit')}}" title="" class="edit-icon"><i
                                    class="fa fa-pencil"></i> <span>Edit</span> </a>
                        <a href="{{url('/campaign/pause/'.$campaign->campaignId)}}" title="" class="push-icon"><i
                                    class="fa fa-pause"></i> <span>Pause</span> </a>
                        <a href="{{url('/campaign/'. $campaign->slug . '/offline-donation')}}" title="" class="offline-icon"><i
                                    class="fa fa-plus"></i> <span>Offline donation</span> </a>
                        <a href="{{url('/campaign/delete/'.$campaign->campaignId)}}" title="" class="delete-icon"><i
                                    class="fa fa-trash"></i> <span>Delete</span> </a>
                        <a href="#" data-toggle="modal" data-target="#myModal1" title="" class="post-icon"><i
                                    class="fa fa-edit"></i> <span>Post an update</span> </a>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="bg_new" style="background:@if(isset($bg_color)) {{$bg_color}} @else #f0fafc @endif">
        <div class="mobile-social hidden-lg hidden-md hidden-sm" style="display: none;">
            <div class="col-xs-6 fb-mbl">
                <a href="javascript:void(0);" class="facebook-btn"
                   onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer', 'toolbar=0,status=0,width=500,height=400');"
                ><i class="fa fa-facebook-square"></i>Share</a>
            </div>
            <div class="col-xs-6 dnt-mbl">
                <a href="{{route('campaign.donation', $campaign->slug)}}" class="donate-btn">Donate Now</a>
            </div>

        </div>

        <div class="container bgWhite campaign-detail-left">
            <div class="row">
                <div class="col-sm-12 hidden-xs">
                    <h4>{{!empty($campaign->campaign_name) ? ucfirst($campaign->campaign_name) : ''}}</h4>
                    <div class="created-text"><i
                                class="fa fa-clock-o"></i>Created {{ Carbon\Carbon::parse($campaign->created_at)->diffForHumans() }}
                    </div>

                <!--                <div class="subCategory">{{!empty($campaign->categoryName) ? ucfirst($campaign->categoryName) : ''}}</div>-->
                <!--                @if(!empty($campaign->createrName)) <div class="subCategory">Fundraiser: {{!empty($campaign->createrName) ? ucfirst($campaign->createrName) : ''}}</div>@endif-->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div class="coverOuter">
                    @if($campaign->media_type=='youtube' && !empty($campaign->media_url))
                    <div class="videoWrapper">
                        <div class="playBtn">
                            <iframe width="100%" height="100%" src="{{$campaign->media_url}}" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    @else
                    <!-- <div class="coverImage" style="background: rgba(0, 0, 0, 0) url('{{isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($base_path.$campaign->campaign_image ) ?  asset($base_path.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg')}}') no-repeat scroll center center / cover ;"></div> -->
                        <div class="row">
                            {!! HTML::image(isset($campaign->campaign_image) && !empty($campaign->campaign_image) && File::exists($base_path.$campaign->campaign_image ) ?  asset($base_path.$campaign->campaign_image)  : asset('images/default-images/default-campaign.jpg'), 'Campaign Picture', array('class' => 'coverImage'))!!}
                        </div>
                    @endif


                    <div class="col-sm-12 hidden-lg hidden-sm row">
                        <h4>{{!empty($campaign->campaign_name) ? ucfirst($campaign->campaign_name) : ''}}</h4>
                        <div class="created-text"><i
                                    class="fa fa-clock-o"></i>Created {{ Carbon\Carbon::parse($campaign->created_at)->diffForHumans() }}
                        </div>
                    </div>

                    <div class="social-links-wrapper hidden-xs">
                        <a class="btn btn-tw" href="#"
                           onclick="popUp=window.open('https://twitter.com/share?url={{urlencode(Request::url())}}&text={{urlencode($campaign->campaign_name)}}', 'popupwindow', 'scrollbars=yes,width=500,height=400'); popUp.focus(); return false"><i
                                    class="fa fa-twitter"></i></a>
                        <a class="btn btn-fb" href="#"
                           onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer','toolbar=0,status=0,width=500,height=400');"
                           href="javascript: void(0)"><i class="fa fa-facebook"></i></a>
                        
                        <a class="btn btn-donor">
                            <i class="fa fa-heart"></i>
                                <span>{{ !empty($totalDonors) ? $totalDonors : 0 }}
                                    <span>{{ !empty($totalDonors) && $totalDonors > 1 ? "donors" : "donor" }}</span>
                                </span>
                        </a>

                        @if(!empty($teamDetails))
                        <a class="btn btn-donor pull-right" data-toggle="modal" data-target="#myModal-team">
                            <i class="fa fa-plus"></i>
                                <span>Join Team</span>
                        </a>

                        @foreach($teamDetails as $detail)
                        <a class="pull-right teamlogo-campainpage" href="{{url('team-details')}}/{{$detail->team_url}}" 
                            title="{{$detail->team_name}}">
                           <img  src="{{asset('images/thumb-campaigns')}}/{{$detail->team_logo}}">
                         </a>
                         @endforeach 
                        @endif
                                               
                        


                    </div>
                    {{--<!--                    <div class="coverImageContent" style="display:none;">--}}
                    {{--<div class="campaignDetials">--}}
                    {{--<div class="shareDonor">--}}
                    {{--<ul>--}}
                    {{--<li class="shares">1.1k <span>shares</span></li>--}}
                    {{--<li class="donors">{{ !empty($totalDonors) ? $totalDonors : 0 }} <span>{{ !empty($totalDonors) && $totalDonors > 1 ? "donors" : "donor" }}</span></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="progressOuter" style="width:150px;height:150px;margin:18px auto;">--}}
                    {{--<div class="percent" style="width:100%;height:100%;">--}}
                    {{--<p style="display:none;">{{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>-->--}}
                </div>


                <div class="hidden-lg hidden-sm">
                <!--  <div class="progressOuter progress-detail-class"
                         style="width:150px;height:150px;margin:25px auto 10px;">
                        <div class="percent progress-text-size" style="width:100%;height:100%;">
                            <p style="display:none;">{{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}</p>
                        </div>
                    </div> -->
                    <!-- This will be shown on the mobile view-->
                    <div class="progress-circle-wrapper">
                        <svg class="progress-circle-image" width="120" height="120" viewBox="0 0 120 120">
                            <circle class="progress-circle__meter" cx="60" cy="60" r="54" stroke-width="5"/>
                            <circle style="stroke:@if($campaign->themeValue != null) {{$btn_color}} @endif" class="progress-circle__value1" cx="60" cy="60" r="54" stroke-width="12"/>
                        </svg>
                        <div class="progress-text"
                             style="color:@if($campaign->themeValue != null) {{$btn_color}} @endif">
                            {{!empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent}}%
                        </div>
                    </div>
                    <div class="donate-value">
                        <span class="aValue"
                              style="color:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif"> <span
                                    class="pound_small">&pound;</span>{{Helper::showMoney($totalAmountRaised,$campaign->campaign_currency)}}</span>
                        of <span
                                class="oValue"> &pound;{{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}</span>
                    </div>
                    @if(!empty($campaign->campaign_duration) && $campaign->campaign_duration == "enddate" && Helper::isExpired($campaign->end_date) == true)
                        <div class="subCategory">
                            Campaign has been expired
                        </div>
                    @elseif($user['role'] != 'Charity')
                        <p class="donateNowBtn">
                            <a href="{{route('campaign.donation', $campaign->slug)}}" type="button"
                               class="lt-green-lg fullwidth text-center"
                               style="background:@if($campaign->themeValue != null) {{$btn_color}} !important; @endif">Donate Now</a>
                        </p>
                        <a href="#" class="share-facebook-btn fullwidth text-center" style="background:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif"
                           onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer', 'toolbar=0,status=0,width=500,height=400');"
                           href="javascript:void(0);"><i class="fa fa-facebook-square"></i>Share on Facebook</a>
                        <div class="profile-wrapper">

                        <span class="user-profile-charity">
                            @if(file_exists(public_path('modules/auth/thumb-profile-images/'.$campaign->userProfilePic)))
                                {!! HTML::image('modules/auth/thumb-profile-images/'.$campaign->userProfilePic) !!}
                            @else
                                {!! HTML::image('images/userDP.png') !!}
                            @endif
                            <span class="user-name">{{$campaign->createrName}}
                                <span class="user-profile">
                                    <span class="user-tags"><i class="fa fa-tags"></i>{{$campaign->categoryName}}</span>
                                    <span class="user-tags"><i class="fa fa-map-marker"></i>{{$campaign->locationName}}</span>
                                </span>
                            </span>

                        </span>
                        </div>
                    @endif
                </div>
                @if($campaign->charity_id)
                    <div class="raising-money">
                        <div class="col-sm-12 raising-money-for">Raising money for:</div>

                        <div class="border-btm col-sm-2">
                            <p class="syria-img">
                                <a href="{{url('/charity/'. $campaign->charitySlug)}}">
                                    <img class="charity-logo"
                                         src="{{ (!empty($campaign->charity_logo) && File::exists($base_path_charity.$campaign->charity_logo) && isset($campaign->charity_logo)) ? asset($base_path_charity.$campaign->charity_logo) : asset('images/default-images/default_logo.png') }}"
                                         alt="">
                                </a>
                            </p>
                        </div>

                        <div class="bigBlack col-sm-10 charity-details">
                            <a href="{{url('/charity/'. $campaign->charitySlug)}}"><h4>{{$campaign->charityName}}</h4>
                            </a>
                            <div class="register-number">{{$campaign->charityRegistration}}</div>
                            <a target="_blank" href="{{url('/campaigns/create?charity='. $campaign->charity_id)}}"
                               class="btn btn-success">Fundraise
                                for us</a>
                        </div>

                        <div class="col-sm-12">
                            <div class="border-bottom mt20"></div>
                        </div>
                    </div>
                @endif
                <div class="tab-card ">

                    <div class="tab-top-wrap">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#about" aria-controls="register" role="tab" data-toggle="tab"><span
                                            class="tab-title">About</span></a>
                            </li>
                            <li role="presentation">
                                <a href="#updates" aria-controls="login" role="tab" data-toggle="tab"><span
                                            class="tab-title">Updates</span><span class="update-count">{{$count}}</span></a>
                            </li>
                            <?php if($campaign->stravaAccessToken != null){?>
                            <li role="presentation">
                                <a href="#training" aria-controls="login" role="tab" data-toggle="tab"><span
                                            class="tab-title">Training</span></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="about">

                            <div class="charityList" id="description-content large-word">
                                <!--<div class="postheading" >About</div>-->
                            <!--                                <div class="postPublish">{{!empty($campaign->createrName) ? "Published by ".$campaign->createrName." - ".Helper::showDate($campaign->created_at) :''}}</div>-->
                                <div class="postdescription readmore-content f19"> {!! isset($campaign->campaign_description) ? $campaign->campaign_description :'' !!}</div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="updates">
                            <div id="campaign-update-div">
                                @include("site.partials.campaign_updates")
                            </div>
                        </div>
                        <?php if($campaign->stravaAccessToken != null){ ?>
                        <div role="tabpanel" class="tab-pane" id="training">
                            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate col-md-offset-6"></span>
                        </div>
                        <?php } ?>

                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-3">
                <div class="hidden-xs">
                <!-- <div class="progressOuter progress-detail-class"
                         style="width:150px;height:150px;margin:25px auto 10px;">
                        <div class="percent" style="width:100%;height:100%;">
                        <p style="display:none;">{{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}</p>
                        </div>
                        
                    </div> -->
                    <div class="progress-circle-wrapper">
                        <svg class="progress-circle-image" width="120" height="120" viewBox="0 0 120 120">
                            <circle class="progress-circle__meter" cx="60" cy="60" r="54" stroke-width="5"/>
                            <circle style="stroke:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif" class="progress-circle__value" cx="60" cy="60" r="54" stroke-width="12"/>
                        </svg>
                        <div class="progress-text"
                             style="color:@if($campaign->themeValue != null) {{$campaign->themeValue}} @endif">
                            {{ !empty($goalPercent) && $goalPercent>100 ? 100 : $goalPercent }}%
                        </div>
                    </div>
                    <div class=" donate-value
                        ">
                        <span class="aValue"
                              style="color:@if($campaign->themeValue != null) {{$btn_color}} @endif">&pound;{{Helper::showMoney($totalAmountRaised,$campaign->campaign_currency)}}</span>
                        of <span
                                class="oValue">&pound;{{Helper::showMoney($campaign->goal_amount_monetary,$campaign->campaign_currency)}}</span>
                    </div>
                    @if(!empty($campaign->campaign_duration) && $campaign->campaign_duration == "enddate" && Helper::isExpired($campaign->end_date) == true)
                        <div class="subCategory">
                            Campaign has been expired
                        </div>
                    @elseif($user['role'] != 'Charity')
                        <p class="donateNowBtn">
                            <a href="{{URL::route('campaign.donation',$campaign->slug)}}" type="button"
                               class="lt-green-lg fullwidth text-center" id="donate_now"
                               style="background:@if($campaign->themeValue != null) {{$btn_color}} @endif">Donate
                                Now</a>
                        </p>
                        <a href="javascript: void(0)" class="share-facebook-btn fullwidth text-center" style="background:@if($campaign->themeValue != null) {{$btn_color}} @endif"
                           onclick="window.open('https://www.facebook.com/dialog/feed?app_id={{env('FACEBOOK_CLIENT_ID')}}&display=popup&amp;caption={{urlencode($campaign->campaign_name)}}&description={{substr(strip_tags($campaign->campaign_description), 0, 50)}}&link={{urlencode(Request::url())}}&picture={{urlencode(asset($base_path.$campaign->campaign_image))}}','sharer','toolbar=0,status=0,width=500,height=400');"
                        ><i class="fa fa-facebook-square"></i>Share on Facebook</a>
                        <div class="profile-wrapper">


                        <span class="user-profile-charity">
                            
                            @if($campaign->userProfilePic == '')

                                {!! HTML::image('images/userDP.png') !!}
                            

                            @elseif(file_exists(public_path('modules/auth/thumb-profile-images/'.$campaign->userProfilePic)))
                                
                                {!! HTML::image('modules/auth/thumb-profile-images/'.$campaign->userProfilePic) !!}
                            
                            @else
                                {!! HTML::image('images/userDP.png') !!}
                            @endif
                            <span class="user-name">{{$campaign->createrName}}
                                <span class="user-profile">
                                    <span class="user-tags"><i class="fa fa-tags"></i>{{$campaign->categoryName}}</span>
                                    <span class="user-tags"><i class="fa fa-map-marker"></i>{{$campaign->locationName}}</span>
                                </span>
                            </span>

                        </span>
                        </div>

                </div>


                <div class="latest-donations-aside">

                    @if(isset($list_of_donors) && !empty($list_of_donors))
                        <h3>Latest Donations</h3>
                    @endif

                    <ul class="ltst-dnos-detail">
                        @foreach($list_of_donors as $donor)
                            <li>
                                @if(!$donor->is_anonymous)
                                    <h4>{{$donor->name}}</h4>
                                @else
                                    <h4>Anonymous</h4>
                                @endif
                                @if(!empty($donor->donationTime))
                                    <h5>{{ Carbon\Carbon::parse($donor->donationTime)->diffForHumans() }}</h5>
                                @elseif(!empty($campaign->created_at))
                                    <h5>{{ Carbon\Carbon::parse($campaign->created_at)->diffForHumans() }}</h5>
                                @endif
                                {{$donor->content}}
                                <div class="ltst-dnt-price">
                                    £{{$donor->donation_amount_monetory}}
                                    @if($donor->is_giftaid)
                                        <span>+ £{{number_format($donor->donation_amount_monetory * 0.25, 2, '.', '')}}
                                            Gift Aid</span>
                                    @endif
                                    @if($donor->is_offline)
                                        <span class="red"> Offline</span>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="text-center">
                        @include('site/partials/pagination', ['paginator' => $list_of_donors])
                    </div>
                </div>


                @elseif($user['role'] == 'Charity')
                    <span class="launch-note label label-info">Charity can't donate to any campaign.</span>
            @endif

            <!--
                <p>
                    <a href="#" class="green-btn fullwidth text-center">share</a>
                </p>
                -->
            </div>
        </div>
    </div>
    </div>

    <!--<div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                {!! Form::open(array('route' => ['donate.offline', $campaign->slug], 'novalidate' => 'novalidate', 'id' => 'offline_donation')) !!}
                <div class="modal-header">
                    <h4 class="modal-title">Add Offline Donation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="modal-donation-amout">
                        <label for="donation_amount">Donation amount <span class="astrikBlock">*</span></label>
                        <div class="inputBlock">
                            <p class="gbpBtn">
                                <input type='hidden' name='goal_amount_type' value='monetary' placeholder="&pound;">
                                <span class="sterling">&pound;</span>
                                {!! Form::text('amount',Session::has('donationAmount') ? Session::get('donationAmount') : '' , ['id'=>'donation_amt']) !!}
                            </p>
                            <p id="amt_error" class="text-danger"></p>
                            {!! $errors->first('goal_amount_monetary', '<div class="text-danger">:message</div>') !!}
                        </div>
                        {{--<input type="text" id="donation_amt" name="amount"--}}
                        {{--value="{{ Session::has('donationAmount') ? Session::get('donationAmount') : ''}}"--}}
                        {{--placeholder="&pound;"/>--}}
                    </div>
                    @if(!empty($donation))
                        <div class="choose-donation">
                            <span>Choose donation type</span>
                        </div>
                    @endif
                    <div class="select-donation">
                        @foreach($donation as $d)
                            <div class="select-donation-type">
                                <label class="control control--radio">
                                    <input
                                            @if($d->is_default)
                                            checked
                                            @endif
                                            type="radio" name="default" value="{{$d->id}}">
                                    {{$d->title}}
                                    <div class="control__indicator"></div>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-donation-amout">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="donation_amount">Donor Name <span class="astrikBlock">*</span></label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" id="first_name" name="firstname" placeholder="First Name"/>
                                <p id="donor_name_error" class="text-danger"></p>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" id="last_name" name="lastname" placeholder="Last Name"/></div>
                        </div>
                    </div>
                    <div class="modal-donation-amout">
                        <label for="donation_amount">Email Address <span class="astrikBlock">*</span></label>
                        <input type="email" id="email_address" name="email" placeholder="Email Address"/>
                        <p id="donor_email_error" class="text-danger"></p>
                    </div>
                    <div class="modal-donation-amout">
                        <label class="control keep-donor">
                            <input checked type="checkbox" name="anonymous" value="Keep donor anonymous"
                                   checked="checked">
                            Keep donor anonymous
                            <div class="control_checkbox"></div>
                        </label>

                    </div>
                    <div class="modal-donation-amout">
                        <label class="control charity-text">
                            <input checked type="checkbox" name="default" value="Keep donor anonymous">
                            Tick here if you would like Charity Name to reclaim the tax you have paid on all your
                            donations made in the last four years, and any future donations you may make.*
                            <div class="control_checkbox"></div>
                        </label>
                    </div>
                    <div class="modal-donation-amout address-select">
                        <label for="donation_amount">Address Information</label>
                        <input type="text" id="address1" name="amount" placeholder="Address"/>
                        <input type="text" id="Town" name="amount" placeholder="Town"/>
                        <input type="text" id="postcode" name="amount" placeholder="Postcode"/>
                    </div>

                </div>
                <div class="modal-footer">
                {!! Form::submit('Add Donation', array('class'=>'btn btn-success')) !!}
                <!-- <a href="#" data-dismiss="modal">Cancel</a> 
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>-->

    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="upd_title">Update Box</h4>
                </div>
                {!! Form::open(['route' => ['campaign-updates.store', $campaign->campaignId], 'id'=>'update-form']) !!}
                <div class="modal-body">
                    <div class='container'>
                        <div class="">
                            {!! Form::textarea('updates', null, ['placeholder'=>'New Update ..','class' => '']) !!}
                            {!! $errors->first('updates', '<div class="text-danger">:message</div>') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success">Save</button>
                    <button class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <!--Join Team Modal -->
    <div class="modal fade" id="myModal-team" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header donation-modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Join The Team</h5>
                <button type="button" class="close close-team" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body join-team-modal">
                <form method="POST" action="{{ url('team/team-join')}}" id="joinForm">
                  {{ csrf_field() }}
                    <label>Choose a team to join<span>*</span></label>
                        <div class="campaign-list">
                        <ul>
                            
                        @if(!empty($teamDetails))
                           
                           {{ csrf_field() }}
                           @foreach($teamDetails as $details) 
                                                        
                            <li>
                            
                                <label><input type="radio" required="true" name="selectedTeam" value="{{$details->team_id}}"><span>{{$details->team_name}}</span></label>
                                <a href="{{url('team-details')}}/{{$details->team_url}}" target="_blank"><i>{{url('team-details')}}/{{$details->team_url}}</i></a>
                            
                            </li>

                            @endforeach
                           
                        @endif    
                            
                        </ul>
                                             
                    </div>
                
              </div>
              </form>
              <div class="modal-footer join-team-modal-footer">
                <a class="" href="" data-dismiss="modal">cancel</a>
                <button class="modal-donate-btn pull-left" onclick="$('#joinForm').submit()">Join Team</button>
                
              </div>
            </div>
          </div>
        </div> <!-- Join Team modal -->

@stop
@section('script')
    <script type="text/javascript">
        // $("#donate_now").click(function(){
        //     $("#donation_amount").focus();
        // });
        $('#myModal').on('shown.bs.modal', function () {
            $('#donation_amount').focus();
        })
    </script> <script type='text/javascript' src="{{ asset('js/map-start.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.27&key=AIzaSyB3bYO0g2eNmoWdsNS0FZL8PuiCBeKUEiI&libraries=drawing&callback=initMap"
            async defer></script>
   
    <script type='text/javascript' src="{{ asset('js/campaign-detail.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
    <script src="{{ asset('js/jQuery.circleProgressBar.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>



    <script src="{{asset('site/js/fusioncharts.js')}}"></script>
    <script src="{{asset('site/js/fusioncharts.charts.js') }} "></script>
    <script src="{{asset('site/js/fusioncharts.theme.management-dashboard.js') }}"></script>
    <script>
        camp_id = '<?php echo $campaign->campaignId; ?>';

        /**
         * strava code
         */
        $(document).ready(function () {
            $.ajax({
                url: 'stravaActivity/' + camp_id,
                //dataType: "json",
                //method: "GET",
                success: function (data) {
                    $("#training").html(data);
                }
            });
        });

    </script>
    <script>
        $(".textarea").wysihtml5();
        $(".textarea").css("color", "#333");
        $(document).ready(function () {
            $("ul.wysihtml5-toolbar li a[data-wysihtml5-command=insertImage]").css("display", 'none');
        });
        $(window).scroll(function () {
            scroll = $(window).scrollTop();
            if (scroll >= 80) {
                $('.mobile-social').show();
            } else if (scroll <= 80) {
                $('.mobile-social').hide();
            }
        });
    </script>

    <script>
        function makepass() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 6; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });

        var progressValue = document.querySelector('.progress-circle__value');
        var progressValue1 = document.querySelector('.progress-circle__value1');

        var RADIUS = 54;
        var CIRCUMFERENCE = 2 * Math.PI * RADIUS;

        function progress(value) {
            var progress = value / 100;
            var dashoffset = CIRCUMFERENCE * (1 - progress);

            console.log('progress:', value + '%', '|', 'offset:', dashoffset)

            progressValue.style.strokeDashoffset = dashoffset;
            progressValue1.style.strokeDashoffset = dashoffset;
        }


        progressValue.style.strokeDasharray = CIRCUMFERENCE;
        progressValue1.style.strokeDasharray = CIRCUMFERENCE;

        var value = <?php echo !empty($goalPercent) && $goalPercent > 100 ? 100 : $goalPercent  ?>;
        progress(value);
    </script>
@stop
