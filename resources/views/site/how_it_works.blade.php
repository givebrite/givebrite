@extends('site.layouts.master')

@section('title', 'How It Works')

@section('content')
<?php /*
@if (isset($data->description)) 
{!! $data->description !!}
@endif
 */ ?>

<div class="hitPage">
    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h6>How It Works In <span>5 Easy Steps</span></h6>


                    <p>Give Brite online fundraising platform is friendly, simple and perfect for individual causes, charities and businesses! </p>


                </div>


            </div>


        </div>


    </div>


    <div class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">1</button>
                    <h6>Sign Up For <span>Free</span></h6>


                    <p>Great reason to start a fundraising campaign, share your story and attract donors today!</p>


                    <div class="imgContain">
                        <img src="/site/images/how-its-work-1.png" alt="How its work"> 
                    </div>


                </div>


            </div>


        </div>


    </div>


    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">2</button>
                    <h6>Set Your <span>Goals</span></h6>


                    <p>Aim for the sky and get the ball rolling! Achieve the best for your campaign and attract family and friends by sharing your campaign through Facebook, Twitter and email to hit your goal with ease! </p>


                    <div class="imgContain">
                        <img src="/site/images/how-its-work-2.png" alt="How its work"> 
                    </div>


                </div>


            </div>


        </div>


    </div>


    <div class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">3</button>
                    <h6>Customise Your <span>Campaign</span></h6>


                    <p>Make it your own, tell your story! Upload a profile and cover photo and add a touch of colour to make your page stand out! Make changes and keep donors posted through our amazing dashboard. </p>


                    <div class="imgContain">
                        <img src="/site/images/how-its-work-3.png" alt="How its work"> 
                    </div>


                </div>


            </div>


        </div>


    </div>


    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">4</button>
                    <h6>Reward Your <span>Donors</span></h6>


                    <p>It’s the best way to encourage engagement and easily increase donations!</p>


                    <div class="imgContain">
                        <img src="/site/images/how-its-work-4.png" alt="How its work"> 
                    </div>


                </div>


            </div>


        </div>


    </div>


    <div class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">5</button>
                    <h6>Spread The <span>Word</span></h6>


                    <p>90% of your donations will be through word of mouth. Tell friends, family even neighbours to donate towards your campaign. Make sure to share on social media as you never know....your campaign may go viral!</p>


                    <div class="imgContain">
                        <img src="/site/images/how-its-work-5.png" alt="How its work"> 
                    </div>


                </div>


            </div>


        </div>


    </div>


</div>

@stop

@section('script')
<script src='{{ asset('js/how-it-works.js') }}'></script>
@stop