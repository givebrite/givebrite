@extends('site.layouts.master')

@section('title', '404')

@section('content')

<div class="container errorPage">
    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="errorImage"><img src="{{ asset('images/error404.png') }}" alt="error"> </p>
            <h1 class="errorTitle">Page not found</h1>
            <p class="errorMsg">Sorry, the URL you entered cannot be found. Please check and try again.</p>
        </div>
    </div>
</div>

@stop