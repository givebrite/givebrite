@extends('site.layouts.master')

@section('title', isset($content->title)?$content->title:"")

@section('content')
<?php header("Content-Type: text/plain"); ?>
<div class="container donatePage-content">
    <div class="row">        
        @if (is_object($content))
        <h2 class="heading">{!! $content->title !!}</h2>
        {!! $content->description !!}
        @else
        <h2 class="heading">Coming soon...</h2>
        @endif
    </div>
</div>
@stop