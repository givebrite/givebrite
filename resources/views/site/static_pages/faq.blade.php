@extends('site.layouts.master')

@section('title', $title)

@section('content')

<div class="container donatePage-content">
    <div class="row">
        <h2 class="heading">{{ $title }}</h2>
        {{ $content }}
    </div>
</div>
@stop