@extends('site.layouts.master')

@section('content')

{!! Form::open(array('url' => 'contact')) !!}
<div class="container">
    <div class="col-sm-12 col-md-6 col-lg-6 contact-wrapper">
        <div class="laungSlogan createHeading">
            <h2>Contact Us</h2>
            <h4></h4>

        </div>
        <div class="signUp-form"> 
            <div class="form-sec">
                <div id="user1-form">      
                    <div class="form-row multiinputs clearfix">
                        <div class="inputBlock"> 
                            {!! Form::text('first_name', Session::has('first_name') ? Session::get('first_name') : '', ['class' => 'form-control','placeholder' => 'First Name']) !!}
                            {!! $errors->first('first_name', '<div class="text-danger">:message</div>') !!} 
                        </div>
                        <div class="inputBlock"> 
                            {!! Form::text('sur_name', Session::has('sur_name') ? Session::get('sur_name') : '', ['class' => 'form-control','placeholder' => 'Surname']) !!}
                            {!! $errors->first('sur_name', '<div class="text-danger">:message</div>') !!} 
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="inputBlock"> {!! Form::email('email', Session::has('email') ? Session::get('email') : '', ['class' => 'form-control','placeholder' => 'E-mail Address']) !!}
                        {!! $errors->first('email', '
                        <div class="text-danger">:message</div>
                        ') !!} </div>
                </div>

                <div class="form-row">
                    <div class="inputBlock"> {!! Form::textarea('contact_message', Session::has('message') ? Session::get('message') : '', ['class' => 'form-control','placeholder' => 'Enter your message']) !!}
                        {!! $errors->first('contact_message', '
                        <div class="text-danger">:message</div>') !!} </div>
                </div>
                <div class="row">
                    <div class="form-row col-sm-12 col-lg-4"> 
                        {!! Form::submit('Send', array('class'=>'fullwidth blue-btn-lg text-center')) !!} 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop