<div class="charityList" id="description-content large-word">
    <div>
        <span>&nbsp;</span>
        <?php if (!empty($latestActivities)) { ?>
            <ul class="timeline">
                <?php
                $count = 1;
                foreach ($latestActivities as $latestActivity) {
                    ?>
                    <li>
                        <div class="timeline-badge info"><i class="glyphicon glyphicon-check"></i></div>
                        <?php if ($count % 2 == 0) { ?>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <div class="timeline-title">
                                        <h6><?php echo date('d', strtotime($latestActivity->start_date)); ?></h6>
                                        <p>
                                            <span><?php echo date('l', strtotime($latestActivity->start_date)); ?></span>
                                            <span><?php echo date('F o', strtotime($latestActivity->start_date)); ?></span>
                                        </p>

                                    </div>
                                    <div class="timeline-km">
                                        <div style="width:35%;">
                                            <h2><?php echo number_format((((int) $latestActivity->distance) / 1000), 2, '.', ''); ?></h2>
                                            <span>KMS</span>
                                        </div>

                                        <div style="width:45%;">
                                            <h2><?php echo gmdate("H:i", $latestActivity->moving_time); ?></h2>
                                            <span>Hrs</span>
                                        </div>

                                        <div style="width:17%;">
                                            <h2><?php echo (int) $latestActivity->average_speed * 3.6; ?></h2>
                                            <span>KM/H</span>
                                        </div>

                                    </div>


                                </div>
                                <div class="timeline-body">
                                    <img class="img-responsive" src="<?php echo $latestActivity->gmap_img; ?>"/>
                                </div>
                            </div>

                            <div class="timeline-panel2 timeline-panel2-graph white-bg">
                                <div class="timeline-heading">
                                    <div class="pull-left"><?php echo date('H:i', strtotime($latestActivity->start_date)); ?></div>
                                    <div class="distance-text pull-right"><?php echo max(json_decode($latestActivity->graph_data)); ?> m<br><span>Elevation</span></div>
                                    <h4 class="timeline-title"><?php echo $latestActivity->activity_name; ?></h4>
                                    <!--<p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11 Lorem Ipsum Lorem Ipsum</small></p>-->
                                </div>
                                <div class="timeline-body">
                                    <div class="timeline-graph">
                                        <p></p>
                                    <!--<p id="lineGraph-{{$latestActivity->activity_id}}"></p>-->
                                        <canvas id="lineGraph-{{$latestActivity->activity_id}}"></canvas>

                                        <script>
                                            var ctx = document.getElementById('lineGraph-<?php echo $latestActivity->activity_id; ?>').getContext('2d');
                                            ctx.canvas.height = 280;
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                data: {
                                                    labels: <?php echo $latestActivity->graph_data; ?>,
                                                    datasets: [{
                                                            label: 'Elevation',
                                                            data: <?php echo $latestActivity->graph_data; ?>,
                                                            backgroundColor: "rgba(127,198,117,0.6)"
                                                        }]
                                                },
                                                options:
                                                        {
                                                            responsive: true,
                                                            maintainAspectRatio: false,
                                                            legend: {
                                                        display: false
                                                                },
                                                            tooltips: {
                                                                callbacks: {
                                                                    label: function (tooltipItem, data) {
                                                                       // return tooltipItem.yLabel + " Metres";
                                                                        return "Elevation in Metres";
                                                                    }
                                                                }
                                                            },
                                                            scales:
                                                                    {
                                                                        xAxes: [{
                                                                                display: false
                                                                            }]
                                                                    },
                                                            elements: {point: {radius: 0, backgroundColor: 'transparent'}}

                                                        }
                                            });

                                        </script>
                                    </div>
                                </div>
                                </div>
                            <?php } else { ?>
                        
                                <div class="timeline-panel2">
                                    <div class="timeline-heading">
                                        <div class="timeline-title">
                                            <h6><?php echo date('d', strtotime($latestActivity->start_date)); ?></h6>
                                            <p>
                                                <span><?php echo date('l', strtotime($latestActivity->start_date)); ?></span>
                                                <span><?php echo date('F o', strtotime($latestActivity->start_date)); ?></span>
                                            </p>

                                        </div>
                                        <div class="timeline-km">
                                            <div style="width:35%;">
                                                <h2><?php echo number_format((((int) $latestActivity->distance) / 1000), 2, '.', ''); ?></h2>
                                                <span>KMS</span>
                                            </div>

                                            <div style="width:45%;">
                                                <h2><?php echo gmdate("H:i", $latestActivity->moving_time); ?></h2>
                                                <span>Hrs</span>
                                            </div>

                                            <div style="width:17%;">
                                                <h2><?php echo (int) $latestActivity->average_speed * 3.6; ?></h2>
                                                <span>KM/H</span>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="timeline-body">
                                        <img class="img-responsive" src="<?php echo $latestActivity->gmap_img; ?>"/>
                                    </div>

                                </div>
                                <div class="timeline-panel timeline-panel-graph  white-bg">
                                    <div class="timeline-heading">
                                        <div class="pull-right"><?php echo date('H:i', strtotime($latestActivity->start_date)); ?></div>
                                    <div class="distance-text pull-left"><?php echo max(json_decode($latestActivity->graph_data)); ?> m<br><span>Elevation</span></div>
                                        <h4 class="timeline-title"><?php echo $latestActivity->activity_name; ?></h4>

                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-graph">
                                            <p></p>
                                    <!--<p id="lineGraph-{{$latestActivity->activity_id}}"></p>-->
                                        <canvas id="lineGraph-{{$latestActivity->activity_id}}"></canvas>

                                        <script>
                                            var ctx = document.getElementById('lineGraph-<?php echo $latestActivity->activity_id; ?>').getContext('2d');
                                            ctx.canvas.height = 280;
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                data: {
                                                    labels: <?php echo $latestActivity->graph_data; ?>,
                                                    datasets: [{
                                                            label: 'Elevation',
                                                            data: <?php echo $latestActivity->graph_data; ?>,
                                                            backgroundColor: "rgba(127,198,117,0.6)"
                                                        }]
                                                },
                                                options:
                                                        {
                                                            responsive: true,
                                                            maintainAspectRatio: false,
                                                             legend: {
                                                        display: false
                                                                },
                                                            tooltips: {
                                                                callbacks: {
                                                                    label: function (tooltipItem, data) {
                                                                        //return tooltipItem.yLabel + " Metres";
                                                                        return "Elevation in Metres";
                                                                    }
                                                                }
                                                            },
                                                            scales:
                                                                    {
                                                                        xAxes: [{
                                                                                display: false
                                                                            }]
                                                                    },
                                                            elements: {point: {radius: 0, backgroundColor: 'transparent'}}

                                                        }
                                            });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                    </li>
                    <?php
                    $count++;
                }
                ?>

            </ul>
            <?php
        } else {
            echo "Result not found...";
        }
        ?>
    </div>

</div>
