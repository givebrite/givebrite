-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 23, 2016 at 06:40 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_givebright`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cta_type` enum('external','internal') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'internal',
  `cta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `status` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `image`, `cta_type`, `cta`, `position`, `status`, `created_at`, `updated_at`) VALUES
(11, 'Banner Second', 'eac520a2458380abe2873ead7e039562e6a86289.png', 'external', 'http://www.givebright.dev', 1, 'yes', '2016-06-10 10:26:17', '2016-06-23 11:01:40'),
(17, 'Banner Second', 'd77281394dcd5c692a3301c059ff63f8dd61ffaa.png', 'external', '', 2, 'yes', '2016-06-23 10:52:44', '2016-06-23 11:01:53');

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `campaign_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `goal_amount_type` enum('time','monetary') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'monetary',
  `goal_amount_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goal_amount_monetary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_type` enum('cause','business') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cause',
  `campaign_category` int(10) unsigned NOT NULL,
  `campaign_duration` enum('enddate','ongoing') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ongoing',
  `start_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'sterling',
  `charity_id` int(10) unsigned DEFAULT NULL,
  `campaign_rewards` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `stage` enum('1','2','3','4','5') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `campaigns_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_categories`
--

CREATE TABLE IF NOT EXISTS `campaign_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `campaign_categories`
--

INSERT INTO `campaign_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Business', 'business', NULL, NULL),
(2, 'Charity', 'charity', NULL, NULL),
(3, 'Community', 'community', NULL, NULL),
(4, 'Competitions', 'competitions', NULL, NULL),
(5, 'Creative', 'creative', NULL, NULL),
(6, 'Events', 'events', NULL, NULL),
(7, 'Faith', 'faith', NULL, NULL),
(8, 'Family', 'family', NULL, NULL),
(9, 'National', 'national', NULL, NULL),
(10, 'News', 'news', NULL, NULL),
(11, 'Newlyweds', 'newlyweds', NULL, NULL),
(12, 'Other', 'other', NULL, NULL),
(13, 'Travel', 'travel', NULL, NULL),
(14, 'Wishes', 'wishes', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_charities`
--

CREATE TABLE IF NOT EXISTS `campaign_charities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `charity_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `charity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `post_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_design`
--

CREATE TABLE IF NOT EXISTS `campaign_design` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `media_type` enum('image','facebook','youtube') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'image',
  `campaign_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `campaign_description` text COLLATE utf8_unicode_ci,
  `media_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lattitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_donations`
--

CREATE TABLE IF NOT EXISTS `campaign_donations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `donation_amount_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donation_amount_monetory` double(8,2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `application_fees_charge` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gateway_fees` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaign_donations_user_id_foreign` (`user_id`),
  KEY `campaign_donations_campaign_id_foreign` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_locations`
--

CREATE TABLE IF NOT EXISTS `campaign_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=202 ;

--
-- Dumping data for table `campaign_locations`
--

INSERT INTO `campaign_locations` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'United Kingdom', 0, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(2, 'London', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(3, 'Birmingham', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(4, 'Leeds', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(5, 'Glasgow', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(6, 'Sheffield', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(7, 'Bradford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(8, 'Liverpool', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(9, 'Edinburgh', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(10, 'Manchester', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(11, 'Bristol', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(12, 'Kirklees', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(13, 'Fife', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(14, 'Wirral', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(15, 'North Lanarkshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(16, 'Wakefield', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(17, 'Cardiff', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(18, 'Dudley', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(19, 'Wigan', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(20, 'East Riding', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(21, 'South Lanarkshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(22, 'Coventry', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(23, 'Belfast', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(24, 'Leicester', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(25, 'Sunderland', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(26, 'Sandwell', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(27, 'Doncaster', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(28, 'Stockport', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(29, 'Sefton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(30, 'Nottingham', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(31, 'Newcastle-upon-Tyne', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(32, 'Kingston-upon-Hull', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(33, 'Bolton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(34, 'Walsall', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(35, 'Plymouth', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(36, 'Rotherham', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(37, 'Stoke-on-Trent', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(38, 'Wolverhampton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(39, 'Rhondda, Cynon, Taff', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(40, 'South Gloucestershire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(41, 'Derby', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(42, 'Swansea', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(43, 'Salford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(44, 'Aberdeenshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(45, 'Barnsley', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(46, 'Tameside', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(47, 'Oldham', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(48, 'Trafford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(49, 'Aberdeen', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(50, 'Southampton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(51, 'Highland', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(52, 'Rochdale', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(53, 'Solihull', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(54, 'Gateshead', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(55, 'Milton Keynes', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(56, 'North Tyneside', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(57, 'Calderdale', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(58, 'Northampton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(59, 'Portsmouth', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(60, 'Warrington', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(61, 'North Somerset', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(62, 'Bury', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(63, 'Luton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(64, 'St Helens', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(65, 'Stockton-on-Tees', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(66, 'Renfrewshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(67, 'York', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(68, 'Thamesdown', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(69, 'Southend-on-Sea', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(70, 'New Forest', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(71, 'Caerphilly', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(72, 'Carmarthenshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(73, 'Bath & North East Somerset', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(74, 'Wycombe', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(75, 'Basildon', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(76, 'Bournemouth', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(77, 'Peterborough', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(78, 'North East Lincolnshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(79, 'Chelmsford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(80, 'Brighton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(81, 'South Tyneside', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(82, 'Charnwood', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(83, 'Aylesbury Vale', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(84, 'Colchester', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(85, 'Knowsley', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(86, 'North Lincolnshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(87, 'Huntingdonshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(88, 'Macclesfield', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(89, 'Blackpool', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(90, 'West Lothian', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(91, 'South Somerset', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(92, 'Dundee', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(93, 'Basingstoke & Deane', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(94, 'Harrogate', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(95, 'Dumfries & Galloway', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(96, 'Middlesbrough', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(97, 'Flintshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(98, 'Rochester-upon-Medway', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(99, 'The Wrekin', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(100, 'Newbury', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(101, 'Falkirk', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(102, 'Reading', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(103, 'Wokingham', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(104, 'Windsor & Maidenhead', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(105, 'Maidstone', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(106, 'Redcar & Cleveland', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(107, 'North Ayrshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(108, 'Blackburn', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(109, 'Neath Port Talbot', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(110, 'Poole', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(111, 'Wealden', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(112, 'Arun', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(113, 'Bedford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(114, 'Oxford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(115, 'Lancaster', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(116, 'Newport', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(117, 'Canterbury', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(118, 'Preston', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(119, 'Dacorum', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(120, 'Cherwell', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(121, 'Perth & Kinross', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(122, 'Thurrock', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(123, 'Tendring', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(124, 'Kings Lynn & West Norfolk', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(125, 'St Albans', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(126, 'Bridgend', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(127, 'South Cambridgeshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(128, 'Braintree', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(129, 'Norwich', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(130, 'Thanet', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(131, 'Isle of Wight', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(132, 'Mid Sussex', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(133, 'South Oxfordshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(134, 'Guildford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(135, 'Elmbridge', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(136, 'Stafford', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(137, 'Powys', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(138, 'East Hertfordshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(139, 'Torbay', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(140, 'Wrexham Maelor', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(141, 'East Devon', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(142, 'East Lindsey', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(143, 'Halton', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(144, 'Warwick', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(145, 'East Ayrshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(146, 'Newcastle-under-Lyme', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(147, 'North Wiltshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(148, 'South Kesteven', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(149, 'Epping Forest', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(150, 'Vale of Glamorgan', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(151, 'Reigate & Banstead', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(152, 'Chester', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(153, 'Mid Bedfordshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(154, 'Suffolk Coastal', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(155, 'Horsham', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(156, 'Nuneaton & Bedworth', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(157, 'Gwynedd', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(158, 'Swale', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(159, 'Havant & Waterloo', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(160, 'Teignbridge', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(161, 'Cambridge', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(162, 'Vale Royal', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(163, 'Amber Valley', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(164, 'North Hertfordshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(165, 'South Ayrshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(166, 'Waverley', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(167, 'Broadland', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(168, 'Crewe & Nantwich', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(169, 'Breckland', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(170, 'Ipswich', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(171, 'Pembrokeshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(172, 'Vale of White Horse', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(173, 'Salisbury', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(174, 'Gedling', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(175, 'Eastleigh', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(176, 'Broxtowe', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(177, 'Stratford-on-Avon', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(178, 'South Bedfordshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(179, 'Angus', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(180, 'East Hampshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(181, 'East Dunbartonshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(182, 'Conway', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(183, 'Sevenoaks', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(184, 'Slough', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(185, 'Bracknell Forest', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(186, 'West Lancashire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(187, 'West Wiltshire', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(188, 'Ashfield', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(189, 'Lisburn', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(190, 'Scarborough', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(191, 'Stroud', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(192, 'Wychavon', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(193, 'Waveney', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(194, 'Exeter', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(195, 'Dover', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(196, 'Test Valley', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(197, 'Gloucester', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(198, 'Erewash', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(199, 'Cheltenham', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(200, 'Bassetlaw', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19'),
(201, 'Scottish Borders', 1, '2016-06-20 07:19:19', '2016-06-20 07:19:19');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_payments`
--

CREATE TABLE IF NOT EXISTS `campaign_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `campaign_id` int(10) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `status` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `connect_access_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connect_livemode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connect_refresh_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connect_token_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connect_stripe_publishable_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connect_stripe_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connect_scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent_application_fees` double(8,2) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_rewards`
--

CREATE TABLE IF NOT EXISTS `campaign_rewards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `reward_id` int(11) DEFAULT NULL,
  `reward_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pledge_amount` double(8,2) NOT NULL,
  `pledge_description` text COLLATE utf8_unicode_ci NOT NULL,
  `shipping_required` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT 'no',
  `location_cost` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_themes`
--

CREATE TABLE IF NOT EXISTS `campaign_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `campaign_themes`
--

INSERT INTO `campaign_themes` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Red', '#ff0000', NULL, NULL),
(2, 'Blue', '#0000ff', NULL, NULL),
(3, 'Yellow', '#ffff00', NULL, NULL),
(4, 'Green', '#00ff00', NULL, NULL),
(5, 'Sky-blue', '#00ffff', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_updates`
--

CREATE TABLE IF NOT EXISTS `campaign_updates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updates` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'General', 'general', NULL, '2016-04-13 05:11:02', '2016-04-13 05:11:02');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `donation_id` int(10) unsigned DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_anonymous` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_campaign_id_index` (`campaign_id`),
  KEY `comments_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title`, `slug`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Fundraise Online', 'fundraise-online', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(2, 'Frequently Asked Questions', 'faq', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(3, 'Privacy Policy', 'privacy', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(4, 'Terms &amp; Conditions', 'terms-conditions', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(5, 'Fundrasing Ideas', 'fundraise-idea', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(6, 'Success Stories', 'success-stories', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(7, 'Pricing', 'pricing', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(8, 'Contact us', 'contact-us', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(9, 'How we compare', 'how-we-compare', '<div class="compare-wrapper">\n      <div class="container">\n        <div class="row">\n          <div class="col-xs-12 text-center">\n            <button class="flat-blue-lg-btn">How We Compare</button>\n          </div>\n          <div class="col-xs-12">\n            <div class="table-responsive compareTable">\n              <table>\n                <tr>\n                  <th></th>\n                  <th class="theme-color">GiveBrite</th>\n                  <th>GoFundMe</th>\n                  <th>GoGetFunding</th>\n                </tr>\n                <tr>\n                  <td class="featuresHeading" colspan="4">Features</td>\n                </tr>\n                <tr>\n                  <td>Feature 01</td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                </tr>\n                <tr>\n                  <td>Feature 02</td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                </tr>\n                <tr>\n                  <td>Feature 03</td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />\n                  </td>\n                </tr>\n                <tr>\n                  <td>Feature 04</td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />\n                  </td>\n                </tr>\n                <tr>\n                  <td>Feature 05</td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />\n                  </td>\n                  <td>\n                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />\n                  </td>\n                </tr>\n              </table>\n            </div>\n            </div>\n          </div>\n        </div>\n      </div>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(10, 'Why Givebrite', 'why-givebrite', '<div class="btn-block text-center">\n                <button class="green-flat-btn">Why GiveBrite?</button>\n            </div>\n            <div class="checkList">\n                <ul class="clearfix">\n                    <li>Feature 1</li>\n                    <li>Feature 2</li>\n                    <li>Feature 3</li>\n                    <li>Feature 4</li>\n                    <li>Feature 5</li>\n                    <li>Feature 6</li>\n                </ul>\n            </div>\n            <div class="btn-block text-center">\n                <a class="flat-orange-btn" href="register">Sign Up for Free</a>\n            </div>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(11, 'About Us', 'about', '<div class="aboutbanner">\n    <div class="container">\n        <div class="row">\n            <div class="col-md-3 text-center">\n                <img src="http://54.83.205.169/givebrite/public/site/images/logo-big.png" alt="Profile" />\n            </div>\n            <div class="col-md-9">\n                <div class="row">\n                    <div class="col-md-6">\n                        <div class="bannerHeading">About Us</div>\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa</p>\n                    </div>\n                    <div class="col-md-6">\n                        <div class="bannerHeading">Overview</div>\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(12, 'How It Works', 'how-it-works', '<div class="videoWrapper">\n    <div class="playBtn">\n        <object height="100%" width="100%" data="http://www.youtube.com/v/Ahg6qcgoay4" type="application/x-shockwave-flash"><param name="src" value="http://www.youtube.com/v/Ahg6qcgoay4"></object>\n        <button class="blue-btn-lg vedio-button">Watch The Video</button>\n    </div>\n</div>\n<div class="hitPage">\n    <div class="bg-white">\n        <div class="container">\n            <div class="row">\n                <div class="col-xs-12">\n                    <h6>How It Works In <span>5 Easy Steps</span></h6>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class="bg-blue">\n        <div class="container">\n            <div class="row">\n                <div class="col-xs-12">\n                    <button class="cirButton">1</button>\n                    <h6>Sign Up For <span>Free</span></h6>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n                    <div class="imgContain">\n                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-1.png" alt="How its work" /> \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class="bg-white">\n        <div class="container">\n            <div class="row">\n                <div class="col-xs-12">\n                    <button class="cirButton">2</button>\n                    <h6>Set Your <span>Goals</span></h6>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n                    <div class="imgContain">\n                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-2.png" alt="How its work" /> \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class="bg-blue">\n        <div class="container">\n            <div class="row">\n                <div class="col-xs-12">\n                    <button class="cirButton">3</button>\n                    <h6>Customise Your <span>Campaign</span></h6>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n                    <div class="imgContain">\n                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-3.png" alt="How its work" /> \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class="bg-white">\n        <div class="container">\n            <div class="row">\n                <div class="col-xs-12">\n                    <button class="cirButton">4</button>\n                    <h6>Reward Your <span>Donors</span></h6>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n                    <div class="imgContain">\n                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-4.png" alt="How its work" /> \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class="bg-blue">\n        <div class="container">\n            <div class="row">\n                <div class="col-xs-12">\n                    <button class="cirButton">5</button>\n                    <h6>Reward Your <span>Donors</span></h6>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n                    <div class="imgContain">\n                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-5.png" alt="How its work" /> \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE IF NOT EXISTS `followers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `follower_id` int(10) unsigned NOT NULL,
  `is_followed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `menus_cat_id_foreign` (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `cat_id`, `title`, `slug`, `link`, `position`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Causes', 'causes', 'campaigns?type=cause', 1, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(2, 1, 'Charities', 'charities', 'charity', 2, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(3, 1, 'How It Works', 'how-it-works', 'articles/how-it-works', 3, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(4, 1, 'About', 'about', 'about-us', 4, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(5, 2, 'About Us', 'about-us', 'about-us', 1, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(6, 2, 'Campaign', 'campaign', 'pages/campaign', 2, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(7, 2, 'How It Works', 'how-it-works', 'articles/how-it-works', 3, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(8, 3, 'About Us', 'about-us', 'about-us', 1, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(9, 3, 'Fundraise Online', 'fundraise-online', 'pages/fundraise-online', 2, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(10, 3, 'Fundrasing Ideas', 'fundrasing-ideas', 'pages/fundraise-idea', 3, 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(11, 4, 'About Us', 'about-us', 'about-us', 1, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(12, 4, 'FAQ', 'faq', 'pages/faq', 2, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(13, 4, 'Privacy Policy', 'privacy-policy', 'pages/privacy', 3, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(14, 4, 'Terms &amp; Conditions', 'terms-amp-conditions', 'pages/terms-conditions', 4, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(15, 5, 'About Us', 'about-us', 'about-us', 1, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(16, 5, 'FAQ', 'faq', 'pages/faq', 2, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(17, 5, 'Privacy Policy', 'privacy-policy', 'pages/privacy', 3, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(18, 5, 'Terms &amp; Conditions', 'terms-amp-conditions', 'pages/terms-conditions', 4, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(19, 6, 'Success Stories', 'success-stories', 'pages/success-stories', 2, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(20, 6, 'How It Works', 'how-it-works', 'articles/how-it-works', 1, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(21, 6, 'FAQ', 'faq', 'pages/faq', 3, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(22, 6, 'Pricing', 'pricing', 'pages/pricing', 4, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(23, 6, 'Contact us', 'contact-us', 'pages/contact-us', 5, 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30');

-- --------------------------------------------------------

--
-- Table structure for table `menu_categories`
--

CREATE TABLE IF NOT EXISTS `menu_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `menu_categories`
--

INSERT INTO `menu_categories` (`id`, `title`, `slug`, `position`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Home header1', 'home-header1', 'header1', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(2, 'Internal Page Header', 'internal-page-header', 'header2', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(3, 'Footer 1', 'footer-1', 'footer1', 1, '2016-06-23 07:34:29', '2016-06-23 07:34:29'),
(4, 'Footer 2', 'footer-2', 'footer2', 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(5, 'Footer 3', 'footer-3', 'footer3', 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(6, 'About Side bar', 'about-side-bar', 'about-sidebar', 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30'),
(7, 'Dashboard Side bar', 'dashboard-side-bar', 'dashboard-sidebar', 1, '2016-06-23 07:34:30', '2016-06-23 07:34:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_07_05_111905_create_visitors_table', 1),
('2014_07_05_144447_create_articles_table', 1),
('2014_07_05_152557_create_options_table', 1),
('2014_07_07_005653_create_categories_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_11_02_051938_create_roles_table', 1),
('2014_11_02_052125_create_permissions_table', 1),
('2014_11_02_052410_create_role_user_table', 1),
('2014_11_02_092851_create_permission_role_table', 1),
('2016_04_26_054129_create_campaigns_table', 2),
('2016_04_26_060022_create_campaigns_table', 3),
('2016_04_26_071732_create_campaigns_table', 4),
('2016_04_26_072011_create_campaign_charities_table', 4),
('2016_04_26_072027_create_campaign_rewards_table', 4),
('2016_04_26_072050_create_campaign_design_table', 4),
('2016_04_27_062337_create_campaigns_table', 5),
('2016_04_27_062416_create_campaign_charities_table', 5),
('2016_04_28_060440_create_campaign_categories_table', 6),
('2016_04_28_071253_create_locations_table', 7),
('2016_04_28_071355_create_campaigns_themes_table', 7),
('2016_04_28_093028_create_campaign_themes_table', 8),
('2016_04_28_093037_create_campaign_locations_table', 9),
('2016_04_29_083012_create_reward_shipping_locations_table', 9),
('2016_04_27_133324_create_user_activation_table', 10),
('2016_04_27_134822_create_user_profile_table', 10),
('2016_04_28_055143_add_coloumn_to_users', 10),
('2016_05_05_075801_add_location_cost_to_campaign_rewards_table', 11),
('2016_05_06_125103_add_media_url_to_campaigns', 12),
('2016_05_06_125629_add_media_url_to_campaigns', 13),
('2016_05_09_045438_remove_stage_from_campaigns_table', 14),
('2016_05_09_050020_remove_stage_from_campaigns_table', 15),
('2016_05_09_051242_add_start_date_to_campaigns_table', 16),
('2016_05_09_051935_add_reward_name_to_campaign_rewards_table', 17),
('2016_05_09_071858_remove_reward_id_from_campaign_rewards_table', 18),
('2016_05_09_072052_add_reward_id_to_campaign_rewards_table', 18),
('2016_05_09_075045_create_rewards_table', 19),
('2016_05_10_072600_create_payment_options_table', 20),
('2016_05_10_080053_create_campaign_payments_table', 20),
('2016_05_06_090622_update_user_profile_table', 21),
('2016_05_12_104535_add_slug_to_campaigns_table', 22),
('2016_05_13_041103_create_comments_table', 23),
('2016_05_10_125233_create_donation_table', 24),
('2016_05_11_094158_update_compaign_table', 24),
('2016_05_17_062211_remove_media_url_from_campaigns_table', 25),
('2016_05_17_062307_add_media_url_to_campaign_design_table', 25),
('2016_05_17_100038_create_campaign_updates_table', 26),
('2016_05_17_110501_add_charity_details_to_user_profile_table', 27),
('2016_05_18_063023_add_duration_to_campaigns_table', 28),
('2016_05_18_063254_add_duration_to_campaigns_table', 29),
('2016_05_18_091013_remove_campaign_description_from_campaign_design_table', 30),
('2016_05_18_091433_add_campaign_description_to_campaign_design_table', 31),
('2016_05_18_091706_add_campaign_description_to_campaign_design_table', 32),
('2016_05_18_103547_add_campaign_currency_to_campaigns_table', 33),
('2016_05_18_120817_remove_phone_from_user_profile_table', 34),
('2016_05_19_050645_add_web_to_user_profile_table', 35),
('2016_05_20_122145_remove_status_from_campaigns_table', 36),
('2016_05_20_122220_add_status_to_campaigns_table', 36),
('2016_05_17_074452_add_profile_pic_to_user_profile', 37),
('2016_05_24_101147_add_slug_to_user_profile', 37),
('2016_05_23_041602_create_payment_details_table', 38),
('2016_05_27_065827_add_is_anonymous_column_to_comments_table', 38),
('2016_05_31_051755_remove_email_from_campaigns_table', 39),
('2016_05_31_051842_add_email_to_campaigns_table', 39),
('2016_05_26_072808_create_followers_table', 40),
('2016_05_30_094715_create_nearby_campaigns_stored_procedure', 40),
('2016_06_02_094407_add_donation_id_to_comments_table', 41),
('2016_06_03_090417_create_menu_category_table', 42),
('2016_06_03_090950_create_menus__table', 42),
('2016_06_07_094849_create_content_table', 43),
('2016_06_10_045012_create_banners_table', 44),
('2016_06_16_051638_remove_campaign_currency_from_campaigns', 45),
('2016_06_16_051759_add_campaign_currency_from_campaigns', 45),
('2016_06_16_071332_add_charges_payment_option', 46),
('2016_06_16_145544_add_columns_to_campaign_donations', 47),
('2016_06_16_115512_add_columns_to_campaign_payments', 48),
('2016_06_16_120524_remove_keys_from_campaign_payments', 48),
('2016_06_16_152503_add_application_fees_to_campaign_payments', 48),
('2016_06_15_102009_drop_profile_pic_from_user_profile', 49),
('2016_06_15_102212_change_default_profile_pic_in_user_profile', 49);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `options_key_unique` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'site.name', 'Givebright', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(2, 'site.slogan', 'Create Campaign ', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(3, 'site.description', 'My Site.', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(4, 'site.keywords', 'pingpong, gravitano', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(5, 'tracking', '<!-- GA Here -->', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(6, 'facebook.link', 'https://www.facebook.com/pingponglabs', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(7, 'twitter.link', 'https://twitter.com/pingponglabs', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(8, 'post.permalink', '{slug}', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(9, 'ckfinder.prefix', 'packages/pingpong/admin', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(10, 'admin.theme', 'default', '2016-04-13 05:11:01', '2016-04-13 05:11:01'),
(11, 'pagination.perpage', '10', '2016-04-13 05:11:01', '2016-04-13 05:11:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE IF NOT EXISTS `payment_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `donation_id` int(10) unsigned NOT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `payment_details_donation_id_index` (`donation_id`),
  KEY `payment_details_payment_id_index` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_options`
--

CREATE TABLE IF NOT EXISTS `payment_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `charge` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `payment_options`
--

INSERT INTO `payment_options` (`id`, `name`, `label`, `charge`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Givebrite', 'color-logo.png', '0', 'Use our very own Debit/Credit card system, we only charge 0.0%', 'no', NULL, NULL),
(2, 'Paypal', 'paypal.png', '3.4', 'PayPal will take 3.4% from the total transaction', 'no', NULL, NULL),
(3, 'Stripe', 'stripe.png', '1.4', 'Stripe will take 1.4% from the total transaction', 'yes', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Manage Users', 'manage_users', 'Manage Users', '2016-04-13 05:11:02', '2016-04-13 05:11:02'),
(2, 'Manage Articles', 'manage_articles', 'Manage Articles', '2016-04-13 05:11:02', '2016-04-13 05:11:02'),
(3, 'Manage Pages', 'manage_pages', 'Manage Pages', '2016-04-13 05:11:02', '2016-04-13 05:11:02'),
(4, 'Manage Categories', 'manage_categories', 'Manage Categories', '2016-04-13 05:11:02', '2016-04-13 05:11:02'),
(5, 'Manage Settings', 'manage_settings', 'Manage Settings', '2016-04-13 05:11:02', '2016-04-13 05:11:02'),
(6, 'Manage Roles', 'manage_roles', 'Manage Roles', '2016-04-13 05:11:02', '2016-04-13 05:11:02'),
(7, 'Manage Permissions', 'manage_permissions', 'Manage Permissions', '2016-04-13 05:11:02', '2016-04-13 05:11:02');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rewards`
--

CREATE TABLE IF NOT EXISTS `rewards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rewards`
--

INSERT INTO `rewards` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Badges', NULL, NULL),
(2, 'Gift Certificates', NULL, NULL),
(3, 'T-Shirts', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'The one who manage the site', '2016-04-13 05:11:02', '2016-04-13 05:11:02'),
(2, 'User', 'user', '', '2016-04-13 05:43:11', '2016-04-13 05:43:11'),
(3, 'Charity', 'charity', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2016-06-23 07:38:59', '2016-06-23 07:38:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sign_up_tag` enum('facebook','twitter','google') COLLATE utf8_unicode_ci DEFAULT NULL,
  `sign_up_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `sign_up_tag`, `sign_up_id`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Give Brite', 'admin@givebrite.com', '$2y$10$.By8u4LeKGdynvdq6nuUd.OYM26MGY5w1yQ2YqaraiUQxaTn5eYnS', NULL, NULL, NULL, '2016-06-23 07:38:59', '2016-06-23 07:38:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_activation`
--

CREATE TABLE IF NOT EXISTS `user_activation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `token_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `otp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `otp_verified` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_activation_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_activation`
--

INSERT INTO `user_activation` (`id`, `user_id`, `token_code`, `is_activated`, `otp_code`, `otp_verified`, `created_at`, `updated_at`) VALUES
(1, 1, 'ghKs9fjBA4SPWcuh07BTieFVbK0aGy', 1, '', 0, '2016-06-23 07:38:59', '2016-06-23 07:38:59');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sur_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registered_charity` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `recieve_updates` tinyint(1) DEFAULT '0',
  `charity_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `charity_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_profile_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `first_name`, `sur_name`, `registered_charity`, `recieve_updates`, `charity_logo`, `registration_no`, `charity_name`, `contact_telephone`, `web`, `created_at`, `updated_at`, `address`, `postcode`, `description`, `slug`, `profile_pic`) VALUES
(1, 1, 'Give', 'Brite', 'no', 1, NULL, NULL, NULL, NULL, NULL, '2016-06-23 07:38:59', '2016-06-23 07:38:59', '', '', NULL, 'give_brite', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hits` int(11) NOT NULL,
  `online` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `campaign_donations`
--
ALTER TABLE `campaign_donations`
  ADD CONSTRAINT `campaign_donations_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `menu_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payment_details`
--
ALTER TABLE `payment_details`
  ADD CONSTRAINT `payment_details_donation_id_foreign` FOREIGN KEY (`donation_id`) REFERENCES `campaign_donations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_activation`
--
ALTER TABLE `user_activation`
  ADD CONSTRAINT `user_activation_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `user_profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
