<?php

use Illuminate\Database\Seeder;

class CampaignThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campaign_themes')->truncate();
         // Insert Table types
        DB::table('campaign_themes')->insert([
                    [
                        'name'=>'Red',
                        'value'=>'#ff0000'
                    ],
                    [
                        'name'=>'Blue',
                        'value'=>'#0000ff'
                    ],
                    [
                        'name'=>'Yellow',
                        'value'=>'#ffff00'
                    ],
                    [
                        'name'=>'Green',
                        'value'=>'#00ff00'
                    ],
                    [
                        'name'=>'Sky-blue',
                        'value'=>'#00ffff'
                    ]
        ]);
    }
}
