<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert Admin User
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@givebrite.com',
            'password' => bcrypt('admin1234'),
            'status'=>'1'
        ]);
        // Insert Admin User
        DB::table('user_activation')->insert([
            'user_id' => '1',
            'is_activated' => '1',
        ]);
         DB::table('role_user')->insert([
            'role_id' => '1',
            'user_id' => '1',
        ]);
         DB::table('user_profile')->insert([
            'user_id' => '1',
             'first_name' =>'Admin',
             'sur_name'=>'',
        ]);
    }
}
