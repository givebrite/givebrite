<?php

use Illuminate\Database\Seeder;

class PaymentOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_options')->truncate();
         // Insert Table types
        DB::table('payment_options')->insert([
                    [
                        'name'=>'Givebrite',
                        'label'=>'color-logo.png',
                        'description'=>'Use our very own Debit/Credit card system, we only charge 0.0%',
                        'status'=>'no'
                    ],
                    [
                        'name'=>'Paypal',
                        'label'=>'paypal.png',
                        'description'=>'PayPal will take 3.4% from the total transaction',
                        'status'=>'no'
                    ],
                    [
                        'name'=>'Stripe',
                        'label'=>'stripe.png',
                        'description'=>'Stripe will take 1.9% from the total transaction',
                        'status'=>'yes'
                    ]
                    ]);
    }
}
