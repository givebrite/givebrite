<?php

use Illuminate\Database\Seeder;

class CampaignPaymentsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('campaign_payments')->insert([

            'user_id' => 115,
            'campaign_id' => 1,
            'payment_option_id' => 3,
            'live_secret_key' => 'sk_test_ZVSGpDtHsIGW4xJSU4q3rwtJ',
            'live_publishable_key' => 'pk_test_0qQj1C9RrLhpGmDDwRaZGyMr',
            'status' => 'yes',
            'created_at' => new \Carbon\Carbon(),
            'updated_at' => new \Carbon\Carbon(),
        ]);
    }

}
