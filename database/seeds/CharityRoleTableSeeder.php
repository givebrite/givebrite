<?php

use Illuminate\Database\Seeder;

class CharityRoleTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'Charity',
            'slug' => 'charity',
            'description' => 'Charity Role',
            'created_at' => new \Carbon\Carbon(),
            'updated_at' => new \Carbon\Carbon(),
        ]);
    }

}
