<?php

use Illuminate\Database\Seeder;

class CampaignCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campaign_categories')->truncate();

                // Insert Table types
                DB::table('campaign_categories')->insert([
                    [
                        'name' => 'Business',
                        'slug' => 'business'
                    ],
                    [
                        'name' => 'Charity',
                        'slug' => 'charity'
                    ],
                    [
                        'name' => 'Community',
                        'slug' => 'community'
                    ],
                    [
                        'name' => 'Competitions',
                        'slug' => 'competitions'
                    ],
                    [
                        'name' => 'Creative',
                        'slug' => 'creative'
                    ],
                    [
                        'name' => 'Events',
                        'slug' => 'events'
                    ],
                    [
                        'name' => 'Faith',
                        'slug' => 'faith'
                    ],
                    [
                        'name' => 'Family',
                        'slug' => 'family'
                    ],
                    [
                        'name' => 'National',
                        'slug' => 'national'
                    ],
                    [
                        'name' => 'News',
                        'slug' => 'news'
                    ],
                    [
                        'name' => 'Newlyweds',
                        'slug' => 'newlyweds'
                    ],
                    [
                        'name' => 'Other',
                        'slug' => 'other'
                    ],
                    [
                        'name' => 'Travel',
                        'slug' => 'travel'
                    ],
                    [
                        'name' => 'Wishes',
                        'slug' => 'wishes'
                    ]
                    ]);
    }
}
