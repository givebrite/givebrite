<?php

use Illuminate\Database\Seeder;

class AdminArticleTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('contents')->truncate();

        // Insert Table types
        DB::table('contents')->insert([
            [
                'title' => 'Fundraise Online',
                'slug' => 'fundraise-online',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'title' => 'Frequently Asked Questions',
                'slug' => 'faq',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'Privacy Policy',
                'slug' => 'privacy',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'Terms &amp; Conditions',
                'slug' => 'terms-conditions',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'Fundrasing Ideas',
                'slug' => 'fundraise-idea',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'Success Stories',
                'slug' => 'success-stories',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'Pricing',
                'slug' => 'pricing',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'Contact us',
                'slug' => 'contact-us',
                'status' => '1',
                'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'How we compare',
                'slug' => 'how-we-compare',
                'status' => '1',
                'description' => '<div class="compare-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center">
            <button class="flat-blue-lg-btn">How We Compare</button>
          </div>
          <div class="col-xs-12">
            <div class="table-responsive compareTable">
              <table>
                <tr>
                  <th></th>
                  <th class="theme-color">GiveBrite</th>
                  <th>GoFundMe</th>
                  <th>GoGetFunding</th>
                </tr>
                <tr>
                  <td class="featuresHeading" colspan="4">Features</td>
                </tr>
                <tr>
                  <td>Feature 01</td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                </tr>
                <tr>
                  <td>Feature 02</td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                </tr>
                <tr>
                  <td>Feature 03</td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />
                  </td>
                </tr>
                <tr>
                  <td>Feature 04</td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />
                  </td>
                </tr>
                <tr>
                  <td>Feature 05</td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-check.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />
                  </td>
                  <td>
                    <img src="http://54.83.205.169/givebrite/public/site/images/color-cross.png" alt="givebrite" />
                  </td>
                </tr>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'Why Givebrite',
                'slug' => 'why-givebrite',
                'status' => '1',
                'description' => '<div class="btn-block text-center">
                <button class="green-flat-btn">Why GiveBrite?</button>
            </div>
            <div class="checkList">
                <ul class="clearfix">
                    <li>Feature 1</li>
                    <li>Feature 2</li>
                    <li>Feature 3</li>
                    <li>Feature 4</li>
                    <li>Feature 5</li>
                    <li>Feature 6</li>
                </ul>
            </div>
            <div class="btn-block text-center">
                <a class="flat-orange-btn" href="register">Sign Up for Free</a>
            </div>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'About Us',
                'slug' => 'about',
                'status' => '1',
                'description' => '<div class="aboutbanner">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center">
                <img src="http://54.83.205.169/givebrite/public/site/images/logo-big.png" alt="Profile" />
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bannerHeading">About Us</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa</p>
                    </div>
                    <div class="col-md-6">
                        <div class="bannerHeading">Overview</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()],
            [
                'title' => 'How It Works',
                'slug' => 'how-it-works',
                'status' => '1',
                'description' => '<div class="videoWrapper">
    <div class="playBtn">
        <object height="100%" width="100%" data="http://www.youtube.com/v/Ahg6qcgoay4" type="application/x-shockwave-flash"><param name="src" value="http://www.youtube.com/v/Ahg6qcgoay4"></object>
        <button class="blue-btn-lg vedio-button">Watch The Video</button>
    </div>
</div>
<div class="hitPage">
    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h6>How It Works In <span>5 Easy Steps</span></h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">1</button>
                    <h6>Sign Up For <span>Free</span></h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <div class="imgContain">
                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-1.png" alt="How its work" /> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">2</button>
                    <h6>Set Your <span>Goals</span></h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <div class="imgContain">
                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-2.png" alt="How its work" /> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">3</button>
                    <h6>Customise Your <span>Campaign</span></h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <div class="imgContain">
                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-3.png" alt="How its work" /> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">4</button>
                    <h6>Reward Your <span>Donors</span></h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <div class="imgContain">
                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-4.png" alt="How its work" /> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <button class="cirButton">5</button>
                    <h6>Reward Your <span>Donors</span></h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <div class="imgContain">
                        <img src="http://54.83.205.169/givebrite/public/site/images/how-its-work-5.png" alt="How its work" /> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>',
                'created_at' => new \Carbon\Carbon(), 'updated_at' => new \Carbon\Carbon()]
        ]);
    }

}
