<?php

use Illuminate\Database\Seeder;

class CampaignLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campaign_locations')->truncate();

                // Insert Table types
                DB::table('campaign_locations')->insert([
                    [
                        'name' => 'Riyadh',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Makkah',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Al Madinah',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Eastern',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Asir',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Al-Qassim',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Tabuk',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Hail',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Najran',
                        'parent_id' => '0'
                    ],
                    [
                        'name' => 'Riyadh',
                        'parent_id' => '1'
                    ],
                    [
                        'name' => 'Jeddah',
                        'parent_id' => '2'
                    ],
                    [
                        'name' => 'Mecca',
                        'parent_id' => '2'
                    ],
                    [
                        'name' => 'Medina',
                        'parent_id' => '3'
                    ],
                    [
                        'name' => 'Hofuf',
                        'parent_id' => '4'
                    ],
                    [
                        'name' => 'Dammam',
                        'parent_id' => '4'
                    ],
                    [
                        'name' => 'Khamis Mushait',
                        'parent_id' => '5'
                    ],
                    [
                        'name' => 'Buraidah',
                        'parent_id' => '6'
                    ],
                    [
                        'name' => 'Khobar',
                        'parent_id' => '4'
                    ],
                    [
                        'name' => 'Tabuk',
                        'parent_id' => '7'
                    ],
                    [
                        'name' => 'Hafar Al-Batin',
                        'parent_id' => '4'
                    ],
                    [
                        'name' => 'Jubail',
                        'parent_id' => '4'
                    ],
                    [
                        'name' => 'Al-Kharj',
                        'parent_id' => '1'
                    ],
                    [
                        'name' => 'Qatif',
                        'parent_id' => '4'
                    ],
                    [
                        'name' => 'Abha',
                        'parent_id' => '5'
                    ],
                    [
                        'name' => 'Najran',
                        'parent_id' => '8'
                    ],
                    [
                        'name' => 'Yanbu',
                        'parent_id' => '3'
                    ],
                    [
                        'name' => 'Al Qunfudhah',
                        'parent_id' => '2'
                    ]
                    ]);
    }
}
