<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);

        Model::reguard();
//	$this->call(RoleAndUserSeeder::class);
	$this->call(AdminArticleTableSeeder::class);
	$this->call(AdminMenuCategoryTableSeeder::class);
    }
}
