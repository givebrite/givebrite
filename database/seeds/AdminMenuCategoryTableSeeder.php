<?php

use Illuminate\Database\Seeder;

class AdminMenuCategoryTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Insert Table types
        $menuCatArray = [
            [
                'title' => 'Home header1',
                'slug' => 'home-header1',
                'status' => '1',
                'position' => 'header1',
            ],
            [
                'title' => 'Internal Page Header',
                'slug' => 'internal-page-header',
                'status' => '1',
                'position' => 'header2',
            ],
            [
                'title' => 'Footer 1',
                'slug' => 'footer-1',
                'status' => '1',
                'position' => 'footer1',
            ],
            [
                'title' => 'Footer 2',
                'slug' => 'footer-2',
                'status' => '1',
                'position' => 'footer2',
            ],
            [
                'title' => 'Footer 3',
                'slug' => 'footer-3',
                'status' => '1',
                'position' => 'footer3',
            ],
            [
                'title' => 'About Side bar',
                'slug' => 'about-side-bar',
                'status' => '1',
                'position' => 'about-sidebar',
            ],
            [
                'title' => 'Dashboard Side bar',
                'slug' => 'dashboard-side-bar',
                'status' => '1',
                'position' => 'dashboard-sidebar',
            ]
        ];


        // Insert Table types
        $homeHeader1 = [
            [
                'title' => 'Causes',
                'slug' => 'causes',
                'link' => 'campaigns?type=cause',
                'status' => '1',
                'position' => '1',
            ],
            [
                'title' => 'Charities',
                'slug' => 'charities',
                'link' => 'charity',
                'status' => '1',
                'position' => '2',
            ],
            [
                'title' => 'How It Works',
                'slug' => 'how-it-works',
                'link' => 'articles/how-it-works',
                'status' => '1',
                'position' => '3',
            ],
            [
                'title' => 'About',
                'slug' => 'about',
                'link' => 'about-us',
                'status' => '1',
                'position' => '4',
            ],
        ];

        $homeHeader2 = [
            [
                'title' => 'About Us',
                'slug' => 'about-us',
                'link' => 'about-us',
                'status' => '1',
                'position' => '1',
            ],
            [
                'title' => 'Campaign',
                'slug' => 'campaign',
                'link' => 'pages/campaign',
                'status' => '1',
                'position' => '2',
            ],
            [
                'title' => 'How It Works',
                'slug' => 'how-it-works',
                'link' => 'articles/how-it-works',
                'status' => '1',
                'position' => '3',
            ]
        ];

        $aboutSidebar = [
            [
                'title' => 'Success Stories',
                'slug' => 'success-stories',
                'link' => 'pages/success-stories',
                'status' => '1',
                'position' => '2',
            ],
            [
                'title' => 'How It Works',
                'slug' => 'how-it-works',
                'link' => 'articles/how-it-works',
                'status' => '1',
                'position' => '1',
            ],
            [
                'title' => 'FAQ',
                'slug' => 'faq',
                'link' => 'pages/faq',
                'status' => '1',
                'position' => '3',
            ],
            [
                'title' => 'Pricing',
                'slug' => 'pricing',
                'link' => 'pages/pricing',
                'status' => '1',
                'position' => '4',
            ],
            [
                'title' => 'Contact us',
                'slug' => 'contact-us',
                'link' => 'pages/contact-us',
                'status' => '1',
                'position' => '5',
            ],
        ];

        $footer1 = [
            [
                'title' => 'About Us',
                'slug' => 'about-us',
                'link' => 'about-us',
                'status' => '1',
                'position' => '1',
            ],
            [
                'title' => 'Fundraise Online',
                'slug' => 'fundraise-online',
                'link' => 'pages/fundraise-online',
                'status' => '1',
                'position' => '2',
            ],
            [
                'title' => 'Fundrasing Ideas',
                'slug' => 'fundrasing-ideas',
                'link' => 'pages/fundraise-idea',
                'status' => '1',
                'position' => '3',
            ]
        ];

        $footer2 = [
            [
                'title' => 'About Us',
                'slug' => 'about-us',
                'link' => 'about-us',
                'status' => '1',
                'position' => '1',
            ],
            [
                'title' => 'FAQ',
                'slug' => 'faq',
                'link' => 'pages/faq',
                'status' => '1',
                'position' => '2',
            ],
            [
                'title' => 'Privacy Policy',
                'slug' => 'privacy-policy',
                'link' => 'pages/privacy',
                'status' => '1',
                'position' => '3',
            ],
            [
                'title' => 'Terms &amp; Conditions',
                'slug' => 'terms-amp-conditions',
                'link' => 'pages/terms-conditions',
                'status' => '1',
                'position' => '4',
            ]
        ];

        $footer3 = [
            [
                'title' => 'About Us',
                'slug' => 'about-us',
                'link' => 'about-us',
                'status' => '1',
                'position' => '1',
            ],
            [
                'title' => 'FAQ',
                'slug' => 'faq',
                'link' => 'pages/faq',
                'status' => '1',
                'position' => '2',
            ],
            [
                'title' => 'Privacy Policy',
                'slug' => 'privacy-policy',
                'link' => 'pages/privacy',
                'status' => '1',
                'position' => '3',
            ],
            [
                'title' => 'Terms &amp; Conditions',
                'slug' => 'terms-amp-conditions',
                'link' => 'pages/terms-conditions',
                'status' => '1',
                'position' => '4',
            ]
        ];

        $menuArray = [
            $homeHeader1,
            $homeHeader2,
            $footer1,
            $footer2,
            $footer3,
            $aboutSidebar
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('menus')->truncate();
        DB::table('menu_categories')->truncate();
        for ($i = 0; $i < count($menuCatArray); $i++) {

            $newMenuCat = array_merge( $menuCatArray[$i], array(
                "created_at" => new \Carbon\Carbon(),
                "updated_at" => new \Carbon\Carbon()
                    )
            );

            DB::table('menu_categories')->insert($newMenuCat);
            $catId = DB::getPdo()->lastInsertId();

            for ($j = $i; $j < count($menuArray); $j++) {
                for ($k = 0; $k < count($menuArray[$j]); $k++) {
                    $newMenu = array_merge( $menuArray[$j][$k], array(
                        "cat_id" => $catId,
                        "created_at" => new \Carbon\Carbon(),
                        "updated_at" => new \Carbon\Carbon()
                            )
                    );
                    DB::table('menus')->insert($newMenu);
                }

                break;
            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
