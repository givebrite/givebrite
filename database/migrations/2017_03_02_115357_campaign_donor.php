<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampaignDonor extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // Create realtion table of campaign and donor_type
        Schema::create('campaign_donor', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->integer('donor_type_id');
            $table->integer('is_default');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        // Reverse the changes
        Schema::drop('campaign_donor');
    }

}
