<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DonationType extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // Create table donation type
        Schema::create('donation_type', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        // Destory Table
        Schema::drop('donation_type');
    }

}
