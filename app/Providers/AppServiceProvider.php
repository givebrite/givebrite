<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Illuminate\Validation\Factory;
use Input;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Validator::extend('image_dimension', function($attribute, $value, $parameters, $validator) {
              $file = Input::file($attribute);
              $image_info = getimagesize($file);
              $image_width = $image_info[0];
              $image_height = $image_info[1];
              if( (isset($parameters[0]) && $parameters[0] != 0) && $image_width > $parameters[0]){
                 return false;
              }
                  
              if( (isset($parameters[1]) && $parameters[1] != 0) && $image_height > $parameters[1] )
              {
                  return false;
              }
              return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
