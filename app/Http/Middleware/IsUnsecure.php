<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use Session;

class IsUnsecure
{

    public function __construct()
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
    	    // No caching for pages
        $request->header("Pragma", "no-cache");
        $request->header("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
        $request->header("Expires", "Sat, 26 Jul 1997 05:00:00 GMT");
         //commented BY DS to setup on unsecure http at port 80
        /*if (!$request->secure() && env('APP_SECURE') === true) {
       	    return redirect()->secure($request->getRequestUri());
        }*/

        return $next($request); 
    }

}
