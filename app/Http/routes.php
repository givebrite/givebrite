<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::group(['prefix' => '', 'middleware' => ['App\Http\Middleware\IsUnsecure']], function () {
   
        Route::get('/new', ['as' => 'site.index', 'uses' => 'SiteController@index']);
       
        Route::get('/', ['as' => 'site.index', 'uses' => 'SiteController@new_index']);
       
        Route::get('/clear-cache', function () {
            $exitCode = Artisan::call('cache:clear');
            // return what you want
        });
        

        Route::get('downloadExcel/{type}/{campaignId}/{charityId?}', 'MaatwebsiteDemoController@downloadExcel');
        Route::get('downloadGiftAidExcel/{type}/{campaignId}/{charityId?}', 'MaatwebsiteDemoController@downloadGiftAidExcel');

        Route::get('test','MaatwebsiteDemoController@test');


        Route::get('/reset-campaign-database', function () {
        die("failed");
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('comments')->truncate();
        DB::table('payment_details')->truncate();
        DB::table('campaign_updates')->truncate();
        DB::table('campaign_rewards')->truncate();
        DB::table('campaign_design')->truncate();
        DB::table('campaign_charities')->truncate();
        DB::table('campaign_payments')->truncate();
        DB::table('campaign_locations')->truncate();
        DB::table('user_profile')->truncate();
        DB::table('user_activation')->truncate();
        DB::table('role_user')->truncate();
        DB::table('password_resets')->truncate();
        DB::table('followers')->truncate();
        DB::table('campaigns')->truncate();
        DB::table('users')->truncate();
        DB::table('campaign_donations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    });

    Route::get('/pages/{slug}', 'SiteFooterController@verifySlug');
    Route::get('/about-us', 'SiteController@about');
    Route::get('/articles/{slug}', 'SiteController@verifySlug');
// Set Charity Id By Clicking On FundRaise for us
    Route::post('charity/set-id', ['as' => 'charity.set.id', 'uses' => 'CharityController@setId']);

    Route::get('/campaigns', ['as' => 'campaign-list-home', 'uses' => 'CampaignController@index']);
    Route::get('/campaigns/category/{category}', ['as' => 'campaign-by-category', 'uses' => 'CampaignController@index']);
    Route::get('/campaigns/type/{type}', ['as' => 'campaign-by-type', 'uses' => 'CampaignController@index']);
    //ajax route
    Route::post('/campaigns/type/{type}', ['as' => 'campaign-by-type', 'uses' => 'CampaignController@search']);
    
    Route::get('campaigns/popular_campaigns', ['as' => 'popular.campaign', 'uses' => 'CampaignController@popularCampaigns']);
    Route::post('campaigns/area_campaigns', ['as' => 'area.campaign', 'uses' => 'CampaignController@areaCampaigns']);
// Route::get('charity', ['as' => 'charity.listing', 'uses' => 'CharityController@getCharityList']);
    Route::get('charity', ['as' => 'charity.listing', 'uses' => 'CharityController@index']);
    Route::get('charity-list', ['as' => 'charity.list_ajax', 'uses' => 'CharityController@getCharities']);

    Route::get('charity/{slug}', ['as' => 'charity.detail', 'uses' => 'CharityController@getCharityDetail']);
    Route::get('search/charity', ['as' => 'search-charity-list', 'uses' => 'CharityController@index']);

    Route::get('404', ['as' => '404', 'uses' => 'SiteController@page404']);
});

Route::get('team-details/{team_slug}', ['uses' => 'TeamController@show']);

Route::group(['prefix' => '', 'middleware' => ['Modules\Auth\Http\Middleware\IsLaunchedMiddleware', 'App\Http\Middleware\IsUnsecure']], function () {
    
    Route::get('campaign/{campaign_slug}', ['as' => 'campaign.show', 'uses' => 'CampaignController@show']);



    Route::get('campaign/{campaign_slug}/updates', ['as' => 'campaign.update.list', 'uses' => 'CampaignController@campaignUpdates']);
    Route::get('campaign/{campaign_slug}/donors', ['as' => 'campaign.donor.list', 'uses' => 'CampaignController@campaignDonors']);
    Route::get('campaign/{campaign_slug}/offline-donation/', ['as' => 'campaign.offlinedonor.list', 'uses' => 'CampaignController@offlineDonation']);

    
});
Route::group(['prefix' => '', 'middleware' => ['Modules\Auth\Http\Middleware\IsLoginMiddleware', 'App\Http\Middleware\IsUnsecure']], function () {
// Routes for campaign detail save updates
    Route::post('campaigns/{campaignId}/save-updates', ['as' => 'campaign-updates.store', 'uses' => 'CampaignController@saveUpdates']);
    Route::get('campaigns/fb_campaigns', ['as' => 'fb.campaign', 'uses' => 'CampaignController@fbCampaigns']);
    Route::post('charity/follow', ['as' => 'charity.follow', 'uses' => 'CharityController@follow']);
    Route::get('donor/{slug}', ['as' => 'donar.profile', 'uses' => 'SiteController@getDonarProfile']);
    Route::get('donor/{slug}/funded_campaign', ['as' => 'donor.funded.campaign', 'uses' => 'SiteController@getDonarFundedCampaign']);
    Route::get('donor/{slug}/raised_campaign', ['as' => 'donor.raised.campaign', 'uses' => 'SiteController@getDonarRaisedCampaign']);
    Route::get('campaign/pause/{campaignId}', 'CampaignController@disableCampaign');
    Route::get('campaign/resume/{campaignId}', 'CampaignController@enableCampaign');
    Route::get('campaign/delete/{campaignId}', 'CampaignController@deleteCampaign');

    Route::post('offline', ['as' => 'donate.offline', 'uses' => 'DonationController@offlineDonate']);
});

Route::post('campaign/crop/{campaignId}', 'CampaignController@crop');

Route::get('campaign/crop/{campaignId}', 'CampaignController@getcrop');

Route::get('contact', 'ContactController@index');

Route::post('contact', 'ContactController@save');

Route::get('donation_list', 'DonationController@index');

Route::get('campaign/stravaActivity/{id}', 'CampaignController@stravaActivity');

Route::post('donation_update', 'CampaignDonorController@create');
Route::get('donation_update', 'CampaignDonorController@create');

Route::post('team/crop/', 'TeamController@crop');
Route::post('team/crop-logo/', 'TeamController@cropLogo');



/* web services api */
//Route::get('campaign/id/{campaignId?}', 'WebservicesController@id');

Route::get('campaign/id/{campaignId?}', array('middleware' => 'cors', 'uses' => 'WebservicesController@id'));
Route::get('campaign/id/{campaignId?}/{date}', array('middleware' => 'cors', 'uses' => 'WebservicesController@withDate'));


