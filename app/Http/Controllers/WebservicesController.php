<?php
namespace App\Http\Controllers;

use App\CampaignDonor;
use App\Http\Requests\CampaignUpdateRequest as CampaignUpdateRequest;
use Config;
use Helper;
use Illuminate\Http\Request;
use Input;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Campaigns\Repositories\EloquentCampaignUpdateRepository as CampaignUpdateRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as CampaignDonationRepo;
use Modules\Campaigns\Repositories\EloquentCampaignStravaActivityRepository as CampaignStravaActRepo;
use Pingpong\Admin\Uploader\ImageUploader;
use Pingpong\Modules\Routing\Controller;
use Redirect;
use Session;
use Toastr;
class WebservicesController  extends Controller{
    
    public function withDate($campaignId,$date) {

        
        if(empty($campaignId)){
            
            echo json_encode('one or more parameter is missing');
        
        }  else {
       
        $campaignDetail = [];
 
        $campaignDonationRepo = new CampaignDonationRepo();
       
        $list_of_donors = $campaignDonationRepo->donationFilterDate($campaignId,$date);
    

        $donation_type = new CampaignDonor();
        $donation = $donation_type->getAllWithJoin($campaignId);
        $campaignUpdateRepo = new CampaignUpdateRepo();
        $count = $campaignUpdateRepo->getCountOneCampaign($campaignId);
        $campaignUpdates = $campaignUpdateRepo->getUpdatesByCampaignId($campaignId);
        $totalDonorQuery = $campaignDonationRepo->findTotalDonorsCountByCampaignId($campaignId);
        $totalDonors = !empty($totalDonorQuery) ? $totalDonorQuery : 0;
        $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaignId);
        $totalAmountRaised = !empty($totalAmountRaisedQuery[0]->totalAmountRaised) ? $totalAmountRaisedQuery[0]->totalAmountRaised : 0;
        
        $campaignDetail['list_of_donors']=(!empty($list_of_donors)) ? $list_of_donors : 'No List of Donors';
        
        $sum = 0;

        foreach($campaignDetail['list_of_donors'] as $doners){

            $sum += $doners->donation_amount_monetory;
        }

        $campaignDetail['totalAmountRaised']= $sum;
        $campaignDetail['totalDonors']= count($campaignDetail['list_of_donors']);
        
        $CampaignStravaActRepo = new CampaignStravaActRepo();
        
        $campaign = $CampaignStravaActRepo->SumofCampaignId($campaignId);
        
        $campaign = (!empty($campaign)) ? $campaign : 'No Strava Activity';  
        
        $campaignDetail['total_distance']= $campaign;
        
        $campaignRepo = new CampaignRepo();
        
        $campaignDetails = $campaignRepo->findByids($campaignId);
        
        $campaignDetail['campaigns'] =  (!empty($campaignDetails)) ? $campaignDetails : 'No Campaign Details';
        
        print_r(json_encode($campaignDetail));


        }
    } 

    public function id($campaignId) {
        

       
        if(empty($campaignId)){
            
            echo json_encode('one or more parameter is missing');
        
        }  else {
       
        $campaignDetail = [];
 
        $campaignDonationRepo = new CampaignDonationRepo();
       
        $list_of_donors = $campaignDonationRepo->latest3Donation1($campaignId);
    

        $donation_type = new CampaignDonor();
        $donation = $donation_type->getAllWithJoin($campaignId);
        $campaignUpdateRepo = new CampaignUpdateRepo();
        $count = $campaignUpdateRepo->getCountOneCampaign($campaignId);
        $campaignUpdates = $campaignUpdateRepo->getUpdatesByCampaignId($campaignId);
        $totalDonorQuery = $campaignDonationRepo->findTotalDonorsCountByCampaignId($campaignId);
        $totalDonors = !empty($totalDonorQuery) ? $totalDonorQuery : 0;
        $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaignId);
        $totalAmountRaised = !empty($totalAmountRaisedQuery[0]->totalAmountRaised) ? $totalAmountRaisedQuery[0]->totalAmountRaised : 0;
        
        $campaignDetail['list_of_donors']=(!empty($list_of_donors)) ? $list_of_donors : 'No List of Donors';
        $campaignDetail['totalAmountRaised']=(!empty($totalAmountRaisedQuery[0]->totalAmountRaised)) ? 
        $totalAmountRaisedQuery[0]->totalAmountRaised :0;
        
        $campaignDetail['totalDonors']=$totalDonors;
        
        $CampaignStravaActRepo = new CampaignStravaActRepo();
        
        $campaign = $CampaignStravaActRepo->SumofCampaignId($campaignId);
        
        $campaign = (!empty($campaign)) ? $campaign : 'No Strava Activity';  
        
        $campaignDetail['total_distance']= $campaign;
        
        $campaignRepo = new CampaignRepo();
        
        $campaignDetails = $campaignRepo->findByids($campaignId);
        
        $campaignDetail['campaigns']=  (!empty($campaignDetails)) ? $campaignDetails : 'No Campaign Details';
        
        print_r(json_encode($campaignDetail));


        }
        
    }
    
 
}

