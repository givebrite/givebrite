<?php

namespace App\Http\Controllers;

use App\CampaignDonor as CampDonor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Input;

class CampaignDonorController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$data = array();
		$cmp_id = Input::get('cmp_id');
		$donor_type = Input::get('donor_type');
		foreach ($donor_type as $key => $name) {
			$chk = DB::table('donation_type')->where('title', $name)->get();
			if (empty($chk)) {
				$ins_res = DB::table('donation_type')->insert(['title' => $name]);
			}
		}

		foreach ($donor_type as $key => $name) {
			$res = DB::table('donation_type')->where('title', $name)->get();
			foreach ($res as $r) {
				$check_db = DB::table('campaign_donor')
					->where(function ($query) use ($cmp_id, $r) {
						$query->where('campaign_id', $cmp_id)
							->where('donor_type_id', $r->id);
					})
					->get();
				if (empty($check_db)) {
					array_push($data, $r->id);
				}
			}
		}

		foreach ($data as $key => $value) {
			$cmp_donor = new CampDonor();
			$cmp_donor->campaign_id = $cmp_id;
			$cmp_donor->donor_type_id = $value;
			$cmp_donor->is_default = '';
			$cmp_donor->save();
			$cmp_donor = '';
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

}
