<?php

namespace App\Http\Controllers;

use App\CampaignDonor;
use App\Http\Requests\CampaignUpdateRequest as CampaignUpdateRequest;
use Config;
use Helper;
use Illuminate\Http\Request;
use Image;
use Input;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Campaigns\Repositories\EloquentTeamRepository as TeamRepo;
use Modules\Campaigns\Repositories\EloquentCampaignUpdateRepository as CampaignUpdateRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as CampaignDonationRepo;
use Modules\Campaigns\Repositories\EloquentCampaignStravaActivityRepository as CampaignStravaActRepo;
use Pingpong\Admin\Uploader\ImageUploader;
use Pingpong\Modules\Routing\Controller;
use Redirect;
use Session;
use Toastr;


class CampaignController extends Controller
{

    /**
     * @var ImageUploader
     */
    protected $uploader;
    protected $imageStoragePath;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader)
    {
        $this->uploader = $uploader;
        $this->imageStoragePath = Config::get('config.upload_path');
    }

    /**
     * Function to set variable for views
     *
     * @author AK 03/10/16
     */
    public function setUp()
    {

    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return view('campaigns::errors.404');
    }

    /**
     * Function To Display Campaign Listing
     *
     */
    public function index(Request $request, $category = null, $type = null)
    {   
        $keyword = Input::get('keyword');
        $campaignCategory = !empty($request->route('category')) ? $request->route('category') : "";
        $campaignType = !empty($request->route('type')) ? $request->route('type') : "";
        // Set Filters And Sorting
        $filters = [
            'search_keyword' => $keyword,
            'campaign_category' => $campaignCategory,
            'campaign_type' => $campaignType,
        ];
        $sort = [];
        //Create Campaign Repo Instance
        $campaignRepo = new CampaignRepo();
        $campaignListQuery = $campaignRepo->getCampaignList($userId = 0, $filters, $sort);
        $campaigns = $campaignListQuery->paginate(Config('config.campaign_list_pagination'))->appends(Input::except('page'));

        $categoriesArray = $campaignRepo->getCampaignCategoriesWithSlug();

        return view('site.campaignlisting', compact('campaigns', 'filters', 'categoriesArray', 'campaignCategory', 'campaignType'));
    }

    public function search(Request $request, $category = null, $type = null){

        $keyword = Input::get('keyword');
        
        if($keyword == ''){

            return response()->json(array('code'=>404 ,'message' => 'No results found'));
        }

        $campaignCategory = !empty($request->route('category')) ? $request->route('category') : "";
        $campaignType = !empty($request->route('type')) ? $request->route('type') : "";
        // Set Filters And Sorting
        $filters = [
            'search_keyword' => $keyword,
            'campaign_category' => $campaignCategory,
            'campaign_type' => $campaignType,
        ];
        $sort = [];
        //Create Campaign Repo Instance
        $campaignRepo = new CampaignRepo();
        $campaignListQuery = $campaignRepo->getCampaignList($userId = 0, $filters, $sort);

        $campaigns = $campaignListQuery->get(); //$campaignListQuery->get()->toArray();//paginate(Config('config.campaign_list_pagination'))->appends(Input::except('page'));
                
        if ( count($campaigns)>0 ) {

            return response()->json(array('code'=>200 ,'campaigns' => $campaigns));
        
        } else {

            return response()->json(array('code'=>400 ,'message' => 'No results found'));
        }
       
        
    }


    /**
     * Display the specified campaign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($slug)
    {   
        
        // Get User Details From Session
        $user = Session::get('user_session_data');
        $userId = !empty($user['id']) ? $user['id'] : 0;
        $this->setup();
        $campaignDetail = [];
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $campaignDetails = $campaignRepo->findBySlug($slug);
        

        // Count of Updates
        // Creating Instance For Campaign Donation Repo\
        $campaignDonationRepo = new CampaignDonationRepo();

        $list_of_donors = $campaignDonationRepo->latest3Donation($campaignDetails[0]->campaignId);
        if (!empty($campaignDetails)) {
            $campaign = $campaignDetails[0];
        } else {
            $campaign = [];
        }

        $teamRepo = new TeamRepo();
        
        $teamDetails = $teamRepo->findByCampaignId($campaign->campaignId);

        //dd($teamDetails);

        $donation_type = new CampaignDonor();
        $donation = $donation_type->getAllWithJoin($campaign->campaignId);
        $campaignUpdateRepo = new CampaignUpdateRepo();
        $count = $campaignUpdateRepo->getCountOneCampaign($campaign->campaignId);
//$campaignUpdates = $campaignUpdateRepo->getUpdatesByCampaignId($campaign->campaignId)->paginate(Config('config.detail_backers_pagination'));
        $campaignUpdates = $campaignUpdateRepo->getUpdatesByCampaignId($campaign->campaignId)->paginate(Config('config.detail_backers_pagination'));
        $totalDonorQuery = $campaignDonationRepo->findTotalDonorsCountByCampaignId($campaign->campaignId);
        $totalDonors = !empty($totalDonorQuery) ? $totalDonorQuery : 0;
        $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaign->campaignId);
        $totalAmountRaised = !empty($totalAmountRaisedQuery[0]->totalAmountRaised) ? $totalAmountRaisedQuery[0]->totalAmountRaised : 0;

        //echo $campaignDetails[0]->campaign_name;
        //dd($campaign);

        if (!empty($campaign)) {
            return view('site.detail', compact('totalDonors', 'latestActivities', 'totalAmountRaised', 'user', 'campaign', 'campaignDetails', 'userId', 'count', 'list_of_donors', 'donation', 'campaignUpdates','teamDetails'));
        } else {
            return $this->redirectNotFound();
        }
    }

    public function saveUpdates(CampaignUpdateRequest $request, $campaignId)
    {
        // Get User Details From Session
        $user = Session::get('user_session_data');
        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findById($campaignId);
        if (!empty($user['id']) && $user['id'] == $campaign['user_id']) {
            $campaignUpdates['updates'] = Input::get('updates');
            $campaignUpdates['campaign_id'] = $campaignId;
            $campaignUpdates['user_id'] = $user['id'];
            // Creating Instance For Campaign Update Repo
            $campaignUpdateRepo = new CampaignUpdateRepo();
            $saveCampaignUpdates = $campaignUpdateRepo->create($campaignUpdates);
            Toastr::success("Updates Created Successfully.", $title = null, $options = []);
            return Redirect::back();
        }
    }

    /**
     * Function to get Campaign Updates
     */
    public function campaignUpdates($slug)
    {
        $campaignRepo = new CampaignRepo();
        // Creating Instance For Campaign Update Repo
        $campaignUpdateRepo = new CampaignUpdateRepo();
        $campaignDetails = $campaignRepo->findBySlug($slug);
        if (!empty($campaignDetails)) {
            $campaign = $campaignDetails[0];
        } else {
            $campaign = [];
        }
        $campaignUpdates = $campaignUpdateRepo->getUpdatesByCampaignId($campaign->campaignId)->paginate(Config('config.detail_backers_pagination'));
        $responseView = view('site.partials.campaign_updates', compact('campaignUpdates', 'campaign'))->render();
        echo json_encode(['responseCode' => 200, 'responseView' => $responseView]);
        die;
    }

    /**
     * Function to get Campaign Donors
     */
    public function campaignDonors($slug)
    {
        $campaignRepo = new CampaignRepo();
        // Creating Instance For Campaign Donation Repo\
        $campaignDonationRepo = new CampaignDonationRepo();
        $campaignDetails = $campaignRepo->findBySlug($slug);
        if (!empty($campaignDetails)) {
            $campaign = $campaignDetails[0];
        } else {
            $campaign = [];
        }

        $backersList = $campaignDonationRepo->findByCampaignId($campaignDetails[0]->campaignId)->paginate(Config('config.detail_backers_pagination'));
        $responseView = view('site.partials.backers', compact('backersList', 'campaign'))->render();
        echo json_encode(['responseCode' => 200, 'responseView' => $responseView]);
        die;
    }

    /**
     * function to count popular Campiagns
     */
    public function popularCampaigns()
    {
        $campaignRepo = new CampaignRepo();
        $campaignsList = $campaignRepo->getCampaignWithJoin(0)->paginate(Config('config.dashboard_home_pagination'));
        $page = Input::get('page');
        $responseView = view('site.partials.popular_campaign', compact('campaignsList', 'page'))->render();
        echo json_encode(['responseCode' => 200, 'responseView' => $responseView]);
        die;
    }

    /*
     * This Function return list of area campaigns via ajax
     */

    public function areaCampaigns()
    {
        $lattitude = Input::get("latitude");
        $longitude = Input::get("longitude");
        $areaCampaigns = [];
        if (!empty($lattitude) && !empty($longitude)) {
            // Get Minimum Distance And unit from config
            $minimumDistance = Config::get('config.minimum_distance_my_area_campaign');
            $unit = Config::get('config.minimum_distance_unit_my_area_campaign');
            // Creting Instance For Campaign Repo
            $campaignRepo = new CampaignRepo();
            $page = Input::get('page');
            $areaCampaigns = $campaignRepo->findCampaignsByArea($lattitude, $longitude, $minimumDistance)->paginate(Config('config.dashboard_home_pagination'));
        }
        $responseView = view('site.partials.myarea_campaign', compact('areaCampaigns', 'page'))->render();
        echo json_encode(['responseCode' => 200, 'responseView' => $responseView]);
        die;
    }

    /*
     * This Function return list of fb campaigns via ajax
     */

    public function fbCampaigns()
    {
        // Get User Details From Session
        $user = Session::get('user_session_data');
        $fbCampaignsList = [];
        if (!empty($user['email'])) {
            $fbCampaignsList = Helper::getFriendsCampaigns($user['email'])->paginate(Config('config.dashboard_home_pagination'));
        }
        $page = Input::get('page');
        $responseView = view('site.partials.myfb_campaign', compact('fbCampaignsList', 'page'))->render();
        echo json_encode(['responseCode' => 200, 'responseView' => $responseView]);
        die;
    }

    public function crop(Request $request, $campaignId)
    {   
        
        //dd($request->all());
        $x = $request->input('x');
        $y = $request->input('y');
        $w = $request->input('w');
        $h = $request->input('h');

        $base64_string = $request->input('imgpath');
        Helper::CropImageHelper($x, $y, $w, $h, $base64_string);
        $sessionData = array('msg' => 'success', 'imageName' => Session::get('image'));
        echo json_encode($sessionData);

        //return redirect()->back();
    }

    public function disableCampaign($campaignId)
    {
        $campaignObj = new CampaignRepo();
        $res = $campaignObj->campaignActivation($campaignId, 0);
        if ($res) {
            Toastr::success("Campaign paused successfully", $title = null, $options = []);
        }
        return redirect()->back();
    }
    
    public function offlineDonation($slug)
    {
        
        // Get User Details From Session
        $user = Session::get('user_session_data');
        $userId = !empty($user['id']) ? $user['id'] : 0;
        $this->setup();
        $campaignDetail = [];
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $campaignDetails = $campaignRepo->findBySlug($slug);
        // Count of Updates
        // Creating Instance For Campaign Donation Repo\
        $campaignDonationRepo = new CampaignDonationRepo();

        $list_of_donors = $campaignDonationRepo->latest3Donation($campaignDetails[0]->campaignId);
        if (!empty($campaignDetails)) {
            $campaign = $campaignDetails[0];
        } else {
            $campaign = [];
        }

        $donation_type = new CampaignDonor();
        $donation = $donation_type->getAllWithJoin($campaign->campaignId);
        $campaignUpdateRepo = new CampaignUpdateRepo();
        $count = $campaignUpdateRepo->getCountOneCampaign($campaign->campaignId);
        
        if (!empty($campaign)) {
            return view('site.offlinedonation', compact('user', 'campaign', 'campaignDetails', 'userId', 'count','donation'));
        } else {
            return $this->redirectNotFound();
        }
    }

    public function enableCampaign($campaignId)
    {
        $campaignObj = new CampaignRepo();
        $res = $campaignObj->campaignActivation($campaignId, 1);
        if ($res) {
            Toastr::success("Campaign resumed successfully", $title = null, $options = []);
        }
        return redirect()->back();
    }

    public function deleteCampaign($campaignId)
    {
        $campaignObj = new CampaignRepo();
        $res = $campaignObj->delCampaign($campaignId);
        if ($res) {
            Toastr::success("Campaign Deleted Successfully.", $title = null, $options = []);
        } else {
            Toastr::success("Campaign has Donations, Could not be deleted.", $title = null, $options = []);
        }
        return redirect()->back();
    }


    public function stravaActivity($id)
    {
        $latestActivities = "";

        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findById($id);

        $stravaObj = new CampaignStravaActRepo();
                
   
        if (!empty($campaign->strava_access_token)) {
            
            $storeActivities = $stravaObj->delete($campaign->id);
//print_r($campaign->id);
            $fetchActCMD = 'curl -G https://www.strava.com/api/v3/athlete/activities -H "Authorization: Bearer ' . $campaign->strava_access_token . '"';

            $activities = json_decode(exec($fetchActCMD, $resultant));
          
            foreach ($activities as $act) {
                $fetchMapCMD = 'curl -G https://www.strava.com/api/v3/activities/' . $act->id . '/streams/latlng -H "Authorization: Bearer ' . $campaign->strava_access_token . '" -d resolution=low';

                $mapImagData = json_decode(exec($fetchMapCMD, $graphMapResult));
                $latfirst = $mapImagData[0];

                $startLatLon = reset($latfirst->data);
                $endLatLon = end($latfirst->data);
           
$gmap='http://maps.googleapis.com/maps/api/staticmap?size=640x400&path=enc:' . $act->map->summary_polyline . '&markers=color:green|' . $startLatLon[0] . ',' . $startLatLon[1] . '&markers=color:blue|' . $endLatLon[0] . ',' . $endLatLon[1] . '&key=AIzaSyCCOpUfuBQ8aQZ5o_fFDV0X5JYPrD1T5tg';
               //$image = file_get_contents($gmap);
          //   $image = $this->file_get_contents_curl($gmap);       
    //  $imageName = '/images/campaigns/strava/' . $act->id . '_' . time() . '.png';
      
              


 if($_SERVER['REMOTE_ADDR'] ==='172.68.254.110'){
   //  echo $_SERVER["DOCUMENT_ROOT"] . $image;
  // $path='/images/campaigns/strava/';
  //$destinationPath = public_path('images/campaigns/strava/');     
                //        $imageName =  $act->id . '_' . time() . '.png';
                      // file_put_contents($destinationPath . $imageName,file_get_contents($gmap));
        //      $fp = fopen('images/test/a.png', 'wb');
         //       fputs($fp, $image);
          //      fclose($fp);
          //      echo $gmap;
//die;
//                 $ch = curl_init($gmap);
//$fp = fopen($destinationPath . $imageName, 'wb');
//curl_setopt($ch, CURLOPT_FILE, $fp);
//curl_setopt($ch, CURLOPT_HEADER, 0);
//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//curl_exec($ch);
//curl_close($ch);
//fclose($fp);   die;    
                        
                        
//                        $url = $gmap;
//                        $fp = fopen($_SERVER["DOCUMENT_ROOT"] . $imageName, 'w+');
//                fputs($fp, $image);
//                fclose($fp);
//$img = $destinationPath . $imageName;
//file_put_contents($img, file_get_contents($url));

//
//        $fp = fopen($destinationPath . $imageName, 'w+');
//                fputs($fp, $image);
//                fclose($fp);
           
 //   echo $destinationPath . $imageName;die;
//            $ch = curl_init($image);
//$fp = fopen($destinationPath . $imageName, 'wb');
//curl_setopt($ch, CURLOPT_FILE, $fp);
//curl_setopt($ch, CURLOPT_HEADER, 0);
//curl_exec($ch);
//curl_close($ch);
//fclose($fp);
//$imageName=    $path . $imageName;

}
    else{
       //   $fp = fopen($_SERVER["DOCUMENT_ROOT"] . $imageName, 'w+');
       //         fputs($fp, $image);
          //      fclose($fp);
      $imageName=    $gmap;
    }
    $imageName=    $gmap;
                $fetchGraphCMD = 'curl -G https://www.strava.com/api/v3/activities/' . $act->id . '/streams/altitude -H "Authorization: Bearer ' . $campaign->strava_access_token . '" -d resolution=low';
             //print_r($graphData);
            $graphData = json_decode(exec($fetchGraphCMD, $graphResult));
                  //   if($_SERVER['REMOTE_ADDR']=='172.68.253.169'){
     
   // } 
                $streamData = '';
                if (isset($graphData[1])) {
                    //$lastData = array_slice($graphData[1]->data, (count($graphData[1]->data)-10));
                    $lastData = $graphData[1]->data;
                    foreach ($lastData as $graph) {
                        //$streamArray[] = array("value"=>$graph);
                        $streamArray[] = $graph;
                    }
                    $streamData = json_encode($streamArray);
                }
                if(!is_null($act->start_latlng[0]) && !is_null($endLatLon[0])){
                $data = array(
                    "campaign_id" => $campaign->id,
                    "activity_id" => $act->id,
                    "activity_name" => $act->name,
                    "distance" => $act->distance,
                    "sports" => $act->type,
                    "start_date" => trim(str_replace("Z", " ", str_replace("T", " ", $act->start_date))),
                    "moving_time" => $act->moving_time,
                    "average_speed" => $act->average_speed,
                    "start_lat" => $act->start_latlng[0],
                    "start_long" => $act->start_latlng[1],
                    "end_lat" => $act->end_latlng[0],
                    "end_long" => $act->end_latlng[1],
                    "summary_polyline" => $act->map->summary_polyline,
                    "gmap_img" => $imageName,
                    "graph_data" => $streamData,
                );
                
           
           $stravaObj->create($data);  
   
          //$image=$data=$streamArray='';
           //  unset($image);
                unset($data);
                unset($streamArray); 
                }
            }
          
            $latestActivities = $stravaObj->findByCampaignId($campaign->id);
  
        }
          
 //$latestActivities = $stravaObj->findByCampaignId($id);
        // $latestActivities = $stravaObj->findByCampaignId($campaign->id);
      
        echo view('site.stravaActivity', compact('latestActivities'));
        //return $latestActivities;
    }
function file_get_contents_curl($url) {
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
curl_close($curl);


    return $result;
}
}
