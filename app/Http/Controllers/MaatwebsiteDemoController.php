<?php 

namespace App\Http\Controllers;

//namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Session;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Input;
use Excel;
use Helper;
use PHPExcel; 
use Illuminate\Support\Facades\Storage;
use PHPExcel_IOFactory;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use ZipArchive;
use DB;

class MaatwebsiteDemoController extends Controller
{
	
     public function test(){

        /*
        $row = 1;
        if (($handle = fopen("hi.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                
                 DB::table('users')->where('id', $data[1])->update(['salutation' =>$data[2]]);
                 
                 DB::table('user_profile')->where('id', $data[1])
                 ->update(['first_name' =>$data[3],'sur_name' =>$data[4]]);
            }

            fclose($handle);
        }
        */

    }

    
    public function importExport()
	{
		return view('importExport');
	}
	public function downloadExcel($type, $campaignId = 0, $charityId = 0)
	{
		$user = Session::get('user_session_data');
        $userRole = $user['role'];

        #$campaignId = !empty(trim(Input::get('campaign_id'))) ? trim(Input::get('campaign_id')) : 0;
        #$charityId = !empty(trim(Input::get('charity_id'))) ? trim(Input::get('charity_id')) : 0;

        //Create EloquentDonationRepo Instance
        $donationRepo = new DonationRepo();
        // Get Donations
        if ($userRole == 'User') {
            $donationList = $donationRepo->getAllDonation($user['id'], $campaignId, $charityId, false, $userRole)->get();
        } else {     
            $donationList = $donationRepo->getAllDonation($user['id'], $campaignId, $charityId, false, $userRole);
        }
        
        //dd($donationList);
        // converting std class object to array
        $donationRecieved = [];
		//echo '<pre>'; print_r($donationList); die;
        foreach ($donationList as $donation) {

            $donationRecieved[] = [
                'Donation id' => !empty($donation->donationId) ? $donation->donationId : "",
                'Campaign Name' => !empty($donation->campaign_name) ? $donation->campaign_name : "",
                'Title' => !empty($donation->salutation) ? $donation->salutation : "",
                'First Name' => !empty($donation->first_name) ? $donation->first_name : "",
                'Last Name' => !empty($donation->sur_name) ? $donation->sur_name : "",
                'PostCode' => !empty($donation->postcode) ? $donation->postcode : "",
                'Address' => !empty($donation->address) ? $donation->address : "",
                'Email' => !empty($donation->email) ? $donation->email : "",
                'Town' => !empty($donation->locality) ? $donation->locality : "",
                'Donation Amount' => !empty($donation->donation_amount_monetory) ? Helper::showMoney($donation->donation_amount_monetory) : "",
                'Donation Type' => !empty($donation->title) ? $donation->title : "",
                'Gift Aid' => !empty($donation->is_giftaid) ? "Yes" : "No",
                'Offline Donation' => ($donation->is_offline) ? "Yes" : "No",
                'paid_fees' => !empty($donation->is_fees_paid_by_donor) ? "Yes" : "No",
//              'paid_fees_amount' => !empty($donation->transaction_fee) ? $donation->transaction_fee : 0,
                'Donated On' => !empty($donation->created_at) ? Helper::showDate($donation->created_at) : "",
                'Given/Recieved' => !empty($donation->donorId) && $donation->donorId == $donation->recieverId ? "Self-Donate" : (!empty($donation->donorId) && $donation->donorId == $user['id'] ? "Given" : "Recieved")
                
            ];
        }

        $countColumn = count($donationRecieved) + 2;
        //$data = $donationRecieved->get()->toArray();
		return Excel::create('Donation_list', function($excel)use ($donationRecieved, $countColumn) {
			
            $excel->sheet('Donation_list', function($sheet) use ($donationRecieved, $countColumn)
	        {
				$sheet->fromArray($donationRecieved);
	        });

		})->download($type);
	}
	public function importExcel()
	{
		if(Input::hasFile('import_file')){
			
            $path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = ['title' => $value->title, 'description' => $value->description];
				}
				if(!empty($insert)){
					DB::table('items')->insert($insert);
					dd('Insert Record successfully.');
				}
			}
		}
		return back();
	}


    public function downloadGiftAidExcel($type, $campaignId = 0, $charityId = 0){


        $user = Session::get('user_session_data');
        $userRole = $user['role'];
        //dd($user);
        #$campaignId = !empty(trim(Input::get('campaign_id'))) ? trim(Input::get('campaign_id')) : 0;
        #$charityId = !empty(trim(Input::get('charity_id'))) ? trim(Input::get('charity_id')) : 0;

        //Create EloquentDonationRepo Instance
        $donationRepo = new DonationRepo();
        // Get Donations
        if ($userRole == 'Charity') {
            
        $donationList = $donationRepo->getAllDonation($user['id'], $campaignId, $charityId, false, $userRole);

        } else {     
            
            abort(401); 
        }
        // converting std class object to array
        $donationRecieved = [];
        //echo '<pre>'; print_r($donationList); die();
        
        foreach ($donationList as $donation) {

            if($donation->is_giftaid == 1){

            if($donation->address != ''){

               $address = $this->splitAddress($donation->address);
               
               if(!$address){

                    $address = array('streetName'=> $donation->address, 'houseNumber'=>'', 
                    'additionToAddress1'=>'', 'combined'=>'');

               } else {

                    $address['combined'] = $address['additionToAddress1'].' '.$address['additionToAddress2'];
               }
                 
            
            } else {

                $address = array('streetName'=> '', 'houseNumber'=>'', 
                    'additionToAddress1'=>'', 'combined'=>'');
            }   
            
            $donationRecieved[] = [
                'Campaign Name' => !empty($donation->campaign_name) ? $donation->campaign_name : "",
                'Title' => !empty($donation->salutation) ? $donation->salutation : "",
                'Name' => !empty($donation->name) ? $donation->name : "",
                'PostCode' => !empty($donation->postcode) ? $this->formatUKPostcode($donation->postcode) : "",
                
                'streetName' => !empty($address['streetName']) ? $address['streetName'] : "",
                'houseNumber' => !empty($address['houseNumber']) ? $address['houseNumber'] : "",
                'additionToAddress1' => !empty($address['combined']) ? $address['combined'] : "",
                'Town' => !empty($donation->locality) ? $donation->locality : "",
                
                'Email' => !empty($donation->email) ? $donation->email : "",
                'DonationAmount' => !empty($donation->donation_amount_monetory) ? Helper::showMoney($donation->donation_amount_monetory) : "",
                'Donation Type' => !empty($donation->title) ? $donation->title : "",
                'Gift Aid' => !empty($donation->is_giftaid) ? "Yes" : "No",
                'paid_fees' => !empty($donation->is_fees_paid_by_donor) ? "Yes" : "No",
                'Donated On' => !empty($donation->created_at) ? Helper::showDate($donation->created_at) : "",
                'Given/Recieved' => !empty($donation->donorId) && $donation->donorId == $donation->recieverId ? "Self-Donate" : (!empty($donation->donorId) && $donation->donorId == $user['id'] ? "Given" : "Recieved"),
                'Donation comment' => !empty($donation->content) ? $donation->content : ""
                ];
            }    
        }
        $countColumn = count($donationRecieved) + 2;
        $data = $donationRecieved;
        
        $this->downloadExceltest($data);
        //dd($data);

        // return Excel::create('Donation', function($excel)use ($donationRecieved, $countColumn) {
        //     $excel->sheet('Donation', function($sheet) use ($donationRecieved, $countColumn)
        //     {
        //         $sheet->fromArray($donationRecieved);
        //     });
        // })->download($type);

    }

    //
    public function formatUKPostcode($postcode)
    {   
        $postcode = trim($postcode);

        $postcode = strtoupper(preg_replace("/[^A-Za-z0-9]/", '', $postcode));
     
        if(strlen($postcode) == 5) {
            $postcode = substr($postcode,0,2).' '.substr($postcode,2,3);
        }
        elseif(strlen($postcode) == 6) {
            $postcode = substr($postcode,0,3).' '.substr($postcode,3,3);
        }
        elseif(strlen($postcode) == 7) {
            $postcode = substr($postcode,0,4).' '.substr($postcode,4,3);
        }
     
        return $postcode;
    }

    /**
     * This function splits an address line like for example "Pallaswiesenstr. 45 App 231" into its individual parts.
     * Supported parts are additionToAddress1, streetName, houseNumber and additionToAddress2. AdditionToAddress1
     * and additionToAddress2 contain additional information that is given at the start and the end of the string, respectively.
     * Unit tests for testing the regular expression that this function uses exist over at https://regex101.com/r/vO5fY7/1.
     * More information on this functionality can be found at http://blog.viison.com/post/115849166487/shopware-5-from-a-technical-point-of-view#address-splitting.
     *
     * @param string $address
     * @return array
     */

    //move to helper
    public function splitAddress($address)
    {
        $regex = '
           /\A\s*
           (?: #########################################################################
               # Option A: [<Addition to address 1>] <House number> <Street name>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<A_Addition_to_address_1>.*?),\s*)? # Addition to address 1
           (?:No\.\s*)?
               (?P<A_House_number>\pN+[a-zA-Z]{0,2}(?:\s*[-\/\pP]\s*\pN+[a-zA-Z]?)*) # House number
           \s*,?\s*
               (?P<A_Street_name>(?:[a-zA-Z]\s*|\pN\pL{2,}\s\pL)\S[^,#]*?(?<!\s)) # Street name
           \s*(?:(?:[,\/]|(?=\#))\s*(?!\s*No\.)
               (?P<A_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           |   #########################################################################
               # Option B: [<Addition to address 1>] <Street name> <House number>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<B_Addition_to_address_1>.*?),\s*(?=.*[,\/]))? # Addition to address 1
               (?!\s*No\.)(?P<B_Street_name>[^0-9# ]\s*\S(?:[^,#](?!\b\pN+\s))*?(?<!\s)) # Street name
           \s*[\/,]?\s*(?:\sNo[.:])?\s*
               (?P<B_House_number>\pN+\s*-?[a-zA-Z]{0,2}(?:\s*[-\/\pP]?\s*\pN+(?:\s*[\-a-zA-Z])?)*|
               [IVXLCDM]+(?!.*\b\pN+\b))(?<!\s) # House number
           \s*(?:(?:[,\/]|(?=\#)|\s)\s*(?!\s*No\.)\s*
               (?P<B_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           )
           \s*\Z/xu';

        $result = preg_match($regex, $address, $matches);
        
        if ($result === 0) {
            
            return  false;

        } elseif ($result === false) {

           return  false;
        }

        if (!empty($matches['A_Street_name'])) {
            return array(
                'additionToAddress1' => $matches['A_Addition_to_address_1'],
                'streetName' => $matches['A_Street_name'],
                'houseNumber' => $matches['A_House_number'],
                'additionToAddress2' => (isset($matches['A_Addition_to_address_2'])) ? $matches['A_Addition_to_address_2'] : ''
            );
        } else {
            return array(
                'additionToAddress1' => $matches['B_Addition_to_address_1'],
                'streetName' => $matches['B_Street_name'],
                'houseNumber' => $matches['B_House_number'],
                'additionToAddress2' => isset($matches['B_Addition_to_address_2']) ? $matches['B_Addition_to_address_2'] : ''
            );
        }
    }

    public function downloadExceltest($data){
        
        $path = realpath(__DIR__ . '/../../../ods');
        $user = Session::get('user_session_data');
        
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';

        $i = 0;    
        
        foreach($data as $row){
            
            $nameSplit = explode(' ',$row['Name']);
            
            $valid = $this->postcode_check($row['PostCode']);

            if(!empty($nameSplit[0]) && !empty($nameSplit[1]) && !empty($row['houseNumber']) && $valid ===TRUE && !empty($row['Title'])){

                $cleaner[] = array(

                       'Title'=> $row['Title'],
                       'Fname'=> $nameSplit[0],
                       'Lname'=> $nameSplit[1],
                       'houseNumber'=> $row['houseNumber'],
                       'PostCode' => $row['PostCode'],
                       'date' => date('d/m/y',strtotime($row['Donated On'])),
                       'DonationAmount'=>$row['DonationAmount']
                        
                );  
               
            }
                     
        }


        
        $objTpl = PHPExcel_IOFactory::load("templates/template1.xls");
        $objTpl->setActiveSheetIndex(0);  //set first sheet as active


        $chunks = array_chunk($cleaner, 1000);
        $count = count($chunks);
        $j = 25;
        // echo '<pre>';
        // print_r($chunks);
        // echo '</pre>';
        foreach($chunks[0] as $chunk) {
              if($j <= 1024 ){
                              
                    $objTpl->getActiveSheet()->setCellValue('C'.$j, $chunk['Title']);
                    $objTpl->getActiveSheet()->setCellValue('D'.$j, $chunk['Fname']);
                    $objTpl->getActiveSheet()->setCellValue('E'.$j, $chunk['Lname']);
                    $objTpl->getActiveSheet()->setCellValue('F'.$j, $chunk['houseNumber']);
                    $objTpl->getActiveSheet()->setCellValue('G'.$j, $chunk['PostCode']);
                    $objTpl->getActiveSheet()->setCellValue('H'.$j, '');
                    $objTpl->getActiveSheet()->setCellValue('I'.$j, '');
                    $objTpl->getActiveSheet()->setCellValue('J'.$j, $chunk['date']);
                    $objTpl->getActiveSheet()->setCellValue('K'.$j, $chunk['DonationAmount']);
              } 
             $j++; 
        }

        $fileName = $path.'/Gift_Aid_Schedule'.$i.'.ods';
        $objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5'); 
        $objWriter->save($fileName);
        $i++;

                               
        // /*Zip the file */
        $zip = new ZipArchive;
        $rand = rand();
        $zipname = $path.'/Gift_Aid_Schedule_'.$rand.'.zip';
        $zip->open($zipname,ZipArchive::CREATE);
        
        for($i = 0; $i<$count; $i++ ){

            $zip->addFile($path.'/Gift_Aid_Schedule'.$i.'.ods','Gift_Aid_Schedule'.$i.'.ods');
        }

        $zip->close();
        
        for($i = 0; $i<$count; $i++ ){

            unlink($path.'/Gift_Aid_Schedule'.$i.'.ods');
        }

        $file_url = $path.'/Gift_Aid_Schedule_'.$rand.'.zip';
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=Gift_Aid_Schedule_".$rand.".zip");
        ob_clean();
        flush(); 
        readfile($file_url); 
    }

    function postcode_check(&$toCheck) {
      
      // Permitted letters depend upon their position in the postcode.
      $alpha1 = "[abcdefghijklmnoprstuwyz]";                          // Character 1
      $alpha2 = "[abcdefghklmnopqrstuvwxy]";                          // Character 2
      $alpha3 = "[abcdefghjkstuw]";                                   // Character 3
      $alpha4 = "[abehmnprvwxy]";                                     // Character 4
      $alpha5 = "[abdefghjlnpqrstuwxyz]";                             // Character 5
      
      // Expression for postcodes: AN NAA, ANN NAA, AAN NAA, and AANN NAA with a space
      // Or AN, ANN, AAN, AANN with no whitespace
      $pcexp[0] = '^(' . $alpha1 . '{1}' . $alpha2 . '{0,1}[0-9]{1,2})([[:space:]]{0,})([0-9]{1}' . $alpha5 . '{2})?$';
     
      // Expression for postcodes: ANA NAA
      // Or ANA with no whitespace
      $pcexp[1] = '^(' . $alpha1 . '{1}[0-9]{1}' . $alpha3 . '{1})([[:space:]]{0,})([0-9]{1}' . $alpha5 . '{2})?$';
     
      // Expression for postcodes: AANA NAA
      // Or AANA With no whitespace
      $pcexp[2] = '^(' . $alpha1 . '{1}' . $alpha2 . '[0-9]{1}' . $alpha4 . ')([[:space:]]{0,})([0-9]{1}' . $alpha5 . '{2})?$';
     
      // Exception for the special postcode GIR 0AA
      // Or just GIR
      $pcexp[3] = '^(gir)([[:space:]]{0,})?(0aa)?$';
     
      // Standard BFPO numbers
      $pcexp[4] = '^(bfpo)([[:space:]]{0,})([0-9]{1,4})$';
     
      // c/o BFPO numbers
      $pcexp[5] = '^(bfpo)([[:space:]]{0,})(c\/o([[:space:]]{0,})[0-9]{1,3})$';
      
      // Overseas Territories
      $pcexp[6] = '^([a-z]{4})([[:space:]]{0,})(1zz)$';
     
      // Anquilla
      $pcexp[7] = '^(ai\-2640)$';
      
      // Load up the string to check, converting into lowercase
      $postcode = strtolower($toCheck);
     
      // Assume we are not going to find a valid postcode
      $valid = false;
     
      // Check the string against the six types of postcodes
      foreach ($pcexp as $regexp) {
        if (preg_match('/' . $regexp . '/i', $postcode, $matches)) {
     
          // Load new postcode back into the form element
          $postcode = strtoupper($matches[1]);
          if (isset($matches[3])) {
            $postcode .= ' ' . strtoupper($matches[3]);
          }
     
          // Take account of the special BFPO c/o format
          $postcode = preg_replace('/C\/O/', 'c/o ', $postcode);
          
          // Remember that we have found that the code is valid and break from loop
          $valid = true;
          break;
        }
      }
     
      // Return with the reformatted valid postcode in uppercase if the postcode was 
      // valid
      if ($valid) {
        $toCheck = $postcode;
        return true;
      } else {
        return false;
      }
    }
}

?>