<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Toastr;
use Redirect;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index(){
    	return view('site.contact');
    }

    public function save(Request $request){
    	//$data = Input::all();
    	$this->validate($request, [
	        'first_name' => 'required',
	        'email' => 'required|email',
	        'contact_message' => 'required'
    	]);

    	$contact = new Contact();
    	$contact->first_name = $request->first_name;
    	$contact->sur_name = $request->sur_name;
    	$contact->contact_message = $request->contact_message;
    	$contact->email = $request->email;
        $contact->save();

        Mail::send('auth::email.contactuser', [], function ($m) use ($request) {
            $m->from('info@givebrite.com', 'GiveBrite');

            $m->to($request->email, $request->first_name)->subject('New Query/Contact');
        });
        Mail::send('auth::email.contactadmin', ['contact' => $contact], function ($m) use ($request) {
            $m->from('info@givebrite.com', 'GiveBrite');

            $m->to('info@givebrite.com', 'GiveBrite')->subject('New Query Received');
            //$m->to('megha.saroop@mail.vinove.com', 'GiveBrite')->subject('New Query Received');
        });

        // Mail::send('auth::email.charity', [], function ($m) use ($request) {
        //     $m->from($request->email, 'GiveBrite');

        //     $m->to('mansoor@justloyalty.co.uk', $request->first_name)->subject('New Query/Contact');
        // });        
        
    	Toastr::success("Message Sent Successfully", $title = null, $options = []);
       	return Redirect::back();

    }
}
