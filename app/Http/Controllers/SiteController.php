<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Admingivebrite\Repositories\EloquentBannerRepository as BannerRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as DonationRepo;
use Modules\Admingivebrite\Repositories\ContentRepository as ContentRepo;
use View;
use Config;
use Input;

class SiteController extends Controller {

    protected $categories,
            $campaigns,
            $campaignDesign,
            $user,
            $content;

    function __construct(CampaignRepo $campaigns, UserProfileRepo $profiles, ContentRepo $content, DonationRepo $donations) {
        $this->campaigns = $campaigns;
        $this->user = $profiles;
        $this->content = $content;
        $this->donations = $donations;
    }

    public function test(){

        dd(1);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
// Get Active Banners
        
        $bannersRepo = new BannerRepo();
        $activeBanners = $bannersRepo->findActiveBanners();
        $categoriesArray = $this->campaigns->getCampaignCategoriesWithSlug();
        $campaignsList = $this->campaigns->getCampaignWithJoin(0)->paginate(Config('config.dashboard_home_pagination'));

        return view('site.index', compact('categoriesArray', 'campaignsList', 'activeBanners'));
    }

    public function new_index(Request $request) {
        
        // Get Active Banners
        $bannersRepo = new BannerRepo();
        $activeBanners = $bannersRepo->findActiveBanners();
        $categoriesArray = $this->campaigns->getCampaignCategoriesWithSlug();
        $campaignsList = $this->campaigns->getCampaignWithJoin(0)->paginate(Config('config.dashboard_home_pagination'));
        $almostFunded = [];
        $nearlyExpired = $this->campaigns->getCampaignWithJoinNearlyExpired(0)->paginate(Config('config.dashboard_home_pagination'));
        foreach ($campaignsList as $campaign) {
            if ($campaign->totalAmountRaised / $campaign->goal_amount_monetary > 0.8) {
                array_push($almostFunded, $campaign);
            }
        }
        
        return view('site.new_index', compact('categoriesArray', 'campaignsList', 'activeBanners', 'almostFunded', 'nearlyExpired'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about() {
        $data = null;
        try {
            $url = Config::get('config.menus_routes_url')['about'];
            $data = $this->content->findByUrl($url);

            $url = Config::get('config.menus_routes_url')['why-givebrite'];
            $features = $this->content->findByUrl($url);

            $url = Config::get('config.menus_routes_url')['how-we-compare'];
            $how_we_compare = $this->content->findByUrl($url);
        } catch (Exception $ex) {
            return $this->redirectNotFound();
        }

        return view('site.about', compact('data', 'features', 'how_we_compare'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verifySlug($slug) {
        $data = null;
        try {
            $data = $this->content->findByUrl($slug);
        } catch (Exception $ex) {
            return $this->redirectNotFound();
        }
        return view('site.how_it_works', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function page405() {
        return view('site.405');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function page406() {
        return view('site.406');
    }

    /**
     * function for 404 response
     * @return \Illuminate\Http\Response
     */
    public function page404() {
        return view('site.404');
    }

    /**
     * Function for Donar Profile display
     * @param type $slug
     * @return type
     */
    public function getDonarProfile($slug = "") {
        if (!empty($slug)) {
            $userProfile = $this->user->findBySlug($slug);
            if ($userProfile) {
                $campaignList = [];
                $fundList = [];

                $total_donated_amount = $this->donations->findByDonarOnActiveCampaign($userProfile->user_id);

                $donated_amount_sum = 0;

                if ($total_donated_amount) {
                    foreach ($total_donated_amount as $amount) {
                        $donated_amount_sum = $donated_amount_sum + $amount->donation_amount_monetory;
                    }
                }

                return view('site.donarprofile', compact('campaignList', 'fundList', 'userProfile', 'donated_amount_sum'));
            }
        } else {
            $this->page405();
        }
    }

    /**
     * Ajax pagination data for donors funded campaigns
     * @param type $slug
     */
    public function getDonarFundedCampaign($slug) {
        if (!empty($slug)) {
            $userProfile = $this->user->findBySlug($slug);
            if ($userProfile) {
                $donar_id = $userProfile->user_id;
                $base_path_campaign = Config::get('config.thumb_campaign_upload_path');
                $fundList = $this->campaigns->findByDonar($donar_id, Config::get('config.donor_fund_pagination'));

                $campaignIds = array();

                foreach ($fundList as $fund) {
                    $campaignIds[] = $fund->id;
                }

                $amount_raised = $this->donations->findTotalAmountRaisedByMultipleCampaignIds($campaignIds);

                for ($i = 0; $i < count($amount_raised); $i++) {
                    $fundList[$i]->totalAmountRaised = $amount_raised[$i]->totalAmountRaised;
                }

                $page = Input::get('page');
                $responseView = view('site.partials.donors.funded_div_append', compact('fundList', 'base_path_campaign', 'page'))->render();

                $paginationData = $fundList;

                $paginationView = view('site.partials.paginationdiv', compact('paginationData'))->render();

                echo json_encode(['responseCode' => 200, 'responseView' => $responseView, 'paginationView' => $paginationView]);
            }
        } else {
            echo json_encode(['responseCode' => 0]);
            die;
        }
    }

    /**
     * Ajax pagination data for donors raised campaigns
     * @param type $slug
     */
    public function getDonarRaisedCampaign($slug) {
        if (!empty($slug)) {
            $userProfile = $this->user->findBySlug($slug);
            if ($userProfile) {
                $donar_id = $userProfile->user_id;
                $campaignResponse = $this->campaigns->getCampaignWithJoin($donar_id);
                $base_path_campaign = Config::get('config.thumb_campaign_upload_path');
                $campaignList = $campaignResponse->paginate(Config::get('config.donor_raise_pagination'));
                $page = Input::get('page');
                $responseView = view('site.partials.donors.raised_div_append', compact('campaignList', 'base_path_campaign', 'page'))->render();

                $paginationData = $campaignList;

                $paginationView = view('site.partials.paginationdiv', compact('paginationData'))->render();
                echo json_encode(['responseCode' => 200, 'responseView' => $responseView, 'paginationView' => $paginationView]);
            }
        } else {
            echo json_encode(['responseCode' => 0]);
            die;
        }
    }

}
