<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Admingivebrite\Repositories\ContentRepository as ContentRepo;
use View;
use Config;
use Input;

class SiteFooterController extends Controller {

    protected $content;

    public function __construct(ContentRepo $content) {
        $this->repository = $content;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifySlug($slug) {
        
        try {
            $content = $this->repository->findByUrl($slug);

            if (isset($content) && is_object($content) && !empty($content)) {
                return view('site.static_pages.fundraise_idea', compact('title', 'content'));
            }

            return $this->redirectNotFound();
            
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

}
