<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Auth\Repositories\Users\EloquentUserRepository as UserRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Auth\Repositories\Users\EloquentFollowerRepository as FollowerRepo;
use Modules\Payment\Repositories\EloquentCommentRepository as CommentRepo;
use Modules\Auth\Repositories\Users\EloquentUserActivationRepository as UserActivationRepo;
use Modules\Campaigns\Entities\Campaign;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Auth;
use Toastr;
use Redirect;
use Config;

class CharityController extends Controller {

    protected $user,
            $userProfile,
            $campaign,
            $follower,
            $userActivation,
            $comment;

    function __construct(UserRepo $user, UserProfileRepo $userProfile, CampaignRepo $campaign, FollowerRepo $follower, UserActivationRepo $userActivation, CommentRepo $comment) {
        $this->user = $user;
        $this->userProfile = $userProfile;
        $this->campaign = $campaign;
        $this->follower = $follower;
        $this->userActivation = $userActivation;
        $this->comment = $comment;
    }

    /**
     * Search Charity
     * @return type
     */
    public function index() {
        $keyword = Input::get('keyword');
        $charityCategory = Input::get('type');

        // Set Filters And Sorting
        $filters = [
            'search_keyword' => $keyword
        ];
        $sort = [];
        $id = 3;
        $paginate_value = 3;
        //Create Campaign Repo Instance
        $charityListQuery = $this->userProfile->findByCharitySearch($id, $filters, $sort);
        $charityList = $charityListQuery->paginate($paginate_value)->appends(Input::except('page'));

        $campaignResponse = $this->campaign->getCampaignWithJoin();
        $campaignList = $campaignResponse->paginate(Config('config.dashboard_home_pagination'));

        $categoriesArray = $this->campaign->getCampaignCategoriesWithSlug();

        return view('site.charity.list', compact('charityList', 'campaignList', 'categoriesArray'));
    }

    /**
     * function for get charity list
     * @return type
     */
    public function getCharityList() {
        $campaignResponse = $this->campaign->getFundRaiserForCharity();
        $campaignList = $campaignResponse->paginate(Config('config.dashboard_home_pagination'));
        $categoriesArray = $this->campaign->getCampaignCategoriesWithSlug();
        $charityList = [];

        return view('site.charity.list', compact('charityList', 'campaignList', 'categoriesArray'));
    }

    /**
     * function for get detail of charity
     * @param type $slug
     * @return type
     */
    public function getCharities() {
        //find from user profile by CHARITY role
        $keyword = Input::get('keyword');

        // Set Filters And Sorting
        $filters = [
            'search_keyword' => trim($keyword)
        ];
        $charityList = $this->userProfile->findByRole(Config::get('config.role_charity'), Config::get('config.charity_list_pagination'), $filters);
        $paginationData = $charityList;
        $base_path_charity = Config::get('config.thumb_charity_logo_upload_path');
        $responseView = view('site.partials.charity-div-append', compact('charityList', 'base_path_charity'))->render();
        $paginationView = view('site.partials.paginationdiv', compact('paginationData'))->render();
        echo json_encode(['responseCode' => 200, 'responseView' => $responseView, 'paginationView' => $paginationView]);
        die;
    }

    /*
     * Function to Display Charity Detail Page
     * @param type $slug
     * @return type
     */

    public function getCharityDetail($slug) {

        $charityDetail = $this->userProfile->findBySlug($slug);

        $user_activation_info = $this->userActivation->findByUserId($charityDetail->user_id);

        if ($user_activation_info->is_activated == 1) {

            //create user object and find email of charity
            $userDetail = $this->user->findById($charityDetail->user_id);
            $email = ($userDetail) ? $userDetail->email : "";

            $charity_id = $charityDetail->user_id;
            //die($charity_id);
            $is_followed = 0;

            //check if user is logged in and get follow/unfollow info of charity by the logged in user
            if (Auth::check()) {
                $user_info = array(
                    'user_id' => $charity_id,
                    'follower_id' => Auth::user()->id
                );

                $follow_response = $this->follower->find($user_info);
                $is_followed = ($follow_response) ? $follow_response->is_followed : 0;
            }


            $charityCampaign = [];

            //create campaign object and find two campaigns created by the charity which has raised maximum amount
            $charityCampaigns = $this->campaign->findByCharityLimit($charity_id);
            
            //$charityCampaigns = $charityCampaignsAll->paginate(Config('config.charity_detail_campaign_pagination'));
            
            $charityComments = $this->comment->findByCharity($charity_id, 25);
            //mitg
            //$sort=[];
            // $campaignListQuery = $this->campaign->getCampaignListByCharity($charity_id);
            // $campaigns=$campaignListQuery->paginate(Config('config.campaign_list_pagination'))->appends(Input::except('page'));
            //
            //dd($charityCampaign);
            //return campaigns on view

            return view('site.charity.detail', compact('charityDetail', 'email', 'charityCampaigns', 'charityComments', 'is_followed'));
        }
        Toastr::Error("Charity is not activated yet", $title = null, $options = []);
        return Redirect::back();
    }

    /**
     * function for follow and unfollow user using ajax
     */
    public function follow() {
        //check if ajax request and logged in user
        if (Request::ajax()) {
            if (Auth::check()) {
                $user_info = array(
                    'user_id' => Input::get('user_id'),
                    'follower_id' => Auth::user()->id,
                    'is_followed' => Input::get('is_followed')
                );

                //create follow object and find if entry already exists
                $follow_response = $this->follower->find($user_info);

                if ($follow_response) {
                    //update if exist
                    $this->follower->update($user_info);
                } else {
                    //create if not
                    $this->follower->create($user_info);
                }
            }
        }
    }

}
