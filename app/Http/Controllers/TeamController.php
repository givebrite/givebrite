<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Modules\Campaigns\Entities\Team as TeamModel;

use Modules\Campaigns\Repositories\EloquentTeamRepository as TeamRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as CampaignDonationRepo;
use Modules\Campaigns\Repositories\EloquentCampaignUpdateRepository as CampaignUpdateRepo;
use App\CampaignDonor;
use Session;
use Helper;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function cropLogo(Request $request){

            //dd($request->all());
        $x = $request->input('x');
        $y = $request->input('y');
        $w = $request->input('w');
        $h = $request->input('h');

        $base64_string = $request->input('imgPathTeam');
        Helper::CropImageHelper($x, $y, $w, $h, $base64_string);
        $sessionData = array('msg' => 'success', 'imageName' => Session::get('image'));
        echo json_encode($sessionData);


    }

    public function crop(Request $request)
    {   
        
        //dd($request->all());
        $x = $request->input('x');
        $y = $request->input('y');
        $w = $request->input('w');
        $h = $request->input('h');

        $base64_string = $request->input('imgpath');
        Helper::CropImageHelper($x, $y, $w, $h, $base64_string);
        $sessionData = array('msg' => 'success', 'imageName' => Session::get('image'));
        echo json_encode($sessionData);

        //return redirect()->back();
    }

    
    /**
     * Display the specified team.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($slug)
    {
        
        // Get User Details From Session
        $user = Session::get('user_session_data');
        $userId = !empty($user['id']) ? $user['id'] : 0;
        
        $teamRepo = new TeamRepo();
        $teamDetails = $teamRepo->findBySlug($slug);
        if(!$teamDetails){

             return redirect('dashboard');
        }

        $teamMembers = $teamRepo->findTeamMembers($teamDetails->team_id);
        
        //teamUpdates 
        $teamUpdates = $teamRepo->findTeamUpdates($teamDetails->team_id);

        //dd($teamMembers);
        $campaignDetail = [];
        // Creating Campaign Repository Instance
        $campaignRepo = new CampaignRepo();
        $campaignDetails = $campaignRepo->findBySlug($teamDetails->slug);
     

        // Count of Updates
        // Creating Instance For Campaign Donation Repo\
        $campaignDonationRepo = new CampaignDonationRepo();

        $list_of_donors = $campaignDonationRepo->latest3Donation($campaignDetails[0]->campaignId);

        if (!empty($campaignDetails)) {
            $campaign = $campaignDetails[0];
        } else {
            $campaign = [];
        }

        $donation_type = new CampaignDonor();
        $donation = $donation_type->getAllWithJoin($campaign->campaignId);
        $campaignUpdateRepo = new CampaignUpdateRepo();
        $count = $campaignUpdateRepo->getCountOneCampaign($campaign->campaignId);

//        $campaignUpdates = $campaignUpdateRepo->getUpdatesByCampaignId($campaign->campaignId)->paginate(  Config('config.detail_backers_pagination'));

        $campaignUpdates = $campaignUpdateRepo->getUpdatesByCampaignId($campaign->campaignId)->paginate(Config('config.detail_backers_pagination'));
        $totalDonorQuery = $campaignDonationRepo->findTotalDonorsCountByCampaignId($campaign->campaignId);
        $totalDonors = !empty($totalDonorQuery) ? $totalDonorQuery : 0;
        $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaign->campaignId);
        $totalAmountRaised = !empty($totalAmountRaisedQuery[0]->totalAmountRaised) ? $totalAmountRaisedQuery[0]->totalAmountRaised : 0;


        if (!empty($campaign)) {
            return view('site.team_details', compact('totalDonors', 'latestActivities', 'totalAmountRaised', 'user', 'campaign', 'campaignDetails', 'userId', 'count', 'list_of_donors', 'donation', 'campaignUpdates','teamDetails','teamMembers','teamUpdates'));

        } else {

            return $this->redirectNotFound();
        }

     

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
