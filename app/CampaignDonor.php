<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class CampaignDonor extends Model {

    protected $table = 'campaign_donor';

    public function getAllWithJoin($campaignId) {
        $post = DB::table('campaign_donor')
                        ->join('donation_type as type', 'type.id', '=', 'campaign_donor.donor_type_id')
                        ->where('campaign_id', $campaignId)->get();
        return $post;
    }

}
