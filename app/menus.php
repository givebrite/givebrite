<?php

//$leftMenu = Menu::instance('admin-menu');

$rightMenu = Menu::instance('admin-menu-right');

/**
 * @see https://github.com/pingpong-labs/menus
 * 
 * @example adding additional menu.
 *
 * $leftMenu->url('your-url', 'The Title');
 * 
 * $leftMenu->route('your-route', 'The Title');
 */
//$leftMenu->url('your-url', 'The Title',2);


Menu::create('admin-menu', function($menu)
{
        $menu->enableOrdering();
        $menu->setPresenter('Pingpong\Admin\Presenters\SidebarMenuPresenter');
        $menu->url('admin',"Dashboard", 1, ['icon' => 'fa fa-dashboard']);
        $menu->url('admin/users',"Individual Fundraiser", 2, ['icon' => 'fa fa-users']);
        $menu->url('admin/charity-users',"Charity Users", 2, ['icon' => 'fa fa-users']);
        $menu->dropdown('Content Pages', function ($sub) {
        $sub->url('admingivebrite/content', 'Articles');
        }, 3, ['icon' => 'fa fa-shield']);
        $menu->url('admingivebrite/banners',"Banners", 3, ['icon' => 'fa fa-flag']);

        $menu->dropdown('Menus', function ($sub) {
            $sub->url('admingivebrite/menu-categories', 'Menu Categories');
            $sub->url('admingivebrite/menus', 'Menus');
        }, 4, ['icon' => 'fa fa-shield']);
        
        $menu->dropdown('Campaign Controls', function ($sub) {
        $sub->url('admingivebrite/campaign-categories', 'Categories');
        $sub->url('admingivebrite/campaign-locations', 'Locations');
        $sub->url('admingivebrite/campaign-themes', 'Campaigns Themes');
        }, 5, ['icon' => 'fa fa-cogs']);
               
        $menu->url('admingivebrite/campaign-list',"All Campaigns",  6 , ['icon' => 'fa fa-shield']);        
        $menu->url('admingivebrite/campaign-donations',"All Donations",  7, ['icon' => 'fa fa-heart-o']);



});

