<?php

namespace App\Helpers;

use App;
use Auth;
use Config;
use Facebook;
use File;
use Image;
use Input;
use Modules\Admingivebrite\Repositories\EloquentCampaignLocationRepository as CampaignLocationRepo;
use Modules\Auth\Http\Controllers\LoginController;
use Modules\Auth\Repositories\Users\EloquentRoleUserRepository as RoleUserRepo;
use Modules\Auth\Repositories\Users\EloquentUserActivationRepository as UserActivationRepo;
use Modules\Auth\Repositories\Users\EloquentUserProfileRepository as UserProfileRepo;
use Modules\Campaigns\Entities\CampaignPayment as CampaignPaymentModel;
use Modules\Campaigns\Repositories\EloquentCampaignLaunchRepository as CampaignPaymentRepo;
use Modules\Campaigns\Repositories\EloquentCampaignRepository as CampaignRepo;
use Modules\Dashboard\Repositories\EloquentDonationRepository as CampaignDonationRepo;
use Session;
use Imagick;

class BasicHelper
{

    protected static $dbDateFormat = 'Y-m-d H:i:s';
    protected static $userDateFormat = 'jS F Y';
    protected static $userDateTimeFormat = 'm/d/Y H:i:s';


    public static function generateRandomString($length = 30) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
    
    public static function splitAddress($address){

        $regex = '
           /\A\s*
           (?: #########################################################################
               # Option A: [<Addition to address 1>] <House number> <Street name>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<A_Addition_to_address_1>.*?),\s*)? # Addition to address 1
           (?:No\.\s*)?
               (?P<A_House_number>\pN+[a-zA-Z]{0,2}(?:\s*[-\/\pP]\s*\pN+[a-zA-Z]?)*) # House number
           \s*,?\s*
               (?P<A_Street_name>(?:[a-zA-Z]\s*|\pN\pL{2,}\s\pL)\S[^,#]*?(?<!\s)) # Street name
           \s*(?:(?:[,\/]|(?=\#))\s*(?!\s*No\.)
               (?P<A_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           |   #########################################################################
               # Option B: [<Addition to address 1>] <Street name> <House number>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<B_Addition_to_address_1>.*?),\s*(?=.*[,\/]))? # Addition to address 1
               (?!\s*No\.)(?P<B_Street_name>[^0-9# ]\s*\S(?:[^,#](?!\b\pN+\s))*?(?<!\s)) # Street name
           \s*[\/,]?\s*(?:\sNo[.:])?\s*
               (?P<B_House_number>\pN+\s*-?[a-zA-Z]{0,2}(?:\s*[-\/\pP]?\s*\pN+(?:\s*[\-a-zA-Z])?)*|
               [IVXLCDM]+(?!.*\b\pN+\b))(?<!\s) # House number
           \s*(?:(?:[,\/]|(?=\#)|\s)\s*(?!\s*No\.)\s*
               (?P<B_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           )
           \s*\Z/xu';

        $result = preg_match($regex, $address, $matches);
        
        if ($result === 0) {
            
            return  false;

        } elseif ($result === false) {

           return  false;
        }

        if (!empty($matches['A_Street_name'])) {
            return array(
                'additionToAddress1' => $matches['A_Addition_to_address_1'],
                'streetName' => $matches['A_Street_name'],
                'houseNumber' => $matches['A_House_number'],
                'additionToAddress2' => (isset($matches['A_Addition_to_address_2'])) ? $matches['A_Addition_to_address_2'] : ''
            );
        } else {
            return array(
                'additionToAddress1' => $matches['B_Addition_to_address_1'],
                'streetName' => $matches['B_Street_name'],
                'houseNumber' => $matches['B_House_number'],
                'additionToAddress2' => isset($matches['B_Addition_to_address_2']) ? $matches['B_Addition_to_address_2'] : ''
            );
        }
    }
    /**
     * This function will be used to set Date format.
     *
     * @param date $date
     * @return date $date
     * @author Ak 18 May 2016
     */
    public static function showDate($date = '0')
    {
        //changing date format and return 0 incase og wrong date
        if (strtotime($date) && $date != 0) {
            $date = date(self::$userDateFormat, strtotime($date));
        } else {
            $date = 'N/A';
        }
        return $date;
    }

    /**
     * This function will be used to set Date & Time format.
     *
     * @param date $date
     * @return date $date
     * @author Ak 18 May 2016
     */
    public static function showDateTime($date)
    {
        //changing date format and return 0 incase og wrong date
        if (strtotime($date)) {
            $date = date(self::$userDateTimeFormat, strtotime($date));
        } else {
            $date = 0;
        }

        return $date;
    }

    /**
     * Function To Check Type Of Campaign
     */
    public static function checkCampaignType($campaignId)
    {
        $campaignRepo = new CampaignRepo();
        $campaign = $campaignRepo->findById($campaignId);
        if (!empty($campaign->charity_id)) {
            return "Charity";
        } else {
            // Check User Role
            $userRepo = new RoleUserRepo();
            $userRole = $userRepo->findByUserId($campaign->user_id);
            if ($userRole->role_id == 3) {
                return "Charity";
            } else {
                return "Individual";
            }
        }
    }

    /**
     * This Function is used to show money format.
     * @author Ak 18 May 2016
     */
    public static function showMoney($money, $currency = "sterling", $decimal = 0)
    {
        $currency = Config::get('currency.' . $currency);
        return number_format($money, $decimal);
    }

    /**
     * This Function is used to show dp of currently active user.
     */
    public static function showUserDp()
    {
        // Get Current User Detail From Session
        $user = Session::get('user_session_data');
        $base_path = $user['role'] == 'Charity' ? Config::get('config.thumb_charity_logo_upload_path') : Config::get('config.thumb_profile_image_upload_path');

        if ($user['role'] == 'Charity') {
            $imagePath = isset($user['charity_logo']) && !empty($user['charity_logo']) && File::exists($base_path . $user['charity_logo']) ? asset($base_path . $user['charity_logo']) : asset('images/userDP.png');
        } else {
            $imagePath = isset($user['profile_pic']) && !empty($user['profile_pic']) ? ((substr($user['profile_pic'], 0, 4) === "http") ? $user['profile_pic'] : ((File::exists($base_path . $user['profile_pic'])) ? asset($base_path . $user['profile_pic']) : asset('images/userDP.png'))) : asset('images/userDP.png');
        }
        return $imagePath;
    }

    /**
     * This Function is used to show image.
     */
    public static function showImage($type, $imageName, $imageSize = 'thumb', $defaultImage = '')
    {
//        echo $imageName;die;
        $basePathArray = [
            'campaign' => Config::get('config.thumb_campaign_upload_path'),
        ];
        $defaultImageArray = [
            'campaign' => 'images/default-images/blank.png',
        ];

        if (!empty($imageName) && !empty($basePathArray[$type]) && File::exists($basePathArray[$type] . $imageName)) {
            // Return Thumb Or large Image
            return $basePathArray[$type] . $imageName;
        } else {

            // Return Default Image
            if (!empty($defaultImageArray[$type])) {
                return $defaultImageArray[$type];
            } elseif (!empty($defaultImage)) {
                return 'images/default-images/' . $defaultImage;
            } else {
                return 'images/default-images/blank.png';
            }
        }
    }

    /*
             * Function to show city and location
             * @param $city
             * @param $loaction
             * @maxLength 10
    */

    public static function showAddress($city, $location, $maxLength = "10")
    {
        $address = "";
        if (!empty($city) && !empty($location)) {
            $address = ucfirst($city) . ", " . ucfirst($location);
            $address = str_limit($address, $limit = $maxLength, $end = '...');
        } else {
            $address = "NA";
        }
        return $address;
    }

    /**
     * Function To get Elapsed time
     */
    public static function getElapsedTime($datetime)
    {
        return \Carbon\Carbon::createFromTimeStamp(strtotime($datetime))->diffForHumans();
    }

    /**
     * Function to get query string
     *
     * @return string
     */
    public static function getQueryString()
    {
        // Temp array
        $arr = array();
        // Array to ignore the parameters from the query string
        $ignore = array('page', 'sort', 'dir');

        // Get the desired array
        foreach (Input::all() as $key => $value) {
            if (!in_array($key, $ignore)) {
                $arr[] = $key . "=" . $value;
            }
        }

        // If the query string is present if not present
        if (count($arr) > 0) {
            return implode("&", $arr);
        } else {
            return "";
        }
    }

    public static function isCharityVarified($userId)
    {
        $activationRepo = new UserActivationRepo();
        $checkCharity = $activationRepo->findByUserId($userId);
        if (!empty($checkCharity) && $checkCharity->is_activated == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function getFriendsCampaigns($email, $media = "facebook")
    {
        // Directly from the IoC
        $fb = App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
        $user = Session::get('social_user');
        $userAccessToken = !empty($user->token) ? $user->token : "";
        $friendAuthIds = [];
        // Facebook Api Request
        try {
            // Facebook Api To fetch friend list of app user
            $response = $fb->get('/me/friends', $userAccessToken);
            // Decode Response
            $response = $response->getDecodedBody();
            if (!empty($response['data']) && count($response['data']) > 0) {
                $friendList = $response['data'];
                foreach ($friendList as $friend) {
                    $friendAuthIds[] = $friend['id'];
                }
                return $fbCampaigns = self::getFacebookCampaignList($friendAuthIds);
            }
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Get Campaign listing For My Facebook Friends
     *
     */
    public static function getFacebookCampaignList($friendAuthIds)
    {
        $campaignRepo = new CampaignRepo();
        return $fbCampaignList = $campaignRepo->getFbCampaigns($friendAuthIds);
    }

    /**
     * Check if user is activated
     * @return boolean
     */
    public static function checkIsActivated()
    {
        if (Auth::check()) {
            $userId = Auth::user()->id;

            //creates UserActivation model and find all entries from the user_activation table
            $activationRepo = new UserActivationRepo();
            $checkUser = $activationRepo->findByUserId($userId);
            if (!empty($checkUser) && $checkUser->is_activated == 1) {
                return true;
            }
        }
        return false;
    }

    public static function getYoutubeImageThumbNail($url = "http://img.youtube.com/vi/SMq-8PGekts/0.jpg")
    {
        if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
            $values = $id[1];
        } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
            $values = $id[1];
        } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
            $values = $id[1];
        } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
            $values = $id[1];
        } else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
            $values = $id[1];
        } else {
            return false;
        }
        return $values;
    }

    /**
     * check if user is admin
     * @return boolean
     */
    public static function checkIsAdmin()
    {
        if (Auth::check()) {
            $userId = Auth::user()->id;

            $role = new RoleUserRepo();

            $checkAdmin = $role->findByUserId($userId);
            if (!empty($checkAdmin) && $checkAdmin->role_id == Config::get('config.role_admin')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set user session if not set
     */
    public static function setUserSessionBlade()
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $login = new LoginController();
            $login->setLoggedInUserInfo($user_id);
        }
    }

    public static function resizeImage($img, $size)
    {
        try {
            $img->resize(intval($size), null, function ($constraint) {
                $constraint->aspectRatio();
            });
            return $img;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function showGoalPercentage($campaignId, $goalAmount)
    {
        if (!empty($campaignId)) {
            // Creating Instance For Campaign Donation Repo\
            $campaignDonationRepo = new CampaignDonationRepo();
            $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaignId);
            $totalAmountRaised = !empty($totalAmountRaisedQuery[0]->totalAmountRaised) ? $totalAmountRaisedQuery[0]->totalAmountRaised : 0;

            $goalPercent = !empty($totalAmountRaised) ? floor(($totalAmountRaised / $goalAmount) * 100) : 0;

            return $goalPercent = ($goalPercent > 100) ? 100 : $goalPercent;
        }
    }

    public static function amountRaised($campaignId)
    {
        $campaignDonationRepo = new CampaignDonationRepo();
        $totalAmountRaisedQuery = $campaignDonationRepo->findTotalAmountRaisedForCampaignId($campaignId);
        $totalAmountRaised = !empty($totalAmountRaisedQuery[0]->totalAmountRaised) ? $totalAmountRaisedQuery[0]->totalAmountRaised : 0;
        return $totalAmountRaised;
    }

    public static function createThumb($image_detail)
    {
        ini_set('memory_limit', '2048M');
        $image = $image_detail['image'];
        $imageRealPath = $image_detail['inputFile']->getRealPath();
        $thumb_size = $image_detail['thumb_size'];
        $large_size = $image_detail['large_size'];
        $thumb_path = $image_detail['thumb_path'];
        $large_path = $image_detail['large_path'];
        $newImageThumb = Image::make($imageRealPath);

        $resized_thumb_image = self::resizeImage($newImageThumb, $thumb_size);

        $thumbPath = public_path(str_finish($thumb_path, '/') . $image->filename);
        $largePath = public_path(str_finish($large_path, '/') . $image->filename);
        if (!is_dir($tpath = dirname($thumbPath))) {
            File::makeDirectory($tpath, 0777, true);
        }
        if (!is_dir($lpath = dirname($largePath))) {
            File::makeDirectory($lpath, 0777, true);
        }

        if ($resized_thumb_image) {
            $resized_thumb_image->save($thumbPath);
        }

        $newImageLarge = Image::make($imageRealPath);
        $resized_large_image = self::resizeImage($newImageLarge, $large_size);
        if ($resized_large_image) {
            $resized_large_image->save($largePath);
        }
    }

    public static function croppedImageThumb($image_detail)
    {
        ini_set('memory_limit', '2048M');
        $image = $image_detail['image'];
        $imageRealPath = $image_detail['inputFile']->getRealPath();
        $thumb_size = $image_detail['thumb_size'];
        $large_size = $image_detail['large_size'];
        $thumb_path = $image_detail['thumb_path'];
        $large_path = $image_detail['large_path'];
        $newImageThumb = Image::make($imageRealPath);

        $resized_thumb_image = self::resizeImage($newImageThumb, $thumb_size);

        $thumbPath = public_path(str_finish($thumb_path, '/') . $image->filename);
        $largePath = public_path(str_finish($large_path, '/') . $image->filename);
        if (!is_dir($tpath = dirname($thumbPath))) {
            File::makeDirectory($tpath, 0777, true);
        }
        if (!is_dir($lpath = dirname($largePath))) {
            File::makeDirectory($lpath, 0777, true);
        }

        if ($resized_thumb_image) {
            $resized_thumb_image->save($thumbPath);
        }

        $newImageLarge = Image::make($imageRealPath);
        $resized_large_image = self::resizeImage($newImageLarge, $large_size);
        if ($resized_large_image) {
            $resized_large_image->save($largePath);
        }
    }

    /**
     * @author AK 16 June 2016
     * @param type $amount
     * @param type $campaignId
     * @return boolean
     */
//    public static function getApplicationFee($amount = 0, $campaignId = '0') {
    //        // Creating New Instnace For Campaign repo
    //        $campaignRepo = new CampaignRepo();
    //        try {
    //            $payment_option_charge = 0; //hard coded for now for stripe
    //            $campaign = $campaignRepo->findById($campaignId);
    //            $campaignCreatorId = $campaign->user_id;
    //            $userRoleRepo = new RoleUserRepo();
    //            $userDetails = $userRoleRepo->findByUserId($campaignCreatorId);
    //            if ((!empty($userDetails->role_id) && $userDetails->role_id == 3) || (!empty($campaign->charity_id))) {// Case Of Charity
    //                $feePercentage = Config::get('config.app_fee_for_charity');
    //            } else {// Case Of User
    //                $feePercentage = Config::get('config.app_fee_for_individual');
    //            }
    //
    //            $feePercentage = $feePercentage + $payment_option_charge;
    //
    //            return round(($amount * $feePercentage) / 100, 2);
    //        } catch (Exception $e) {
    //            return false;
    //        }
    //    }
    //
    public static function getApplicationFeeFromDB($amount = 0, $campaignId = '0')
    {
        // Creating New Instnace For Campaign repo
        $campaignRepo = new CampaignRepo();
        try {
            $payment_option_charge = 0; //hard coded for now for stripe
            $campaign = $campaignRepo->findById($campaignId);
            $campaignCreatorId = $campaign->user_id;
            $userRoleRepo = new RoleUserRepo();
            $userDetails = $userRoleRepo->findByUserId($campaignCreatorId);
            $campaign_payment_model = new CampaignPaymentRepo();

            $campaignType = self::checkCampaignType($campaignId);

            if ($campaignType == 'Charity') {
                $charityId = !empty($campaign->charity_id) ? $campaign->charity_id : $campaign->user_id;
                $campaign_payment_response = CampaignPaymentModel::where('user_id', $charityId)
                    ->where('campaign_id', 0)
                    ->where('payment_option_id', 3)
                    ->where('status', 'yes')
                    ->first();
            } else {
                $campaign_payment_response = $campaign_payment_model->findByCampaignId($campaignId);
            }

            if ($campaign_payment_response) {
                $feePercentage = $campaign_payment_response->percent_application_fees;
                $feePercentage = $feePercentage + $payment_option_charge;

                return round(($amount * $feePercentage) / 100, 2);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @author AK 16 June 2016
     * @param type $amount
     * @param type $paymentOptionId
     * @return boolean
     */
    public static function getFinalAmountCharge($amount = 0, $paymentOptionId = 3)
    {
        try {
            if (!empty($paymentOptionId)) {
                $paymentModel = new \Modules\Campaigns\Entities\PaymentOption();
                $payentOption = $paymentModel::find($paymentOptionId);
                $charge = !empty($payentOption->charge) ? $payentOption->charge : "0";
                return $amount = round(($amount + ($amount * $charge) / 100), 2);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public static function recieveUpdates($userId)
    {
        if (!empty($userId)) {
            // Create Instance for User Id
            $userRepo = new UserProfileRepo();
            $userPreofile = $userRepo->findByUserId($userId);
            $recieveUpdates = !empty($userPreofile->recieve_updates) && $userPreofile->recieve_updates == 1 ? true : false;
            return $recieveUpdates;
        } else {
            return false;
        }
    }

    public static function getTotalAmountToBeCharged($amount = 0, $campaignId = '0')
    {
        // Creating New Instnace For Campaign repo
        $campaignRepo = new CampaignRepo();
        try {
            $stripePercent = 0.986; //Hard Coded for stripe for now
            $stripeFixedAmount = 0.20;

            $campaign = $campaignRepo->findById($campaignId);
            $campaignCreatorId = $campaign->user_id;
            $userRoleRepo = new RoleUserRepo();
            $userDetails = $userRoleRepo->findByUserId($campaignCreatorId);
            $campaign_payment_model = new CampaignPaymentRepo();
            $campaignType = self::checkCampaignType($campaignId);

            if ($campaignType == 'Charity') {
                $charityId = !empty($campaign->charity_id) ? $campaign->charity_id : $campaign->user_id;
                $campaign_payment_response = CampaignPaymentModel::where('user_id', $charityId)
                    ->where('campaign_id', 0)
                    ->where('payment_option_id', 3)
                    ->where('status', 'yes')
                    ->first();
            } else {
                $campaign_payment_response = $campaign_payment_model->findByCampaignId($campaignId);
            }

            if ($campaign_payment_response) {
                $feePercentage = $campaign_payment_response->percent_application_fees;
                $giveBritePercentAmount = self::calculatePercentAmount($amount, $feePercentage);
                $totalAmount = ($amount + ($giveBritePercentAmount) + $stripeFixedAmount) / ($stripePercent);
                return round($totalAmount, 2);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public static function calculatePercentAmount($amount = 0, $percent = '0')
    {
        return round(($amount * $percent) / 100, 2);
    }

    public static function getStripeEstimatedAmount($amount = 0)
    {
        $stripe_percent = 1.4; // hard coded for now
        $stripe_fixed_amount = 0.20;
        $percent_amount = round(($amount * $stripe_percent) / 100, 2);
        $total_amount = $percent_amount + $stripe_fixed_amount;
        return $total_amount;
    }

    /*
             * function to refresh session
    */
    public static function refreshSession()
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $login = new LoginController();
            $login->setLoggedInUserInfo($user_id);
        }
    }

    /*
             * Function To check Campaign Is Expired Or Not
    */

    public static function isExpired($endDate)
    {
        $currentDate = date("m/d/y H:i:s");
        $endDate = !empty($endDate) ? $endDate : "";
        if ($currentDate > $endDate) {
            return true;
        } else {
            return false;
        }
    }

    /*
             * Function To get Location and cityname by id
    */

    public static function getLocationById($locationId)
    {
        if (!empty($locationId)) {
            $campaignLocationRepo = new CampaignLocationRepo();
            $location = $campaignLocationRepo->findById($locationId);
            return $location->name;
        }
    }

    /**
     * Helper Unlink Images
     */
    public static function unlinkImages($imageName, $type = 'campaign')
    {
        if (!empty($type) && $type == 'campaign') {
            $mainPath = Config::get('config.upload_path');
            $thumbPath = Config::get('config.thumb_campaign_upload_path');
            $largePath = Config::get('config.large_campaign_upload_path');
        } elseif (!empty($type) && $type == 'charity') {
            $mainPath = Config::get('config.charity_logo_upload_path');
            $thumbPath = Config::get('config.thumb_charity_logo_upload_path');
            $largePath = Config::get('config.large_charity_logo_upload_path');
        } elseif (!empty($type) && $type == 'user') {
            $mainPath = Config::get('config.profile_image_upload_path');
            $thumbPath = Config::get('config.thumb_profile_image_upload_path');
            $largePath = Config::get('config.large_profile_image_upload_path');
        } else {
            return true;
        }

        // Delete Main File
        if (!empty($mainPath) && File::exists($mainPath . $imageName)) {
            // Delete a single file
            try {
                File::delete($mainPath . $imageName);
            } catch (\Exception $ex) {
                return true;
            }
        }

        // Delete Thumb File
        if (!empty($thumbPath) && File::exists($thumbPath . $imageName)) {
            // Delete a single file
            try {
                File::delete($thumbPath . $imageName);
            } catch (\Exception $ex) {
                return true;
            }
        }
        // Delete Large File
        if (!empty($largePath) && File::exists($largePath . $imageName)) {
            // Delete a single file
            try {
                File::delete($largePath . $imageName);
            } catch (\Exception $ex) {
                return true;
            }
        }

        return true;
    }

    public static function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        return $output_file;
    }

    public static function CropImageHelper($x, $y, $w, $h, $base64_string)
    {
//        if (Session::has('image')) {
//            Session::pull('image');
//        }
        $imgname = md5(uniqid()) . ".jpg";

        $tempImagePath = 'images/thumb-campaigns/' . $imgname;

        $imagePath = self::base64_to_jpeg($base64_string, $tempImagePath);

        $croppedName = md5(uniqid()) . ".jpg";

        $configUpload = Config::get('config.upload_path') . $croppedName;

        $thumbPath  = Config::get('config.thumb_campaign_upload_path') . $croppedName;
        $largePath  = Config::get('config.large_campaign_upload_path') . $croppedName;
        $large_path = Config::get('config.large_campaign_upload_path');
        $thumb_size = Config::get('config.thumb_campaign_size');
        $large_size = Config::get('config.large_campaign_size');

        $image = new Imagick($imagePath);
        
        //Save the main image remove one if not used
        $image->writeImage($configUpload);
        $image->writeImage($largePath);

        //Save the main image
        $image->cropImage($w, $h, $x, $y);
        $image->writeImage($thumbPath);
        
        $newImageThumb = Image::make($thumbPath);
        $resized_thumb_image = self::resizeImage($newImageThumb, $thumb_size);
        if ($resized_thumb_image) {
            $resized_thumb_image->save($thumbPath);
        }
              
        Session::set('image', $croppedName);
        unlink($tempImagePath);
        //echo $outFile;
    }


    public static function substr_close_tags($code, $limit = 300)
    {
        if ( strlen($code) <= $limit )
        {
            return $code;
        }

        $html = substr($code, 0, $limit);
        preg_match_all ( "#<([a-zA-Z]+)#", $html, $result );

        foreach($result[1] AS $key => $value)
        {
            if ( strtolower($value) == 'br' )
            {
                unset($result[1][$key]);
            }
        }

        $openedtags = $result[1];

        preg_match_all ( "#</([a-zA-Z]+)>#iU", $html, $result );
        $closedtags = $result[1];

        foreach($closedtags AS $key => $value)
        {
            if ( ($k = array_search($value, $openedtags)) === FALSE )
            {
                continue;
            }
            else
            {
                unset($openedtags[$k]);
            }
        }

        if ( empty($openedtags) )
        {
            if ( strpos($code, ' ', $limit) == $limit )
            {
                return $html."...";
            }
            else
            {
                return substr($code, 0, strpos($code, ' ', $limit))."...";
            }
        }

        $position = 0;
        $close_tag = '';
        foreach($openedtags AS $key => $value)
        {   
            $p = strpos($code, ('</'.$value.'>'), $limit);

            if ( $p === FALSE )
            {
                $code .= ('</'.$value.'>');
            }
            else if ( $p > $position )
            {
                $close_tag = '</'.$value.'>';
                $position = $p;
            }
        }

        if ( $position == 0 )
        {
            return $code;
        }

        return substr($code, 0, $position).$close_tag."...";
    }

}
