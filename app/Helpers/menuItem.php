<?php

namespace App\Helpers;

use Helper;
use Lang;
use Modules\Admingivebrite\Repositories\MenusRepository as MenuRepo;
use Route;
use Session;
use URL;

/**
 * Menu helper class is used to manage different menu option for different user
 *
 * @author DS 01/21/16
 */
class MenuItemHelper {

	/**
	 * Function to get menu items
	 *
	 * @return type Array
	 * @author DS 01/21/16
	 */
	public static function getMenu() {
		$user = Session::get('user_session_data');
		$role = !empty($user['role']) ? $user['role'] : "";
		// $menuNavigationItems = config('dashboardmenus.' . $role);
		if ($user['active']) {
			$menuNavigationItems = config('dashboardmenus.' . 'Active' . $role);
		} else {
			$menuNavigationItems = config('dashboardmenus.' . $role);
		}

		return self::getMenuView($menuNavigationItems);
	}

	/**
	 * Function to get menu view for dashboard
	 *
	 * @author DS 01/21/16
	 */
	public static function getMenuView($menuNavigationItems) {
		$route_current = Route::currentRouteName();
		$menu = '';
		$class = config('dashboardmenus.Class');
		$routes = config('dashboardmenus.Routes');
		if (is_array($menuNavigationItems) && count($menuNavigationItems) > 0) {
			foreach ($menuNavigationItems as $key => $value) {
				$menu .= '<div class="listBox">'
				. '<div class="dash_head"> <h3>' . Lang::get($key) . '</h3> </div>';
				if (is_array($value) && count($value) > 0) {
					$menu .= '<div class="innerListbox">'
						. '<ul>';

					foreach ($value as $subKey => $subValue) {
						$menu .= '<li class="' . (isset($class[$subKey]) ? $class[$subKey] : '') . '">'
							. '<a href="' . (isset($routes[$subKey]) && !empty($routes[$subKey]) ? URL::route($routes[$subKey]) : '#') . '"><div class="dash_side">';
						$menu .= Lang::get($subValue);
						$menu .= '</div></a>'
							. '</li>';
					}

					$menu .= '</ul>'
						. '</div>';
				}

				$menu .= '</div>';
			}
		}

		return $menu;
	}

	public static function getCustomMenu($menuCategory) {

		$menu = new MenuRepo();
		$all = $menu->selectAll($menuCategory);
		return $all;
	}

}
