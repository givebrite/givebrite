<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationType extends Model {

    // Table Name
    protected $table = 'donation_type';
    protected $fillable = ['title'];

    public function selectAll() {
        $a = [];
        $post = DonationType::all();
        foreach ($post as $p) {
            array_push($a, $p->title);
        }
        return $a;
    }

    public function findByTitle($title) {
        $data = DonationType::all()->where('title', $title);
        return $data;
    }

    public function findById($donation_id) {
        $post = DonationType::all()->where('id', $donation_id)->first();       
        return $post;
    }

}
