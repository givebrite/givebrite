function follow(objButton) {

    var is_followed = 0;
    var user_id = objButton.id;
    var token = objButton.name;
    if (objButton.value === "Follow")
    {
        is_followed = 1;
        objButton.value = "Unfollow";
    } else
    {
        is_followed = 0;
        objButton.value = "Follow";
    }
    $.ajax({
        type: "POST",
        url: 'follow',
        data: {user_id: user_id, is_followed: is_followed,'_token': token},
        success: function (data) {
                
            }
    });
}

function setCharityIdForCampaignCreation(objButton)
{
    var charity_id = objButton.id;
    var token = objButton.name;
    $.ajax({
        type: "POST",
        url: 'set-id',
        data: {id: charity_id, '_token': token},
        success: function (data) {
            window.location.href = $('#baseUrl').val() + '/campaigns/create';
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        }
    });
}
