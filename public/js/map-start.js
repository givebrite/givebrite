/**
 * This file contains the logic for the Sell My Home page
 * 
 * @author DS 03-18-2016
 */
$("document").ready(function(){
    getLocation();
});
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
       alert("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    $("#current_latitude").val(position.coords.latitude);
    $("#current_longitude").val(position.coords.longitude);
}
var map;
var markers = [];

function initMap() {
    if ($('#lat-val').length > 0) {
        var lat = $('#lat-val').val();
        var long = $('#long-val').val();
        lat = parseFloat(lat);
        long = parseFloat(long);
        var myLatLng = {lat: lat, lng: long};

        map = new google.maps.Map(document.getElementById('campaign-map'), {
            zoom: 12,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
            center: myLatLng
        });
        if (lat != '' && long != '') {
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
            markers.push(marker);
        }

    } 
}