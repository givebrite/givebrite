$(function () {
    var liNumItemsCampaign = $(".campaign-pagination .pagination li").length;
    var liNumItemsFund = $(".fund-pagination .pagination li").length;

    if (liNumItemsCampaign > 3)
    {
        $(".loadMoreBlockCampaign").css("display", "block");
    }

    if (liNumItemsFund > 3)
    {
        $(".loadMoreBlockFund").css("display", "block");
    }

    $(".mobile-btn-campaign").click(function () {
        $('.scroll-campaign').jscroll({
            nextSelector: '.campaign-pagination .pagination li.active +  li a',
            contentSelector: 'div.scroll-campaign',
            callback: function () {
                $(".scroll-campaign").delay(800).fadeIn(400);
            }
        });
        $(".loadMoreBlockCampaign").css("display", "none");
    });

    $(".mobile-btn-fund").click(function () {
        $('.scroll-fund').jscroll({
            nextSelector: '.fund-pagination .pagination li.active + li a',
            contentSelector: 'div.scroll-fund',
            callback: function () {
                $(".scroll-fund").delay(800).fadeIn(400);
            }
        });
        $(".loadMoreBlockFund").css("display", "none");
    });


});






