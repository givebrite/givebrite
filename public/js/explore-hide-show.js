$(document).ready(function () {
    var screenWidth = $(window).width();
    if (screenWidth < 768)
    {
        $(".explore-custom").hide();
        $(".colHeading").click(function () {
            $(".explore-custom").slideToggle("slow");
        });
    }
});