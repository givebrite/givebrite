$(document).ready(function() {
    getUpdates(1);
    getBackers(1);
    $(document).on('click', '#update-pagination-div .pagination a', function(e) {
        getUpdates($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    });
    $(document).on('click', '#backers-pagination-div .pagination a', function(e) {
        getBackers($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    });
});

function getUpdates(page) {
    var url = $("#get-updates-url").val();
    var request = $(this);
    if (request.data('requestRunning')) {
   //     return;
    }
    request.data('requestRunning', true);
    $.ajax({
        url: url+'?page=' + page,
        cache: false,
        dataType: 'json',
    }).done(function(data) {
        if(data.responseCode==200){
            $('#campaign-update-div').html(data.responseView);
            // $('.readmore-content').readmore({speed: 500});
        }
        request.data('requestRunning', false);
    }).fail(function() {
      //  alert('Campaigns could not be loaded.');
    });
   
}

function getBackers(page) {
    var url = $("#get-backers-url").val();
    var request = $(this);
    if (request.data('requestRunning')) {
    //    return;
    }
    request.data('requestRunning', true);
    $.ajax({
        url: url+'?page=' + page,
        dataType: 'json',
        cache: false,
    }).done(function(data) {
        if(data.responseCode==200){
            $('#backers-list-div').html(data.responseView);
            // $('.readmore-content').readmore({speed: 500});
        }
        request.data('requestRunning', false);
    }).fail(function() {
      //  alert('Campaigns could not be loaded.');
    });
   
}
