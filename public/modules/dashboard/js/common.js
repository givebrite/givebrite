/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $("a").removeClass("selectTab");
    var currentUrl = location.href;
    var parts = currentUrl.split("?");
    currentUrl = parts[0];
    $('.innerListbox  a[href="'+currentUrl+'" ]').addClass("selectTab");
    $('.innerListbox  a[href="#" ]').css("cursor","default");
    // Ajax Request For Campaign Status Update
    $(".changeCampaignStatus").click(function(e){
        e.preventDefault();
        var url=$(this).attr('href');
        var request = $(this);
            if (request.data('requestRunning')) {
           //     return;
            }
    request.data('requestRunning', true);
   $.ajax({
        url: url,
        type: 'post',
        dataType:'json',
        cache: false,
    processData: false,
		headers: {
		'X-CSRF-TOKEN': $("input[name=_token]").val()
	},
        data: {},
        success: function (response) {
            if(response.responseCode==200){
                toastr.clear();
               toastr.success(response.responseMessage);
               if(response.responseClass == 'info'){
                   $('a[href="'+url+'" ]').text("Start Campaign");
                   $('a[href="'+url+'" ]').removeClass('btn-danger');
                   $('a[href="'+url+'" ]').addClass('btn-info');
                   $("#update_"+response.campaignId).addClass("hidden");
               }else{
                   $('a[href="'+url+'" ]').text("Stop Campaign");
                   $('a[href="'+url+'" ]').removeClass('btn-info');
                   $('a[href="'+url+'" ]').addClass('btn-danger');
                   $("#update_"+response.campaignId).removeClass("hidden");
               }
               e.preventDefault();
            }
        },
        error: function(response){
            
        },
        complete: function () {
            request.data('requestRunning', false);
        }
    });
    });
});
