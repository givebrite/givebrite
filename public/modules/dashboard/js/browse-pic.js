$(document).ready(function () {
    
    if ($('#viewProfileImage').length > 0) {
        if ($('#viewProfileImage', this).attr('src') == '') {
            $('#viewProfileImage').hide();
        }

        $("#upload_pic").change(function() {
            showImageBackground(this, '#viewProfileImage');
        });
    }
    
    $("#remove-profile-image").click(function() {
        //Function to save rewards
        var url = $("#romove-profile-url").val();
         var request = $(this);
        if (request.data('requestRunning')) {
          //  return;
        }
        request.data('requestRunning', true);
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: {},
            success: function(response) {
                if (response.responseCode == 200) {
                   $("#viewProfileImage").css("background-image","url("+response.responseData+")");
                   $("#remove-profile-image").hide();
                }
            },
            error: function(response) {
               toastr.error("Oops something went wrong! Please reload your browser.");
            },
            complete: function() {
                request.data('requestRunning', false);
            }
        });

    });  
    
    if ($('.phone').length > 0) {
//        $('.phone').mask('0000000000', {clearIfNotMatch: true});
        $('.zip').mask('99999-9999');
        $('.registration').mask('9999-9999-9999-9999');
    }
});    