/**
 * Method to get image path and render it
 * @param input file array
 * @param string id of image element
 * @returns none
 * @author AK  2/03/2015
 */
var imgPath;
function showImage(input, id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(id).show();
            $(id).attr('src', e.target.result);
            $("#viewCampaignImage1").attr('src', e.target.result);
            $("#viewTeamImage").attr('src', e.target.result);
            $("#img_temp_path").val(e.target.result);
            imgPath = e.target.result;

            $("#crop-buttons").show();
        };
        if ($(".customURLbox").length > 0) {
            $(".customURLbox").removeClass('hidden-xs');
        }
        //   Image::make(e.target.result)->orientate();
        reader.readAsDataURL(input.files[0]);


    }

}

$("input[name=campaign_image]").change(function () {
    file = this.files;
    //console.log(file);
    setTimeout(function () {
        $("#cropit").click();
    }, 1000);
});

$("input[name=cover_photo]").change(function () {
    file = this.files;
    //console.log(file);
    setTimeout(function () {
        $("#cropit").click();
    }, 1000);
});

$("input[name=team_logo]").change(function () {
    
    file = this.files;
    $('#viewTeamImage').attr('src',file);
    //console.log(file);
    setTimeout(function () {
        $("#cropit-two").click();
    }, 1000);
});


//$(function() {
//                $('#cropimage').Jcrop({
//                    onSelect: updateCoords
//                });
//            });
function updateCoords(c) {
    
    $(".jcrop-box").attr('type', 'button')
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
}

function updateCoordsN(c) {
    
    $(".jcrop-box").attr('type', 'button')
    $('#xx').val(c.x);
    $('#yy').val(c.y);
    $('#ww').val(c.w);
    $('#hh').val(c.h);
}


var jcrop_api;

$("#cropit").click(function (e) {

    var x = $('#x').val();
    var y = $('#y').val();
    var w = $('#w').val();
    var h = $('#h').val();
        
    $("#imgpath").val(imgPath);

    if (typeof jcrop_api != 'undefined') {
        jcrop_api.destroy();
    }
    
    $('.bd-example-modal-lg').on('shown.bs.modal', function () {
        $('#viewCampaignImage1').Jcrop({
            allowSelect: false,
            aspectRatio: 3 / 2,
            boxWidth: 855, 
            onSelect: updateCoords,
            setSelect: [x,y,w,h],
            minSize : [360,270]
            
        });
    });
});

var jcrop_api;

$("#cropit-two").click(function (e) {

    var x = $('#xX').val();
    var y = $('#yy').val();
    var w = $('#ww').val();
    var h = $('#hh').val();

    $("#imgPathTeam").val(imgPath);

    if (typeof jcrop_api != 'undefined') {
        jcrop_api.destroy();
    }
    $('.team-logo-model').on('shown.bs.modal', function () {
        $('#viewTeamImage').Jcrop({
            aspectRatio: 3 / 2,
            onSelect: updateCoordsN,
            boxWidth: 855, 
            setSelect: [x,y,w,h],
            minSize : [360,270]
         });
    });
});



function showImageBackground(input, id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(id).show();
            $(id).css("background-image", "url(" + e.target.result + ")");
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function showError(fields) {
    // Check For Array
    if (fields.constructor === Array) {
        for (var i = 0, length = fields.length; i < length; i++) {
            var spanId = fields[i] + "-" + "error";
            var divId = fields[i] + "_" + "error";
            var error = $("#" + spanId).text();
            // Empty Error Div
            $("#" + divId).text("");
            if (error.length > 0) {
                $("#" + divId).text(error);
            }
        }
    } else if (fields) {
        var spanId = fields + "-" + "error";
        var divId = fields + "_" + "error";
        var error = $("#" + spanId).text();
        // Empty Error Div
        $("#" + divId).text("");
        if (error.length > 0) {
            $("#" + divId).text(error);
        }
    }

}