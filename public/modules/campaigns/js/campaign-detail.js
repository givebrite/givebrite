if ($('#viewCharityImage').length > 0) {
        if ($('#viewCharityImage', this).attr('src') == '') {
            $('#viewCharityImage').hide();
        }

        $("#charityImage").change(function() {
            showImage(this, '#viewCharityImage');
        });
    }
    
$(document).ready(function($){
    if ($('.phone').length > 0) {
        $('.phone').mask('999-999-9999');
        $(".phone").on("blur", function () {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);
            if (last.length == 3) {
                var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
                var lastfour = move + last;
                var first = $(this).val().substr(0, 9);
                $(this).val(first + '-' + lastfour);
            }
        });
        $('.zip').mask('99999-9999');
        $('.registration').mask('9999-9999-9999-9999');
    }
   // Manage Charity Block 
   manageCharityBlock();
    $("input[name=end_date]").on('focus', function(ev) { ev.preventDefault(); }); // untested!
});    

$("input[name='registered_charity']").change(function (){
    manageCharityBlock();
});

function manageCharityBlock(){
    if($("input[name='registered_charity']:checked").val()=='yes'){
        $("#charity-form").css('display','inline');
    }else{
        $("#charity-form").css('display','none');
    }
}