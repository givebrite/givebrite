$(document).ready(function(){
  if($("#postcode").val().length>0){
    getCityAndTown();
  } 
   $("input[name=postcode]")
       .geocomplete({details:".details"})
       .bind("geocode:result", function(event, result){
           getCityAndTown();
    });
  if ($('.location-link').length > 0) {
    $(".location-link").click(function() {
        if($(".hide-Location").length>0){
            $("input[name=postcode]").rules( "remove" );
            if($("#postcode-error").length>0){
                $("#postcode-error").text("");
            }
            $("input[name=postcode]").val("");
            $("input[name=country]").val("");
            $("input[name=locality]").val("");
            $("#location-div").removeClass('hide-Location');
            $("#location-div").addClass('show-Location');
        }else{
             $("#location-div").removeClass('show-Location');
             $("#location-div").addClass('hide-Location');
        }
    });
  }
  manageMediaType();
 
 var geocoder;
 var map; 
  function getCityAndTown() {
            var address = $(".details input[name=formatted_address]").val();
            if(address.length>0){
                geocoder = new google.maps.Geocoder();
                var request = {
                    address:address,
                }
            }else{
                return false;
            }
            
            geocoder.geocode(request, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {                       
                    for (var component in results[0]['address_components']) {
                        for (var i in results[0]['address_components'][component]['types']) {
                            if (results[0]['address_components'][component]['types'][i] == "postal_town") {
                                town = results[0]['address_components'][component]['long_name'];
                                $("input[name=locality]").val(town);
                            }
                            if (results[0]['address_components'][component]['types'][i] == "country") {
                                country = results[0]['address_components'][component]['long_name'];
                                $("input[name=country]").val(country);
                            }
                            if (results[0]['address_components'][component]['types'][i] == "postal_code") {
                                postalcode = results[0]['address_components'][component]['short_name'];
                                $("input[name=postcode]").val(postalcode);
                            }
                        }
                    }
                    // Lat-- Long
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    $("input[name=lat]").val(latitude);
                    $("input[name=lng]").val(longitude);
                    errorText=$("#postcode-error").text();
                    if(errorText.length>0){
                        if($(".show-Location").length>0){
                            $("#location-div").removeClass('show-Location');
                            $("#location-div").addClass('hide-Location');
                        }
                    }else if($("#postcode").val().length>0){
                        $("#location-div").removeClass("hide-Location");
                        $("#location-div").addClass("show-Location");
                    }
                } else {
                    alert('Invalid Zipcode');
                    $("#postcode").val("");
                }
            });
        }
});

if ($('#viewCampaignImage').length > 0) {
    if ($('#viewCampaignImage', this).attr('src') == '') {
        $('#viewCampaignImage').hide();
    }

    $("#campaignImage").change(function() {
        showImage(this, '#viewCampaignImage');
    });
}
    
 $(".facebookUpload").click(function(){
     $("input[name='media_type']").val("facebook");
     manageMediaType();
 });
 $(".pcUpload").click(function(){
     $("input[name='media_type']").val("image");
     manageMediaType();
 });
 $(".youtubeUpload").click(function(){
     $("input[name='media_type']").val("youtube");
     manageMediaType();
 });
  
function manageMediaType(){
    $("#facebook-url").css("display",'none');
    $("#image-div").css("display",'none');
    $("#youtube-url").css("display",'none');
    var divSelected=$("input[name='media_type']").val();
    if(divSelected=='facebook'){
        $("#facebook-url").css("display",'inline-block');
    }else if(divSelected=='image'){
        $("#image-div").css("display",'inline-block');
    }else if(divSelected=='youtube'){
        $("#youtube-url").css("display",'inline-block');
    }
    
}  
/**
 * Function to change cityByLocation
 */
function changeCityByLocation() {
    var locationId = $("select[name=location]").val();
    var url = $("#get-city-url").val();
    var request = $(this);
    if (request.data('requestRunning') || locationId.length == 0) {
        return;
    }
    request.data('requestRunning', true);
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {'locationId': locationId},
        success: function(response) {
            if (response.responseCode == 200) {
                $("#cityByLocation").html(response.responseView);
                var cityPrevious=$("#prev-address").val();
                $("select[name='city']").val(cityPrevious);
            }
        },
        error: function(response) {
        },
        complete: function() {
            request.data('requestRunning', false);
        }
    });
}

function showMedia(type){
    if(type=='facebook'){
        $("#facebook-url").css('display','inline-block');
    }else if(type=='youtube'){
        $("#youtube-url").css('display','inline-block');
    }
}

function setAddress()
{
    $("input[name='address']").val($("select[name='city'] option:selected").text());
}

function setCity()
{
     $("select[name='city']").val($("#address_id").val());
}
