var format = ['bold', 'italic', 'underline','link','image','video'];

var quill = new Quill('#editor-container', {
  modules: {
    toolbar: [
      ['bold', 'italic', 'underline'],
      ['link','image','video']
    ]
  },
  placeholder: 'Tell the world about your story here...',
  theme: 'snow',  // or 'bubble'
  formats : format
});


var syncHtml = debounce(function() {
  var contents = $(".ql-editor").html();
  $('#camp_desc').val(contents);
}, 500);

quill.on('text-change', function() {
   syncHtml();
});

$(".ql-editor").html($('#camp_desc').val())

/*
$("#campaign-design").submit(function(e){
  $("#camp_desc").val(quill.getText());
});*/

function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};