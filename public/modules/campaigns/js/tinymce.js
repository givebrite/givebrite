// Overriding the shortcut keys
tinymce.on('SetupEditor', function (editor) {
    editor.shortcuts = {add: function () {}};
});

tinymce.init({
    selector: '.textarea',
    setup: function (ed) {
        ed.on("change", function () {
            validateDescription(ed);
        });
    },
    height: 188,
    toolbar: "bold italic underline | link image media",
    menubar: "",
    plugins: [
        // 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        // 'searchreplace wordcount visualblocks visualchars code fullscreen',
        //'insertdatetime media nonbreaking save table contextmenu directionality',
        //'emoticons template paste textcolor colorpicker textpattern imagetools'
        'paste link image media'
    ],
    paste_preprocess: function (pl, o) {
        //example: keep bold,italic,underline and paragraphs
        //o.content = strip_tags( o.content,'<b><u><i><p>' );

        // remove all tags => plain text
        o.content = strip_tags(o.content, '<br>, <p>');
    },
    media_live_embeds: true,
    templates: [
    ],
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css',
        '{{ asset("modules/campaigns/css/campaign.css") }}'
    ]
});

function strip_tags(str, allowed_tags)
{
    var key = '', allowed = false;
    var matches = [];
    var allowed_array = [];
    var allowed_tag = '';
    var i = 0;
    var k = '';
    var html = '';
    var replacer = function (search, replace, str) {
        return str.split(search).join(replace);
    };
// Build allowes tags associative array
    if (allowed_tags) {
        allowed_array = allowed_tags.match(/([a-zA-Z0-9]+)/gi);
    }
    str += '';

// Match tags
    matches = str.match(/(<\/?[\S][^>]*>)/gi);
// Go through all HTML tags
    for (key in matches) {
        if (isNaN(key)) {
            // IE7 Hack
            continue;
        }

        // Save HTML tag
        html = matches[key].toString();
        // Is tag not in allowed list? Remove from str!
        allowed = false;

        // Go through all allowed tags
        for (k in allowed_array) {            // Init
            allowed_tag = allowed_array[k];
            i = -1;

            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + '>');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + ' ');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('</' + allowed_tag);
            }

            // Determine
            if (i == 0) {
                allowed = true;
                break;
            }
        }
        if (!allowed) {
            str = replacer(html, "", str); // Custom replace. No regexing
        }
    }
    return str;
}

$(document).ready(function () {
    $("#deleteit").click(function (e) {
        e.preventDefault();
        $("#viewCampaignImage").attr("src", "");
        $("#crop-buttons").hide();
    });
});