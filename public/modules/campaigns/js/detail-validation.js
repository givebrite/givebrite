$(document).ready(function(){
     
     $( "#datepicker" ).datepicker();
     var form = $("#campaign-detail");
     var validator = form.validate();
     // Add Custom Rules On Page Load
     customDetailValidation();
     
     form.submit(function(){
        $(".text-danger").text("");
        $( "#datepicker" ).datepicker('hide'); 
        customDetailValidation();
        
        var error = false;
        $(".help-block").each(function(){
            if($(this).text().length>0){
                error = true;
            }
        });
        $(".text-danger").each(function(){
            if($(this).text().length>0){
                error = true;
            }
        });
        if(error){
            $("#submit").attr('disabled',false);
            // $("#submit").html('Next <i class="fa fa-arrow-right" aria-hidden="true"></i>');
        }else{
            $("#submit").attr('disabled',true);
            $("#submit").text('Loading ...');
        }
        
     });
     
     $("input").change(function(){
         $(".text-danger").text("");
     });
     $( "#datepicker" ).click(function(){
         $( "#datepicker" ).datepicker('show'); 
     });
     $(".fa-calendar").click(function(){
         $( "#datepicker" ).datepicker('show'); 
     });
     
     $("input[name=campaign_duration]").change(function(){
         customDetailValidation();
     });
     
     $("input[name=end_date]").change(function(){
         $(".text-danger").text("");
         $("#datepicker-error").text("");
         customDetailValidation();
     });
    
 });
 
function customDetailValidation(){
    if ($("input[name=campaign_duration]:checked").val() === "enddate") {
          $("input[name=end_date]").rules( "add", {
            required: true,
            minDate: true,
           // focusInvalid: false,
            messages: {
              required: "End date is required."
            }
        });
    }else{
        $("#datepicker-error").text("");
        $("input[name=end_date]").rules("remove");
        $("input[name=end_date]").val("");
    }
    $.validator.addMethod("minDate", function(value, element) {
        var curDate = new Date();
        var inputDate = new Date(value);
        if (inputDate > curDate)
            return true;
        return false;
    }, "End date must be greater than current date.");   // error message
 }
 
 
 