$(document).ready(function(){
    $("form#campaign-design").submit(function(e) {
        $(".text-danger").text("");
        validateTheme(e);
        validateDescription(e);
        
        var error = false;
        $(".help-block").each(function(){
            if($(this).text().length>0){
                error = true;
            }
        });
        $(".text-danger").each(function(){
            if($(this).text().length>0){
                error = true;
            }
        });
        if(error){
            $("#submit").attr('disabled',false);
            // $("#submit").html('Next <i class="fa fa-arrow-right" aria-hidden="true"></i>');
        }else{
            $("#submit").attr('disabled',true);
            // $("#submit").text('Loading ...');
        }
        
    });
    $("input").change(function(){
         $(".text-danger").text("");
     });
    $('input[name=theme_id]').change(function(e){
       validateTheme(e);
    });
   
});


function validateTheme(e) {
    //theme_id-error
    var themeSelected = $('input[name=theme_id]:checked').val();
    if (themeSelected) {
        $("#theme_id_error").text("");
        e.stopPropagation();
    } else {
        $("#theme_id_error").text("Theme is required");
        e.preventDefault();
    }
}

function validateDescription(e) {
    // Description Validation
    tinyMCE.triggerSave();
    var descriptionVal = $("textarea[name=campaign_description]").val();
    if (descriptionVal.length > 0) {
        $("#campaign_description_error").text("");
        if (descriptionVal.length < 50) {
            $("#campaign_description_error").text("The campaign description must be at least 50 characters.");
            e.preventDefault();
        } else {
            e.stopPropagation();
            $("#campaign_description_error").text("");
        }
    } else {
        $("#campaign_description_error").text("Description is required");
        e.preventDefault();
    }
}