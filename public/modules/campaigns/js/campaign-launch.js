$(function () {
    var d = [];
    $.ajax({
        url: site_url + '/donation_list/',
        dataType: 'json',
        success: function (data) {
            d = data;
        }
    });

    // var d_i = ["Lilah", "Sadaqa", "Zakaat", "General"];

    setTimeout(function () {
        $("#donation_text").autocomplete({
            source: d,
        });
    }, 1500);

});

$(document).ready(function () {

    // donation type toggles
    $("#cmn-toggle-1").click(function () {
        if ($("#cmn-toggle-1").prop('checked')) {
            $("#donation-type").show();
            $("#switchselect1").addClass('darkbg');
            $("#switchselect1").removeClass('lightbg');
        } else {
            $("#donation-type").hide();
            $("#switchselect1").addClass('lightbg');
            $("#switchselect1").removeClass('darkbg');
        }
    });

    // Payment gateway toggles
    $("#cmn-toggle-2").click(function () {
        if ($("#cmn-toggle-2").prop('checked')) {
            $("#switchselect2").addClass('darkbg');
            $("#switchselect2").removeClass('lightbg');
        } else {
            $("#switchselect2").addClass('lightbg');
            $("#switchselect2").removeClass('darkbg');
        }
    });

    $("#cmn-toggle-3").click(function () {
        if ($("#cmn-toggle-3").prop('checked')) {
            $("#switchselect3").addClass('darkbg');
            $("#switchselect3").removeClass('lightbg');
        } else {
            $("#switchselect3").addClass('lightbg');
            $("#switchselect3").removeClass('darkbg');
        }
    });

    // Enter control
    $("#donation_text").keypress(function (e) {
        if (e.which == 13) {
            $("#add_type").click();
        }
    });

    /*before posting the form check if the donation type inputbox is not empty*/ 
    $( "#launchForm" ).submit(function( e ) {
      
      title = $("#donation_text").val();
      
      if(title != ''){

             var html =
                    '<tr id="donor' + title + '">\
                        <input type="hidden" name="donation_type[]" value="' + title + '" /><td>' + title + '</td>\
                        <td align="center"><div class="control-group">\
                                <label class="control control--radio">\
                                    <input type="radio" name="dnt_type" value="'+title+'"/>\
                                    <div class="control__indicator"></div>\
                                </label>\
                            </div></td>\
                        <td><a id="btn' + title + '" href="" class="deleteDonation"><i class="fa fa-trash" ></i></a></td>\
                    </tr>';
            $("#donate_tbl tbody").append(html);   
      }
    
    });


    // Add donation type
    $("#add_type").click(function (e) {
        e.preventDefault();
        title = $("#donation_text").val();

        if (title == '') {
            alert('Please fill donation type');
            return false;
        }
        $('td:first-child').each(function () {
            if (title == $(this).text()) {
                alert('Already Added');
                exit;
            }
        });
        var html =
                '<tr id="donor' + title + '">\
                    <input type="hidden" name="donation_type[]" value="' + title + '" /><td>' + title + '</td>\
                    <td align="center"><div class="control-group">\
                            <label class="control control--radio">\
                                <input type="radio" name="dnt_type" value="'+ title +'"/>\
                                <div class="control__indicator"></div>\
                            </label>\
                        </div></td>\
                    <td><a id="btn' + title + '" href="" class="deleteDonation"><i class="fa fa-trash" ></i></a></td>\
                </tr>';
        $("#donate_tbl tbody").append(html);
        $("#donate_tbl").show();
        $("#donation_text").val('');

        $('.deleteDonation').click(function (e) {
            e.preventDefault();
            rmid = 'donor' + this.id.substring(3);
            $("#" + rmid).html('');
        });
    });

    // Store campaign
    $("#launch-link").click(function (e) {

        // Getting the id of Campaign
        var cmp_id = $("#cmp_id").val();

        // Getting the array of donor_types added by user
        var donor_type = [];
        $('td:first-child').each(function () {
            donor_type.push($(this).text());
        });

        
        // Disabled as connected charities do not need a payment gateway
        /*
        // Getting at least one payment gateway
        if (!$("#cmn-toggle-2").prop('checked')) {
            e.preventDefault();
            $("#pay_error").html('Please turn on the payment gateway');
            return false;
        }*/

        // Payment gateway toggle 
        $("#cmn-toggle-2").click(function(){
            if(($this).attr('checked')){
                alert('checked');
            }else{
                alert('unchecked');
            }
        });

        // Submitting data to the database
        if ($("#cmn-toggle-1").prop("checked")) {
            // Ajax call to save everything
            data = {cmp_id: cmp_id, donor_type: donor_type};
            $.ajax({
                url: site_url + '/donation_update/',
                method: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    console.log(data);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

    });
});

function managePaymentSetup(campaignPaymentId, optionid, status) {
    if (status == 'yes') {
        disablePaymentOption(campaignPaymentId, optionid);
    } else if (status == 'no') {
        disablePaymentOption(campaignPaymentId, optionid);
    } else {
        $("input[name='live_secret_key']").val('');
        $("input[name='live_publishable_key']").val('');
        $("input[name='payment_option_id']").val(optionid);
        $("#enable-payment-option").css('display', 'block');
        $("a").css('cursor', 'pointer');
    }

}

function savePaymentOption() {
    var url = $("form#payment-form").attr('action');
    var optionId = $("input[name='payment_option_id']").val();
    data = $("form#payment-form").serialize();
    var request = $(this);
    if (request.data('requestRunning')) {
        return;
    }
    request.data('requestRunning', true);
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data,
        success: function (response) {
            if (response.responseCode == 200) {
                $("#enable-payment-option").css('display', 'none')
                $("a").css('cursor', 'pointer');
                $("#enable-button-" + optionId).html(response.responseView);
                toastr.success('Payment option added successfully.');
            }
        },
        error: function (response) {
            var errors = response.responseJSON;
            $.each(errors, function (index, value) {
                $("#err_" + index).text(value);
            });
        },
        complete: function () {
            request.data('requestRunning', false);
        }
    });
}

function disablePaymentOption(campaignPaymentId, optionId) {
    var url = $("form#disable-payment-form-" + campaignPaymentId).attr('action');
    var request = $(this);
    if (request.data('requestRunning')) {
        return;
    }
    var data = $("form#disable-payment-form-" + campaignPaymentId).serialize();
    request.data('requestRunning', true);
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data,
        success: function (response) {
            if (response.responseCode == 200) {
                $("a").css('cursor', 'pointer');
                if (response.payment_status == 'yes')
                {
                    $("#launch-link").removeClass('disabled-link');
                } else
                {
                    $("#launch-link").addClass('disabled-link');
                }
                $("#enable-button-" + optionId).html(response.responseView);
                toastr.clear();
                toastr.success('Payment option changed successfully.');
            }
        },
        error: function (response) {

        },
        complete: function () {
            request.data('requestRunning', false);
        }
    });
}
