// set cursor to pointer for all anchor tags
$("a").css("cursor","pointer");

$(document).ready(function(){
    manageSelect();
});

function manageSelect(){
    // start by setting everything to enabled
    $('#saveLocation select[name="shipping_location[]"] option').attr('disabled',false);

    // loop each select and set the selected value to disabled in all other selects
    $('#saveLocation select[name="shipping_location[]"]').each(function(){
        var $this = $(this);
        $('#saveLocation select[name="shipping_location[]"]').not($this).find('option').each(function(){
              // if($(this).attr('value') == $this.val())
               // Check Comma Seperated String Contain value or not
              if( $this.val() != null && $this.val().toString().match(new RegExp("(?:^|,)"+$(this).attr('value')+"(?:,|$)"))){
                   $(this).attr('disabled',true);
               }
        });
    });
}
 
function addLocation(){
    var newLocation=$("#to-be-append").html();
    $("#saveLocation").append(newLocation);
    $('#saveLocation .location-select').selectpicker();
}

//Function to save rewards
function saveReward(){
    var url=$("#reward-form").attr("action");
    
    var str="";var count=0;
     $('#saveLocation select[name="shipping_location[]"]').each(function(){
        if(count==0){
            str=$(this).val();
        }
        else {
            str=str+'#'+$(this).val();
        }
        count++;
    });
     $('input[name="location[]"]').val(str);
    var data= $("#reward-form").serialize();
    var request = $(this);
        if (request.data('requestRunning')) {
            return;
        }
    request.data('requestRunning', true);
   $.ajax({
        url: url,
        type: 'post',
        dataType:'json',
        data: data,
        success: function (response) {
            if(response.responseCode==200){
                 $("#campaign-reward-list").html(response.responseView);
                 $('#reward-form').trigger("reset");
                 $("a").css("cursor","pointer");
                 $("#add-reward-div").css('display','none');
                 $('#saveLocation .location-select').selectpicker();
            }
        },
        error: function(response){
            var errors = response.responseJSON;
            $.each(errors, function(index, value) {
               $("#err_"+index).text(value);
            });
        },
        complete: function () {
            request.data('requestRunning', false);
        }
    });
}

//Display Edit Reward Form
function editReward(url, campId){
    
    var request = $(this);
        if (request.data('requestRunning')) {
            return;
        }
    request.data('requestRunning', true);
    $.ajax({
        url: url,
        type: 'get',
        dataType:'json',
        data: {'campaignId':campId},
        success: function (response) {
             if(response.responseCode==200){
                 $("#campaign-reward-form").html(response.responseView);
                 $("a").css("cursor","pointer");
                 $('#saveLocation .location-select').selectpicker();
                 $("#add-reward-div").css('display','block');
            }
        },
        error: function(response){
        },
        complete: function () {
            request.data('requestRunning', false);
        }
    });
}



// function Delete Rewards By Campaign Id
function deleteReward(url)
{
   var request = $(this);
        if (request.data('requestRunning')) {
            return;
        }
    request.data('requestRunning', true);
    $.ajax({
        url: url,
        type: 'get',
        dataType:'json',
        data: {},
        success: function (response) {
             $("#campaign-reward-list").html(response.responseView);
             $("a").css("cursor","pointer");
        },
        error: function(response){
        },
        complete: function () {
            request.data('requestRunning', false);
        }
    });
}
/**
 * 
 * Update Campaign Rewards required or not
 */
$('[name="campaign_rewards"]').change(function(){
        var request = $(this);
        if (request.data('requestRunning')) {
            return;
        }
    var url=$("#confirm-reward-form").attr('action');
    var data= $("#confirm-reward-form").serialize();
    request.data('requestRunning', true);
    $.ajax({
        url: url,
        type: 'post',
        dataType:'json',
        data: data,
        success: function (response) {
           
        },
        error: function(response){
        },
        complete: function () {
            request.data('requestRunning', false);
        }
    });
});
