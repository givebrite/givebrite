$(document).ready(function () {
    var checked = ($("input[type=radio][name='sign_up_option']:checked").val());
    if (checked === "charity")
    {
        $(".headingForm").text("Sign up with email address");
        $(".social-media-buttons").css("display", "none");
        $("#charity1-form").css("display", "block");
        $("#charity2-form").css("display", "block");
        $("#user1-form").css("display", "none");
        $("#user2-form").css("display", "none");
        $("#user3-form").css("display", "none");
   }else{
       $("#viewProfileImage").attr("src", "");
        $(".headingForm").text("Or sign up with email address");
        $(".social-media-buttons").css("display", "block");
        $("#charity1-form").css("display", "none");
        $("#charity2-form").css("display", "none");
        $("#user1-form").css("display", "block");
        $("#user2-form").css("display", "block");
        $("#user3-form").css("display", "block");
        addUserValidation();   
    }

    $("input:radio").click(function () {
        $("#viewProfileImage").attr("src", "");
        if ($(this).val() == "indiviual") {
            $(".headingForm").text("Or sign up with email address");
            $(".social-media-buttons").css("display", "block");
            $("#charity1-form").css("display", "none");
            $("#charity2-form").css("display", "none");
            $("#user1-form").css("display", "block");
            $("#user2-form").css("display", "block");
            $("#user3-form").css("display", "block");
            $(".text-danger").text("");
            $(".help-block").text("");
            addUserValidation();
        } else {
            $(".headingForm").text("Sign up with email address");
            $(".social-media-buttons").css("display", "none");
            $("#charity1-form").css("display", "block");
            $("#charity2-form").css("display", "block");
            $("#user1-form").css("display", "none");
            $("#user2-form").css("display", "none");
            $("#user3-form").css("display", "none");
            $(".text-danger").text("");
            $(".help-block").text("");
        }
    });
    
    $("form#register-form").submit(function(){
        if($(".text-danger").length>0){
            $(".text-danger").text("");
        }
        if($("input[type=radio][name='sign_up_option']:checked").val() === 'indiviual'){
           addUserValidation();
         }
    });
    
});



function addUserValidation()
{
    // Rule -1 (First Name Required)
    $("input[name=first_name]").rules( "add", {
        required: true,
      //  min: 2,
        messages: {
        required: "First name is required",
      //  min: "First name should contain atleast 2 characters"
        }
    });
  
}