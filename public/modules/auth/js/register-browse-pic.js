$(document).ready(function () {

    if ($('#viewProfileImage').length > 0) {
        if ($('#viewProfileImage', this).attr('src') == '') {
            $('#viewProfileImage').hide();
        }

        $("#upload_pic").change(function () {
            showImage(this, '#viewProfileImage');
        });
    }

    if ($('.phone').length > 0) {
//        $('.phone').mask('0000000000', {clearIfNotMatch: true});
        $('.zip').mask('99999-9999');
        $('.registration').mask('9999-9999-9999-9999');
    }
    
    $("form#register-form input").change(function(e) {
        $(this).siblings(".text-danger").text("");
    });
    $("form#register-form textarea").change(function(e) {
        $(this).siblings(".text-danger").text("");
    });
});    