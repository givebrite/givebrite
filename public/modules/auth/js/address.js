$(document).ready(function(){
  if($("#postcode").val().length>0){
    getCityAndTown();
  } 
   $("input[name=postcode]")
       .geocomplete({details:".details"})
       .bind("geocode:result", function(event, result){
           getCityAndTown();
    });
  if ($('.location-link').length > 0) {
    $(".location-link").click(function() {
        if($(".hide-Location").length>0){
            $("input[name=postcode]").rules( "remove" );
            if($("#postcode-error").length>0){
                $("#postcode-error").text("");
            }
            $("input[name=postcode]").val("");
            $("input[name=country]").val("");
            $("input[name=locality]").val("");
            $("#location-div").removeClass('hide-Location');
            $("#location-div").addClass('show-Location');
        }else{
             $("#location-div").removeClass('show-Location');
             $("#location-div").addClass('hide-Location');
        }
    });
  }
 var geocoder;
 var map; 
  function getCityAndTown() {
            var address = $(".details input[name=formatted_address]").val();
            if(address.length>0){
                geocoder = new google.maps.Geocoder();
                var request = {
                    address:address,
                }
            }else{
                return false;
            }
            
            geocoder.geocode(request, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $("input[name=address]").val($("input[name=postcode]").val());
                    for (var component in results[0]['address_components']) {
                        for (var i in results[0]['address_components'][component]['types']) {
                            if (results[0]['address_components'][component]['types'][i] == "postal_town") {
                                town = results[0]['address_components'][component]['long_name'];
                                $("input[name=locality]").val(town);
                            }
                            if (results[0]['address_components'][component]['types'][i] == "country") {
                                country = results[0]['address_components'][component]['long_name'];
                                $("input[name=country]").val(country);
                            }
                            if (results[0]['address_components'][component]['types'][i] == "postal_code") {
                                postalcode = results[0]['address_components'][component]['short_name'];
                                $("input[name=postcode]").val(postalcode);
                            }
                        }
                    }
                    // Lat-- Long
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    $("input[name=lat]").val(latitude);
                    $("input[name=lng]").val(longitude);
                    errorText=$("#postcode-error").text();
                    if(errorText.length>0){
                        if($(".show-Location").length>0){
                            $("#location-div").removeClass('show-Location');
                            $("#location-div").addClass('hide-Location');
                        }
                    }else if($("#postcode").val().length>0){
                        $("#location-div").removeClass("hide-Location");
                        $("#location-div").addClass("show-Location");
                    }
                } else {
                    alert('Invalid Zipcode');
                    $("#postcode").val("");
                }
            });
        }
});