FusionCharts.ready(function () {

    // Global / General Config
    var App,
            dom,
            dashboards,
            viewHelpers,
            chartConfig,
            currentYear = '2017',
            DOCUMENT = document,
            ieVersion = getIEVersion(),
            //All chart configurations are stored in this variable
            chartConfig = {
                //Chart Configurations for summary tab begins
                //Yearly sales comparison chart configuration
                yearlySalesSummary: {
                    type: 'MSCombiDY2D',
                    id: 'yearlySalesSummary',
                    width: '100%',
                    height: '340',
                    dataFormat: 'json',
                    renderAt: 'yearly-sales-chart',
                    dataSource: {
                        chart: {
                            theme: 'management',
                            numberPrefix: '£',
                        },
                        categories: [{
                                category: [{
                                        label: 'S'
                                    }, {
                                        label: 'M'
                                    }, {
                                        label: 'T'
                                    }, {
                                        label: 'W'
                                    }, {
                                        label: 'T'
                                    }, {
                                        label: 'F'
                                    }, {
                                        label: 'S'
                                    }, {
                                        label: 'S'
                                    }, {
                                        label: 'M'
                                    }, {
                                        label: 'T'
                                    }, {
                                        label: 'W'
                                    }, {
                                        label: 'T'
                                    }, {
                                        label: 'F'
                                    }, {
                                        label: 'S'
                                    }, {
                                        label: 'S'
                                    }, {
                                        label: 'M'
                                    }, {
                                        label: 'T'
                                    }, {
                                        label: 'W'
                                    }, {
                                        label: 'T'
                                    }, {
                                        label: 'F'
                                    }, {
                                        label: 'S'
                                    }, {
                                        label: 'S'
                                    }]
                            }],
                        dataset: [{
                                renderAs: 'column',
                                showValues: '0',
                                data: [{
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }]
                            }, {
                                parentYAxis: 'S',
                                renderAs: 'line',
                                showValues: '0',
                                data: [{
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }, {
                                        value: '100'
                                    }, {
                                        value: '150'
                                    }, {
                                        value: '200'
                                    }]
                            }]
                    }
                },
            };

    App = {
        // Handles application initialization.
        //Any code that you need to execute before the all the dashboards are executed can be specified here.
        init: function (callback) {
            callback();
        }
    }


    dom = {
        // Get the value of a specific dom element by id.
        getElementValue: function (id) {
            var $element = DOCUMENT.getElementById(id);
            return $element.value;
        },
        // Get dom element by id.
        getById: function (id) {
            return DOCUMENT.getElementById(id);
        },
        // Get value of current dom element in scope.
        queryCurrentValue: function (id, obj) {
            // IE fix, because IE doesn't recognize `this` well enough.
            if (obj === window) {
                return dom.getElementValue(id);
            }

            return obj.value;
        }
    };

    dashboards = {
        // An item is a single dashboard.
        items: {},
        // Current active / shown dashboard.
        active: null,
        // Current year for which the data should be shown
        currentYear: null,
        // Add a single dashboard to the items object.
        add: function (id, callback) {
            this.items[id] = {
                rendered: false,
                callback: callback,
                currentYear: currentYear
            };
        },
        // Run / Execute the callback of a particular dashboard.
        run: function (id) {
            var self = this;
            FusionCharts.ready(function () {
                dom.getById(id).className = '';
                self.items[id].rendered = true;
                self.items[id].callback();
                viewHelpers.addActiveLink(id + '-' + 'link');
                self.active = id;
                self.items[id].currentYear = currentYear;
            });
        },
        // Show a dashboard. This also toggles the active dashboard.
        show: function (id) {
            var self = this,
                    $element,
                    $activeElement,
                    $link,
                    redraw = false;

            if (this.items[this.active].rendered) {
                if (currentYear != this.items[id].currentYear) {
                    redraw = true;
                    this.items[id].currentYear = currentYear;
                }
            }

            for (var key in this.items) {
                $element = dom.getById(key);
                viewHelpers.addActiveLink(key + '-' + 'link');
                $link = dom.getById(key + '-' + 'link');

                if (key !== id) {
                    $element.style.display = 'none';
                    $link.className = '';
                } else {
                    this.active = key;
                    $activeElement = $element;
                }
            }

            $activeElement.style.display = 'block';
            if (!this.items[this.active].rendered) {
                this.run(this.active);
            }
            if (redraw) {
                this.redraw(id);
            }
        },
        // Redraw all the charts of a particular dashboard.
        redraw: function (id) {
            FusionCharts.ready(function () {
                var ids = getEventIds(id);
                eventListeners.trigger('change', function (e) {
                    eventHandlers.trigger.topLevelYearFilter(ids, {
                        trigger: e,
                        year: currentYear,
                        eventName: 'change'
                    });
                });
            });
        }
    };

    //Handles adding of events and triggering.

    dashboards.add('summary', function () {
        // Config for Yearly Sales Chart
        var yearlySalesChart,
                yearlySalesChartConfig = chartConfig.yearlySalesSummary;


        yearlySalesChart = new FusionCharts(yearlySalesChartConfig);
        yearlySalesChart.render();

    });

    // Private Methods
    // Get all chart dom ids for a dashboard tab.
    var getEventIds = function (id) {
        var ids = {
            summary: ['top_sales_performers_summary_year_filter', 'top_revenues_country_year_filter',
                'top_products_summary_year_filter', 'top_revenues_cities_summary_year_filter'
            ],
        };

        return ids[id];
    };

    // Polyfill for string trim
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    };

    //// Polyfill for index of array
    var indexOfArray = function (list, value) {
        var i, listLength;
        if (Array.prototype.indexOf) {
            return list.indexOf(value);
        } else {
            listLength = list.length;
            for (i = 0; i < listLength; i++) {
                if (list[i] === value) {
                    return i;
                }
            }
        }

        return -1;
    };

    // Polyfill for get elements by class name
    var getElementsByClassName = function (tag, className) {
        var i, result,
                elements, elementLength;

        if (DOCUMENT.getElementsByClassName) {
            result = DOCUMENT.getElementsByClassName('fusioncharts-container');
        } else {
            result = [];
            elements = DOCUMENT.getElementsByTagName(tag);
            elementLength = elements.length;
            for (i = 0; i < elementLength; i++) {
                if (elements[i].className === className) {
                    result.push(elements[i]);
                }
            }
        }

        return result;
    };

    // Check if IE6
    function isIE6() {
        return (ieVersion > 0 && ieVersion < 7) ? true : false;
    }

    // Check if IE is less than version 8
    function isLessThan8() {
        return (ieVersion > 0 && ieVersion <= 8) ? true : false;
    }

    // Get current version of IE
    function getIEVersion() {
        var pattern = /MSIE (\d+\.\d+);/;

        if (pattern.test(window.navigator.userAgent)) {
            return new Number(RegExp.$1);
        }

        return 0;
    }

    // Handle permalink of tabs switching.
    var urlHandler = function () {
        var tab,
                availableTabs,
                pattern = /#([^#]+)/;
        tab = DOCUMENT.URL ? DOCUMENT.URL.match(pattern) : window.location.match(pattern);
        availableTabs = ['summary'];

        if (tab) {
            if (indexOfArray(availableTabs, tab[1].trim()) === -1) {
                dashboards.run('summary');
            } else {
                dashboards.run(tab[1]);
            }
        } else {
            dashboards.run('summary');
        }
    };

    /**
     * Main App Initializtion
     */
    App.init(function () {
        urlHandler();
    });

});