/*
 FusionCharts JavaScript Library
 Copyright FusionCharts Technologies LLP
 License Information at <http://www.fusioncharts.com/license>
 */

/**
 * This file create a theme file for the dashboard. The cosmetics for the
 * different charts are defined here.
 *
 * More information on theming can be found at
 * http://docs.fusioncharts.com/tutorial-configuring-your-chart-theme-manager.html
 */
FusionCharts.register('theme', {
    name: 'management',
    theme: {
        base: {
            chart: {
                baseFont: "'HelveticaNeue', 'Helvetica Neue', Helvetica, Arial, Verdana",
                baseFontSize: "11",
                baseFontColor: "#666666",
                bgAlpha: "0",
                bgColor: "#FFFFFF",
                canvasBorderThickness: "0",
                showBorder: "0",
                showShadow: "0"
            }
        },
        MSCombiDY2D: {
            chart: {
                divLineDashed: "1",
                divLineDashLen: "2",
                formatNumberScale: 10,
                legendBorderThickness: 0,
                legendShadow: 0,
                paletteColors: "#ececea, #dfdfdc, #005072, #feb000, #5b8ab0, #29bac7, #e36966, #d4d6bc",
                plotHoverEffect: "1",
                plotSpacePercent: "10",
                showAlternateHGridColor: "0",
                showHoverEffect: "1",
                showPlotBorder: "0",
                showToolTipShadow: "0",
                showValues: "0",
                showXAxisLine: "1",
                toolTipBgColor: "#34a13e",
                toolTipBorderRadius: "5",
                toolTipColor: "#EEEEEE",
                toolTipFontSize: "11",
                useEllipsesWhenOverflow: 1,
                usePlotGradientColor: "0",
                xAxisNameFontColor: "#333333",
                xAxisNameFontSize: "11",
                xAxisLineColor: "#979797",
                xAxisLineThickness: "1",
                lineThickness: "1",
                anchorRadius: "3",
                anchorBorderThickness: "2"
            }
        },
    }
});
