/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$("#popular_campaign").click(function(){
    getCampaigns(1);
});
$("#my_area_campaign").click(function(){
    getAreaCampaigns(1);
});

$("#my_fb_campaign").click(function(){
    getFbCampaigns(1);
});


// Ajax Pagination Example

$(document).ready(function() {
     $(document).on('click', '.pagination a', function(e) {
        var activeTab=$('ul#campaign_list_tab li.active a').attr('id');
        /*Condtions According To Active Tab*/
            if(activeTab=='popular_campaign'){
                getCampaigns($(this).attr('href').split('page=')[1]);
            }else if(activeTab=='my_area_campaign'){
                getAreaCampaigns($(this).attr('href').split('page=')[1]);
            }else if(activeTab=='my_fb_campaign'){
                getFbCampaigns($(this).attr('href').split('page=')[1]);
            }
        /*End Condtions*/
        e.preventDefault();
    });
});

function getCampaigns(page) {
  //  $("#loadingDiv").removeClass('hidden');
    var url = $("#popular_campaign_url").val();
    var request = $(this);
            if (request.data('requestRunning')) {
         //       return;
            }
    request.data('requestRunning', true);
    $.ajax({
        url: url+'?page=' + page,
        dataType: 'json',
        
    }).done(function(data) {
        if(data.responseCode==200){
    //        setTimeout(function(){ $("#loadingDiv").addClass('hidden'); }, 1000);
            $('#popular').html(data.responseView);
            $('#popular-page-'+page+' .percent').percentageLoader({
		valElement: 'p',
		strokeWidth: 5,
		bgColor: '#d9d9d9',
                ringColor: '#37c2df',
		textColor: '#9f9f9f',
		fontSize: '18px',
		fontWeight: 'bold'
	    });
        }
        request.data('requestRunning', false);
    }).fail(function() {
      //  alert('Campaigns could not be loaded.');
    });
   
}
    
 function  getAreaCampaigns(page){
    
    var lat= $("#current_latitude").val();
    var long=$("#current_longitude").val();
   // $("#loading-image").css("display","inline");
    if(lat.length > 0 && long.length>0){
       // Ajax request To Fetch My Area Campaigns
       var url=$("#area_campaign_url").val();
       var request = $(this);
            if (request.data('requestRunning')) {
                return;
            }
       request.data('requestRunning', true);
       $.ajax({
            url: url+'?page=' + page,
            type: 'post',
            dataType:'json',
            data: {"latitude":lat,"longitude":long},
	    headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            },
            success: function (response) {
                if(response.responseCode==200){
                   //$("#loading-image").css("display","none") 
                   $("#myarea").html(response.responseView);
                   $('#area-page-'+page+' .percent').percentageLoader({
                        valElement: 'p',
                        strokeWidth: 5,
                        bgColor: '#d9d9d9',
                        ringColor: '#37c2df',
                        textColor: '#9f9f9f',
                        fontSize: '18px',
                        fontWeight: 'bold'
	            });
                }
            },
            error: function(response){
                var errors = response.responseJSON;
                $.each(errors, function(index, value) {
                   $("#err_"+index).text(value);
                });
            },
            complete: function () {
                request.data('requestRunning', false);
            }
        });
    }
    else{
        // Do Nothing
    }
 }  

function  getFbCampaigns(page){
    var url = $("#facebook_campaign_url").val();
    var request = $(this);
            if (request.data('requestRunning')) {
                return;
            }
    request.data('requestRunning', true);
    $.ajax({
        url: url+'?page=' + page,
        dataType: 'json',
    }).done(function(data) {
        if(data.responseCode==200){
            $("#myfacebook").html(data.responseView);
            $('#fb-page-'+page+' .percent').percentageLoader({
                        valElement: 'p',
                        strokeWidth: 5,
                        bgColor: '#d9d9d9',
                        ringColor: '#37c2df',
                        textColor: '#9f9f9f',
                        fontSize: '18px',
                        fontWeight: 'bold'
	            });
        }
       // location.hash = page;
        request.data('requestRunning', false);
    }).fail(function() {
      //  alert('Campaigns could not be loaded.');
    });
 } 
