$(document).ready(function() {
    getCharities(1);
    $(document).on('click', '.pagination a', function(e) {
        getCharities($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    });
    $("input[name=keyword]").keypress(function(e) {
            // Enter pressed?
            if(e.which == 10 || e.which == 13) {
                getCharities(1,true);
            }
    });

    
});
$("#charity-search").click(function(){
    getCharities(1,true);
});
$("#load-more-charity").click(function(){
    var next = $( ".pagination li a[aria-label='Next']" );
    var nextText = next.text();
    if(nextText.length > 0){
        next.trigger( "click" );
    }
});
function getCharities(page,searchClick) {
    var url = $("#charity_url").val();
    var search = $("input[name=keyword]").val();
    if(search.length > 0){
      url=  url+'?keyword='+search+'&page=' + page; 
    }else{
      url= url+'?page=' + page; 
    }
    var request = $(this);
            if (request.data('requestRunning')) {
                return;
            }
    request.data('requestRunning', true);
    $.ajax({
        url: url,
        dataType: 'json',
        
    }).done(function(data) {
        if(data.responseCode==200){
           $("#charity-pagination").html(data.paginationView);
          if(searchClick==true){
             $(".scroll").html(data.responseView);
          } else{
            $(data.responseView).hide()
            .appendTo('.scroll')
            .fadeIn('slow');
          }
          
    
        // Remove Load more button on last page
            var next = $( ".pagination li a[aria-label='Next']" );
            var nextText = next.text();
            if(nextText.length > 0){
                $("#load-more-charity").css("display","inline");
            }else{
                $("#load-more-charity").css("display","none");
            }
        }
        request.data('requestRunning', false);
    }).fail(function() {
      //  alert('Campaigns could not be loaded.');
    });
   
}



