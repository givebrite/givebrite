$(document).ready(function() {
    getDonorsFundedCampaign(1);
    getDonorsRaisedCampaign(1);
    // On Click Of Funded Load More
    $(document).on('click', '#funded-pagination .pagination a', function(e) {
        getDonorsFundedCampaign($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    });
    // On Click Of Raised Load More
    $(document).on('click', '#raised-pagination .pagination a', function(e) {
        getDonorsRaisedCampaign($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    });
});

$("#load-more-funded-campaign").click(function(){
    var next = $( "#funded-pagination .pagination li a[aria-label='Next']" );
    var nextText = next.text();
    if(nextText.length > 0){
        next.trigger( "click" );
    }
});

$("#load-more-raised-campaign").click(function(){
    var next = $( "#raised-pagination .pagination li a[aria-label='Next']" );
    var nextText = next.text();
    if(nextText.length > 0){
        next.trigger( "click" );
    }
});

function getDonorsFundedCampaign(page)
{
    var url = $("#funded_campaign_url").val();
    var request = $(this);
            if (request.data('requestRunning')) {
         //       return;
            }
    request.data('requestRunning', true);
    $.ajax({
        url: url+'?page=' + page,
        dataType: 'json',
        
    }).done(function(data) {
        if(data.responseCode==200){
           $("#funded-pagination").html(data.paginationView);
            $(data.responseView).hide()
            .appendTo('#funded-scroll')
            .fadeIn('slow');
            $('#fund-list-page-'+page+' .percent').percentageLoader({
		valElement: 'p',
		strokeWidth: 5,
		bgColor: '#d9d9d9',
                ringColor: '#37c2df',
		textColor: '#9f9f9f',
		fontSize: '18px',
		fontWeight: 'bold'
	    });
        // Remove Load more button on last page
            var next = $( "#funded-pagination .pagination li a[aria-label='Next']" );
            var nextText = next.text();
            if(nextText.length > 0){
                $("#load-more-funded-campaign").css("display","inline");
            }else{
                $("#load-more-funded-campaign").css("display","none");
            }
        }
        request.data('requestRunning', false);
    }).fail(function() {
        
    });
}

function getDonorsRaisedCampaign(page)
{
    var url = $("#raised_campaign_url").val();
    var request = $(this);
            if (request.data('requestRunning')) {
         //       return;
            }
    request.data('requestRunning', true);
    $.ajax({
        url: url+'?page=' + page,
        dataType: 'json',
        
    }).done(function(data) {
        if(data.responseCode==200){
           $("#raised-pagination").html(data.paginationView);
            $(data.responseView).hide()
            .appendTo('#raised-scroll')
            .fadeIn('slow');
            $('#raise-list-page-'+page+' .percent').percentageLoader({
		valElement: 'p',
		strokeWidth: 5,
		bgColor: '#d9d9d9',
                ringColor: '#37c2df',
		textColor: '#9f9f9f',
		fontSize: '18px',
		fontWeight: 'bold'
	    });
        // Remove Load more button on last page
            var next = $( "#raised-pagination .pagination li a[aria-label='Next']" );
            var nextText = next.text();
            if(nextText.length > 0){
                $("#load-more-raised-campaign").css("display","inline");
            }else{
                $("#load-more-raised-campaign").css("display","none");
            }
        }
        request.data('requestRunning', false);
    }).fail(function() {
    });
}