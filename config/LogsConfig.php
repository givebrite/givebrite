<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class LogsConfig {

    public function setUpFile() {
        $now = date('Y-m-d');

        // create a log channel
        $log = new Logger(Config::get('config.project_name'));
        $log->pushHandler(new StreamHandler(storage_path() . '/logs/customlogs-' . $now . '.log', Logger::DEBUG));

        return $log;
    }

    public static function displayLogsError($type, $error) {

        $logsFile = new LogsConfig();
        $log = $logsFile->setUpFile();
        
        switch ($type) {
            case Config::get('config.info_type'):
                $log->addInfo($error);
                break;
            case Config::get('config.warning_type'):
                $log->addWarning($error);
                break;
            case Config::get('config.error_type'):
                $log->addError($error);
                break;
            default:
                $log->addNotice($error);
                break;
        }
    }

}