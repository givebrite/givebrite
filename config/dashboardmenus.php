<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
	// Menu Items For User Dashboard
	'Admin' => [
		'site/dashboard_sidebar.menus.my_account_title' => [
			'Account Active' => 'site/dashboard_sidebar.menus.my_account.account_active',
			'Profile' => 'site/dashboard_sidebar.menus.my_account.profile',
			// 'FaceBook'=>'site/dashboard_sidebar.menus.my_account.facebook',
			// 'Twitter'=>'site/dashboard_sidebar.menus.my_account.twitter'
			//'Logout'=>'site/dashboard_sidebar.menus.my_account.logout'
		],

		'site/dashboard_sidebar.menus.campaigns_title' => [
			'My Campaigns' => 'site/dashboard_sidebar.menus.campaigns.my_campaigns',
			'Donations' => 'site/dashboard_sidebar.menus.campaigns.donations',
			'Comments' => 'site/dashboard_sidebar.menus.campaigns.comments',
			// 'Rewards'=>'site/dashboard_sidebar.menus.campaigns.rewards',
			// 'Insights'=>'site/dashboard_sidebar.menus.campaigns.insights',
		],
		/**
		'site/dashboard_sidebar.menus.tools_title'=>[
		'Events'=>'site/dashboard_sidebar.menus.tools.events',
		'Social Media Marketing'=>'site/dashboard_sidebar.menus.tools.social_media_marketing',
		'Send Texts'=>'site/dashboard_sidebar.menus.tools.send_texts',
		'Send Emails'=>'site/dashboard_sidebar.menus.tools.send_emails',
		'Order Print Material'=>'site/dashboard_sidebar.menus.tools.order_print_material',
		]
		 *
		 */
	],

	'User' => [
		'site/dashboard_sidebar.menus.my_account_title' => [
			'Account Active' => 'site/dashboard_sidebar.menus.my_account.account',
			'Profile' => 'site/dashboard_sidebar.menus.my_account.profile',
			'Stripe' => 'site/dashboard_sidebar.menus.my_account.stripe',
			// 'FaceBook' => 'site/dashboard_sidebar.menus.my_account.facebook',
			// 'Twitter' => 'site/dashboard_sidebar.menus.my_account.twitter',
			// 'Logout' => 'site/dashboard_sidebar.menus.my_account.logout',
		],

		'site/dashboard_sidebar.menus.campaigns_title' => [
			'My Campaigns' => 'site/dashboard_sidebar.menus.campaigns.my_campaigns',
			'Donations' => 'site/dashboard_sidebar.menus.campaigns.donations',
			'Comments' => 'site/dashboard_sidebar.menus.campaigns.comments',
			// 'Rewards'=>'site/dashboard_sidebar.menus.campaigns.rewards',
			// 'Insights'=>'site/dashboard_sidebar.menus.campaigns.insights',
		],
		/**
		'site/dashboard_sidebar.menus.tools_title'=>[
		'Events'=>'site/dashboard_sidebar.menus.tools.events',
		'Social Media Marketing'=>'site/dashboard_sidebar.menus.tools.social_media_marketing',
		'Send Texts'=>'site/dashboard_sidebar.menus.tools.send_texts',
		'Send Emails'=>'site/dashboard_sidebar.menus.tools.send_emails',
		'Order Print Material'=>'site/dashboard_sidebar.menus.tools.order_print_material',
		]
		 *
		 */
	],

	'ActiveUser' => [
		'site/dashboard_sidebar.menus.my_account_title' => [
			'Account Active' => 'site/dashboard_sidebar.menus.my_account.account_active',
			'Profile' => 'site/dashboard_sidebar.menus.my_account.profile',
			'Stripe' => 'site/dashboard_sidebar.menus.my_account.stripe',
			// 'FaceBook' => 'site/dashboard_sidebar.menus.my_account.facebook',
			// 'Twitter' => 'site/dashboard_sidebar.menus.my_account.twitter',
			// 'Logout' => 'site/dashboard_sidebar.menus.my_account.logout',
		],

		'site/dashboard_sidebar.menus.campaigns_title' => [
			'My Campaigns' => 'site/dashboard_sidebar.menus.campaigns.my_campaigns',
			'Team Pages'=>'site/dashboard_sidebar.menus.campaigns.team_pages',
			'Donations' => 'site/dashboard_sidebar.menus.campaigns.donations',
			'Comments' => 'site/dashboard_sidebar.menus.campaigns.comments',
			//'Rewards'=>'site/dashboard_sidebar.menus.campaigns.rewards',
			// 'Insights'=>'site/dashboard_sidebar.menus.campaigns.insights',
		],
		/**
		'site/dashboard_sidebar.menus.tools_title'=>[
		'Events'=>'site/dashboard_sidebar.menus.tools.events',
		'Social Media Marketing'=>'site/dashboard_sidebar.menus.tools.social_media_marketing',
		'Send Texts'=>'site/dashboard_sidebar.menus.tools.send_texts',
		'Send Emails'=>'site/dashboard_sidebar.menus.tools.send_emails',
		'Order Print Material'=>'site/dashboard_sidebar.menus.tools.order_print_material',
		]
		 *
		 */
	],

	// Menu Items For Charity Dashboard
	'Charity' => [
		'site/dashboard_sidebar.menus.my_account_title' => [
			'Account Active' => 'site/dashboard_sidebar.menus.my_account.account',
			'Profile' => 'site/dashboard_sidebar.menus.my_account.profile',
			//  'FaceBook'=>'site/dashboard_sidebar.menus.my_account.facebook',
			// 'Twitter'=>'site/dashboard_sidebar.menus.my_account.twitter'
			//'Logout'=>'site/dashboard_sidebar.menus.my_account.logout'
		],

		'site/dashboard_sidebar.menus.campaigns_title' => [
			'My Campaigns' => 'site/dashboard_sidebar.menus.campaigns.my_campaigns',
			'Donations' => 'site/dashboard_sidebar.menus.campaigns.donations',
			'Comments' => 'site/dashboard_sidebar.menus.campaigns.comments',
			'Stripe Connection' => 'site/dashboard_sidebar.menus.my_account.stripe',
			//'Rewards'=>'site/dashboard_sidebar.menus.campaigns.rewards',
			//'Insights'=>'site/dashboard_sidebar.menus.campaigns.insights',
		],
		/**
		'site/dashboard_sidebar.menus.tools_title'=>[
		'Events'=>'site/dashboard_sidebar.menus.tools.events',
		'Social Media Marketing'=>'site/dashboard_sidebar.menus.tools.social_media_marketing',
		'Send Texts'=>'site/dashboard_sidebar.menus.tools.send_texts',
		'Send Emails'=>'site/dashboard_sidebar.menus.tools.send_emails',
		'Order Print Material'=>'site/dashboard_sidebar.menus.tools.order_print_material',
		]**/

	],

	'ActiveCharity' => [
		'site/dashboard_sidebar.menus.my_account_title' => [
			'Account Active' => 'site/dashboard_sidebar.menus.my_account.account_active',
			'Profile' => 'site/dashboard_sidebar.menus.my_account.profile',
			//  'FaceBook'=>'site/dashboard_sidebar.menus.my_account.facebook',
			// 'Twitter'=>'site/dashboard_sidebar.menus.my_account.twitter'
			//'Logout'=>'site/dashboard_sidebar.menus.my_account.logout'
		],

		'site/dashboard_sidebar.menus.campaigns_title' => [
			'My Campaigns' => 'site/dashboard_sidebar.menus.campaigns.my_campaigns',
			'Team Pages'=>'site/dashboard_sidebar.menus.campaigns.team_pages',
			'Donations' => 'site/dashboard_sidebar.menus.campaigns.donations',
			'Comments' => 'site/dashboard_sidebar.menus.campaigns.comments',
			'Stripe Connection' => 'site/dashboard_sidebar.menus.my_account.stripe',
			//'Rewards'=>'site/dashboard_sidebar.menus.campaigns.rewards',
			//'Insights'=>'site/dashboard_sidebar.menus.campaigns.insights',
		],
		/**
		'site/dashboard_sidebar.menus.tools_title'=>[
		'Events'=>'site/dashboard_sidebar.menus.tools.events',
		'Social Media Marketing'=>'site/dashboard_sidebar.menus.tools.social_media_marketing',
		'Send Texts'=>'site/dashboard_sidebar.menus.tools.send_texts',
		'Send Emails'=>'site/dashboard_sidebar.menus.tools.send_emails',
		'Order Print Material'=>'site/dashboard_sidebar.menus.tools.order_print_material',
		]**/

	],

	/**
	 * Class for all
	 * 'Key'=>'class name'
	 */
	'Class' => [
		'Account Active' => 'account-sec',
		'Profile' => 'profile-sec',
		'Stripe' => 'stripe-sec',
		'FaceBook' => 'face-sec',
		'Twitter' => 'twitter-sec',
		'Logout' => 'profile-sec',
		'My Campaigns' => 'camp-sec',
		'Team Pages' => 'team-pages',
		'Donations' => 'dona-sec',
		'Comments' => 'comm-sec',
		'Rewards' => 'reward-sec',
		'Team Pages' => 'team-pages',
		'Insights' => 'insight-sec',
		'Events' => 'events-sec',
		'Social Media Marketing' => 'socialMedia-sec',
		'Send Texts' => 'sendText-sec',
		'Send Emails' => 'sendEmails-sec',
		'Order Print Material' => 'orderPring-sec',
		'Stripe Connection' => 'profile-sec',
	],

	'Routes' => [
		'Account Active' => 'dashboard.index',
		'Profile' => 'dashboard.profile',
		'Stripe Connection' => 'stripe.connect',
		'Stripe' => 'stripe.connect',
		'FaceBook' => '',
		'Twitter' => '',
		'Logout' => 'logout',
		'My Campaigns' => 'dashboard.campaign-list',
		'Donations' => 'dashboard.campaign-donations',
		'Comments' => 'dashboard.comments-list',
		'Rewards' => '',
		'Team Pages' => 'dashboard.team-pages',
		'Insights' => '',
		'Events' => '',
		'Social Media Marketing' => '',
		'Send Texts' => '',
		'Send Emails' => '',
		'Order Print Material' => '',
	],
];
