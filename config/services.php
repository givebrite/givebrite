<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Third Party Services
      |--------------------------------------------------------------------------
      |
      | This file is for storing the credentials for third party services such
      | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
      | default location for this type of information, allowing packages
      | to have a conventional place to find your various credentials.
      |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'twitter' => [
        'client_id' => 'dkRy5scm1AkB4qX0X1BNkgcgK',
        'client_secret' => 'SYADyjn0aUV9WuHRwdakbjM8pDJy5036fPDBqsE3h2FkTC3adh',
        'redirect' => 'http://givebrite.com/public/auth/callback/twitter',
    ],
    'facebook' => [
        'client_id' => '1083528158379481',
        'client_secret' => 'b13c2469cf1782f178dc82384bad4195',
        'redirect' => 'http://givebrite.com/givebrite/public/auth/callback/facebook',
        'user_access_token' => 'EAACEdEose0cBABQ5pSp20wzmjyNXsgZBMDLg9up9RpcTBis0w467Px6WBn3nnzLAKuNZAZAiO5XXBefpOTaFkq6tqZAa5AKKZCodwD5sbtxjlRc3DfRyRSju5JHAcfKxqUKf99qL5ZA0m8OiWVP5EGdxBb3kvcqLjjAEAjZB3vvGQZDZD'
    ],
    'google' => [
        'client_id' => '1361340481-gjrmgeijtuvka5fqknjtdbeu2d6g6vlg.apps.googleusercontent.com',
        'client_secret' => 'bEjUzXPLCacAjB8a6G9AqisX',
        'redirect' => 'http://givebrite.com/auth/callback/google',
    ],
];
